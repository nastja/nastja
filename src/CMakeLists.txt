# Source files.
#
set(SOURCES
  main.cpp
  apps/cells.cpp
  apps/droplet.cpp
  apps/heat.cpp
  apps/phasefield.cpp
  apps/phasefield2.cpp
  apps/phasefield_temp.cpp
  apps/sleep.cpp
  sweeps/sweep_cells.cpp
  sweeps/sweep_ecmdegrade.cpp
  sweeps/sweep_signaldiffusion.cpp
  sweeps/sweep_heat.cpp
  sweeps/sweep_heat_avx.cpp
  sweeps/sweep_heat_iter.cpp
  sweeps/sweep_pf-vp.cpp
  sweeps/sweep_pf1.cpp
  sweeps/sweep_pftemp.cpp
  sweeps/sweep_sleep.cpp
  sweeps/sweep_temp.cpp
)

if(NASTJA_ENABLE_PFC)
  list(APPEND SOURCES
    apps/pfc.cpp
    sweeps/sweep_pfc.cpp
  )
endif()

if(NASTJA_ENABLE_CUDA)
  list(APPEND SOURCES
    apps/phasefield_temp_cuda.cpp
    sweeps/sweep_heat_cuda.cpp
    sweeps/sweep_heat_kernel.cu
    sweeps/sweep_temp_cuda.cpp
    sweeps/sweep_temp_kernel.cu
    sweeps/sweep_pftemp_cuda.cpp
    sweeps/sweep_pftemp_kernel.cu
  )
endif()

# NAStJA target using the library.
#
add_executable(nastja ${SOURCES})
target_link_libraries(nastja PRIVATE nastjalibrary)
set_target_properties(nastja PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")
target_compile_definitions(nastja PUBLIC NASTJA_MULTI_APPLICATION)
if(HAS_IPO_SUPPORT)
  set_target_properties(nastja PROPERTIES INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()

add_subdirectory(lib)
