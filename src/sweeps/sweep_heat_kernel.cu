/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/utility.h"
#include "lib/cuda/cuda_sweep_ops.h"
#include "lib/stencil/direction.h"
#include "sweep_heat_kernel.h"

/**
 * With nvcc 10.1 there are problems with the JSON library includes, therefore we can't include field.h because the json headers are used there.
 * https://github.com/nlohmann/json/issues/1347
 *
 * This might be solved in nvcc 10.2
 * https://github.com/nlohmann/json/issues/1773#issuecomment-557489347
 */

namespace nastja {
namespace cuda {

using namespace stencil;

__constant__ CudaGpuFieldPtrs<real_t> oldGpuPtr;
__constant__ CudaGpuFieldPtrs<real_t> newGpuPtr;

template <Direction dir>
__global__ void heatKernel(CudaArguments args, CudaSimData cSimData) {
  //real_t* dataNew, real_t* data, real_t* exchangeNew, real_t* exchange
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x + args.startX;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y + args.startY;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z + args.startZ;

  // if (x < args.sizeX - args.boundarySizeX && y < args.sizeY - args.boundarySizeY && z < args.sizeZ - args.boundarySizeZ) {
  if (checkLimits<dir>(x, y, z, args)) {
    real_t cells[7];
    loadStencil<7, dir>(cells, oldGpuPtr, x, y, z, args);

    real_t cell    = cells[toUnderlying(Direction::C)];
    real_t cell_xm = cells[toUnderlying(Direction::L)];
    real_t cell_xp = cells[toUnderlying(Direction::R)];
    real_t cell_ym = cells[toUnderlying(Direction::D)];
    real_t cell_yp = cells[toUnderlying(Direction::U)];
    real_t cell_zm = cells[toUnderlying(Direction::B)];
    real_t cell_zp = cells[toUnderlying(Direction::F)];

    cell += cSimData.deltat * (real_t(-6.0) * cell + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm) / (cSimData.deltax * cSimData.deltax);

    storeStencil<dir>(newGpuPtr, x, y, z, args, cell);
  }
}

void sweep_heat_kernel(cudaStream_t stream, cudaStream_t haloStream, CudaGpuFieldPtrs<real_t> h_oldGpuPtr, CudaGpuFieldPtrs<real_t> h_newGpuPtr, CudaArguments args, CudaSimData cSimData) {
  cudaDeviceSynchronize();

  // Using MemcpyToSymbol instead of kernel parameter allows for an easy description of an abstract kernel type.
  // This is used in the following line to iterate through all 27 kernels
  gpuErrchk(cudaMemcpyToSymbol(oldGpuPtr, &h_oldGpuPtr, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(newGpuPtr, &h_newGpuPtr, sizeof(CudaGpuFieldPtrs<real_t>)));

  // This is performing something like a loop unrolling at compile time.
  // Therefore all templated instances of the heatKernel can be created at compiletime
  // This is not working with a usual for loop over all the directions, template packs have to be used.
  staticFor<numberOfDirections>([&](auto i) { launchKernel<static_cast<Direction>(i())>(heatKernel<static_cast<Direction>(i())>, stream, haloStream, args, cSimData); });
}

}  // namespace cuda
}  // namespace nastja
