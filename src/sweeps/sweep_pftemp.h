/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/math/polynomial.h"
#include "lib/simdata/datapfm.h"

namespace nastja {

constexpr bool anisotropic = true;
constexpr bool betazero    = true;

class SweepPhaseFieldTemperature : public Sweep {
public:
  explicit SweepPhaseFieldTemperature() {
    registerFieldname("phasefield");
    registerFieldname("temperature");
  }

  ~SweepPhaseFieldTemperature() override                        = default;
  SweepPhaseFieldTemperature(const SweepPhaseFieldTemperature&) = delete;
  SweepPhaseFieldTemperature(SweepPhaseFieldTemperature&&)      = default;
  SweepPhaseFieldTemperature& operator=(const SweepPhaseFieldTemperature&) = delete;
  SweepPhaseFieldTemperature& operator=(SweepPhaseFieldTemperature&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override;

private:
  real_t diffu(real_t center, real_t upper) const;
  real_t diffl(real_t center, real_t lower) const;
  real_t diffc(real_t lower, real_t upper) const;
  real_t dfLdphi(real_t phi, real_t T, const DataPFM& data) const;
  real_t dfTdphi(real_t phi, real_t T, const DataPFM& data) const;
  real_t A(const math::Vector3<real_t>& q, const DataPFM& data) const;
  math::Vector3<real_t> dAdq(const math::Vector3<real_t>& q, const DataPFM& data) const;
};

}  // namespace nastja
