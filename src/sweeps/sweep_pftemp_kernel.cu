/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/utility.h"
#include "lib/cuda/cuda_math.h"
#include "lib/cuda/cuda_real_functions.h"
#include "lib/cuda/cuda_sweep_ops.h"
#include "lib/stencil/direction.h"
#include "sweep_pftemp_kernel.h"

/**
 * With nvcc 10.1 there are problems with the JSON library includes, therefore we can't include field.h because the json headers are used there.
 * https://github.com/nlohmann/json/issues/1347
 *
 * This might be solved in nvcc 10.2
 * https://github.com/nlohmann/json/issues/1773#issuecomment-557489347
 */

namespace nastja {
namespace cuda {

__constant__ real_t CVa[4];
__constant__ real_t CVb[4];
__constant__ real_t La[5];
__constant__ real_t k[2];
int phaseConstCopied = 0;

__constant__ real_t epsilon1 = 0.10191;
__constant__ real_t epsilon2 = -0.00134;
__constant__ real_t epsilon3 = 0.00876;

__constant__ real_t Tm  = 1.0;
__constant__ real_t gab = 0.4396396751;
__constant__ real_t k0  = 5.5797034;

__constant__ real_t epsilon_k = 0.230331;
__constant__ real_t delta_k   = -0.196511;

__constant__ real_t MplusF = 0.182223;

__constant__ CudaGpuFieldPtrs<real_t> phaseOldGpuPtr;
__constant__ CudaGpuFieldPtrs<real_t> phaseNewGpuPtr;

__constant__ CudaGpuFieldPtrs<real_t> tempNewGpuPtr;

using namespace stencil;

__device__ real_t dfLdphi(real_t phi, real_t T, real_t* La) {
  real_t L = calc(La, 5, T);
  return L * (T - Tm) / Tm * dhdphi(phi);
}

__device__ real_t dfTdphi(real_t phi, real_t T, real_t* CVa, real_t* CVb) {
  real_t intcva = calcIntegral(CVa, 4, T) - calcIntegral(CVa, 4, Tm);
  real_t intcvb = calcIntegral(CVb, 4, T) - calcIntegral(CVb, 4, Tm);

  real_t suminta = CVa[0] * real_log(T / Tm);
  for (unsigned long j = 1; j <= 3; j++) {
    suminta += real_t(1.0) / real_t(j) * CVa[j] * (real_pow(T, j) - real_pow(Tm, j));
  }

  real_t sumintb = CVb[0] * real_log(T / Tm);
  for (unsigned long j = 1; j <= 3; j++) {
    sumintb += real_t(1.0) / real_t(j) * CVb[j] * (real_pow(T, j) - real_pow(Tm, j));
  }

  return (intcva - T * suminta - intcvb + T * sumintb) * dhdphi(phi);
}

__device__ real_t A(const real3_t& q) {
  real_t q2 = q.x * q.x + q.y * q.y + q.z * q.z;

  real_t q4 = q2 * q2;
  real_t q6 = q4 * q2;

  real_t q44 = (q.x * q.x * q.x * q.x + q.y * q.y * q.y * q.y + q.z * q.z * q.z * q.z) / q4;

  real_t q123 = (q.x * q.x * q.y * q.y * q.z * q.z) / q6;

  return real_t(1.0) + epsilon1 * (q44 - real_t(0.6)) +
         epsilon2 * (real_t(3.0) * q44 + real_t(66.0) * q123 - real_t(17.0) / real_t(7.0)) +
         epsilon3 * (real_t(5.0) * q44 * q44 - real_t(16.0) * q123 - (real_t(94.0) * q44 - real_t(33.0)) / real_t(13.0));
}

__device__ real3_t normalize(real3_t a) {
  real3_t ret;
  real_t divsqrt;
  divsqrt = real_sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
  ret.x   = a.x * divsqrt;
  ret.y   = a.y * divsqrt;
  ret.z   = a.z * divsqrt;
  return ret;
}

__inline__ __device__ real3_t dAdq(const real3_t& q) {
  real3_t res = make_real3(real_t(0), real_t(0), real_t(0));

  real_t q2 = q.x * q.x + q.y * q.y + q.z * q.z;

  if (q2 == real_t(0.0)) return res;

  real_t q2r = real_t(1.0) / q2;

  real_t q123 = real_t(1.0);
  real_t q44  = real_t(0.0);
  real3_t q3;

  //unrolled by hand
  // for (unsigned long k = 0; k < 3; k++) {
  real_t q_2 = q.x * q.x;

  q123 *= q_2 * q2r;
  q44 += q_2 * q_2;

  q3.x = q_2 * q.x;

  // now y
  q_2 = q.y * q.y;

  q123 *= q_2 * q2r;
  q44 += q_2 * q_2;

  q3.y = q_2 * q.y;

  // now z
  q_2 = q.z * q.z;

  q123 *= q_2 * q2r;
  q44 += q_2 * q_2;

  q3.z = q_2 * q.z;

  //}
  q44 *= q2r * q2r;  // (q_1^4+q_2^4+q_3^4)/(|q|^4)

  real_t ac = real_t(1.0) + epsilon1 * (q44 - real_t(0.6)) +
              epsilon2 * (real_t(3.0) * q44 + real_t(66.0) * q123 - real_t(17.0) / real_t(7.0)) +
              epsilon3 * (real_t(5.0) * q44 * q44 - real_t(16.0) * q123 - (real_t(94.0) * q44 - real_t(33.0)) / real_t(13.0));

  //for (unsigned long k = 0; k < 3; k++) { unrolled by hand
  res.x = real_t(2.0) * ac * q.x;

  if (q123 != real_t(0.0)) {
    real_t q123q2 = q123 * q2 / q.x;
    res.x += epsilon1 * (real_t(4.0) * q3.x * q2r - q44 * q.x) +
             epsilon2 * real_t(12.0) * (q3.x * q2r + real_t(11.0) * q123q2 - (q44 + real_t(33.0) * q123) * q.x) +
             epsilon3 * (real_t(40.0) * q44 * (q3.x * q2r - q44 * q.x) - real_t(32.0) * (q123q2 - real_t(3.0) * q123 * q.x + real_t(11.75) / real_t(13.0) * (q3.x * q2r - q44 * q.x)));
  }
  //now y
  res.y = real_t(2.0) * ac * q.y;

  if (q123 != real_t(0.0)) {
    real_t q123q2 = q123 * q2 / q.y;
    res.y += epsilon1 * (real_t(4.0) * q3.y * q2r - q44 * q.y) +
             epsilon2 * real_t(12.0) * (q3.y * q2r + real_t(11.0) * q123q2 - (q44 + real_t(33.0) * q123) * q.y) +
             epsilon3 * (real_t(40.0) * q44 * (q3.y * q2r - q44 * q.y) - real_t(32.0) * (q123q2 - real_t(3.0) * q123 * q.y + real_t(11.75) / real_t(13.0) * (q3.y * q2r - q44 * q.y)));
  }
  // now z
  res.z = real_t(2.0) * ac * q.z;

  if (q123 != real_t(0.0)) {
    real_t q123q2 = q123 * q2 / q.z;
    res.z += epsilon1 * (real_t(4.0) * q3.z * q2r - q44 * q.z) +
             epsilon2 * real_t(12.0) * (q3.z * q2r + real_t(11.0) * q123q2 - (q44 + real_t(33.0) * q123) * q.z) +
             epsilon3 * (real_t(40.0) * q44 * (q3.z * q2r - q44 * q.z) - real_t(32.0) * (q123q2 - real_t(3.0) * q123 * q.z + real_t(11.75) / real_t(13.0) * (q3.z * q2r - q44 * q.z)));
  }
  //}

  res.x *= ac;
  res.y *= ac;
  res.z *= ac;

  return res;
}

__inline__ __device__ void calcGradients(real_t* phiD, real_t* regG, real_t dxr) {
  real3_t grad_phi_xm = make_real3((phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::L)]) * dxr,
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::DL)], phiD[toUnderlying(Direction::UL)], dxr)),
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BL)], phiD[toUnderlying(Direction::FL)], dxr)));

  real3_t grad_phi_xp = make_real3((phiD[toUnderlying(Direction::R)] - phiD[toUnderlying(Direction::C)]) * dxr,
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::DR)], phiD[toUnderlying(Direction::UR)], dxr)),
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BR)], phiD[toUnderlying(Direction::FR)], dxr)));

  real3_t grad_phi_ym = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::DL)], phiD[toUnderlying(Direction::DR)], dxr)),
                                   (phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::D)]) * dxr,
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BD)], phiD[toUnderlying(Direction::FD)], dxr)));

  real3_t grad_phi_yp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::UL)], phiD[toUnderlying(Direction::UR)], dxr)),
                                   (phiD[toUnderlying(Direction::U)] - phiD[toUnderlying(Direction::C)]) * dxr,
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BU)], phiD[toUnderlying(Direction::FU)], dxr)));

  real3_t grad_phi_zm = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::BL)], phiD[toUnderlying(Direction::BR)], dxr)),
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::BD)], phiD[toUnderlying(Direction::BU)], dxr)),
                                   (phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::B)]) * dxr);

  real3_t grad_phi_zp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::FL)], phiD[toUnderlying(Direction::FR)], dxr)),
                                   real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::FD)], phiD[toUnderlying(Direction::FU)], dxr)),
                                   (phiD[toUnderlying(Direction::F)] - phiD[toUnderlying(Direction::C)]) * dxr);

  regG[toUnderlying(Direction::R)] = dAdq(grad_phi_xp).x;
  regG[toUnderlying(Direction::U)] = dAdq(grad_phi_yp).y;
  regG[toUnderlying(Direction::F)] = dAdq(grad_phi_zp).z;
  regG[toUnderlying(Direction::L)] = dAdq(grad_phi_xm).x;
  regG[toUnderlying(Direction::D)] = dAdq(grad_phi_ym).y;
  regG[toUnderlying(Direction::B)] = dAdq(grad_phi_zm).z;
}

__inline__ __device__ void calcGradientsShared(real_t* phiD, real_t* regG, real_t dxr) {
  __shared__ real_t gradSh[3][cudaBlockSizeZ + 1][cudaBlockSizeY + 1][cudaBlockSizeX + 1];

  // grad_phi_xp
  real3_t gradtmp = make_real3((phiD[toUnderlying(Direction::R)] - phiD[toUnderlying(Direction::C)]) * dxr,
                               real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::DR)], phiD[toUnderlying(Direction::UR)], dxr)),
                               real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BR)], phiD[toUnderlying(Direction::FR)], dxr)));

  gradSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = dAdq(gradtmp).x;

  if (threadIdx.y == 0) {
    //grad_phi_ym
    gradtmp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::DL)], phiD[toUnderlying(Direction::DR)], dxr)),
                         (phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::D)]) * dxr,
                         real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BD)], phiD[toUnderlying(Direction::FD)], dxr)));

    gradSh[1][threadIdx.z + 1][threadIdx.y][threadIdx.x + 1] = dAdq(gradtmp).y;
  }

  //grad_phi_yp
  gradtmp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::UL)], phiD[toUnderlying(Direction::UR)], dxr)),
                       (phiD[toUnderlying(Direction::U)] - phiD[toUnderlying(Direction::C)]) * dxr,
                       real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BU)], phiD[toUnderlying(Direction::FU)], dxr)));

  gradSh[1][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = dAdq(gradtmp).y;

  if (threadIdx.z == 0) {
    //grad_phi_zm
    gradtmp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::BL)], phiD[toUnderlying(Direction::BR)], dxr)),
                         real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::BD)], phiD[toUnderlying(Direction::BU)], dxr)),
                         (phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::B)]) * dxr);

    gradSh[2][threadIdx.z][threadIdx.y + 1][threadIdx.x + 1] = dAdq(gradtmp).z;
  }

  //grad_phi_zp
  gradtmp = make_real3(real_t(0.5) * (diffc(phiD[toUnderlying(Direction::L)], phiD[toUnderlying(Direction::R)], dxr) + diffc(phiD[toUnderlying(Direction::FL)], phiD[toUnderlying(Direction::FR)], dxr)),
                       real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::FD)], phiD[toUnderlying(Direction::FU)], dxr)),
                       (phiD[toUnderlying(Direction::F)] - phiD[toUnderlying(Direction::C)]) * dxr);

  gradSh[2][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = dAdq(gradtmp).z;

  regG[toUnderlying(Direction::R)] = gradSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  regG[toUnderlying(Direction::U)] = gradSh[1][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  regG[toUnderlying(Direction::F)] = gradSh[2][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  __syncthreads();
  regG[toUnderlying(Direction::L)] = gradSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x];
  regG[toUnderlying(Direction::D)] = gradSh[1][threadIdx.z + 1][threadIdx.y][threadIdx.x + 1];
  regG[toUnderlying(Direction::B)] = gradSh[2][threadIdx.z][threadIdx.y + 1][threadIdx.x + 1];

  if (threadIdx.x == 0) {
    real3_t grad_phi_xm = make_real3((phiD[toUnderlying(Direction::C)] - phiD[toUnderlying(Direction::L)]) * dxr,
                                     real_t(0.5) * (diffc(phiD[toUnderlying(Direction::D)], phiD[toUnderlying(Direction::U)], dxr) + diffc(phiD[toUnderlying(Direction::DL)], phiD[toUnderlying(Direction::UL)], dxr)),
                                     real_t(0.5) * (diffc(phiD[toUnderlying(Direction::B)], phiD[toUnderlying(Direction::F)], dxr) + diffc(phiD[toUnderlying(Direction::BL)], phiD[toUnderlying(Direction::FL)], dxr)));

    regG[toUnderlying(Direction::L)] = dAdq(grad_phi_xm).x;
  }
}

template <Direction dir>
__global__ void pftempKernel(CudaArguments args, CudaSimData cSimData) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x + args.startX;  //boundarySizeX;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y + args.startY;  //boundarySizeY;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z + args.startZ;  //boundarySizeZ;

  // if (x < args.sizeX - args.boundarySizeX && y < args.sizeY - args.boundarySizeY && z < args.sizeZ - args.boundarySizeZ) {
  if (checkLimits<dir>(x, y, z, args)) {
    real_t phiCells[19];
    real_t tempOldC;
    loadStencil<19, dir>(phiCells, phaseOldGpuPtr, x, y, z, args);
    loadStencil<1, dir>(&tempOldC, tempNewGpuPtr, x, y, z, args);

    real_t gradPhi[7];
    calcGradients(phiCells, gradPhi, cSimData.deltax_R);
    //calcGradientsShared(phiCells, gradPhi, cSimData.deltax_R);

    real_t L = calc(La, 5, tempOldC);

    real_t T    = tempOldC;
    real_t phiC = phiCells[toUnderlying(Direction::C)];
    real_t divgradphi;
    real_t tau_R;

    if (anisotropic) {  // set true
      // anisotrope kinetic
      real3_t grad_center = make_real3(diffc(phiCells[toUnderlying(Direction::L)], phiCells[toUnderlying(Direction::R)], cSimData.deltax_R),
                                       diffc(phiCells[toUnderlying(Direction::D)], phiCells[toUnderlying(Direction::U)], cSimData.deltax_R),
                                       diffc(phiCells[toUnderlying(Direction::B)], phiCells[toUnderlying(Direction::F)], cSimData.deltax_R));

      if (isNearlyEqual(grad_center.x, real_t(0)) && isNearlyEqual(grad_center.y, real_t(0)) && isNearlyEqual(grad_center.z, real_t(0))) {
        // no gradient everything is zero
        storeStencil<dir>(phaseNewGpuPtr, x, y, z, args, phiC);
        return;
      }

      if (betazero) {  // set true
        // arbitrarily relaxation parameter beta=0
        real_t gamma = A(grad_center);
        tau_R        = Tm * T * interpolate_byPhi(k, phiC) / (L * L * cSimData.epsilon * MplusF * gamma * gamma);

      } else {
        // anisotropic kinetic
        grad_center = normalize(grad_center);

        real_t n44, n66, n123;
        n123 = grad_center.x * grad_center.x * grad_center.y * grad_center.y * grad_center.z * grad_center.z;
        n44  = real_pow(grad_center.x, real_t(4.0)) + real_pow(grad_center.y, real_t(4.0)) + real_pow(grad_center.z, real_t(4.0));
        n66  = real_pow(grad_center.x, real_t(6.0)) + real_pow(grad_center.y, real_t(6.0)) + real_pow(grad_center.z, real_t(6.0));

        real_t k = k0 * (real_t(1.0) + epsilon_k * (real_t(-3.0) + real_t(4.0) * n44) + delta_k * (n66 + real_t(30.0) * n123));

        tau_R = k * Tm * T / L;
      }

      // a anisotrop
      divgradphi = diffl(gradPhi[toUnderlying(Direction::R)], gradPhi[toUnderlying(Direction::L)], cSimData.deltax_R) +
                   diffl(gradPhi[toUnderlying(Direction::U)], gradPhi[toUnderlying(Direction::D)], cSimData.deltax_R) +
                   diffl(gradPhi[toUnderlying(Direction::F)], gradPhi[toUnderlying(Direction::B)], cSimData.deltax_R);
    } else {
      // a isotrop
      divgradphi = diffl(gradPhi[toUnderlying(Direction::R)], gradPhi[toUnderlying(Direction::L)], cSimData.deltax_R) +
                   diffl(gradPhi[toUnderlying(Direction::U)], gradPhi[toUnderlying(Direction::D)], cSimData.deltax_R) +
                   diffl(gradPhi[toUnderlying(Direction::F)], gradPhi[toUnderlying(Direction::B)], cSimData.deltax_R);

      // isotrope kinetic
      tau_R = k0 * Tm * T / L;
    }

    divgradphi *= gab;

    // w
    real_t dwdphi = gab * (real_t(1.0) - real_t(2.0) * phiC);
    dwdphi *= real_t(32.0) / (real_t(M_PI) * real_t(M_PI) * cSimData.epsilon * cSimData.epsilon);

    // f
    real_t dfdphi = dfLdphi(phiC, T, La) + dfTdphi(phiC, T, CVa, CVb);
    // dfdphi += h(phi) * CVa.calc(T);
    dfdphi /= T * cSimData.epsilon;

    real_t dphi = divgradphi - dwdphi - dfdphi;

    dphi *= tau_R;
    dphi *= cSimData.deltat;

    phiC += dphi;
    phiC = real_max(real_min(phiC, real_t(1.0)), real_t(0.0));  // Simplex

    storeStencil<dir>(phaseNewGpuPtr, x, y, z, args, phiC);
  }
}

void copyPhaseConst() {
  real_t kl[2];
  real_t CVal[4];
  real_t CVbl[4];
  real_t Lal[5];

  kl[0] = real_t(2103.320339) / real_t(100.0);
  kl[1] = real_t(1708.947775) / real_t(100.0);

  CVal[0] = real_t(0.9816705873);
  CVal[1] = real_t(0.04412186595);
  CVal[2] = real_t(-0.02403402118);
  CVal[3] = real_t(-0.001758431968);

  CVbl[0] = real_t(1.331101576);
  CVbl[1] = real_t(-0.4598970843);
  CVbl[2] = real_t(0.05225148201);
  CVbl[3] = real_t(-0.003237275948);

  Lal[0] = real_t(-0.02161349859);
  Lal[1] = real_t(0.7705647776);
  Lal[2] = real_t(-0.4349444313);
  Lal[3] = real_t(0.05210816941);
  Lal[4] = real_t(-0.003776436992);

  cudaMemcpyToSymbol(k, &kl, 2 * sizeof(real_t));
  cudaMemcpyToSymbol(CVa, &CVal, 4 * sizeof(real_t));
  cudaMemcpyToSymbol(CVb, &CVbl, 4 * sizeof(real_t));
  cudaMemcpyToSymbol(La, &Lal, 5 * sizeof(real_t));
  phaseConstCopied = 1;
}

void sweep_pftemp_kernel(cudaStream_t stream, cudaStream_t haloStream, CudaGpuFieldPtrs<real_t> h_oldGpuPtr1, CudaGpuFieldPtrs<real_t> h_newGpuPtr1, CudaGpuFieldPtrs<real_t> /*h_oldGpuPtr2*/, CudaGpuFieldPtrs<real_t> h_newGpuPtr2, const CudaArguments args, CudaSimData cSimData) {
  if (phaseConstCopied == 0) copyPhaseConst();

  // Using MemcpyToSymbol instead of kernel parameter allows for an easy description of an abstract kernel type.
  // This is used in the following line to iterate through all 27 kernels
  gpuErrchk(cudaMemcpyToSymbol(phaseOldGpuPtr, &h_oldGpuPtr1, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(phaseNewGpuPtr, &h_newGpuPtr1, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(tempNewGpuPtr, &h_newGpuPtr2, sizeof(CudaGpuFieldPtrs<real_t>)));

  // This is performing something like a loop unrolling at compile time.
  // Therefore all templated instances of the heatKernel can be created at compiletime
  // This is not working with a usual for loop over all the directions, template packs have to be used.
  staticFor<numberOfDirections>([&](auto i) {
    launchKernel<static_cast<Direction>(i())>(pftempKernel<static_cast<Direction>(i())>, stream, haloStream, args, cSimData);
  });
}

}  // namespace cuda
}  // namespace nastja
