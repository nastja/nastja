/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/sweep.h"

namespace nastja {

/**
 * @defgroup pfm Phase-field method
 * A computational model for continuum simulation of microstructure evolution.
 */

/**
 * Class for sweep a simple phase-field method.
 *
 * @ingroup sweeps pfm
 */
class SweepPhaseField : public Sweep {
public:
  explicit SweepPhaseField() {
    registerFieldname("phasefield");
  }

  ~SweepPhaseField() override                 = default;
  SweepPhaseField(const SweepPhaseField&)     = delete;
  SweepPhaseField(SweepPhaseField&&) noexcept = default;
  SweepPhaseField& operator=(const SweepPhaseField&) = delete;
  SweepPhaseField& operator=(SweepPhaseField&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
};

}  // namespace nastja
