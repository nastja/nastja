/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_heat.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;

void SweepHeat::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName("temperature")->get<real_t>();

  const auto& boundarySize = field.getBoundarySize();

  const real_t factor = getSimData().deltat / (getSimData().deltax * getSimData().deltax);

  for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
      for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
        auto index     = field.getIndex(x, y, z);
        real_t cell    = field.getCell<1>(index);
        real_t cell_xm = field.getCell<1>(index, Direction::L);
        real_t cell_xp = field.getCell<1>(index, Direction::R);
        real_t cell_ym = field.getCell<1>(index, Direction::D);
        real_t cell_yp = field.getCell<1>(index, Direction::U);
        real_t cell_zm = field.getCell<1>(index, Direction::B);
        real_t cell_zp = field.getCell<1>(index, Direction::F);
        cell += factor * (-6.0 * cell + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm);
        field.getCell(x, y, z, 0) = cell;
      }
    }
  }
}

void SweepHeat::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
