/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_temp.h"
#include "lib/field/field.h"
#include "lib/math/pfmath.h"
#include "lib/simdata/datapfm.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace math;
using namespace stencil;

real_t SweepTemperature::diffu(const real_t center, const real_t upper) const {
  return (upper - center) * getSimData().deltax_R;
}

real_t SweepTemperature::diffl(const real_t center, const real_t lower) const {
  return (center - lower) * getSimData().deltax_R;
}

void SweepTemperature::executeBlock(block::Block* block) {
  auto& temperature = block->getFieldByName("temperature")->get<real_t>();
  auto& phasefield  = block->getFieldByName("phasefield")->get<real_t>();

  const auto& data = getSimData().getData<DataPFM>();

  auto boundarysize = temperature.getBoundarySize();

  for (unsigned int z = boundarysize[2]; z < temperature.getSize(2) - boundarysize[2]; z++) {
    for (unsigned int y = boundarysize[1]; y < temperature.getSize(1) - boundarysize[1]; y++) {
      for (unsigned int x = boundarysize[0]; x < temperature.getSize(0) - boundarysize[0]; x++) {
        auto index  = temperature.getIndex(x, y, z);
        real_t T    = temperature.getCell<1>(index);
        real_t T_xm = temperature.getCell<1>(index, Direction::L);
        real_t T_xp = temperature.getCell<1>(index, Direction::R);
        real_t T_ym = temperature.getCell<1>(index, Direction::D);
        real_t T_yp = temperature.getCell<1>(index, Direction::U);
        real_t T_zm = temperature.getCell<1>(index, Direction::B);
        real_t T_zp = temperature.getCell<1>(index, Direction::F);

        real_t phi    = phasefield.getCell<1>(index);
        real_t phi_xm = phasefield.getCell<1>(index, Direction::L);
        real_t phi_xp = phasefield.getCell<1>(index, Direction::R);
        real_t phi_ym = phasefield.getCell<1>(index, Direction::D);
        real_t phi_yp = phasefield.getCell<1>(index, Direction::U);
        real_t phi_zm = phasefield.getCell<1>(index, Direction::B);
        real_t phi_zp = phasefield.getCell<1>(index, Direction::F);

        real_t gradT_xp = diffu(T, T_xp);
        real_t gradT_yp = diffu(T, T_yp);
        real_t gradT_zp = diffu(T, T_zp);
        real_t gradT_xm = diffu(T_xm, T);
        real_t gradT_ym = diffu(T_ym, T);
        real_t gradT_zm = diffu(T_zm, T);

        real_t energyflux_xp = interpolate_byPhi(data.k, interpolate(phi, phi_xp)) * gradT_xp;
        real_t energyflux_yp = interpolate_byPhi(data.k, interpolate(phi, phi_yp)) * gradT_yp;
        real_t energyflux_zp = interpolate_byPhi(data.k, interpolate(phi, phi_zp)) * gradT_zp;
        real_t energyflux_xm = interpolate_byPhi(data.k, interpolate(phi, phi_xm)) * gradT_xm;
        real_t energyflux_ym = interpolate_byPhi(data.k, interpolate(phi, phi_ym)) * gradT_ym;
        real_t energyflux_zm = interpolate_byPhi(data.k, interpolate(phi, phi_zm)) * gradT_zm;

        real_t divgrad = diffl(energyflux_xp, energyflux_xm) + diffl(energyflux_yp, energyflux_ym) + diffl(energyflux_zp, energyflux_zm);  ///< nabla . k nabla T

        if (only_diffusion) {
          T += getSimData().deltat * divgrad;
        } else {
          real_t Temp   = (T - data.Tm) / data.Tm;
          real_t cva    = data.CVa.calc(T);
          real_t cvb    = data.CVb.calc(T);
          real_t dLadT  = data.La.calcDerivation(1, T);
          real_t d2LadT = data.La.calcDerivation(2, T);
          real_t L      = data.La.calc(T);
          real_t intcva = data.CVa.calcIntegral(T) - data.CVa.calcIntegral(data.Tm);
          real_t intcvb = data.CVb.calcIntegral(T) - data.CVb.calcIntegral(data.Tm);

          real_t deltaphia = phasefield.getCell(x, y, z) - phi;

          real_t sumnum   = (-dLadT * T * Temp - L + intcva - intcvb) * dhdphi(phi) * deltaphia;
          real_t sumdenum = -(d2LadT * T * Temp + 2.0 * dLadT * T / data.Tm - cva) * h(phi) + cvb * h(1.0 - phi);

          T += (getSimData().deltat * divgrad - sumnum) / (sumdenum);
        }

        temperature.getCell(index) = T;
      }
    }
  }
}

void SweepTemperature::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
