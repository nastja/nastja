/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_heat_iter.h"
#include "lib/field/field.h"

namespace nastja {

using namespace stencil;

void SweepHeatIter::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName("temperature")->get<real_t>();

  const real_t factor = getSimData().deltat / (getSimData().deltax * getSimData().deltax);

  const unsigned int v = 1;  // vector length

  auto view = field.createView<2>();
  for (const auto& cell : view) {
    // Do unrolling by to so that SSE optimization can be done automatically by the compiler.
    real_t cell_c   = view.get<1>(cell, Direction::C);
    real_t cell_c2  = view.get<1>(cell + v, Direction::C);
    real_t cell_xm  = view.get<1>(cell, Direction::L);
    real_t cell_xm2 = view.get<1>(cell + v, Direction::L);
    real_t cell_xp  = view.get<1>(cell, Direction::R);
    real_t cell_xp2 = view.get<1>(cell + v, Direction::R);
    real_t cell_ym  = view.get<1>(cell, Direction::D);
    real_t cell_ym2 = view.get<1>(cell + v, Direction::D);
    real_t cell_yp  = view.get<1>(cell, Direction::U);
    real_t cell_yp2 = view.get<1>(cell + v, Direction::U);
    real_t cell_zm  = view.get<1>(cell, Direction::B);
    real_t cell_zm2 = view.get<1>(cell + v, Direction::B);
    real_t cell_zp  = view.get<1>(cell, Direction::F);
    real_t cell_zp2 = view.get<1>(cell + v, Direction::F);
    cell_c += factor * (-6.0 * cell_c + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm);
    cell_c2 += factor * (-6.0 * cell_c2 + cell_xp2 + cell_xm2 + cell_yp2 + cell_ym2 + cell_zp2 + cell_zm2);
    view.get(cell)     = cell_c;
    view.get(cell + v) = cell_c2;
  }
}

void SweepHeatIter::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
