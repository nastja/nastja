/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/math/polynomial.h"

namespace nastja {

constexpr bool only_diffusion = false;

class SweepTemperatureCuda : public Sweep {
public:
  explicit SweepTemperatureCuda() {
    registerFieldname("temperature");
    registerFieldname("phasefield");
  }

  ~SweepTemperatureCuda() override                  = default;
  SweepTemperatureCuda(const SweepTemperatureCuda&) = delete;
  SweepTemperatureCuda(SweepTemperatureCuda&&)      = default;
  SweepTemperatureCuda& operator=(const SweepTemperatureCuda&) = delete;
  SweepTemperatureCuda& operator=(SweepTemperatureCuda&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;

private:
  //real_t diffu(real_t center, real_t upper) const;
  //real_t diffl(real_t center, real_t lower) const;

  const std::array<real_t, 2> k{2103.320339 / 100.0, 1708.947775 / 100.0};  ///< The array of the thermal conductivity of the liquid and solid phase.

  const real_t Tm{1.0};  ///< The melting temperature.

  math::Polynomial CVa{0.9816705873, 0.04412186595, -0.02403402118, -0.001758431968};                ///< The polynomial of the volumetric heat capacity of the liquid phase.
  math::Polynomial CVb{1.331101576, -0.4598970843, 0.05225148201, -0.003237275948};                  ///< The polynomial of the volumetric heat capacity of the solid pahse.
  math::Polynomial La{-0.02161349859, 0.7705647776, -0.4349444313, 0.05210816941, -0.003776436992};  ///< The polynomial of the latent heat.
};

}  // namespace nastja
