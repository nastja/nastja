/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/sweep.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/functions/energy/energyfactory.h"
#include "lib/functions/functionregister.h"
#include "lib/functions/visitor.h"
#include "lib/functions/visitorfactory.h"
#include "lib/math/random.h"
#include "lib/simdata/datacells.h"
#include "lib/storage/paramstorage.h"

namespace nastja {

/**
 * @defgroup cells CellsInSilico module
 * A computational model of the collective behavior of cellular structures.
 * Grid based model
 */

/**
 * Class for sweep cells.
 * Performs one sweep over the field and propagates the state by one Monte Carlo sweep (MCS).
 * Field is accessed via a visitor pattern then local energy changes of a proposed move are determined.
 * Changes are accepted or rejected by the Metropolis criterion.
 *
 * @tparam DIM  The dimension.
 * @ingroup sweeps cells
 */
template <unsigned int DIM = 3>
class SweepCells : public Sweep {
public:
  explicit SweepCells() {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
    registerFieldname("cells");
  }

  ~SweepCells() override            = default;
  SweepCells(const SweepCells&)     = delete;
  SweepCells(SweepCells&&) noexcept = default;
  SweepCells& operator=(const SweepCells&) = delete;
  SweepCells& operator=(SweepCells&&) = delete;

  void init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) override;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void setup(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;

  void dump(Archive& ar) const override;
  void undump(Archive& ar) override;

  /**
   * Gets one position of the Field as input and sets the field to the new value of that point after one MonteCarlo Step
   *
   * @param view       The view.
   * @param it         The iterator.
   * @param dir        The dir.
   * @param container  The container.
   * @param data       The data.
   */
  template <typename FieldView, typename FieldIterator>
  void monteCarlo(FieldView& view, FieldIterator& it, stencil::Direction dir, CellBlockContainer& container, DataCells& data) {
    auto& generator = getSimData().generator;

    cellid_t centerCellID   = view.get(*it);
    cellid_t neighborCellID = view.get(*it, dir);

    if (neighborCellID == centerCellID || neighborCellID < data.liquid) return;

    auto& centerCell   = container.cells(centerCellID);
    auto& neighborCell = container.cells(neighborCellID);

    EnergyFunctionPack pack{centerCellID, neighborCellID, centerCell, neighborCell, container, data};

    real_t energy = energyFunctions_.calc(view, it, dir, pack);

    // Metropolis criteria (temperature)
    if ((energy > 0.0 && exp(-energy / temperature_) <= probability_(generator))) return;

    // set voxel to neighbor; use changes
    if (centerCellID > data.liquid) {
      centerCell.deltaSurface += pack.changeCenterSurface;
      centerCell.deltaVolume += pack.changeCenterVolume;
    }
    if (neighborCellID > data.liquid) {
      neighborCell.deltaSurface += pack.changeNeighborSurface;
      neighborCell.deltaVolume += pack.changeNeighborVolume;
    }

    view.get(*it) = neighborCellID;
    centerCell.voxelsInBlock--;
    neighborCell.voxelsInBlock++;
  }

private:
  math::UniformReal<float> probability_{0, 1};  ///< The uniform distribution of the probability.

  real_t temperature_{};  ///< Metropolis temperature

  FunctionRegister<IEnergy<DIM>> energyFunctions_;  ///< The register of the engery functions.

  std::unique_ptr<IVisitor<DIM>> visitor_;  ///< The pointer to the visitor.
};

}  // namespace nastja
