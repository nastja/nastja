/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_heat_avx.h"
#include "lib/field/field.h"
#include "lib/simd/simd.h"
#include "lib/simdata/simdata.h"

namespace nastja {

using namespace simd;
using namespace stencil;

template <typename T, unsigned int TSplitX>
void calculate(field::Field<T, TSplitX>& field, SimData& simData) {
  const real_t factor = simData.deltat / (simData.deltax * simData.deltax);

  const preal_t factorp  = set(factor);
  const preal_t factorm6 = set(real_t(-6.0));

  auto view = field.template createView<field::SimdStepSize>();
  for (const auto& cell : view) {
    __asm volatile("# LLVM-MCA-BEGIN heat");
    preal_t cell_c  = view.template load<1>(cell, Direction::C);
    preal_t cell_xm = view.template load_unaligned<1>(cell, Direction::L);
    preal_t cell_xp = view.template load_unaligned<1>(cell, Direction::R);
    preal_t cell_ym = view.template load<1>(cell, Direction::D);
    preal_t cell_yp = view.template load<1>(cell, Direction::U);
    preal_t cell_zm = view.template load<1>(cell, Direction::B);
    preal_t cell_zp = view.template load<1>(cell, Direction::F);

    cell_c = cell_c + factorp * (factorm6 * cell_c + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm);
    view.store(cell, cell_c);
  }
  __asm volatile("# LLVM-MCA-END heat");
}

void SweepHeatAVX::executeBlock(block::Block* block) {
  if (auto* field = block->getFieldByName("temperature")->getPtr<real_t>()) {
    calculate(*field, getSimData());
  } else if (auto* field = block->getFieldByName("temperature")->getPtr<real_t, true>()) {
    calculate(*field, getSimData());
  }
}

void SweepHeatAVX::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
