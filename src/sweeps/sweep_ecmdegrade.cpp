/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_ecmdegrade.h"
#include "lib/field/field.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/logger.h"
#include "lib/storage/paramstorage.h"
namespace nastja {

using namespace config::literals;
using namespace stencil;


void EcmDegradation::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  /// @key{CellsInSilico.ecmdegradation.steps, uint, 1}
  /// Number of each n-th steps when the ECM is degraded.
  eachNthStep = getSimData().getConfig().getValue<unsigned int>("CellsInSilico.ecmdegradation.steps"_jptr, 1);

  /// @key{CellsInSilico.ecmdegradation.probability, ParameterStorage<real_t>}
  /// The probability matrix of a solid voxel being degraded during the sweep when it is next to a cell voxel. This needs to be an NxN matrix with N=max_celltype. degradation probabilities are written into the columns
  degradationProbability_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.ecmdegradation.probability"_jptr, getSimData().getConfig());
}

/**
 * Turn solid voxels next to cells into liquid voxels in the whole block
 *
 * @param block Pointer to the block.
 */
void EcmDegradation::executeBlock(block::Block* block) {

  logger::get("cellevents").info(R"("block": {}, "event": "ecm degradation")", block->id);
  auto& field = block->getFieldByName("cells")->get<cellid_t>();
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& degProbability = degradationProbability_->getStorage();

  auto& data = getSimData().getData<DataCells>();
  auto view  = field.createView(field.getInnerBoundingBox());
  for (auto* voxel : view) {
    auto& currentCell = view.get(voxel);

    if (currentCell >= data.liquid) continue;

    for (unsigned int i = 0; i < 27; i++) {
      auto neighborID = view.get(voxel, stencil::d27map[i]);

      if (neighborID <= data.liquid) continue;

      auto neighborType{container.cells(neighborID).type};

      if (degProbability.getValue(currentCell, neighborType) <= 0. ) continue;

      if(probability_(getSimData().generator) < degProbability.getValue(currentCell,neighborType)){
        currentCell = data.liquid;      
        break;
      }
    }
  }
}

void EcmDegradation::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
