/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/math/polynomial.h"

namespace nastja {

constexpr bool anisotropic = true;
constexpr bool betazero    = true;

class SweepPhaseFieldTemperatureCuda : public Sweep {
public:
  explicit SweepPhaseFieldTemperatureCuda() {
    registerFieldname("phasefield");
    registerFieldname("temperature");
  }

  ~SweepPhaseFieldTemperatureCuda() override                            = default;
  SweepPhaseFieldTemperatureCuda(const SweepPhaseFieldTemperatureCuda&) = delete;
  SweepPhaseFieldTemperatureCuda(SweepPhaseFieldTemperatureCuda&&)      = default;
  SweepPhaseFieldTemperatureCuda& operator=(const SweepPhaseFieldTemperatureCuda&) = delete;
  SweepPhaseFieldTemperatureCuda& operator=(SweepPhaseFieldTemperatureCuda&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;
};

}  // namespace nastja
