/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_pftemp_cuda.h"
#include "lib/field/field.h"
#include "lib/math/pfmath.h"
#include "lib/math/vector.h"
#include "lib/stencil/direction.h"
#include "sweep_pftemp_kernel.h"

namespace nastja {

using namespace stencil;
using namespace config::literals;

void SweepPhaseFieldTemperatureCuda::setup(const block::BlockRegister& blockRegister) {
  auto& config = getSimData().getConfig();

  /// @keyignore
  auto epsilon = config.getValue<real_t>("PhaseField.epsilon"_jptr, 1.0);

  for (const auto& block : blockRegister) {
    auto& phasefield  = block.second->getFieldByName("phasefield")->get<real_t>();
    auto& cuSimData   = phasefield.getCuSimData();
    cuSimData.epsilon = epsilon;
  }
}

void SweepPhaseFieldTemperatureCuda::executeBlock(block::Block* block) {
  auto& phasefield  = block->getFieldByName("phasefield")->get<real_t>();
  auto& temperature = block->getFieldByName("temperature")->get<real_t>();

  cuda::sweep_pftemp_kernel(phasefield.getCuMainStream(), phasefield.getCuHaloStream(),
                            phasefield.getGpuPtrs(1), phasefield.getGpuPtrs(0),
                            temperature.getGpuPtrs(1), temperature.getGpuPtrs(0),
                            phasefield.getCuArgs(), phasefield.getCuSimData());
}

void SweepPhaseFieldTemperatureCuda::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
