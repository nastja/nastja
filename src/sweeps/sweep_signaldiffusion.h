/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/simdata/datacells.h"
#include <random>

namespace nastja {

/**
 * Class for sweep cells.
 *
 * @tparam DIM  The dimension.
 * @ingroup sweeps cells
 */
template <unsigned int DIM = 3>
class SweepSignalDiffusion : public Sweep {
public:
  explicit SweepSignalDiffusion() {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
    registerFieldname("cells");
  }

  ~SweepSignalDiffusion() override                      = default;
  SweepSignalDiffusion(const SweepSignalDiffusion&)     = delete;
  SweepSignalDiffusion(SweepSignalDiffusion&&) noexcept = default;
  SweepSignalDiffusion& operator=(const SweepSignalDiffusion&) = delete;
  SweepSignalDiffusion& operator=(SweepSignalDiffusion&&) = delete;

  void init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) override;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void setup(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;

private:
  void calcDiffusion(block::Block* block) const;
  void initializeNeighborhood(block::Block* block) const;

  int apoptoticCellType_{};  ///< The apoptotic cell type.
};

}  // namespace nastja
