/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"

namespace nastja {

/**
 * Class for sweep sleep. Doing nothing, just sleep.
 *
 * @ingroup sweeps
 */
class SweepSleep : public Sweep {
public:
  explicit SweepSleep() {
    registerFieldname("phasefield");
  }

  ~SweepSleep() override            = default;
  SweepSleep(const SweepSleep&)     = delete;
  SweepSleep(SweepSleep&&) noexcept = default;
  SweepSleep& operator=(const SweepSleep&) = delete;
  SweepSleep& operator=(SweepSleep&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
};

}  // namespace nastja
