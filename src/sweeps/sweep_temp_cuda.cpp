/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_temp_cuda.h"
#include "lib/field/field.h"
#include "lib/math/pfmath.h"
#include "lib/stencil/direction.h"
#include "sweep_temp_kernel.h"

namespace nastja {

using namespace stencil;
using namespace config::literals;

void SweepTemperatureCuda::setup(const block::BlockRegister& blockRegister) {
  auto& config = getSimData().getConfig();

  /// @keyignore
  auto epsilon = config.getValue<real_t>("PhaseField.epsilon"_jptr, 1.0);

  for (const auto& block : blockRegister) {
    auto& temperature = block.second->getFieldByName("temperature")->get<real_t>();
    auto& cuSimData   = temperature.getCuSimData();
    cuSimData.epsilon = epsilon;
  }
}

void SweepTemperatureCuda::executeBlock(block::Block* block) {
  auto& temperature = block->getFieldByName("temperature")->get<real_t>();
  auto& phasefield  = block->getFieldByName("phasefield")->get<real_t>();

  cuda::sweep_temp_kernel(temperature.getCuMainStream(), temperature.getCuHaloStream(),
                          phasefield.getGpuPtrs(1), phasefield.getGpuPtrs(0),
                          temperature.getGpuPtrs(1), temperature.getGpuPtrs(0),
                          temperature.getCuArgs(), temperature.getCuSimData());
}

void SweepTemperatureCuda::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
