/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include <fftw3.h>

namespace nastja {

/**
 * @defgroup pfc Phase-field crystal
 * A computational model for atomistic simulation.
 */

/**
 * Class for sweep a simple phase-field crystal model.
 *
 * @tparam DIM  The dimension.
 * @ingroup sweeps pfc
 */
template <unsigned int DIM = 3>
class SweepPFC : public Sweep {
public:
  explicit SweepPFC();
  ~SweepPFC() override;
  SweepPFC(const SweepPFC&)     = delete;
  SweepPFC(SweepPFC&&) noexcept = default;
  SweepPFC& operator=(const SweepPFC&) = delete;
  SweepPFC& operator=(SweepPFC&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;

private:
  double getWaveVectorSq(unsigned long index) const;
  bool cutoff(double kappa) const;

  fftw_plan fft_psi;    ///< The plan for the fast fourier transformation of the psi field.
  fftw_plan fft_psinl;  ///< The plan for the fast fourier transformation of the psi non-linear field.
  fftw_plan ifft_psi;   ///< The plan for the inverse fast fourier transformation of the psi field.

  double cutx;  ///< The cutoff frequency in x direction.
  double cuty;  ///< The cutoff frequency in y direction.
  double cutz;  ///< The cutoff frequency in z direction.

  double scalefactor;  ///< The scale factor for the inverse transformation.

  double epsilon;      ///< The pfc undercooling.
  double cutoffValue;  ///< The cutoff value.

  int n0;  ///< The first dimension.
  int n1;  ///< The second dimension.
  int n2;  ///< The third dimension.
  int nc;  ///< The last dimension is only half long in the complex transformed field.
};

}  // namespace nastja
