/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/sweep.h"

namespace nastja {

/**
 * Class for sweep phase-field with volume preserve.
 *
 * @ingroup sweeps pfm
 */
class SweepPfVp : public Sweep {
public:
  explicit SweepPfVp() {
    registerFieldname("phasefield");
  }

  ~SweepPfVp() override           = default;
  SweepPfVp(const SweepPfVp&)     = delete;
  SweepPfVp(SweepPfVp&&) noexcept = default;
  SweepPfVp& operator=(const SweepPfVp&) = delete;
  SweepPfVp& operator=(SweepPfVp&&) = delete;

  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); };
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;

  void dump(Archive& ar) const override;
  void undump(Archive& ar) override;

private:
  real_t initialVolume_{};       ///< The initial volume.
  real_t deltaHiddenVolume_{0};  ///< The change in volume.
  real_t increaseVolume_{0};     ///< The volume is increased by this value.
};

}  // namespace nastja
