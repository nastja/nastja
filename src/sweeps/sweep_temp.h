/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/sweep.h"
#include "lib/math/polynomial.h"

namespace nastja {

constexpr bool only_diffusion = false;

class SweepTemperature : public Sweep {
public:
  explicit SweepTemperature() {
    registerFieldname("temperature");
    registerFieldname("phasefield");
  }

  ~SweepTemperature() override              = default;
  SweepTemperature(const SweepTemperature&) = delete;
  SweepTemperature(SweepTemperature&&)      = default;
  SweepTemperature& operator=(const SweepTemperature&) = delete;
  SweepTemperature& operator=(SweepTemperature&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;

private:
  real_t diffu(real_t center, real_t upper) const;
  real_t diffl(real_t center, real_t lower) const;
};

}  // namespace nastja
