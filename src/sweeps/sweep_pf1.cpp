/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_pf1.h"
#include "lib/logger.h"
#include "lib/timing.h"
#include "lib/action/dynamicblock.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;

void SweepPhaseField::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName("phasefield")->get<real_t>();

  const auto& boundarySize = field.getBoundarySize();
  unsigned long vectorSize = field.getVectorSize();

  // eps=8, gamma=1, mu=1
  real_t const A = 8;                           // eps * gamma / mu
  real_t const B = 32.0 / (8.0 * M_PI * M_PI);  // 32 / eps * pi^2
  real_t const C = 2 * B;                       // 2B
  const real_t F = 0.1;                         // Delta f= f_alpha-f_beta

  real_t blockVolume = 0.0;

  for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
      for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
        for (unsigned long v = 0; v < vectorSize; v++) {
          auto index     = field.getIndex(x, y, z);
          real_t cell    = field.getCell<1>(index, v);
          real_t cell_xm = field.getCell<1>(index, Direction::L, v);
          real_t cell_xp = field.getCell<1>(index, Direction::R, v);
          real_t cell_ym = field.getCell<1>(index, Direction::D, v);
          real_t cell_yp = field.getCell<1>(index, Direction::U, v);
          real_t cell_zm = field.getCell<1>(index, Direction::B, v);
          real_t cell_zp = field.getCell<1>(index, Direction::F, v);
          cell += getSimData().deltat * (A * (-6.0 * cell + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm) / (getSimData().deltax * getSimData().deltax) + C * cell - B +
                  6.0 * F * cell * (1.0 - cell));
          cell = fmax(fmin(cell, 1.0), 0.0);

          field.getCell(index, v) = cell;

          blockVolume += cell;
        }
      }
    }
  }

  if (block->hasDataContainer<DynamicBlockContainer>()) {
    auto& data             = block->getDataContainer<DynamicBlockContainer>();
    real_t fullBlockVolume = field.getVolume();
    blockVolume *= getSimData().deltax * getSimData().deltax * getSimData().deltax;

    if (data.ttl != 0) {
      data.ttl--;
    } else {
      if (blockVolume == 0.0 || blockVolume == fullBlockVolume) {
        // The inner domain of the block is empty or full, test boundary
        real_t boundarySum = field.sumBoundary(0);
        if (blockVolume == 0.0 && boundarySum == 0.0) {
          logger::get().trace(logger::Category::DynCnstBlk, "EMPTY BLOCK {}!", field.getID());
          data.setEmpty();
        } else if (blockVolume == fullBlockVolume && boundarySum == field.calcBoundaryCells()) {
          logger::get().trace(logger::Category::DynCnstBlk, "FULL BLOCK {}!", field.getID());
          data.setFull();
        }
      }
    }
  }
}

void SweepPhaseField::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
