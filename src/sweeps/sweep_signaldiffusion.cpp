/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_signaldiffusion.h"
#include "lib/cell.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/math/calculatesurface.h"
#include "lib/math/calculatevolume.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D2C4.h"
#include "lib/stencil/D3C6.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <memory>

namespace nastja {

using namespace stencil;
using namespace config::literals;

template <unsigned int DIM>
using DxC6 = typename std::conditional<DIM == 3, D3C6, D2C4>::type;

template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  Sweep::init(block, fieldpool);
}

template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
  auto& data   = getSimData().template initData<DataCells>();  ///< Data for CellsInSilico.
  auto& config = getSimData().getConfig();

  /// @key{CellsInSilico.signaling.diffusionsignal0, ParameterStorage<real_t>}
  /// The matrix of diffusion constants for signal 0, dimensions are the cell Types (i.e. diffusionsignal0[2,5] is the diffusion constant from cells of type 2 to type 5).
  data.diffusionSignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.diffusionsignal0"_jptr, config);

  /// @key{CellsInSilico.signaling.diffusionsignal1, ParameterStorage<real_t>}
  /// The matrix of diffusion constants for signal 1, dimensions are the cell Types (i.e. diffusionsignal1[2,5] is the diffusion constant from cells of type 2 to type 5).
  data.diffusionSignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.diffusionsignal1"_jptr, config);

  /// @key{CellsInSilico.signaling.diffusionsignal2, ParameterStorage<real_t>}
  /// The matrix of diffusion constants for signal 2, dimensions are the cell Types (i.e. diffusionsignal2[2,5] is the diffusion constant from cells of type 2 to type 5).
  data.diffusionSignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.diffusionsignal2"_jptr, config);

  /// @key{CellsInSilico.signaling.constantsignal, ParameterStorage<int>}
  /// The vector with constant signals. If constantsignal[cellType] =! 0 then cells of this cellType will have constant signal contents
  data.constantSignal = std::make_unique<ParameterStorageBuilder<int>>("CellsInSilico.signaling.constantsignal"_jptr, config);

  /// @key{CellsInSilico.signaling.startsignal0, ParameterStorage<real_t>}
  /// The vector of the initial value for signal 0 for each cell type.
  data.startSignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.startsignal0"_jptr, config);

  /// @key{CellsInSilico.signaling.startsignal1, ParameterStorage<real_t>}
  /// The vector of the initial value for signal 1 for each cell type.
  data.startSignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.startsignal1"_jptr, config);

  /// @key{CellsInSilico.signaling.startsignal2, ParameterStorage<real_t>}
  /// The vector of the initial value for signal 2 for each cell type.
  data.startSignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.startsignal2"_jptr, config);
  // TODO: change for a variable size of signals.

  /// @keyignore
  eachNthStep = config.template getValue<int>("CellsInSilico.signaling.steps"_jptr, 1);

  /// @keyignore
  /// @todo reuse.
  apoptoticCellType_ = config.template getValue<int>("CellsInSilico.celldeath.apoptoticcelltype"_jptr, -2);
}

/// calculates the flux trough the cell surfaces which is then added to the delta Signals
template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::calcDiffusion(block::Block* block) const {
  auto& data        = getSimData().template getData<DataCells>();
  auto& container   = block->getDataContainer<CellBlockContainer>();
  auto& constantSig = data.constantSignal->getStorage();

  for (auto& cell : container.cells) {
    int currentType = cell.second.type;
    if ((getSimData().timestep - cell.second.birth) < 10) continue;

    for (const auto& neighbor : cell.second.neighbors) {
      cellid_t neighborCellID = neighbor.first;
      if (cell.second.type == apoptoticCellType_) continue;
      if (!container.cells.contains(neighborCellID)) continue;
      if (container.cells(neighborCellID).type == apoptoticCellType_) continue;

      auto neighborSurfaceFraction  = static_cast<real_t>(neighbor.second);
      auto neighborSurface = std::max(container.cells(neighbor.first).surface, real_t(1)); 
      int neighborType      = container.cells(neighborCellID).type;
      real_t currentSurface = std::max(cell.second.surface, real_t(1));

      for (unsigned int sig = 0; sig < numberOfSignals; sig++) {
        real_t currentSignal  = cell.second.signal[sig];
        real_t neighborSignal = container.cells(neighborCellID).signal[sig];

        if (currentSignal <= neighborSignal) continue;

        auto& diffusionConst = data.diffusionSignal[sig]->getStorage();

        real_t flux = 0.0;
        if (cell.second.type <= static_cast<int>(data.liquid)) {
          flux = neighborSurface >= 1.0 ? neighborSurfaceFraction * diffusionConst.getValue(currentType, neighborType) * (currentSignal - neighborSignal) / neighborSurface : 0.;
        } else {
          flux = currentSurface >= 1.0 ? neighborSurfaceFraction * diffusionConst.getValue(currentType, neighborType) * (currentSignal - neighborSignal) / currentSurface : 0.;
        }

        if (constantSig.getValue(neighborType) == 0) container.cells(neighborCellID).deltaSignal[sig] += flux;
        if (constantSig.getValue(currentType) == 0) cell.second.deltaSignal[sig] += -flux;
      }
    }
  }
}

/**
 * Calculates the shared surface of adjacent cells this is then used for the diffusion.
 *
 * @param block  The block.
 */
template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::initializeNeighborhood(block::Block* block) const {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& field     = block->getFieldByName("cells")->get<cellid_t>();
  auto liquid     = container.getSimData().getData<DataCells>().liquid;

  container.cells.clearNeighbors();
  auto view = field.createView();
  for (const auto& cell : view) {
    cellid_t currentCell = view.get(cell, Direction::C);
    if (currentCell == liquid) continue;

    for (auto b : DxC6<DIM>()) {
      cellid_t neighborCellID = view.get(cell, D3C6::dir[b]);
      if (neighborCellID == currentCell || neighborCellID == liquid /*|| !container.cells.contains(currentCell)*/) continue;

      container.cells(currentCell).neighbors[neighborCellID]++;
      container.cells(neighborCellID).neighbors[currentCell]++;
    }
  }
}

template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::executeBlock(block::Block* block) {
  // Do signaling after exchange of neighborhood
  if ((getSimData().timestep) % eachNthStep == 0 && getSimData().timestep > 2) {
    initializeNeighborhood(block);
    calcDiffusion(block);
  }
}

template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

template <unsigned int DIM>
void SweepSignalDiffusion<DIM>::setup(const block::BlockRegister& blockRegister) {
  auto& data = getSimData().template getData<DataCells>();
  for (const auto& block : blockRegister) {
    auto& container = block.second->getDataContainer<CellBlockContainer>();

    for (auto& cell : container.cells) {
      int currentType = cell.second.type;
      for (unsigned int i = 0; i < numberOfSignals; i++) {
        auto& startSig        = data.startSignal[i]->getStorage();
        cell.second.signal[i] = startSig.getValue(currentType);
      }
    }
  }
}

template class SweepSignalDiffusion<3>;
template class SweepSignalDiffusion<2>;

}  // namespace nastja
