/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/utility.h"
#include "lib/cuda/cuda_math.h"
#include "lib/cuda/cuda_real_functions.h"
#include "lib/cuda/cuda_sweep_ops.h"
#include "lib/stencil/direction.h"
#include "sweep_temp_kernel.h"

/**
 * With nvcc 10.1 there are problems with the JSON library includes, therefore we can't include field.h because the json headers are used there.
 * https://github.com/nlohmann/json/issues/1347
 *
 * This might be solved in nvcc 10.2
 * https://github.com/nlohmann/json/issues/1773#issuecomment-557489347
 */

namespace nastja {
namespace cuda {

using namespace stencil;

__constant__ real_t CVa[4];
__constant__ real_t CVb[4];
__constant__ real_t La[5];
__constant__ real_t k[2];
int tempConstCopied = 0;

__constant__ real_t Tm = 1.0;

__constant__ CudaGpuFieldPtrs<real_t> phaseOldGpuPtr;
__constant__ CudaGpuFieldPtrs<real_t> phaseNewGpuPtr;

__constant__ CudaGpuFieldPtrs<real_t> tempOldGpuPtr;
__constant__ CudaGpuFieldPtrs<real_t> tempNewGpuPtr;

__inline__ __device__ void calcEnergyflux(real_t* tempCells, real_t* phiCells, volatile real_t* efluxCells, real_t dxr) {
  //interpolate_byPhi(k, interpolate(phi, phi_yp)) * gradT_yp
  efluxCells[toUnderlying(Direction::R)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::R)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::R)]))) * (tempCells[toUnderlying(Direction::R)] - tempCells[toUnderlying(Direction::C)]) * dxr;
  efluxCells[toUnderlying(Direction::U)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::U)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::U)]))) * (tempCells[toUnderlying(Direction::U)] - tempCells[toUnderlying(Direction::C)]) * dxr;
  efluxCells[toUnderlying(Direction::F)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::F)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::F)]))) * (tempCells[toUnderlying(Direction::F)] - tempCells[toUnderlying(Direction::C)]) * dxr;
  efluxCells[toUnderlying(Direction::L)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::L)]) * dxr;  //tdiffu(T_xm, T, dxr);
  efluxCells[toUnderlying(Direction::D)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::D)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::D)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::D)]) * dxr;  //tdiffu(T_ym, T, dxr);
  efluxCells[toUnderlying(Direction::B)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::B)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::B)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::B)]) * dxr;  //tdiffu(T_zm, T, dxr);
}

__inline__ __device__ void calcEnergyfluxShared(real_t* tempCells, real_t* phiCells, volatile real_t* efluxCells, real_t dxr) {
  __shared__ real_t efluxSh[3][cudaBlockSizeZ + 1][cudaBlockSizeY + 1][cudaBlockSizeX + 1];

  //if(threadIdx.x == 0) efluxSh[0][threadIdx.z+1][threadIdx.y+1][threadIdx.x] = (k[0] * real_t(0.5)*(phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]) + k[1] * (real_t(1.0) - real_t(0.5)*(phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::L)])*dxr;
  efluxSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::R)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::R)]))) * (tempCells[toUnderlying(Direction::R)] - tempCells[toUnderlying(Direction::C)]) * dxr;
  if (threadIdx.y == 0) efluxSh[1][threadIdx.z + 1][threadIdx.y][threadIdx.x + 1] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::D)] + phiCells[toUnderlying(Direction::C)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::D)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::D)]) * dxr;
  efluxSh[1][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::U)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::U)]))) * (tempCells[toUnderlying(Direction::U)] - tempCells[toUnderlying(Direction::C)]) * dxr;
  if (threadIdx.z == 0) efluxSh[2][threadIdx.z][threadIdx.y + 1][threadIdx.x + 1] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::B)] + phiCells[toUnderlying(Direction::C)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::B)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::B)]) * dxr;
  efluxSh[2][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::F)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::F)]))) * (tempCells[toUnderlying(Direction::F)] - tempCells[toUnderlying(Direction::C)]) * dxr;

  efluxCells[toUnderlying(Direction::R)] = efluxSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  efluxCells[toUnderlying(Direction::U)] = efluxSh[1][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  efluxCells[toUnderlying(Direction::F)] = efluxSh[2][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x + 1];
  __syncthreads();
  efluxCells[toUnderlying(Direction::L)] = efluxSh[0][threadIdx.z + 1][threadIdx.y + 1][threadIdx.x];
  efluxCells[toUnderlying(Direction::D)] = efluxSh[1][threadIdx.z + 1][threadIdx.y][threadIdx.x + 1];
  efluxCells[toUnderlying(Direction::B)] = efluxSh[2][threadIdx.z][threadIdx.y + 1][threadIdx.x + 1];

  // moved down for improved register usage with no performance loss
  if (threadIdx.x == 0) efluxCells[toUnderlying(Direction::L)] = (k[0] * real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]) + k[1] * (real_t(1.0) - real_t(0.5) * (phiCells[toUnderlying(Direction::C)] + phiCells[toUnderlying(Direction::L)]))) * (tempCells[toUnderlying(Direction::C)] - tempCells[toUnderlying(Direction::L)]) * dxr;
  ;
}

template <Direction dir>
__global__ void tempKernel(CudaArguments args, CudaSimData cSimData) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x + args.startX;  //boundarySizeX;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y + args.startY;  //boundarySizeY;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z + args.startZ;  //boundarySizeZ;

  if (checkLimits<dir>(x, y, z, args)) {
    real_t phiCells[7];
    real_t tempCells[7];
    loadStencil<7, dir>(phiCells, phaseOldGpuPtr, x, y, z, args);
    loadStencil<7, dir>(tempCells, tempOldGpuPtr, x, y, z, args);

    real_t phiNewC;
    loadStencil<1, dir>(&phiNewC, phaseNewGpuPtr, x, y, z, args);

    real_t efluxCells[7];
    calcEnergyflux(tempCells, phiCells, efluxCells, cSimData.deltax_R);
    //calcEnergyfluxShared(tempCells, phiCells, efluxCells, cSimData.deltax_R);

    real_t T   = tempCells[toUnderlying(Direction::C)];
    real_t phi = phiCells[toUnderlying(Direction::C)];

    real_t divgrad = diffl(efluxCells[toUnderlying(Direction::R)], efluxCells[toUnderlying(Direction::L)], cSimData.deltax_R) +
                     diffl(efluxCells[toUnderlying(Direction::U)], efluxCells[toUnderlying(Direction::D)], cSimData.deltax_R) +
                     diffl(efluxCells[toUnderlying(Direction::F)], efluxCells[toUnderlying(Direction::B)], cSimData.deltax_R);

    if (only_diffusion) {
      T += cSimData.deltat * divgrad;
    } else {
      real_t Temp   = (T - Tm) / Tm;
      real_t cva    = calc(CVa, 4, T);
      real_t cvb    = calc(CVb, 4, T);
      real_t dLadT  = calcDerivation(La, 5, 1, T);
      real_t d2LadT = calcDerivation(La, 5, 2, T);
      real_t L      = calc(La, 5, T);
      real_t intcva = calcIntegral(CVa, 4, T) - calcIntegral(CVa, 4, Tm);
      real_t intcvb = calcIntegral(CVb, 4, T) - calcIntegral(CVb, 4, Tm);

      real_t deltaphia = phiNewC - phi;

      real_t sumnum   = (-dLadT * T * Temp - L + intcva - intcvb) * dhdphi(phi) * deltaphia;
      real_t sumdenum = -(d2LadT * T * Temp + real_t(2.0) * dLadT * T / Tm - cva) * h(phi) + cvb * h(real_t(1.0) - phi);

      T = T + ((cSimData.deltat * divgrad - sumnum) / (sumdenum));
    }

    storeStencil<dir>(tempNewGpuPtr, x, y, z, args, T);
  }
}

void copyTempConst() {
  real_t kl[2];
  real_t CVal[4];
  real_t CVbl[4];
  real_t Lal[5];

  kl[0] = real_t(2103.320339) / real_t(100.0);
  kl[1] = real_t(1708.947775) / real_t(100.0);

  CVal[0] = real_t(0.9816705873);
  CVal[1] = real_t(0.04412186595);
  CVal[2] = real_t(-0.02403402118);
  CVal[3] = real_t(-0.001758431968);

  CVbl[0] = real_t(1.331101576);
  CVbl[1] = real_t(-0.4598970843);
  CVbl[2] = real_t(0.05225148201);
  CVbl[3] = real_t(-0.003237275948);

  Lal[0] = real_t(-0.02161349859);
  Lal[1] = real_t(0.7705647776);
  Lal[2] = real_t(-0.4349444313);
  Lal[3] = real_t(0.05210816941);
  Lal[4] = real_t(-0.003776436992);

  cudaMemcpyToSymbol(k, &kl, 2 * sizeof(real_t));
  cudaMemcpyToSymbol(CVa, &CVal, 4 * sizeof(real_t));
  cudaMemcpyToSymbol(CVb, &CVbl, 4 * sizeof(real_t));
  cudaMemcpyToSymbol(La, &Lal, 5 * sizeof(real_t));
  tempConstCopied = 1;
}

void sweep_temp_kernel(cudaStream_t stream, cudaStream_t haloStream, CudaGpuFieldPtrs<real_t> h_oldGpuPtr1, CudaGpuFieldPtrs<real_t> h_newGpuPtr1, CudaGpuFieldPtrs<real_t> h_oldGpuPtr2, CudaGpuFieldPtrs<real_t> h_newGpuPtr2, CudaArguments args, CudaSimData cSimData) {
  if (tempConstCopied == 0) copyTempConst();

  // Using MemcpyToSymbol instead of kernel parameter allows for an easy description of an abstract kernel type.
  // This is used in the following line to iterate through all 27 kernels
  gpuErrchk(cudaMemcpyToSymbol(phaseOldGpuPtr, &h_oldGpuPtr1, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(phaseNewGpuPtr, &h_newGpuPtr1, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(tempOldGpuPtr, &h_oldGpuPtr2, sizeof(CudaGpuFieldPtrs<real_t>)));
  gpuErrchk(cudaMemcpyToSymbol(tempNewGpuPtr, &h_newGpuPtr2, sizeof(CudaGpuFieldPtrs<real_t>)));

  // This is performing something like a loop unrolling at compile time.
  // Therefore all templated instances of the heatKernel can be created at compiletime
  // This is not working with a usual for loop over all the directions, template packs have to be used.
  staticFor<numberOfDirections>([&](auto i) {
    launchKernel<static_cast<Direction>(i())>(tempKernel<static_cast<Direction>(i())>, stream, haloStream, args, cSimData);
  });
}

}  // namespace cuda
}  // namespace nastja
