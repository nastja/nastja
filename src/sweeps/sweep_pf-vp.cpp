/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_pf-vp.h"
#include "lib/logger.h"
#include "lib/action/dynamicblock.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;

void SweepPfVp::execute(const block::BlockRegister& blockRegister) {
  initialVolume_ += increaseVolume_;

  real_t chi    = 0.0;
  real_t volume = 0.0;

  // eps=8, gamma=1, mu=1
  real_t const A = 8;                           // eps * gamma / mu
  real_t const B = 32.0 / (8.0 * M_PI * M_PI);  // 32 / eps * pi^2
  real_t const C = 2 * B;

  for (const auto& block : blockRegister) {
    auto& field = block.second->getFieldByName("phasefield")->get<real_t>();

    if (block.second->hasDataContainer<DynamicBlockContainer>()) {
      auto& data = block.second->getDataContainer<DynamicBlockContainer>();
      deltaHiddenVolume_ += data.addedVolume;
      data.addedVolume = 0;
    }

    const auto& boundarySize = field.getBoundarySize();
    unsigned long vectorSize = field.getVectorSize();

    for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
      for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
        for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
          for (unsigned long v = 0; v < vectorSize; v++) {
            auto index     = field.getIndex(x, y, z);
            real_t cell    = field.getCell<1>(index, v);
            real_t cell_xm = field.getCell<1>(index, Direction::L, v);
            real_t cell_xp = field.getCell<1>(index, Direction::R, v);
            real_t cell_ym = field.getCell<1>(index, Direction::D, v);
            real_t cell_yp = field.getCell<1>(index, Direction::U, v);
            real_t cell_zm = field.getCell<1>(index, Direction::B, v);
            real_t cell_zp = field.getCell<1>(index, Direction::F, v);
            cell += getSimData().deltat * (A * (-6.0 * cell + cell_xp + cell_xm + cell_yp + cell_ym + cell_zp + cell_zm) / (getSimData().deltax * getSimData().deltax) + C * cell - B);
            cell = fmax(fmin(cell, 1.0), 0.0);

            field.getCell(index, v) = cell;

            volume += cell;
            chi += cell * cell * (1. - cell) * (1. - cell);
            // chi    += 6. * cell * (1. - cell);
          }
        }
      }
    }
  }

  std::array<real_t, 3> values{};
  values[0] = volume;
  values[1] = chi;
  values[2] = deltaHiddenVolume_;

  // logger::get().debug("{:f}, {:f}", volume, chi);
  MPI_Allreduce(MPI_IN_PLACE, values.data(), values.size(), MPI_NASTJA_REAL, MPI_SUM, MPI_COMM_WORLD);

  // logger::get().debug("ALL {:f}, {:f}", values[0], values[1]);
  initialVolume_ += values[2];
  real_t deltaVolume = initialVolume_ - values[0];

  if (fabs(deltaVolume) == 0.) return;  //TODO nearlyEqual

  chi = deltaVolume / values[1];

  deltaHiddenVolume_ = 0;
  for (const auto& block : blockRegister) {
    auto& field = block.second->getFieldByName("phasefield")->get<real_t>();

    real_t blockVolume = 0;

    const auto& boundarySize = field.getBoundarySize();
    unsigned long vectorSize = field.getVectorSize();
    for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
      for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
        for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
          for (unsigned long v = 0; v < vectorSize; v++) {
            real_t cell = field.getCell(x, y, z, v);
            cell += chi * cell * cell * (1. - cell) * (1. - cell);
            // cell += 6. * cell * (1. - cell);

            cell = fmax(fmin(cell, 1.), 0.);

            field.getCell(x, y, z, v) = cell;

            blockVolume += cell;
          }
        }
      }
    }

    if (block.second->hasDataContainer<DynamicBlockContainer>()) {
      auto& data             = block.second->getDataContainer<DynamicBlockContainer>();
      real_t fullBlockVolume = field.getVolume();
      if (data.ttl != 0) {
        data.ttl--;
      } else {
        if (blockVolume == 0.0 || blockVolume == fullBlockVolume) {
          // The inner domain of the block is empty or full, test boundary
          real_t boundarySum = field.sumBoundary(0);
          if (boundarySum == 0.0) {
            logger::get().trace(logger::Category::DynCnstBlk, "EMPTY BLOCK {}!", field.getID());
            data.setEmpty();
          } else if (boundarySum == field.calcBoundaryCells()) {
            logger::get().trace(logger::Category::DynCnstBlk, "FULL BLOCK {}!", field.getID());
            data.setFull();
            deltaHiddenVolume_ -= fullBlockVolume;
          }
        }
      }
    }
  }
}

void SweepPfVp::setup(const block::BlockRegister& blockRegister) {
  real_t volume = 0.0;

  for (const auto& block : blockRegister) {
    auto& field = block.second->getFieldByName("phasefield")->get<real_t>();

    const auto& boundarySize = field.getBoundarySize();
    unsigned long vectorSize = field.getVectorSize();

    for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
      for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
        for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
          for (unsigned long v = 0; v < vectorSize; v++) {
            real_t cell = field.getCell(x, y, z, v);

            volume += cell;
          }
        }
      }
    }
  }

  MPI_Allreduce(MPI_IN_PLACE, &volume, 1, MPI_NASTJA_REAL, MPI_SUM, MPI_COMM_WORLD);

  // logger::get().debug("ALL {:f}, {:f}", values[0], values[1]);
  initialVolume_ = volume;
  logger::get().oneinfo("Initial volume is set to {:f}.", initialVolume_);
}

void SweepPfVp::dump(Archive& ar) const {
  ar.pack(initialVolume_);
  ar.pack(deltaHiddenVolume_);
  ar.pack(increaseVolume_);
}

void SweepPfVp::undump(Archive& ar) {
  ar.unpack(initialVolume_);
  ar.unpack(deltaHiddenVolume_);
  ar.unpack(increaseVolume_);
}

}  // namespace nastja
