/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/sweep.h"

namespace nastja {

/**
 * Class for sweep heat equation.
 *
 * @ingroup sweeps
 */
class SweepHeat : public Sweep {
public:
  explicit SweepHeat() {
    registerFieldname("temperature");
  }

  ~SweepHeat() override           = default;
  SweepHeat(const SweepHeat&)     = delete;
  SweepHeat(SweepHeat&&) noexcept = default;
  SweepHeat& operator=(const SweepHeat&) = delete;
  SweepHeat& operator=(SweepHeat&&) = delete;

  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
};

}  // namespace nastja
