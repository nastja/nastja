/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/storage/paramstorage.h"
#include "lib/stencil/direction.h"
#include "lib/math/random.h"
#include <unordered_set>

namespace nastja {

/**
 Detect cells next to solids and degrade the solid voxels in question to liquid
 *
 * Actions have to be registered via `registerAction()`.
 *
 * @ingroup actions cells
 */
class EcmDegradation : public Action {
public:
  explicit EcmDegradation(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~EcmDegradation() override                = default;
  EcmDegradation(const EcmDegradation&)     = delete;
  EcmDegradation(EcmDegradation&&) noexcept = default;
  EcmDegradation& operator=(const EcmDegradation&) = delete;
  EcmDegradation& operator=(EcmDegradation&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override {
    return !(getSimData().timestep == 0) &&
           ((getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0);
  }
private:
  math::UniformReal<real_t> probability_{0, 1};      ///< The uniform distribution of the probability. 
  std::unique_ptr<ParameterStorageBuilder<real_t>> degradationProbability_{};  ///< The pointer to the parameter storage for the degradation probability
};

}  // namespace nastja
