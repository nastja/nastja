/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_pftemp.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/math/pfmath.h"
#include "lib/math/vector.h"
#include "lib/simdata/datapfm.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace math;
using namespace stencil;

void SweepPhaseFieldTemperature::setup(const block::BlockRegister& /*blockRegister*/) {
  auto& config = getSimData().getConfig();
  getSimData().initData<DataPFM>(config);
}

real_t SweepPhaseFieldTemperature::dfLdphi(real_t phi, real_t T, const DataPFM& data) const {
  real_t L = data.La.calc(T);
  return L * (T - data.Tm) / data.Tm * dhdphi(phi);
}

real_t SweepPhaseFieldTemperature::dfTdphi(real_t phi, real_t T, const DataPFM& data) const {
  real_t intcva = data.CVa.calcIntegral(T) - data.CVa.calcIntegral(data.Tm);
  real_t intcvb = data.CVb.calcIntegral(T) - data.CVb.calcIntegral(data.Tm);

  real_t suminta = data.CVa[0] * std::log(T / data.Tm);
  for (unsigned long j = 1; j <= data.CVa.getDegree(); j++) {
    suminta += 1.0 / real_t(j) * data.CVa[j] * (std::pow(T, j) - std::pow(data.Tm, j));
  }

  real_t sumintb = data.CVb[0] * std::log(T / data.Tm);
  for (unsigned long j = 1; j <= data.CVb.getDegree(); j++) {
    sumintb += 1.0 / real_t(j) * data.CVb[j] * (std::pow(T, j) - std::pow(data.Tm, j));
  }

  return (intcva - T * suminta - intcvb + T * sumintb) * dhdphi(phi);
}

real_t SweepPhaseFieldTemperature::diffu(const real_t center, const real_t upper) const {
  return (upper - center) * getSimData().deltax_R;
}

real_t SweepPhaseFieldTemperature::diffl(const real_t center, const real_t lower) const {
  return (center - lower) * getSimData().deltax_R;
}

real_t SweepPhaseFieldTemperature::diffc(const real_t lower, const real_t upper) const {
  return 0.5 * (upper - lower) * getSimData().deltax_R;
}

real_t SweepPhaseFieldTemperature::A(const Vector3<real_t>& q, const DataPFM& data) const {
  real_t q2 = q[0] * q[0] + q[1] * q[1] + q[2] * q[2];

  real_t q4 = q2 * q2;
  real_t q6 = q4 * q2;

  real_t q44  = (q[0] * q[0] * q[0] * q[0] + q[1] * q[1] * q[1] * q[1] + q[2] * q[2] * q[2] * q[2]) / q4;
  real_t q123 = (q[0] * q[0] * q[1] * q[1] * q[2] * q[2]) / q6;

  return 1.0 + data.epsilon1 * (q44 - 0.6) +
         data.epsilon2 * (3.0 * q44 + 66.0 * q123 - 17.0 / 7.0) +
         data.epsilon3 * (5.0 * q44 * q44 - 16.0 * q123 - (94.0 * q44 - 33.0) / 13.0);
}

Vector3<real_t> SweepPhaseFieldTemperature::dAdq(const Vector3<real_t>& q, const DataPFM& data) const {
  Vector3<real_t> res;

  real_t q2 = q[0] * q[0] + q[1] * q[1] + q[2] * q[2];

  if (q2 == 0.0) return res;

  real_t q2r = 1.0 / q2;

  real_t q123 = 1.0;
  real_t q44  = 0.0;
  Vector3<real_t> q3;
  for (unsigned long k = 0; k < 3; k++) {
    real_t q_2 = q[k] * q[k];

    q123 *= q_2 * q2r;
    q44 += q_2 * q_2;

    q3[k] = q_2 * q[k];
  }
  q44 *= q2r * q2r;  // (q_1^4+q_2^4+q_3^4)/(|q|^4)

  real_t ac = 1.0 + data.epsilon1 * (q44 - 0.6) +
              data.epsilon2 * (3.0 * q44 + 66.0 * q123 - 17.0 / 7.0) +
              data.epsilon3 * (5.0 * q44 * q44 - 16.0 * q123 - (94.0 * q44 - 33.0) / 13.0);

  for (unsigned long k = 0; k < 3; k++) {
    res[k] = 2.0 * ac * q[k];

    if (q123 != 0.0) {
      real_t q123q2 = q123 * q2 / q[k];
      res[k] += data.epsilon1 * (4.0 * q3[k] * q2r - q44 * q[k]) +
                data.epsilon2 * 12.0 * (q3[k] * q2r + 11.0 * q123q2 - (q44 + 33.0 * q123) * q[k]) +
                data.epsilon3 * (40.0 * q44 * (q3[k] * q2r - q44 * q[k]) - 32.0 * (q123q2 - 3.0 * q123 * q[k] + 11.75 / 13.0 * (q3[k] * q2r - q44 * q[k])));
    }
  }

  res *= ac;

  return res;
}

void SweepPhaseFieldTemperature::executeBlock(block::Block* block) {
  auto& phasefield  = block->getFieldByName("phasefield")->get<real_t>();
  auto& temperature = block->getFieldByName("temperature")->get<real_t>();

  const auto& data = getSimData().getData<DataPFM>();

  auto boundarysize = phasefield.getBoundarySize();

  for (unsigned long z = boundarysize[2]; z < phasefield.getSize(2) - boundarysize[2]; z++) {
    for (unsigned long y = boundarysize[1]; y < phasefield.getSize(1) - boundarysize[1]; y++) {
      for (unsigned long x = boundarysize[0]; x < phasefield.getSize(0) - boundarysize[0]; x++) {
        auto index    = phasefield.getIndex(x, y, z);
        real_t phi    = phasefield.getCell<1>(index);
        real_t phi_xm = phasefield.getCell<1>(index, Direction::L);
        real_t phi_xp = phasefield.getCell<1>(index, Direction::R);
        real_t phi_ym = phasefield.getCell<1>(index, Direction::D);
        real_t phi_yp = phasefield.getCell<1>(index, Direction::U);
        real_t phi_zm = phasefield.getCell<1>(index, Direction::B);
        real_t phi_zp = phasefield.getCell<1>(index, Direction::F);

        real_t T = temperature.getCell(x, y, z);

        real_t gradphi_xp = diffu(phi, phi_xp);
        real_t gradphi_yp = diffu(phi, phi_yp);
        real_t gradphi_zp = diffu(phi, phi_zp);
        real_t gradphi_xm = diffu(phi_xm, phi);
        real_t gradphi_ym = diffu(phi_ym, phi);
        real_t gradphi_zm = diffu(phi_zm, phi);

        real_t L = data.La.calc(T);

        real_t divgradphi;
        real_t tau_R;

        if (anisotropic) {
          // anisotrope kinetic
          Vector3<real_t> grad_center{diffc(phi_xm, phi_xp),
                                      diffc(phi_ym, phi_yp),
                                      diffc(phi_zm, phi_zp)};
          if (grad_center == Vector3<real_t>::zero) {
            // no gradient everything is zero
            phasefield.getCell(index) = phi;
            continue;
          }

          if (betazero) {
            // arbitrarily relaxation parameter beta=0
            real_t gamma = A(grad_center, data);
            tau_R        = data.Tm * T * interpolate_byPhi(data.k, phi) / (L * L * data.epsilon * data.MplusF * gamma * gamma);
          } else {
            // anisotropic kinetic
            grad_center.normalize();

            real_t n123 = grad_center[0] * grad_center[0] * grad_center[1] * grad_center[1] * grad_center[2] * grad_center[2];
            real_t n44  = std::pow(grad_center[0], 4.0) + std::pow(grad_center[1], 4.0) + std::pow(grad_center[2], 4.0);
            real_t n66  = std::pow(grad_center[0], 6.0) + std::pow(grad_center[1], 6.0) + std::pow(grad_center[2], 6.0);

            real_t k = data.k0 * (1.0 + data.epsilon_k * (-3.0 + 4.0 * n44) + data.delta_k * (n66 + 30.0 * n123));

            tau_R = k * data.Tm * T / L;
          }

          const real_t half = 0.5;
          // a anisotrop
          Vector3<real_t> grad_phi_xm{diffu(phi_xm, phi),
                                      half * (diffc(phi_ym, phi_yp) + diffc(phasefield.getCell<1>(index, Direction::DL), phasefield.getCell<1>(index, Direction::UL))),
                                      half * (diffc(phi_zm, phi_zp) + diffc(phasefield.getCell<1>(index, Direction::BL), phasefield.getCell<1>(index, Direction::FL)))};

          Vector3<real_t> grad_phi_xp{diffu(phi, phi_xp),
                                      half * (diffc(phi_ym, phi_yp) + diffc(phasefield.getCell<1>(index, Direction::DR), phasefield.getCell<1>(index, Direction::UR))),
                                      half * (diffc(phi_zm, phi_zp) + diffc(phasefield.getCell<1>(index, Direction::BR), phasefield.getCell<1>(index, Direction::FR)))};

          Vector3<real_t> grad_phi_ym{half * (diffc(phi_xm, phi_xp) + diffc(phasefield.getCell<1>(index, Direction::DL), phasefield.getCell<1>(index, Direction::DR))),
                                      diffu(phi_ym, phi),
                                      half * (diffc(phi_zm, phi_zp) + diffc(phasefield.getCell<1>(index, Direction::BD), phasefield.getCell<1>(index, Direction::FD)))};

          Vector3<real_t> grad_phi_yp{half * (diffc(phi_xm, phi_xp) + diffc(phasefield.getCell<1>(index, Direction::UL), phasefield.getCell<1>(index, Direction::UR))),
                                      diffu(phi, phi_yp),
                                      half * (diffc(phi_zm, phi_zp) + diffc(phasefield.getCell<1>(index, Direction::BU), phasefield.getCell<1>(index, Direction::FU)))};

          Vector3<real_t> grad_phi_zm{half * (diffc(phi_xm, phi_xp) + diffc(phasefield.getCell<1>(index, Direction::BL), phasefield.getCell<1>(index, Direction::BR))),
                                      half * (diffc(phi_ym, phi_yp) + diffc(phasefield.getCell<1>(index, Direction::BD), phasefield.getCell<1>(index, Direction::BU))),
                                      diffu(phi_zm, phi)};

          Vector3<real_t> grad_phi_zp{half * (diffc(phi_xm, phi_xp) + diffc(phasefield.getCell<1>(index, Direction::FL), phasefield.getCell<1>(index, Direction::FR))),
                                      half * (diffc(phi_ym, phi_yp) + diffc(phasefield.getCell<1>(index, Direction::FD), phasefield.getCell<1>(index, Direction::FU))),
                                      diffu(phi, phi_zp)};

          divgradphi = diffl(dAdq(grad_phi_xp, data)[0], dAdq(grad_phi_xm, data)[0]) + diffl(dAdq(grad_phi_yp, data)[1], dAdq(grad_phi_ym, data)[1]) + diffl(dAdq(grad_phi_zp, data)[2], dAdq(grad_phi_zm, data)[2]);
        } else {
          // a isotrop
          divgradphi = diffl(gradphi_xp, gradphi_xm) + diffl(gradphi_yp, gradphi_ym) + diffl(gradphi_zp, gradphi_zm);

          // isotrope kinetic
          tau_R = data.k0 * data.Tm * T / L;
        }

        divgradphi *= data.gab;

        // w
        real_t dwdphi = data.gab * (1.0 - 2.0 * phi);
        dwdphi *= 32.0 / (M_PI * M_PI * data.epsilon * data.epsilon);

        // f
        real_t dfdphi = dfLdphi(phi, T, data) + dfTdphi(phi, T, data);
        // dfdphi += h(phi) * data.CVa.calc(T);
        dfdphi /= T * data.epsilon;

        real_t dphi = divgradphi - dwdphi - dfdphi;

        dphi *= tau_R;
        dphi *= getSimData().deltat;

        phi += dphi;

        phasefield.getCell(index) = fmax(fmin(phi, 1.0), 0.0);  // Simplex
      }
    }
  }
}

void SweepPhaseFieldTemperature::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
