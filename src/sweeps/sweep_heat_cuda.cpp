/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_heat_cuda.h"
#include "lib/field/field.h"
#include "sweep_heat_kernel.h"

namespace nastja {

void SweepHeatCuda::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName("temperature")->get<real_t>();

  cuda::sweep_heat_kernel(field.getCuMainStream(), field.getCuHaloStream(),
                          field.getGpuPtrs(1), field.getGpuPtrs(0),
                          field.getCuArgs(), field.getCuSimData());
}

void SweepHeatCuda::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
