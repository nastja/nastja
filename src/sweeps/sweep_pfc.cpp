/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_pfc.h"
#include "lib/logger.h"
#include "lib/field/field.h"
// #include <fftw3_omp.h>
#include <omp.h>

namespace nastja {

using namespace config::literals;

template <unsigned int DIM>
SweepPFC<DIM>::SweepPFC() {
  static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  registerFieldname("psi");
  registerFieldname("psift");
  registerFieldname("nlpsift");
}
template <unsigned int DIM>
SweepPFC<DIM>::~SweepPFC() {
  fftw_destroy_plan(fft_psi);
  fftw_destroy_plan(ifft_psi);
  fftw_destroy_plan(fft_psinl);
#ifdef _OPENMP
  fftw_cleanup_threads();
#else
  fftw_cleanup();
#endif
}

template <unsigned int DIM>
void SweepPFC<DIM>::setup(const block::BlockRegister& blockRegister) {
  auto& config = getSimData().getConfig();

  /// @key{PhaseFieldCrystal.epsilon, real_t, 0.5}
  /// The parameter @f$\varepsilon@f$ related to the undercooling.
  epsilon = config.template getValue<real_t>("PhaseFieldCrystal.epsilon"_jptr, 0.5);

  /// @key{PhaseFieldCrystal.cutoff, real_t, 0.0}
  /// The radius of the cut-off frequencies.
  cutoffValue = config.template getValue<real_t>("PhaseFieldCrystal.cutoff"_jptr, 0.0);
  if (cutoffValue > 0.0) {
    cutoffValue = 2.0 * M_PI / cutoffValue;
    cutoffValue *= cutoffValue;
  } else {
    cutoffValue = std::numeric_limits<double>::infinity();
  }

  auto& block = *(blockRegister.begin()->second);

  auto& psi     = block.getFieldByName("psi")->get<double>();
  auto& psift   = block.getFieldByName("psift")->get<double>();
  auto& psiftnl = block.getFieldByName("nlpsift")->get<double>();

  fftw_plan_with_nthreads(omp_get_max_threads());

  long measurement = FFTW_ESTIMATE;
  logger::get().oneinfo("Starting fftw wisdom: {}", measurement);
  fftw_import_wisdom_from_filename("wisdom");

  if (DIM == 3) {
    n0 = psi.getSize(2);
    n1 = psi.getSize(1);
    n2 = psi.getSize(0);
    nc = (n2 / 2 + 1);

    scalefactor = 1.0 / (n0 * n1 * n2);

    // n2 is the fastest changing parameter
    fft_psi   = fftw_plan_dft_r2c_3d(n0, n1, n2, psi.getCellBasePtr(), (fftw_complex*)psift.getCellBasePtr(), measurement);
    ifft_psi  = fftw_plan_dft_c2r_3d(n0, n1, n2, (fftw_complex*)psift.getCellBasePtr(), psi.getCellBasePtr(), measurement);
    fft_psinl = fftw_plan_dft_r2c_3d(n0, n1, n2, psi.getCellBasePtr(), (fftw_complex*)psiftnl.getCellBasePtr(), measurement);

    cutx = 2.0 * M_PI / (n2 * getSimData().deltax);
    cuty = 2.0 * M_PI / (n1 * getSimData().deltax);
    cutz = 2.0 * M_PI / (n0 * getSimData().deltax);
  } else {
    n0 = psi.getSize(0);
    n1 = psi.getSize(1);
    nc = (n1 / 2 + 1);

    scalefactor = 1.0 / (n0 * n1);

    fft_psi   = fftw_plan_dft_r2c_2d(n0, n1, psi.getCellBasePtr(), (fftw_complex*)psift.getCellBasePtr(), measurement);
    ifft_psi  = fftw_plan_dft_c2r_2d(n0, n1, (fftw_complex*)psift.getCellBasePtr(), psi.getCellBasePtr(), measurement);
    fft_psinl = fftw_plan_dft_r2c_2d(n0, n1, psi.getCellBasePtr(), (fftw_complex*)psiftnl.getCellBasePtr(), measurement);

    cutx = 2.0 * M_PI / (n0 * getSimData().deltax);
    cuty = 2.0 * M_PI / (n1 * getSimData().deltax);
  }

  fftw_export_wisdom_to_filename("wisdom");
  logger::get().oneinfo("Finished fftw wisdom");
}

/**
 * Calculate the square of the wave vector @f$(2 \pi k)^2@f$.
 *
 * @param  index  The index.
 *
 * @return wave vector
 */
template <>
double SweepPFC<3>::getWaveVectorSq(const unsigned long index) const {
  long x     = index % nc;
  long layer = index / nc;
  long y     = layer % n1;
  long z     = layer / n1;

  // > Nyquist frequency?
  if (z > n0 / 2) z -= n0;
  if (y > n1 / 2) y -= n1;

  // Calculate the components of the wave vector.
  double kx = cutx * x;
  double ky = cuty * y;
  double kz = cutz * z;

  return kx * kx + ky * ky + kz * kz;
}

template <>
double SweepPFC<2>::getWaveVectorSq(const unsigned long index) const {
  long x = index / nc;
  long y = index - x * nc;

  // > Nyquist frequency?
  if (x > n0 / 2) x -= n0;

  // Calculate the components of the wave vector.
  double kx = cutx * x;
  double ky = cuty * y;

  return kx * kx + ky * ky;
}

template <unsigned int DIM>
bool SweepPFC<DIM>::cutoff(const double kappa) const {
  return (kappa > cutoffValue);
}

template <unsigned int DIM>
void SweepPFC<DIM>::executeBlock(block::Block* block) {
  unsigned long index = 0;

  double C0;
  double C1;
  double C2;
  double C3;
  double C4;
  double A2;
  double A3;
  double gamma;
  double EB;
  double kmsq;
  if (DIM == 2) {
    // simple
    A2      = 0.;
    A3      = 1.;
    epsilon = 0.2;
    C0      = 1. - epsilon;
    C1      = 2.;
    C2      = 1.;
    C3      = 0.;
    C4      = 0.;
  } else {
    A2    = -0.6917 / 2.;
    A3    = 0.0854 / 3.;
    C0    = 1. - (-49.);
    gamma = 11.583;
    EB    = 38.085;
    kmsq  = 2.985 * 2.985;
    C1    = (2. * gamma + 4. * EB) / kmsq;
    C2    = (gamma + 6. * EB) / (kmsq * kmsq);
    C3    = (4. * EB) / (kmsq * kmsq * kmsq);
    C4    = (EB) / (kmsq * kmsq * kmsq * kmsq);
  }

  auto& psi     = block->getFieldByName("psi")->get<double>();
  auto& psift   = block->getFieldByName("psift")->get<double>();
  auto& psiftnl = block->getFieldByName("nlpsift")->get<double>();

  auto data   = psi.getCellBasePtr();
  auto dataft = (fftw_complex*)psift.getCellBasePtr();
  auto datanl = (fftw_complex*)psiftnl.getCellBasePtr();

  fftw_execute(fft_psi);

  // Calculate the non-linear part in real-space.
  #pragma omp parallel for
  for (index = 0; index < psi.getCells(); index++) {
    data[index] = (A3 * data[index] + A2) * data[index] * data[index];
  }

  // Do the fft for the non-linear part.
  fftw_execute(fft_psinl);

  // Calculate the sub-operators.
  #pragma omp parallel for
  for (index = 0; index < psift.getCells(); index++) {
    double kappa = getWaveVectorSq(index);
    // if (cutoff(kappa)) {
    //   for (int i = 0; i < 2; i++) {
    //     dataft[index][i] = 0.0;
    //   }
    // } else {
    for (int i = 0; i < 2; i++) {
      // sub operator A1
      dataft[index][i] += -kappa * datanl[index][i] * getSimData().deltat;
      // sub operator A2
      if (DIM == 2) {
        dataft[index][i] /= (1.0 - getSimData().deltat * (-kappa * (C0 - kappa * (C1 - kappa * C2))));
      } else {
        dataft[index][i] /= (1.0 - getSimData().deltat * (-kappa * (C0 - kappa * (C1 - kappa * (C2 - kappa * (C3 - kappa * C4))))));
      }
    }
    // }
  }

  fftw_execute(ifft_psi);

  #pragma omp parallel for
  for (index = 0; index < psi.getCells(); index++) {
    data[index] *= scalefactor;
  }
}

template <unsigned int DIM>
void SweepPFC<DIM>::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

template class SweepPFC<3>;
template class SweepPFC<2>;

}  // namespace nastja
