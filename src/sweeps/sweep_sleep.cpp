/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_sleep.h"
#include "lib/timing.h"
#include "lib/action/dynamicblock.h"
#include "lib/field/field.h"
#include <chrono>
#include <thread>

namespace nastja {

void SweepSleep::executeBlock(block::Block* /*block*/) {
  std::this_thread::sleep_for(std::chrono::milliseconds(65));
}

void SweepSleep::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
