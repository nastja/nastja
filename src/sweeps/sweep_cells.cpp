/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep_cells.h"
#include "lib/logger.h"
#include "lib/timing.h"
#include "lib/field/field.h"
#include "lib/math/calculatesurface.h"
#include "lib/math/calculatevolume.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D2C5.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/D3C7.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <memory>

namespace nastja {

using namespace stencil;
using namespace config::literals;

template <unsigned int DIM>
void SweepCells<DIM>::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  Sweep::init(block, fieldpool);
  block->registerDataContainer<CellBlockContainer>(block);
}

template <unsigned int DIM>
void SweepCells<DIM>::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
  auto& data   = getSimData().template initData<DataCells>();  ///< Data for CellsInSilico.
  auto& config = getSimData().getConfig();

  /// @key{CellsInSilico, group}
  /// This groups all keys for the CellsInSilico module.

  /// @key{CellsInSilico.temperature, real_t}
  /// The Metropolis temperature.
  temperature_ = config.template getValue<real_t>("CellsInSilico.temperature"_jptr);

  /// @key{CellsInSilico.liquid, uint, 0}
  /// The liquid cellID. Cells with cellID < liquid are _solid_ cells (i.e. they are not changed by the cpm), cells with
  /// cellID > liquid are _normal_ cells (and will be changed by the CPM).
  data.liquid = config.template getValue<unsigned int>("CellsInSilico.liquid"_jptr, 0);

  /// @key{CellsInSilico.visitor.checkerboard, FactoryVisitor, "00"}
  /// The checkerboard for the visitor pattern.
  FactoryVisitor<DIM> factoryVisitor;
  visitor_ = config.factoryCreate(factoryVisitor, "CellsInSilico.visitor.checkerboard"_jptr, "00", getSimData());

  FactoryEnergy<DIM> factoryEnergy;
  /// @key{CellsInSilico.energyfunctions, FactoryEnergy[]}
  /// Add these energy functions to the Hamiltonian.
  config.registerFunctions(energyFunctions_, factoryEnergy, "CellsInSilico.energyfunctions"_jptr);
  energyFunctions_.init(data, config);

  const std::string& outdir = getSimData().getCmdLine().outdir;
  std::string filename      = fmt::format("{}/{:06d}/cellevents.log", outdir, getSimData().mpiData.getRank());

  /// @keyignore
  auto groupSize = config.template getValue<unsigned int>("Settings.logger.group"_jptr, 0);

  auto& logger = logger::registerLogger("cellevents", filename, groupSize);

  logger.registerTimeStepPtr(&getSimData().timestep);
  logger.setFormatAll("[\n  {{\"timestep\": {4}, ");  // Timestep
  logger.setFormatEnd("{}}}");
  logger.info(R"("event": "start")");
  logger.setFormatAll(",\n  {{\"timestep\": {4}, ");  // Timestep
  logger.setEndString("\n]\n");
}
  
template <unsigned int DIM>
void SweepCells<DIM>::executeBlock(block::Block* block) {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& field     = block->getFieldByName("cells")->get<cellid_t>();

  visitor_->visit(field, container, *this);
}

template <unsigned int DIM>
void SweepCells<DIM>::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

template <unsigned int DIM>
void SweepCells<DIM>::setup(const block::BlockRegister& blockRegister) {
  auto& data = getSimData().template getData<DataCells>();

  bool first = true;
  for (const auto& block : blockRegister) {
    auto& container = block.second->getDataContainer<CellBlockContainer>();
    /// assigning the available space of cellIDs to each block. this inhibits that the same new cell ID after initialization or division is chosen in different blocks -> cellIDs are unique
    cellid_t firstID  = container.reservedCellID;
    cellid_t blockIDs = (std::numeric_limits<cellid_t>::max() - firstID) / (block.second->blocks[0] * block.second->blocks[1] * block.second->blocks[2]);

    container.reservedCellID    = firstID + blockIDs * block.first;
    container.reservedCellIDMax = firstID + blockIDs * (block.first + 1) - 1;

    if (first) {
      logger::get().oneinfo("Reserved cellIDs per block: {}", blockIDs);
      first = false;
    }
    /// setup energy function
    energyFunctions_.setup(block.second, container, data);

    // setup voxels in block
    auto view = block.second->getFieldByName("cells")->get<cellid_t>().createView();
    for (auto* voxel : view) {
      cellid_t currentCell = view.get(voxel);
      if (currentCell <= data.liquid) continue;

      container.cells(currentCell).voxelsInBlock++;
    }
  }
}

template <unsigned int DIM>
void SweepCells<DIM>::dump(Archive& ar) const {
  energyFunctions_.dump(ar);
  visitor_->dump(ar);
}

template <unsigned int DIM>
void SweepCells<DIM>::undump(Archive& ar) {
  energyFunctions_.undump(ar);
  visitor_->undump(ar);
}

template class SweepCells<3>;
template class SweepCells<2>;

}  // namespace nastja
