/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "phasefield2.h"
#include "lib/action/actionregister.h"
#include "lib/action/borderdetection.h"
#include "lib/action/dynamicblock.h"
#include "lib/action/loadbalance.h"
#include "lib/stencil/D3C27.h"
#include "sweeps/sweep_pf1.h"
#include <memory>

using namespace nastja;

void AppPhasefield2::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("phasefield", stencil::D3C27{}, field::FieldType::real, 2);
}

void AppPhasefield2::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  actionpool.registerElement("Sweep:Phi", std::make_unique<SweepPhaseField>());
  registerWriterElements();
}

void AppPhasefield2::registerActionList() {
  blockmanager.registerAction("Sweep:Phi");

  blockmanager.registerAction("BC:phasefield");

  blockmanager.registerActionWriters();

  blockmanager.registerAction("Rotate:phasefield");
}

bool AppPhasefield2::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();
  blockmanager.executeActionWriterOnBlocks();

  blockmanager.executeActionOnBlocks("Rotateback:phasefield");

  return false;
}
