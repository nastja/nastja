/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sleep.h"
#include "lib/action/actionregister.h"
#include "lib/action/border.h"
#include "lib/stencil/D3C7.h"
#include "sweeps/sweep_sleep.h"
#include <memory>

using namespace nastja;

void AppSleep::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("phasefield", stencil::D3C7{}, field::FieldType::real);
}

void AppSleep::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  actionpool.registerElement("Sweep:Sleep", std::make_unique<SweepSleep>());
  actionpool.registerElement("BC:Phasefield_open", std::make_unique<Border>("phasefield", BorderType::open));
  actionpool.registerElement("BC:Phasefield_send", std::make_unique<Border>("phasefield", BorderType::send));
  actionpool.registerElement("BC:Phasefield_final", std::make_unique<Border>("phasefield", BorderType::finalize));
  registerWriterElements();
}

void AppSleep::registerActionList() {
  blockmanager.registerAction("Sweep:Sleep");

  blockmanager.registerAction("BC:Phasefield_open");
  blockmanager.registerAction("BC:Phasefield_send");
  blockmanager.registerAction("BC:Phasefield_final");

  blockmanager.registerActionWriters();

  blockmanager.registerAction("Rotate:phasefield");
}

bool AppSleep::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();

  blockmanager.executeActionWriterOnBlocks();

  blockmanager.executeActionOnBlocks("Rotateback:phasefield");

  return false;
}
