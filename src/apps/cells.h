/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"
#include <string>

using namespace nastja::config::literals;

/**
 * Application for Cell CPM simulations.
 * Here actions are selected and the order of actions is defined.
 *
 */
class AppCells : public nastja::Application {
public:
  explicit AppCells(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
    auto& config = simdata.getConfig();

    /// @key{CellsInSilico.2D, bool, false}
    /// Flag to enable the 2D simulation.
    use2D_ = config.getValue<bool>("CellsInSilico.2D"_jptr, false);

    /// @key{CellsInSilico.contactinhibition.enabled, bool, false}
    /// Flag to enable contact inhibition.
    useContactInhibition_ = config.getValue<bool>("CellsInSilico.contactinhibition.enabled"_jptr, false);

    /// @key{CellsInSilico.division.enabled, bool, false}
    /// Flag to enable the cell division.
    useDivision_ = config.getValue<bool>("CellsInSilico.division.enabled"_jptr, false);

    /// @key{CellsInSilico.signaling.enabled, bool, false}
    /// Flag to enable the signaling module.
    useSignaling_ = config.getValue<bool>("CellsInSilico.signaling.enabled"_jptr, false);

    /// @key{CellsInSilico.ecmdegradation.enabled, bool, false}
    /// Flag to enable ECM degradation.
    useECMDeg_ = config.getValue<bool>("CellsInSilico.ecmdegradation.enabled"_jptr, false);

    /// @key{CellsInSilico.signaling.constant, bool, false}
    /// Flag to enable constant signaling (turns off the signalling sweep if true).
    useConstantSignaling_ = config.getValue<bool>("CellsInSilico.signaling.constant"_jptr, false);

    /// @key{CellsInSilico.orientation.enabled, bool, false}
    /// Flag to enable the orientation module.
    useOrientation_ = config.getValue<bool>("CellsInSilico.orientation.enabled"_jptr, false);

    /// @key{CellsInSilico.polarity.enabled, bool, false}
    /// Flag to enable the polarity module.
    usePolarity_ = config.getValue<bool>("CellsInSilico.polarity.enabled"_jptr, false);


    /// @key{CellsInSilico.logcellproperties.enabled, bool, false}
    /// Flag to enable the cell property logging module (use for debugging only).
    useLogger_ = config.getValue<bool>("CellsInSilico.logcellproperties.enabled"_jptr, false);


  }

  ~AppCells() override          = default;
  AppCells(const AppCells&)     = delete;
  AppCells(AppCells&&) noexcept = default;
  AppCells& operator=(const AppCells&) = delete;
  AppCells& operator=(AppCells&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;

  bool use2D_{false};                 ///< Flag to enable 2D simulation.
  bool useSignaling_{false};          ///< Flag to enable signaling.
  bool useConstantSignaling_{false};  ///< Flag to enable signaling.
  bool useContactInhibition_{false};  ///< Flag to enable contact inhibition
  bool useDivision_{false};           ///< Flag to enable cell division.
  bool useOrientation_{false};        ///< Flag to enable cell orientation. (Motility)
  bool usePolarity_{false};           ///< Flag to enable cell polarity.
  bool useECMDeg_{false};             ///< Flag to enable ecm degradation
  bool useLogger_{false};             ///< Flag to enable logging of cell properties
};
