/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

using namespace nastja::config::literals;

class AppHeat : public nastja::Application {
public:
  explicit AppHeat(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
    /// @key{Settings.loopvariant, int, 0}
    /// Chooses the loop variant.
    variant_ = simdata.getConfig().getValue<int>("Settings.loopvariant"_jptr, 0);
  }
  ~AppHeat() override         = default;
  AppHeat(const AppHeat&)     = delete;
  AppHeat(AppHeat&&) noexcept = default;
  AppHeat& operator=(const AppHeat&) = delete;
  AppHeat& operator=(AppHeat&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;

  int variant_{0};
};
