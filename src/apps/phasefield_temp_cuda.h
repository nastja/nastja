/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

class AppPhasefieldTemperatureCuda : public nastja::Application {
public:
  explicit AppPhasefieldTemperatureCuda(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
  }
  ~AppPhasefieldTemperatureCuda() override                          = default;
  AppPhasefieldTemperatureCuda(const AppPhasefieldTemperatureCuda&) = delete;
  AppPhasefieldTemperatureCuda(AppPhasefieldTemperatureCuda&&)      = default;
  AppPhasefieldTemperatureCuda& operator=(const AppPhasefieldTemperatureCuda&) = delete;
  AppPhasefieldTemperatureCuda& operator=(AppPhasefieldTemperatureCuda&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;
};
