/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "phasefield_temp.h"
#include "lib/action/border.h"
#include "lib/stencil/D3C27.h"
#include "sweeps/sweep_pftemp.h"
#include "sweeps/sweep_temp.h"
#include <memory>

using namespace nastja;

void AppPhasefieldTemperature::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("phasefield", stencil::D3C27{}, field::FieldType::real);
  fieldpool.registerEmplace("temperature", stencil::D3C27{}, field::FieldType::real);
}

void AppPhasefieldTemperature::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  actionpool.registerElement("Sweep:Phi", std::make_unique<SweepPhaseFieldTemperature>());
  actionpool.registerElement("Sweep:Temp", std::make_unique<SweepTemperature>());
  actionpool.registerElement("BC:Phi_open", std::make_unique<Border>("phasefield", BorderType::open));
  actionpool.registerElement("BC:Phi_send", std::make_unique<Border>("phasefield", BorderType::send));
  actionpool.registerElement("BC:Phi_finalize", std::make_unique<Border>("phasefield", BorderType::finalize));
  actionpool.registerElement("BC:Temp_open", std::make_unique<Border>("temperature", BorderType::open));
  actionpool.registerElement("BC:Temp_send", std::make_unique<Border>("temperature", BorderType::send));
  actionpool.registerElement("BC:Temp_finalize", std::make_unique<Border>("temperature", BorderType::finalize));

  registerWriterElements();
}

void AppPhasefieldTemperature::registerActionList() {
  blockmanager.registerAction("BC:Phi_open");
  blockmanager.registerAction("Sweep:Phi");
  blockmanager.registerAction("BC:Phi_send");

  // pull this block forward, add an offset and add it to the post loop actions
  blockmanager.registerAction("BC:Temp_finalize", 1, true);
  blockmanager.registerActionWriters("temperature", 1, true);
  blockmanager.registerAction("Rotate:temperature", 1, true);

  blockmanager.registerAction("BC:Temp_open");
  blockmanager.registerAction("Sweep:Temp");
  blockmanager.registerAction("BC:Temp_send");

  blockmanager.registerAction("BC:Phi_finalize");

  blockmanager.registerActionWriters();

  blockmanager.registerAction("Rotate:phasefield");
}

bool AppPhasefieldTemperature::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();

  blockmanager.executeActionWriterOnBlocks();

  // Only rotate Phi because Temp is rotated in the execution loop before the sweep is executed.
  blockmanager.executeActionOnBlocks("Rotateback:phasefield");

  return false;
}
