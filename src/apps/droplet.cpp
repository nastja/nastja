/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "droplet.h"
#include "lib/action/actionregister.h"
#include "lib/action/borderdetection.h"
#include "lib/action/dynamicblock.h"
#include "lib/action/loadbalance.h"
#include "lib/stencil/D3C27.h"
#include "sweeps/sweep_pf-vp.h"

using namespace nastja;

void AppDroplet::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("phasefield", stencil::D3C27{}, field::FieldType::real);
}

void AppDroplet::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  // actionpool.registerElement("Sweep:Phi", std::make_unique<SweepPhaseField>());
  actionpool.registerElement("Sweep:Phi", std::make_unique<SweepPfVp>());
  actionpool.registerElement("BorderDetection:Phasefield", std::make_unique<BorderDetection>("phasefield", 26));
  // actionpool.registerElement("Reader:voxels", std::make_unique<Voxelizer>("phasefield"));
  actionpool.registerElement("DynBlock", std::make_unique<DynamicBlock>(blockmanager, 26));
  actionpool.registerElement("LoadBalance", std::make_unique<LoadBalance>(blockmanager));
  registerWriterElements();
}

void AppDroplet::registerActionList() {
  blockmanager.registerAction("Sweep:Phi");

  if (useDynamic_) {
    blockmanager.registerAction("BorderDetection:Phasefield");
    blockmanager.registerAction("DynBlock");
    blockmanager.registerAction("LoadBalance");
  }

  blockmanager.registerAction("BC:phasefield");

  blockmanager.registerActionWriters();

  blockmanager.registerAction("Rotate:phasefield");
}

bool AppDroplet::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();
  blockmanager.executeActionWriterOnBlocks();

  blockmanager.executeActionOnBlocks("Rotateback:phasefield");

  return false;
}
