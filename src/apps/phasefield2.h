/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

/**
 * Class for application phasefield demo with a field of vector legth of two.
 *
 * The second vector component is zero.
 */
class AppPhasefield2 : public nastja::Application {
public:
  explicit AppPhasefield2(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
  }
  ~AppPhasefield2() override                = default;
  AppPhasefield2(const AppPhasefield2&)     = delete;
  AppPhasefield2(AppPhasefield2&&) noexcept = default;
  AppPhasefield2& operator=(const AppPhasefield2&) = delete;
  AppPhasefield2& operator=(AppPhasefield2&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;
};
