/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

class AppSleep : public nastja::Application {
public:
  explicit AppSleep(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {}

  ~AppSleep() override          = default;
  AppSleep(const AppSleep&)     = delete;
  AppSleep(AppSleep&&) noexcept = default;
  AppSleep& operator=(const AppSleep&) = delete;
  AppSleep& operator=(AppSleep&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;
};
