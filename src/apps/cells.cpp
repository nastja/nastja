/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cells.h"
#include "lib/action/actionregister.h"
#include "lib/action/activecells.h"
#include "lib/action/cellcleaner.h"
#include "lib/action/cellcontactinhibition.h"
#include "lib/action/celldivision.h"
#include "lib/action/cellorientation.h"
#include "lib/action/cellmajoraxis.h"
#include "lib/action/centerofmass.h"
#include "lib/action/exchangecontainer.h"
#include "lib/action/signaling.h"
#include "lib/action/logcellproperties.h"
#include "lib/stencil/D3C27.h"
#include "sweeps/sweep_cells.h"
#include "sweeps/sweep_ecmdegrade.h"
#include "sweeps/sweep_signaldiffusion.h"
#include <memory>

using namespace nastja;

void AppCells::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("cells", stencil::D3C27{}, field::FieldType::integer, 1, 1);
}

void AppCells::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  if (use2D_) {
    if (useSignaling_) {
      actionpool.registerElement("Sweep:SignalDiffusion", std::make_unique<SweepSignalDiffusion<2>>());
    }
    actionpool.registerElement("Sweep:Cells", std::make_unique<SweepCells<2>>());
    actionpool.registerElement("Clean:Cells", std::make_unique<CellCleaner<2>>("cells"));
    if (useDivision_) {
      actionpool.registerElement("Divide:Cells", std::make_unique<CellDivision<2>>("cells"));
    }
  } else {
    if (useSignaling_) {
      actionpool.registerElement("Sweep:SignalDiffusion", std::make_unique<SweepSignalDiffusion<>>());
    }
    actionpool.registerElement("Sweep:Cells", std::make_unique<SweepCells<>>());
    actionpool.registerElement("Clean:Cells", std::make_unique<CellCleaner<>>("cells"));

    if (useContactInhibition_){
      actionpool.registerElement("ContactInhibit:Cells", std::make_unique<CellContactInhibition>("cells"));
    }
    if (useDivision_) {
      actionpool.registerElement("Divide:Cells", std::make_unique<CellDivision<>>("cells"));
    }
  }
  if (useSignaling_) {
    actionpool.registerElement("Signaling", std::make_unique<Signaling>());
  }
  if (useOrientation_) {
    actionpool.registerElement("CellOrientation", std::make_unique<CellOrientation>("cells"));
  }
  if (usePolarity_) {
    actionpool.registerElement("CellPolarity", std::make_unique<CellMajorAxis>("cells"));
  }  
  actionpool.registerElement("ExchangeContainer", std::make_unique<ExchangeContainer>("cells"));
  actionpool.registerElement("ActiveCellsDetection", std::make_unique<ActiveCellsDetection>("cells"));
  actionpool.registerElement("CenterOfMass", std::make_unique<CenterOfMass>("cells"));

  if (useECMDeg_) {
    actionpool.registerElement("ECMDegrade", std::make_unique<EcmDegradation>("cells"));
  }

  if (useLogger_) {
    actionpool.registerElement("LogCellProperties", std::make_unique<LogCellProperties>("cells"));
  }


  registerWriterElements();
}

void AppCells::registerActionList() {
  if (useSignaling_) {
    if (!useConstantSignaling_) {
      blockmanager.registerAction("Sweep:SignalDiffusion");
    }
    blockmanager.registerAction("Signaling");
  }
  blockmanager.registerAction("Sweep:Cells");
  if (useECMDeg_) {
    blockmanager.registerAction("ECMDegrade");
  }
  blockmanager.registerAction("BC:cells");

  blockmanager.registerAction("Clean:Cells");

  if(useLogger_){
    blockmanager.registerAction("LogCellProperties");
  }

  if (useDivision_) {
    blockmanager.registerAction("Divide:Cells");
  }
  blockmanager.registerAction("ActiveCellsDetection");  //TODO: this could be used for hide the exchange
  blockmanager.registerAction("CenterOfMass");
  blockmanager.registerAction("ExchangeContainer");
  
  if (useContactInhibition_){
    blockmanager.registerAction("ContactInhibit:Cells");
  }
  if (useOrientation_) {
    blockmanager.registerAction("CellOrientation");
  }
  if (usePolarity_) {
    blockmanager.registerAction("CellPolarity");
  }  

  blockmanager.registerActionWriters();
    
}

bool AppCells::init() {
  if (Application::init()) return true;

  blockmanager.executeActionOnBlocks("ActiveCellsDetection");  // Detects new cells in the halo.
  blockmanager.executeActionOnBlocks("ExchangeContainer");     // Exchange additional cell data about theses cells.
  MPI_Barrier(MPI_COMM_WORLD);                                 // Avoid intereferences with the next ExchangeContainer.
  blockmanager.setupActions();                                 // Setups action/sweep.
  blockmanager.executeActionOnBlocks("CenterOfMass");          // Calculate the cell centers.
  blockmanager.executeActionOnBlocks("ExchangeContainer");     // Exchange initialized additional cell data. Must be a full exchange.
  blockmanager.executeActionWriterOnBlocks();                  // Write out filling when requested.

  return false;
}
