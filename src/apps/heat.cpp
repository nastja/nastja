/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "heat.h"
#include "lib/action/actionregister.h"
#include "lib/field/fieldproperties.h"
#include "lib/io/vtkimage.h"
#include "lib/io/vtkimageread.h"
#include "lib/stencil/D3C27.h"
#include "sweeps/sweep_heat.h"
#include "sweeps/sweep_heat_avx.h"
#include "sweeps/sweep_heat_iter.h"
#include <memory>
#ifdef USE_CUDA
#include "sweeps/sweep_heat_cuda.h"
#endif

using namespace nastja;

void AppHeat::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  if (variant_ == 3) {
    fieldpool.registerEmplace("temperature", stencil::D3C27{}, field::FieldType::real | field::FieldType::cuda);
  } else {
    int vectorLength = 1;
    unsigned int ft  = field::FieldType::real;

    switch (variant_) {
      case 2:
        ft |= field::FieldType::aligned;
        break;
      case 4:
        vectorLength = 2;
        ft |= field::FieldType::splitX;
        break;
      default:
        break;
    }

    fieldpool.registerEmplace("temperature", stencil::D3C27{}, ft, vectorLength);
  }
}

void AppHeat::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  actionpool.registerElement("Sweep:Temperature", std::make_unique<SweepHeat>());
  actionpool.registerElement("Sweep:TemperatureIter", std::make_unique<SweepHeatIter>());
  actionpool.registerElement("Sweep:TemperatureAVX", std::make_unique<SweepHeatAVX>());
#ifdef USE_CUDA
  actionpool.registerElement("Sweep:TemperatureCuda", std::make_unique<SweepHeatCuda>());
#endif
  actionpool.registerElement("Read", std::make_unique<VtkImageReader>("temperature"));
  registerWriterElements();
}

void AppHeat::registerActionList() {
  switch (variant_) {
    case 0:
      blockmanager.registerAction("Sweep:Temperature");
      break;
    case 1:
      blockmanager.registerAction("Sweep:TemperatureIter");
      break;
    case 2:
      blockmanager.registerAction("Sweep:TemperatureAVX");
      break;
    case 3:
      blockmanager.registerAction("Sweep:TemperatureCuda");
      break;
    case 4:
      blockmanager.registerAction("Sweep:TemperatureAVX");
      break;
  }
  blockmanager.registerAction("BC:temperature");
  blockmanager.registerActionWriters();
  blockmanager.registerAction("Rotate:temperature");
}

bool AppHeat::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();

  blockmanager.executeActionWriterOnBlocks();

  blockmanager.executeActionOnBlocks("Rotateback:temperature");

  return false;
}
