/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

using namespace nastja::config::literals;

class AppPhasefield : public nastja::Application {
public:
  explicit AppPhasefield(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
    /// @key{Settings.usedynamic, bool, false}
    /// Flag enables the dynamic blocks.
    useDynamic_ = simdata.getConfig().getValue<bool>("Settings.usedynamic"_jptr, false);
  }
  ~AppPhasefield() override               = default;
  AppPhasefield(const AppPhasefield&)     = delete;
  AppPhasefield(AppPhasefield&&) noexcept = default;
  AppPhasefield& operator=(const AppPhasefield&) = delete;
  AppPhasefield& operator=(AppPhasefield&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;

  bool useDynamic_{false};  ///< Flag use the dynamic block actions.
};
