/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "pfc.h"
#include "lib/action/actionregister.h"
#include "lib/stencil/D3C1.h"
#include "sweeps/sweep_pfc.h"
#include <memory>

using namespace nastja;

void AppPFC::registerFields() {
  auto& fieldpool = blockmanager.getFieldPool();
  fieldpool.registerEmplace("psi", stencil::D3C1{}, field::FieldType::real);
  fieldpool.registerEmplace("psift", stencil::D3C1{}, field::FieldType::complex);
  fieldpool.registerEmplace("nlpsift", stencil::D3C1{}, field::FieldType::complex);
}

void AppPFC::registerActions() {
  auto& actionpool = blockmanager.getActionPool();
  if (use2D) {
    actionpool.registerElement("Sweep:PFC", std::make_unique<SweepPFC<2>>());
  } else {
    actionpool.registerElement("Sweep:PFC", std::make_unique<SweepPFC<3>>());
  }
  registerWriterElements();
}

void AppPFC::registerActionList() {
  blockmanager.registerAction("Sweep:PFC");
  blockmanager.registerActionWriters();
}

bool AppPFC::init() {
  if (Application::init()) return true;

  blockmanager.setupActions();
  blockmanager.executeActionWriterOnBlocks();

  return false;
}
