/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"

using namespace nastja::config::literals;

class AppPFC : public nastja::Application {
public:
  explicit AppPFC(nastja::block::BlockManager& blockmanager, nastja::SimData& simdata)
      : Application{blockmanager, simdata} {
    /// @key{PhaseFieldCrystal.2D, bool, false}
    /// Flag enabels the 2D model.
    use2D = simdata.getConfig().getValue<bool>("PhaseFieldCrystal.2D"_jptr, false);
  }
  ~AppPFC() override        = default;
  AppPFC(const AppPFC&)     = delete;
  AppPFC(AppPFC&&) noexcept = default;
  AppPFC& operator=(const AppPFC&) = delete;
  AppPFC& operator=(AppPFC&&) = delete;

  bool init() override;

private:
  void registerFields() override;
  void registerActions() override;
  void registerActionList() override;

  bool use2D{false};  ///< Flag 2D simulation.
};
