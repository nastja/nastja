/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/pool.h"
#include "lib/action/actionregister.h"
#include "lib/action/border.h"
#include "lib/action/filling.h"
#include "lib/action/loggersync.h"
#include "lib/action/rotatefield.h"
#include "lib/action/undump.h"
#include "lib/block/blockmanager.h"
#include "lib/config/configutils.h"
#include "lib/io/factoryio.h"
#include "lib/io/writerreader.h"
#include "lib/simdata/simdata.h"
#include "fmt/format.h"
#include <memory>
#ifdef USE_CUDA
#include "lib/action/cudatransfer.h"
#include "lib/action/writecudaexchangebuffer.h"
#endif

namespace nastja {

class Application {
public:
  explicit Application(block::BlockManager& blockmanager, SimData& simdata)
      : blockmanager{blockmanager}, simdata{simdata} {
    resume = simdata.getCmdLine().resume;
  }

  virtual ~Application()              = default;
  Application(const Application&)     = delete;
  Application(Application&&) noexcept = default;
  Application& operator=(const Application&) = delete;
  Application& operator=(Application&&) = delete;

  /**
   * Set up the general application stuff.
   *
   * The fields are registered by the `registerFields()` function. Default actions are registered, specifically the
   * `Undump` action and for each field a `Rotate:field`, `Rotateback:field`, and `BC:field` action. The latter provides
   * the full boundary exchange action. Besides, a `Filling` action for all fields is added. Afterwards the
   * `registerActions()` function is called for application-specific actions.  Then the `registerActionList()` is called
   * to register the action for the time-step loop of the specific application. If the resume flag is activated, then
   * the `Undump` action is registered.
   */
  void setup() {
    // FieldPool
    registerFields();

    // ActionPool
    auto& actionPool = blockmanager.getActionPool();
    actionPool.registerElement("Undump", std::make_unique<Undump>());
    actionPool.registerElement("LoggerSync", std::make_unique<LoggerSync>());

    auto& fieldpool = blockmanager.getFieldPool();

    std::vector<std::string> v;
    for (const auto& field : fieldpool) {
      v.push_back(field.first);
      actionPool.registerElement("Rotate:" + field.first, std::make_unique<RotateField>(field.first));
      actionPool.registerElement("Rotateback:" + field.first, std::make_unique<RotateField>(field.first, true));
      if (field.second->hasHalo()) {
        actionPool.registerElement("BC:" + field.first, std::make_unique<Border>(field.first, BorderType::fullExchange));
      }
#ifdef USE_CUDA
      actionPool.registerElement("CUDA_cph2d_h2e:" + field.first, std::make_unique<CudaTransfer>(field.first, 1));
      actionPool.registerElement("CUDA_checkExchanges:" + field.first, std::make_unique<CudaTransfer>(field.first, 2));
#endif
    }
    actionPool.registerElement("Filling", std::make_unique<Filling>(v));

    registerActions();

    // ActionList
    registerActionList();
    if (resume) blockmanager.registerAction("Undump");
  }

  /**
   * Initialize general application stuff.
   *
   * After block initialization, `Undump` is performed if the resume flag is activated. If not, the `Filling` action
   * initializes the fields and performs a full exchange for each field.
   *
   * @return True if in resume mode.
   */
  virtual bool init() {
    blockmanager.initBlocks();

    if (resume) {
      blockmanager.executeActionOnBlocks("Undump");
      blockmanager.deregisterAction("Undump");
      return true;
    }

    blockmanager.executeActionOnBlocks("Filling");
    if (blockmanager.getActionPool().hasElement("Filling:post")) {
      blockmanager.executeActionOnBlocks("Filling:post");
    }

    // Full boundary exchange
    auto& fieldpool = blockmanager.getFieldPool();
    for (const auto& field : fieldpool) {
#ifdef USE_CUDA
      if (field.second->isCudaDatatype()) {
        // copy data filled on host to the device and fill on device exchange array
        blockmanager.executeActionOnBlocks("CUDA_cph2d_h2e:" + field.first);
      }
#endif

      if (field.second->hasHalo()) {
        blockmanager.executeActionOnBlocks("BC:" + field.first);
      }

#ifdef USE_CUDA
      if (field.second->isCudaDatatype()) {
        // check which sides are exchanged per field after first mpi border exchange
        blockmanager.executeActionOnBlocks("CUDA_checkExchanges:" + field.first);
      }
#endif
    }

    return false;
  };

protected:
  virtual void registerActions()    = 0;
  virtual void registerFields()     = 0;
  virtual void registerActionList() = 0;

  /**
   * Register all writer elements from the config file to the actionPool.
   */
  void registerWriterElements() {
    FactoryIO factoryIO;

    auto& config = simdata.getConfig();
    /// @key{WriteActions, string[]}
    /// This key defines the orders of the writers defined under Writers.
    auto writers_jptr = config::getJsonPointer("WriteActions");
    for (unsigned int writer = 0; writer < config.arraySize(writers_jptr); writer++) {
      auto writer_jptr = writers_jptr / writer;
      if (!config.isString(writer_jptr)) {
        throw std::invalid_argument(fmt::format("WriteAction '{}' must be a string, is '{}'.", writer_jptr));
      }
      auto name              = config.getValue<std::string>(writer_jptr);
      auto writerAction_jptr = config::getJsonPointer("Writers." + name);
      if (!config.contains(writerAction_jptr)) {
        throw std::invalid_argument(fmt::format("WriteAction '{}' is not defined in 'Writers'.", name));
      }

      /// @key{Writers.\template{writer}.field, string, ""}
      /// The field that this Writer uses.
      auto field = config.getValue<std::string>(writerAction_jptr / "field", "");

      // The writer property is defined in the config via the Writers.\template{writer=x} annotations
      auto actionWriter = config.factoryCreate(factoryIO, writerAction_jptr / "writer", "", field);
      if (dynamic_cast<Dump*>(actionWriter.get()) != nullptr) {
        actionWriter->setType(ActionType::iodump);
      } else {
        actionWriter->setType(ActionType::io);
      }
      blockmanager.getActionPool().registerElement(name, std::move(actionWriter));
    }

    blockmanager.registerAction("LoggerSync");
  }

  block::BlockManager& blockmanager;
  SimData& simdata;

  bool resume{false};
};

}  // namespace nastja
