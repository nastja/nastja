/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "evaluator.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include <algorithm>
#include <string>

namespace nastja {
namespace evaluate {

Evaluator::Evaluator() {
  symbolTable_.add_constants();  // pi, epsilon and inf
  expression_.register_symbol_table(symbolTable_);
  expression_.register_symbol_table(compositor_.symbol_table());
  symbolTable_.add_variable("Nx", domainSizeX_);
  symbolTable_.add_variable("Ny", domainSizeY_);
  symbolTable_.add_variable("Nz", domainSizeZ_);
  symbolTable_.add_variable("dt", deltat_);
  symbolTable_.add_variable("dx", deltax_);

  compositor_.add_auxiliary_symtab(symbolTable_);

  randomFunc_ = std::make_unique<RandomFunc<real_t>>();
  randomGlobalFunc_ = std::make_unique<RandomFunc<real_t>>();
  symbolTable_.add_function("rnd", *randomFunc_);
  symbolTable_.add_function("rndg", *randomGlobalFunc_);
}

inline std::string trim(const std::string& s) {
  auto wsFront = std::find_if_not(s.begin(), s.end(), [](int c) { return std::isspace(c); });
  return std::string(wsFront, std::find_if_not(s.rbegin(), std::string::const_reverse_iterator(wsFront), [](int c) { return std::isspace(c); }).base());
}

void Evaluator::addConstant(const std::string& name, const real_t value) {
  symbolTable_.add_constant(name, value);
  logger::get().oneinfo(fmt::format("Evaluator: Add constant '{}={}'.", name, value));
}

void Evaluator::addConstant(const std::string& expression) {
  std::size_t equal = expression.find('=');
  if (equal == std::string::npos) {
    throw std::invalid_argument("Invalid syntax.");
  }

  std::string name = trim(expression.substr(0, equal));
  std::string expr = expression.substr(equal + 1);

  real_t eval = evaluate(expr);
  if (!symbolTable_.add_constant(name, eval)) {
    throw std::invalid_argument(fmt::format("Can not readd constant '{}'.", name));
  }
  logger::get().oneinfo(fmt::format("Evaluator: Add constant '{}={}={}'.", name, expr, eval));
}

/**
 * Adds a variable.
 *
 * @note The reference to variable must stay valid.
 *
 * @param name      The name.
 * @param variable  The variable.
 */
void Evaluator::addVariable(const std::string& name, real_t& variable) {
  symbolTable_.add_variable(name, variable);
  logger::get().oneinfo(fmt::format("Evaluator: Add variable '{}'.", name));
}

void Evaluator::addFunction(const std::string& expression) {
  exprtk::function_compositor<real_t>::function function;
  std::size_t open  = expression.find('(');
  std::size_t close = expression.find(')');
  std::size_t equal = expression.find('=');

  if (open == std::string::npos || close == std::string::npos || equal == std::string::npos || open > close || close > equal) {
    throw std::invalid_argument(fmt::format("Invalid syntax '{}'.", expression));
  }
  std::string name = trim(expression.substr(0, open));
  std::string vars = expression.substr(open + 1, close - open - 1);
  std::string expr = expression.substr(equal + 1);

  auto varList = split(vars, ',');
  function.name(name);
  function.expression(expr);
  for (auto& var : varList) {
    function.var(trim(var));
  }

  if (symbolTable_.symbol_exists(name)) return;

  if (compositor_.add(function)) {
    logger::get().oneinfo(fmt::format("Evaluator: Add function '{}'.", expression));
  }
}

real_t Evaluator::evaluate(const std::string& expression_string) {
  compile_impl(expression_string, expression_);

  return expression_.value();
}

template <typename Out>
void Evaluator::split(const std::string& s, const char delimiter, Out result) const {
  std::stringstream ss;
  ss.str(s);
  std::string item;
  while (std::getline(ss, item, delimiter)) {
    *(result++) = item;
  }
}

std::vector<std::string> Evaluator::split(const std::string& s, const char delimiter) const {
  std::vector<std::string> elements;
  split(s, delimiter, std::back_inserter(elements));
  return elements;
}

}  // namespace evaluate
}  // namespace nastja
