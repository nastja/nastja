/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/evaluate/evaluator.h"
#include <string>
#include <vector>
#include "exprtk.hpp"

namespace nastja {
namespace evaluate {

/**
 * This class describes an expression storage.
 *
 * @tparam T  The data type of the underlaying field.
 */
template <typename T>
class ExpressionStorage {
public:
  /**
   * Constructs a new instance.
   *
   * @param simdata  The simdata.
   * @param infolog  The infolog string, parameter are `i` and `condition` form addCondition.
   */
  explicit ExpressionStorage(SimData& simdata, const std::string& infolog)
      : evaluator_(simdata.getConfig().getEvaluator()),
        extension_{evaluator_.initExtension<T>(simdata)},
        infolog_{infolog} {}

  void addCondition(unsigned int i, std::string condition) {
    if (i >= expressions_.size()) {
      expressions_.resize(i + 1);
    }

    logger::get().oneinfo(infolog_, i, condition);

    evaluator_.compile(condition, expressions_[i]);
  }

  template <typename... ARGs>
  void set(ARGs&&... args) { extension_.set(std::forward<ARGs>(args)...); }

  bool condition(unsigned int i) const {
    return value(i) != 0.0;
  }

  real_t value(unsigned int i) const {
    if (i >= expressions_.size()) return 0.0;
    return expressions_[i].value();
  }

private:
  using expression_t = exprtk::expression<real_t>;

  Evaluator& evaluator_;
  T& extension_;
  std::string infolog_;

  std::vector<expression_t> expressions_;
};

}  // namespace evaluate
}  // namespace nastja
