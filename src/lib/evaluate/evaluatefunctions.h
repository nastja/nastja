/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/random.h"
#include <exprtk.hpp>
#include <random>

namespace nastja {
namespace evaluate {

/**
 * Add uniform distributed random numbers to evaluator.
 *
 * rnd() gives a number between [0,1).
 * rnd(seed) first sets the seed, then returns a number between [0,1).
 *
 * @tparam T  The data type.
 */
template <typename T>
class RandomFunc : public exprtk::igeneric_function<T> {
public:
  using parameter_list_t = typename exprtk::igeneric_function<T>::parameter_list_t;
  using scalar_t         = typename exprtk::igeneric_function<T>::generic_type::scalar_view;

  explicit RandomFunc(unsigned long seed = std::mt19937_64::default_seed) : exprtk::igeneric_function<T>("Z|T"), defaultgenerator_{}, generator_{&defaultgenerator_} {
    generator_->seed(seed);
  }

  inline T operator()(const std::size_t& ps_index, parameter_list_t parameters) final {
    if (ps_index == 1) {
      scalar_t seed{(parameters[0])};
      generator_->seed(static_cast<unsigned long>(seed()));
    }

    return uniform_(*generator_);
  }

  void linkRandomGenerator(std::mt19937_64& randomGenerator) {
    generator_ = &randomGenerator;
  }

private:
  std::mt19937_64 defaultgenerator_;    ///< The Mersenne twister prime number generator.
  std::mt19937_64* generator_;          ///< The Mersenne twister prime number generator.
  math::UniformReal<T> uniform_{0, 1};  ///< The uniform distribution.
};

}  // namespace evaluate
}  // namespace nastja
