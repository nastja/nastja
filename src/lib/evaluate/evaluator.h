/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/evaluate/evaluatefunctions.h"
#include "lib/evaluate/extension.h"
#include "lib/storage/extensionstorage.h"
#include <memory>
#include <string>
#include <vector>
#include "exprtk.hpp"

namespace nastja {
namespace evaluate {

/**
 * This class describes an evaluator.
 *
 * The Evaluator holds `Nx`, `Ny`, `Nz` for the domainsize, `dt` and `dx` for the discretization step in time and space.
 * `rnd` provides a random generator with the same seed for each rank. `rndg` is a global one with different seeds.
 * `time` is the total simulation time, i.e., time-step * dt,
 */
class Evaluator {
public:
  explicit Evaluator();

  void addConstant(const std::string& name, real_t value);
  void addConstant(const std::string& expression);
  void addVariable(const std::string& name, real_t& variable);
  void addFunction(const std::string& expression);

  template <typename F>
  void addFunction(const std::string& name, F& function) {
    symbolTable_.add_function(name, function);
    logger::get().oneinfo(fmt::format("Evaluator: Add function '{}'.", name));
  }

  real_t evaluate(const std::string& expression_string);

  /**
   * Compile a expression from the expression string. The symbol table is added.
   *
   * @param expression_string  The expression string.
   * @param expression         The expression.
   */
  void compile(const std::string& expression_string, exprtk::expression<real_t>& expression) {
    expression.register_symbol_table(symbolTable_);
    expression.register_symbol_table(compositor_.symbol_table());
    compile_impl(expression_string, expression);
  }

  void setDomainVariables(const int Nx, const int Ny, const int Nz, const real_t dx, const real_t dt) {
    domainSizeX_ = Nx;
    domainSizeY_ = Ny;
    domainSizeZ_ = Nz;
    deltax_      = dx;
    deltat_      = dt;
  }

  void link(real_t& time, std::mt19937_64& local, std::mt19937_64& global) {
    symbolTable_.add_variable("time", time);
    randomFunc_->linkRandomGenerator(local);
    randomGlobalFunc_->linkRandomGenerator(global);
  }

  /**
   * Gets the evaluator extension.
   *
   * @tparam E  The extension type.
   *
   * @return Reference to the extension.
   */
  template <typename E, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  E& getExtension() {
    if (!extensions_.has<E>()) {
      auto type = std::type_index(typeid(E));
      throw std::runtime_error(fmt::format("The evaluator extension '{}' is not initialized. Use initExtension() before getExtension().", type.name()));
    }

    return extensions_.get<E>();
  }

  /**
   * Determines if the extension is initialized.
   *
   * @tparam E  The extension type.
   *
   * @return True if the extension is initialized, False otherwise.
   */
  template <typename E, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  bool hasExtension() const {
    return extensions_.has<E>();
  }

  /**
   * Initializes the extension.
   *
   * @param args  The packed arguments.
   *
   * @tparam E     The extension type.
   * @tparam ARGs  The parameter pack.
   *
   * @return Reference to the extension.
   */
  template <typename E, typename... ARGs, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  E& initExtension(ARGs&&... args) {
    return extensions_.init<E>(std::forward<ARGs>(args)...);
  }

private:
  template <typename Out>
  void split(const std::string& s, char delimiter, Out result) const;

  std::vector<std::string> split(const std::string& s, char delimiter) const;

  void compile_impl(const std::string& expression_string, exprtk::expression<real_t>& expression) {
    if (!parser_.compile(expression_string, expression)) {
      for (std::size_t i = 0; i < parser_.error_count(); ++i) {
        exprtk::parser_error::type error = parser_.get_error(i);
        logger::get().oneerror("\t{} in expression\n\t\t'{}'\n\t\t{:>{}}\n\t\t{} at position {}.",
                               error.diagnostic,
                               expression_string,
                               '^', 2 + static_cast<unsigned int>(error.token.position),
                               exprtk::parser_error::to_str(error.mode),
                               static_cast<unsigned int>(error.token.position));
      }
      throw std::invalid_argument("Invalid expression.");
    }
  }

  exprtk::expression<real_t> expression_;
  exprtk::parser<real_t> parser_;
  exprtk::symbol_table<real_t> symbolTable_;
  exprtk::function_compositor<real_t> compositor_;

  real_t domainSizeX_{};  ///< Variable for the domain size in x direction.
  real_t domainSizeY_{};  ///< Variable for the domain size in y direction.
  real_t domainSizeZ_{};  ///< Variable for the domain size in z direction.
  real_t deltax_{1.0};    ///< Variable for the deltax.
  real_t deltat_{1.0};    ///< Variable for the deltat.

  ExtensionStorage<Extension> extensions_;  ///< Evaluate extension storage.

  std::unique_ptr<RandomFunc<real_t>> randomFunc_{nullptr};
  std::unique_ptr<RandomFunc<real_t>> randomGlobalFunc_{nullptr};
};

}  // namespace evaluate
}  // namespace nastja
