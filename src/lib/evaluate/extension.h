/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace nastja {
namespace evaluate {

/**
 * This class describes the interface for evaluator extensions.
 */
class Extension {
public:
  virtual ~Extension() = default;
};

}  // namespace evaluate
}  // namespace nastja
