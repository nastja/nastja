/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/cell.h"
#include "lib/datatypes.h"
#include "lib/timing.h"
#include "lib/evaluate/evaluator.h"
#include "lib/evaluate/extension.h"
#include "lib/math/random.h"
#include "lib/simdata/datacells.h"
#include "lib/simdata/simdata.h"
#include <exprtk.hpp>
#include <random>
#include <regex>
#include <vector>

namespace nastja {
namespace evaluate {

/**
 * Access to the signal vector.
 *
 * @tparam T  The exprtk data type.
 */
template <typename T>
class Signal : public exprtk::ifunction<T> {
public:
  explicit Signal(std::array<real_t, numberOfSignals>& signal) : exprtk::ifunction<T>(1), signal_{signal} {}

  /**
   * Function call operator.
   *
   * @param s  The signal. Exptk handle floating point numbers, but s shoud be an interger, so it is only save for the
   *           mantisse bits.
   *
   * @return The value of the signal.
   */
  inline T operator()(const T& s) final {
    return signal_[s];
  }

private:
  std::array<real_t, numberOfSignals>& signal_;
};

/**
 * This class describes the cell  evaluator extension.
 */
class CellExtension : public Extension {
public:
  explicit CellExtension(SimData& simdata) : simdata_{simdata}, evaluator_(simdata.getConfig().getEvaluator()) {
    evaluator_.addVariable("volume", volume_);
    evaluator_.addVariable("volume0", volume0_);
    evaluator_.addVariable("surface", surface_);
    evaluator_.addVariable("surface0", surface0_);
    evaluator_.addVariable("age", age_);
    evaluator_.addVariable("generation", generation_);
    evaluator_.addVariable("inhibitionstrength", inhibitionStrength_);
    evaluator_.addFunction("signal", signalFunction_);

    simdata.getConfig().readDefineFunctions();
  }

  /**
   * Sets the evaluator extension variable from a cell.
   *
   * @param cell  The cell.
   */
  void set(const Cell& cell) {
    volume_     = cell.volume;
    surface_    = cell.surface;
    age_        = simdata_.timestep - cell.birth;
    signal_     = cell.signal;
    generation_ = cell.generation;
    inhibitionStrength_ = cell.inhibitionStrength;

    auto& data = simdata_.getData<DataCells>();
    volume0_   = data.defaultVolume->getStorage().getValue(cell.type);
    surface0_  = data.defaultSurface->getStorage().getValue(cell.type);
  }

private:
  real_t volume_{};                               ///< The volume.
  real_t volume0_{};                              ///< The target volume.
  real_t surface_{};                              ///< The surface.
  real_t surface0_{};                             ///< The target surface.
  real_t age_{};                                  ///< The age.
  real_t inhibitionStrength_{};                   ///< The inhibition strength
  std::array<real_t, numberOfSignals> signal_{};  ///< The signals.
  real_t generation_{};                     ///< The cell generation

  SimData& simdata_;      ///< The reference to the simdata.
  Evaluator& evaluator_;  ///< The reference to the evaluator.

  Signal<real_t> signalFunction_{signal_};  ///< The signal function.
};

}  // namespace evaluate
}  // namespace nastja
