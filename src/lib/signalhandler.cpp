/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "signalhandler.h"

namespace nastja {

// #pragma STDC FENV_ACCESS ON
#if defined(__APPLE__) && defined(__MACH__)
#ifndef __ARM_ARCH
// Public domain polyfill for feenableexcept on OS X
// http://www-personal.umich.edu/~williams/archive/computation/fe-handling-example.c

int feenableexcept(unsigned int excepts) {
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv)) {
    return -1;
  }
  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // unmask
  fenv.__control &= ~new_excepts;
  fenv.__mxcsr &= ~(new_excepts << 7);

  return fesetenv(&fenv) ? -1 : old_excepts;
}

int fedisableexcept(unsigned int excepts) {
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // all previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv)) {
    return -1;
  }
  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // mask
  fenv.__control |= new_excepts;
  fenv.__mxcsr |= new_excepts << 7;

  return fesetenv(&fenv) ? -1 : old_excepts;
}

#else

int fedisableexcept(unsigned int /*excepts*/) { 
  return 0; 
}

int feenableexcept(unsigned int /*excepts*/) {
  return 0;
}

#endif
#endif

volatile std::sig_atomic_t gSignalStatus = 0;

/**
 * Interrupt function sets the signal status, or exits if allready a signal is set
 *
 * @param signal  The signal.
 */
extern "C" void handler(int signal) {
  if (gSignalStatus > 0) {
    std::exit(0);
  }

  gSignalStatus = signal;
}

/**
 * Determines the last time-step.
 */
void SignalHandler::determineLastTimeStep() {
  int flag = 0;
  MPI_Test(&request_, &flag, MPI_STATUS_IGNORE);
  if (flag == 0) return;

  eeState_ = EmergencyExitState::determined;

  simdata_.setTimesteps(exitTimeStep_ + stepsOfTimeStepGuard_ + 1);
  logger::get().onewarning("Emergency exit mode determined the last time-step, the simulation will end on time-step {}.", simdata_.getTimesteps());
}

/**
 * Fetches signal and handles emergency exit mode.
 */
void SignalHandler::fetch() {
  if (doReportFPE_) {
    reportFPE();
  }

  if (eeState_ == EmergencyExitState::requested) {
    determineLastTimeStep();
    return;
  }

  if (eeState_ == EmergencyExitState::determined) return;

  if (gSignalStatus == 0) return;
  eeState_ = EmergencyExitState::requested;

  exitTimeStep_ = simdata_.timestep;

  MPI_Iallreduce(MPI_IN_PLACE, &exitTimeStep_, 1, MPI_UNSIGNED_LONG, MPI_MAX, emergencyComm_, &request_);

  std::string ctrlcmsg;
  if (simdata_.mpiData.getSize() <= 1) {
    ctrlcmsg = " Press CTRL+C again to stop immediately.";
  }
  logger::get().onewarning("Got signal '{}'({}) at time-step {}. Simulation goes in the emergency exit mode. Waiting for exit time-step.{}",
                           getSignalName(gSignalStatus), gSignalStatus, simdata_.timestep, ctrlcmsg);
}

}  // namespace nastja
