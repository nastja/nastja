/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include <map>
#include <memory>
#include <string>

namespace nastja {

/**
 * Factory Interface
 *
 * Use `TBase` for the base class and `ARGs` for arguments of the constructor.
 *
 * Usage:
 * ~~~
 * class FactoryIO: public Factory<WriterReader, const std::string&>
 * ~~~
 * where FactoryIO handles objects derived from
 * WriterReader and the constructor accept a const std::string&.
 *
 * See
 * - http://stackoverflow.com/a/26950454/2525525
 * - http://stackoverflow.com/a/29154449/2525525
 */
template <typename TBase, typename... ARGs>
class Factory {
public:
  /**
   * Register a type.
   *
   * @param key  The key.
   *
   * @tparam TDerived  The type.
   */
  template <typename TDerived>
  void registerType(const std::string& key) {
    static_assert(std::is_base_of<TBase, TDerived>::value,
                  "Factory::registerType does not accept this type because it is not derived from base class");
    createFunctions_[key] = std::make_unique<Creator<TDerived>>();
  }

  /**
   * Creates an unique pointer from the factory.
   *
   * @param key   The key.
   * @param args  The arguments.
   *
   * @return Retuns a unique pointer of the base type.
   */
  std::unique_ptr<TBase> create(const std::string& key, ARGs&&... args) const {
    auto it = createFunctions_.find(key);
    if (it == createFunctions_.end()) {
      throw std::invalid_argument(fmt::format("Element '{}' is unknown in factory.", key));
    }
    return (*it->second).create(std::forward<ARGs>(args)...);
  }

  /**
   * Creates a pointer from the factory.
   *
   * @param key   The key.
   * @param args  The arguments.
   *
   * @return Retuns a pointer of the base type.
   */
  TBase* create_ptr(const std::string& key, ARGs&&... args) const {
    auto it = createFunctions_.find(key);
    if (it == createFunctions_.end()) {
      throw std::invalid_argument(fmt::format("Element '{}' is unknown in factory.", key));
    }
    return (*it->second).create_ptr(std::forward<ARGs>(args)...);
  }

  /**
   * Determines if the factory is empty.
   *
   * @return True if empty, False otherwise.
   */
  bool isEmpty() { return createFunctions_.empty(); }

  std::string getNamesFormatted() const {
    std::stringstream result;

    std::for_each(std::begin(createFunctions_), std::end(createFunctions_),
                  [&result](const std::pair<const std::string, std::unique_ptr<ICreator>>& element) {
                    result << " * " << element.first << "\n";
                  });

    return result.str();
  }

private:
  class ICreator {
  public:
    ICreator()                    = default;
    virtual ~ICreator()           = default;
    ICreator(const ICreator&)     = delete;
    ICreator(ICreator&&) noexcept = default;
    ICreator& operator=(const ICreator&) = delete;
    ICreator& operator=(ICreator&&) = delete;

    virtual std::unique_ptr<TBase> create(ARGs&&...) const = 0;
    virtual TBase* create_ptr(ARGs&&...) const             = 0;
  };

  template <typename TDerived>
  class Creator : public ICreator {
  public:
    std::unique_ptr<TBase> create(ARGs&&... args) const override {
      return std::unique_ptr<TBase>{new TDerived(std::forward<ARGs>(args)...)};
    }
    TBase* create_ptr(ARGs&&... args) const override {
      return new TDerived(std::forward<ARGs>(args)...);
    }
  };

  std::map<std::string, std::unique_ptr<ICreator>> createFunctions_;  ///< The map of create functions.
};

}  // namespace nastja
