/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/application.h"
#include "lib/factory.h"
#include "lib/logger.h"
#include "lib/signalhandler.h"
#include "lib/statusoutput.h"
#include "lib/timing.h"
#include "lib/block/blockmanager.h"
#include "lib/config/cmdline.h"
#include "lib/config/config.h"
#include "lib/io/writerreader.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

/**
 * NASTJA_SINGLE_APP can be used to generate a singel application using the NAStJA libary and your own sweeps.
 *
 * Just include the lib/nastja.h header and create a standard application.
 *
 * @param app  The application.
 */
#ifdef NASTJA_MULTI_APPLICATION
#define NASTJA_SINGLE_APP(app)
#else
#define NASTJA_SINGLE_APP(app)         \
  int main(int argc, char* argv[]) {   \
    nastja::NAStJA nastja{argc, argv}; \
    nastja.setApplication<app>();      \
    return nastja.run();               \
  }
#endif

namespace nastja {
using namespace config::literals;

class FactoryApps : public nastja::Factory<Application, nastja::block::BlockManager&, nastja::SimData&> {};

class NAStJA {
public:
  explicit NAStJA(int argc, char* argv[])
      : mpiData_{argc, argv},
        config_{argc, argv},
        simdata_{config_, mpiData_},
        blockmanager_{simdata_},
        statusOutput_{simdata_},
        signalHandler_{simdata_},
        timingPath_{simdata_.getCmdLine().outdir + "/timing/"},
        application_{nullptr},
        defaultApplication_{"Phasefield"} {
    auto& logger = logger::get();
    logger.registerTimeStepPtr(&(simdata_.timestep));

    WriterReader::checkPath(timingPath_);
  }

  ~NAStJA() {
    logger::closeAll();
  }

  /**
   * Sets the application.
   *
   * @tparam T  The application.
   */
  template <typename T>
  void setApplication() {
    application_ = std::make_unique<T>(blockmanager_, simdata_);
  }

  /**
   * Gets the factory.
   *
   * @return The factory.
   */
  FactoryApps& getFactory() { return factory_; }

  int run();

private:
  MPIData mpiData_;                   ///< The MPI data object initializes the MPI environment.
  config::Config config_;             ///< The configuration reads the command line and the configure file. It stores all configure keys during initialization.
  SimData simdata_;                   ///< The simdata holds all simulation relevant data.
  block::BlockManager blockmanager_;  ///< The block manager holds all local blocks and runs the action on them.
  StatusOutput statusOutput_;         ///< The status output print the simulation time during simulation.
  SignalHandler signalHandler_;       ///< The signal handler listens to system signals and handles them.
  FactoryApps factory_;               ///< The factory holding apllications.

  std::string timingPath_;                    ///< The path to the timing report, it is created on the start.
  std::unique_ptr<Application> application_;  ///< The pointer to the application that will run.
  std::string defaultApplication_;            ///< The name of the default application.
};

}  // namespace nastja
