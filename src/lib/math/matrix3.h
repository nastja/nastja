/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/vector.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <type_traits>

namespace nastja {
namespace math {

/**
 * Class for Matrix3, a three dimensional vector.
 *
 * @tparam T  Type of the vector elements.
 */
template <typename T>
class Matrix3 {
  static_assert(std::is_arithmetic<T>::value, "The Matrix3 class needs an arithmetic data type.");

public:
  /// Define the type of the length, this can not be an integral type because of the sqrt.
  using length_type = typename std::conditional<std::is_integral<T>::value, real_t, T>::type;

  /// @name Constructors
  /// @{
  explicit Matrix3() : m_{T(), T(), T(), T(), T(), T(), T(), T(), T()} {}
  explicit Matrix3(T initial) : m_{initial, initial, initial, initial, initial, initial, initial, initial, initial} {}
  explicit Matrix3(const Vector3<T>& a, const Vector3<T>& b, const Vector3<T>& c) : m_{a[0], b[0], c[0], a[1], b[1], c[1], a[2], b[2], c[2]} {}
  explicit Matrix3(T xx, T xy, T xz, T yx, T yy, T yz, T zx, T zy, T zz) : m_{xx, xy, xz, yx, yy, yz, zx, zy, zz} {}
  explicit Matrix3(const T* initial) : m_{initial[0], initial[1], initial[2], initial[3], initial[4], initial[5], initial[6], initial[7], initial[8]} {}

  template <typename U>
  explicit Matrix3(const Matrix3<U>& m) : m_{T(m[0]), T(m[1]), T(m[2]), T(m[3]), T(m[4]), T(m[5]), T(m[6]), T(m[7]), T(m[8])} {}

  template <typename U>
  explicit Matrix3(U alpha, U beta, U gamma) {
    setRotation(alpha, beta, gamma);
  }

  template <typename U, typename V>
  explicit Matrix3(Vector3<U> axis, V theta) {
    setRotation(axis, theta);
  }
  /// @}

  /// @name Operators
  /// @{
  template <typename U>
  Matrix3<typename std::common_type<T, U>::type> operator+(const Matrix3<U>& other) const {
    return Matrix3<typename std::common_type<T, U>::type>(m_[0] + other[0], m_[1] + other[1], m_[2] + other[2],
                                                          m_[3] + other[3], m_[4] + other[4], m_[5] + other[5],
                                                          m_[6] + other[6], m_[7] + other[7], m_[8] + other[8]);
  }

  template <typename U>
  Matrix3<typename std::common_type<T, U>::type> operator-(const Matrix3<U>& other) const {
    return Matrix3<typename std::common_type<T, U>::type>(m_[0] - other[0], m_[1] - other[1], m_[2] - other[2],
                                                          m_[3] - other[3], m_[4] - other[4], m_[5] - other[5],
                                                          m_[6] - other[6], m_[7] - other[7], m_[8] - other[8]);
  }

  Matrix3 operator-() const {
    return Matrix3(-m_[0], -m_[1], -m_[2],
                   -m_[3], -m_[4], -m_[5],
                   -m_[6], -m_[7], -m_[8]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix3<typename std::common_type<T, U>::type> operator*(U scalar) const {
    return Matrix3<typename std::common_type<T, U>::type>(m_[0] * scalar, m_[1] * scalar, m_[2] * scalar,
                                                          m_[3] * scalar, m_[4] * scalar, m_[5] * scalar,
                                                          m_[6] * scalar, m_[7] * scalar, m_[8] * scalar);
  }

  template <typename U>
  Vector3<typename std::common_type<T, U>::type> operator*(const Vector3<U>& other) const {
    return Vector3<typename std::common_type<T, U>::type>{m_[0] * other[0] + m_[1] * other[1] + m_[2] * other[2],
                                                          m_[3] * other[0] + m_[4] * other[1] + m_[5] * other[2],
                                                          m_[6] * other[0] + m_[7] * other[1] + m_[8] * other[2]};
  }

  template <typename U>
  Vector3<typename std::common_type<T, U>::type> multiplyT(const Vector3<U>& other) const {
    return Vector3<typename std::common_type<T, U>::type>{m_[0] * other[0] + m_[3] * other[1] + m_[6] * other[2],
                                                          m_[1] * other[0] + m_[4] * other[1] + m_[7] * other[2],
                                                          m_[2] * other[0] + m_[5] * other[1] + m_[8] * other[2]};
  }

  template <typename U>
  Matrix3<typename std::common_type<T, U>::type> operator*(const Matrix3<U>& other) const {
    return Matrix3<typename std::common_type<T, U>::type>(m_[0] * other[0] + m_[1] * other[3] + m_[2] * other[6],
                                                          m_[0] * other[1] + m_[1] * other[4] + m_[2] * other[7],
                                                          m_[0] * other[2] + m_[1] * other[5] + m_[2] * other[8],
                                                          m_[3] * other[0] + m_[4] * other[3] + m_[5] * other[6],
                                                          m_[3] * other[1] + m_[4] * other[4] + m_[5] * other[7],
                                                          m_[3] * other[2] + m_[4] * other[5] + m_[5] * other[8],
                                                          m_[6] * other[0] + m_[7] * other[3] + m_[8] * other[6],
                                                          m_[6] * other[1] + m_[7] * other[4] + m_[8] * other[7],
                                                          m_[6] * other[2] + m_[7] * other[5] + m_[8] * other[8]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix3<typename std::common_type<T, U>::type> operator/(U scalar) const {
    return Matrix3<typename std::common_type<T, U>::type>(m_[0] / scalar, m_[1] / scalar, m_[2] / scalar,
                                                          m_[3] / scalar, m_[4] / scalar, m_[5] / scalar,
                                                          m_[6] / scalar, m_[7] / scalar, m_[8] / scalar);
  }

  template <typename U>
  Matrix3& operator+=(const Matrix3<U>& other) {
    m_[0] += other[0];
    m_[1] += other[1];
    m_[2] += other[2];
    m_[3] += other[3];
    m_[4] += other[4];
    m_[5] += other[5];
    m_[6] += other[6];
    m_[7] += other[7];
    m_[8] += other[8];
    return *this;
  }

  template <typename U>
  Matrix3& operator-=(const Matrix3<U>& other) {
    m_[0] -= other[0];
    m_[1] -= other[1];
    m_[2] -= other[2];
    m_[3] -= other[3];
    m_[4] -= other[4];
    m_[5] -= other[5];
    m_[6] -= other[6];
    m_[7] -= other[7];
    m_[8] -= other[8];
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix3& operator*=(U scalar) {
    m_[0] *= scalar;
    m_[1] *= scalar;
    m_[2] *= scalar;
    m_[3] *= scalar;
    m_[4] *= scalar;
    m_[5] *= scalar;
    m_[6] *= scalar;
    m_[7] *= scalar;
    m_[8] *= scalar;
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix3& operator/=(U scalar) {
    m_[0] /= scalar;
    m_[1] /= scalar;
    m_[2] /= scalar;
    m_[3] /= scalar;
    m_[4] /= scalar;
    m_[5] /= scalar;
    m_[6] /= scalar;
    m_[7] /= scalar;
    m_[8] /= scalar;
    return *this;
  }

  template <typename U>
  Matrix3& operator*=(const Matrix3<U>& other) {
    Matrix3 tmp(m_[0] * other[0] + m_[1] * other[3] + m_[2] * other[6],
                m_[0] * other[1] + m_[1] * other[4] + m_[2] * other[7],
                m_[0] * other[2] + m_[1] * other[5] + m_[2] * other[8],
                m_[3] * other[0] + m_[4] * other[3] + m_[5] * other[6],
                m_[3] * other[1] + m_[4] * other[4] + m_[5] * other[7],
                m_[3] * other[2] + m_[4] * other[5] + m_[5] * other[8],
                m_[6] * other[0] + m_[7] * other[3] + m_[8] * other[6],
                m_[6] * other[1] + m_[7] * other[4] + m_[8] * other[7],
                m_[6] * other[2] + m_[7] * other[5] + m_[8] * other[8]);
    return this->operator=(tmp);
  }

  template <typename U>
  bool operator==(const Matrix3<U>& other) const {
    return m_[0] == other[0] && m_[1] == other[1] && m_[2] == other[2] &&
           m_[3] == other[3] && m_[4] == other[4] && m_[5] == other[5] &&
           m_[6] == other[6] && m_[7] == other[7] && m_[8] == other[8];
  }

  template <typename U>
  bool operator!=(const Matrix3<U>& other) const {
    return m_[0] != other[0] || m_[1] != other[1] || m_[2] != other[2] ||
           m_[3] != other[3] || m_[4] != other[4] || m_[5] != other[5] ||
           m_[6] != other[6] || m_[7] != other[7] || m_[8] != other[8];
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator==(U scalar) const {
    return m_[0] == scalar && m_[1] == scalar && m_[2] == scalar &&
           m_[3] == scalar && m_[4] == scalar && m_[5] == scalar &&
           m_[6] == scalar && m_[7] == scalar && m_[8] == scalar;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator!=(U scalar) const {
    return m_[0] != scalar || m_[1] != scalar || m_[2] != scalar ||
           m_[3] != scalar || m_[4] != scalar || m_[5] != scalar ||
           m_[6] != scalar || m_[7] != scalar || m_[8] != scalar;
  }
  /// @}

  /// @name Element access
  /// @{
  T& operator[](unsigned int index) {
    return m_[index];
  }

  const T& operator[](unsigned int index) const {
    return m_[index];
  }

  T& operator()(unsigned int i, unsigned int j) {
    return m_[i * 3 + j];
  }

  const T& operator()(unsigned int i, unsigned int j) const {
    return m_[i * 3 + j];
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  T* data() noexcept {
    return m_.data();
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  const T* data() const noexcept {
    return m_.data();
  }

  /**
   * Sets the matrix.
   *
   * @param xx  Element 0, 0.
   * @param xy  Element 0, 1.
   * @param xz  Element 0, 2.
   * @param yx  Element 1, 0.
   * @param yy  Element 1, 1.
   * @param yz  Element 1, 2.
   * @param zx  Element 2, 0.
   * @param zy  Element 2, 1.
   * @param zz  Element 2, 2.
   */
  void set(T xx, T xy, T xz, T yx, T yy, T yz, T zx, T zy, T zz) {
    m_[0] = xx;
    m_[1] = xy;
    m_[2] = xz;
    m_[3] = yx;
    m_[4] = yy;
    m_[5] = yz;
    m_[6] = zx;
    m_[7] = zy;
    m_[8] = zz;
  }

  /**
   * Sets the rotation.
   *
   * @param alpha  The alpha angle.
   * @param beta   The beta angle.
   * @param gamma  The gamma angle.
   */
  template <typename U>
  Matrix3<T> setRotation(U alpha, U beta, U gamma) {
    m_[0] = std::cos(beta) * std::cos(gamma);
    m_[1] = std::sin(alpha) * std::sin(beta) * std::cos(gamma) - std::cos(alpha) * std::sin(gamma);
    m_[2] = std::cos(alpha) * std::sin(beta) * std::cos(gamma) + std::sin(alpha) * std::sin(gamma);

    m_[3] = std::cos(beta) * std::sin(gamma);
    m_[4] = std::sin(alpha) * std::sin(beta) * std::sin(gamma) + std::cos(alpha) * std::cos(gamma);
    m_[5] = std::cos(alpha) * std::sin(beta) * std::sin(gamma) - std::sin(alpha) * std::cos(gamma);

    m_[6] = -std::sin(beta);
    m_[7] = std::sin(alpha) * std::cos(beta);
    m_[8] = std::cos(alpha) * std::cos(beta);

    return *this;
  }

  /**
   * Sets the rotation for a rotation matrix rotating around a given axis.
   *
   * @param axis   The rotation axis.
   * @param theta  The rotation angle in radians.
   */
  template <typename U, typename V>
  Matrix3<T> setRotation(Vector3<U> axis, V theta) {
    auto v = axis.normalize();

    auto c = std::cos(theta);
    auto s = std::sin(theta);
    auto C = 1.0 - c;

    m_[0] = v.x() * v.x() * C + c;
    m_[1] = v.x() * v.y() * C - v.z() * s;
    m_[2] = v.x() * v.z() * C + v.y() * s;

    m_[3] = v.y() * v.x() * C + v.z() * s;
    m_[4] = v.y() * v.y() * C + c;
    m_[5] = v.y() * v.z() * C - v.x() * s;

    m_[6] = v.z() * v.x() * C - v.y() * s;
    m_[7] = v.z() * v.y() * C + v.x() * s;
    m_[8] = v.z() * v.z() * C + c;

    return *this;
  }
  /// @}

private:
  std::array<T, 9> m_;  ///< Array of length nine, holding the 3x3 matrix with element type T.
};

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param m   The matrix.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const Matrix3<T>& m) {
  return os << "[[" << m[0] << "," << m[1] << "," << m[2] << "]"
            << "\n"
            << " [" << m[3] << "," << m[4] << "," << m[5] << "]"
            << "\n"
            << " [" << m[6] << "," << m[7] << "," << m[8] << "]]";
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator==(U scalar, const Matrix3<T>& v) {
  return v == scalar;
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator!=(U scalar, const Matrix3<T>& v) {
  return v != scalar;
}

/**
 * Scalar multiplication
 *
 * @param scalar  The scalar.
 * @param m       The matrix.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return The matrix scalar product.
 */
template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Matrix3<T> operator*(U scalar, const Matrix3<T>& m) {
  return m * scalar;
}

}  // namespace math
}  // namespace nastja
