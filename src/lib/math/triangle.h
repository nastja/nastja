/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/math/vector.h"

namespace nastja {
namespace math {

class Triangle {
public:
  /**
   * Calculates the normal.
   *
   * @param normalInvertion  Flag for invert the normal.
   * @param normalize        Flag for normalize.
   *
   * @return True, if it is a valid triangle, false else.
   */
  bool calcNormal(bool normalInvertion = false, bool normalize = false) {
    const Vector3<real_t> v1 = p[1] - p[0];
    const Vector3<real_t> v2 = p[2] - p[0];

    n = crossProduct(v1, v2);

    if (n.length() == 0.0) return false;

    if (normalInvertion) invertNormal();

    if (normalize) n.normalize();

    return true;
  }

  void normalize() {
    n.normalize();
  }

  void invertNormal() {
    n *= -1.0;
  }

  real_t getDistance(const int x, const int y, const int z) const {
    return (x - p[2][0]) * n[0] + (y - p[2][1]) * n[1] + (z - p[2][2]) * n[2];
  }

  real_t dotProductNormal(const int x, const int y, const int z) const {
    return (x * n[0] + y * n[1] + z * n[2]);
  }

  std::array<Vector3<real_t>, 3> p;  ///< Array of the three edge points vectors building the triangle.
  Vector3<real_t> n;                 ///< The normal vector of the triangle.
};

}  // namespace math
}  // namespace nastja
