/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/calculatesurface.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/direction.h"

namespace nastja {
namespace math {

template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class Surface {
public:
  explicit Surface()          = default;
  virtual ~Surface()          = default;
  Surface(const Surface&)     = default;
  Surface(Surface&&) noexcept = default;
  Surface& operator=(const Surface&) = default;
  Surface& operator=(Surface&&) = default;

  virtual real_t operator()(const FieldView& view, T* ptr, T checkID)       = 0;
  virtual real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) = 0;
};

/**
 * This class describes the calculation of the surface by counting faces.
 * A face is counted if the cellID of the neighbor voxel differs.
 */
template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class SurfaceCount : public Surface<T, FieldView> {
public:
  explicit SurfaceCount()               = default;
  virtual ~SurfaceCount()               = default;
  SurfaceCount(const SurfaceCount&)     = default;
  SurfaceCount(SurfaceCount&&) noexcept = default;
  SurfaceCount& operator=(const SurfaceCount&) = default;
  SurfaceCount& operator=(SurfaceCount&&) = default;

  real_t operator()(const FieldView& view, T* ptr, T checkID) {
    if (view.get(ptr) != checkID) return 0.0;

    return math::calcSurfaceFaces(view, ptr, checkID);
  }

  real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) {
    if (view.get(ptr) == changeID) return 0.0;
    if (view.get(ptr) != checkID && changeID != checkID) return 0.0;

    int faces = 6 - 2 * math::calcSurfaceFaces(view, ptr, checkID);

    return checkID == changeID ? -faces : faces;
  }
};

/**
 * This class describes the calculation of the surface by approximation via marching cubes.
 */
template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class SurfaceMC : public Surface<T, FieldView> {
public:
  explicit SurfaceMC()            = default;
  virtual ~SurfaceMC()            = default;
  SurfaceMC(const SurfaceMC&)     = default;
  SurfaceMC(SurfaceMC&&) noexcept = default;
  SurfaceMC& operator=(const SurfaceMC&) = default;
  SurfaceMC& operator=(SurfaceMC&&) = default;

  real_t operator()(const FieldView& view, T* ptr, T checkID) {
    return calcCube(view, ptr, checkID, calcSurfaceMCLookup);
  }

  real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) {
    return calcStencilDiff(view, ptr, checkID, changeID, calcSurfaceMCLookup);
  }
};

}  // namespace math
}  // namespace nastja
