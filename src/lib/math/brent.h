/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <functional>

namespace nastja {
namespace math {

void bracket(const std::function<real_t(real_t)>& f, real_t& a, real_t& b, unsigned int Niter = 50, real_t FACTOR = 1.6);
real_t brent(const std::function<real_t(real_t)>& f, real_t a, real_t b, real_t t, unsigned int Niter = 500);

}  // namespace math
}  // namespace nastja
