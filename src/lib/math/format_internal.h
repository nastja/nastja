/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/datatypes.h"
#include "lib/math/boundingbox.h"
#include "lib/math/matrix2.h"
#include "lib/math/matrix3.h"
#include "lib/math/vector.h"
#include "fmt/format.h"
#include "fmt/ranges.h"

#ifndef NASTJA_FORMAT_IS_INCLUDED
#error This file is not for direct inclusion, use format.h instead.
#endif

namespace fmt {

template <>
struct formatter<nastja::math::Vector3<NASTJA_TYPE_SPECIFICATION>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::math::Vector3<NASTJA_TYPE_SPECIFICATION>& v, FormatContext& ctx) {
    return format_to(
        ctx.out(),
        "[{}]",
        fmt::join(v, ","));
  }
};

template <>
struct formatter<nastja::math::Vector2<NASTJA_TYPE_SPECIFICATION>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::math::Vector2<NASTJA_TYPE_SPECIFICATION>& v, FormatContext& ctx) {
    return format_to(
        ctx.out(),
        "[{}]",
        fmt::join(v, ","));
  }
};

template <>
struct formatter<nastja::math::BoundingBox<NASTJA_TYPE_SPECIFICATION>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::math::BoundingBox<NASTJA_TYPE_SPECIFICATION>& b, FormatContext& ctx) {
    return format_to(
        ctx.out(),
        "[{},{}]",
        b.lower(), b.upper());
  }
};

template <>
struct formatter<nastja::math::Matrix3<NASTJA_TYPE_SPECIFICATION>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::math::Matrix3<NASTJA_TYPE_SPECIFICATION>& m, FormatContext& ctx) {
    return format_to(
        ctx.out(),
        "[[{},{},{}],\n [{},{},{}],\n [{},{},{}]]",
        m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8]);
  }
};

template <>
struct formatter<nastja::math::Matrix2<NASTJA_TYPE_SPECIFICATION>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::math::Matrix2<NASTJA_TYPE_SPECIFICATION>& m, FormatContext& ctx) {
    return format_to(
        ctx.out(),
        "[[{},{}],\n [{},{}]]",
        m[0], m[1], m[2], m[3]);
  }
};

}  // namespace fmt
