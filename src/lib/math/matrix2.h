/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/vector.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <type_traits>

namespace nastja {
namespace math {

/**
 * Class for Matrix2, a three dimensional vector.
 *
 * @tparam T  Type of the vector elements.
 */
template <typename T>
class Matrix2 {
  static_assert(std::is_arithmetic<T>::value, "The Matrix2 class needs an arithmetic data type.");

public:
  /// Define the type of the length, this can not be an integral type because of the sqrt.
  using length_type = typename std::conditional<std::is_integral<T>::value, real_t, T>::type;

  /// @name Constructors
  /// @{
  explicit Matrix2() : m{T(), T(), T(), T()} {}
  explicit Matrix2(T initial) : m{initial, initial, initial, initial} {}
  explicit Matrix2(const Vector2<T>& a, const Vector2<T>& b) : m{a[0], b[0], a[1], b[1]} {}
  explicit Matrix2(T xx, T xy, T yx, T yy) : m{xx, xy, yx, yy} {}
  explicit Matrix2(const T* initial) : m{initial[0], initial[1], initial[2], initial[3]} {}

  template <typename U>
  explicit Matrix2(const Matrix2<U>& m) : m{T(m[0]), T(m[1]), T(m[2]), T(m[3])} {}
  /// @}

  /// @name Operators
  /// @{
  template <typename U>
  Matrix2<typename std::common_type<T, U>::type> operator+(const Matrix2<U>& other) const {
    return Matrix2<typename std::common_type<T, U>::type>(m[0] + other[0], m[1] + other[1],
                                                          m[2] + other[2], m[3] + other[3]);
  }

  template <typename U>
  Matrix2<typename std::common_type<T, U>::type> operator-(const Matrix2<U>& other) const {
    return Matrix2<typename std::common_type<T, U>::type>(m[0] - other[0], m[1] - other[1],
                                                          m[2] - other[2], m[3] - other[3]);
  }

  Matrix2 operator-() const {
    return Matrix2(-m[0], -m[1],
                   -m[2], -m[3]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix2<typename std::common_type<T, U>::type> operator*(U scalar) const {
    return Matrix2<typename std::common_type<T, U>::type>(m[0] * scalar, m[1] * scalar,
                                                          m[2] * scalar, m[3] * scalar);
  }

  template <typename U>
  Vector2<typename std::common_type<T, U>::type> operator*(const Vector2<U>& other) const {
    return Vector2<typename std::common_type<T, U>::type>{m[0] * other[0] + m[1] * other[1],
                                                          m[2] * other[0] + m[3] * other[1]};
  }

  template <typename U>
  Vector2<typename std::common_type<T, U>::type> multiplyT(const Vector2<U>& other) const {
    return Vector2<typename std::common_type<T, U>::type>{m[0] * other[0] + m[2] * other[1],
                                                          m[1] * other[0] + m[3] * other[1]};
  }

  template <typename U>
  Matrix2<typename std::common_type<T, U>::type> operator*(const Matrix2<U>& other) const {
    return Matrix2<typename std::common_type<T, U>::type>(m[0] * other[0] + m[1] * other[2],
                                                          m[0] * other[1] + m[1] * other[3],
                                                          m[2] * other[0] + m[3] * other[2],
                                                          m[2] * other[1] + m[3] * other[3]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix2<typename std::common_type<T, U>::type> operator/(U scalar) const {
    return Matrix2<typename std::common_type<T, U>::type>(m[0] / scalar, m[1] / scalar,
                                                          m[2] / scalar, m[3] / scalar);
  }

  template <typename U>
  Matrix2& operator+=(const Matrix2<U>& other) {
    m[0] += other[0];
    m[1] += other[1];
    m[2] += other[2];
    m[3] += other[3];
    return *this;
  }

  template <typename U>
  Matrix2& operator-=(const Matrix2<U>& other) {
    m[0] -= other[0];
    m[1] -= other[1];
    m[2] -= other[2];
    m[3] -= other[3];
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix2& operator*=(U scalar) {
    m[0] *= scalar;
    m[1] *= scalar;
    m[2] *= scalar;
    m[3] *= scalar;
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Matrix2& operator/=(U scalar) {
    m[0] /= scalar;
    m[1] /= scalar;
    m[2] /= scalar;
    m[3] /= scalar;
    return *this;
  }

  template <typename U>
  Matrix2& operator*=(const Matrix2<U>& other) {
    Matrix2 tmp(m[0] * other[0] + m[1] * other[2],
                m[0] * other[1] + m[1] * other[3],
                m[2] * other[0] + m[3] * other[2],
                m[2] * other[1] + m[3] * other[3]);
    return this->operator=(tmp);
  }

  template <typename U>
  bool operator==(const Matrix2<U>& other) const {
    return m[0] == other[0] && m[1] == other[1] &&
           m[2] == other[2] && m[3] == other[3];
  }

  template <typename U>
  bool operator!=(const Matrix2<U>& other) const {
    return m[0] != other[0] || m[1] != other[1] ||
           m[2] != other[2] || m[3] != other[3];
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator==(U scalar) const {
    return m[0] == scalar && m[1] == scalar &&
           m[2] == scalar && m[3] == scalar;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator!=(U scalar) const {
    return m[0] != scalar || m[1] != scalar ||
           m[2] != scalar || m[3] != scalar;
  }
  /// @}

  /// @name Element access
  /// @{
  T& operator[](unsigned int index) {
    return m[index];
  }

  const T& operator[](unsigned int index) const {
    return m[index];
  }

  T& operator()(unsigned int i, unsigned int j) {
    return m[i * 2 + j];
  }

  const T& operator()(unsigned int i, unsigned int j) const {
    return m[i * 2 + j];
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  T* data() noexcept {
    return m.data();
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  const T* data() const noexcept {
    return m.data();
  }

  /**
   * Sets the matrix.
   *
   * @param xx  Element 0, 0.
   * @param xy  Element 0, 1.
   * @param yx  Element 1, 0.
   * @param yy  Element 1, 1.
   */
  void set(T xx, T xy, T yx, T yy) {
    m[0] = xx;
    m[1] = xy;
    m[2] = yx;
    m[3] = yy;
  }

  /**
   * Sets the rotation.
   *
   * @param alpha  The angle.
   */
  template <typename U>
  Matrix2<T>& setRotation(U alpha) {
    m[0] = std::cos(alpha);
    m[1] = -std::sin(alpha);
    m[2] = std::sin(alpha);
    m[3] = std::cos(alpha);

    return *this;
  }
  /// @}

private:
  std::array<T, 4> m;  ///< Array of length four, holding the 2x2 matrix with element type T.
};

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param m   The matrix.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const Matrix2<T>& m) {
  return os << "[[" << m[0] << "," << m[1] << "]"
            << "\n"
            << " [" << m[2] << "," << m[3] << "]]";
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator==(U scalar, const Matrix2<T>& v) {
  return v == scalar;
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator!=(U scalar, const Matrix2<T>& v) {
  return v != scalar;
}

/**
 * Scalar multiplication
 *
 * @param scalar  The scalar.
 * @param m       The matrix.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return The matrix scalar product.
 */
template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Matrix2<T> operator*(U scalar, const Matrix2<T>& m) {
  return m * scalar;
}

}  // namespace math
}  // namespace nastja
