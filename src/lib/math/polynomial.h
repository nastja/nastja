/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <cstddef>
#include <vector>

namespace nastja {
namespace math {

class Polynomial {
public:
  explicit Polynomial(std::initializer_list<real_t> l) : coeff_{l} {}
  ~Polynomial()                     = default;
  Polynomial(const Polynomial&)     = delete;
  Polynomial(Polynomial&&) noexcept = default;
  Polynomial& operator=(const Polynomial&) = delete;
  Polynomial& operator=(Polynomial&&) = delete;

  real_t operator[](std::size_t i) const { return coeff_[i]; }

  real_t calc(real_t value) const {
    return calcDerivation(0, value);
  }

  real_t calcDerivation(std::size_t order, real_t value) const {
    real_t sum = 0.0;

    for (std::size_t i = coeff_.size() - order - 1; i <= coeff_.size(); i--) {
      std::size_t d = 1;
      for (std::size_t j = (i + 1); j <= i + order; j++) {
        d *= j;
      }
      sum = (sum * value) + real_t(d) * coeff_[i + order];
    }

    return sum;
  }

  real_t calcIntegral(real_t value) const {
    real_t sum = 0.0;

    for (std::size_t i = coeff_.size() - 1; i <= coeff_.size(); i--) {
      sum = (sum + coeff_[i] / real_t(i + 1)) * value;
    }

    return sum;
  }

  auto getDegree() const { return coeff_.size() - 1; }

private:
  std::vector<real_t> coeff_;  ///< The vector of the coefficients.
};

}  // namespace math
}  // namespace nastja
