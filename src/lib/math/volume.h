/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/calculatevolume.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/direction.h"

namespace nastja {
namespace math {

template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class Volume {
public:
  explicit Volume()         = default;
  virtual ~Volume()         = default;
  Volume(const Volume&)     = default;
  Volume(Volume&&) noexcept = default;
  Volume& operator=(const Volume&) = default;
  Volume& operator=(Volume&&) = default;

  virtual real_t operator()(const FieldView& view, T* ptr, T checkID) = 0;
  virtual real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) = 0;
};

/**
 * This class describes the calculation of the volume by counting faces.
 * A face is counted if the cellID of the neighbor voxel differs.
 */
template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class VolumeCount : public Volume<T, FieldView> {
public:
  explicit VolumeCount()              = default;
  virtual ~VolumeCount()              = default;
  VolumeCount(const VolumeCount&)     = default;
  VolumeCount(VolumeCount&&) noexcept = default;
  VolumeCount& operator=(const VolumeCount&) = default;
  VolumeCount& operator=(VolumeCount&&) = default;

  real_t operator()(const FieldView& view, T* ptr, T checkID) {
    return view.get(ptr) == checkID ? 1.0 : 0.0;
  }

  real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) {
    if (view.get(ptr) == changeID) return 0.0;
    if (view.get(ptr) != checkID && changeID != checkID) return 0.0;

    return checkID == changeID ? 1.0 : -1.0;
  }
};

/**
 * This class describes the calculation of the volume by approximation via marching cubes.
 */
template <typename T = cellid_t, typename FieldView = field::FieldView<T, 1, 1>>
class VolumeMC : public Volume<T, FieldView> {
public:
  explicit VolumeMC()           = default;
  virtual ~VolumeMC()           = default;
  VolumeMC(const VolumeMC&)     = default;
  VolumeMC(VolumeMC&&) noexcept = default;
  VolumeMC& operator=(const VolumeMC&) = default;
  VolumeMC& operator=(VolumeMC&&) = default;

  real_t operator()(const FieldView& view, T* ptr, T checkID) {
    return calcCube(view, ptr, checkID, calcVolumeMCLookup);
  }

  real_t diff(const FieldView& view, T* ptr, T checkID, T changeID) {
    return calcStencilDiff(view, ptr, checkID, changeID, calcVolumeMCLookup);
  }
};

}  // namespace math
}  // namespace nastja
