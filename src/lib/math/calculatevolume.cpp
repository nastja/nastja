/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "calculatevolume.h"
#include "lib/math/calculatecubes.h"

namespace nastja {
namespace math {

/**
 * The Volumes of the 15 representatives in the marching cubes algorithm.
 * Volumes are calculated from the individual tetrahedrons.
 * Cases can be found here: https://upload.wikimedia.org/wikipedia/commons/a/a7/MarchingCubes.svg
 */
//constexpr static real_t volumeCases[15] = {
//    0,
//    1 / 48.,
//    1 / 8.,
//    1 / 24.,
//    17 / 48.,
//    1 / 2.,
//    1 / 3.,
//    1 / 12.,
//    1 / 2.,
//    5 / 48.,
//    1 / 24.,
//    7 / 48.,
//    1 / 16.,
//    1 / 4.,
//    1 / 2.};
constexpr static std::array<real_t, 15> volumeCases{
    0.0,
    0.125,
    0.25,
    0.25,
    0.375,
    0.5,
    0.5,
    0.5,
    0.5,
    0.5,
    0.25,
    0.375,
    0.375,
    0.5,
    0.5};

/**
 * Look for an representative for the given cube bit pattern and lookup the volume in the volumeCases table.
 *
 * @param  i The cube bit pattern.
 *
 * @return   The volume of the representative.
 */
constexpr real_t volume(unsigned int i) noexcept {
  return volumeCases[findRepresentative(i)];
}

/// LUT of the volume area of all 256 possible cube bit pattern.
constexpr static std::array<real_t, 256> volumeArea{
    volume(0), volume(1), volume(2), volume(3), volume(4), volume(5), volume(6), volume(7), volume(8), volume(9),
    volume(10), volume(11), volume(12), volume(13), volume(14), volume(15), volume(16), volume(17), volume(18), volume(19),
    volume(20), volume(21), volume(22), volume(23), volume(24), volume(25), volume(26), volume(27), volume(28), volume(29),
    volume(30), volume(31), volume(32), volume(33), volume(34), volume(35), volume(36), volume(37), volume(38), volume(39),
    volume(40), volume(41), volume(42), volume(43), volume(44), volume(45), volume(46), volume(47), volume(48), volume(49),
    volume(50), volume(51), volume(52), volume(53), volume(54), volume(55), volume(56), volume(57), volume(58), volume(59),
    volume(60), volume(61), volume(62), volume(63), volume(64), volume(65), volume(66), volume(67), volume(68), volume(69),
    volume(70), volume(71), volume(72), volume(73), volume(74), volume(75), volume(76), volume(77), volume(78), volume(79),
    volume(80), volume(81), volume(82), volume(83), volume(84), volume(85), volume(86), volume(87), volume(88), volume(89),
    volume(90), volume(91), volume(92), volume(93), volume(94), volume(95), volume(96), volume(97), volume(98), volume(99),
    volume(100), volume(101), volume(102), volume(103), volume(104), volume(105), volume(106), volume(107), volume(108), volume(109),
    volume(110), volume(111), volume(112), volume(113), volume(114), volume(115), volume(116), volume(117), volume(118), volume(119),
    volume(120), volume(121), volume(122), volume(123), volume(124), volume(125), volume(126), volume(127), volume(128), volume(129),
    volume(130), volume(131), volume(132), volume(133), volume(134), volume(135), volume(136), volume(137), volume(138), volume(139),
    volume(140), volume(141), volume(142), volume(143), volume(144), volume(145), volume(146), volume(147), volume(148), volume(149),
    volume(150), volume(151), volume(152), volume(153), volume(154), volume(155), volume(156), volume(157), volume(158), volume(159),
    volume(160), volume(161), volume(162), volume(163), volume(164), volume(165), volume(166), volume(167), volume(168), volume(169),
    volume(170), volume(171), volume(172), volume(173), volume(174), volume(175), volume(176), volume(177), volume(178), volume(179),
    volume(180), volume(181), volume(182), volume(183), volume(184), volume(185), volume(186), volume(187), volume(188), volume(189),
    volume(190), volume(191), volume(192), volume(193), volume(194), volume(195), volume(196), volume(197), volume(198), volume(199),
    volume(200), volume(201), volume(202), volume(203), volume(204), volume(205), volume(206), volume(207), volume(208), volume(209),
    volume(210), volume(211), volume(212), volume(213), volume(214), volume(215), volume(216), volume(217), volume(218), volume(219),
    volume(220), volume(221), volume(222), volume(223), volume(224), volume(225), volume(226), volume(227), volume(228), volume(229),
    volume(230), volume(231), volume(232), volume(233), volume(234), volume(235), volume(236), volume(237), volume(238), volume(239),
    volume(240), volume(241), volume(242), volume(243), volume(244), volume(245), volume(246), volume(247), volume(248), volume(249),
    volume(250), volume(251), volume(252), volume(253), volume(254)};

/**
 * Calculates the volume for the inputCell checkID with marching cubes around the input with the 26 neighbors.
 *
 * @param stencil  The stencil.
 * @param checkID  Check for this value.
 *
 * @return volume area.
 */
real_t calcVolumeMC(const std::array<cellid_t, 27>& stencil, cellid_t checkID) {
  real_t volume = 0.0;

  for (unsigned int xoffset = 0; xoffset <= 1; xoffset++) {
    for (unsigned int yoffset = 0; yoffset <= 1; yoffset++) {
      for (unsigned int zoffset = 0; zoffset <= 1; zoffset++) {
        auto cube = cubeFromStencil(stencil, xoffset, yoffset, zoffset);

        volume += calcVolumeMC(cube, checkID);
      }
    }
  }

  return volume;
}

/**
 * Calculates the volume for the inputCell checkID from one cube (2x2).
 *
 * ~~~
 *    4----5
 *   /|   /|
 *  7----6 |
 *  | 0--|-1
 *  |/   |/
 *  3----2
 * ~~~
 *
 * @param  cube     The cube.
 * @param  checkID  Check for this value.
 *
 * @return volume area.
 */
real_t calcVolumeMC(const std::array<cellid_t, 8>& cube, cellid_t checkID) {
  unsigned int cubeBitPattern = calcCubeBitPattern(cube, checkID);
  return calcVolumeMCLookup(cubeBitPattern);
}

/**
 * @overload
 *
 * @param  cubeBitPattern The cube bit pattern.
 *
 * @return volume area.
 */
real_t calcVolumeMCLookup(unsigned int cubeBitPattern) {
  static_assert(volumeArea[0] == 0.0, "This assert makes sure that we use constexpr.");
  if (countBits(cubeBitPattern) > 4) {
    return 1.0 - volumeArea[cubeBitPattern];
  }

  return volumeArea[cubeBitPattern];
}

/**
 * Calculates the volume for the inputCell checkID with marching cubes around the input with the 26 neighbors.
 *
 * @param  stencil  The stencil.
 * @param  checkID  Check for this value.
 *
 * @return volume area.
 */
real_t calcVolumeCount(const std::array<cellid_t, 27>& stencil, cellid_t checkID) {
  real_t volume = 0.0;

  for (int xoffset = 0; xoffset <= 1; xoffset++) {
    for (int yoffset = 0; yoffset <= 1; yoffset++) {
      for (int zoffset = 0; zoffset <= 1; zoffset++) {
        auto cube = cubeFromStencil(stencil, xoffset, yoffset, zoffset);

        volume += calcVolumeCount(cube, checkID);
      }
    }
  }

  return volume;
}

/**
 * Calculates the volume for the inputCell checkID from one cube (2x2).
 *
 * ~~~
 *    4----5
 *   /|   /|
 *  7----6 |
 *  | 0--|-1
 *  |/   |/
 *  3----2
 * ~~~
 *
 * @param  cube     The cube.
 * @param  checkID  Check for this value.
 *
 * @return volume area.
 */
real_t calcVolumeCount(const std::array<cellid_t, 8>& cube, cellid_t checkID) {
  return calcVolumeCountLookup(calcCubeBitPattern(cube, checkID));
}

/**
 * @overload
 *
 * @param  cubeBitPattern The cube index bit pattern.
 *
 * @return Volume area.
 */
real_t calcVolumeCountLookup(unsigned int cubeBitPattern) {
  return 0.125 * countBits(cubeBitPattern);
}

}  // namespace math
}  // namespace nastja
