/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <cmath>
#include <limits>
#include <type_traits>

namespace nastja {
namespace math {

/**
 * Check for equal values.
 *
 * Generic function for the comparison of two numeric values. Depending on the types of the two arguments, a floating
 * point aware comparison considers the limited machine accuracy.
 *
 * @param a  First value.
 * @param b  Second value.
 *
 * @tparam T  Type
 *
 * @return true if the two values are equal.
 */
template <typename T>
inline typename std::enable_if<!std::is_floating_point<T>::value, bool>::type equalImpl(T a, T b) {
  return a == b;
}

/**
 * @overload
 * @param a  First value.
 * @param b  Second value.
 * @return true if the two values are equal.
 */
template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value, bool>::type equalImpl(T a, T b) {
  return std::fabs(a - b) < std::numeric_limits<T>::epsilon();
}

/**
 * Check for nearly equal values.
 *
 * Generic function for the comparison of two numeric values. Depending on the types of the two arguments, a floating
 * point aware comparison considers the limited machine accuracy.
 *
 * @param a  First value.
 * @param b  Second value.
 *
 * @tparam T  Type of first value.
 * @tparam U  Type of second value.
 *
 * @return true if the two values are equal.
 */
template <typename T, typename U>
inline bool nearlyEqual(T a, U b) {
  return equalImpl<typename std::common_type<T, U>::type>(a, b);
}

}  // namespace math
}  // namespace nastja
