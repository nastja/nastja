/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "brent.h"
#include "fmt/format.h"
#include <cfloat>

namespace nastja {
namespace math {

/**
 * Expand bracket for surrounding the root.
 *
 * @param f      The function.
 * @param a      Left bracket.
 * @param b      Right bracket.
 * @param Niter  Number of iterations.
 * @param FACTOR Factor for bracket expansion.
 */
void bracket(const std::function<real_t(real_t)>& f, real_t& a, real_t& b, const unsigned int Niter /* = 50*/, const real_t FACTOR /* = 1.6*/) {
  if (a == b) {
    throw std::range_error(fmt::format("a == b, can not expand range"));
  }

  real_t fa = f(a);
  real_t fb = f(b);

  for (unsigned int i = 0; i < Niter; i++) {
    if (fa * fb < 0.0) {  // sign changes
      return;
    }

    if (fabs(fa) < fabs(fa)) {
      a += FACTOR * (a - b);
      fa = f(a);
    } else {
      b += FACTOR * (b - a);
      fb = f(b);
    }
  }
}

/**
 * Brent's method for root-finding
 *
 * @param f     The function.
 * @param a     Left bracket.
 * @param b     Right bracket.
 * @param t     Tolerance.
 * @param Niter Number of iterations.
 *
 * @return The root.
 */
real_t brent(const std::function<real_t(real_t)>& f, real_t a, real_t b, const real_t t, const unsigned int Niter /* = 500*/) {
  real_t fa = f(a);
  real_t fb = f(b);

  if (fa * fb > 0) {
    bracket(f, a, b);
  }

  real_t c  = a;
  real_t fc = fa;  // At the initialization c = a

  real_t d = b - a;
  real_t e = d;
  real_t s;
  real_t p;
  real_t q;
  real_t r;

  unsigned int iter = 0;

  while (iter < Niter) {
    iter++;

    if (fb * fc > 0) {
      c  = a;
      fc = fa;
      d  = b - a;
      e  = d;
    }

    if (fabs(fc) < fabs(fb)) {
      a  = b;
      b  = c;
      c  = a;
      fa = fb;
      fb = fc;
      fc = fa;
    }

    real_t tol = 2 * DBL_EPSILON * fabs(b) + t;
    real_t m   = (c - b) / 2;  //Toleranz

    if (!((fabs(m) > tol) && (fabs(fb) > 0))) {
      // logger::get().warning("{}, {:e}\n", iter, fb);
      return b;
    }

    if ((fabs(e) < tol) || (fabs(fa) <= fabs(fb))) {
      d = m;
      e = m;
    } else {
      s = fb / fa;
      if (a == c) {
        p = 2 * m * s;
        q = 1 - s;
      } else {
        q = fa / fc;
        r = fb / fc;
        p = s * (2 * m * q * (q - r) - (b - a) * (r - 1));
        q = (q - 1) * (r - 1) * (s - 1);
      }
      if (p > 0) {
        q = -q;
      } else {
        p = -p;
      }

      s = e;
      e = d;
      if ((2 * p < 3 * m * q - fabs(tol * q)) && (p < fabs(s * q / 2))) {
        d = p / q;
      } else {
        d = m;
        e = m;
      }
    }

    a  = b;
    fa = fb;

    if (fabs(d) > tol) {
      b = b + d;
    } else {
      if (m > 0) {
        b = b + tol;
      } else {
        b = b - tol;
      }
    }

    fb = f(b);
  }
  // logger::get().warning("{}, {:e}\n", iter, f(b));
  return b;
}

}  // namespace math
}  // namespace nastja
