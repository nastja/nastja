/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/timing.h"
#include "lib/storage/sparestoragecell.h"
#include <utility>
#include <vector>
#include "nanoflann.hpp"

namespace nastja {
namespace math {

/**
 * A simple container-of-points adaptor for nanoflann, without duplicating the storage.
 *
 * @tparam Container  The container of points type
 * @tparam DataType   The type of the point coordinates.
 * @tparam Distance   The distance metric to use: nanoflann::metric_L1, nanoflann::metric_L2,
 *                    nanoflann::metric_L2_Simple, etc.
 * @tparam IndexType  The type for indices in the KD-tree index.
 * @tparam DIM        If set to >0, it specifies a compile-time fixed dimensionality for the points in the data set,
 *                    allowing more compiler optimizations.
 */
template <typename Container, typename DataType = double, int DIM = -1, typename Distance = nanoflann::metric_L2, typename IndexType = std::size_t>
class KDTreeAdaptor {
public:
  using self_t   = KDTreeAdaptor<Container, DataType, DIM, Distance, IndexType>;
  using metric_t = typename Distance::template traits<DataType, self_t>::distance_t;
  using index_t  = nanoflann::KDTreeSingleIndexAdaptor<metric_t, self_t, DIM, IndexType>;

  explicit KDTreeAdaptor(const std::size_t dim, const Container& container, const int leaf_max_size = 10)
      : container_{container} {
    assert(container.empty() || point_size() == dim);
    if (DIM > 0 && static_cast<int>(dim) != DIM) {
      throw std::runtime_error("The point data dimensionality does not match the 'DIM' template argument.");
    }

    index_ = std::make_unique<index_t>(static_cast<int>(dim), *this, nanoflann::KDTreeSingleIndexAdaptorParams(leaf_max_size));
    rebuildIndex();
  }

  void rebuildIndex() {
    FunctionTimer ft("KDtree-rebuildIndex");
    if (std::is_same<Container, SpareStorageCell>::value) {
      key_to_index(container_);
    }
    index_->buildIndex();
  }

  /**
   * Query for the closest points around a given point.
   *
   * @param queryPoint   The query point.
   * @param numClosest   The number of closest points.
   * @param indices      Returns the indices.
   * @param distancesSq  Returns the square of the distances.
   */
  void findNeighbors(const DataType* queryPoint, const std::size_t numClosest, IndexType* indices, DataType* distancesSq) const {
    nanoflann::KNNResultSet<DataType, IndexType> resultSet(numClosest);
    resultSet.init(indices, distancesSq);
    index_->findNeighbors(resultSet, queryPoint, nanoflann::SearchParams());

    if (map_.empty()) return;

    for (std::size_t i = 0; i < resultSet.size(); i++) {
      indices[i] = unmap_index_internal(container_, indices[i]);
    }
  }

  std::size_t radiusSearch(const DataType* queryPoint, const DataType& radius, std::vector<std::pair<IndexType, DataType>>& indicesDists) const {
    nanoflann::SearchParams searchParams{32, 0, false};

    auto count = index_->radiusSearch(queryPoint, radius, indicesDists, searchParams);

    if (map_.empty()) return count;

    for (auto& neighbor : indicesDists) {
      neighbor.first = unmap_index_internal(container_, neighbor.first);
    }

    return count;
  }

  /// @name Interface expected by KDTreeSingleIndexAdaptor
  /// @{
  const self_t& derived() const {
    return *this;
  }

  self_t& derived() {
    return *this;
  }

  /**
   * Get the number of data points.
   *
   * @return The number of data points.
   */
  std::size_t kdtree_get_point_count() const {
    return container_.size() - skipLiquid_;
  }

  /**
   * Get the n-th componet of the point p
   *
   * @param p  The index of the point.
   * @param n  The index of the component.
   *
   * @return The n-th component of the point p.
   */
  DataType kdtree_get_pt(const std::size_t p, const std::size_t n) const {
    return get_pt_internal(container_, p, n);
  }

  /**
   * Get the bounding box.
   *
   * @return False, so that the standard bounding box computation loop will be executed.
   */
  template <typename BBOX>
  bool kdtree_get_bbox(BBOX& /*bb*/) const {
    return false;
  }
  /// @}

private:
  /**
   * Builds the map from key to index 1..N. Liquid and solid entries in cells are skipped, they have negative coordinates.
   *
   * @param container  The container.
   */
  template <typename C>
  typename std::enable_if_t<std::is_same<C, SpareStorageCell>::value>
  key_to_index(const C& container) {
    map_.resize(container.size());

    IndexType i = 0;
    skipLiquid_ = 0;
    for (const auto& key : container) {
      if (key.second.center.x() == -1) {
        skipLiquid_++;
        continue;
      }

      map_[i] = key.first;
      i++;
    }
  }

  template <typename C>
  typename std::enable_if_t<!std::is_same<C, SpareStorageCell>::value>
  key_to_index(const C& /*container*/) {}

  template <typename C>
  typename std::enable_if_t<std::is_same<C, SpareStorageCell>::value, DataType>
  get_pt_internal(const C& container, const std::size_t p, const std::size_t n) const {
    return container(map_.at(p)).center[n];
  }

  template <typename C>
  typename std::enable_if_t<!std::is_same<C, SpareStorageCell>::value, DataType>
  get_pt_internal(const C& container, const std::size_t p, const std::size_t n) const {
    return container[p][n];
  }

  std::size_t point_size() {
    return point_size_internal(container_);
  }

  template <typename C>
  typename std::enable_if_t<std::is_same<C, SpareStorageCell>::value, std::size_t>
  point_size_internal(const C& container) const {
    return container.begin()->second.center.size();
  }

  template <typename C>
  typename std::enable_if_t<!std::is_same<C, SpareStorageCell>::value, std::size_t>
  point_size_internal(const C& container) const {
    return container[0].size();
  }

  /**
   * Unmap from linerar index to container index (e.g., cellID).
   *
   * @param index      The index.
   *
   * @tparam C  The container Type.
   *
   * @return The container index.
   */
  template <typename C>
  typename std::enable_if_t<std::is_same<C, SpareStorageCell>::value, IndexType>
  unmap_index_internal(const C& /*container*/, IndexType index) const {
    return map_.at(index);
  }

  template <typename C>
  typename std::enable_if_t<!std::is_same<C, SpareStorageCell>::value, std::size_t>
  unmap_index_internal(const C& /*container*/, IndexType index) const {
    return index;
  }

  const Container& container_;      ///< Reference to the container of points.
  std::size_t skipLiquid_{};        ///< Skip the liquid and solid cells without a coordinate.
  std::unique_ptr<index_t> index_;  ///< The kd-tree index for calling methods as usual with any other FLANN index.
  std::vector<IndexType> map_{};
};

}  // namespace math
}  // namespace nastja
