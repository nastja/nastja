/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <array>

namespace nastja {
namespace math {

/**
 * Bit pattern for all 15 representatives cubes.
 *
 * ~~~
 *  Bit pattern
 *    2----3
 *   /|   /|
 *  6----7 |
 *  | 0--|-1
 *  |/   |/
 *  4----5
 *
 *    .----.    .----.    .----.    .----o    .----.
 *   /|   /|   /|   /|   /|   /|   /|   /|   /|   /|
 *  .----. |  .----. |  .----. |  .----. |  .----o |
 *  | .--|-.  | o--|-.  | o--|-o  | o--|-.  | o--|-.
 *  |/   |/   |/   |/   |/   |/   |/   |/   |/   |/
 *  .----.    .----.    .----.    .----.    .----.
 *     0         1         2         3         4
 *
 *    o----.    .----.    .----o    o----o    o----.
 *   /|   /|   /|   /|   /|   /|   /|   /|   /|   /|
 *  .----. |  .----o |  .----. |  .----. |  .----. |
 *  | o--|-o  | o--|-o  | o--|-.  | o--|-o  | o--|-o
 *  |/   |/   |/   |/   |/   |/   |/   |/   |/   |/
 *  .----.    .----.    .----o    .----.    o----.
 *     5         6         7         8         9
 *
 *    o----.    o----.    o----.    .----.    .----o
 *   /|   /|   /|   /|   /|   /|   /|   /|   /|   /|
 *  .----. |  o----. |  .----o |  o----o |  o----. |
 *  | o--|-o  | o--|-o  | o--|-o  | o--|-o  | o--|-.
 *  |/   |/   |/   |/   |/   |/   |/   |/   |/   |/
 *  .----o    .----.    .----.    .----.    .----o
 *    10        11        12        13        14
 * ~~~
 */
constexpr static std::array<unsigned int, 15> mapCases{
    0b00000000,
    0b00000001,
    0b00000011,
    0b00001001,
    0b10000001,
    0b00000111,
    0b10000011,
    0b00101001,
    0b00001111,
    0b00010111,
    0b00100111,
    0b01000111,
    0b10000111,
    0b11000011,
    0b01101001};

constexpr static std::array<unsigned long, 27> cubeMask{
    1ul,
    1ul << 8 | 2ul,
    2ul << 8,
    1ul << 16 | 4ul,
    1ul << 24 | 2ul << 16 | 4ul << 8 | 8ul,
    2ul << 24 | 8ul << 8,
    4ul << 16,
    4ul << 24 | 8ul << 16,
    8ul << 24,
    16ul | 1ul << 32,
    16ul << 8 | 1ul << 40 | 2ul << 32 | 32ul,
    2ul << 40 | 32ul << 8,
    16ul << 16 | 1ul << 48 | 4ul << 32 | 64ul,
    128ul | 16ul << 24 | 1ul << 56 | 2ul << 48 | 32ul << 16 | 4ul << 40 | 64ul << 8 | 8ul << 32,
    128ul << 8 | 2ul << 56 | 32ul << 24 | 8ul << 40,
    64ul << 16 | 4ul << 48,
    128ul << 16 | 4ul << 56 | 64ul << 24 | 8ul << 48,
    128ul << 24 | 8ul << 56,
    16ul << 32,
    16ul << 40 | 32ul << 32,
    32ul << 40,
    16ul << 48 | 64ul << 32,
    16ul << 56 | 32ul << 48 | 64ul << 40 | 128ul << 32,
    32ul << 56 | 128ul << 40,
    64ul << 48,
    128ul << 48 | 64ul << 56,
    128ul << 56,
};

/**
 * Rotation of bits in z-direction.
 *
 * 7654 3210 becomes 5746 1302.
 *
 * @param in The input bits.
 *
 * @return z-rotation of bits.
 */
constexpr unsigned int bitRotationZ(unsigned int in) {
  unsigned int shift1 = ((in << 1) & 0x22) | ((in >> 1) & 0x44);  // .74. .30.
  unsigned int shift2 = ((in << 2) & 0x88) | ((in >> 2) & 0x11);  // 5..6 1..2
  return shift1 | shift2;                                         // 5746 1302
}

/**
 * Rotation of bits in y-direction.
 *
 * 7654 3210 becomes 6240 7351.
 *
 * @param in The input bits.
 *
 * @return y-rotation of bits.
 */
constexpr unsigned int bitRotationY(unsigned int in) {
  unsigned int shift4 = ((in << 4) & 0x50) | ((in >> 4) & 0x0A);  // .2.0 7.5.
  unsigned int shift1 = ((in << 1) & 0xA0) | ((in >> 1) & 0x05);  // 6.4. .3.1
  return shift1 | shift4;                                         // 6240 7351
}

/**
 * Rotation of bits in x-direction.
 *
 * 7654 3210 becomes 3276 1054.
 *
 * @param in The input bits.
 *
 * @return x-rotation of bits.
 */
constexpr unsigned int bitRotationX(unsigned int in) {
  unsigned int shift2 = ((in >> 2) & 0x30) | ((in << 2) & 0x0C);  // ..76 10..
  unsigned int shift4 = ((in << 4) & 0xC0) | ((in >> 4) & 0x03);  // 32.. ..54
  return shift2 | shift4;                                         // 3276 1054
}

/**
 * Rolls a cube to its net with x-, 3 times z- and x-rotation.
 *
 * @param  in   The cube index input.
 * @param  step The step <=4.
 *
 * @return The rotated cube index.
 */
constexpr unsigned int cubeNetRotation(unsigned int in, unsigned int step) {
  if (step == 0 || step == 4) return bitRotationX(in);
  return bitRotationZ(in);
}

/**
 * Count bits.
 *
 * @param  v The bit input.
 *
 * @return The number of set bits.
 */
constexpr unsigned int countBits(unsigned int v) {
  unsigned int c = 0;
  for (c = 0; v != 0u; c++) {
    v &= v - 1;  // clear the least significant bit set
  }
  return c;
}

/**
 * Check if the cube index is a representative.
 *
 * @param  r The cube index.
 *
 * @return The representative if found. -1 else.
 */
constexpr int isRepresentative(unsigned int r) {
  for (unsigned int i = 0; i < 15; i++) {
    if (mapCases[i] == r) return i;
  }
  return -1;
}

/**
 * Find the representative for the given cube index.
 *
 * @param  in The cube index.
 *
 * @return The representative.
 */
constexpr int findRepresentative(unsigned int in) {
  if (countBits(in) > 4) {
    in = in ^ 0xFF;
  }

  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 4; j++) {
      int r = isRepresentative(in);
      if (r >= 0) return r;
      in = bitRotationY(in);
    }

    in = cubeNetRotation(in, i);
  }

  return -1;
}

/**
 * Determines if the value on position index appears first on index.
 *
 * @param cube   The cube.
 * @param index  The index.
 *
 * @return True if it is the firs occurrence, False otherwise.
 */
inline bool isFirstValueInCube(const std::array<cellid_t, 8>& cube, unsigned int index) {
  for (unsigned int i = 0; i < index; i++) {
    if (cube[i] == cube[index]) return false;
  }

  return true;
}

/**
 * Makes a cube from a stencil.
 *
 * @param stencil  The stencil.
 * @param xoffset  The x offset.
 * @param yoffset  The y offset.
 * @param zoffset  The z offset.
 *
 * @return an array of the cube.
 */
inline std::array<cellid_t, 8> cubeFromStencil(const std::array<cellid_t, 27>& stencil, unsigned int xoffset, unsigned int yoffset, unsigned int zoffset) {
  std::array<cellid_t, 8> cube{};

  // clang-format off
  cube[0] = stencil[(zoffset    ) * 9 + (yoffset    ) * 3 + (xoffset    )];
  cube[1] = stencil[(zoffset    ) * 9 + (yoffset    ) * 3 + (xoffset + 1)];
  cube[2] = stencil[(zoffset    ) * 9 + (yoffset + 1) * 3 + (xoffset    )];
  cube[3] = stencil[(zoffset    ) * 9 + (yoffset + 1) * 3 + (xoffset + 1)];
  cube[4] = stencil[(zoffset + 1) * 9 + (yoffset    ) * 3 + (xoffset    )];
  cube[5] = stencil[(zoffset + 1) * 9 + (yoffset    ) * 3 + (xoffset + 1)];
  cube[6] = stencil[(zoffset + 1) * 9 + (yoffset + 1) * 3 + (xoffset    )];
  cube[7] = stencil[(zoffset + 1) * 9 + (yoffset + 1) * 3 + (xoffset + 1)];
  // clang-format on

  return cube;
}

/**
 * Calculates the cube bit pattern.
 *
 * @param cube   The cube.
 * @param check  The cell to check.
 *
 * @return The cube bit pattern.
 */
inline unsigned int calcCubeBitPattern(const std::array<cellid_t, 8>& cube, cellid_t check) {
  unsigned int cubePattern = 0;

  if (cube[0] == check) cubePattern |= 1u;
  if (cube[1] == check) cubePattern |= 2u;
  if (cube[2] == check) cubePattern |= 4u;
  if (cube[3] == check) cubePattern |= 8u;
  if (cube[4] == check) cubePattern |= 16u;
  if (cube[5] == check) cubePattern |= 32u;
  if (cube[6] == check) cubePattern |= 64u;
  if (cube[7] == check) cubePattern |= 128u;

  return cubePattern;
}

/**
 * Calculates the cube bit pattern.
 *
 * @param view   The view.
 * @param ptr    The pointer.
 * @param check  The check.
 *
 * @return The cube bit pattern.
 */
template <typename FieldView, typename T>
inline unsigned long calcCubeBitPattern(const FieldView& view, T* ptr, cellid_t check) {
  unsigned long cubePattern = 0;
  for (unsigned int i = 0; i < 8; i++) {
    if (view.get(ptr, stencil::d8map[i]) == check) cubePattern |= 1u << i;
  }

  return cubePattern;
}

/**
 * Calculates eight cube bit pattern from a stencil around index.
 *
 * @param view   The view.
 * @param ptr    The pointer.
 * @param check  The cellID to check.
 *
 * @return The eight cube bit pattern.
 */
template <typename FieldView, typename T>
inline unsigned long calcCubeStencilBitPattern(const FieldView& view, T* ptr, cellid_t check) {
  unsigned long cubePattern0 = 0;
  unsigned long cubePattern1 = 0;
  unsigned long cubePattern2 = 0;

  // loop unrolling to reduce port pressure
  for (unsigned int i = 0; i < 27; i += 3) {
    if (view.get(ptr, stencil::d27map[i + 0]) == check) cubePattern0 |= cubeMask[i + 0];
    if (view.get(ptr, stencil::d27map[i + 1]) == check) cubePattern1 |= cubeMask[i + 1];
    if (view.get(ptr, stencil::d27map[i + 2]) == check) cubePattern2 |= cubeMask[i + 2];
  }

  return cubePattern0 | cubePattern1 | cubePattern2;
}

/**
 * Calculates the surface or volume change around the input with the 26 neighbors.
 *
 * @param view      The view.
 * @param ptr       The pointer.
 * @param checkID   The cellID that surface is calculated.
 * @param changeID  The cellID of the changed center.
 * @param lookup    The functor of the surface or volume lookup.

 * @tparam Functor  The functor type.
 *
 * @return Surface/volume difference (changed - current).
 */
template <typename FieldView, typename T, typename Functor>
real_t calcStencilDiff(const FieldView& view, T* ptr, cellid_t checkID, cellid_t changeID, Functor lookup) {
  if (view.get(ptr) == changeID) return 0.0;

  unsigned long cubeStencilBitPattern = calcCubeStencilBitPattern(view, ptr, checkID);

  // current surface
  real_t surface = 0.0;
  for (unsigned int i = 0; i < 8; i++) {
    unsigned long cubePattern = (cubeStencilBitPattern >> (8 * i)) & 0xFFu;
    surface += lookup(cubePattern);
  }

  unsigned long centermask = (128ul | 64ul << 8 | 32ul << 16 | 16ul << 24 | 8ul << 32 | 4ul << 40 | 2ul << 48 | 1ul << 56);
  if (checkID == changeID) {  // set centerbits
    cubeStencilBitPattern |= centermask;
  } else {  // clear centerbits
    centermask = ~centermask;
    cubeStencilBitPattern &= centermask;
  }
  // remove center bits, changed surface
  for (unsigned int i = 0; i < 8; i++) {
    unsigned long cubePattern = (cubeStencilBitPattern >> (8 * i)) & 0xFFu;
    surface -= lookup(cubePattern);
  }

  return -surface;
}

/**
 * Calculates the surface or volume on one cube.
 *
 * @param view     The view.
 * @param ptr      The pointer.
 * @param checkID  The cellID that surface is calculated.
 * @param lookup   The functor of the surface or volume lookup.
 *
 * @tparam Functor  The functor type.
 *
 * @return Surface/volume.
 */
template <typename FieldView, typename T, typename Functor>
real_t calcCube(const FieldView& view, T* ptr, cellid_t checkID, Functor lookup) {
  unsigned long cubePattern = calcCubeBitPattern(view, ptr, checkID);

  return lookup(cubePattern);
}

}  // namespace math
}  // namespace nastja
