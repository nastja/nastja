/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/matrix3.h"
#include "lib/math/vector.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <cmath>
#include <iostream>

namespace nastja {
namespace math {

/**
 * Class for bounding box.
 *
 * A lower and upper vector define the bounding box. It is ensured that lower <= upper in each component.
 *
 * @tparam T  Type of the edge elements.
 * @tparam N  Number of dimensions.
 */
template <typename T, std::size_t N = 3>
class BoundingBox {
public:
  /// @name Constructors
  /// @{
  explicit BoundingBox() : lower_{}, upper_{} {}

  explicit BoundingBox(Vector<T, N> a, Vector<T, N> b) : lower_{a}, upper_{b} {
    ensureInvariant();
  }

  explicit BoundingBox(Vector<T, N> center, T ext) : lower_{center}, upper_{center} {
    extend(ext);
  }

  template <typename U>
  explicit BoundingBox(Vector<U, N> a, Vector<U, N> b) : lower_{a}, upper_{b} {
    ensureInvariant();
  }

  template <typename U>
  explicit BoundingBox(const BoundingBox<U>& b) : lower_{b[0]}, upper_{b[1]} {}
  /// @}

  /// @name Operators
  /// @{
  bool operator==(const BoundingBox& other) const {
    return lower_ == other.lower_ && upper_ == other.upper_;
  }

  bool operator!=(const BoundingBox& other) const {
    return lower_ != other.lower_ || upper_ != other.upper_;
  }
  /// @}

  /// @name Element access
  /// @{
  Vector<T, N>& operator[](const unsigned int index) {
    return index == 0 ? lower_ : upper_;
  }

  const Vector<T, N>& operator[](const unsigned int index) const {
    return index == 0 ? lower_ : upper_;
  }

  /**
   * Provides access to the x-component of the lower corner.
   *
   * @return The x-component of the lower corner.
   */
  T xMin() const { return lower_[0]; }

  /**
   * Provides access to the y-component of the lower corner.
   *
   * @return The y-component of the lower corner.
   */
  T yMin() const {
    static_assert(N >= 2, "yMax() is only defined for two or more dimensions.");
    return lower_[1];
  }

  /**
   * Provides access to the z-component of the lower corner.
   *
   * @return The z-component of the lower corner.
   */
  T zMin() const {
    static_assert(N >= 3, "zMin() is only defined for three or more dimensions.");
    return lower_[2];
  }

  /**
   * Provides access to the x-component of the upper corner.
   *
   * @return The x-component of the upper corner.
   */
  T xMax() const { return upper_[0]; }

  /**
   * Provides access to the y-component of the upper corner.
   *
   * @return The y-component of the upper corner.
   */
  T yMax() const {
    static_assert(N >= 2, "yMax() is only defined for two or more dimensions.");
    return upper_[1];
  }

  /**
   * Provides access to the z-component of the upper corner.
   *
   * @return The z-component of the upper corner.
   */
  T zMax() const {
    static_assert(N >= 3, "zMax() is only defined for three or more dimensions.");
    return upper_[2];
  }

  /**
   * Provides acces to the lower corner.
   *
   * @return The lower corner.
   */
  Vector<T, N> lower() const { return lower_; }

  /**
   * Provides acces to the upper corner.
   *
   * @return The upper corner.
   */
  Vector<T, N> upper() const { return upper_; }
  /// @}

  /// @name Capacity
  /// @{

  /**
   * Tests for an empty bounding box.
   *
   * @return true if the bounding box is empty, false otherwise.
   */
  bool empty() const {
    bool val{false};
    for (std::size_t i = 0; i < N; i++) {
      val |= nearlyEqual(lower_[i], upper_[i]);
    }
    return val;
  }
  /// @}

  /// @name Arithmetic Operators
  /// @{

  /**
   * Translates the bounding box.
   *
   * @param x  The translation in x-direction.
   * @param y  The translation in y-direction.
   * @param z  The translation in z-direction.
   *
   * @return The translated bounding box.
   */
  BoundingBox& translate(const T x, const T y, const T z) {
    static_assert(N == 3, "translate(x, y) is only defined for three dimensions.");
    lower_[0] += x;
    lower_[1] += y;
    lower_[2] += z;

    upper_[0] += x;
    upper_[1] += y;
    upper_[2] += z;

    return *this;
  }

  /**
   * Translates the bounding box.
   *
   * @param x  The translation in x-direction.
   * @param y  The translation in y-direction.
   *
   * @return The translated bounding box.
   */
  BoundingBox& translate(const T x, const T y) {
    static_assert(N == 2, "translate(x, y) is only defined for two dimensions.");
    lower_[0] += x;
    lower_[1] += y;

    upper_[0] += x;
    upper_[1] += y;

    return *this;
  }

  /**
   * @overload
   *
   * @param v  The translation vector.
   *
   * @return The translated bounding box.
   */
  BoundingBox& translate(const Vector<T, N>& v) {
    lower_ += v;
    upper_ += v;

    return *this;
  }

  /**
   * Intersects with an other bounding box.
   *
   * @param other  The other bounding box.
   *
   * @return The intersected bounding box.
   */
  BoundingBox& intersect(const BoundingBox& other) {
    for (std::size_t i = 0; i < N; i++) {
      lower_[i] = std::max(lower_[i], other.lower_[i]);
    }
    for (std::size_t i = 0; i < N; i++) {
      upper_[i] = std::max(lower_[i], std::min(upper_[i], other.upper_[i]));
    }
    return *this;
  }

  /**
   * Extends the bounding box.
   *
   * @param e  The extending vector.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const Vector<T, N> e) {
    lower_ -= e;
    upper_ += e;

    ensureInvariant();

    return *this;
  }

  /**
   * @overload
   *
   * @param e  The extending scalar.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const T e) {
    return extend(Vector<T, N>::one * e);
  }

  /**
   * @overload
   *
   * @param a  The extending lower vector.
   * @param b  The extending upper vector.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const Vector<T, N> a, const Vector<T, N> b) {
    lower_ -= a;
    upper_ += b;

    ensureInvariant();

    return *this;
  }

  /**
   * @overload
   *
   * @param a  The extending lower scalar.
   * @param b  The extending upper scalar.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const T a, const T b) {
    return extend(Vector<T, N>::one * a, Vector<T, N>::one * b);
  }

  /**
   * Expands the BoundingBox by adding a point that is included in the expanded BoundingBox.
   *
   * @param v  A vector.
   *
   * @return The expanded BoundingBox.
   */
  BoundingBox& expand(const Vector<T, N> v) {
    for (std::size_t i = 0; i < N; i++) {
      upper_[i] = std::max(upper_[i], v[i]);
      lower_[i] = std::min(lower_[i], v[i]);
    }

    return *this;
  }

  /**
   * Rotates the box and then calculates the BoundingBox.
   *
   * @param rotation  The rotation.
   * @param center    The center.
   *
   * @return The rotation extension.
   */
  BoundingBox getRotationExtension(const Matrix3<real_t>& rotation, const Vector3<real_t>& center) {
    static_assert(N == 3, "getRotationExtension is only defined for three dimensions.");
    auto bbox = *this;
    Vector3<real_t> v{};

    for (unsigned int corner = 0; corner < 8; corner++) {
      for (unsigned int comp = 0; comp < 3; comp++) {
        v[comp] = (corner & (1u << comp)) != 0u ? upper_[comp] : lower_[comp];
      }
      v -= center;
      v = rotation.multiplyT(v);
      v += center;

      bbox.expand(static_cast<Vector3<long>>(round(v)));
    }

    return bbox;
  }

  /// @}

  /**
   * Determines if a point a is inside.
   *
   * @param a  The point.
   *
   * @return True if inside, False otherwise.
   */
  bool isInside(const Vector<T, N> a) const {
    bool val{true};
    for (std::size_t i = 0; i < N; i++) {
      val &= a[i] < upper_[i] && a[i] >= lower_[i];
    }
    return val;
  }

  bool isInside(const T x, const T y, const T z) const {
    static_assert(N == 3, "isInside(x, y, z) is only defined for three dimensions.");
    return isInside(Vector<T, N>{x, y, z});
  }

  bool isInside(const T x, const T y) const {
    static_assert(N == 2, "isInside(x, y) is only defined for two dimensions.");
    return isInside(Vector<T, N>{x, y});
  }

  /**
   * Determines if a point is inside the periodic extension.
   *
   * @param periodicBox  The periodic box.
   * @param a            The point.
   *
   * @note unsigned types can result in underflows.
   *
   * @return True if inside of the periodic extension, False otherwise.
   */
  bool isInsidePeriodic(const BoundingBox<T, N>& periodicBox, const Vector<T, N>& a) const {
    bool val{true};
    for (std::size_t i = 0; i < N; i++) {
      T shiftup   = a[i] + (periodicBox.upper()[i] - periodicBox.lower()[i]);
      T shiftdown = a[i] - (periodicBox.upper()[i] - periodicBox.lower()[i]);
      val &= (a[i] < upper_[i] && a[i] >= lower_[i]) || (shiftup < upper_[i] && shiftup >= lower_[i]) || (shiftdown < upper_[i] && shiftdown >= lower_[i]);
    }

    return val;
  }

private:
  /**
   * Ensures the invariant that lower <= upper in each component.
   */
  void ensureInvariant() {
    for (std::size_t i = 0; i < N; i++) {
      if (lower_[i] > upper_[i]) std::swap(lower_[i], upper_[i]);
    }
  }

  Vector<T, N> lower_;  ///< The coordinate vector of the lower corner.
  Vector<T, N> upper_;  ///< The coordinate vector of the upper corner.
};

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param bb  The bounding box.
 *
 * @tparam T  Type of the corner elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream& os, const BoundingBox<T>& bb) {
  return os << "[" << bb[0] << "," << bb[1] << "]";
}

/**
 * Intersects two bounding boxes
 *
 * @param a  The first bounding box.
 * @param b  The first bounding box.
 *
 * @tparam T  Type of the corner elements.
 *
 * @return The intersection of the bounding boxes.
 */
template <typename T>
BoundingBox<T> intersect(const BoundingBox<T>& a, const BoundingBox<T>& b) {
  auto lower = maxComponent(a.lower(), b.lower());
  auto upper = maxComponent(lower, minComponent(a.upper(), b.upper()));
  return BoundingBox<T>{lower, upper};
}

/**
 * Gets the squared distances to the first/last voxel in the bounding box for all 26 directions.
 *
 * @param bbox  The bounding box.
 * @param p     The point.
 *
 * @return The squared distances.
 */
template <typename T>
std::array<T, 26> borderDistancesSqr(const BoundingBox<T>& bbox, const Vector3<T>& p) {
  std::array<T, 26> distance;
  auto lower = p - bbox.lower();
  auto upper = bbox.upper() - p - 1;
  lower *= lower;
  upper *= upper;

  for (unsigned int i = 0; i < stencil::D3C26::size; i++) {
    distance[i] = 0;
    for (unsigned int d = 0; d < 3; d++) {
      if (stencil::c(d, stencil::D3C26::dir[i]) == -1) {
        distance[i] += lower[d];
      } else if (stencil::c(d, stencil::D3C26::dir[i]) == 1) {
        distance[i] += upper[d];
      }
    }
  }

  return distance;
}

}  // namespace math
}  // namespace nastja
