/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/field/field.h"
#include <array>

namespace nastja {
namespace math {

//The following three functions are for calculating the volume via Marching cubes
real_t calcVolumeMC(const std::array<cellid_t, 8>& cube, cellid_t checkID);
real_t calcVolumeMCLookup(unsigned int cubeBitPattern);
real_t calcVolumeMC(const std::array<cellid_t, 27>& stencil, cellid_t checkID);

//The following three functions are for volume calculation using 1/8 volume for MC case 2 and so on. Therefore finer grid manhattan norm
real_t calcVolumeCount(const std::array<cellid_t, 8>& cube, cellid_t checkID);
real_t calcVolumeCountLookup(unsigned int cubeBitPattern);
real_t calcVolumeCount(const std::array<cellid_t, 27>& stencil, cellid_t checkID);

/**
 * Calculates the volume change with marching cubes around the input with the 26 neighbors.
 *
 * @param view      The view.
 * @param ptr       The pointer.
 * @param checkID   The cellID that volume is calculated.
 * @param changeID  The cellID of the changed center.
 *
 * @return Volume difference (changed - current).
 */
template <typename FieldView, typename T>
real_t calcVolumeDiff(const FieldView& view, T* ptr, cellid_t checkID, cellid_t changeID) {
  return calcStencilDiff(view, ptr, checkID, changeID, calcVolumeMCLookup);
}

/**
 * Calculates the volume change with marching cubes around the input with the 26 neighbors.
 *
 * @param view      The view.
 * @param ptr       The pointer.
 * @param checkID   The cellID that volume is calculated.
 * @param changeID  The cellID of the changed center.
 *
 * @return Volume difference (changed - current).
 */
template <typename FieldView, typename T>
real_t calcVolumeCountDiff(const FieldView& view, T* ptr, cellid_t checkID, cellid_t changeID) {
  return calcStencilDiff(view, ptr, checkID, changeID, calcVolumeCountLookup);
}

}  // namespace math
}  // namespace nastja
