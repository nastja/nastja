/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace nastja {
namespace math {

/**
 * Integer division with rounding to closest.
 *
 * @param n  The dividend.
 * @param d  The divisor.
 *
 * @return The quotient rounded to nearest, 0.5 -> 1 and -0.5 -> -1.
 */

constexpr int divRoundClosest(const int n, const int d) {
  return ((n < 0) ^ (d < 0)) ? ((n - d / 2) / d) : ((n + d / 2) / d);
}

constexpr long divRoundClosest(const long n, const long d) {
  return ((n < 0) ^ (d < 0)) ? ((n - d / 2) / d) : ((n + d / 2) / d);
}

/**
 * Integer division with round half up
 *
 * @param n  The dividend.
 * @param d  The divisor.
 *
 * @return The quotient rounded half up, 0.5 -> 1 and -0.5 -> 0.
 */
constexpr int divRoundHalfUp(int n, int d) {
  return ((n < 0) ^ (d < 0)) ? ((n - n * d + d / 2) / d + n) : ((n + d / 2) / d);
}

constexpr long divRoundHalfUp(long n, long d) {
  return ((n < 0) ^ (d < 0)) ? ((n - n * d + d / 2) / d + n) : ((n + d / 2) / d);
}

/**
 * Interger division n/d rounds up. May overflow for n+d.
 *
 * @param n  The dividend.
 * @param d  The divisor.
 *
 * @return ceil(n/d)
 */
constexpr unsigned long divRoundUp(const unsigned long n, const unsigned long d) {
  return (n + d - 1) / d;
}

/**
 * Counts the number of tailing zeros.
 *
 * @param n  The integer n.
 *
 * @return Number of tailing zeros.
 */
constexpr int countTailingZeros(unsigned int n) {
  if (n == 0) return 32;

  int r = 0;
  while ((n & 0x1u) == 0) {
    n >>= 1u;
    r++;
  }

  return r;
}

/**
 * Counts the number of leading zeros.
 *
 * @param n  The integer n.
 *
 * @return Number of leading zeros.
 */
constexpr unsigned int countLeadingZeros(unsigned int n) {
  unsigned int w = 8 * sizeof(unsigned int);

  if (n == 0) return w;

  unsigned int t = 1u << (w - 1);
  unsigned int r = 0;
  while ((n & t) == 0) {
    t >>= 1u;
    r++;
  }

  return r;
}

/**
 * Determines whether the specified n is power of two.
 *
 * @param n  The integer n.
 *
 * @return True if the specified n is power of two, False otherwise.
 */
constexpr bool isPowerOfTwo(unsigned int n) {
  return (n != 0) && ((n & (n - 1)) == 0);
}

/**
 * Calculates the natural modulo, i.e., -1 mod 4 = 3
 *
 * @param n  The integer n.
 * @param d  The divisor.
 *
 * @return n mod d.
 */
constexpr long mod(long n, long d) {
  return ((n % d) + d) % d;
}

/**
 * Calculates the power of two.
 *
 * @param n  The exponent.
 *
 * @return 2^n.
 */
constexpr unsigned int powerOfTwo(unsigned int n) {
  return (1u >> n);
}

/**
 * Determines if a bit is set.
 *
 * @param bits  The bits.
 * @param n     The bit to check.
 *
 * @return True if set, False otherwise.
 */
constexpr unsigned int isSet(unsigned int bits, unsigned int n) {
  return bits & (1u << n);
}

}  // namespace math
}  // namespace nastja
