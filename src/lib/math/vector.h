/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/math/utils.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <sstream>
#include <type_traits>

namespace nastja {
namespace math {

/**
 * Class for n dimensional Vector.
 *
 * @tparam T  The data type of the vector elements.
 */
template <typename T, std::size_t N>
class Vector {
  static_assert(std::is_arithmetic<T>::value, "The Vector class needs an arithmetic data type.");

public:
  /// Define the type of the length, this can not be an integral type because of the sqrt.
  using length_type = typename std::conditional<std::is_integral<T>::value, real_t, T>::type;

  /// @name Constructors
  /// @{

  /**
   * Constructs a new instance.
   */
  explicit Vector() = default;

  /**
   * Constructs a new instance.
   *
   * @param l  The initializer list, should have the length N.
   */
  Vector(std::initializer_list<T> l) { set(l); }

  /**
   * Constructs a new instance.
   *
   * @param initial  The initial data in a raw pointer. Must contain N elements.
   */
  explicit Vector(const T* initial) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] = initial[i];
    }
  }

  /**
   * Constructs a new instance.
   *
   * @param initial  The initial from another vector.
   *
   * @tparam U  The data type of the other vector elements
   */
  template <typename U>
  explicit Vector(const Vector<U, N>& initial) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] = static_cast<T>(initial[i]);
    }
  }

  /**
   * Constructs a new instance.
   *
   * @param initial  The initial data from an array.
   */
  explicit Vector(std::array<T, N> initial) : v_{initial} {}
  /// @}

  /// @name Conversions
  /// @{
  explicit operator std::array<T, N>() const { return v_; }
  /// @}

  /// @name Iterators
  /// @{
  auto begin() const noexcept { return v_.begin(); }
  auto begin() noexcept { return v_.begin(); }
  auto end() const noexcept { return v_.end(); }
  auto end() noexcept { return v_.end(); }
  /// @}

  /// @name Operators
  /// @{

  /**
   * Negation operator.
   *
   * @return The result of the negation.
   */
  Vector operator-() const {
    Vector<T, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = -v_[i];
    }
    return r;
  }

  /**
   * Addition operator.
   *
   * @param other  The other vector.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the addition.
   */
  template <typename U>
  Vector<typename std::common_type<T, U>::type, N> operator+(const Vector<U, N>& other) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] + other[i];
    }
    return r;
  }

  /**
   * Subtraction operator.
   *
   * @param other  The other vector.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the subtraction.
   */
  template <typename U>
  Vector<typename std::common_type<T, U>::type, N> operator-(const Vector<U, N>& other) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] - other[i];
    }
    return r;
  }

  /**
   * Element-wise multiplication operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the multiplication.
   */
  template <typename U>
  Vector<typename std::common_type<T, U>::type, N> operator*(const Vector<U, N>& other) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] * other[i];
    }
    return r;
  }

  /**
   * Element-wise division operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the division.
   */
  template <typename U>
  Vector<typename std::common_type<T, U>::type, N> operator/(const Vector<U, N>& other) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] / other[i];
    }
    return r;
  }

  /**
   * Element-wise modulus operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the modulus.
   */
  template <typename U, typename = typename std::enable_if<std::is_integral<typename std::common_type<T, U>::type>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator%(const Vector<U, N>& other) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] % other[i];
    }
    return r;
  }

  /**
   * Addition operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the addition.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator+(U scalar) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] + scalar;
    }
    return r;
  }

  /**
   * Subtraction operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the subtraction.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator-(U scalar) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] - scalar;
    }
    return r;
  }

  /**
   * Multiplication operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the multiplication.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator*(U scalar) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] * scalar;
    }
    return r;
  }

  /**
   * Division operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the division.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator/(U scalar) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] / scalar;
    }
    return r;
  }

  /**
   * Modulus operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the modulus.
   */
  template <typename U, typename = typename std::enable_if<std::is_integral<typename std::common_type<T, U>::type>::value>::type>
  Vector<typename std::common_type<T, U>::type, N> operator%(U scalar) const {
    Vector<typename std::common_type<T, U>::type, N> r{};
    for (std::size_t i = 0; i < N; i++) {
      r[i] = v_[i] % scalar;
    }
    return r;
  }

  /**
   * Calculates the dot product @f$c = a \cdot b@f$.
   *
   * @param other  The other vector.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return A scalar of the dot product.
   */
  template <typename U>
  typename std::common_type<T, U>::type dotProduct(const Vector<U, N>& other) const {
    typename std::common_type<T, U>::type sum = 0.0;
    for (std::size_t i = 0; i < N; i++) {
      sum += v_[i] * other[i];
    }
    return sum;
  }

  /**
   * Calculates the cross product @f$c = a \times b@f$.
   *
   * @param other  The other vector.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return A vector of the cross product of a with b.
   */
  template <typename U>
  Vector& crossProduct(const Vector<U, N>& other) {
    static_assert(N == 3, "The cross product is only defined in three dimensions.");
    T tmpv1 = v_[2] * other[0] - v_[0] * other[2];
    T tmpv2 = v_[0] * other[1] - v_[1] * other[0];

    v_[0] = v_[1] * other[2] - v_[2] * other[1];
    v_[1] = tmpv1;
    v_[2] = tmpv2;

    return *this;
  }

  /**
   * Addition assignment operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the addition assignment.
   */
  template <typename U>
  Vector& operator+=(const Vector<U, N>& other) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] += other[i];
    }
    return *this;
  }

  /**
   * Subtraction assignment operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the subtraction assignment
   */
  template <typename U>
  Vector& operator-=(const Vector<U, N>& other) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] -= other[i];
    }
    return *this;
  }

  /**
   * Element-wise multiplication assignment operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the multiplication assignment
   */
  template <typename U>
  Vector& operator*=(const Vector<U, N>& other) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] *= other[i];
    }
    return *this;
  }

  /**
   * Element-wise division assignment operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the division assignment
   */
  template <typename U>
  Vector& operator/=(const Vector<U, N>& other) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] /= other[i];
    }
    return *this;
  }

  /**
   * Element-wise modulus assignment operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the modulus assignment
   */
  template <typename U, typename = typename std::enable_if<std::is_integral<typename std::common_type<T, U>::type>::value>::type>
  Vector& operator%=(const Vector<U, N>& other) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] %= other[i];
    }
    return *this;
  }

  /**
   * Addition assignment operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the addition assignment.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector& operator+=(U scalar) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] += scalar;
    }
    return *this;
  }

  /**
   * Subtraction assignment operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the subtraction assignment.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector& operator-=(U scalar) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] -= scalar;
    }
    return *this;
  }

  /**
   * Multiplication assignment operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the multiplication assignment.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector& operator*=(U scalar) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] *= scalar;
    }
    return *this;
  }

  /**
   * Division assignment operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the division assignment.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector& operator/=(U scalar) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] /= scalar;
    }
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_integral<typename std::common_type<T, U>::type>::value>::type>
  Vector& operator%=(U scalar) {
    for (std::size_t i = 0; i < N; i++) {
      v_[i] %= scalar;
    }
    return *this;
  }

  /**
   * Equality operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the equality
   */
  template <typename U>
  bool operator==(const Vector<U, N>& other) const {
    bool val{true};
    for (std::size_t i = 0; i < N; i++) {
      val &= nearlyEqual(v_[i], other[i]);
    }
    return val;
  }

  /**
   * Equality operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the equality.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator==(U scalar) const {
    bool val{true};
    for (std::size_t i = 0; i < N; i++) {
      val &= nearlyEqual(v_[i], scalar);
    }
    return val;
  }

  /**
   * Inequality operator.
   *
   * @param other  The other.
   *
   * @tparam U  The data type of the other vector elements.
   *
   * @return The result of the inequality.
   */
  template <typename U>
  bool operator!=(const Vector<U, N>& other) const {
    bool val{false};
    for (std::size_t i = 0; i < N; i++) {
      val |= !nearlyEqual(v_[i], other[i]);
    }
    return val;
  }

  /**
   * Inequality operator.
   *
   * @param scalar  The scalar.
   *
   * @tparam U  The data type of the scalar.
   *
   * @return The result of the inequality.
   */
  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator!=(U scalar) const {
    bool val{false};
    for (std::size_t i = 0; i < N; i++) {
      val |= !nearlyEqual(v_[i], scalar);
    }
    return val;
  }
  /// @}

  /// @name Element access
  /// @{

  /**
   * Array indexer operator.
   *
   * @param index  The index.
   *
   * @return The result of the array indexer.
   */
  T& operator[](unsigned int index) {
    return v_[index];
  }

  /**
   * Array indexer operator.
   *
   * @param index  The index.
   *
   * @return The result of the array indexer.
   */
  const T& operator[](unsigned int index) const {
    return v_[index];
  }

  /**
   * Named access to x-coordinate.
   *
   * @return Reference to the x-coordinate.
   */
  T& x() {
    return v_[0];
  }

  /**
   * Named access to x-coordinate.
   *
   * @return Reference to the x-coordinate.
   */
  const T& x() const {
    return v_[0];
  }

  /**
   * Named access to y-coordinate.
   *
   * @return Reference to the y-coordinate.
   */
  T& y() {
    static_assert(N >= 2, "y() access is only defined for three or more dimensions.");
    return v_[1];
  }

  /**
   * Named access to y-coordinate.
   *
   * @return Reference to the y-coordinate.
   */
  const T& y() const {
    static_assert(N >= 2, "y() access is only defined for three or more dimensions.");
    return v_[1];
  }

  /**
   * Named access to z-coordinate.
   *
   * @return Reference to the z-coordinate.
   */
  T& z() {
    static_assert(N >= 3, "z() access is only defined for three or more dimensions.");
    return v_[2];
  }

  /**
   * Named access to z-coordinate.
   *
   * @return Reference to the z-coordinate.
   */
  const T& z() const {
    static_assert(N >= 3, "z() access is only defined for three or more dimensions.");
    return v_[2];
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  T* data() noexcept {
    return v_.data();
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  const T* data() const noexcept {
    return v_.data();
  }

  /**
   * Returns the number of elements in the container.
   *
   * @return Returns the number of elements in the container.
   */
  std::size_t size() const noexcept {
    return v_.size();
  }

  /**
   * Set the vector.
   *
   * @param l  The initializer list for new values.
   */
  void set(std::initializer_list<T> l) {
    assert(l.size() == N);  // Vector must be set/initilized with N elements.
    std::copy_n(std::begin(l), std::min(N, l.size()), std::begin(v_));
  }
  /// @}

  /// @name Arithmetic operators
  /// @{

  /**
   * Calculates the square of the length.
   *
   * @return The squared length of the vector.
   */
  T lengthSqr() const {
    return dotProduct(*this);
  }

  /**
   * Calculates the length.
   *
   * @return The length of the vector.
   */
  length_type length() const {
    return std::sqrt(static_cast<length_type>(dotProduct(*this)));
  }

  /**
   * Normalize the vector.
   *
   * The normalization of a Vector is only possible when its type and its length_type are the same. For Vector of
   * integral types use getNormalized() instead.
   *
   * @return The normalized vector.
   */
  Vector& normalize() {
    static_assert(std::is_same<length_type, T>::value, "Normalization of a Vector is not possible when type differs from its length_type.");

    const length_type len{length()};
    assert(len != 0.0);
    const length_type lenR{length_type(1) / len};

    return operator*=(lenR);
  }

  /**
   * Gets the normalized vector.
   *
   * @return The normalized vector.
   */
  Vector<length_type, N> getNormalized() const {
    const length_type len{length()};
    assert(len != 0.0);
    const length_type lenR{length_type(1) / len};

    Vector<length_type, N> r{};
    for (unsigned int i = 0; i < N; i++) {
      r[i] = static_cast<length_type>(v_[i]) * lenR;
    }
    return r;
  }
  /// @}

  static const Vector zero;  ///< Zero vector.
  static const Vector one;   ///< All one vector.

private:
  std::array<T, N> v_{};  ///< Array of length N, holding the vector with element type T.
};

template <typename T, std::size_t N>
const Vector<T, N> Vector<T, N>::zero{};

template <typename T, std::size_t N>
const Vector<T, N> Vector<T, N>::one{
    []() {
      Vector<T, N> v;
      for (std::size_t i = 0; i < N; i++) {
        v[i] = T{1};
      }
      return v;
    }()};

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param v   The vector.
 *
 * @tparam T  The data type of the vector elements.
 *
 * @return Reference to the output stream.
 */
template <typename T, std::size_t N>
inline std::ostream& operator<<(std::ostream& os, const Vector<T, N>& v) {
  os << "[";
  for (std::size_t i = 0; i < N; i++) {
    os << v[i] << (i == N - 1 ? "" : ",");
  }
  return os << "]";
}

/**
 * Equality operator.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 * @tparam N  The size of the vector.
 *
 * @return The result of the equality.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator==(U scalar, const Vector<T, N>& v) {
  return v == scalar;
}

/**
 * Inequality operator.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 * @tparam N  The size of the vector.
 *
 * @return The result of the inequality.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator!=(U scalar, const Vector<T, N>& v) {
  return v != scalar;
}

/**
 * Addition operator.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 * @tparam N  The size of the vector.
 *
 * @return The result of the addition.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector<typename std::common_type<T, U>::type, N> operator+(U scalar, const Vector<T, N>& v) {
  return v + scalar;
}

/**
 * Subtraction operator.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 * @tparam N  The size of the vector.
 *
 * @return The result of the subtraction.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector<typename std::common_type<T, U>::type, N> operator-(U scalar, const Vector<T, N>& v) {
  Vector<typename std::common_type<T, U>::type, N> r{};
  for (std::size_t i = 0; i < N; i++) {
    r[i] = scalar - v[i];
  }
  return r;
}

/**
 * Scalar multiplication.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 *
 * @return The vector scalar product of common type.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector<typename std::common_type<T, U>::type, N> operator*(U scalar, const Vector<T, N>& v) {
  return v * scalar;
}

/**
 * Division operator.
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  The data type of the vector elements.
 * @tparam U  The data type of the scalar.
 * @tparam N  The size of the vector.
 *
 * @return The result of the division
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector<typename std::common_type<T, U>::type, N> operator/(U scalar, const Vector<T, N>& v) {
  Vector<typename std::common_type<T, U>::type, N> r{};
  for (std::size_t i = 0; i < N; i++) {
    r[i] = scalar / v[i];
  }
  return r;
}

/**
 * Calculates the cross product @f$c = a \times b@f$.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  The data type of the vector a elements.
 * @tparam U  The data type of the vector b elements.
 *
 * @return A vector of the cross product of a with b of common type.
 */
template <typename T, typename U, std::size_t N, typename = typename std::enable_if<N == 3>::type>
inline Vector<typename std::common_type<T, U>::type, N> crossProduct(const Vector<T, N>& a, const Vector<U, N>& b) {
  return Vector<typename std::common_type<T, U>::type, N>{a[1] * b[2] - a[2] * b[1],
                                                          a[2] * b[0] - a[0] * b[2],
                                                          a[0] * b[1] - a[1] * b[0]};
}

/**
 * Calculates the dot product @f$c = a \cdot b@f$.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  The data type of the vector a elements.
 * @tparam U  The data type of the vector b elements.
 *
 * @return The dot product of a with b of common type.
 */
template <typename T, typename U, std::size_t N>
inline typename std::common_type<T, U>::type dotProduct(const Vector<T, N>& a, const Vector<U, N>& b) {
  typename std::common_type<T, U>::type sum = 0.0;
  for (std::size_t i = 0; i < N; i++) {
    sum += a[i] * b[i];
  }
  return sum;
}

/**
 * Calculates the element-wise minimum.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  The data type of the vector a elements.
 * @tparam U  The data type of the vector b elements.
 * @tparam N  The size of the vector.
 *
 * @return The element-wise minimum.
 */
template <typename T, typename U, std::size_t N>
inline Vector<typename std::common_type<T, U>::type, N> minComponent(const Vector<T, N>& a, const Vector<U, N>& b) {
  Vector<typename std::common_type<T, U>::type, N> v;
  for (std::size_t i = 0; i < N; i++) {
    v[i] = std::min(a[i], b[i]);
  }
  return v;
}

/**
 * Calculates the element-wise maximum.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  The data type of the vector a elements.
 * @tparam U  The data type of the vector b elements.
 * @tparam N  The size of the vector.
 *
 * @return The element-wise maximum.
 */
template <typename T, typename U, std::size_t N>
inline Vector<typename std::common_type<T, U>::type, N> maxComponent(const Vector<T, N>& a, const Vector<U, N>& b) {
  Vector<typename std::common_type<T, U>::type, N> v;
  for (std::size_t i = 0; i < N; i++) {
    v[i] = std::max(a[i], b[i]);
  }
  return v;
}

/**
 * Element-wise round.
 *
 * @param a  Vector a.
 *
 * @tparam T  The data type of the vector a elements.
 * @tparam N  The size of the vector.
 *
 * @return The element-wise rounded vector.
 */
template <typename T, typename LengthType = typename std::conditional<std::is_integral<T>::value, real_t, T>::type, typename std::size_t N>
inline Vector<LengthType, N> round(const Vector<T, N>& a) {
  Vector<LengthType, N> v{};
  for (std::size_t i = 0; i < N; i++) {
    v[i] = std::round(a[i]);
  }
  return v;
}

template <typename T>
using Vector2 = Vector<T, 2>;

template <typename T>
using Vector3 = Vector<T, 3>;

}  // namespace math
}  // namespace nastja
