/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "calculatesurface.h"

namespace nastja {
namespace math {

/**
 * The surfaces of the 15 representatives in the marching cubes algorithm.
 * Surfaces are calculated from the individual triangles.
 */
constexpr static std::array<real_t, 15> surfaceCases{
    0,                     // Case  0: No or all bits are set.
    0.21650635094610965,   // Case  1: One or 7 bits are set S = sqrt(3)/8
    0.7071067811865476,    // Case  2: Two bits on one edge  S = sqrt(2)/2
    0.4330127018922193,    // Case  3: Two times case 1      S = sqrt(3)/4
    0.4330127018922193,    // Case  4: Two times case 1      S = sqrt(3)/4
    1.149519052838328985,  // Case  5:                       S = 1/2 + 3*sqrt(3)/8
    0.9236131321326571,    // Case  6: Case 1 plus case 2    S = sqrt(3)/8 + sqrt(2)/2
    0.6495190528383289,    // Case  7: Three times case 1    S = 3*sqrt(3)/8
    1.0,                   // Case  8: One plane             S = 1
    1.299038105676658,     // Case  9:                       S = (3*sqrt(3))/4
    1.5731321849709861,    // Case 10:                       S = 1/sqrt(2)+ sqrt(3)/2
    1.5731321849709861,    // Case 11: Mirror of case 10     S = sqrt(3)/2 + 1/sqrt(2)
    1.3660254037844386,    // Case 12: Case 5 plus case 1    S = (1+sqrt(3))/2
    1.4142135623730950,    // Case 13: Two times case 2      S = sqrt(2)
    0.8660254037844386     // Case 14: Four times case 1     S = sqrt(3)/2
};

/**
 * Look for an representative for the given cube bit pattern and lookup the surface in the surfaceCases Table.
 *
 * @param cubeBitPattern  The cube bit pattern.
 *
 * @return The surface of the representative.
 */
constexpr real_t surface(unsigned int cubeBitPattern) noexcept {
  return surfaceCases[findRepresentative(cubeBitPattern)];
}

///< LUT of the surface area of all 256 possible cube bit pattern.
constexpr static std::array<real_t, 256> surfaceArea{
    surface(0), surface(1), surface(2), surface(3), surface(4), surface(5), surface(6), surface(7), surface(8), surface(9),
    surface(10), surface(11), surface(12), surface(13), surface(14), surface(15), surface(16), surface(17), surface(18), surface(19),
    surface(20), surface(21), surface(22), surface(23), surface(24), surface(25), surface(26), surface(27), surface(28), surface(29),
    surface(30), surface(31), surface(32), surface(33), surface(34), surface(35), surface(36), surface(37), surface(38), surface(39),
    surface(40), surface(41), surface(42), surface(43), surface(44), surface(45), surface(46), surface(47), surface(48), surface(49),
    surface(50), surface(51), surface(52), surface(53), surface(54), surface(55), surface(56), surface(57), surface(58), surface(59),
    surface(60), surface(61), surface(62), surface(63), surface(64), surface(65), surface(66), surface(67), surface(68), surface(69),
    surface(70), surface(71), surface(72), surface(73), surface(74), surface(75), surface(76), surface(77), surface(78), surface(79),
    surface(80), surface(81), surface(82), surface(83), surface(84), surface(85), surface(86), surface(87), surface(88), surface(89),
    surface(90), surface(91), surface(92), surface(93), surface(94), surface(95), surface(96), surface(97), surface(98), surface(99),
    surface(100), surface(101), surface(102), surface(103), surface(104), surface(105), surface(106), surface(107), surface(108), surface(109),
    surface(110), surface(111), surface(112), surface(113), surface(114), surface(115), surface(116), surface(117), surface(118), surface(119),
    surface(120), surface(121), surface(122), surface(123), surface(124), surface(125), surface(126), surface(127), surface(128), surface(129),
    surface(130), surface(131), surface(132), surface(133), surface(134), surface(135), surface(136), surface(137), surface(138), surface(139),
    surface(140), surface(141), surface(142), surface(143), surface(144), surface(145), surface(146), surface(147), surface(148), surface(149),
    surface(150), surface(151), surface(152), surface(153), surface(154), surface(155), surface(156), surface(157), surface(158), surface(159),
    surface(160), surface(161), surface(162), surface(163), surface(164), surface(165), surface(166), surface(167), surface(168), surface(169),
    surface(170), surface(171), surface(172), surface(173), surface(174), surface(175), surface(176), surface(177), surface(178), surface(179),
    surface(180), surface(181), surface(182), surface(183), surface(184), surface(185), surface(186), surface(187), surface(188), surface(189),
    surface(190), surface(191), surface(192), surface(193), surface(194), surface(195), surface(196), surface(197), surface(198), surface(199),
    surface(200), surface(201), surface(202), surface(203), surface(204), surface(205), surface(206), surface(207), surface(208), surface(209),
    surface(210), surface(211), surface(212), surface(213), surface(214), surface(215), surface(216), surface(217), surface(218), surface(219),
    surface(220), surface(221), surface(222), surface(223), surface(224), surface(225), surface(226), surface(227), surface(228), surface(229),
    surface(230), surface(231), surface(232), surface(233), surface(234), surface(235), surface(236), surface(237), surface(238), surface(239),
    surface(240), surface(241), surface(242), surface(243), surface(244), surface(245), surface(246), surface(247), surface(248), surface(249),
    surface(250), surface(251), surface(252), surface(253), surface(254), surface(255)};

real_t calcSurfaceMC(const std::array<cellid_t, 27>& stencil, cellid_t checkID) {
  real_t surface = 0.0;

  for (unsigned int xoffset = 0; xoffset <= 1; xoffset++) {
    for (unsigned int yoffset = 0; yoffset <= 1; yoffset++) {
      for (unsigned int zoffset = 0; zoffset <= 1; zoffset++) {
        auto cube = cubeFromStencil(stencil, xoffset, yoffset, zoffset);

        surface += calcSurfaceMC(cube, checkID);
      }
    }
  }

  return surface;
}

/**
 * Calculates the surface for the inputCell checkID from one cube (2 x 2 x 2).
 *
 * @param cube     The cube.
 * @param checkID  Check for this value.
 *
 * @return The surface area.
 */
real_t calcSurfaceMC(const std::array<cellid_t, 8>& cube, cellid_t checkID) {
  return calcSurfaceMCLookup(calcCubeBitPattern(cube, checkID));
}

/**
 * @overload
 *
 * @param  cubeBitPattern  The cube index bit pattern.
 *
 * @return Surface area.
 */
real_t calcSurfaceMCLookup(unsigned int cubeBitPattern) {
  static_assert(surfaceArea[0] == 0.0, "This assert makes sure that we use constexpr.");
  return surfaceArea[cubeBitPattern];
}

}  // namespace math
}  // namespace nastja
