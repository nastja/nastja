/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/math/calculatecubes.h"
#include "lib/stencil/D3C6.h"
#include <array>

namespace nastja {
namespace math {

real_t calcSurfaceMC(const std::array<cellid_t, 8>& cube, cellid_t checkID);
real_t calcSurfaceMCLookup(unsigned int cubeBitPattern);

/**
 * Calculates the surface change with marching cubes around the input with the 26 neighbors.
 *
 * @param view      The view.
 * @param ptr       The pointer.
 * @param checkID   The cellID that surface is calculated.
 * @param changeID  The cellID of the changed center.
 *
 * @return Surface area difference (changed - current).
 */
template <typename FieldView, typename T>
real_t calcSurfaceMCDiff(const FieldView& view, T* ptr, cellid_t checkID, cellid_t changeID) {
  return calcStencilDiff(view, ptr, checkID, changeID, calcSurfaceMCLookup);
}

/**
 * Calculates the surface with marching cubes on one cube.
 *
 * @param view     The view.
 * @param ptr      The pointer.
 * @param checkID  The cellID that surface is calculated.
 *
 * @return Surface area.
 */
template <typename FieldView, typename T>
real_t calcSurfaceMC(const FieldView& view, T* ptr, cellid_t checkID) {
  return calcCube(view, ptr, checkID, calcSurfaceMCLookup);
}

real_t calcSurfaceMC(const std::array<cellid_t, 27>& stencil, cellid_t checkID); // TODO make deprecated

template <typename FieldView, typename T>
int calcSurfaceFaces(const FieldView& view, T* ptr, T checkID) {
  int faces = 0;

  for (auto d : stencil::D3C6()) {
    if (view.get(ptr, stencil::D3C6::dir[d]) != checkID) faces++;
  }

  return faces;
}

}  // namespace math
}  // namespace nastja
