/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <array>
#include <cfloat>
#include <cmath>

namespace nastja {
namespace math {

inline real_t interpolate_byPhi(const std::array<real_t, 2> array, const real_t phi) {
  return array[0] * phi + array[1] * (1.0 - phi);
}

inline real_t h(const real_t phi) {
  real_t phi2 = phi * phi;
  return phi2 * phi * (6.0 * phi2 - 15.0 * phi + 10.0);
}

inline real_t dhdphi(const real_t phi) {
  real_t phi2 = phi * phi;
  return 30.0 * phi2 * (phi2 - 2.0 * phi + 1.0);
}

inline real_t interpolate(const real_t a, const real_t b) {
  return 0.5 * (a + b);
}

}  // namespace math
}  // namespace nastja
