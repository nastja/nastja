/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#define NASTJA_FORMAT_IS_INCLUDED

#define NASTJA_TYPE_SPECIFICATION float
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#define NASTJA_TYPE_SPECIFICATION double
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#define NASTJA_TYPE_SPECIFICATION int
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#define NASTJA_TYPE_SPECIFICATION unsigned int
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#define NASTJA_TYPE_SPECIFICATION long
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#define NASTJA_TYPE_SPECIFICATION unsigned long
#include "format_internal.h"
#undef NASTJA_TYPE_SPECIFICATION

#undef NASTJA_FORMAT_IS_INCLUDED
