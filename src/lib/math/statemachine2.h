/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <map>

namespace nastja {

/**
 * This class describes a state machine.
 *
 * Transitions must be added and the initial state must be set for using.
 *
 * @tparam State  Type of the states.
 * @tparam Event  Type of the events.
 */
template <typename State, typename Event>
class StateMachine2 {
public:
  explicit StateMachine2() = default;

  /**
   * Adds a transition from state by event to new state.
   *
   * @param state     The state.
   * @param event     The event.
   * @param newState  The new state.
   */
  void addTransition(const State state, const Event event, const State newState) {
    transitions_[state][event] = newState;
  }

  /**
   * Sets the state.
   *
   * @param state  The state.
   */
  void setState(const State state) { currentState_ = state; }

  /**
   * Gets the current state.
   *
   * @return The state.
   */
  State getState() const { return currentState_; }

  /**
   * Processes an event on the current state.
   *
   * @param event  The event.
   */
  void processEvent(const Event event) {
    if (transitions_[currentState_].count(event) == 0) return;

    currentState_ = transitions_[currentState_][event];
  }

private:
  State currentState_;  ///< The current state.

  std::map<State, std::map<Event, State>> transitions_;  ///< Transitions from state by event to state.
};

}  // namespace nastja
