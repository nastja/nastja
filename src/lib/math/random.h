/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace nastja {
namespace math {

/**
 * Uniform discrete distribution for random numbers, from gcc to have a cross-platform implementation.
 *
 * A discrete random distribution on the range @f$[min, max]@f$ with equal
 * probability throughout the range.
 */
template <typename T = int>
class UniformInt {
public:
  /**
   * Constructs a uniform distribution object.
   */
  explicit UniformInt(T min = 0, T max = 9)
      : min_{min}, max_{max} {}

  /**
   * Gets the inclusive lower bound of the distribution range.
   */
  T min() const { return min_; }

  /**
   * Gets the inclusive upper bound of the distribution range.
   */
  T max() const { return max_; }

  /**
   * Sets the minimum.
   *
   * @param min  The minimum.
   */
  void setMin(T min) { min_ = min; }

  /**
   * Sets the maximum.
   *
   * @param max  The maximum.
   */
  void setMax(T max) { max_ = max; }

  /**
   * Resets the distribution state.
   *
   * Does nothing for the uniform integer distribution.
   */
  void reset() {}

  /**
   * Gets a uniformly distributed random number in the range
   * @f$(min, max)@f$.
   */
  template <typename UniformRandomNumberGenerator>
  T operator()(UniformRandomNumberGenerator& urng) {
    return impl(urng, min_, max_);
  }

  /**
   * Gets a uniform random number in the range @f$[0, n)@f$.
   *
   * This function is aimed at use with std::random_shuffle.
   */
  template <typename UniformRandomNumberGenerator>
  T operator()(UniformRandomNumberGenerator& urng, T n) {
    return impl(urng, 0, n - 1);
  }

private:
  template <typename UniformRandomNumberGenerator>
  T impl(UniformRandomNumberGenerator& urng, T min, T max) {
    return T(double(urng() - urng.min()) / double(urng.max() - urng.min() + 1.0) * (max - min + 1)) + min;
  }

  T min_;  ///< The lower bound of the distribution.
  T max_;  ///< The upper bound of the distribution.
};

/**
 * Uniform continuous distribution for random numbers.
 *
 * A continuous random distribution on the range [min, max) with equal
 * probability throughout the range.
 */
template <typename T = double>
class UniformReal {
public:
  /**
   * Constructs a UniformReal object.
   *
   * @param min  The lower bound of the distribution.
   * @param max  The upper bound of the distribution.
   */
  explicit UniformReal(T min = T(0), T max = T(1))
      : min_{min}, max_{max} {}

  T min() const { return min_; }

  T max() const { return max_; }

  void setMin(T min) { min_ = min; }

  void setMax(T max) { max_ = max; }

  void reset() {}

  template <typename UniformRandomNumberGenerator>
  T operator()(UniformRandomNumberGenerator& urng) {
    return (T(urng() - urng.min()) / T(urng.max() - urng.min()) * (max_ - min_)) + min_;
  }

private:
  T min_;  ///< The lower bound of the distribution.
  T max_;  ///< The upper bound of the distribution.
};

}  // namespace math
}  // namespace nastja
