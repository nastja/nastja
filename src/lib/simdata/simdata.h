/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datatypes.h"
#include "lib/config/cmdline.h"
#include "lib/config/config.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/simdata/mpidata.h"
#include "lib/storage/extensionstorage.h"
#include "fmt/core.h"
#include <cassert>
#include <memory>
#include <random>

namespace nastja {

namespace block {
class BlockManager;
}

namespace config {
class Config;
}

class SimData {
public:
  explicit SimData(config::Config& config, MPIData& mpiData);

  unsigned long getTimesteps() const {
    return timesteps_;
  }

  void setTimesteps(unsigned long timesteps) {
    timesteps_ = timesteps;
  }

  real_t getSimulatedTime() const {
    return totalTime;
  }

  void nextTimestep() {
    timestep++;
    totalTime += deltat;
  }

  config::Config& getConfig() const;
  const config::CommandLine& getCmdLine() const;

  void registerBlockmanager(block::BlockManager* blockmanager) { blockmanager_ = blockmanager; }

  unsigned long createSeed() const;
  void seedAll(unsigned long seed);
  unsigned long getSeed(const nlohmann::json::json_pointer& jptr);

  void dump(Archive& ar) const;
  void undump(Archive& ar);

  real_t deltax;              ///< The lattice discretization delta x.
  real_t deltat;              ///< The time discretization delta x.
  real_t deltax_R;            ///< The reciprocal delta x.
  unsigned long timestep{0};  ///< The current time-step.
  real_t totalTime{0};        ///< The integrated time.
  MPIData& mpiData;           ///< The reference to the MPI data object.

  bool outputFilling{false};  ///< Flag for output the initial filling.

  std::mt19937_64 generator{};        ///< The Mersenne twister prime number generator, block-wise local seed.
  std::mt19937_64 globalGenerator{};  ///< The Mersenne twister prime number generator, global seed for filling.

  /**
   * Gets the data container.
   *
   * @tparam Container  The data container type.
   *
   * @return Reference to the appended data container.
   */
  template <typename Container, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  Container& getData() {
    if (!appendedData_.has<Container>()) {
      auto type = std::type_index(typeid(Container));
      throw std::runtime_error(fmt::format("The DataContainer '{}' is not initialized. Use initData() before getData().", type.name()));
    }

    return appendedData_.get<Container>();
  }

  /**
   * Determines if the data container is initialized.
   *
   * @tparam Container  The data container type.
   *
   * @return True if the data container is initialized, False otherwise.
   */
  template <typename Container, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  bool hasData() const {
    return appendedData_.has<Container>();
  }

  /**
   * Initializes the data.
   *
   * @param args  The packed arguments.
   *
   * @tparam Container  The data container type.
   * @tparam ARGs       The parameter pack.
   *
   * @return Reference to the appended data container.
   */
  template <typename Container, typename... ARGs, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  Container& initData(ARGs&&... args) {
    return appendedData_.init<Container>(std::forward<ARGs>(args)...);
  }

private:
  unsigned long baseSeed_;                      ///< The global random base seed.
  block::BlockManager* blockmanager_{nullptr};  ///< The pointer to the block manager.
  config::Config& config_;                      ///< The reference to the configure file.

  unsigned long timesteps_{0};  ///< The number of total time-steps.

  ExtensionStorage<DataContainer> appendedData_;  ///< Storage for appended data objects.
};

}  // namespace nastja
