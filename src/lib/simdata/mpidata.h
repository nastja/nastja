/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <mpi.h>

namespace nastja {

extern const MPI_Datatype MPI_NASTJA_REAL;
extern const MPI_Datatype MPI_NASTJA_INTEGER;

/**
 * The maximum MPI tag is 2^31-1 for IntelMPI, 2^30 for openMPI < 4.0, 2^23-1 for openMPI >= 4.0, and 2^29-1 for
 * ParaStationMPI. For each side of each block an unique tag is needed. The tags highest available 5 bits
 * (0x00'F8'00'00) represents the side. The lowest 18 bits (0x00'03'FF'FF) hold the block ID. The maximum block ID is
 * 2^18 - 1 = 262'143.
 */
constexpr unsigned int MPITagBitsForBlockID = 18;

/**
 * 0x01 - 0x1A (1 - 26) reserved for side.
 * 0x1B - 0x1F for other fundamentals.
 */
constexpr int MPIExchangeBorder        = (0x1Bu << MPITagBitsForBlockID);
constexpr int MPIExchangeContainerBase = (0x1Cu << MPITagBitsForBlockID);
constexpr int MPICountExchange         = (0x1Du << MPITagBitsForBlockID);

constexpr int MPILocalMsgExchange  = 21;
constexpr int MPILocalMsgSetup     = 22;
constexpr int MPILocalFCSExchange  = 23;
constexpr int MPILocalPackExchange = 24;
constexpr int MPIGlobalMsgExchange = 42;
constexpr int MPIToken             = 43;
constexpr int MPIOutput            = 44;
constexpr int MPITriangleExchange  = 60;
constexpr int MPICSVExchange       = 61;
constexpr int MPILogger            = 80;  ///< base of the logger MPI tags, reserve 20 tags.
constexpr int MPILoggerFinalize    = 81;

class MPIData {
public:
  explicit MPIData() = default;
  explicit MPIData(int argc, char* argv[]) { init(argc, argv); }

  ~MPIData();

  MPIData(const MPIData&)     = delete;
  MPIData(MPIData&&) noexcept = default;
  MPIData& operator=(const MPIData&) = delete;
  MPIData& operator=(MPIData&&) = delete;

  void init(int argc, char* argv[]);

  int getRank() const { return rank_; }
  int getSize() const { return size_; }

  /**
   * Gets the maximum block ID.
   *
   * @return The maximum block ID.
   */
  static unsigned int getMaxBlockID() { return (1u << (MPITagBitsForBlockID)) - 1u; }
  void checkMPITags() const;

private:
  unsigned int getUBTags() const;

  int size_{1};  ///< The MPI size.
  int rank_{0};  ///< The MPI rank.
};

}  // namespace nastja
