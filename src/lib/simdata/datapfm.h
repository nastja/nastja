/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/math/polynomial.h"
#include "lib/math/vector.h"
#include "lib/storage/paramstorage.h"
#include <array>
#include <memory>

namespace nastja {

struct DataPFM : public DataContainer {
  explicit DataPFM() = default;
  explicit DataPFM(config::Config& config) {
    using namespace config::literals;
    /// @key{PhaseField.epsilon, real_t, 1.0}
    /// The parameter @f$\varepsilon@f$ for the phase-field method.
    epsilon = config.getValue<real_t>("PhaseField.epsilon"_jptr, 1.0);
  }

  const std::array<real_t, 2> k{{2103.320339 / 100.0, 1708.947775 / 100.0}};  ///< The array of the thermal conductivity of the liquid and solid phase.

  const real_t Tm{1.0};            ///< The melting temperature.
  real_t epsilon{0.0};             ///< The epsilon, related to the phase-field interface with.
  const real_t gab{0.4396396751};  ///< The interface entropie density.
  const real_t k0{5.5797034};      ///< The kinetic coefficent.

  const real_t epsilon_k{0.230331};  ///< The epsilon_k parameter in the anisotropic kinetic.
  const real_t delta_k{-0.196511};   ///< The delta_k parameter in the anisotropic kinetic.

  const real_t epsilon1{0.10191};   ///< The epsilon_1 parameter in the anisotropic surface tension.
  const real_t epsilon2{-0.00134};  ///< The epsilon_2 parameter in the anisotropic surface tension.
  const real_t epsilon3{0.00876};   ///< The epsilon_3 parameter in the anisotropic surface tension.

  const real_t MplusF{0.182223};  ///< The integral value for the beta zero kinetic.

  math::Polynomial CVa{0.9816705873, 0.04412186595, -0.02403402118, -0.001758431968};                ///< The polynomial of the volumetric heat capacity of the liquid phase.
  math::Polynomial CVb{1.331101576, -0.4598970843, 0.05225148201, -0.003237275948};                  ///< The polynomial of the volumetric heat capacity of the solid pahse.
  math::Polynomial La{-0.02161349859, 0.7705647776, -0.4349444313, 0.05210816941, -0.003776436992};  ///< The polynomial of the latent heat.

  void dump(Archive& /*ar*/) const final{};
  void undump(Archive& /*ar*/) final{};
};

}  // namespace nastja
