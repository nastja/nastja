/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "simdata.h"
#include "lib/block/blockmanager.h"
#include "lib/config/config.h"
#include <chrono>
#include <memory>
#include <mpi.h>
#include <string>

namespace nastja {

using json = nlohmann::json;
using namespace config::literals;

SimData::SimData(config::Config& config, MPIData& mpiData)
    : mpiData{mpiData}, config_{config} {
  /// @key{Settings.timesteps, int}
  /// The number of time-steps.
  setTimesteps(config.getValue<int>("Settings.timesteps"_jptr));

  /// @key{Settings.deltax, real_t, 1.0}
  /// The discretization width.
  deltax = config.getValue<real_t>("Settings.deltax"_jptr, 1.0);

  /// @key{Settings.deltat, real_t, 1.0}
  /// The time-step discretization width.
  deltat = config.getValue<real_t>("Settings.deltat"_jptr, 1.0);

  deltax_R = 1.0 / deltax;

  /// @key{Settings.randomseed, uint, 0}
  /// If the value >0 it sets the random seed. For 0 a seed depending on time is chosen.
  ///
  /// This seed is for the local block-wise random generator, each MPI process is seeded with this seed + rank.
  auto seed = config.getValue<unsigned long>("Settings.randomseed"_jptr, 0);
  if (seed == 0) {
    seed = createSeed();
    config.appendToSave("Settings.randomseed"_jptr, seed);
  }
  seedAll(seed);

  /// @key{Filling.randomseed, uint, 0}
  /// If the value >0 it sets the global random seed. For 0 a seed depending on time is chosen.
  ///
  /// Filling needs a random generator that draws the same number for all blocks. It is available in the evaluator via
  /// `rndg()`. For each fill element the seed is set to this seed + element number. If a fill element needs a specific
  /// seed it can be set via the `seed` inside of the elements.
  ///
  /// The automatically generated element-wise seed is not stored in config_saved. Reproducing a simulation works anyway.
  /// To reorder the fill elements, the seed must be added to the elements to prevent the same random object.
  baseSeed_ = config.getValue<unsigned long>("Filling.randomseed"_jptr, 0);
  if (baseSeed_ == 0) {
    baseSeed_ = createSeed();
    config.appendToSave("Filling.randomseed"_jptr, baseSeed_);
  }
  globalGenerator.seed(baseSeed_);

  config.linkEvaluator(*this);

  /// @key{Filling.initialoutput, bool, false}
  /// Flag enables the initial output of filled fields.
  outputFilling = config.getValue<bool>("Filling.initialoutput"_jptr, false);

  mpiData.checkMPITags();
}

/**
 * Creates a seed based on time.
 *
 * @return The seed
 */
unsigned long SimData::createSeed() const {
  const auto time    = std::chrono::high_resolution_clock::now().time_since_epoch();
  const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(time);

  // microseconds fraction of the current time
  unsigned long seed = std::chrono::duration_cast<std::chrono::microseconds>(time - seconds).count();

  if (mpiData.getSize() > 1) {
    MPI_Bcast(&seed, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
  }

  return seed;
}

/**
 * Seeds all generators.
 *
 * @note We generate a seed and spread this to all the MPI ranks. The random generators are seeded with this value plus
 * the rank. This is not the best way to generate unique sequences on all ranks. For the cellular Potts model this is
 * okay, the actions depends strongly on the geometry. For systematicaly add a random on each grid point this is not
 * good enough, it could generate some unwanted pattern.
 *
 * @param seed  The seed.
 */
void SimData::seedAll(unsigned long seed) {
  generator.seed(seed + mpiData.getRank());
}

/**
 * Get a seed from filling or baseseed.
 *
 * @note If the value >0 it sets the random seed. If the value =0 the seed depends on time, or no global seed is given.
 *       If seed is not contained but global seed is contained, global seed sets the random seed.
 *
 * @param jptr        The json pointer to filling element.
 *
 * @return The seed.
 */
unsigned long SimData::getSeed(const json::json_pointer& jptr) {
  auto seed = config_.getValue<unsigned long>(jptr / "seed", 0);
  if (seed != 0) return seed;

  int baseOffset = 0;
  try {
    baseOffset = std::stoi(jptr.back());
  } catch (...) {
    logger::get().onewarning("Offset to base seed not found in '{}'.", jptr);
  }

  return baseSeed_ + baseOffset;
}

config::Config& SimData::getConfig() const {
  return config_;
}

const config::CommandLine& SimData::getCmdLine() const {
  return config_.getCmdLine();
}

void SimData::dump(Archive& ar) const {
  ar.pack(timestep);

  std::stringstream sout;
  sout << generator;
  std::string tmp = sout.str();
  ar.pack(tmp);

  appendedData_.dump(ar);

  blockmanager_->dump(ar);
}

void SimData::undump(Archive& ar) {
  ar.unpack(timestep);

  std::string tmp;
  ar.unpack(tmp);
  std::stringstream sin(tmp);
  sin >> generator;

  appendedData_.undump(ar);

  blockmanager_->undump(ar);
}

}  // namespace nastja
