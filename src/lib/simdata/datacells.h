/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/math/vector.h"
#include "lib/storage/paramstorage.h"
#include <array>
#include <memory>

namespace nastja {

struct DataCells : public DataContainer {
  std::unique_ptr<ParameterStorageBuilder<real_t>> storageAdhesion{};         ///< The pointer to the parameter storage for the adhesions.
  std::unique_ptr<ParameterStorageBuilder<real_t>> storagePolarityAdhesion{}; ///< The pointer to the parameter storage for the polarity adhesions.
  std::unique_ptr<ParameterStorageBuilder<real_t>> storagePolarityAdhesionThreshold{}; ///< The pointer to the parameter storage for the polarity adhesion scalar product thresholds.

  std::unique_ptr<ParameterStorageBuilder<real_t>> storageCoupling{};         ///< The pointer to the parameter storage for the couplings.
  std::unique_ptr<ParameterStorageBuilder<real_t>> defaultSurface{};          ///< The pointer to the parameter storage for the default surfaces.
  std::unique_ptr<ParameterStorageBuilder<real_t>> defaultVolume{};           ///< The pointer to the parameter storage for the default volumes.
  std::unique_ptr<ParameterStorageBuilder<real_t>> lambdaSurface{};           ///< The pointer to the parameter storage for the lambda for the surface term.
  std::unique_ptr<ParameterStorageBuilder<real_t>> lambdaVolume{};            ///< The pointer to the parameter storage for the lambda for the volume term
  std::unique_ptr<ParameterStorageBuilder<real_t>> changeSurface{};           ///< The pointer to the parameter storage for the change in size.
  std::unique_ptr<ParameterStorageBuilder<real_t>> changeVolume{};            ///< The pointer to the parameter storage for the change in size.

std::unique_ptr<ParameterStorageBuilder<real_t>> lambdaPolarity{};            ///< The pointer to the parameter storage for the lambda for the polarity term

  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> startSignal{};          ///< The array of pointers to the parameter storage for the initial value of the signals.
  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> relativeDecaySignal{};  ///< The array of pointers to the parameter storage for the relative signal decay.
  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> absoluteDecaySignal{};  ///< The array of pointers to the parameter storage for the absolute signal decay.
  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> absoluteProductionSignal{};  ///< The array of pointers to the parameter storage for the absolute signal production.

  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> diffusionSignal{};      ///< The array of pointers to the parameter storage for the signal diffusion.
  std::array<std::unique_ptr<ParameterStorageBuilder<real_t>>, numberOfSignals> interactionSignal{};    ///< The array of pointers to the parameter storage for the signal interactions.
  std::unique_ptr<ParameterStorageBuilder<int>> constantSignal{};                                       ///< The pointer to the parameter storage for the constant signal.
  std::unique_ptr<ParameterStorageBuilder<real_t>> typeChangeGreater{};                                 ///< The pointer to the parameter storage for the change of type.
  std::unique_ptr<ParameterStorageBuilder<real_t>> typeChangeSmaller{};                                 ///< The pointer to the parameter storage for the change of type

  math::Vector3<real_t> potentialDirection{};  ///< The vector for the direction of the potential.

  cellid_t liquid{0};  ///< The liquid cell ID, all cell IDs smaller are solids.

  real_t divisionRateFactor{1.0};  ///< Factor e [0,1] that reduces the Division rate of all cells after radiation therapy or similar

  void dump(Archive& /*ar*/) const final{};
  void undump(Archive& /*ar*/) final{};
};

}  // namespace nastja
