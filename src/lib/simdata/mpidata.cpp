/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mpidata.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/math/arithmetic.h"
#include "fmt/format.h"
#include <stdexcept>
#include <type_traits>

namespace nastja {

const MPI_Datatype MPI_NASTJA_REAL = (std::is_same<real_t, float>::value ? MPI_FLOAT : (std::is_same<real_t, double>::value ? MPI_DOUBLE : MPI_LONG_DOUBLE));

static_assert(std::is_same<cellid_t, unsigned int>::value || std::is_same<cellid_t, unsigned long>::value, "cellid_t not implemented for MPI");
const MPI_Datatype MPI_NASTJA_INTEGER = std::is_same<cellid_t, unsigned int>::value ? MPI_INT : MPI_LONG;

MPIData::~MPIData() {
  int initialized;
  int finalized;
  MPI_Initialized(&initialized);
  MPI_Finalized(&finalized);
  if (initialized && !finalized) {
    MPI_Finalize();
  }
}

void MPIData::init(int argc, char* argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size_);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
}

/**
 * Gets the upper bound of MPI tags.
 *
 * @return The upper bound of MPI tags.
 */
unsigned int MPIData::getUBTags() const {
  void* v{};
  int flag{};

  MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_TAG_UB, &v, &flag);
  if (flag == 0) return 0;

  return *static_cast<unsigned int*>(v);
}

/**
 * Check for the maximum available MPI Tags.
 */
void MPIData::checkMPITags() const {
  unsigned int val = getUBTags();

  unsigned int bits = 32 - math::countLeadingZeros(val);

  unsigned int setSignificant = 0b1'1111u << (bits - 5);
  if ((val | setSignificant) > val) bits--;

  if (MPITagBitsForBlockID + 5 > bits) {
    throw std::runtime_error(fmt::format("The code is compiled with MPITagBitsForBlockID={}, the current MPI version only supports {} block ID bits. Decrease MPITagBitsForBlockID or use another MPI implementation.", MPITagBitsForBlockID, bits - 5));
  }
  logger::get().oneinfo("The upper bound of the MPI tag is {:n}. {} bits can be used totally; {} bits for block IDs (max. {:n} block IDs).", val, bits, bits - 5, (1u << (bits - 5)) - 1);
  logger::get().oneinfo("MPITagBitsForBlockID is currently {} (max. {:n} block IDs).", MPITagBitsForBlockID, getMaxBlockID());
}

}  // namespace nastja
