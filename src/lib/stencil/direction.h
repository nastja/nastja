/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/utility.h"
#include "lib/math/vector.h"
#include <array>
#include <string>

namespace nastja {
namespace stencil {

/**
 * @defgroup stencil Stencil
 *
 * Stencil handel different direction and the neighbored cells.
 */

/**
 * Enumeration of all directions
 *
 * ```
 *    ^ (y- down, y+ up)
 *    |
 *    +--> (x- left, x+ right)
 *   /
 *  v (z- back, z+ forward)
 *
 * Order based
 * +--------+ +--------+ +--------+
 * |21 14 22| | 9  4 10| |25 18 26|
 * |11  5 12| | 1  0  2| |15  6 16|
 * |19 13 20| | 7  3  8| |23 17 24|
 * +--------+ +--------+ +--------+
 * ```
 *  @ingroup stencil
 */
enum class Direction {
  C       = 0,   ///< Center
  L       = 1,   ///< Left               (LowerX)
  R       = 2,   ///< Right              (UpperX)
  D       = 3,   ///< Down               (LowerY)
  U       = 4,   ///< Up                 (UpperY)
  B       = 5,   ///< Back               (LowerZ)
  F       = 6,   ///< Forward            (UpperZ)
  DL      = 7,   ///< Down-Left          (LowerXLowerY)
  DR      = 8,   ///< Down-Right         (UpperXLowerY)
  UL      = 9,   ///< Up-Left            (LowerXUpperY)
  UR      = 10,  ///< Up-Right           (UpperXUpperY)
  BL      = 11,  ///< Back-Left          (LowerXLowerZ)
  BR      = 12,  ///< Back-Right         (UpperXLowerZ)
  BD      = 13,  ///< Back-Down          (LowerYLowerZ)
  BU      = 14,  ///< Back-Up            (UpperYLowerZ)
  FL      = 15,  ///< Forward-Left       (LowerXUpperZ)
  FR      = 16,  ///< Forward-Right      (UpperXUpperZ)
  FD      = 17,  ///< Forward-Down       (LowerYUpperZ)
  FU      = 18,  ///< Forward-Up         (UpperYUpperZ)
  BDL     = 19,  ///< Back-Down-Left     (LxLyLz)
  BDR     = 20,  ///< Back-Down-Right    (UxLyLz)
  BUL     = 21,  ///< Back-Up-Left       (LxUyLz)
  BUR     = 22,  ///< Back-Up-Right      (UxUyLz)
  FDL     = 23,  ///< Forward-Down-Left  (LxLyUz)
  FDR     = 24,  ///< Forward-Down-Right (UxLyUz)
  FUL     = 25,  ///< Forward-Up-Left    (LxUyUz)
  FUR     = 26,  ///< Forward-Up-Right   (UxUyUz)
  INVALID = 27   ///< Invalid direction
};

constexpr unsigned int numberOfDirections = 27;  ///< The number of total possible directions in 3D.

/// Array of relative coordinates for x, y, and z axis.
/// @ingroup stencil
// clang-format off
constexpr std::array<int, 3 * numberOfDirections> relativeCoordinats {
  // C   L   R   D   U   B   F  DL  DR  UL  UR  BL  BR  BD  BU  FL  FR  FD  FU BDL BDR BUL BUR FDL FDR FUL FUR
    {0, -1,  1,  0,  0,  0,  0, -1,  1, -1,  1, -1,  1,  0,  0, -1,  1,  0,  0, -1,  1, -1,  1, -1,  1, -1,  1,
     0,  0,  0, -1,  1,  0,  0, -1, -1,  1,  1,  0,  0, -1,  1,  0,  0, -1,  1, -1, -1,  1,  1, -1, -1,  1,  1,
     0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0, -1, -1, -1, -1,  1,  1,  1,  1, -1, -1, -1, -1,  1,  1,  1,  1}};
// clang-format on

/**
 * Relative coordinates for each direction and each axis.
 * Using constexpr std::array<> not working on gpu, therefore using old definition of array in constexpr function
 * @ingroup stencil
 *
 * @param axis  The axis.
 * @param dir   The direction.
 *
 * @return The relative coordinate.
 */
#ifdef USE_CUDA

constexpr int c(unsigned int axis, unsigned int dir) {
  // clang-format off
  const std::array<int, 3 * numberOfDirections> c {
    // C   L   R   D   U   B   F  DL  DR  UL  UR  BL  BR  BD  BU  FL  FR  FD  FU BDL BDR BUL BUR FDL FDR FUL FUR
      {0, -1,  1,  0,  0,  0,  0, -1,  1, -1,  1, -1,  1,  0,  0, -1,  1,  0,  0, -1,  1, -1,  1, -1,  1, -1,  1,
       0,  0,  0, -1,  1,  0,  0, -1, -1,  1,  1,  0,  0, -1,  1,  0,  0, -1,  1, -1, -1,  1,  1, -1, -1,  1,  1,
       0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0, -1, -1, -1, -1,  1,  1,  1,  1, -1, -1, -1, -1,  1,  1,  1,  1}};
  return c[axis * numberOfDirections + dir];
  // clang-format on
}
constexpr int c(unsigned int axis, Direction dir) { return c(axis, toUnderlying(dir)); }

#else 

constexpr int c(unsigned int axis, unsigned int dir) {
  return relativeCoordinats[axis * numberOfDirections + dir];
}
constexpr int c(unsigned int axis, Direction dir) { return c(axis, toUnderlying(dir)); }

#endif




/**
 * The x-component of relative coordinates for each direction.
 * @ingroup stencil
 *
 * @param dir  The direction.
 *
 * @return The relative x-coordinate.
 */
constexpr int cx(unsigned int dir) { return c(0, dir); }
constexpr int cx(Direction dir) { return cx(toUnderlying(dir)); }

/**
 * The y-component of relative coordinates for each direction.
 * @ingroup stencil
 *
 * @param dir  The direction.
 *
 * @return The relative y-coordinate.
 */
constexpr int cy(unsigned int dir) { return c(1, dir); }
constexpr int cy(Direction dir) { return cy(toUnderlying(dir)); }

/**
 * The z-component of relative coordinates for each direction.
 * @ingroup stencil
 *
 * @param dir  The direction.
 *
 * @return The relative z-coordinate.
 */
constexpr int cz(unsigned int dir) { return c(2, dir); }
constexpr int cz(Direction dir) { return cz(toUnderlying(dir)); }

/// Array of the inverse side of each direction.
/// @ingroup stencil
constexpr std::array<Direction, numberOfDirections> inverseDirection{
    Direction::C,    ///< invC
    Direction::R,    ///< invL
    Direction::L,    ///< invR
    Direction::U,    ///< invD
    Direction::D,    ///< invU
    Direction::F,    ///< invB
    Direction::B,    ///< invF
    Direction::UR,   ///< invDL
    Direction::UL,   ///< invDR
    Direction::DR,   ///< invUL
    Direction::DL,   ///< invUR
    Direction::FR,   ///< invBL
    Direction::FL,   ///< invBR
    Direction::FU,   ///< invBD
    Direction::FD,   ///< invBU
    Direction::BR,   ///< invFL
    Direction::BL,   ///< invFR
    Direction::BU,   ///< invFD
    Direction::BD,   ///< invFU
    Direction::FUR,  ///< invBDL
    Direction::FUL,  ///< invBDR
    Direction::FDR,  ///< invBUL
    Direction::FDL,  ///< invBUR
    Direction::BUR,  ///< invFDL
    Direction::BUL,  ///< invFDR
    Direction::BDR,  ///< invFUL
    Direction::BDL,  ///< invFUR
};

constexpr Direction getInverseDirection(Direction dir) { return inverseDirection[static_cast<unsigned int>(dir)]; }

/// Maps from Direction to index in the 3x3x3 field.
constexpr std::array<unsigned int, numberOfDirections> s27map_{13, 12, 14, 10, 16, 4, 22, 9, 11, 15, 17, 3, 5, 1, 7, 21, 23, 19, 25, 0, 2, 6, 8, 18, 20, 24, 26};
constexpr unsigned int s27map(Direction dir) { return s27map_[static_cast<unsigned int>(dir)]; }

/// Maps from index of a 3x3x3 field to Direction
constexpr std::array<Direction, numberOfDirections> d27map{
    Direction::BDL,  ///< ---
    Direction::BD,   ///< --0
    Direction::BDR,  ///< --+
    Direction::BL,   ///< -0-
    Direction::B,    ///< -00
    Direction::BR,   ///< -0+
    Direction::BUL,  ///< -+-
    Direction::BU,   ///< -+0
    Direction::BUR,  ///< -++
    Direction::DL,   ///< 0--
    Direction::D,    ///< 0-0
    Direction::DR,   ///< 0-+
    Direction::L,    ///< 00-
    Direction::C,    ///< 000
    Direction::R,    ///< 00+
    Direction::UL,   ///< 0+-
    Direction::U,    ///< 0+0
    Direction::UR,   ///< 0++
    Direction::FDL,  ///< +--
    Direction::FD,   ///< +-0
    Direction::FDR,  ///< +-+
    Direction::FL,   ///< +0-
    Direction::F,    ///< +00
    Direction::FR,   ///< +0+
    Direction::FUL,  ///< ++-
    Direction::FU,   ///< ++0
    Direction::FUR,  ///< +++
};

/// Maps from Direction to index in the 2x2x2 field.
constexpr std::array<unsigned int, numberOfDirections> s8map_{0, 0, 1, 0, 2, 0, 4, 0, 0, 0, 3, 0, 0, 0, 0, 0, 5, 0, 6, 0, 0, 0, 0, 0, 0, 0, 7};
constexpr unsigned int s8map(Direction dir) { return s8map_[static_cast<unsigned int>(dir)]; }

/// Maps from index of a 2x2x2 field to Direction
constexpr std::array<Direction, numberOfDirections> d8map{
    Direction::C,    ///< 000
    Direction::R,    ///< 00+
    Direction::U,    ///< 0+0
    Direction::UR,   ///< 0++
    Direction::F,    ///< +00
    Direction::FR,   ///< +0+
    Direction::FU,   ///< ++0
    Direction::FUR,  ///< +++
};

/**
 * Generator for the bitset of skipping boundaries.
 *
 * @param axis       The axis.
 * @param direction  The direction.
 *
 * @return The bits of the direction.
 */
constexpr unsigned int genSkipBitset(unsigned int axis, int direction) {
  unsigned int mask = 0;
  for (unsigned int i = 1; i < numberOfDirections; i++) {
    mask |= (c(axis, i) == direction) << (i - 1);
  }
  return mask;
}

/**
 * A bit set for the border that are skipped if the n-th side is a non-periodic domain boundary.
 * If the bit is set this exchange is skipped. HaloSide is decoded from lower to higher bit.
 *
 * @ingroup stencil
 */
constexpr std::array<unsigned int, 6> skipExchange{
    genSkipBitset(0, -1),
    genSkipBitset(0, 1),
    genSkipBitset(1, -1),
    genSkipBitset(1, 1),
    genSkipBitset(2, -1),
    genSkipBitset(2, 1)};

/**
 * Generator for the bitset of common borders.
 *
 * @param dir  The direction.
 *
 * @return Set all bits to one when the side is includet in the direction
 */
constexpr unsigned int genCommonBitset(Direction dir) {
  if (dir == Direction::C) return 1;

  unsigned int mask = 0;
  for (unsigned int i = 1; i < numberOfDirections; i++) {
    if ((cx(dir) == cx(i) || (cx(dir) != 0 && cx(i) == 0)) &&
        (cy(dir) == cy(i) || (cy(dir) != 0 && cy(i) == 0)) &&
        (cz(dir) == cz(i) || (cz(dir) != 0 && cz(i) == 0))) {
      mask |= 1u << i;
    }
  }

  return mask;
}

/**
 * A bit set that denotes common borders.
 */
constexpr std::array<unsigned int, numberOfDirections> commonSide{
    genCommonBitset(Direction::C),
    genCommonBitset(Direction::L),
    genCommonBitset(Direction::R),
    genCommonBitset(Direction::D),
    genCommonBitset(Direction::U),
    genCommonBitset(Direction::B),
    genCommonBitset(Direction::F),
    genCommonBitset(Direction::DL),
    genCommonBitset(Direction::DR),
    genCommonBitset(Direction::UL),
    genCommonBitset(Direction::UR),
    genCommonBitset(Direction::BL),
    genCommonBitset(Direction::BR),
    genCommonBitset(Direction::BD),
    genCommonBitset(Direction::BU),
    genCommonBitset(Direction::FL),
    genCommonBitset(Direction::FR),
    genCommonBitset(Direction::FD),
    genCommonBitset(Direction::FU),
    genCommonBitset(Direction::BDL),
    genCommonBitset(Direction::BDR),
    genCommonBitset(Direction::BUL),
    genCommonBitset(Direction::BUR),
    genCommonBitset(Direction::FDL),
    genCommonBitset(Direction::FDR),
    genCommonBitset(Direction::FUL),
    genCommonBitset(Direction::FUR)};

/// String representation for each direction
/// @ingroup stencil
const std::array<std::string, numberOfDirections> directionString{
    "C", "L", "R", "D", "U", "B", "F",
    "DL", "DR", "UL", "UR", "BL", "BR", "BD", "BU", "FL", "FR", "FD", "FU",
    "BDL", "BDR", "BUL", "BUR", "FDL", "FDR", "FUL", "FUR"};

inline static const std::string& getDirectionString(Direction dir) { return directionString[static_cast<unsigned int>(dir)]; }

void initializeFieldOffsets(std::array<long, numberOfDirections>& offsets, const math::Vector3<unsigned long>& size, unsigned long vectorSize, unsigned long N = 1);

template <typename T>
math::Vector3<T> makeVector3(stencil::Direction dir) { return math::Vector3<T>{static_cast<T>(cx(dir)), static_cast<T>(cy(dir)), static_cast<T>(cz(dir))}; }

}  // namespace stencil

/**
 * Addition operator.
 *
 * @param v    The vector.
 * @param dir  The direction.
 *
 * @tparam T  The data type of the vector elements.
 *
 * @return The result of the addition.
 */
template <typename T>
inline math::Vector3<T> operator+(const math::Vector3<T>& v, stencil::Direction dir) {
  math::Vector3<T> r;

  for (unsigned int i = 0; i < 3; i++) {
    r[i] = v[i] + stencil::c(i, dir);
  }

  return r;
}

template <typename T>
inline math::Vector3<T> operator+(stencil::Direction dir, const math::Vector3<T>& v) {
  return v + dir;
}

/**
 * Multiplication operator.
 *
 * @param v    The vector.
 * @param dir  The direction.
 *
 * @tparam T  The data type of the vector elements.
 *
 * @return The result of the multiplication.
 */
template <typename T>
inline math::Vector3<T> operator*(const math::Vector3<T>& v, stencil::Direction dir) {
  math::Vector3<T> r;

  for (unsigned int i = 0; i < 3; i++) {
    r[i] = v[i] * stencil::c(i, dir);
  }

  return r;
}

template <typename T>
inline math::Vector3<T> operator*(stencil::Direction dir, const math::Vector3<T>& v) {
  return v * dir;
}

namespace math {

/**
   * Calculates the dot product @f$c = v \cdot dir@f$.
   *
   * @param v    The vecctor
   * @param dir  The direction.
   *
   * @tparam T  The data type of the vector elements.
   *
   * @return A scalar of the dot product.
   */
template <typename T>
inline T dotProduct(const Vector3<T>& v, stencil::Direction dir) {
  T sum{};

  for (unsigned int i = 0; i < 3; i++) {
    sum += v[i] * stencil::c(i, dir);
  }

  return sum;
}

template <typename T>
inline T dotProduct(stencil::Direction dir, const Vector3<T>& v) {
  return dotProduct(v, dir);
}

}  // namespace math

}  // namespace nastja
