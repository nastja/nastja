/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "direction.h"
#include <array>

namespace nastja {

using namespace stencil;

namespace stencil {

void initializeFieldOffsets(std::array<long, numberOfDirections>& offsets, const math::Vector3<unsigned long>& size, unsigned long vectorSize, unsigned long N /*= 1*/) {
  for (unsigned int dir = 0; dir < numberOfDirections; dir++) {
    long fieldIndex = size[0] * size[1] * cz(dir) + size[0] * cy(dir);
    if (N == 1) {
      offsets[dir] = (fieldIndex + cx(dir)) * vectorSize;
    } else {
      // here we only can manage the y and z offset. Each x element is N * vectorSize, but size[0] / N, so N is canceled
      offsets[dir] = fieldIndex * vectorSize;
    }
  }
}

}  // namespace stencil
}  // namespace nastja
