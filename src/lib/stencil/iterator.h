/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/stencil/direction.h"

namespace nastja {
namespace stencil {

template <typename Stencil>
class Iterator {
public:
  explicit Iterator(unsigned int i) : iter_{i} {}

  /// @name Operators
  /// @{
  Iterator& operator++() {
    ++iter_;
    return *this;
  }

  Iterator operator++(int) {
    auto tmp(*this);
    operator++();
    return tmp;
  }

  bool operator==(const Iterator& other) const { return iter_ == other.iter_; }
  bool operator!=(const Iterator& other) const { return iter_ != other.iter_; }
  /// @}

  /// @name Access functions
  /// @{
  /// Access the index.
  unsigned int& operator*() { return iter_; }
  /// Access the index.
  unsigned int index() const { return iter_; }

  /// Access the direction.
  Direction direction() const { return Stencil::dir[iter_]; }

  /// Access the x component of the direction.
  int cx() const { return stencil::cx(Stencil::dir[iter_]); }

  /// Access the y component of the direction.
  int cy() const { return stencil::cy(Stencil::dir[iter_]); }

  /// Access the z component of the direction.
  int cz() const { return stencil::cz(Stencil::dir[iter_]); }

  /**
   * Access the component of the direction.
   *
   * @param dimension  The dimension.
   *
   * @return The component of the dimension.
   */
  int c(const unsigned int dimension) const { return stencil::c(dimension, Stencil::dir[iter_]); }

  /// Access a string of the direction.
  const std::string& directionString() const { return stencil::getDirectionString(Stencil::dir[iter_]); }
  /// @}

private:
  unsigned int iter_;
};

}  // namespace stencil
}  // namespace nastja
