/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/stencil/stencil.h"

namespace nastja {
namespace stencil {

using D2C9 = Stencil<2, 9>;

template<> const char* const D2C9::name;
template<> const std::array<Direction, D2C9::size> D2C9::dir;
template<> const std::array<unsigned int, numberOfDirections> D2C9::index;

}  // namespace stencil
}  // namespace nastja
