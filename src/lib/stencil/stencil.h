/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/stencil/direction.h"
#include "lib/stencil/iterator.h"

namespace nastja {
namespace stencil {

template <unsigned int DIM, unsigned int SIZE>
struct Stencil {
  static const unsigned int dimension = DIM;
  static const unsigned int size      = SIZE;

  static const char* const name;

  /// Definition of the stencil by a subset of directions.
  static const std::array<Direction, size> dir;

  /// Maps direction to index.
  static const std::array<unsigned int, numberOfDirections> index;

  using iterator = stencil::Iterator<Stencil<DIM, SIZE>>;
  static iterator begin() { return iterator{0}; }
  static iterator end() { return iterator{size}; }
};

}  // namespace stencil
}  // namespace nastja
