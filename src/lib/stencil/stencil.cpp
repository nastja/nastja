/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/stencil/D2C1.h"
#include "lib/stencil/D2C4.h"
#include "lib/stencil/D2C5.h"
#include "lib/stencil/D2C8.h"
#include "lib/stencil/D2C9.h"
#include "lib/stencil/D3C1.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/D3C7.h"
#include "lib/stencil/D3C8asym.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;

// Workaround this definitions can be moved to the stencil structs with inline variables of C++17

constexpr unsigned int invalid = static_cast<unsigned int>(Direction::INVALID);

// 2D stencils
template<> const char* const D2C1::name = "D2C1";
template<> const std::array<Direction, D2C1::size> D2C1::dir{Direction::C};
template<> const std::array<unsigned int, numberOfDirections> D2C1::index{0, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D2C4::name = "D2C4";
template<> const std::array<Direction, D2C4::size> D2C4::dir{Direction::L, Direction::R, Direction::D, Direction::U};
template<> const std::array<unsigned int, numberOfDirections> D2C4::index{invalid, 0, 1, 2, 3, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D2C5::name = "D2C5";
template<> const std::array<Direction, D2C5::size> D2C5::dir{Direction::C, Direction::L, Direction::R, Direction::D, Direction::U};
template<> const std::array<unsigned int, numberOfDirections> D2C5::index{0, 1, 2, 3, 4, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D2C8::name = "D2C8";
template<> const std::array<Direction, D2C8::size> D2C8::dir{Direction::L, Direction::R, Direction::D, Direction::U, Direction::DL, Direction::DR, Direction::UL, Direction::UR};
template<> const std::array<unsigned int, numberOfDirections> D2C8::index{invalid, 0, 1, 2, 3, invalid, invalid, 4, 5, 6, 7, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D2C9::name = "D2C9";
template<> const std::array<Direction, D2C9::size> D2C9::dir{Direction::C, Direction::L, Direction::R, Direction::D, Direction::U, Direction::DL, Direction::DR, Direction::UL, Direction::UR};
template<> const std::array<unsigned int, numberOfDirections> D2C9::index{0, 1, 2, 3, 4, invalid, invalid, 5, 6, 7, 8, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

// 3D stencils
template<> const char* const D3C1::name = "D3C1";
template<> const std::array<Direction, D3C1::size> D3C1::dir{Direction::C};
template<> const std::array<unsigned int, numberOfDirections> D3C1::index{0, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D3C6::name = "D3C6";
template<> const std::array<Direction, D3C6::size> D3C6::dir{Direction::L, Direction::R, Direction::D, Direction::U, Direction::B, Direction::F};
template<> const std::array<unsigned int, numberOfDirections> D3C6::index{invalid, 0, 1, 2, 3, 4, 5, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D3C7::name = "D3C7";
template<> const std::array<Direction, D3C7::size> D3C7::dir{Direction::C, Direction::L, Direction::R, Direction::D, Direction::U, Direction::B, Direction::F};
template<> const std::array<unsigned int, numberOfDirections> D3C7::index{0, 1, 2, 3, 4, 5, 6, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid, invalid};

template<> const char* const D3C26::name = "D3C26";
template<> const std::array<Direction, D3C26::size> D3C26::dir{Direction::L, Direction::R, Direction::D, Direction::U, Direction::B, Direction::F, Direction::DL, Direction::DR, Direction::UL, Direction::UR, Direction::BL, Direction::BR, Direction::BD, Direction::BU, Direction::FL, Direction::FR, Direction::FD, Direction::FU, Direction::BDL, Direction::BDR, Direction::BUL, Direction::BUR, Direction::FDL, Direction::FDR, Direction::FUL, Direction::FUR};
template<> const std::array<unsigned int, numberOfDirections> D3C26::index{invalid, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};

template<> const char* const D3C27::name = "D3C27";
template<> const std::array<Direction, D3C27::size> D3C27::dir{Direction::C, Direction::L, Direction::R, Direction::D, Direction::U, Direction::B, Direction::F, Direction::DL, Direction::DR, Direction::UL, Direction::UR, Direction::BL, Direction::BR, Direction::BD, Direction::BU, Direction::FL, Direction::FR, Direction::FD, Direction::FU, Direction::BDL, Direction::BDR, Direction::BUL, Direction::BUR, Direction::FDL, Direction::FDR, Direction::FUL, Direction::FUR};
template<> const std::array<unsigned int, numberOfDirections> D3C27::index{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};

template<> const char* const D3C8asym::name = "D3C8asym";
template<> const std::array<Direction, D3C8asym::size> D3C8asym::dir{Direction::C, Direction::R, Direction::U, Direction::F, Direction::UR, Direction::FR, Direction::FU, Direction::FUR};
template<> const std::array<unsigned int, numberOfDirections> D3C8asym::index{0, invalid, 1, invalid, 2, invalid, 3, invalid, invalid, invalid, 4, invalid, invalid, invalid, invalid, invalid, 5, invalid, 6, invalid, invalid, invalid, invalid, invalid, invalid, invalid, 7};


namespace stencil {

template struct Stencil<2, 1>;
template struct Stencil<2, 4>;
template struct Stencil<2, 5>;
template struct Stencil<2, 8>;
template struct Stencil<2, 9>;
template struct Stencil<3, 1>;
template struct Stencil<3, 6>;
template struct Stencil<3, 7>;
template struct Stencil<3, 26>;
template struct Stencil<3, 27>;
template struct Stencil<3, 8>;

}  // namespace stencil
}  // namespace nastja
