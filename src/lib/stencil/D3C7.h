/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/stencil/stencil.h"

namespace nastja {
namespace stencil {

using D3C7 = Stencil<3, 7>;

template<> const char* const D3C7::name;
template<> const std::array<Direction, D3C7::size> D3C7::dir;
template<> const std::array<unsigned int, numberOfDirections> D3C7::index;

}  // namespace stencil
}  // namespace nastja
