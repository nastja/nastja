/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "message.h"
#include <cassert>
#include <cstring>

namespace nastja {

/**
 * Gives the position of a given message.
 *
 * @param data Vector of all messages
 * @param tag  Tag of the searched message
 *
 * @return Tuple of begin and end position
 */
std::tuple<int, int> Message::findMessage(std::vector<int>& data, const MessageID tag) {
  int length = data.size();
  int pos    = 0;

  while (pos < length) {
    int header   = data[pos];
    int size     = header & 0x00FFFFFF;
    int blockTag = (header & 0xFF000000) >> 24;

    if (blockTag == tag) {  // found block
      return std::make_tuple(pos + 1, pos + size + 1);
    }
    if (blockTag == 255) {  // here is an end block
      break;
    }

    pos += size + 1;
  }

  return std::make_tuple(-1, -1);
}

/**
 * Append a message block msg to the messages data.
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  this vector will be append to data
 */
void Message::append(std::vector<int>& data, const MessageID tag, const std::vector<int>& msg) {
  data.push_back(msg.size() | tag << 24);
  data.insert(data.end(), msg.begin(), msg.end());
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  this set will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const std::set<int>& msg) {
  data.push_back(msg.size() | tag << 24);
  data.insert(data.end(), msg.begin(), msg.end());
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  this integer will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const int msg) {
  data.push_back(1 | tag << 24);
  data.push_back(msg);
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  this float will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const float msg) {
  int size = sizeof(float) / sizeof(int);
  data.push_back(size | tag << 24);
  auto const* ptr = reinterpret_cast<int const*>(&msg);
  data.insert(data.end(), ptr, ptr + size);
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  this double will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const double msg) {
  int size = sizeof(double) / sizeof(int);
  data.push_back(size | tag << 24);
  auto const* ptr = reinterpret_cast<int const*>(&msg);
  data.insert(data.end(), ptr, ptr + size);
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg2 this first integer will be append to data
 * @param msg1 this second integer will be also append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const int msg1, const int msg2) {
  data.push_back(2 | tag << 24);
  data.push_back(msg1);
  data.push_back(msg2);
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param list this list of integer will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, std::initializer_list<int> list) {
  data.push_back(list.size() | tag << 24);
  for (auto msg : list) {
    data.push_back(msg);
  }
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param map  this map of integer vector will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const std::map<int, std::vector<int>>& map) {
  data.push_back(0xFFFFFFFF);
  int dummyPos = data.size() - 1;

  int length = 0;

  for (const auto& vec : map) {
    length += vec.second.size() + 2;

    data.push_back(vec.first);
    data.push_back(vec.second.size());

    data.insert(data.end(), vec.second.begin(), vec.second.end());
  }

  data[dummyPos] = (length | tag << 24);
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param map  this map of integer set will be append to data
 *
 */
void Message::append(std::vector<int>& data, const MessageID tag, const std::map<int, std::set<int>>& map) {
  data.push_back(0xFFFFFFFF);
  int dummyPos = data.size() - 1;

  int length = 0;

  for (const auto& set : map) {
    length += set.second.size() + 2;

    data.push_back(set.first);
    data.push_back(set.second.size());

    data.insert(data.end(), set.second.begin(), set.second.end());
  }

  data[dummyPos] = (length | tag << 24);
}

/**
 * Read message with tag from a message vector.
 *
 * @param data Reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param map  Read to this map of integer vectors.
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, std::map<int, std::vector<int>>& map) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;

  while (start < end) {
    int key    = data[start++];
    int length = data[start++];
    auto& vec  = map[key];
    vec.insert(vec.end(), data.begin() + start, data.begin() + start + length);
    start += length;
  }

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  Read to this vector
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, std::vector<int>& msg) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;

  msg.insert(msg.end(), data.data() + start, data.data() + end);

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  Read to this set
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, std::set<int>& msg) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;

  msg.insert(data.data() + start, data.data() + end);

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  Read to this integer.
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, int& msg) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;
  msg = data[start];

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  Read to this integer.
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, float& msg) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;

  unsigned int size = sizeof(float) / sizeof(int);

  assert(start + size <= data.size());

  std::memcpy(&msg, &data[start], size);

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg  Read to this integer.
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, double& msg) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1) return false;

  unsigned int size = sizeof(double) / sizeof(int);

  assert(start + size <= data.size());

  std::memcpy(&msg, &data[start], size);

  return true;
}

/**
 * @overload
 *
 * @param data reference to the vector holding all message blocks
 * @param tag  MessageID tag
 * @param msg1  Read to this integer.
 * @param msg2  Read to this integer.
 *
 * @return true if tag was found.
 */
bool Message::read(std::vector<int>& data, const MessageID tag, int& msg1, int& msg2) {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(data, tag);
  if (start == -1 || (start + 2 != end)) return false;
  msg1 = data[start];
  msg2 = data[start + 1];

  return true;
}

/**
 * Write the header
 *
 * @param data        reference to the vector holding all message blocks
 * @param tag         MessageID tag
 * @param blockHeader position of the header
 *
 */
void Message::header(std::vector<int>& data, const MessageID tag, const std::size_t blockHeader) {
  data[blockHeader] = ((data.size() - blockHeader - 1) | tag << 24);
}

/**
 * Add an end block.
 *
 * @param data reference to the vector holding all message blocks
 */
void Message::end(std::vector<int>& data) {
  data.push_back(0xFF000000);
}

}  // namespace nastja
