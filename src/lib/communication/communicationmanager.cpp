/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "communicationmanager.h"
#include "lib/archive.h"
#include "lib/logger.h"
#include "lib/communication/message.h"
#include "lib/stencil/D3C26.h"
#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>

namespace nastja {

using namespace stencil;

CommManager::~CommManager() {
  for (auto& comm : commSet) {
    delete comm.second;
  }
}

void CommManager::logPrint(const std::string& str, const int partner, const std::vector<int>& v) {
#ifdef NDEBUG
  return;
#endif
  std::stringstream result;
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(result, " "));
  auto& logFile = logger::get("msg");
  logFile.log("{:5d}, {:3} {}:{}", blockmanager_.getTimestep(), partner, str, result.str());
  logFile.flush();
}

void CommManager::logCommunicators() {
#ifdef NDEBUG
  return;
#endif
  auto key_selector = [](auto pair) { return pair.first; };

  std::vector<int> blks(blockmanager_.getBlockSet().size());
  transform(blockmanager_.getBlockSet().begin(), blockmanager_.getBlockSet().end(), blks.begin(), key_selector);

  std::stringstream result;
  std::copy(blks.begin(), blks.end(), std::ostream_iterator<int>(result, " "));
  auto& logComm = logger::get("comm");
  logComm.log("{:5d} BLKL: ", blockmanager_.getTimestep(), result.str());
  std::stringstream annb;
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    if (!comm.second->announceBlocks_.empty()) {
      result.str("");
      std::copy(comm.second->announceBlocks_.begin(), comm.second->announceBlocks_.end(), std::ostream_iterator<int>(result, " "));
      annb << "[" << comm.first << "] " << result.str();
    }
  }
  logComm.log("{:5d} ANNB: {}", blockmanager_.getTimestep(), annb.str());

  std::vector<int> comms;
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    comms.push_back(comm.first);
  }

  result.str("");
  std::copy(comms.begin(), comms.end(), std::ostream_iterator<int>(result, " "));
  logComm.log("{:5d} COMM: ", blockmanager_.getTimestep(), result.str());
  std::stringstream annc;
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    if (!comm.second->announceComms_.empty()) {
      result.str("");
      std::copy(comm.second->announceComms_.begin(), comm.second->announceComms_.end(), std::ostream_iterator<int>(result, " "));
      annc << "[" << comm.first << "] " << result.str();
    }
  }
  logComm.log("{:5d} ANNC: ", blockmanager_.getTimestep(), annc.str());

  std::stringstream delc;
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    if (!comm.second->deletedComms_.empty()) {
      result.str("");
      std::copy(comm.second->deletedComms_.begin(), comm.second->deletedComms_.end(), std::ostream_iterator<int>(result, " "));
      delc << "[" << comm.first << "] " << result.str();
    }
  }
  logComm.log("{:5d} DELC: ", blockmanager_.getTimestep(), delc.str());

  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    result.str("");
    std::copy(comm.second->knownBlocks_.begin(), comm.second->knownBlocks_.end(), std::ostream_iterator<int>(result, " "));
    logComm.log("{:5d} KNWB, {:2d}: {}", blockmanager_.getTimestep(), comm.first, result.str());

    result.str("");
    std::copy(comm.second->knownComms_.begin(), comm.second->knownComms_.end(), std::ostream_iterator<int>(result, " "));
    logComm.log("{:5d} KNWC, {:2d}: {}", blockmanager_.getTimestep(), comm.first, result.str());
  }
  logComm.flush();
}

/**
 * Setup all Connection by connecting (sending an FCS) and receiving the blocks holds on the partner communicators.
 */
void CommManager::setupConnections() {
  setAllCommConnected();
  recv();
  updateBlocks();
}

/**
 * Find or create new Communicator.
 *
 * @param rank The ID of the Communicator, the partner rank.
 *
 * @return Pointer to the Communicator.
 */
Communicator* CommManager::provideCommunicator(const int rank) {
  Communicator* comm;

  auto it = commSet.find(rank);
  if (it == commSet.end()) {
    comm = new Communicator(blockmanager_, *this);

    commSet[rank] = comm;
    comm->setPartnerRank(rank);

    logger::get().trace(logger::Category::DynNewComm, "NEW COMM {}", rank);
  } else {
    comm = it->second;
  }

  return comm;
}

/**
 * Register the Communicator objects given in the neighborhood of the given Block.
 *
 * @param block pointer to the block.
 */
void CommManager::setupCommunicator(block::Block* block) {
  for (int z = -2; z <= 2; z++) {
    for (int y = -2; y <= 2; y++) {
      for (int x = -2; x <= 2; x++) {
        int rank = block->getLocalNeighborRank(x, y, z);

        if (rank < 0) continue;

        provideCommunicator(rank);
      }
    }
  }
}

/**
 * Check for the need of new Communicator objects.
 * If ID is in any block neighborhood, the Communicator will be created.
 *
 * @param id   ID for update.
 * @param rank Rank for update.
 */
void CommManager::updateCommunication(const unsigned int id, const int rank) {
  if (rank < 0) return;

  auto comm_it = commSet.find(rank);
  if (comm_it != commSet.end()) return;

  if (blockmanager_.isInNeighborhood(id)) {
    logger::get().debug("Found global block {} on process {}. Start communication handshake.", id, rank);
    provideCommunicator(rank);
  }
}

/**
 * Update the set of Communication objects.
 * If there are Blocks on new communicators.
 *
 * @param announceBlocks set of block ID, rank pairs
 */
void CommManager::updateCommunications(const BlockRankSet& announceBlocks) {
  for (const auto& block : announceBlocks) {
    updateCommunication(block.first, block.second);
  }
}

void CommManager::updateBlocks() const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    comm.second->updateBlocks(*this);
  }
}

/**
 * Updates the Neighbors from announced blocks.
 */
void CommManager::updateNeighbors() const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    comm.second->updateNeighbors(*this);
  }
}

/**
 * Does the communicator exist and is connected?
 *
 * @param commID ID of the communicator
 *
 * @return true if exists and is connected.
 */
bool CommManager::isConnected(const int commID) const {
  auto comm = commSet.find(commID);
  if (comm == commSet.end()) return false;

  return comm->second->isConnected();
}

/**
 * Create and connect all communicators in the given list.
 *
 * @param commIDs List of IDs
 */
void CommManager::connectMissing(const std::set<int>& commIDs) {
  for (auto commID : commIDs) {
    auto comm = provideCommunicator(commID);
    comm->setConnected();
  }

  unsigned int cntReceived = 0;
  do {
    cntReceived = 0;
    for (auto commID : commIDs) {
      auto comm = provideCommunicator(commID);
      comm->probe();
      if (comm->testReceived()) cntReceived++;
    }
  } while (cntReceived < commIDs.size());
}

/**
 * Remove Block neighborhood from the Communicator objects.
 *
 * @param id   ID for remove.
 *
 * @return Number of updated blocks.
 */
int CommManager::removeBlock(const unsigned int id) const {
  for (const auto& comm : commSet) {
    comm.second->removeBlock(id);
  }
  std::set<block::Block*> blocks = blockmanager_.removeBlock(id);

  return blocks.size();
}

/**
 * Reset the receive status of each Communicator object.
 */
void CommManager::cleanAllCommReceivedStatus() const {
  for (const auto& comm : commSet) {
    comm.second->cleanReceivedStatus();
  }
}

/**
 * Set all Communicator objects to connected.
 */
void CommManager::setAllCommConnected() const {
  for (const auto& comm : commSet) {
    comm.second->setConnected();
  }
}

/**
 * Test if all receive statuses are set.
 *
 * @return true if all set.
 */
bool CommManager::testCommAllReceived() const {
  for (const auto& comm : commSet) {
    if (!comm.second->testReceived()) return false;
  }
  return true;
}

/**
 * Test if all established connections receive statuses are set.
 *
 * @return true if all set.
 */
bool CommManager::testCommEstablishedReceived() const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    if (!comm.second->testReceived()) return false;
  }
  return true;
}

/**
 * Wait for all sends completed.
 */
void CommManager::waitCommAllSend() const {
  for (const auto& comm : commSet) {
    comm.second->waitSend();
  }
}

/**
 * Wait for all buffer sends completed.
 */
void CommManager::waitCommAllBufferSend() const {
  for (const auto& comm : commSet) {
    comm.second->waitBufferSend();
  }
}

/**
 * Sending messages for each Communication object.
 */
void CommManager::send() const {
  for (const auto& comm : commSet) {
    comm.second->send();
  }
}

/**
 * Received messages for each Communication object.
 * Take care of connection setup messages from the previous time step. These messages are send before regular messages
 * and such they arrive before regular messages. For connections in setup-mode probe for regular messages must check
 * setup messages before.
 */
void CommManager::recv() {
  cleanAllCommReceivedStatus();
  clearNewCommBuffers();

  //loop until all received and no more messages are waiting
  while (!testCommAllReceived()) {
    int waitingMessage = 0;
    do {
      // Recv
      MPI_Status status;
      MPI_Iprobe(MPI_ANY_SOURCE, MPILocalMsgSetup, MPI_COMM_WORLD, &waitingMessage, &status);

      if (waitingMessage != 0) {
        int count;
        MPI_Get_count(&status, MPI_INT, &count);

        auto comm = provideCommunicator(status.MPI_SOURCE);
        comm->receiveNew(count);
      }
    } while (waitingMessage != 0);

    for (const auto& comm : commSet) {
      comm.second->probe();
    }
  }
}

/**
 * Sending buffer for each Communication object.
 */
void CommManager::sendBuffer() const {
  for (const auto& comm : commSet) {
    comm.second->sendingBuffer();
  }
}

/**
 * Received buffer for each Communication object.
 */
void CommManager::recvBuffer() {
  cleanAllCommReceivedStatus();

  //loop until all received and no more messages are waiting
  while (!testCommEstablishedReceived()) {
    for (const auto& comm : commSet) {
      if (!comm.second->isConnected()) continue;

      comm.second->probeBuffer();
    }
  }
}

/**
 * Delete Communicator and announce this deletion to the others.
 *
 * @param rank Rank of the communicator to delete.
 */
void CommManager::deleteComm(const int rank) {
  std::stringstream result;
  std::copy(commSet[rank]->knownBlocks_.begin(), commSet[rank]->knownBlocks_.end(), std::ostream_iterator<int>(result, " "));
  result << "Incoming:";
  std::copy(commSet[rank]->incomingBlocks_.begin(), commSet[rank]->incomingBlocks_.end(), std::ostream_iterator<int>(result, " "));
  result << "Announce:";
  std::copy(commSet[rank]->announceBlocks_.begin(), commSet[rank]->announceBlocks_.end(), std::ostream_iterator<int>(result, " "));

  delete commSet[rank];
  commSet.erase(rank);
  logger::get().trace(logger::Category::CommDel, "Delete communicator {} Blocks: {}", rank, result.str());

  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    comm.second->appendDeleteComm(rank);
  }

  auto comm = commSet.find(blockmanager_.getMPIRank());
  if (comm != commSet.end()) comm->second->removeKnownComm(rank);
}

/**
 * Check for unused communicators and delete them.
 */
void CommManager::cleanUnusedComms() {
  std::set<int> usedRanks;
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    usedRanks.insert(comm.first);
  }

  auto unusedRanks = blockmanager_.getUnusedRanks(usedRanks);

  for (const auto& rank : unusedRanks) {
    deleteComm(rank);
  }
}

/**
 * Add rank to announceComms for each connected Communicator.
 *
 * @param rank The rank to announce.
 */
void CommManager::appendAnnounceComm(const int rank) const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    comm.second->appendAnnounceComm(rank);
  }
}

void CommManager::addToBaseComm(const int commID) {
  commSet.at(blockmanager_.getMPIRank())->appendKnownComm(commID);
}

/**
 * Gives the rank of a given ID if it is known by one Communicator.
 *
 * @param id The ID.
 *
 * @return The rank or -1 if not known.
 */
int CommManager::getRankByID(const int id) const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    if (comm.second->find(id)) {
      return comm.first;
    }
  }
  return -1;
}

/**
 * Check if ID is in the announce list from Communicator with rank.
 *
 * @param id   The ID.
 * @param rank The rank.
 *
 * @return true if ID is in announce list
 */
bool CommManager::isInAnnounce(const int id, const int rank) const {
  auto it = commSet.find(rank);
  if (it == commSet.end()) return false;  // TODO: or is this an EXIT?

  return it->second->isInAnnounce(id);
}

std::set<int> CommManager::getNeighborRanks(const unsigned int id, const unsigned int numOfSides /*= 26*/) const {
  std::set<int> ranks;

  assert(numOfSides == 6 || numOfSides == 18 || numOfSides == 26);

  for (unsigned int side = 0; side < numOfSides; side++) {
    int searchID = blockmanager_.getRelativeBlockID(id, D3C26::dir[side]);
    int rank     = getRankByID(searchID);

    if (rank >= 0 && !isInAnnounce(searchID, rank)) {
      ranks.insert(rank);
    }
  }

  return ranks;
}

/**
 * Clear all newCommBuffers.
 */
void CommManager::clearNewCommBuffers() const {
  for (const auto& comm : commSet) {
    comm.second->clearNewCommBuffer();
  }
}

/**
 * Get the load transmitted from rank.
 *
 * @param rank The rank.
 *
 * @return The load.
 */
double CommManager::getLoad(const int rank) const {
  const auto& comm = commSet.find(rank);

  if (comm == commSet.end()) return -1.0;

  return (*comm).second->getLoad();
}

/**
 * Append a block ID to the incomingBlocks at the Communicator from rank.
 * If the communicator does not exist it will be created if the Block is in the neighborhood of any local Block.
 *
 * @param id   ID of the new Block
 * @param rank Rank of the new Block
 */
void CommManager::appendIncomingBlock(const int id, const int rank) {
  Communicator* comm;

  auto comm_it = commSet.find(rank);
  if (comm_it == commSet.end()) {
    if (!blockmanager_.isInNeighborhood(id)) return;

    comm = provideCommunicator(rank);
  } else {
    comm = comm_it->second;
  }
  comm->appendIncomingBlock(id);
}

/**
 * Calculate the group average of blocks.
 *
 * @return Ceil of the average.
 */
double CommManager::calcGroupAverage() const {
  double sum = 0.0;
  int cnt    = 0;

  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    sum += comm.second->getNumOfBlocks();
    cnt++;
  }

  if (cnt == 0) return 0;

  return ceil(sum / cnt);
}

/**
 * Reset all offer to 0.
 */
void CommManager::resetOfferedSpareBlocks() const {
  for (const auto& comm : commSet) {
    comm.second->setOfferedSpareBlocks(0);
  }
}

/**
 * Calc the offer.
 *
 * First find all Comms with the maximum number of blocks, then give the spare blocks to them equally and spread the
 * rest of spar blocks to them.
 *
 * @todo Give Blocks to Comms with less than max. (For 6 free and 3, 2, 1 only 3 gets an offer.)
 */
void CommManager::calcOfferedSpareBlocks() const {
  resetOfferedSpareBlocks();
  int numOfSpareBlocks = calcGroupAverage() - blockmanager_.getBlockSet().size();

  if (numOfSpareBlocks <= 0) return;

  // get Comms with maxNumOfBlocks
  std::vector<Communicator*> maxComms;
  int maxNumOfBlocks = 0;

  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    int numOfBlocks = comm.second->getNumOfBlocks();

    if (maxNumOfBlocks < numOfBlocks) {
      maxNumOfBlocks = numOfBlocks;
      maxComms.clear();
      maxComms.push_back(comm.second);
    } else if (maxNumOfBlocks == numOfBlocks) {
      maxComms.push_back(comm.second);
    }
  }

  // spread spareBlocks on this Comms
  int offer = numOfSpareBlocks / maxComms.size();

  numOfSpareBlocks -= offer * maxComms.size();
  for (const auto& comm : maxComms) {
    comm->setOfferedSpareBlocks(offer);

    // add rest
    if (numOfSpareBlocks > 0) {
      numOfSpareBlocks--;
      comm->incOfferedSpareBlocks();
    }
  }
}

void CommManager::getCorrectedOfferedSpareBlocks(std::map<int, BlockTuple> newBlockInfo) const {
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    auto& message = comm.second->recvBuffer;

    int spareBlocks;
    if (!Message::read(message, MessageID::offerSpareBlocks, spareBlocks)) continue;

    comm.second->setOfferedSpareBlocks(spareBlocks);

    // remove the known number of new blocks
    for (auto& block : newBlockInfo) {
      if (std::get<BlockTupleIndex::rank>(block.second) == comm.first) comm.second->decOfferedSpareBlocks();
      if (comm.second->getOfferedSpareBlocks() == 0) continue;
    }
  }
}

/**
 * Find the FastCommunications.
 * Get the difference set of the locale set of communicators and of the remote set of communicators.  This difference
 * set holds all communicators, that are possibly needed by the remote.  In the next step the neighborhood is searched,
 * if any new block of the remote has a neighbor on any communicator from this difference set, this communicator and the
 * remote is a pair for the fast communication setup.  This pair is added to the return: The map with key of the remote
 * communicator holds a vector of all paired communicators to this (remote) communicator.  Pair means that if A in the
 * vector of B then B is in the vector of A.
 *
 * @param newBlocksByRank reference to the map of the BlockIDs of new blocks per rank.
 */
void CommManager::findFastCommunications(const std::map<int, std::vector<std::tuple<int, int>>>& newBlocksByRank) {
  for (const auto& rank : newBlocksByRank) {
    if (rank.first == blockmanager_.getMPIRank()) continue;

    auto localList  = commSet.at(blockmanager_.getMPIRank())->getKnownComms();
    auto remoteList = commSet.at(rank.first)->getKnownComms();

    std::vector<int> diffComm;
    std::set_difference(localList.begin(), localList.end(), remoteList.begin(), remoteList.end(), std::back_inserter(diffComm));

    if (diffComm.empty()) continue;

    for (const auto& commID : diffComm) {
      auto comm = commSet.at(commID);
      if (!comm->isConnected()) continue;

      for (const auto& tuple : rank.second) {
        int id = std::get<0>(tuple);
        if (blockmanager_.isInNeighborhood(id, comm->getKnownBlocks())) {
          fastComms_[rank.first].insert(commID);
          fastComms_[commID].insert(rank.first);
          break;
        }
      }
    }
  }
}

/**
 * Append messages to each Communication object.
 */
void CommManager::appendMessages() const {
  std::vector<int> msgNew;
  std::vector<int> msgDelete;

  for (const auto& block : delBlocks_) {
    msgDelete.push_back(std::get<0>(block));
    msgDelete.push_back(std::get<1>(block));
  }

  msgNew.push_back(0);
  for (const auto& comm : commSet) {
    if (!comm.second->isConnected()) continue;

    msgNew.push_back(comm.first);
  }

  msgNew[0] = msgNew.size() - 1;
  for (const auto& rank : newBlocksByRank_) {
    for (const auto& block : rank.second) {
      msgNew.push_back(std::get<0>(block));
      msgNew.push_back(std::get<1>(block));
      msgNew.push_back(rank.first);
    }
  }

  for (const auto& comm : commSet) {
    std::vector<int>& messages = comm.second->sendBuffer;

    messages.clear();

    if (!comm.second->isConnected()) continue;

    Message::append(messages, MessageID::newBlocks, msgNew);
    Message::append(messages, MessageID::delBlocks, msgDelete);
    if (int offer = comm.second->getOfferedSpareBlocks() > 0) {
      Message::append(messages, MessageID::offerSpareBlocks, offer);
    }

    Message::append(messages, MessageID::newComm, fastComms_);
  }
}

/**
 * Append local messages to target Communication object for FCS-consistence.
 *
 * @param target ID of the partner rank.
 *
 * @note this add all messages: del, new and move even if move it not used for dynblock, it should be empty.
 */
void CommManager::appendLocalMessages(const int target) const {
  auto& comm = commSet.at(target);

  std::vector<int>& messages = comm->sendBuffer;

  std::vector<int> msgDelete;
  for (const auto& block : delBlocks_) {
    msgDelete.push_back(std::get<0>(block));
    msgDelete.push_back(std::get<1>(block));
  }
  Message::append(messages, MessageID::delBlocks, msgDelete);
  /*
  //Do not send new blocks information if new block message also gets a list of receivers.
  std::vector<int> msgNew;
  msgNew.push_back(0);
  for (const auto& rank : newBlocksByRank) {
    if (rank.first != blockmanager.getMPIRank()) continue;
    for (const auto& block : rank.second) {
      msgNew.push_back(std::get<0>(block));
      msgNew.push_back(std::get<1>(block));
      msgNew.push_back(rank.first);
    }
  }
  Message::append(messages, MessageID::newBlocks, msgNew);
*/
  Message::append(messages, MessageID::moveBlocks, moveBlocks_);

  Message::append(messages, MessageID::newComm, fastComms_);
}

void CommManager::clearBufferedMessages() {
  delBlocks_.clear();
  newBlocksByRank_.clear();
  moveBlocks_.clear();
  fastComms_.clear();
}

}  // namespace nastja
