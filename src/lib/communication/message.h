/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <map>
#include <set>
#include <vector>

namespace nastja {

struct MessageIDEnum {
  enum MessageID : int {
    blockList = 1,
    newBlocks,
    delBlocks,
    moveBlocks,
    commList,
    delComm,
    newComm,
    state,
    timestep,
    offerSpareBlocks,
    load
  };
};
using MessageID = MessageIDEnum::MessageID;

/**
 * @class Message
 * Provide some function to handle message blocks.
 *
 * Messages are build from an arbitrary number of message blocks.  Each block consist of a header and data.  A message
 * must end by an end block.  Each message have the size of n int values.  Each message block header has a length of one
 * int value, 32 bits.  The message starts directly with the first message block header.  The highest byte consist of an
 * tag, the lower 3 bytes holds the size of the data.  The end block has the tag 255 and a zero size.  This gives a
 * value of `0xFF000000` for the whole end block.
 *
 * The header can be build and read with simple binary operations:
 * ```
 * header = (size | tag << 24);
 *
 * size = header & 0x00FFFFFF;
 * tag  = (header & 0xFF000000) >> 24;
 * ```
 */
class Message {
public:
  static std::tuple<int, int> findMessage(std::vector<int>& data, MessageID tag);

  static void append(std::vector<int>& data, MessageID tag, const std::vector<int>& msg);
  static void append(std::vector<int>& data, MessageID tag, const std::set<int>& msg);
  static void append(std::vector<int>& data, MessageID tag, const std::map<int, std::vector<int>>& map);
  static void append(std::vector<int>& data, MessageID tag, const std::map<int, std::set<int>>& map);
  static void append(std::vector<int>& data, MessageID tag, int msg);
  static void append(std::vector<int>& data, MessageID tag, float msg);
  static void append(std::vector<int>& data, MessageID tag, double msg);
  static void append(std::vector<int>& data, MessageID tag, int msg1, int msg2);
  static void append(std::vector<int>& data, MessageID tag, std::initializer_list<int> list);

  static bool read(std::vector<int>& data, MessageID tag, std::vector<int>& msg);
  static bool read(std::vector<int>& data, MessageID tag, std::set<int>& msg);
  static bool read(std::vector<int>& data, MessageID tag, std::map<int, std::vector<int>>& map);
  static bool read(std::vector<int>& data, MessageID tag, int& msg);
  static bool read(std::vector<int>& data, MessageID tag, float& msg);
  static bool read(std::vector<int>& data, MessageID tag, double& msg);
  static bool read(std::vector<int>& data, MessageID tag, int& msg1, int& msg2);

  static void header(std::vector<int>& data, MessageID tag, std::size_t blockHeader);
  static void end(std::vector<int>& data);
};

}  // namespace nastja
