/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/logger.h"
#include "lib/action/blockTuple_enum.h"
#include "lib/block/blockmanager.h"
#include "lib/communication/communicator.h"
#include "lib/io/writerreader.h"
#include "fmt/format.h"
#include <fstream>
#include <memory>
#include <set>
#include <sstream>
#include <string>

namespace nastja {

namespace block {
class BlockManager;
}

class Communicator;

using CommMap = std::map<int, Communicator*>;

class CommManager {
public:
  explicit CommManager(block::BlockManager& blockmanager)
      : blockmanager_{blockmanager} {
#ifndef NDEBUG
    const std::string& outdir = blockmanager.getCmdLine().outdir;
    std::string filename      = fmt::format("{}/{:06d}/msg.log", outdir, blockmanager.getMPIRank());
    WriterReader::checkPath(filename);
    logger::registerLogger("msg", filename);
    logger::get("msg").setFormat(logger::Level::off, "");

    filename = fmt::format("{}/{:06d}/comm.log", outdir, blockmanager.getMPIRank());
    WriterReader::checkPath(filename);
    logger::registerLogger("comm", filename);
    logger::get("comm").setFormat(logger::Level::off, "");
#endif
  }

  ~CommManager();
  CommManager(const CommManager&) = delete;
  CommManager(CommManager&&)      = default;
  CommManager& operator=(const CommManager&) = delete;
  CommManager& operator=(CommManager&&) = delete;

  void setupConnections();

  Communicator* provideCommunicator(int rank);
  void setupCommunicator(block::Block* block);

  void updateCommunications(const BlockRankSet& announceBlocks);
  void updateBlocks() const;
  void updateNeighbors() const;
  void appendIncomingBlock(int id, int rank);

  int removeBlock(unsigned int id) const;

  void cleanAllCommReceivedStatus() const;

  void recv();
  void send() const;

  bool testCommEstablishedReceived() const;
  void recvBuffer();
  void sendBuffer() const;

  void connectMissing(const std::set<int>& commIDs);

  void appendAnnounceComm(int rank) const;
  void addToBaseComm(int commID);

  void cleanUnusedComms();

  void waitCommAllSend() const;
  void waitCommAllBufferSend() const;

  int getRankByID(int id) const;
  std::set<int> getNeighborRanks(unsigned int id, unsigned int numOfSides = 26) const;

  bool isConnected(int commID) const;

  bool isInAnnounce(int id, int rank) const;

  double calcGroupAverage() const;
  void resetOfferedSpareBlocks() const;
  void calcOfferedSpareBlocks() const;
  void getCorrectedOfferedSpareBlocks(std::map<int, BlockTuple> newBlockInfo) const;

  void appendMessages() const;
  void appendLocalMessages(int target) const;
  void clearBufferedMessages();
  void findFastCommunications(const std::map<int, std::vector<std::tuple<int, int>>>& newBlocksByRank);
  void clearBufferedFCS() { fastComms_.clear(); }

  double getLoad(int rank) const;

  double calcLocalLoad() const { return blockmanager_.getBlockSet().size(); }
  double getLocalLoad() const { return localLoad_; }
  void setLocalLoad(const double load) { localLoad_ = load; }

  std::map<int, std::vector<std::tuple<int, int>>>& getNewBlocksByRank() { return newBlocksByRank_; }
  std::vector<std::tuple<int, int>>& getDelBlocks() { return delBlocks_; }
  std::vector<int>& getMoveBlocks() { return moveBlocks_; }
  std::map<int, std::set<int>>& getFastComms() { return fastComms_; }

  void logPrint(const std::string& str, int partner, const std::vector<int>& v);
  void logCommunicators();

  CommMap commSet;  ///< Holds for each communication partner one Communicator object.

private:
  void setAllCommConnected() const;

  bool testCommAllReceived() const;

  void updateCommunication(unsigned int id, int rank);
  void clearNewCommBuffers() const;

  void deleteComm(int rank);

  double localLoad_{0};  ///< A value for the local load.

  std::map<int, std::vector<std::tuple<int, int>>> newBlocksByRank_;  ///< Information for the newBlock message (newRank: newID, Type)
  std::vector<std::tuple<int, int>> delBlocks_;                       ///< Information for the delBlock message (blockID, Type)
  std::vector<int> moveBlocks_;                                       ///< Information for the moveBlock message (blockID)
  std::map<int, std::set<int>> fastComms_;                            ///< Information for the newBlock message (newRank: newID, Type)

  block::BlockManager& blockmanager_;  ///< Reference to the BlockManager.

  std::ofstream logFile_;  ///< The ofstream for the log file.
  std::ofstream logComm_;  ///< The ofstream for the communicator log file.
};

}  // namespace nastja
