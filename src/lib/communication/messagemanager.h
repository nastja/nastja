/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/block/blockmanager.h"

namespace nastja {

namespace block {
class BlockManager;
}

/**
 * @class MessageManager
 * Managed the global information exchange of new blocks.
 *
 * Different protocols are imaginable. So Allgather gather the new block to each process and Allgather_all gather all
 * blocks to each process.  These both are collective distributions and not practicable for many processes.  The ring
 * distribution is the simplest network which managed this information exchange in an non-collective way, other graphs—
 * especially some with more than one edge are imaginable, too.  The basic protocol stay the same.  Locally new blocks
 * are packed with the local rank and an time to live (ttl) value to a message.  Messages are sent to all connected
 * other nodes, on each hop the ttl is decrease by one.  If ttl reach zero the information is exchange to every node,
 * will be announce to the BlockManager by the DynamicBlock object and the message will be destroyed.  A buffer for the
 * messages is helpful to reduce the amount of messages, so every message that was sent to one node does not need to
 * sent a second time to this node.
 *
 * The drawback of this is that neighboring blocks, which are created at the same time, will start there boundary
 * distribution at first, when all processes know them, i.e. after ttl time-steps.
 *
 */
class MessageManager {
  friend class TESTING_MessageManager;

public:
  explicit MessageManager(block::BlockManager& blockmanager);

  void send(std::vector<int>& localNewBlocks);
  BlockRankSet recv(std::vector<int>& localNewBlocks);

private:
  void initRing();
  void initManhattan(int dimension);

  std::vector<int> getDimensions(int dimension);
  static int getManhattanDiameter(const std::vector<int>& dimensions);
  int getLower(const std::vector<int>& coord, int d, const std::vector<int>& n);
  int getUpper(const std::vector<int>& coord, int d, const std::vector<int>& n);
  static int getIndex(const std::vector<int>& coordinate, const std::vector<int>& size);
  static std::vector<int> getCoordinate(int index, const std::vector<int>& size);

  void sendPeers(std::vector<int>& localNewBlocks);
  BlockRankSet recvPeers();

  BlockRankSet Allgather(std::vector<int>& localNewBlocks);
  BlockRankSet Allgather_all(std::vector<int>& localNewBlocks);
  bool probe(int source);

  using Pair = std::pair<int, int>;

  std::map<int, Pair> messages_;  ///< messages for sending
  std::map<int, Pair> buffer_;    ///< buffer of known messages

  std::vector<int> sourceRanks_;  ///< Rank of the sources
  std::vector<int> targetRanks_;  ///< Rank of the targets

  unsigned int ttl_{};       ///< time to live
  unsigned int totalCores_;  ///< total number of cores

  std::vector<int> recvBuffer_;  ///< The receive buffer.
  std::vector<int> sendBuffer_;  ///< The send buffer.

  std::vector<MPI_Request> sendRequests_;  ///< Vector of the send requests.
  BlockRankSet blockList_;                 ///< Blocks that are globally known.
  block::BlockManager& blockmanager_;      ///< Reference to the BlockManager.
};

}  // namespace nastja
