/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "communicator.h"
#include "lib/logger.h"
#include "lib/communication/message.h"
#include <algorithm>
#include <iterator>
#include <sstream>

namespace nastja {

enum State {
  sendSYN     = 0x33,
  sentSYN     = 0x36,
  sendACKfin  = 0x10,
  sendACK     = 0x30,
  sentACK     = 0x60,
  sendSYNACK  = 0x31,
  sentSYNACK  = 0x64,
  waitFin     = 0x08,
  Established = 0x00
};

enum Event {
  send_ANY    = 0x00,
  recv_SYN    = 0x01,
  recv_ACK    = 0x10,
  recv_SYNACK = 0x11
};

// clang-format off
static constexpr TransitionEntry connection_table[] = {
  // Start     Event        Next         Function
  //---------+------------+------------+--------+
  {sendSYN,    send_ANY,    sentSYN,     nullptr},
  {sendSYN,    recv_SYN,    sendSYNACK,  nullptr},
  //---------+------------+------------+--------+
  {sentSYN,    recv_SYNACK, sendACKfin,  nullptr},
  {sentSYN,    recv_SYN,    sendACK,     nullptr},
  //---------+------------+------------+--------+
  {sendACKfin, send_ANY,    waitFin,     nullptr},
  //---------+------------+------------+--------+
  {waitFin,    recv_ACK,    Established, nullptr},
  {waitFin,    recv_SYNACK, Established, nullptr},
  //---------+------------+------------+--------+
  {sendACK,    recv_ACK,    sendACKfin,  nullptr},
  {sendACK,    send_ANY,    sentACK,     nullptr},
  //---------+------------+------------+--------+
  {sentACK,    recv_ACK,    Established, nullptr},
  //---------+------------+------------+--------+
  {sendSYNACK, send_ANY,    sentSYNACK,  nullptr},
  //---------+------------+------------+--------+
  {sentSYNACK, recv_ACK,    Established, nullptr}
};
// clang-format on

static const std::size_t TABLE_SIZE = std::extent<decltype(connection_table)>::value;

/**
 * <pre>
 * +-----+
 * | New |
 * +-----+
 *   | set
 *   v
 * +---------------+  recv SYN
 * | send SYN 0x33 | ----------------------------------------+
 * +---------------+                                         |
 *   | send_ANY                                              |
 *   v                                                       v
 * +---------------+  recv SYN  +---------------+   +-------------------+
 * | sent SYN 0x36 | ---------> | send ACK 0x30 |   | send SYN,ACK 0x31 |
 * +---------------+            +---------------+   +-------------------+
 *   | recv SYN,ACK      recv_ACK |     | send_ANY           | send_ANY
 *   v                    +-------+     v                    v
 * +-------------------+  |     +---------------+   +-------------------+
 * | send ACK+fin 0x10 |<-+     | sent ACK 0x60 |   | sent SYN,ACK 0x64 |
 * +-------------------+        +---------------+   +-------------------+
 *   | send_ANY                         | recv ACK           | recv ACK
 *   |                                  v                    |
 *   |   +--------------+  recv_ANY   +------------------+   |
 *   +-> | waitFin 0x08 | ----------> | established 0x00 | <-+
 *       +--------------+             +------------------+
 * </pre>
 *
 * send_SYN 0x01 | recv_SYN 0x02 | send_ACK 0x10 | recv_ACK 0x20 | keep sending | connectionState   | sendState
 * ---------     |----------     |----------     |----------     |----------    |-------            |-------
 *        0      |        0      |        0      |        0      |        0     | 0x33 send SYN     | 0x01
 *        0      |        1      |        0      |        0      |        0     | 0x31 send SYN,ACK | 0x11
 *        1      |        1      |        1      |        0      |SYN,ACK 0x44  | 0x64 sent SYN,ACK | 0x11
 *        1      |        0      |        0      |        0      |    SYN 0x04  | 0x36 sent SYN     | 0x01
 *        1      |        1      |        0      |        0      |        0     | 0x30 send ACK     | 0x10
 *        1      |        1      |        1      |        0      |    ACK 0x40  | 0x60 sent ACK     | 0x10
 *        1      |        1      |        0      |        1      |        0     | 0x10 send ACK+fin | 0x10
 *        1      |        1      |        1      |        1      |    fin 0x08  | 0x08 fin          | 0x00
 *        1      |        1      |        1      |        1      |        0     | 0x00 established  | 0x00
 * The connectionState is build from not `send_SYN 0x01` or not `recv_SYN 0x02` or not `send_ACK 0x10` or
 * not `recv_ACK 0x20` or `keep sending`.
 * If the connectionState is changed the sendState also changed.  It will be set to the value in the table.  This can be
 * calculated by `0x11 & connectionState` and if the `keep sending` bit is set, the sendState needs to be inverted
 * (`0x11 ^ sendState`).
 *
 * A new Communicator is initialized with connectionState 0x33 and sendStat 0x01, the first line in the table.
 *
 * The setup of the connection between two Communicators is done transparently in the send and receive functions.
 * For the ConnectionManager the testReceived function is important:  This is true if the Communicator has received a
 * message.  For connections in the early setup phase this is also true.  The early setup phase is active until the
 * communicator knows from its partner, i.e. until the communicator receives a first message (recv_SYN (0x02)) from its
 * partner.
 *
 * All boundary communications can be setup only if both side do the same.  Therefore it is essential that the
 * Communicators on both sites enter the established state (0x00) at the same time step.  A Communicator can only be
 * part of the setup of boundary communications if it is established.
 *
 * The send function must be called before the receive function.  During the connection setup the sendStat is append to
 * the message queue.  If send_ACK (0x10) is set a list of blockIDs from announceBlock is also added to the message
 * queue.
 *
 * If the connectionState is not established (0x00) yet, received messages are looking for an incoming state and set the
 * connectionState.
 */
Communicator::Communicator(block::BlockManager& blockmanager, CommManager& commmanager)
    : connectionSM_{connection_table, TABLE_SIZE}, blockmanager_{blockmanager}, commmanager_{commmanager} {
  connectionSM_.setState(0x33);  // send SYN
  sendState_ = 0x01;             // send SYN
  cleanReceivedStatus();
}

/**
 * Set this Communicator to the connected state.
 * This can be done by the handshake, by a received FCS message or a received FCS command from another communicator.
 * sendFCS is called if it was not called before, the communicator is added to the known comms of this process.
 */
void Communicator::setConnected() {
  connectionSM_.setState(0x00);
  sendState_ = calcSendState(connectionSM_.getState());

  if (!wasSentFCS_) {
    // If this communicator is new, so make a FCS
    commmanager_.addToBaseComm(partnerRank_);

    sendFCSExchange();
    wasSentFCS_ = true;
    received_   = receivedFCS_;
  }
}

/**
 * Test for a valid received.
 *
 * @return true (if received or the connection has not received SYN (0x02) yet) and not waiting on a FCS
 */
bool Communicator::testReceived() const {
  return (receivedFCS_ == wasSentFCS_) && (received_ || ((connectionSM_.getState() & 0x02) != 0u));
}

void Communicator::send() {
  if (sendState_ != 0) {
    setupConnectionSend();
  } else {
    appendMsgAnnouncements();
  }

  Message::append(sendBuffer, MessageID::timestep, (int)blockmanager_.getTimestep());
  Message::end(sendBuffer);
  if ((connectionSM_.getState() & 0x02) != 0u) {
    MPI_Isend(sendBuffer.data(), sendBuffer.size(), MPI_INT, partnerRank_, MPILocalMsgSetup, MPI_COMM_WORLD, &sendRequest_);
    commmanager_.logPrint("SEND_SetupM", partnerRank_, sendBuffer);
  } else {
    MPI_Isend(sendBuffer.data(), sendBuffer.size(), MPI_INT, partnerRank_, MPILocalMsgExchange, MPI_COMM_WORLD, &sendRequest_);
    commmanager_.logPrint("SEND_dynblk", partnerRank_, sendBuffer);
  }
  logger::get().trace(logger::Category::CommExch, "({:3}): send({:4}) {}", blockmanager_.getTimestep(), sendBuffer.size(), partnerRank_);
}

void Communicator::receive(const int count) {
  int timestep = -1;

  recvBuffer.resize(count);
  MPI_Recv(recvBuffer.data(), count, MPI_INT, partnerRank_, MPILocalMsgExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  commmanager_.logPrint("RECV_dynblk", partnerRank_, recvBuffer);
  if (connectionSM_.getState() != 0u) {
    setupConnectionRecv();
  } else {
    Message::read(recvBuffer, MessageID::timestep, timestep);

    if (timestep == blockmanager_.getTimestep()) {
      received_ = true;
    } else {
      if (receivedFCS_ == wasSentFCS_) {  // If a FCS was sent we have to wait until it was received.
        received_ = true;
      } else {
        received_ = false;
      }
    }
  }

  logger::get().trace(logger::Category::CommExch, "({:3},{:3}): recv({:4}) {} ({})", blockmanager_.getTimestep(), timestep, count, partnerRank_, received_);
  readAnnouncements();
}

void Communicator::receiveNew(const int count) {
  recvBuffer.resize(count);
  MPI_Recv(recvBuffer.data(), count, MPI_INT, partnerRank_, MPILocalMsgSetup, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  commmanager_.logPrint("RECV_DynNew", partnerRank_, recvBuffer);
  if (connectionSM_.getState() != 0u) {
    setupConnectionRecv();
  }

  int timestep = -1;
  Message::read(recvBuffer, MessageID::timestep, timestep);

  logger::get().trace(logger::Category::CommExch, "({:3},{:3}): new ({:4}) {} ({})", blockmanager_.getTimestep(), timestep, count, partnerRank_, received_);
}

void Communicator::setupConnectionSend() {
  Message::append(sendBuffer, MessageID::state, sendState_, blockmanager_.getTimestep());
  processConnectionEvent(send_ANY);
}

void Communicator::setupConnectionRecv() {
  int timestep;
  int event;
  if (!Message::read(recvBuffer, MessageID::state, event, timestep)) return;

  if (timestep != blockmanager_.getTimestep()) {  // This is a message from the previous time step.
    received_ = false;
  } else {
    if (receivedFCS_ == wasSentFCS_) {  // If a FCS was sent we have to wait until it was received.
      received_ = true;
    } else {
      received_ = false;
    }
  }

  // this call setConnected if stat==0x00 and set received = false
  processConnectionEvent(event);
}

void Communicator::sendFCSExchange() {
  waitSend();
  sendBuffer.clear();

  std::vector<int> blockIDs;
  for (const auto& block : blockmanager_.getBlockSet()) {
    blockIDs.push_back(block.first);
  }
  Message::append(sendBuffer, MessageID::blockList, blockIDs);

  std::vector<int> commList;
  for (const auto& comm : commmanager_.commSet) {
    if (!comm.second->isConnected()) continue;
    commList.push_back(comm.first);
  }
  Message::append(sendBuffer, MessageID::commList, commList);

  commmanager_.appendLocalMessages(partnerRank_);

  announceBlocks_.clear();
  announceComms_.clear();

  Message::end(sendBuffer);
  MPI_Isend(sendBuffer.data(), sendBuffer.size(), MPI_INT, partnerRank_, MPILocalFCSExchange, MPI_COMM_WORLD, &sendRequest_);
  commmanager_.logPrint("SEND_FCSExc", partnerRank_, sendBuffer);

  logger::get().trace(logger::Category::CommFCT, "Sent FCS to {}", partnerRank_);
}

void Communicator::receiveFCSExchange(const int count) {
  recvBuffer.resize(count);

  MPI_Recv(recvBuffer.data(), count, MPI_INT, partnerRank_, MPILocalFCSExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  commmanager_.logPrint("RECV_FCSExc", partnerRank_, recvBuffer);
  readAnnouncements();
  commmanager_.appendAnnounceComm(partnerRank_);
  logger::get().trace(logger::Category::CommFCT, "Received FCS from {}", partnerRank_);

  receivedFCS_ = true;
  received_    = true;
  setConnected();
}

/**
 * Probe for a message and receive if there is one.
 * For connection during the setup phase a setup message is probed before.
 * @note If a setup and regular message is received at this timestep, the setup message is from the previous timestep.
 */
void Communicator::probe() {
  if (received_ && (receivedFCS_ == wasSentFCS_)) return;

  while (true) {
    MPI_Status status;
    int waitingMessage;
    MPI_Iprobe(partnerRank_, MPI_ANY_TAG, MPI_COMM_WORLD, &waitingMessage, &status);
    if (waitingMessage == 0) return;

    int count;
    MPI_Get_count(&status, MPI_INT, &count);
    switch (status.MPI_TAG) {
      case MPILocalMsgSetup:
        receiveNew(count);
        break;
      case MPILocalMsgExchange:
        receive(count);
        return;
      case MPILocalFCSExchange:
        receiveFCSExchange(count);
        return;
      default:
        return;
    }
  }
}

void Communicator::processConnectionEvent(unsigned int event) {
  int oldState = connectionSM_.getState();
  connectionSM_.process_event(event);
  sendState_ = calcSendState(connectionSM_.getState());
  cnt_++;
  logger::get().trace(logger::Category::CommExch, "{}{}({:03}): {:#04x}---{:#04x}-->{:#04x}({:#04x})", partnerRank_, (event == 0 ? 's' : 'r'), cnt_, oldState, event, connectionSM_.getState(), sendState_);
  //TODO: function call
  if (connectionSM_.getState() == 0x00) setConnected();
}

/** Append Announcement of blocks and comms to the message.
 */
void Communicator::appendMsgAnnouncements() {
  Message::append(sendBuffer, MessageID::blockList, announceBlocks_);
  announcedBlocks_.insert(announceBlocks_.begin(), announceBlocks_.end());

  std::stringstream result;
  std::copy(announceBlocks_.begin(), announceBlocks_.end(), std::ostream_iterator<int>(result, " "));
  logger::get().trace(logger::Category::CommAnnc, "Announce blocks to {}: {}", partnerRank_, result.str());

  announceBlocks_.clear();

  Message::append(sendBuffer, MessageID::commList, announceComms_);
  announceComms_.clear();
  Message::append(sendBuffer, MessageID::delComm, deletedComms_);
  deletedComms_.clear();

  Message::append(sendBuffer, MessageID::load, commmanager_.getLocalLoad());
}

void Communicator::readAnnouncements() {
  readAnnouncedBlocks();
  readAnnouncedComms();
  readAnnouncedLoad();
}

/**
 * Read announced Blocks.
 * The blocksList message part sends the list of new blocks.  This blocks are cached in the incomingBlocks set.
 */
void Communicator::readAnnouncedBlocks() {
  // incomingBlocks.clear();
  Message::read(recvBuffer, MessageID::blockList, incomingBlocks_);
  std::stringstream result;
  std::copy(incomingBlocks_.begin(), incomingBlocks_.end(), std::ostream_iterator<int>(result, " "));
  logger::get().trace(logger::Category::CommInc, "Incoming blocks from {}: {}", partnerRank_, result.str());
}

/**
 * Read announced Comms.
 * delComm are erased from the knownComms-set.
 * commList is added to the knownComms-set.
 * newComms are created and set connected. They will make a FTC.
 * @note deleted comms can be directly recreated, so they are in delComm and commList. To be sure that the communicator
 * is in the knownComms-set, first delete then append it.
 */
void Communicator::readAnnouncedComms() {
  int start;
  int end;
  std::tie(start, end) = Message::findMessage(recvBuffer, MessageID::delComm);
  if (start != -1) {
    for (int pos = start; pos < end; pos++) {
      if (newCommBuffer_.count(recvBuffer[pos]) > 0) continue;
      knownComms_.erase(recvBuffer[pos]);
    }
  }

  Message::read(recvBuffer, MessageID::commList, knownComms_);

  std::map<int, std::vector<int>> fastComms;
  Message::read(recvBuffer, MessageID::newComm, fastComms);

  // For all connections with me: create partner comms and append my new partner to announce if the comm doesn't get the
  // message or to the known comms if it get the message.
  // For all other connections without me: Add the commID to the known comms if it get the message.
  if (fastComms.find(blockmanager_.getMPIRank()) != fastComms.end()) {
    for (const auto commID : fastComms.at(blockmanager_.getMPIRank())) {
      auto comm = commmanager_.provideCommunicator(commID);
      comm->setConnected();
      for (auto partner : commmanager_.commSet) {
        if (partner.first == commID) continue;  // no self announcements
        if (!isInKnownComm(partner.first)) {
          // announce if source don't send to partner
          partner.second->appendAnnounceComm(commID);
        }
      }
    }
  }

  for (const auto& target : fastComms) {
    if (commmanager_.commSet.find(target.first) == commmanager_.commSet.end()) continue;
    auto comm = commmanager_.commSet.at(target.first);
    for (const auto commID : target.second) {
      comm->appendKnownComm(commID);
      comm->appendNewCommBuffer(commID);
    }
  }
}

/**
 * Read the announced load.
 */
void Communicator::readAnnouncedLoad() {
  Message::read(recvBuffer, MessageID::load, transmittedLoad_);
}

/**
 * Calculate the send state from the connection state.
 *
 * @note the initial send state has to be set to 0x01 for the initial connection state 0x33.  It is not calculated by
 * this function.
 *
 * @param connectionState state of the connection
 *
 * @return sent state
 */
int Communicator::calcSendState(const int connectionState) {
  int sendState = 0x11 & connectionState;

  int keepSending = connectionState & 0x44;
  if (keepSending != 0) {
    sendState = keepSending >> 2;
  }

  return sendState;
}

void Communicator::sendingBuffer() {
  if (!isConnected()) return;
  auto* sendPtr = const_cast<void*>(sendPackedBuffer_.data());
  auto sendSize = static_cast<int>(sendPackedBuffer_.size());
  MPI_Isend(sendPtr, sendSize, MPI_CHAR, partnerRank_, MPILocalPackExchange, MPI_COMM_WORLD, &sendBufferRequest_);
  // commmanager.logPrint("RECV_Buffer", partnerRank, recvBuffer);
}

void Communicator::probeBuffer() {
  if (received_ && (receivedFCS_ == wasSentFCS_)) return;

  while (true) {
    MPI_Status status;
    int waitingMessage;
    MPI_Iprobe(partnerRank_, MPI_ANY_TAG, MPI_COMM_WORLD, &waitingMessage, &status);
    if (waitingMessage == 0) return;

    int count;
    switch (status.MPI_TAG) {
      case MPILocalPackExchange:
        MPI_Get_count(&status, MPI_CHAR, &count);
        receiveBuffer(count);
        return;
      case MPILocalFCSExchange:
        MPI_Get_count(&status, MPI_INT, &count);
        receiveFCSExchange(count);
        return;
      case MPILocalMsgSetup:
        MPI_Get_count(&status, MPI_INT, &count);
        receiveSetupClean(count);
      default:
        return;
    }
  }
}

/**
 * Receives a setup message, from the DynBlock-step. This happens when in DynBlock a setup is started and in the load
 * balance step a FCS is done.
 *
 * @note maybe the sender could cancel the message if he get a FCS
 *
 * @param count The length of the message.
 */
void Communicator::receiveSetupClean(const int count) {
  recvBuffer.resize(count);
  MPI_Recv(recvBuffer.data(), count, MPI_INT, partnerRank_, MPILocalMsgSetup, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  commmanager_.logPrint("RECV_SClean", partnerRank_, recvBuffer);

  int timestep = -1;
  Message::read(recvBuffer, MessageID::timestep, timestep);

  logger::get().trace(logger::Category::CommExch, "({:3},{:3}): clean new ({:4}) {} ({})", blockmanager_.getTimestep(), timestep, count, partnerRank_, received_);
}

void Communicator::receiveBuffer(const int size) {
  recvPackedBuffer_.reset();
  recvPackedBuffer_.resize(size);
  void* recvPtr = recvPackedBuffer_.data();
  MPI_Recv(recvPtr, size, MPI_CHAR, partnerRank_, MPILocalPackExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  received_ = true;
  readMessage();
  commmanager_.logPrint("RECV_Buffer", partnerRank_, recvBuffer);
  readAnnouncements();
}

void Communicator::readMessage() {
  if (recvPackedBuffer_.size() == 0) return;
  recvPackedBuffer_.unpack(recvBuffer);
}

/**
 * Update the blocks with the IDs from incomingBlocks.
 * The incoming IDs are moved and append to the knownBlocks set.
 */
void Communicator::updateBlocks(const CommManager& commManager) {
  if (!isConnected()) return;

  for (const auto& id : incomingBlocks_) {
    if (!insert(id)) continue;  // no new element inserted

    blockmanager_.updateBlocks(id, partnerRank_, commManager);
  }

  incomingBlocks_.clear();
}

/**
 * Update announced blocks.
 */
void Communicator::updateNeighbors(const CommManager& commManager) {
  if (!isConnected()) return;

  for (const auto& id : announcedBlocks_) {
    blockmanager_.updateNeighbors(id, commManager);
  }

  announcedBlocks_.clear();
}

/**
 * Insert an ID to the knownNeighbors. Increase the counter if the ID already known.
 *
 * @param id The ID.
 *
 * @return true if a new element is insert.
 */
bool Communicator::insert(int id) {
  auto ret = knownBlocks_.insert(id);
  return ret.second;
}

void Communicator::appendIncomingBlock(const int id) {
  incomingBlocks_.insert(id);
}

void Communicator::appendAnnounceBlock(const int id) {
  announceBlocks_.insert(id);
}

bool Communicator::isInAnnounce(const int id) const {
  return announceBlocks_.count(id) > 0;
}

bool Communicator::isInKnownComm(const int commID) const {
  return knownComms_.count(commID) > 0;
}

void Communicator::appendAnnounceComm(const int rank) {
  announceComms_.insert(rank);
  deletedComms_.erase(rank);
}

void Communicator::appendKnownComm(const int commID) {
  knownComms_.insert(commID);
}

void Communicator::appendNewCommBuffer(const int commID) {
  newCommBuffer_.insert(commID);
}

void Communicator::removeKnownComm(const int commID) {
  knownComms_.erase(commID);
}

void Communicator::appendDeleteComm(const int rank) {
  deletedComms_.insert(rank);
  announceComms_.erase(rank);
}

/**
 * Find the ID.
 *
 * @param id The ID.
 *
 * @return true if known, false else.
 */
bool Communicator::find(int id) const {
  return knownBlocks_.count(id) > 0;
}

/**
 * Remove a block from the communicator
 *
 * @param id  The block to be removed.
 */
void Communicator::removeBlock(const unsigned int id) {
  knownBlocks_.erase(id);
}

}  // namespace nastja
