/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <cstddef>
namespace nastja {

class StateMachine;

struct TransitionEntry {
  unsigned int currentState;
  unsigned int event;
  unsigned int nextState;
  void (*stateAction)(StateMachine* SM);
};

class StateMachine {
public:
  explicit StateMachine(const TransitionEntry* const table, const std::size_t size)
      : table_{table}, tableSize_{size} {
    currentState_ = table[0].currentState;
  }

  void setState(const unsigned int initial_state) { currentState_ = initial_state; }
  unsigned int getState() const { return currentState_; }

  void process_event(unsigned int event) {
    TransitionEntry const* entry           = table_begin();
    TransitionEntry const* const entry_end = table_end();

    while ((entry != entry_end)) {
      if (entry->currentState == currentState_ && entry->event == event) {
        currentState_ = entry->nextState;
        break;
      }
      entry++;
    }
  }

  void execute_state() {
    TransitionEntry const* entry           = table_begin();
    TransitionEntry const* const entry_end = table_end();

    while ((entry != entry_end)) {
      if (entry->currentState == currentState_) {
        if (entry->stateAction != nullptr) (entry->stateAction)(this);
        break;
      }
      entry++;
    }
  }

private:
  TransitionEntry const* table_begin() { return &table_[0]; }
  TransitionEntry const* table_end() { return &table_[tableSize_]; }

  unsigned int currentState_;
  const TransitionEntry* const table_;
  const std::size_t tableSize_;
};

}  // namespace nastja
