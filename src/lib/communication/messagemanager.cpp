/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "messagemanager.h"

namespace nastja {

MessageManager::MessageManager(block::BlockManager& blockmanager)
    : blockmanager_{blockmanager} {
  totalCores_ = blockmanager.getMPISize();
  // initRing();
  initManhattan(3);
}

void MessageManager::initRing() {
  int rank = blockmanager_.getMPIRank();

  ttl_ = blockmanager_.getMPISize() / 2;

  int left  = (totalCores_ + rank - 1) % totalCores_;
  int right = (rank + 1) % totalCores_;

  sourceRanks_.push_back(left);
  sourceRanks_.push_back(right);
  targetRanks_.push_back(left);
  targetRanks_.push_back(right);
}

void MessageManager::initManhattan(const int dimension) {
  std::vector<int> n = getDimensions(dimension);

  ttl_ = getManhattanDiameter(n);

  std::vector<int> coord = getCoordinate(blockmanager_.getMPIRank(), n);

  int sum = 0;
  for (int i : coord) {
    sum += i;
  }

  for (int d = 0; d < dimension; d++) {
    int lower = getLower(coord, d, n);
    int upper = getUpper(coord, d, n);

    if ((sum - coord[d]) % 2 == 0) {
      sourceRanks_.push_back(lower);
      targetRanks_.push_back(upper);
    } else {
      sourceRanks_.push_back(upper);
      targetRanks_.push_back(lower);
    }
  }
}

int MessageManager::getLower(const std::vector<int>& coord, const int d, const std::vector<int>& n) {
  std::vector<int> tmp(coord);

  tmp[d]--;
  if (tmp[d] < 0) tmp[d] = n[d] - 1;

  unsigned int index = getIndex(tmp, n);
  while (index >= totalCores_) {
    tmp[d]--;
    index = getIndex(tmp, n);
  }

  return index;
}

int MessageManager::getUpper(const std::vector<int>& coord, const int d, const std::vector<int>& n) {
  std::vector<int> tmp(coord);

  tmp[d]++;
  if (tmp[d] >= n[d]) tmp[d] = 0;

  unsigned int index = getIndex(tmp, n);
  if (index >= totalCores_) {
    tmp[d] = 0;
    index  = getIndex(tmp, n);
  }

  return index;
}

std::vector<int> MessageManager::getDimensions(const int dimension) {
  std::vector<int> n(dimension);

  int remain = totalCores_;
  for (int d = 0; d < dimension; d++) {
    int size_dim = ceil(pow(remain, 1.0 / (dimension - d)));

    remain = ceil((double)remain / (double)size_dim);
    n[d]   = size_dim;
  }

  return n;
}

/**
 * Calculates the diameter of a Manhattan Street Network with the given sizes per dimensions.
 *
 * @param dimensions  The vector of the sizes per dimension.
 *
 * @return The diameter.
 */
int MessageManager::getManhattanDiameter(const std::vector<int>& dimensions) {
  bool allMod4 = true;

  double sum = 0;
  for (int dimension : dimensions) {
    sum += dimension;

    if (!(allMod4 && dimension % 4 == 0)) allMod4 = false;
  }

  return ceil(sum * 0.5) + (allMod4 ? 1 : 0);
}

/**
 * Get the index in a multidimensional coordinate system.
 *
 * @param coordinate Multidimensional coordinates.
 * @param size       Size of the multidimensional coordinates.
 *
 * @return The index.
 */
int MessageManager::getIndex(const std::vector<int>& coordinate, const std::vector<int>& size) {
  int sum = 0;

  for (int d = size.size() - 1; d >= 0; d--) {
    sum = sum * size[d] + coordinate[d];
  }

  return sum;
}

/**
 * Get the multidimensional coordinates.
 *
 * @param index Index in a multidimensional coordinate system.
 * @param size  Size of the multidimensional coordinates.
 *
 * @return A vector of the multidimensional coordinates.
 */
std::vector<int> MessageManager::getCoordinate(const int index, const std::vector<int>& size) {
  std::vector<int> coordinate(size.size());

  int remain = index;
  for (int d = size.size() - 1; d >= 0; d--) {
    int hyperplane = 1;
    for (int i = d - 1; i >= 0; i--) {
      hyperplane *= size[i];
    }
    coordinate[d] = remain / hyperplane;
    remain -= coordinate[d] * hyperplane;
  }

  return coordinate;
}

// all blocks to all blocks for Debugging
BlockRankSet MessageManager::Allgather_all(std::vector<int>& /*localNewBlocks*/) {
  BlockRankSet blockList;
  int mpiSize    = blockmanager_.getMPISize();
  const int size = 100;
  std::vector<int> sendBuf(size);
  std::vector<int> recvBuf(size * mpiSize);
  auto it = blockmanager_.getBlockSet().begin();
  for (int i = 0; i < size; i++) {
    if (it != blockmanager_.getBlockSet().end()) {
      sendBuf[i] = (*it).first;
      it++;
    } else {
      sendBuf[i] = -1;
    }
    printf("%d,", sendBuf[i]);
  }
  MPI_Allgather(sendBuf.data(), size, MPI_INT, recvBuf.data(), size, MPI_INT, MPI_COMM_WORLD);
  for (int r = 0; r < mpiSize; r++) {
    for (int i = 0; i < size; i++) {
      if (recvBuf[size * r + i] >= 0) {
        printf("(%d,%d,%d[%d]) ", i, recvBuf[size * r + i], r, size * r + i);
        blockList.insert(std::make_pair(recvBuf[size * r + i], r));
      }
    }
  }
  return blockList;
}

// new blocks to all
BlockRankSet MessageManager::Allgather(std::vector<int>& localNewBlocks) {
  BlockRankSet blockList;
  int mpiSize    = blockmanager_.getMPISize();
  const int size = 100;
  std::vector<int> sendBuf(size);
  std::vector<int> recvBuf(size * mpiSize);

  for (unsigned int i = 0; i < localNewBlocks.size(); i++) {
    sendBuf[i] = localNewBlocks[i];
  }
  sendBuf[localNewBlocks.size()] = -1;

  MPI_Allgather(sendBuf.data(), size, MPI_INT, recvBuf.data(), size, MPI_INT, MPI_COMM_WORLD);

  for (int r = 0; r < mpiSize; r++) {
    for (int j = 0; j < size; j++) {
      if (recvBuf[size * r + j] < 0) break;

      printf("(%d,%d,%d[%d]) ", j, recvBuf[size * r + j], r, size * r + j);
      blockList.insert(std::make_pair(recvBuf[size * r + j], r));
    }
  }
  return blockList;
}

/**
 * Append localNewBlocks to the messages and send it to all peers.
 * Messages hold all new Blocks from previous receiving.
 *
 * @param localNewBlocks Vector of all new block IDs.
 */
void MessageManager::sendPeers(std::vector<int>& localNewBlocks) {
  // wait for send and clear
  MPI_Waitall(sendRequests_.size(), sendRequests_.data(), MPI_STATUS_IGNORE);
  sendRequests_.clear();

  int rank = blockmanager_.getMPIRank();

  blockList_.clear();

  // Delete old messages in buffer
  auto it = buffer_.begin();
  while (it != buffer_.end()) {
    if ((it->second.second--) == 0) {
      blockList_.insert(std::make_pair(it->first, it->second.first));
      it = buffer_.erase(it);
    } else {
      ++it;
    }
  }

  // insert new messages
  for (const auto id : localNewBlocks) {
    auto pair = std::make_pair(id, std::make_pair(rank, ttl_));
    buffer_.insert(pair);
    messages_.insert(pair);
  }

  // sending
  sendBuffer_.clear();
  for (const auto& message : messages_) {
    sendBuffer_.push_back(message.first);
    sendBuffer_.push_back(message.second.first);
    sendBuffer_.push_back(message.second.second);
  }

  for (const auto target : targetRanks_) {
    MPI_Request sendRequest;
    MPI_Isend(sendBuffer_.data(), sendBuffer_.size(), MPI_INT, target, MPIGlobalMsgExchange, MPI_COMM_WORLD, &sendRequest);
    sendRequests_.push_back(sendRequest);
  }
}

bool MessageManager::probe(const int source) {
  int waitingMessage;
  MPI_Status status;
  MPI_Iprobe(source, MPIGlobalMsgExchange, MPI_COMM_WORLD, &waitingMessage, &status);

  if (waitingMessage == 0) return false;

  int count;
  MPI_Get_count(&status, MPI_INT, &count);
  recvBuffer_.clear();
  recvBuffer_.resize(count);
  MPI_Recv(recvBuffer_.data(), count, MPI_INT, status.MPI_SOURCE, MPIGlobalMsgExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  // find new messages
  for (int j = 0; j < count; j += 3) {
    if (buffer_.find(recvBuffer_[j]) == buffer_.end()) {
      auto pair = std::make_pair(recvBuffer_[j], std::make_pair(recvBuffer_[j + 1], recvBuffer_[j + 2]));
      buffer_.insert(pair);
      messages_.insert(std::make_pair(recvBuffer_[j], std::make_pair(recvBuffer_[j + 1], recvBuffer_[j + 2] - 1)));
    }
  }

  return true;
}

BlockRankSet MessageManager::recvPeers() {
  messages_.clear();

  std::vector<bool> received(sourceRanks_.size());

  // receiving
  unsigned int cntReceived = 0;
  do {
    for (unsigned int i = 0; i < sourceRanks_.size(); i++) {
      if (!received[i]) {
        if (probe(sourceRanks_[i])) {
          received[i] = true;
          cntReceived++;
        }
      }
    }
  } while (cntReceived < sourceRanks_.size());

  return blockList_;
}

void MessageManager::send(std::vector<int>& localNewBlocks) {
  sendPeers(localNewBlocks);
}

BlockRankSet MessageManager::recv(std::vector<int>& /*localNewBlocks*/) {
  return recvPeers();
}

}  // namespace nastja
