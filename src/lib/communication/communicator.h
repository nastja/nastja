/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/block/blockmanager.h"
#include "lib/communication/communicationmanager.h"
#include "lib/communication/statemachine.h"
#include <set>
#include <vector>

namespace nastja {

class Communicator {
  friend class CommManager;
  friend class TESTING_Communicator;

public:
  explicit Communicator(block::BlockManager& blockmanager, CommManager& commmanager);

  void send();
  void probe();
  void receiveNew(int count);
  void waitSend() { MPI_Wait(&sendRequest_, MPI_STATUSES_IGNORE); }
  void waitBufferSend() { MPI_Wait(&sendBufferRequest_, MPI_STATUSES_IGNORE); }
  bool testReceived() const;
  void cleanReceivedStatus() { received_ = false; }
  void setReceivedStatus() { received_ = true; }
  void setConnected();
  bool isConnected() { return connectionSM_.getState() == 0x00; }

  void sendingBuffer();
  void probeBuffer();
  void receiveBuffer(int size);
  // TODO: find a better solution, use a common memory for all communicators.
  Archive& getSendArchive() { return sendPackedBuffer_; }
  Archive& getRecvArchive() { return recvPackedBuffer_; }

  bool insert(int id);
  bool find(int id) const;
  void removeBlock(unsigned int id);
  bool isInAnnounce(int id) const;
  bool isInKnownComm(int commID) const;
  void appendIncomingBlock(int id);
  void appendAnnounceBlock(int id);
  void appendAnnounceComm(int rank);
  void appendDeleteComm(int rank);
  void appendKnownComm(int commID);
  void appendNewCommBuffer(int commID);
  void removeKnownComm(int commID);
  void appendMsgAnnouncements();
  void clearNewCommBuffer() { newCommBuffer_.clear(); }

  std::set<int>& getAnnounceComms() { return announceComms_; }
  std::set<int>& getAnnounceBlocks() { return announceBlocks_; }
  std::set<int>& getKnownComms() { return knownComms_; }
  std::set<int>& getKnownBlocks() { return knownBlocks_; }

  void updateBlocks(const CommManager& commManager);
  void updateNeighbors(const CommManager& commManager);

  void setPartnerRank(const int rank) { partnerRank_ = rank; };
  int getPartnerRank() const { return partnerRank_; };

  double getLoad() const { return transmittedLoad_; }
  int getNumOfBlocks() const { return knownBlocks_.size(); }

  void setOfferedSpareBlocks(int value) { offeredSpareBlocks_ = value; }
  void incOfferedSpareBlocks() { offeredSpareBlocks_++; }
  void decOfferedSpareBlocks() { offeredSpareBlocks_--; }
  int getOfferedSpareBlocks() const { return offeredSpareBlocks_; }

  void readMessage();

  std::vector<int> recvBuffer;  ///< buffer for receiving data
  std::vector<int> sendBuffer;  ///< buffer for sending data

private:
  static int calcSendState(int connectionState);
  void receive(int count);
  void setupConnectionSend();
  void setupConnectionRecv();
  void processConnectionEvent(unsigned int event);

  void sendFCSExchange();
  void receiveFCSExchange(int count);
  void receiveSetupClean(int count);

  void readAnnouncements();
  void readAnnouncedBlocks();
  void readAnnouncedComms();
  void readAnnouncedLoad();

  std::set<int> knownBlocks_;      ///< holds a set of all blocks this Communicator knows from the communication partner.
  std::set<int> incomingBlocks_;   ///< holds a set of all blocks this Communicator get announced from the communication partner, these become knowBlocks later.
  std::set<int> announceBlocks_;   ///< holds a set of all blocks this Communicator has to announce to the communication partner.
  std::set<int> announcedBlocks_;  ///< holds a set of all blocks this Communicator has announced to the communication partner.
  std::set<int> knownComms_;       ///< holds a set of all Communicators this Communicator knows from the communication partner.
  std::set<int> announceComms_;    ///< holds a set of all Communicators this Communicator has to announce to the communication partner.
  std::set<int> deletedComms_;     ///< holds a set of all Communicators this Communicator has to announce for deletion to the communication partner.
  std::set<int> newCommBuffer_;    ///< holds a temporary set of all Communicators that created in this step.

  int partnerRank_{};                                ///< The rank of the partner.
  MPI_Request sendRequest_{MPI_REQUEST_NULL};        ///< sending request, for check if send is in progress
  MPI_Request sendBufferRequest_{MPI_REQUEST_NULL};  ///< sending request, for check if send is in progress

  bool received_{};  ///< Flag, has received data.

  bool receivedFCS_{false};  ///< Flag, has received a FCS.
  bool wasSentFCS_{false};   ///< Flag, a FCS was sent.
  int sendState_;            ///< The stat of the sending process.
  int cnt_{0};

  StateMachine connectionSM_;          ///< holds the state of the connection. Based on the TCP handshake.
                                       ///< getState() == 0x00 - established
  block::BlockManager& blockmanager_;  ///< The reverence to the blockmanager.
  CommManager& commmanager_;           ///< The reverence to the communicationmanager.

  double transmittedLoad_{0};  ///< The load of the partner

  // Values for LoadBalance
  int offeredSpareBlocks_{0};  ///< The number of offerd spare blocks.

  Archive sendPackedBuffer_;  ///< The packed buffer archive for sending.
  Archive recvPackedBuffer_;  ///< The packed buffer archive for receiving.
};

}  // namespace nastja
