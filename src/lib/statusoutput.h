/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger.h"
#include "lib/config/config.h"
#include "lib/simdata/simdata.h"
#include "fmt/chrono.h"
#include <chrono>
#include <cmath>
#include <iomanip>

namespace nastja {

using namespace config::literals;

class StatusOutput {
public:
  explicit StatusOutput(SimData& simdata) : simdata_{simdata}, endTime_{MPI_Wtime()} {
    /// @key{Settings.statusoutput, int, 100}
    /// Print out the status of a time-step every n-th step.
    eachNthStep_     = simdata.getConfig().getValue<int>("Settings.statusoutput"_jptr, 100);
    startTime_       = MPI_Wtime();
    resumedTimestep_ = simdata.timestep;
  }

  void start() {
    logger::get().onelog("Starting simulation...");
    startTime_ = MPI_Wtime();
  }

  void finished() const {
    logger::get().onelog("Simulation completed. Total time used: {} average time per step: {} s", getHumanReadableDuration(timeSum_), getAvgTime());
  }

  /**
   * Print a status line with current time-step and estimated time to complete (ETC).
   */
  void call() {
    endTime_          = MPI_Wtime();
    double neededTime = endTime_ - startTime_;
    timeSum_ += neededTime;
    statusTimeSum_ += neededTime;

    if ((eachNthStep_ != 0u) && simdata_.timestep % eachNthStep_ == 0) {
      double average   = statusTimeSum_ / eachNthStep_;
      double timeToEnd = static_cast<double>(simdata_.getTimesteps() - simdata_.timestep) * timeSum_ / static_cast<double>(simdata_.timestep - resumedTimestep_);
      logger::get().onelog("Time-step: {:7} ({:g}) {:11f} s/step ETC: {}", simdata_.timestep, simdata_.getSimulatedTime(), average, getHumanReadableDuration(timeToEnd));
      statusTimeSum_ = 0;
    }
    startTime_ = endTime_;
  }

private:
  double getAvgTime() const {
    return timeSum_ / std::max(1lu, simdata_.getTimesteps());
  }

  /**
   * Gets the human readable duration.
   *
   * @param time  The duration in seconds.
   *
   * @return The human readable duration as hh:mm:ss.
   */
  static std::string getHumanReadableDuration(double time) {
    if (std::isinf(time)) return "--:--:--";

    auto seconds = std::chrono::seconds(static_cast<long long>(std::round(time)));
    auto hours   = std::chrono::duration_cast<std::chrono::hours>(seconds);

    return fmt::format("{:02}:{:%M:%S}", hours.count(), seconds);
  }

  SimData& simdata_;  ///< The reference to the SimData.

  double startTime_;         ///< The starttime of this time-step.
  double endTime_;           ///< The end time of this time-step.
  double timeSum_{0};        ///< The sum of the time used so far in the whole simulation.
  double statusTimeSum_{0};  ///< The sum of the time used since the last status output.

  unsigned int eachNthStep_;          ///< Execute the status output every n-th step.
  unsigned long resumedTimestep_{0};  ///< The initial time-step of a resumed run.
};

}  // namespace nastja
