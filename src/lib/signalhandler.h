/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger.h"
#include "lib/config/config.h"
#include "lib/simdata/simdata.h"
#include <cfenv>
#include <csignal>

namespace nastja {

using namespace config::literals;

// #pragma STDC FENV_ACCESS ON
#if defined(__APPLE__) && defined(__MACH__)
int feenableexcept(unsigned int excepts);
int fedisableexcept(unsigned int excepts);
#endif

extern "C" void handler(int signal);

/**
 * Class for signal handler.
 *
 * In slurm use 'sbatch --signal USR2@60' to send SIGUSR2 to the program.
 * For MPICH SIGUSR2 will be bypassed to processes.
 * Open MPI bypasses SIGUSR1 and SIGUSR2.
 * IntelMPI can propagate SIGINT, SIGALRM, and SIGTERM when I_MPI_JOB_SIGNAL_PROPAGATION is set.
 * ParastationMPI will forward all possible signals.
 */
class SignalHandler {
public:
  explicit SignalHandler(SimData& simdata) : simdata_{simdata} {
    /// @key{Settings.timestepguard, uint, 1}
    /// This number of time-steps is used as a guarantee for a clean shutdown. The guard is used to synchronize the
    /// processes. Due to the halo exchange, usually, the processes are synchronized. However, the exit signal can
    /// appear in two different time-steps. Therefore, all processes send their current time-step to all others. In the
    /// next time-step, the maximum time-step is received. The last time-step is then the maximum time-step plus two
    /// (another time-loop is needed to call the writers) plus this guard. Nevertheless, for a clean shutdown, it must
    /// be guaranteed that a final termination will not appear within this guarded time-steps. Use this guard if further
    /// time-steps are required for guarding.
    stepsOfTimeStepGuard_ = simdata.getConfig().getValue<unsigned int>("Settings.timestepguard"_jptr, 1);

    /// @key{Settings.handleFPE, string, "signal"}
    /// @values{"signal", "report", "ignore"}
    /// Determine how to handle floating-point exceptions.
    /// - _signal_ Sends the SIGFPE signal this will terminate the simulation with a debuggable crash.
    /// - _report_ Just report floating-point exceptions.
    /// - _ignore_ Ignores floating-point exceptions. Use this if the code may have exceptions that will be handled
    ///            later.
    auto handleFPE = simdata.getConfig().getValue<std::string>("Settings.handleFPE"_jptr, "signal");
    if (handleFPE == "signal") {
      // enable SIGFPE on floating-point exceptions
#ifdef __ARM_ARCH
      logger::get().onewarning("SIGFPE is not supported for ARM.");
#endif
      feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
    } else if (handleFPE == "report") {
      doReportFPE_ = true;
    }

    std::signal(SIGINT, &handler);   // sets up ctrl+c handler
    std::signal(SIGUSR2, &handler);  // sets up custom that is bypassing by MPI

    MPI_Comm_dup(MPI_COMM_WORLD, &emergencyComm_);  // The emergency exit reduce call must be independent of any other reduce call.
  }

  void fetch();

private:
  enum class EmergencyExitState {
    run,         ///< Normal run.
    requested,   ///< Received an exit signal, a last time-step is requested.
    determined,  ///< The last time-step is determined.
  };

  void determineLastTimeStep();

  /**
   * Gets the signal name.
   *
   * @param signal  The signal number.
   *
   * @return The string of the signal name.
   */
  static std::string getSignalName(int signal) {
    std::string signalName;
    switch (signal) {
      case SIGINT:
        signalName = "SIGINT";
        break;
      case SIGFPE:
        signalName = "SIGFPE";
        break;
      case SIGALRM:
        signalName = "SIGALRM";
        break;
      case SIGTERM:
        signalName = "SIGTERM";
        break;
      case SIGUSR1:
        signalName = "SIGUSR1";
        break;
      case SIGUSR2:
        signalName = "SIGUSR2";
        break;
    }

    return signalName;
  }

  /**
   * Reports floating-point exceptions.
   */
  static void reportFPE() {
    if (std::fetestexcept(FE_ALL_EXCEPT | ~FE_INEXACT) == 0) return;

    auto& logger = logger::get();
    if (std::fetestexcept(FE_DIVBYZERO) != 0) logger.warning("FE_DIVBYZERO");
    if (std::fetestexcept(FE_INVALID) != 0) logger.warning("FE_INVALID");
    if (std::fetestexcept(FE_OVERFLOW) != 0) logger.warning("FE_OVERFLOW");
    if (std::fetestexcept(FE_UNDERFLOW) != 0) logger.warning("FE_UNDERFLOW");

    std::feclearexcept(FE_ALL_EXCEPT);
  }

  EmergencyExitState eeState_{EmergencyExitState::run};  ///< The emergency exit state.
  bool doReportFPE_{false};                              ///< True to report floating-point exceptions.
  unsigned int stepsOfTimeStepGuard_{8};                 ///< Number of steps used for a clean shutdown.
  unsigned long exitTimeStep_{0};                        ///< Number of steps used for a clean shutdown.
  SimData& simdata_;                                     ///< The reference to the SimData.

  MPI_Comm emergencyComm_;  ///< The emergency exit mode needs its own communicator to not influence other collective comunications.
  MPI_Request request_{MPI_REQUEST_NULL};
};

}  // namespace nastja
