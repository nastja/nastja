/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cmdline.h"
#include "fmt/format.h"
#include <iostream>
#include "version.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#endif
#include <args.hxx>
#ifdef __clang__
#pragma clang diagnostic pop
#endif

namespace nastja {
namespace config {

void CommandLine::parse(int& argc, char* argv[]) {
  args::ArgumentParser parser("https://nastja.gitlab.io", "");
  args::ValueFlag<std::string> config_(parser, "config", "The JSON-config file.", {'c', "config"}, "config.json");
  args::ValueFlag<std::string> configstring_(parser, "configstring", "A JSON-string which overwrites objects in the config file.", {'C', "configstring"}, "");
  args::ValueFlag<std::string> outdir_(parser, "outdir", "The output directory.", {'o', "outdir"}, "outdir");
  args::ValueFlag<int> resume_(parser, "dump number", "Resume the simulation with the given dump.", {'R', "resume"}, -1);
  args::HelpFlag help(parser, "help", "Display this help menu.", {'h', "help"});

  try {
    parser.ParseCLI(argc, argv);
  } catch (args::Help) {
    std::cout << parser;
    exit(0);
  } catch (args::ParseError& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    exit(1);
  } catch (args::ValidationError& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    exit(0);
  }

  config = args::get(config_);

  configstring = args::get(configstring_);
  std::replace(configstring.begin(), configstring.end(), '\'', '"');

  resumeFrame = args::get(resume_);
  if (resumeFrame >= 0) {
    resume = true;
  }

  outdir = args::get(outdir_);
}

}  // namespace config
}  // namespace nastja
