/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "configutils.h"

namespace nastja {
namespace config {

const std::array<std::string, 8> typeNames{
    "int",            // i
    "unsigned int",   // j
    "long",           // l
    "unsigned long",  // m
    "double",         // d
    "float",          // f
    "bool",           // b
    "string",         // Ss
};

const char* getTypename(const char* c) {
  switch (c[0]) {
    case 'i': return typeNames[0].c_str();
    case 'j': return typeNames[1].c_str();
    case 'l': return typeNames[2].c_str();
    case 'm': return typeNames[3].c_str();
    case 'd': return typeNames[4].c_str();
    case 'f': return typeNames[5].c_str();
    case 'b': return typeNames[6].c_str();
    case 'S': return typeNames[7].c_str();
    default: return c;
  }
}

/**
 * Gets the json pointer from a given key string.
 *
 * @param key  The key stirng.
 */
json::json_pointer getJsonPointer(const std::string& key) {
  auto pointer = "/" + key;
  std::replace(pointer.begin(), pointer.end(), '.', '/');

  return json::json_pointer(pointer);
}

/**
 * Filling function for fields are appended, i.e., the arrays under the key "Filling".
 *
 * @param target  The target json object.
 * @param source  The source json object.
 */
void updateFilling(json& target, json& source) {
  for (auto element = source.begin(); element != source.end();) {
    if (!element.value().is_array() || !target.contains(element.key())) {  // use merge_patch
      ++element;
      continue;
    }

    for (const auto& object : element.value()) {
      target[element.key()].push_back(object);
    }
    element = source.erase(element);
  }
}

/**
 * Update values from source into target by using merge_patch. The only exception are in "Filling".
 *
 * @param target         The target json object.
 * @param source         The source json object.
 */
void update(json& target, json& source) {
  if (target.contains("Filling") && source.contains("Filling")) {
    updateFilling(target["Filling"], source["Filling"]);
  }

  target.merge_patch(source);
}

}  // namespace config
}  // namespace nastja
