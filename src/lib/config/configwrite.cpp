/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "configwrite.h"
#include "lib/io/writerreader.h"
#include <algorithm>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace nastja {
namespace config {

/**
 * Finds the end of a quoted string.
 *
 * @param s      The string.
 * @param start  The start position of the quoted string,i.e. character after `"`.
 *
 * @return The end position of the matched `"`,
 */
std::size_t find_string_end(const std::string& s, std::size_t start) {
  auto end = start;

  do {
    // find next quote
    end = s.find('"', end + 1);
    if (end == std::string::npos) return std::string::npos;
  } while (s[end - 1] == '\\');  // end when quote is not escaped; end of string

  return end;
}

/**
 * Finds the first character given in pattern outside of a quoted string.
 *
 * @param s        The string
 * @param pattern  The pattern.
 * @param start    The start.
 *
 * @return The position of the found character.
 */
std::size_t find_first_of_quoted(const std::string& s, const std::string& pattern, std::size_t start = 0) {
  while (true) {
    auto found = s.find_first_of(pattern + "\"", start);
    if (found == std::string::npos) return std::string::npos;

    if (s[found] == '"') {
      start = find_string_end(s, found + 1);
      if (start == std::string::npos) return std::string::npos;
      start++;
    } else {
      return found;
    }
  }
}

/**
 * Compress the innermost json array in one line consider with correct spacing, i.e., one space after a comma.
 *
 * @param js      The json string.
 * @param start   The start of the array in the string.
 * @param length  The length of the array string.
 */
void onelinearray(std::string& js, std::size_t start, std::size_t length) {
  // make one line
  std::replace(js.begin() + start, js.begin() + start + length, '\n', ' ');

  auto space_start = start;
  while (true) {
    // how many spaces does we have?
    auto spaces = js.find_first_not_of(" ", space_start) - space_start;

    // have we reached the end?
    if (space_start + spaces > start + length) return;

    // remove spaces and subtract from length
    js.erase(space_start, spaces);
    length -= spaces;

    // If here starts quoted string, skip it
    if (js[space_start] == '"') space_start = find_string_end(js, space_start + 1);

    // find next space_start
    space_start = js.find(' ', space_start + 1);

    // have we reached the end?
    if (space_start >= start + length) return;

    // keep space after comma
    if (js[space_start - 1] == ',') space_start++;
  }
}

/**
 * Compress a JSON dump, so that the inner most array is printed in one line.
 *
 * @param js  The json string.
 */
void compressJsonString(std::string& js) {
  auto arraystart = find_first_of_quoted(js, "[");
  if (arraystart == std::string::npos) return;

  while (true) {
    auto find = find_first_of_quoted(js, "[]", arraystart + 1);
    if (find == std::string::npos) return;

    if (js[find] == '[') {
      arraystart = find;
    } else {
      onelinearray(js, arraystart + 1, find - arraystart - 1);
      arraystart = find_first_of_quoted(js, "[", arraystart + 1);
      if (arraystart == std::string::npos) return;
    }
  }
}

void ConfigWrite::writeConfig(const std::string& path) const {
  int flag;
  int rank = 0;
  MPI_Initialized(&flag);
  if (flag != 0) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }
  if (rank > 0) return;

  std::stringstream filename;

  filename << path << "/config_save.json";
  WriterReader::checkPath(filename.str());
  std::ofstream outputFileStream(filename.str());

  auto json = jsonSave_.dump(2);
  compressJsonString(json);

  outputFileStream << json;
}

}  // namespace config
}  // namespace nastja
