/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/config/configutils.h"
#include "nlohmann/json.hpp"
#include <sstream>

namespace nastja {
namespace config {

using json = nlohmann::json;

class ConfigWrite {
public:
  explicit ConfigWrite() = default;

  template <typename T>
  void append(const json::json_pointer& jptr, const T& data) {
    jsonSave_[jptr] = data;
  }

  void writeConfig(const std::string& path) const;

private:
  json jsonSave_{};  ///< The json object for saving the read config values.
};

}  // namespace config
}  // namespace nastja
