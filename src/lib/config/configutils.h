/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "nlohmann/json.hpp"
#include <string>

namespace nastja {
namespace config {

using json = nlohmann::json;

json::json_pointer getJsonPointer(const std::string& key);

void updateFilling(json& target, json& source);
void update(json& target, json& source);

const char* getTypename(const char* c);

/// Conversion literals
namespace literals {

/**
 * _jptr conversion operator.
 *
 * @return The json pointer
 */
inline json::json_pointer operator"" _jptr(const char* s, std::size_t n) {
  return getJsonPointer(std::string(s, n));
}

}  // namespace literals

}  // namespace config
}  // namespace nastja
