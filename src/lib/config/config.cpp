/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/config/configutils.h"
#include "lib/evaluate/evaluator.h"
#include "lib/simd/simd.h"
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <utility>
#include "mpi.h"
#include "version.h"

namespace nastja {
namespace config {

Config::Config()
    : jsonConfig_{},
      evaluator_{std::make_unique<evaluate::Evaluator>()},  // Define in cpp, for better compile time.
      configWrite_{std::make_unique<ConfigWrite>()} {}

/**
 * Constructs a new instance.
 *
 * The command line interface is parse to include all config information from the given file.
 *
 * @param argc  The count of arguments.
 * @param argv  The arguments array.
 */
Config::Config(int argc, char* argv[]) : Config{} {
  printHeader();
  parseCmdLine(argc, argv);
  loadFile();
  addInfo();
  addEvaluations();
}

Config::~Config() = default;  // Define here to be aware of the incomplete type from Evaluator.

/// @cond INTERNAL
template <>
bool Config::isType<int>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number_integer();
}

template <>
bool Config::isType<unsigned int>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number_unsigned();
}

template <>
bool Config::isType<long>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number_integer();
}

template <>
bool Config::isType<unsigned long>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number_unsigned();
}

template <>
bool Config::isType<float>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number();
}

template <>
bool Config::isType<double>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_number();
}

template <>
bool Config::isType<bool>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_boolean();
}

template <>
bool Config::isType<std::string>(const json::json_pointer& jptr) const {
  return contains(jptr) && jsonConfig_[jptr].is_string();
}
/// @endcond

/**
 * Loads the config file.
 * Looks for files in the include object and append/update them. Append/update the json string from command line, if
 * there is any. Broadcast the resulting json string to all processes.
 *
 * @param filename  The filename.
 */
void Config::loadFile(const std::string& filename) {
  int rank{};
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) {
    std::ifstream config_doc(filename, std::ios::binary);

    if (!config_doc.is_open()) {
      fmt::print("Can not open config file '{}'\n", filename);
      exit(1);
    }
    try {
      config_doc >> jsonConfig_;
    } catch (json::parse_error& e) {
      logger::get().oneerror("Can not read config file:\n{}\nexception id {} at byte {}.\n", e.what(), e.id, e.byte);
      exit(1);
    }

    appendIncludeFiles(filename);

    if (!cmdline_.configstring.empty()) {
      appendFromString(cmdline_.configstring);
    }
  }

  exchangeConfig();
}

/**
 * Packs the config and exchanges it to other ranks.
 */
void Config::exchangeConfig() {
  std::vector<std::uint8_t> msgpackBuffer;
  int length{};
  int rank{};
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) {
    msgpackBuffer = json::to_msgpack(jsonConfig_);
    length        = msgpackBuffer.size();
  }

  MPI_Bcast(&length, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if (rank != 0) {
    msgpackBuffer.resize(length);
  }

  MPI_Bcast(msgpackBuffer.data(), length, MPI_CHAR, 0, MPI_COMM_WORLD);

  if (rank != 0) {
    jsonConfig_ = json::from_msgpack(msgpackBuffer);
  }
}

/**
 * Looks for included files and appends them.
 *
 * @param filename  The filename of the config file.
 */
void Config::appendIncludeFiles(const std::string& filename) {
  const auto pos  = filename.find_last_of('/');
  const auto base = filename.substr(0, pos + 1);

  for (auto itr : jsonConfig_["Include"]) {
    std::string includeFile = itr.get<std::string>();
    if (includeFile.at(0) != '/') {
      includeFile.insert(0, base);
    }
    logger::get().info("Load config include '{}'.", includeFile);
    appendFromFile(includeFile);
  }
}

/**
 * Appends a from file.
 *
 * @param filename  The filename.
 */
void Config::appendFromFile(const std::string& filename) {
  std::ifstream includeFile(filename);

  if (!includeFile.is_open()) {
    fmt::print("Can not open config include file '{}'\n", filename);
    exit(1);
  }

  json json;
  includeFile >> json;

  update(jsonConfig_, json);
}

/**
 * Appends a from string.
 *
 * @param strJson  The json string.
 */
void Config::appendFromString(const std::string& strJson) {
  json json;

  try {
    json = json::parse(strJson);
  } catch (json::parse_error& e) {
    logger::get().oneerror("Error during JSON-string interpretation:\n{}\nexception id {} at byte {}.\n", e.what(), e.id, e.byte);
    exit(1);
  }

  update(jsonConfig_, json);
}

/**
 * Loads the config from a string.
 *
 * @param strJson  The config json string.
 */
void Config::loadString(const std::string& strJson) {
  appendFromString(strJson);
  addEvaluations();
}

template <typename T>
T Config::parseValue(const json::json_pointer& jptr, const DefaultValue<T> defaultValue) const {
  T data;

  if (isType<T>(jptr)) {
    data = jsonConfig_[jptr].get<T>();
  } else if (defaultValue.has() && (!contains(jptr) || jsonConfig_[jptr].empty() || isEmptyString(jptr))) {
    data = defaultValue.get();
  } else if (isType<std::string>(jptr)) {
    try {
      data = evaluator_->evaluate(jsonConfig_[jptr].get<std::string>());
    } catch (const std::invalid_argument& e) {
      throw std::invalid_argument(fmt::format("Key {} exception: {}", jptr, e.what()));
    }
  } else {
    if (defaultValue.has()) {
      throw std::invalid_argument(fmt::format("Key {} expects type {}, but is '{}'.", jptr, getTypename(typeid(T).name()), jsonConfig_[jptr].dump()));
    }
    throw std::invalid_argument(fmt::format("Mandatory key {} is missing.", jptr));
  }

  appendToSave(jptr, data);

  return data;
}
template int Config::parseValue<int>(const json::json_pointer& jptr, const DefaultValue<int> defaultValue) const;
template unsigned int Config::parseValue<unsigned int>(const json::json_pointer& jptr, const DefaultValue<unsigned int> defaultValue) const;
template long Config::parseValue<long>(const json::json_pointer& jptr, const DefaultValue<long> defaultValue) const;
template unsigned long Config::parseValue<unsigned long>(const json::json_pointer& jptr, const DefaultValue<unsigned long> defaultValue) const;
template float Config::parseValue<float>(const json::json_pointer& jptr, const DefaultValue<float> defaultValue) const;
template double Config::parseValue<double>(const json::json_pointer& jptr, const DefaultValue<double> defaultValue) const;
template bool Config::parseValue<bool>(const json::json_pointer& jptr, const DefaultValue<bool> defaultValue) const;
template std::string Config::parseValue<std::string>(const json::json_pointer& jptr, const DefaultValue<std::string> defaultValue) const;

/**
 * Gets JSON object.
 *
 * @param jptr  The json pointer.
 *
 * @return Reference to the JSON object.
 */
json& Config::getObject(const json::json_pointer& jptr) {
  auto& data = jsonConfig_[jptr];
  appendToSave(jptr, data);
  return data;
}

/**
 * Adds config parameter to the evaluator.
 */
void Config::addEvaluations() {
  addConstants();
  addFunctions();
  loadVariables();
}

/**
 * Adds constants given in `DefineConstants`.
 */
void Config::addConstants() {
  auto& json = getObject(getJsonPointer("DefineConstants"));
  if (json.is_array()) {
    for (auto& itr : json) {
      evaluator_->addConstant(itr.get<std::string>());
    }
  }
}

/**
 * Adds functions defined in `DefineFunctions`.
 */
void Config::addFunctions() {
  auto& json = getObject(getJsonPointer("DefineFunctions"));
  if (json.is_array()) {
    for (auto& itr : json) {
      evaluator_->addFunction(itr.get<std::string>());
    }
  }
}

/**
 * Loads config variables to the evaluator.
 */
void Config::loadVariables() {
  std::array<int, 3> blockSize{};
  std::array<int, 3> blockCnt{};

  load1DArray(blockSize, getJsonPointer("Geometry.blocksize"), 0);
  load1DArray(blockCnt, getJsonPointer("Geometry.blockcount"), 0);
  auto dt = getValue<real_t>(getJsonPointer("Settings.deltax"), 1.0);
  auto dx = getValue<real_t>(getJsonPointer("Settings.deltat"), 1.0);

  evaluator_->setDomainVariables(blockSize[0] * blockCnt[0], blockSize[1] * blockCnt[1], blockSize[2] * blockCnt[2], dx, dt);
}

/**
 * Adds an information.
 */
void Config::addInfo() const {
  json info;

  auto t  = std::time(nullptr);
  auto tm = *std::localtime(&t);

#ifdef __INTEL_COMPILER
  char timestr[20];
  std::strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", &tm);
  info["starttime"] = timestr;
#else
  std::stringstream ss;
  ss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S");
  info["startTime"] = ss.str();
#endif

  info["githash"]   = GIT_HASH;
  info["gitbranch"] = GIT_BRANCH;
  info["version"]   = VERSION;

  if (HAS_APPLICATION) {
    info["application_githash"]   = APPLICATION_GIT_HASH;
    info["application_gitbranch"] = APPLICATION_GIT_BRANCH;
  }

  info["compiler"] = __VERSION__;

  info["instructionSet"] = simd::getInstructionSetName();

  int flag{};
  int size{};
  int rank = 0;
  MPI_Initialized(&flag);
  if (flag != 0) {
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }
  info["processes"] = size;
  if (rank == 0) {
    fmt::print("#Information: {}\n", info.dump(2));
  }
  appendToSave(getJsonPointer("#Information"), info);
}

/**
 * Prints the NAStJA header.
 */
void Config::printHeader() {
  int rank{};
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank != 0) return;

  std::string app{};
  if (HAS_APPLICATION) {
    app = fmt::format("{} v{}", APPLICATION, APPLICATION_VERSION);
  }

  fmt::print(R"(   _  _   _   ___ _      _  _
  | \| | /_\ / __| |_ _ | |/_\   {}
  | .` |/ _ \\__ \  _| || / _ \
  |_|\_/_/ \_\___/\__|\__/_/ \_\v{}
  Neoteric Autonomous STencil code for Jolly Algorithms

)",
                           app, VERSION);
}

/**
 * Links simdata elements to evaluator.
 *
 * @param simdata  The simdata.
 */
void Config::linkEvaluator(SimData& simdata) {
  evaluator_->link(simdata.totalTime, simdata.generator, simdata.globalGenerator);
}

}  // namespace config
}  // namespace nastja
