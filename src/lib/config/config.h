/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/utility.h"
#include "lib/config/cmdline.h"
#include "lib/config/configutils.h"
#include "lib/config/configwrite.h"
#include "lib/functions/functionregister.h"
#include "lib/math/boundingbox.h"
#include "lib/math/matrix3.h"
#include "lib/math/vector.h"
#include "lib/simdata/simdata.h"
#include "nlohmann/json.hpp"
#include <memory>
#include <random>
#include <set>
#include <string>
#include <sys/cdefs.h>
#include <utility>

namespace nastja {

class SimData;

// SFINAE, checks if a function has a getCategory method.
static auto hasCategory = isValid([](auto&& x) -> decltype(x.getCategory()) {});

template <class T>
auto getCategory(T& obj) -> typename std::enable_if<decltype(hasCategory(obj))::value, unsigned int>::type {
  return obj.getCategory();
}

template <class T>
auto getCategory(T& obj) -> typename std::enable_if<!decltype(hasCategory(obj))::value, unsigned int>::type {
  return 0u;
}

namespace evaluate {
class Evaluator;
}

/**
 * The configuration namespace.
 */
namespace config {

using json = nlohmann::json;

/**
 * This class describes a configuration.
 */
class Config {
public:
  /// @name Constructors
  /// @{

  explicit Config();
  explicit Config(int argc, char* argv[]);
  ~Config();

  Config(const Config&)  = delete;
  Config(Config&& other) = default;
  Config& operator=(const Config&) = delete;
  Config& operator=(Config&&) = delete;

  /// @}

  /// @name Element access (primitive data)
  /// @{

  /**
   * Get a value.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The data type.
   *
   * @return The value.
   */
  template <typename T>
  T getValue(const json::json_pointer& jptr) const {
    return parseValue(jptr, DefaultValue<T>());
  }

  /**
   * Get a value.
   *
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type.
   *
   * @return The value or the default value if not present in the json.
   */
  template <typename T>
  T getValue(const json::json_pointer& jptr, const T defaultValue) const {
    return parseValue(jptr, DefaultValue<T>(defaultValue));
  }

  /// @}

  /// @name Element access (vector data)
  /// @{

  /**
   * Gets a vector.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The data type of the vector elements.
   *
   * @return The vector.
   */
  template <typename T>
  std::vector<T> getVector(const json::json_pointer& jptr) const {
    unsigned int size = arraySize(jptr);

    std::vector<T> v;
    v.resize(size);
    loadNDArray_impl(v.data(), &size, 1, jptr, DefaultValue<T>());

    return v;
  }

  /**
   * Gets a vector2.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The data type of the vector elements.
   *
   * @return The vector2.
   */
  template <typename T>
  math::Vector2<T> getVector2(const json::json_pointer& jptr) const {
    unsigned int size = 2;

    math::Vector2<T> v;
    loadNDArray_impl(v.data(), &size, 1, jptr, DefaultValue<T>());

    return v;
  }

  /**
   * Gets a vector3.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The data type of the vector elements.
   *
   * @return The vector3.
   */
  template <typename T>
  math::Vector3<T> getVector3(const json::json_pointer& jptr) const {
    unsigned int size = 3;

    math::Vector3<T> v;
    loadNDArray_impl(v.data(), &size, 1, jptr, DefaultValue<T>());

    return v;
  }

  /**
   * Gets a boundingbox.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The data type of the vector elements.
   *
   * @return The boundingbox.
   */
  template <typename T>
  math::BoundingBox<T> getBoundingBox(const json::json_pointer& jptr) const {
    unsigned int size = 3;

    math::BoundingBox<T> b;
    loadNDArray_impl(b[0].data(), &size, 1, jptr / 0, DefaultValue<T>());
    loadNDArray_impl(b[1].data(), &size, 1, jptr / 1, DefaultValue<T>());
    b.extend(0, 1);  // In the config file the upper corner is included.

    return b;
  }

  /**
   * Gets the rotation marix from the Euler angles.
   *
   * @param jptr  The json pointer to the Euler angles.
   *
   * @return The rotation marix.
   */
  math::Matrix3<real_t> getRotationMatrix(const json::json_pointer& jptr) const {
    math::Vector3<real_t> angle{};

    if (isArray(jptr)) {
      /// @keyignore
      angle = getVector3<real_t>(jptr);
      angle *= M_PI / 180.0;
    }

    return math::Matrix3<real_t>(angle[0], angle[1], angle[2]);
  }

  /// @}

  /// @name Element access (n-dimensional linear array data)
  /// @{

  /**
   * Loads 1D data.
   *
   * @param array  The target for the as a std::array.
   * @param jptr   The json pointer.
   *
   * @tparam T  The data type of the array.
   * @tparam N  The number of elements.
   */
  template <typename T, std::size_t N>
  void load1DArray(std::array<T, N>& array, const json::json_pointer& jptr) const {
    unsigned int sizes = N;
    loadNDArray_impl(array.data(), &sizes, 1, jptr, DefaultValue<T>());
  }

  /**
   * Loads 1D data.
   *
   * @param array  The target for the data as a std::vector.
   * @param n      The length of the array.
   * @param jptr   The json pointer.
   *
   * @tparam T  The data type of the array.
   */
  template <typename T>
  void load1DArray(std::vector<T>& array, const unsigned int n, const json::json_pointer& jptr) const {
    array.resize(n);
    loadNDArray_impl(array.data(), &n, 1, jptr, DefaultValue<T>());
  }

  /**
   * Loads 1D data.
   *
   * @param array  The target for the data as a Vector3.
   * @param jptr   The json pointer.
   *
   * @tparam T  The data type of the array.
   */
  template <typename T>
  void load1DArray(math::Vector3<T>& array, const json::json_pointer& jptr) const {
    unsigned int sizes = 3;
    loadNDArray_impl(array.data(), &sizes, 1, jptr, DefaultValue<T>());
  }

  /**
   * Loads 1D data with default value.
   *
   * @param array         The target for the data as a std::array.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type of the array.
   * @tparam N  The number of elements.
   */
  template <typename T, std::size_t N>
  void load1DArray(std::array<T, N>& array, const json::json_pointer& jptr, const T defaultValue) const {
    unsigned int sizes = N;
    loadNDArray_impl(array.data(), &sizes, 1, jptr, DefaultValue<T>(defaultValue));
  }

  /**
   * Loads 1D data with default value.
   *
   * @param array         The target for the data as a std::vector.
   * @param n             Number of elements to read.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type of the array.
   */
  template <typename T>
  void load1DArray(std::vector<T>& array, const unsigned int n, const json::json_pointer& jptr, const T defaultValue) const {
    array.resize(n);
    loadNDArray_impl(array.data(), &n, 1, jptr, DefaultValue<T>(defaultValue));
  }

  /**
   * Loads 1D data with default value.
   *
   * @param array         The target for the data as a Vector3.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type of the array.
   */
  template <typename T>
  void load1DArray(math::Vector3<T>& array, const json::json_pointer& jptr, const T defaultValue) const {
    unsigned int sizes = 3;
    loadNDArray_impl(array.data(), &sizes, 1, jptr, DefaultValue<T>(defaultValue));
  }

  /**
   * Loads 2D data.
   *
   * @param array  The target for the data as a std::vector.
   * @param n      Array of number of elements per dimension.
   * @param jptr   The json pointer.
   *
   * @tparam T  The data type of the array.
   * @tparam U  The type of thse size array.
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load2DArray(std::vector<T>& array, const std::array<U, 2> n, const json::json_pointer& jptr) const {
    array.resize(n[0] * n[1]);
    loadNDArray_impl(array.data(), n.data(), 2, jptr, DefaultValue<T>());
  }

  /**
   * @overload
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load2DArray(std::vector<T>& array, const math::Vector2<U> n, const json::json_pointer& jptr) const {
    load2DArray(array, static_cast<std::array<U, 2>>(n), jptr);
  }

  /**
   * Loads 2D data with default value.
   *
   * @param array         The target for the data as a std::vector.
   * @param n             Array of number of elements per dimension.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type of the array.
   * @tparam U  The type of thse size array.
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load2DArray(std::vector<T>& array, const std::array<U, 2> n, const json::json_pointer& jptr, const T defaultValue) const {
    array.resize(n[0] * n[1]);
    loadNDArray_impl(array.data(), n.data(), 2, jptr, DefaultValue<T>(defaultValue));
  }

  /**
   * @overload
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load2DArray(std::vector<T>& array, const math::Vector2<U> n, const json::json_pointer& jptr, const T defaultValue) const {
    load2DArray(array, static_cast<std::array<U, 2>>(n), jptr, defaultValue);
  }

  /**
   * Loads 3D data.
   *
   * @param array  The target for the data as a std::vector.
   * @param n      Array of number of elements per dimension.
   * @param jptr   The json pointer.
   *
   * @tparam T  The data type of the array.
   * @tparam U  The type of thse size array.
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load3DArray(std::vector<T>& array, const std::array<U, 3> n, const json::json_pointer& jptr) const {
    array.resize(n[0] * n[1] * n[2]);
    loadNDArray_impl(array.data(), n.data(), 3, jptr, DefaultValue<T>());
  }

  /**
   * @overload
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load3DArray(std::vector<T>& array, const math::Vector3<U> n, const json::json_pointer& jptr) const {
    load3DArray(array, static_cast<std::array<U, 3>>(n), jptr);
  }

  /**
   * Loads 3D data with default value.
   *
   * @param array         The target for the data as a std::vector.
   * @param n             Array of number of elements per dimension.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   *
   * @tparam T  The data type of the array.
   * @tparam U  The type of thse size array.
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load3DArray(std::vector<T>& array, const std::array<U, 3> n, const json::json_pointer& jptr, const T defaultValue) const {
    array.resize(n[0] * n[1] * n[2]);
    loadNDArray_impl(array.data(), n.data(), 3, jptr, DefaultValue<T>(defaultValue));
  }

  /**
   * @overload
   */
  template <typename T, typename U, std::enable_if_t<std::is_integral<U>::value, int> = 0>
  void load3DArray(std::vector<T>& array, const math::Vector3<U> n, const json::json_pointer& jptr, const T defaultValue) const {
    load3DArray(array, static_cast<std::array<U, 3>>(n), jptr, defaultValue);
  }

  /// @}

  /// @name Element access (factory interface)
  /// @{

  /**
   * Creates an object from the factory.
   *
   * @param factory       The factory.
   * @param jptr          The json pointer to the config where the factory key is saved.
   * @param defaultValue  The default key in the factory.
   * @param args          The arguments needed for initialization the object.
   *
   * @tparam Factory  The factory type.
   * @tparam ARGs     The argument list.
   *
   * @return unique_ptr of the created object.
   */
  template <typename Factory, typename... ARGs>
  auto factoryCreate(const Factory& factory, const json::json_pointer& jptr, const std::string& defaultValue, ARGs&&... args) {
    const std::string& value = getValue<std::string>(jptr, defaultValue);
    try {
      return factory.create(value, std::forward<ARGs>(args)...);
    } catch (const std::invalid_argument&) {
      logger::get().error(
          "For the key {} the factory does not provide an element '{}'.\n"
          "Known elements in this factory are:\n{}",
          jptr, value, factory.getNamesFormatted());
      throw;
    }
  }

  /**
   * Registers functions in the given function registry.
   *
   * @param registry  The registry.
   * @param factory   The factory.
   * @param jptr      The json pointer to the config with an array of factory key.
   * @param args      The arguments.
   *
   * @tparam Function  Basis class of functions.
   * @tparam Factory   The factory type.
   * @tparam ARGs      The argument list.
   */
  template <typename Function, typename Factory, typename... ARGs>
  void registerFunctions(FunctionRegister<Function>& registry, const Factory& factory, const json::json_pointer& jptr, ARGs&&... args) {
    if (!isArray(jptr)) throw std::invalid_argument(fmt::format("Key {} expects an array.", jptr));

    std::set<unsigned int> categories;
    for (unsigned int i = 0; i < arraySize(jptr); i++) {
      auto* function = registry.registerFunction(factory.create(getValue<std::string>(jptr / i), std::forward<ARGs>(args)...));

      auto category = getCategory(*function);
      if (category > 0 && !categories.insert(category).second) {
        throw std::invalid_argument(fmt::format("A function of the same category is already registerd, '{}'' can not be added.", getValue<std::string>(jptr / i)));
      }

      logger::get().oneinfo("Register function '{}'", getValue<std::string>(jptr / i));
    }
  }

  /// @}

  /// @name Check functions
  /// @{

  /**
   * Checks if the json pointer points to a key in the config.
   *
   * @param jptr  The json pointer.
   *
   * @return True if the json pointer is found, False otherwise.
   */
  bool contains(const json::json_pointer& jptr) const { return jsonConfig_.contains(jptr); }

  /**
   * Determines whether the specified json pointer is an array.
   *
   * @param jptr  The json pointer.
   *
   * @return True if the specified json pointer is an array, False otherwise.
   */
  bool isArray(const json::json_pointer& jptr) const { return contains(jptr) && jsonConfig_[jptr].is_array(); }

  /**
   * The size of the array the json pointer points to.
   *
   * @param jptr  The json pointer.
   *
   * @return The size of the array, if the json pointer points to an array, 0 otherwise.
   */
  std::size_t arraySize(const json::json_pointer& jptr) const { return isArray(jptr) ? jsonConfig_[jptr].size() : 0; }

  /**
   * Determines whether the specified json pointer is a string.
   *
   * @param jptr  The json pointer.
   *
   * @return True if the specified json pointer is a string, False otherwise.
   */
  bool isString(const json::json_pointer& jptr) const { return contains(jptr) && jsonConfig_[jptr].is_string(); }

  /**
   * Determines whether the specified json pointer is an empty string.
   *
   * @param jptr  The json pointer.
   *
   * @return True if the specified json pointer is an empty string, False otherwise.
   */
  bool isEmptyString(const json::json_pointer& jptr) const { return isString(jptr) && jsonConfig_[jptr].get<std::string>().empty(); }

  /**
   * Determines whether the specified json pointer is an object.
   *
   * @param jptr  The json pointer.
   *
   * @return True if the specified json pointer is an object, False otherwise.
   */
  bool isObject(const json::json_pointer& jptr) const { return contains(jptr) && jsonConfig_[jptr].is_object(); }

  /// @}

  /// @name JSON manipulators
  /// @{

  void loadFile(const std::string& filename);

  /**
   * Loads the config file from the command line.
   */
  void loadFile() { loadFile(cmdline_.config); }

  /**
   * Closes the config and writes the config_save.
   */
  void close() const {
    if (configWrite_) configWrite_->writeConfig(cmdline_.outdir);
  }

  void loadString(const std::string& strJson);

  /**
   * Appends data to the config save.
   *
   * @param jptr  The json pointer.
   * @param data  The data.
   *
   * @tparam T  The data type.
   */
  template <typename T>
  void appendToSave(const json::json_pointer& jptr, const T& data) const {
    if (configWrite_) configWrite_->append(jptr, data);
  }

  /**
   * Gets the raw json object.
   *
   * This allows to change the json. Use it with care.
   *
   * @return The raw json.
   */
  json& getRawJson() { return jsonConfig_; }

  static void printHeader();
  void addInfo() const;

  /**
   * Prints the json config.
   */
  void print() {
    fmt::format("{}\n", jsonConfig_.dump(2));
  }

  /// @}

  /// @name Command line interface
  /// @{

  /**
   * Gets the command line.
   *
   * @return The command line.
   */
  const CommandLine& getCmdLine() const { return cmdline_; }

  /**
   * Parses the command line
   *
   * @param argc  The count of arguments.
   * @param argv  The arguments array.
   */
  void parseCmdLine(int argc, char* argv[]) { cmdline_.parse(argc, argv); }  // NOLINT

  /// @}

  /// @name Evaluator interface
  /// @{

  void linkEvaluator(SimData& simdata);
  evaluate::Evaluator& getEvaluator() const { return *(evaluator_.get()); }

  void readDefineFunctions() { addFunctions(); }

  /// @}

private:
  /**
   * This class describes a default values. If there is a default value it provides the value.
   *
   * @tparam T  The data type.
   */
  template <typename T>
  class DefaultValue {
  public:
    explicit DefaultValue() : hasDefault_{false} {}
    explicit DefaultValue(T value) : hasDefault_{true}, value_{value} {}

    /**
     * Boolean that denotes if there is a default value.
     *
     * @return Is there a default value?
     */
    bool has() const { return hasDefault_; }

    /**
     * Gets the default value.
     *
     * @return The default value.
     */
    T get() const { return value_; }

  private:
    bool hasDefault_{};  ///< Flag for has a default value.
    T value_{};          ///< The default value.
  };

  json& getObject(const json::json_pointer& jptr);

  /**
  * Parses a value.
  *
  * @param jptr          The json pointer.
  * @param defaultValue  The default value.
  *
  * @tparam T  The type.
  *
  * @return The value, if it is present, or the default when it should be used, else it tries to evaluate a string.
  *
  * @note For strings the evaluator is not used, because the string is returned directly.
  *
  * @note The evaluator reads `Geometry.blocksize`, `Geometry.blockcount`, `Settings.deltax`, and `Settings.deltat` to
  * build the variables `Nx`, `Ny`, `Nz`, `dt`, and `dx`. Predefine related variables in `DefineConstants` to use
  * expressions in in the keys above.
  */
  template <typename T>
  T parseValue(const json::json_pointer& jptr, DefaultValue<T> defaultValue) const;

  /**
   * Loads an n-dimensional linearized array from the config file.
   *
   * Makes a recursive call, starting with the last dimension, e.g., the first dimension is the fastest changing.
   * Dimension is counting down, the stride is sum up to build the linear index: ((z * size_y) + y) * size_x + x. Here,
   * the parenthesis are the strides per dimension.
   *
   * @param array         The array.
   * @param sizes         The array with the sizes.
   * @param dimension     The dimension.
   * @param jptr          The json pointer.
   * @param defaultValue  The default value.
   * @param stride        The stride.
   *
   * @tparam T  The data type.
   */
  template <typename T>
  void loadNDArray_impl(T* array, const unsigned int* sizes, unsigned int dimension, const json::json_pointer& jptr, const DefaultValue<T> defaultValue, int stride = 0) const {
    dimension--;
    for (unsigned int i = 0; i < sizes[dimension]; i++) {
      if (dimension == 0) {
        array[stride + i] = parseValue<T>(jptr / i, defaultValue);
      } else {
        loadNDArray_impl(array, sizes, dimension, jptr / i, defaultValue, (stride + i) * sizes[dimension - 1]);
      }
    }
  }

  /**
   * Checks config value for a given type.
   *
   * @param jptr  The json pointer.
   *
   * @tparam T  The type.
   *
   * @return True, if the type matches, false otherwise.
   */
  template <typename T>
  inline bool isType(const json::json_pointer& jptr) const;  //TODO internal usage only

  void appendIncludeFiles(const std::string& filename);
  void appendFromFile(const std::string& filename);
  void appendFromString(const std::string& strJson);
  void exchangeConfig();
  void addEvaluations();
  void addConstants();
  void addFunctions();
  void loadVariables();

  json jsonConfig_;      ///< The json object for the config values.
  CommandLine cmdline_;  ///< The command line object.

  std::unique_ptr<evaluate::Evaluator> evaluator_;  ///< The Pointer to the evaluator object.
  std::unique_ptr<ConfigWrite> configWrite_;        ///< The pointer to the configure write object.
};

}  // namespace config
}  // namespace nastja
