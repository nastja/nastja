/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <string>

namespace nastja {
namespace config {

class CommandLine {
public:
  void parse(int& argc, char* argv[]);

  std::string config{"config.json"};  ///< The configure file name.
  std::string configstring{""};       ///< The string that overwrites the values in the configure file.
  std::string outdir{"outdir"};       ///< The output directory.
  bool resume{false};                 ///< The resume flag.
  int resumeFrame{0};                 ///< The frame number for the dump from which the simulation will be resumed.
};

}  // namespace config
}  // namespace nastja
