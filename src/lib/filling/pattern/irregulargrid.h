/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/pattern/pattern.h"
#include "lib/math/boundingbox.h"
#include "lib/math/nanoflannadapter.h"
#include "lib/math/random.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C6.h"
#include <memory>
#include <random>
#include "nanoflann.hpp"
#include <unordered_map>
#include <cmath>


namespace nastja {
namespace filling {
namespace pattern {

using namespace nastja::config::literals;

template <typename T>
class PatternIrrGrid : public Pattern<T> {
public:
  explicit PatternIrrGrid(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize),
        blockBB_{offset - boundarySize, offset + blocksize + boundarySize},
        generator_{simdata.globalGenerator} {
    auto& config = simdata.getConfig();



    if (config.contains(jptr / "value")) {
      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.value, uint, 0}
      /// The value of the first cell. If this is below liquid, it will remain the same
      firstValue_ = config.getValue<T>(jptr / "value");
    }

    if (config.contains(jptr / "cylinderradius")) {
      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.cylinderradius, real_t, 1.0}
      /// The radius of the generated cylinders. -1 indicates a fully filled cube (infinity distance).
      auto diameter = config.getValue<real_t>(jptr / "cylinderradius");
      cylinderRadius_ = std::ceil(diameter);
    }

    if (config.contains(jptr / "cylinderlength")) {
      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.cylinderlength, real_t, 1.0}
      /// The radius of the generated cylinders. -1 indicates a fully filled cube (infinity distance).
      auto length = config.getValue<real_t>(jptr / "cylinderlength");
      cylinderLength_ = std::ceil(length);
    }



    auto jptrPeriodic = jptr / "periodic";
    if (config.contains(jptrPeriodic)) {
      if (!config.getValue<bool>(jptrPeriodic / "x", false)) isPeriodic_ |= stencil::skipExchange[0] | stencil::skipExchange[1];
      if (!config.getValue<bool>(jptrPeriodic / "y", false)) isPeriodic_ |= stencil::skipExchange[2] | stencil::skipExchange[3];
      if (!config.getValue<bool>(jptrPeriodic / "z", false)) isPeriodic_ |= stencil::skipExchange[4] | stencil::skipExchange[5];
      isPeriodic_ = ~isPeriodic_;
    }

    /// @keyignore
    auto blockcount = config.getVector3<long>("Geometry.blockcount"_jptr);
    domain_[1]      = blockcount * static_cast<math::Vector3<long>>(blocksize);  // set upper domain boundary to bounding box


    if (config.contains(jptr / "surroundingsDefault")) {
      /// @key{Filling.\template{field}[].surroundingsDefault, uint, 0}
      /// The value of the sourroundings area when a cell has a maximum distance from the center.
      surroundingsDefault_ = config.getValue<T>(jptr / "value");
    } else {
      if (simdata.hasData<DataCells>()) {
        surroundingsDefault_ = simdata.getData<DataCells>().liquid;
      }
    }



    if (config.contains(jptr / "pointlist")) {
      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.pointlist, Vector3<uint>[]}
      /// Array of points of the cylinder centers.
      readPointCloud(jptr / "pointlist");
    } else {
      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.box, Bbox<uint>}
      /// @depends{!Filling.\template{field}[].\template{pattern=irregulargrid}.pointlist}
      /// The bounding box for generating an irregular grid of cylinders.
      auto bbox = config.getBoundingBox<unsigned long>(jptr / "box");

      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.seed, uint, default_seed}
      /// @depends{Filling.\template{field}[].\template{pattern=irregulargrid}.box}
      /// The seed for generating a point cloud.
      auto seed = simdata.getSeed(jptr);

      auto& rawJson          = config.getRawJson();
      rawJson[jptr / "seed"] = seed;
      config.appendToSave(jptr / "seed", seed);
      generator_.seed(seed);

      /// @key{Filling.\template{field}[].\template{pattern=irregulargrid}.volumefraction, uint}
      /// Target volume fraction within bbox.


      auto numPoints{0};
      if(config.contains(jptr / "volumefraction")) {
        volumeFraction_ = config.getValue<real_t>(jptr / "volumefraction");

        auto cylinderVol = M_1_PI * cylinderRadius_ * cylinderRadius_ * cylinderLength_ ;
        auto bboxVol = bbox.upper()[0] - bbox.lower()[0];
        bboxVol *= (bbox.upper()[1] - bbox.lower()[1]);
        bboxVol *= (bbox.upper()[2] - bbox.lower()[2]);

        numPoints = std::ceil(volumeFraction_ * bboxVol / cylinderVol);
        
      }
      else if(config.contains(jptr / "count")) {
        numPoints = config.getValue<unsigned long>(jptr / "count");        
      }
      else{
        numPoints = config.getValue<unsigned long>(jptr / "count");        
      }

      generateCylinderCloud(bbox, seed, numPoints);
    }

//    kdtree_ = std::make_unique<kdtree_t>(3, centers_);
  }


  /**
   * Reads a point cloud from the json point list. PLACEHOLDER, CURRENTLY DOES NOTHING
   *
   * @param pointList  The pointList.
   */
  void readPointCloud(const json::json_pointer& pointList) {
    //This function currently does nothing
    auto const placeholder = pointList; //This line is only for suppressing a warning
    return;
  }

  /**
   * Generates an cylinder cloud.
   *
   * @param bbox       The bounding box.
   * @param seed       The seed.
   * @param numPoints  The number cells.
   */
  void generateCylinderCloud(const math::BoundingBox<unsigned long>& bbox, unsigned long seed, unsigned long numPoints) {
    generator_.seed(seed);

    // The upper boundary is included in UniformInt.
    math::UniformInt<long> uniformX(bbox.lower()[0], bbox.upper()[0] - 1);
    math::UniformInt<long> uniformY(bbox.lower()[1], bbox.upper()[1] - 1);
    math::UniformInt<long> uniformZ(bbox.lower()[2], bbox.upper()[2] - 1);


    math::UniformReal<real_t> uniformPhi_(0, 2 * M_PI);
    math::UniformReal<real_t> uniformTheta_(0, M_PI);

    for (unsigned long j = 0; j < numPoints; j++) {
      long x = uniformX(generator_);
      long y = uniformY(generator_);
      long z = uniformZ(generator_);
      real_t phi = uniformPhi_(generator_);
      real_t theta = uniformTheta_(generator_);


      math::Vector3<long> centerpoint{x, y, z};// The center of the cylinder

      math::Vector3<real_t> orientation{ //The orientation of the current cylinder
                                      std::sin(theta)*std::cos(phi),
                                      std::sin(theta)*std::sin(phi),
                                      std::cos(theta)
                                      };



      addPoint(centerpoint);
      
      math::Vector3<long> newPoint{x, y, z};
      // Add remaining points along cylinder orientation
      for(unsigned long i = 1; i < 0.5*cylinderLength_; ++i){
          for (int k = 0; k < 3; ++k) newPoint[k] = static_cast<long>(static_cast<real_t>(centerpoint[k]) + i*orientation[k]);
          if(newPoint[0] >= 0 and newPoint[1] >= 0 and newPoint[2] >= 0) addPoint(newPoint); //Make sure that all coordinates are greater than 0
          for (int k = 0; k < 3; ++k) newPoint[k] = static_cast<long>(static_cast<real_t>(centerpoint[k]) - i*orientation[k]);
          if(newPoint[0] >= 0 and newPoint[1] >= 0 and newPoint[2] >= 0) addPoint(newPoint); //Make sure that all coordinates are greater than 0
      }

      // Add outer part of cylinder
//      long N_angles{cylinderRadius_*cylinderRadius_};
//      radial_vector = cross

      
//,orientation

    }
    logger::get().trace("Local block has {} elements.", centers_.size());
  }


//Returns an integer which is unique for every set of positive intergers (a,b,c)
auto cantor_pair_three(const math::Vector3<long>& point) -> unsigned long{  
    unsigned long two_pair = .5*(point[0] + point[1])*(point[0] + point[1] + 1) + point[1];
    return .5*(two_pair + point[2])*(two_pair + point[2] + 1) + point[2];    
//End of function
}


  /**
   * Adds a point to the centers, when it is in the range.
   *
   * @param point  The point.
   */
  void addPoint(math::Vector3<long> point) {
  
  
    if (!blockBB_.isInsidePeriodic(domain_, point)) return;


    unsigned long insert_val = cantor_pair_three(point);
    if(centers_.find(insert_val) != centers_.end()) return;

    centers_.insert({insert_val, true });

    //orientations_.push_back(orientation)
  }

  T calc(unsigned long x, unsigned long y, unsigned long z) override {
    // Add offset for global coordinates
    math::Vector3<long> voxel{static_cast<long>(x), static_cast<long>(y), static_cast<long>(z)};
    voxel += offset;
    voxel -= boundarySize;

    if(centers_.find(cantor_pair_three(voxel)) == centers_.end()) return surroundingsDefault_;    


    return firstValue_;
  }



private:

  using Pattern<T>::blocksize;
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;
  using Pattern<T>::simdata;

  std::bitset<26> isPeriodic_{};
  math::BoundingBox<long> blockBB_;  ///< The bounding box of the current block.
  math::BoundingBox<long> domain_;   ///< The bounding box of the whole domain.
  std::mt19937_64& generator_;       ///< Reference to the globale generator_.

  long cylinderRadius_{1}; ///< The diameter of the generated cylinders along their two minor axes
  long cylinderLength_{1};///< The length of the generated cylinders along their major axis


  T firstValue_{0};             ///< The value of the first cylinder. If this is a solid, every cylinder will get this
  T surroundingsDefault_{0};    ///< The default value
  
  
  real_t volumeFraction_{0.1}; ///< The volume fraction to which the supplied volume is filled


  std::unordered_map<unsigned long,bool> centers_{};
  //std::vector<math::Vector3<long>> centers_{};  ///< Vector of the cylinder centers. Centers are shifted by one blocksize in each direction.
  //std::vector<math::Vector3<long>> orientations_{};  ///< Vector of the cylinder orientations.
  std::vector<T> indices_{};                    ///< Vector of the coresponding index.

};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
