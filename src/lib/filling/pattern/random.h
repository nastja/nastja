/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/filling/pattern/pattern.h"
#include "lib/math/random.h"
#include "nlohmann/json.hpp"
#include <random>

namespace nastja {
namespace filling {
namespace pattern {

using json = nlohmann::json;

template <typename T>
class PatternRandom : public Pattern<T> {
public:
  explicit PatternRandom(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize), generator_{simdata.generator} {
  }

  T calc(unsigned long /*x*/, unsigned long /*y*/, unsigned long /*z*/) override {
    return uniform_(generator_);
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  std::mt19937_64& generator_;
  math::UniformReal<T> uniform_{0, 1};
};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
