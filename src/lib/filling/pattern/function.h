/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/evaluate/evaluator.h"
#include "lib/filling/pattern/pattern.h"
#include "nlohmann/json.hpp"

namespace nastja {
namespace filling {
namespace pattern {

using json = nlohmann::json;

template <typename T>
class PatternFunction : public Pattern<T> {
public:
  explicit PatternFunction(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    auto& config = simdata.getConfig();

    /// @key{Filling.\template{field}[].\template{pattern=function}.function, string}
    /// The expression string.
    auto expressionString = config.getValue<std::string>(jptr / "function");

    symbol_table_.add_variable("x", coord_x_);
    symbol_table_.add_variable("y", coord_y_);
    symbol_table_.add_variable("z", coord_z_);

    expression_.register_symbol_table(symbol_table_);
    auto& evaluator = simdata.getConfig().getEvaluator();

    evaluator.compile(expressionString, expression_);
  }

  T calc(unsigned long x, unsigned long y, unsigned long z) override {
    math::Vector3<real_t> v{real_t(x), real_t(y), real_t(z)};
    v += offset;
    v -= boundarySize;

    coord_x_ = v.x();
    coord_y_ = v.y();
    coord_z_ = v.z();

    return expression_.value();
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  using expression_t   = exprtk::expression<real_t>;
  using symbol_table_t = exprtk::symbol_table<real_t>;

  real_t coord_x_{};
  real_t coord_y_{};
  real_t coord_z_{};

  symbol_table_t symbol_table_;
  expression_t expression_;  ///< The expression.
};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
