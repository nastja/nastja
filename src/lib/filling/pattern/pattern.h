/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/math/matrix3.h"
#include "lib/simdata/simdata.h"
#include "nlohmann/json.hpp"
#include <vector>

namespace nastja {
namespace filling {
namespace pattern {

using json = nlohmann::json;

/**
 * Class for pattern.
 *
 * ```
 * +----------+  +----------+
 * | +------+ |  | +------+ |
 * | |      | |  | |      | |
 * | |      | |  | |      | |
 * | |      | |  | |      | |
 * | +------+ |  | +------+ |
 * +----------+  +----------+
 *   |           | |      | |
 *   |           | |      | ofset + blocksize + boundarySize
 *   |           | |      offset + blocksize
 *   |           | offset
 *   |           offset - boundarySize
 *   global (0,0,0)
 *   in memory (1,1,1)
 * ```
 *
 * @tparam T  The data type of the field.
 */
template <typename T>
class Pattern {
public:
  explicit Pattern(const json::json_pointer& /*jptr*/, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : simdata{simdata},
        offset{offset},
        boundarySize{boundarySize},
        blocksize{blocksize} {}

  virtual ~Pattern()          = default;
  Pattern(const Pattern&)     = default;
  Pattern(Pattern&&) noexcept = default;
  Pattern& operator=(const Pattern&) = delete;
  Pattern& operator=(Pattern&&) = delete;

  virtual T calc(unsigned long x, unsigned long y, unsigned long z) = 0;

protected:
  SimData& simdata;                           ///< The reference to the SimData.
  math::Vector3<unsigned long> offset;        ///< The block offset to calculate global indices.
  math::Vector3<unsigned long> boundarySize;  ///< The boundarySize.
  math::Vector3<unsigned long> blocksize;     ///< The block size.
};

template <typename T>
class PatternConst : public Pattern<T> {
public:
  explicit PatternConst(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    /// @schemaFactoryDefault{Filling.\template{field}[].\template{pattern}, const}
    /// @key{Filling.\template{field}[].\template{pattern=const}.value, real_t}
    /// The constant value of the fill area.
    value_ = simdata.getConfig().getValue<T>(jptr / "value");
  }

  T calc(unsigned long /*x*/, unsigned long /*y*/, unsigned long /*z*/) override {
    return value_;
  }

private:
  T value_;  ///< The constant filling value;
};

template <typename T>
class PatternFCC : public Pattern<T> {
public:
  explicit PatternFCC(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    auto& config = simdata.getConfig();
    /// @key{Filling.\template{field}[].\template{pattern=fcc}.psi0, real_t}
    /// The psi_0 mean value of the filling.
    psi0_ = config.getValue<real_t>(jptr / "psi0");

    /// @key{Filling.\template{field}[].\template{pattern=fcc}.A, real_t}
    /// The amplitude.
    A_ = config.getValue<real_t>(jptr / "A");

    /// @key{Filling.\template{field}[].\template{pattern=fcc}.q, real_t}
    /// The reciprocale lattice constant.
    q_ = config.getValue<real_t>(jptr / "q");
    h_ = simdata.deltax;

    /// @key{Filling.\template{field}[].\template{pattern=fcc}.patternrotation, Vector3<real_t>}
    /// The Euler angles for the rotation.
    rotation_ = config.getRotationMatrix(jptr / "patternrotation");
  }

  T calc(unsigned long x, unsigned long y, unsigned long z) override {
    math::Vector3<real_t> v{real_t(x), real_t(y), real_t(z)};
    v += offset;
    v -= boundarySize;
    v = rotation_.multiplyT(v);
    return psi0_ + A_ * (std::cos(q_ * v[0] * h_) * std::cos(q_ * v[1] * h_) * std::cos(q_ * v[2] * h_));
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  real_t psi0_;                     ///< The mean value.
  real_t A_;                        ///< The amplitude.
  real_t q_;                        ///< The reciprocal lattice constant.
  real_t h_;                        ///< The lattice resolution delta x.
  math::Matrix3<real_t> rotation_;  ///< The rotation matrix.
};

template <typename T>
class PatternBCC : public Pattern<T> {
public:
  explicit PatternBCC(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    auto& config = simdata.getConfig();
    /// @key{Filling.\template{field}[].\template{pattern=bcc}.psi0, real_t}
    /// The psi_0 mean value of the filling.
    psi0_ = config.getValue<real_t>(jptr / "psi0");

    /// @key{Filling.\template{field}[].\template{pattern=bcc}.A, real_t}
    /// The amplitude.
    A_ = config.getValue<real_t>(jptr / "A");

    /// @key{Filling.\template{field}[].\template{pattern=bcc}.q, real_t}
    /// The reciprocale lattice constant.
    q_ = config.getValue<real_t>(jptr / "q");
    h_ = simdata.deltax;

    /// @key{Filling.\template{field}[].\template{pattern=bcc}.patternrotation, Vector3<real_t>}
    /// The Euler angles for the rotation.
    rotation_ = config.getRotationMatrix(jptr / "patternrotation");
  }

  T calc(unsigned long x, unsigned long y, unsigned long z) override {
    math::Vector3<real_t> v{real_t(x), real_t(y), real_t(z)};
    v += offset;
    v -= boundarySize;
    v = rotation_.multiplyT(v);
    return psi0_ + A_ * (std::cos(q_ * v[0] * h_) * std::cos(q_ * v[1] * h_) + std::cos(q_ * v[0] * h_) * std::cos(q_ * v[2] * h_) + std::cos(q_ * v[1] * h_) * std::cos(q_ * v[2] * h_));
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  real_t psi0_;                     ///< The mean value.
  real_t A_;                        ///< The amplitude.
  real_t q_;                        ///< The reciprocal lattice constant.
  real_t h_;                        ///< The lattice resolution delta x.
  math::Matrix3<real_t> rotation_;  ///< The rotation matrix.
};

template <typename T>
class PatternHEX : public Pattern<T> {
public:
  explicit PatternHEX(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    auto& config = simdata.getConfig();
    /// @key{Filling.\template{field}[].\template{pattern=hex}.psi0, real_t}
    /// The psi_0 mean value of the filling.
    psi0_ = config.getValue<real_t>(jptr / "psi0");

    /// @key{Filling.\template{field}[].\template{pattern=hex}.A, real_t}
    /// The amplitude.
    A_ = config.getValue<real_t>(jptr / "A");

    /// @key{Filling.\template{field}[].\template{pattern=hex}.q, real_t}
    /// The reciprocale lattice constant.
    q_ = config.getValue<real_t>(jptr / "q");
    h_ = simdata.deltax;
  }

  T calc(unsigned long x, unsigned long y, unsigned long /*z*/) override {
    math::Vector3<real_t> v{real_t(x), real_t(y), 0.0};
    v += offset;
    v -= boundarySize;
    return psi0_ + A_ * (std::cos(q_ * v.x() * h_) * std::cos(q_ * v.y() * h_ / std::sqrt(3.0)) - 0.5 * std::cos(2.0 * q_ * v.y() / std::sqrt(3.0)));
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  real_t psi0_;  ///< The mean value.
  real_t A_;     ///< The amplitude.
  real_t q_;     ///< The reciprocal lattice constant.
  real_t h_;     ///< The lattice resolution delta x.
};

template <typename T>
class PatternSQUARE : public Pattern<T> {
public:
  explicit PatternSQUARE(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize) {
    auto& config = simdata.getConfig();
    /// @key{Filling.\template{field}[].\template{pattern=square}.psi0, real_t}
    /// The psi_0 mean value of the filling.
    psi0_ = config.getValue<real_t>(jptr / "psi0");

    /// @key{Filling.\template{field}[].\template{pattern=square}.A, real_t}
    /// The amplitude.
    A_ = config.getValue<real_t>(jptr / "A");

    /// @key{Filling.\template{field}[].\template{pattern=square}.q, real_t}
    /// The reciprocale lattice constant.
    q_ = config.getValue<real_t>(jptr / "q");
    h_ = simdata.deltax;
  }

  T calc(unsigned long x, unsigned long y, unsigned long /*z*/) override {
    math::Vector3<real_t> v{real_t(x), real_t(y), 0.0};
    v += offset;
    v -= boundarySize;
    return psi0_ + A_ * (std::cos(q_ * v.x() * h_) + std::cos(q_ * v.y() * h_));
  }

private:
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;

  real_t psi0_;  ///< The mean value.
  real_t A_;     ///< The amplitude.
  real_t q_;     ///< The reciprocal lattice constant.
  real_t h_;     ///< The lattice resolution delta x.
};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
