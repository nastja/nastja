/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/filling/pattern/function.h"
#include "lib/filling/pattern/pattern.h"
#include "lib/filling/pattern/random.h"
#include "lib/filling/pattern/voronoi.h"
#include "lib/filling/pattern/irregulargrid.h"
#include "lib/simdata/simdata.h"
#include "nlohmann/json.hpp"

namespace nastja {
namespace filling {
namespace pattern {

template <typename T>
class FactoryPattern : public Factory<Pattern<T>, const json::json_pointer&, SimData&, const math::Vector3<unsigned long>&, const math::Vector3<unsigned long>&, const math::Vector3<unsigned long>&> {
public:
  explicit FactoryPattern() {
    base_t::template registerType<PatternConst<T>>("const");
    base_t::template registerType<PatternFunction<T>>("function");
    base_t::template registerType<PatternRandom<T>>("random");
    base_t::template registerType<PatternVoronoi<T>>("voronoi");
    base_t::template registerType<PatternIrrGrid<T>>("irregulargrid");
#ifdef USE_PFC
    // The following pattern makes only sense for floating point types.
    base_t::template registerType<PatternBCC<T>>("bcc");
    base_t::template registerType<PatternFCC<T>>("fcc");
    base_t::template registerType<PatternHEX<T>>("hex");
    base_t::template registerType<PatternSQUARE<T>>("square");
#endif
  }

private:
  using base_t = Factory<Pattern<T>, const json::json_pointer&, SimData&, const math::Vector3<unsigned long>&, const math::Vector3<unsigned long>&, const math::Vector3<unsigned long>&>;
};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
