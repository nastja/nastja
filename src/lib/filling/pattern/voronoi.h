/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/pattern/pattern.h"
#include "lib/math/boundingbox.h"
#include "lib/math/nanoflannadapter.h"
#include "lib/math/random.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C6.h"
#include <memory>
#include <random>
#include "nanoflann.hpp"

namespace nastja {
namespace filling {
namespace pattern {

using namespace nastja::config::literals;

template <typename T>
class PatternVoronoi : public Pattern<T> {
public:
  explicit PatternVoronoi(const json::json_pointer& jptr, SimData& simdata, const math::Vector3<unsigned long>& offset, const math::Vector3<unsigned long>& boundarySize, const math::Vector3<unsigned long>& blocksize)
      : Pattern<T>(jptr, simdata, offset, boundarySize, blocksize),
        blockBB_{offset - boundarySize, offset + blocksize + boundarySize},
        generator_{simdata.globalGenerator} {
    auto& config = simdata.getConfig();

    if (config.contains(jptr / "value")) {
      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.value, uint, 0}
      /// The value of the first Voronoi cell.
      firstValue_ = config.getValue<T>(jptr / "value");
    }

    if (config.contains(jptr / "cellradius")) {
      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.cellradius, real_t, -1.0}
      /// The maximal distance to the center of a Voronoi cell. -1 indicates a full Voronoi cell (inifity distance).
      auto distance = config.getValue<real_t>(jptr / "cellradius");
      maximumDistance_    = std::ceil(distance);
      maximumDistanceSqr_ = maximumDistance_ < 0 ? 0 : std::round(distance * distance);
    }

    if (maximumDistance_ >= 0) {
      if (config.contains(jptr / "surroundingsDefault")) {
        /// @key{Filling.\template{field}[].surroundingsDefault, uint, 0}
        /// The value of the sourroundings area when a cell has a maximum distance from the center.
        surroundingsDefault_ = config.getValue<T>(jptr / "value");
      } else {
        if (simdata.hasData<DataCells>()) {
          surroundingsDefault_ = simdata.getData<DataCells>().liquid;
        }
      }
    }
    blockBB_.extend(std::max(0l, maximumDistance_));

    auto jptrPeriodic = jptr / "periodic";
    if (config.contains(jptrPeriodic)) {
      if (!config.getValue<bool>(jptrPeriodic / "x", false)) isPeriodic_ |= stencil::skipExchange[0] | stencil::skipExchange[1];
      if (!config.getValue<bool>(jptrPeriodic / "y", false)) isPeriodic_ |= stencil::skipExchange[2] | stencil::skipExchange[3];
      if (!config.getValue<bool>(jptrPeriodic / "z", false)) isPeriodic_ |= stencil::skipExchange[4] | stencil::skipExchange[5];
      isPeriodic_ = ~isPeriodic_;
    }

    /// @keyignore
    auto blockcount = config.getVector3<long>("Geometry.blockcount"_jptr);
    domain_[1]      = blockcount * static_cast<math::Vector3<long>>(blocksize);  // set upper domain boundary to bounding box

    if (config.contains(jptr / "pointlist")) {
      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.pointlist, Vector3<uint>[]}
      /// Array of points of the Voronoi cell centers.
      readPointCloud(jptr / "pointlist");
    } else {
      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.box, Bbox<uint>}
      /// @depends{!Filling.\template{field}[].\template{pattern=voronoi}.pointlist}
      /// The bounding box for generating a point cloud of Voronoi cell centers.
      auto bbox = config.getBoundingBox<unsigned long>(jptr / "box");

      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.seed, uint, default_seed}
      /// @depends{Filling.\template{field}[].\template{pattern=voronoi}.box}
      /// The seed for generating a point cloud.
      auto seed = simdata.getSeed(jptr);

      auto& rawJson          = config.getRawJson();
      rawJson[jptr / "seed"] = seed;
      config.appendToSave(jptr / "seed", seed);
      generator_.seed(seed);

      /// @key{Filling.\template{field}[].\template{pattern=voronoi}.count, uint}
      /// @depends{!Filling.\template{field}[].\template{pattern=voronoi}.pointlist && Filling.\template{field}[].\template{pattern=voronoi}.box}
      /// Number of generating points.
      auto numPoints = config.getValue<unsigned long>(jptr / "count");

      generatePointCloud(bbox, seed, numPoints);
    }

    kdtree_ = std::make_unique<kdtree_t>(3, centers_);
  }

  /**
   * Reads a point cloud from the json point list.
   *
   * @param pointList  The pointList.
   */
  void readPointCloud(const json::json_pointer& pointList) {
    auto& config = simdata.getConfig();

    for (unsigned int j = 0; j < config.arraySize(pointList); j++) {
      auto point = config.template getVector3<long>(pointList / j);
      addPoint(j, point);
    }
    logger::get().trace("Local block has {} elements.", centers_.size());
  }

  /**
   * Generates a point cloud.
   *
   * @param bbox       The bounding box.
   * @param seed       The seed.
   * @param numPoints  The number cells.
   */
  void generatePointCloud(const math::BoundingBox<unsigned long>& bbox, unsigned long seed, unsigned long numPoints) {
    generator_.seed(seed);

    // The upper boundary is included in UniformInt.
    math::UniformInt<long> uniformX(bbox.lower()[0], bbox.upper()[0] - 1);
    math::UniformInt<long> uniformY(bbox.lower()[1], bbox.upper()[1] - 1);
    math::UniformInt<long> uniformZ(bbox.lower()[2], bbox.upper()[2] - 1);

    for (unsigned long j = 0; j < numPoints; j++) {
      long x = uniformX(generator_);
      long y = uniformY(generator_);
      long z = uniformZ(generator_);
      math::Vector3<long> point{x, y, z};
      addPoint(j, point);
    }
    logger::get().trace("Local block has {} elements.", centers_.size());
  }

  /**
   * Adds a point to the centers, when it is in the range.
   *
   * @param j      The index.
   * @param point  The point.
   */
  void addPoint(unsigned long j, math::Vector3<long> point) {
    if (maximumDistance_ >= 0 && !blockBB_.isInsidePeriodic(domain_, point)) return;

    centers_.push_back(point);
    indices_.push_back(j);
  }

  T calc(unsigned long x, unsigned long y, unsigned long z) override {
    // Add offset for global coordinates
    math::Vector3<long> voxel{static_cast<long>(x), static_cast<long>(y), static_cast<long>(z)};
    voxel += offset;
    voxel -= boundarySize;

    auto nearestNeighbor = periodicNearestNeighbor(voxel);

    if ((maximumDistance_ >= 0) && (nearestNeighbor.second > maximumDistanceSqr_)) return surroundingsDefault_;

    return indices_[nearestNeighbor.first] + firstValue_;
  }

  /**
   * Searches for the nearest neighbor of point.
   *
   * @param point               The point.
   * @param periodic            The bitset of the 26 possible periodic representations, 1 for check, 0 ignore.
   * @param domain              The whole domain.
   * @param maximumDistanceSqr  The squared of the maximum distance to check, 0 for infinity.
   *
   * @return Pair of index of the nearest neighbor and the squared distance.
   */
  std::pair<std::size_t, long> periodicNearestNeighbor(math::Vector3<long> point, std::bitset<26> periodic, math::BoundingBox<long> domain, long maximumDistanceSqr = 0) {
    std::size_t index = 0;
    long distanceSqr  = 0;

    const std::size_t numNearestNeighbors = 1;
    kdtree_->findNeighbors(point.data(), numNearestNeighbors, &index, &distanceSqr);

    auto distanceToPeriodicBorder = math::borderDistancesSqr(domain, point);
    for (auto i : stencil::D3C26()) {
      if (!periodic[i] ||
          distanceToPeriodicBorder[i] > distanceSqr ||
          (maximumDistanceSqr >= 0 && distanceToPeriodicBorder[i] > maximumDistanceSqr)) continue;

      long tmpDist         = 0;
      std::size_t tmpIndex = 0;
      // shift point in opposite direction
      auto currentPoint = point - stencil::D3C26::dir[i] * domain.upper();
      kdtree_->findNeighbors(currentPoint.data(), numNearestNeighbors, &tmpIndex, &tmpDist);

      if (tmpDist < distanceSqr) {
        distanceSqr = tmpDist;
        index       = tmpIndex;
      }
    }

    return std::make_pair(index, distanceSqr);
  }

  std::pair<std::size_t, long> periodicNearestNeighbor(math::Vector3<long> point) {
    return periodicNearestNeighbor(point, isPeriodic_, domain_, maximumDistanceSqr_);
  }

private:
  /// Define the type of the kd-tree adapted to the used container
  using kdtree_t = math::KDTreeAdaptor<std::vector<math::Vector3<long>>, long, 3, nanoflann::metric_L2_Simple>;

  using Pattern<T>::blocksize;
  using Pattern<T>::boundarySize;
  using Pattern<T>::offset;
  using Pattern<T>::simdata;

  std::bitset<26> isPeriodic_{};
  math::BoundingBox<long> blockBB_;  ///< The bounding box of the current block.
  math::BoundingBox<long> domain_;   ///< The bounding box of the whole domain.
  std::mt19937_64& generator_;       ///< Reference to the globale generator_.

  long maximumDistance_{-1};     ///< The maximum distance from a point center. Negative values indicate an infinity size, this is equal to Voronoi filling.
  long maximumDistanceSqr_{0};  ///< The square of the maximum distance.
  T firstValue_{0};             ///< The value of the first Voronoi cell.
  T surroundingsDefault_{0};    ///< The default value. The area outside of `maximumDistance` is filled with this value.

  std::vector<math::Vector3<long>> centers_{};  ///< Vector of the point centers. Centers are shifted by one blocksize in each direction.
  std::vector<T> indices_{};                    ///< Vector of the coresponding index.
  std::unique_ptr<kdtree_t> kdtree_{};
};

}  // namespace pattern
}  // namespace filling
}  // namespace nastja
