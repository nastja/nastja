/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/vessel/vesseltree.h"

namespace nastja {
namespace filling {
namespace vessel {

class EdgeIterator {
public:
  EdgeIterator(const VesselTree& tree, long src, long dst);

  struct Edge {
    Edge(long src, long dst) : src{src}, dst{dst} {}

    long src;
    long dst;
    unsigned long weight{0};
  };

  EdgeIterator operator++();

  bool operator!=(const EdgeIterator& other) const;

  const Edge operator*() const;

private:
  const VesselTree& tree_;
  std::vector<long> unvisitedNodes_;

  Edge edge_;
};

class EdgeContainer {
public:
  explicit EdgeContainer(const VesselTree& tree) : tree_{tree} {}

  EdgeIterator begin() {
    return EdgeIterator{tree_, 0, 1};
  }

  EdgeIterator end() {
    return EdgeIterator{tree_, -1, -1};
  }

private:
  const VesselTree& tree_;
};

}  // namespace vessel
}  // namespace filling
}  // namespace nastja
