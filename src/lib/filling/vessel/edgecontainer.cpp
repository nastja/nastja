/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "edgecontainer.h"
#include <stdexcept>

namespace nastja {
namespace filling {
namespace vessel {

EdgeIterator::EdgeIterator(const VesselTree& tree, long src, long dst) : tree_{tree}, edge_{src, dst} {
  if (src == -1) return;

  edge_.weight = tree[edge_.src].leaveCount;

  if (tree[src].left != dst && tree[src].right != dst) {
    throw std::runtime_error(fmt::format("{}->{} is not an edge.", src, dst));
  }

  auto children = tree[src].getChildren();
  if (children.first != -1 && !tree[children.first].isLeave()) unvisitedNodes_.push_back(children.first);
  if (children.second != -1 && !tree[children.second].isLeave()) unvisitedNodes_.push_back(children.second);
}

EdgeIterator EdgeIterator::operator++() {
  auto rightChild = tree_[edge_.src].right;

  if (rightChild != -1 && edge_.dst != rightChild) {
    edge_.dst    = rightChild;
    edge_.weight = tree_[edge_.src].leaveCount;
  } else {
    if (unvisitedNodes_.empty()) {
      // end iterator
      edge_.src = -1;
      edge_.dst = -1;
    } else {
      edge_.src = unvisitedNodes_[0];  // is assumed to be not a leave.
      unvisitedNodes_.erase(unvisitedNodes_.begin());
      edge_.dst    = tree_[edge_.src].left;  // left exist, right maybe when split...
      edge_.weight = tree_[edge_.src].leaveCount;

      auto children = tree_[edge_.src].getChildren();
      if (children.first != -1 && !tree_[children.first].isLeave()) unvisitedNodes_.push_back(children.first);
      if (children.second != -1 && !tree_[children.second].isLeave()) unvisitedNodes_.push_back(children.second);
    }
  }

  return *this;
}

bool EdgeIterator::operator!=(const EdgeIterator& other) const {
  return edge_.src != other.edge_.src || edge_.dst != other.edge_.dst;
}

const EdgeIterator::Edge EdgeIterator::operator*() const {
  return edge_;
}

}  // namespace vessel
}  // namespace filling
}  // namespace nastja
