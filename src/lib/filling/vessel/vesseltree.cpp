/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "vesseltree.h"
#include "lib/filling/vessel/edgecontainer.h"

namespace nastja {
namespace filling {
namespace vessel {

VesselTree::VesselTree(const json::json_pointer& jptr, config::Config& config) {
  /// @key{Filling.\template{field}[].\template{shape=vessel}.box, Bbox<int>}
  /// The bounding box.
  boundingBox_ = config.getBoundingBox<long>(jptr / "box");

  /// @key{Filling.\template{field}[].\template{shape=vessel}.root, Vector3<uint>}
  /// The vessel root.
  root_ = config.getVector3<long>(jptr / "root");

  auto listPtr = jptr / "targetlist";
  for (unsigned int i = 0; i < config.arraySize(listPtr); i++) {
    /// @key{Filling.\template{field}[].\template{shape=vessel}.targetlist[], Vector3<uint>}
    /// The vessel targets.
    auto target = config.template getVector3<long>(listPtr / i);
    targetList_.emplace_back(target);
  }

  for (unsigned int i = 0; i < targetList_.size(); ++i) {
    /// @key{Filling.\template{field}[].\template{shape=vessel}.targetlistweight[], uint}
    /// The target weights.
    targetListWeight_.push_back(config.getVector<unsigned int>(jptr / "targetlistweight")[i]);

    /// @key{Filling.\template{field}[].\template{shape=vessel}.splitratecapillaries, Vector3<real_t>}
    /// Probability to split in two.
    splitRateCapillaries_.push_back(config.getVector<double>(jptr / "splitratecapillaries")[i]);

    /// @key{Filling.\template{field}[].\template{shape=vessel}.diffusion[], uint}
    /// Diffusion distance of nutriments.
    diffusionDistance_.push_back(config.getVector<unsigned int>(jptr / "diffusion")[i]);
  }

  /// @key{Filling.\template{field}[].\template{shape=vessel}.numpartcapillaries, uint}
  /// The number of part on capillaries.
  numPartCapillaries_ = config.getValue<unsigned int>(jptr / "numpartcapillaries");

  /// @key{Filling.\template{field}[].\template{shape=vessel}.lengthvessel, Vector3<uint>}
  /// The vessel edge maximum, minimum length and dcreasing step of the vessel elements.
  auto lengths = config.getVector3<unsigned int>(jptr / "lengthvessel");
  lengthMax_   = lengths[0];
  lengthMin_   = lengths[1];
  lengthStep_  = lengths[2];

  /// @key{Filling.\template{field}[].\template{shape=vessel}.splitvessel, real_t}
  /// Probability to split in two.
  splitRateVessel_ = config.getValue<double>(jptr / "splitvessel");

  unsigned long seed = std::mt19937_64::default_seed;
  if (config.contains(jptr / "seed")) {
    /// @key{Filling.\template{field}[].\template{shape=vessel}.seed, uint, default_seed}
    /// The seed for generating vessel nodes.
    seed = config.getValue<unsigned long>(jptr / "seed");
  }
  generator_.seed(seed);

  buildTree();
}

void VesselTree::buildTree() {
  buildVessels();
  buildCapillaries();
  updateLeaveCount();
}

void VesselTree::buildVessels() {
  math::Vector3<long> target;
  std::vector<std::vector<unsigned int>> vesselTargetList;  //list of nearest vessel for each target.

  Node rootNode;
  rootNode.coord = root_;
  tree_.push_back(rootNode);

  std::vector<unsigned int> list;
  for (unsigned long j = 0; j < targetList_.size(); ++j) {
    list.push_back(j);
  }
  target = selectAverageTarget(list);  //calculate the coordinate of the target.

  setupRandom(rootNode.coord, target, VesselType::vessel);  // adapt the random distribution to the target.
  Node newNode = buildNode(0, lengthMax_);                  // construct the next node.
  tree_.push_back(newNode);
  tree_[0].left = 1;

  std::vector<unsigned long> skipNode;
  // build other vessels.
  for (unsigned int length = lengthMax_ - lengthStep_; length > lengthMin_; length -= lengthStep_) {
    if (length <= 0) abort();

    vesselTargetList = selectNearestNode();  // select the nearest vessel for each node.

    unsigned int numNodes = tree_.size();
    for (unsigned int i = 0; i < numNodes; i++) {                                                             // loop on every nodes.
      if (!tree_[i].isLeave() || std::find(skipNode.begin(), skipNode.end(), i) != skipNode.end()) continue;  // if the node is not a leave continue.
      if (vesselTargetList[i].empty()) {
        skipNode.push_back(i);
        continue;
      }

      target = selectAverageTarget(vesselTargetList[i]);  // calculate the average target if there is more than 1 target.
      setupRandom(tree_[i].coord, target, VesselType::vessel);
      newNode = buildNode(i, length);
      tree_.push_back(newNode);
      tree_[i].left = tree_.size() - 1;

      unsigned int targetID = nearestTarget(tree_[tree_.size() - 1].coord);
      if (distToTarget(tree_[tree_.size() - 1].coord, targetList_[targetID]) < (diffusionDistance_[targetID] * diffusionDistance_[targetID])) {
        skipNode.push_back(tree_.size() - 1);
      }

      if (randomRate(splitRateVessel_) && vesselTargetList[i].size() > 1) {
        setupRandom(tree_[i].coord, targetList_[vesselTargetList[i][0]], VesselType::vessel);
        newNode = buildNode(i, length);
        tree_.push_back(newNode);
        tree_[i].right = tree_.size() - 1;
        targetID       = nearestTarget(tree_[tree_.size() - 1].coord);
        if (distToTarget(tree_[tree_.size() - 1].coord, targetList_[targetID]) < (diffusionDistance_[targetID] * diffusionDistance_[targetID])) {
          skipNode.push_back(tree_.size() - 1);
        }
      }
    }
  }
}

void VesselTree::buildCapillaries() {
  unsigned int itMax = 100;
  std::vector<unsigned int> splitCounts;
  for (unsigned int i = 0; i < targetList_.size(); ++i) {
    splitCounts.push_back(1);
  }
  auto targetDirection = buildTargetDirection();
  for (unsigned int part = 0; part < numPartCapillaries_; ++part) {
    auto treeSize = tree_.size();
    for (unsigned int i = 0; i < treeSize; ++i) {
      if (!tree_[i].isLeave()) continue;

      auto targetID   = nearestTarget(tree_[i].coord);
      auto splitCount = splitCounts[targetID];
      real_t length   = 1.5 * static_cast<real_t>(diffusionDistance_[targetID]) / static_cast<real_t>(numPartCapillaries_);

      math::Vector3<real_t> target;
      if (splitCount == 1) {
        // if it is the first split, get the main direction.
        target = targetDirection[targetID];
      } else {
        // otherwise, Direction is the direction of the previous vessel.
        target = static_cast<math::Vector3<real_t>>(tree_[i].coord - tree_[tree_[i].parent].coord);
        target = target / target.length();
        target += targetDirection[targetID] / 3;
      }
      setupRandom(tree_[i].coord, static_cast<math::Vector3<long>>(tree_[i].coord + length * target), VesselType::capillaries);

      auto direction = target.getNormalized();

      // create n points
      int n = (!randomRate(splitRateCapillaries_[targetID]) || splitCount >= targetListWeight_[targetID]) ? 1 : 2;
      for (int j = 0; j < n; ++j) {
        if (j == 1) splitCounts[targetID]++;
        direction.normalize();
        auto coord  = rndCoordinates();
        auto vector = static_cast<math::Vector3<real_t>>(coord - tree_[i].coord);

        // Build a Point
        auto it = 1u;
        while ((direction.dotProduct(vector) < 0.8 * vector.length()) && (it < itMax)) {
          auto coord = rndCoordinates();
          vector     = static_cast<math::Vector3<real_t>>(coord - tree_[i].coord);
          it++;
        }
        if (it == itMax) continue;
        if (vector.length() == 0) continue;

        vector = vector * length / vector.length();
        for (int k = 0; k < 3; ++k) {
          coord[k] = tree_[i].coord[k] + round(vector[k]);
        }

        Node newNode;
        newNode.parent = i;
        newNode.coord  = coord;
        tree_.push_back(newNode);

        if (tree_[i].left == -1) {
          tree_[i].left = tree_.size() - 1;
        } else {
          tree_[i].right = tree_.size() - 1;
        }

        // adapt direction for the 2nd point.
        direction *= direction.dotProduct(vector);
        direction = 2 * direction - vector;
      }
    }
  }
}

void VesselTree::updateLeaveCount() {
  for (auto node : tree_) {
    if (!node.isLeave()) continue;

    long currentID = node.parent;
    while (currentID != -1) {
      tree_[currentID].leaveCount++;
      currentID = tree_[currentID].parent;
    }
  }
}

/**
 * Return the average coordinate of every target selected by the leave.
 *
 * Every leaves have a list in vesselTargetList containing the ID of the all target point which selected this leave as
 * the closest one. This function build the average of their coordinates.
 *
 * @param vesselTargetList  An array containing every point target for every node.
 *
 * @return Average target point.
 */
math::Vector3<long> VesselTree::selectAverageTarget(const std::vector<unsigned int>& vesselTargetList) {
  math::Vector3<long> target;
  long weight = 0;

  for (unsigned int i : vesselTargetList) {
    target += targetListWeight_[i] * targetList_[i];  // Sum of every x, y and z coordinate for every target.
    weight += targetListWeight_[i];
  }

  target[0] = round(target[0] / weight);
  target[1] = round(target[1] / weight);  // divide to the size to get the average coordinate.
  target[2] = round(target[2] / weight);

  return target;
}

/**
 * Setup random distributions.
 *
 * Setup the random distribution around the point targeted to simulate a direction of propagation of the blood vessel.
 * This distribution is adapted for each leave It depends on the distance between the node and the target.
 *
 * @param leave   The coordinate of the leave.
 * @param target  The coordinate of the point targeted by the leave.
 * @param type    The type.
 */
void VesselTree::setupRandom(math::Vector3<long> leave, math::Vector3<long> target, VesselType type) {
  int dist;
  if (type == VesselType::vessel) {
    dist = (leave[0] - target[0]) * (leave[0] - target[0]);
    dist += (leave[1] - target[1]) * (leave[1] - target[1]);  //calculate the distance between the target and the node
    dist += (leave[2] - target[2]) * (leave[1] - target[2]);
    dist = round(0.25 * std::sqrt(dist));  // calculate the size of a cube in the sphere.
  } else {
    dist = round(0.5 * std::sqrt((target - leave).dotProduct(target - leave)));
  }

  // setup the random minimum and maximum in this cube
  for (int i = 0; i < 3; i++) {
    uniform_[i].setMin(std::max(boundingBox_.lower()[i], target[i] - dist));
    uniform_[i].setMax(std::min(boundingBox_.upper()[i], target[i] + dist));
  }
}

/**
 * Build a new node.
 *
 * Build a new node with a distance of length between it and a (root) node.
 *
 * @param nodeID  The ID of root node.
 * @param length  The distance between the new node and the root one.
 *
 * @return The node.
 */
VesselTree::Node VesselTree::buildNode(unsigned int nodeID, unsigned int length) {
  Node newNode;
  newNode.parent = nodeID;
  newNode.coord  = rndCoordinates();

  auto vect = (newNode.coord - tree_[nodeID].coord).getNormalized() * length;  // normalise to length

  for (unsigned int i = 0; i < 3; i++) {
    newNode.coord[i] = tree_[nodeID].coord[i] + round(vect[i]);  // build the node
  }

  return newNode;
}

/**
 * Select the nearest leave for every target.
 *
 * For every targeted point, calculate the nearest leave and place it in the corresponding list.
 *
 * @return A list which contains a list of target for each node.
 */
std::vector<std::vector<unsigned int>> VesselTree::selectNearestNode() {
  std::vector<std::vector<unsigned int>> vesselTargetList;
  vesselTargetList.resize(tree_.size());
  for (unsigned long i = 0; i < targetList_.size(); i++) {
    long minDist = std::numeric_limits<long>::max();
    unsigned id  = 0;
    for (unsigned long j = 0; j < tree_.size(); j++) {
      long dist = (tree_[j].coord - targetList_[i]).lengthSqr();  // calculate the distance between the node and the target.

      if (dist < minDist) {  // if this node is closer than the previous one, select it as the new nearest node.
        minDist = dist;
        id      = j;
      }
    }
    vesselTargetList[id].push_back(i);  // ad the target to the vessel target list.
  }

  return vesselTargetList;
}

/**
 * Get the nearest target for a given leave.
 *
 * @param leave  The coordinates of the leave.
 *
 * @return The Id of the nearest target.
 */
unsigned int VesselTree::nearestTarget(math::Vector3<long> leave) {
  unsigned int near    = 0;
  unsigned int minDist = (leave[0] - targetList_[0][0]) * (leave[0] - targetList_[0][0]);
  minDist += (leave[1] - targetList_[0][1]) * (leave[1] - targetList_[0][1]);
  minDist += (leave[2] - targetList_[0][2]) * (leave[2] - targetList_[0][2]);

  for (unsigned int i = 1; i < targetList_.size(); ++i) {
    unsigned int dist = (leave[0] - targetList_[i][0]) * (leave[0] - targetList_[i][0]);
    dist += (leave[1] - targetList_[i][1]) * (leave[1] - targetList_[i][1]);
    dist += (leave[2] - targetList_[i][2]) * (leave[2] - targetList_[i][2]);
    if (dist > minDist) continue;
    near    = i;
    minDist = dist;
  }
  return near;
}

/**
 * Check if a node is near enough to a target.
 *
 * Check if the node is closer to the target than the diffusion length.
 *
 * @param node    Node checked.
 * @param target  The target point checked.
 *
 * @return True if close enough, False otherwise.
 */
unsigned int VesselTree::distToTarget(math::Vector3<long> node, math::Vector3<long> target) {
  unsigned int distTarget{0};
  for (unsigned int j = 0; j < 3; j++) {
    distTarget += (node[j] - target[j]) * (node[j] - target[j]);
  }
  return distTarget;
}

/**
 * Calculate random value.
 *
 * Give random value between 0 and 10000 and check if the value is lower than rate * 10000.
 *
 * @param rate  The rate.
 *
 * @return True if it is lower, False otherwise.
 */
bool VesselTree::randomRate(real_t rate) {
  return uniformRate_(generator_) <= rate;
}

/**
 * Build a list of direction for every target.
 *
 * For every leaves, build a vector "direction" to the nearest target.
 *
 * @return A list of vector direction.
 */
std::vector<math::Vector3<real_t>> VesselTree::buildTargetDirection() {
  std::vector<math::Vector3<real_t>> result;
  result.resize(targetList_.size());

  for (auto& node : tree_) {
    if (!node.isLeave()) continue;
    auto coord       = node.coord;
    auto targetID    = nearestTarget(coord);
    result[targetID] = targetList_[targetID] - static_cast<math::Vector3<real_t>>(coord);
    result[targetID] = result[targetID].normalize();
  }

  return result;
}

/**
 * Gets random coordinates.
 *
 * @return A vector of random coordinates.
 */
math::Vector3<long> VesselTree::rndCoordinates() {
  return math::Vector3<long>{uniform_[0](generator_), uniform_[1](generator_), uniform_[2](generator_)};
}

EdgeContainer VesselTree::getEdges() const {
  return EdgeContainer(*this);
}

}  // namespace vessel
}  // namespace filling
}  // namespace nastja
