/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/math/random.h"
#include "lib/math/vector.h"
#include "nlohmann/json.hpp"
#include <utility>
#include <vector>

namespace nastja {
namespace filling {
namespace vessel {

using json = nlohmann::json;

class EdgeContainer;

class VesselTree {
public:
  VesselTree(const json::json_pointer& jptr, config::Config& config);

  struct Node {
    bool isLeave() const {
      return left == -1 && right == -1;
    }

    std::pair<long, long> getChildren() const {
      return std::make_pair(left, right);
    }

    long parent = -1;
    long left   = -1;
    long right  = -1;
    math::Vector3<long> coord;
    unsigned long leaveCount = 0;  //number of leaves under this node
  };

  Node operator[](unsigned long index) const {
    return tree_[index];
  }

  std::vector<Node> getNodes() const {
    return tree_;
  }

  EdgeContainer getEdges() const;

private:
  enum class VesselType { vessel,
                          capillaries };

  void buildTree();
  void buildVessels();
  void buildCapillaries();
  void updateLeaveCount();
  Node buildNode(unsigned int nodeID, unsigned int length);
  math::Vector3<long> selectAverageTarget(const std::vector<unsigned int>& vesselTargetList);
  void setupRandom(math::Vector3<long> leave, math::Vector3<long> target, VesselType Type);
  std::vector<std::vector<unsigned int>> selectNearestNode();
  unsigned int nearestTarget(math::Vector3<long> leave);
  unsigned int distToTarget(math::Vector3<long> node, math::Vector3<long> target);
  bool randomRate(real_t rate);
  std::vector<math::Vector3<real_t>> buildTargetDirection();
  math::Vector3<long> rndCoordinates();

  std::vector<Node> tree_;

  std::array<math::UniformInt<long>, 3> uniform_;  ///< Uniform distributions for x, y, z.
  math::UniformReal<real_t> uniformRate_;          ///< 0 to 1 uniform distribution.
  std::mt19937_64 generator_{};
  real_t splitRateVessel_;                    ///< Probability to split in 2 leaves in vessels
  std::vector<real_t> splitRateCapillaries_;  ///< Probability to split in 2 leaves in vessels

  math::BoundingBox<long> boundingBox_;
  std::vector<math::Vector3<long>> targetList_;  ///< List of point targeted by the tree.
  std::vector<unsigned int> targetListWeight_;
  math::Vector3<long> root_;  ///< Tree's starting point.

  std::vector<unsigned int> diffusionDistance_;  ///< Diffusion distance of nutriments
  unsigned int numPartCapillaries_;              ///< number of part on capillaries
  unsigned int lengthMin_;                       ///< Edge minimum length.
  unsigned int lengthMax_;                       ///< Edge maximum length.
  unsigned int lengthStep_;                      ///< Decrease step for vessels length.
};

}  // namespace vessel
}  // namespace filling
}  // namespace nastja
