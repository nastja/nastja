/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/shape/shape.h"
#include "lib/math/vector.h"

namespace nastja {
namespace filling {
namespace shapes {

/**
 * Class for filling a cube.
 */
class Cube : public Shape {
public:
  explicit Cube(const json::json_pointer& jptr, config::Config& config) : Shape(jptr, config) {
    /// @key{Filling.\template{field}[].\template{shape=cube}.box, Bbox<int>}
    /// The BoundingBox of the cube.
    cube_ = config.getBoundingBox<long>(jptr / "box");

    /// @key{Filling.\template{field}[].\template{shape=cube}.rotation, Vector3<real_t>}
    /// The Euler angles for the rotation.
    rotation_ = config.getRotationMatrix(jptr / "rotation");

    center_ = real_t(0.5) * (static_cast<math::Vector3<real_t>>(cube_.lower()) + static_cast<math::Vector3<real_t>>(cube_.upper()));

    bbox_ = cube_.getRotationExtension(rotation_, center_);
  }

  bool isInside(math::Vector3<long> coordinate) const override {
    auto v = static_cast<math::Vector3<real_t>>(coordinate);

    v -= center_;
    v = rotation_.multiplyT(v);
    v += center_;

    return cube_.isInside(static_cast<math::Vector3<long>>(math::round(v)));
  }

  math::BoundingBox<long> getBoundingBox() const override {
    return bbox_;
  }

private:
  math::BoundingBox<long> cube_;    ///< The cuboid box.
  math::BoundingBox<long> bbox_;    ///< The bounding box.
  math::Matrix3<real_t> rotation_;  ///< The rotation matrix.
  math::Vector3<real_t> center_;    ///< The center of the rotation.
};

}  // namespace shapes
}  // namespace filling
}  // namespace nastja
