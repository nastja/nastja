/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/shape/shape.h"

namespace nastja {
namespace filling {
namespace shapes {

/**
 * Class for filling a sphere.
 */
class Sphere : public Shape {
public:
  explicit Sphere(const json::json_pointer& jptr, config::Config& config) : Shape(jptr, config) {
    /// @key{Filling.\template{field}[].\template{shape=sphere}.center, Vector3<int>}
    /// The center of the sphere.
    center_ = config.getVector3<long>(jptr / "center");

    /// @key{Filling.\template{field}[].\template{shape=sphere}.radius, uint}
    /// The radius of the sphere.
    radius_       = config.getValue<unsigned long>(jptr / "radius");
    radiusSquare_ = radius_ * radius_;
  }

  explicit Sphere(math::Vector3<long> center, unsigned int radius) : center_{center}, radius_{radius}, radiusSquare_{radius * radius} {}

  bool isInside(math::Vector3<long> coordinate) const override {
    auto distance = static_cast<math::Vector3<long>>(coordinate) - center_;

    return static_cast<unsigned long>(math::dotProduct(distance, distance)) <= radiusSquare_;
  }

  math::BoundingBox<long> getBoundingBox() const override {
    return math::BoundingBox<long>(center_, radius_).extend(0, 1);
  }

private:
  math::Vector3<long> center_;  ///< The center.
  unsigned long radius_;        ///< The radius.
  unsigned long radiusSquare_;  ///< The squared radius.
};

}  // namespace shapes
}  // namespace filling
}  // namespace nastja
