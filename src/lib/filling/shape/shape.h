/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include "lib/simdata/simdata.h"
#include "nlohmann/json.hpp"
#include <cmath>

namespace nastja {
namespace filling {
namespace shapes {

using json = nlohmann::json;

/**
 * Abstract class for filling shape.
 */
class Shape {
public:
  explicit Shape(const json::json_pointer& /*jptr*/, config::Config& /*config*/) {}
  explicit Shape()        = default;
  virtual ~Shape()        = default;
  Shape(const Shape&)     = delete;
  Shape(Shape&&) noexcept = default;
  Shape& operator=(const Shape&) = delete;
  Shape& operator=(Shape&&) = default;

  /**
   * Gets the bounding box of the shape.
   *
   * @return The bounding box.
   */
  virtual math::BoundingBox<long> getBoundingBox() const = 0;

  /**
   * Determines if the coordinate is inside of the shape without checking outside the bounding box.
   *
   * @param coordinate  The coordinate.
   *
   * @return True if inside, False otherwise.
   */
  virtual bool isInside(math::Vector3<long> coordinate) const = 0;

  /**
   * Determines if the coordinate is inside of the shape without checking outside the bounding box.
   *
   * @param x  The x-coordinate.
   * @param y  The y-coordinate.
   * @param z  The z-coordinate.
   *
   * @return True if inside, False otherwise.
   */
  bool isInside(unsigned long x, unsigned long y, unsigned long z) const {
    return isInside(math::Vector3<long>{static_cast<long>(x), static_cast<long>(y), static_cast<long>(z)});
  }
};

}  // namespace shapes
}  // namespace filling
}  // namespace nastja
