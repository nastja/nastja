/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/filling/shape/shape.h"
#include "lib/math/matrix3.h"

namespace nastja {
namespace filling {
namespace shapes {

/**
 * Class for filling an ellipsoid.
 */
class Ellipsoid : public Shape {
public:

  explicit Ellipsoid(const json::json_pointer& jptr, config::Config& config) : Shape(jptr, config) {
    /// @key{Filling.\template{field}[].\template{shape=ellipsoid}.center, Vector3<int>}
    /// The center of the ellipsoid.
    center_ = config.getVector3<long>(jptr / "center");

    /// @key{Filling.\template{field}[].\template{shape=ellipsoid}.xsemiaxis, uint}
    /// The principal semi-axis in x-direction of the ellipsoid.
    semiaxis_[0] = config.getValue<unsigned long>(jptr / "xsemiaxis");

    /// @key{Filling.\template{field}[].\template{shape=ellipsoid}.ysemiaxis, uint}
    /// The principal semi-axis in y-direction of the ellipsoid.
    semiaxis_[1] = config.getValue<unsigned long>(jptr / "ysemiaxis");

    /// @key{Filling.\template{field}[].\template{shape=ellipsoid}.zsemiaxis, uint}
    /// The principal semi-axis in z-direction of the ellipsoid.
    semiaxis_[2] = config.getValue<unsigned long>(jptr / "zsemiaxis");

    /// @key{Filling.\template{field}[].\template{shape=ellipsoid}.rotation, Vector3<real_t>}
    /// The Euler angles for the rotation.
    rotation_ = config.getRotationMatrix(jptr / "rotation");

    bbox_ = math::BoundingBox<long>(static_cast<math::Vector3<long>>(center_ - semiaxis_), static_cast<math::Vector3<long>>(center_ + semiaxis_ + 1));
    bbox_ = bbox_.getRotationExtension(rotation_, static_cast<math::Vector3<real_t>>(center_));

    auto semiaxisSqr          = semiaxis_ * semiaxis_;
    semiaxisInverseSquare_[0] = 1.0 / semiaxisSqr[0];
    semiaxisInverseSquare_[1] = 1.0 / semiaxisSqr[1];
    semiaxisInverseSquare_[2] = 1.0 / semiaxisSqr[2];
  }

  Ellipsoid(const math::Vector3<long>& center, const math::Vector3<unsigned long>& semiaxis, const math::Vector3<real_t>& rotation){
    center_[0] = center[0]; center_[1] = center[1]; center_[2] = center[2];
    semiaxis_[0] = semiaxis[0]; semiaxis_[1] = semiaxis[1]; semiaxis_[2] = semiaxis[2];  
    rotation_ = math::Matrix3<real_t>(rotation[0], rotation[1], rotation[2]);

    bbox_ = math::BoundingBox<long>(static_cast<math::Vector3<long>>(center_ - semiaxis_), static_cast<math::Vector3<long>>(center_ + semiaxis_ + 1));
    bbox_ = bbox_.getRotationExtension(rotation_, static_cast<math::Vector3<real_t>>(center_));

    auto semiaxisSqr          = semiaxis_ * semiaxis_;
    semiaxisInverseSquare_[0] = 1.0 / semiaxisSqr[0];
    semiaxisInverseSquare_[1] = 1.0 / semiaxisSqr[1];
    semiaxisInverseSquare_[2] = 1.0 / semiaxisSqr[2];
  }

  bool isInside(math::Vector3<long> coordinate) const override {
    auto v = static_cast<math::Vector3<real_t>>(coordinate);

    v -= center_;
    v = rotation_.multiplyT(v);  // no need to translate v back, because we need the distance from the rotated
                                 // coordinate to the center.

    return math::dotProduct(v * v, semiaxisInverseSquare_) <= 1.0;
  }

  math::BoundingBox<long> getBoundingBox() const override {
    return bbox_;
  }

private:
  math::Vector3<long> center_;                   ///< The center.
  math::Vector3<unsigned long> semiaxis_;        ///< The vector of principal semi-axes.
  math::Vector3<real_t> semiaxisInverseSquare_;  ///< The vector of inverse squared principal semi-axes.
  math::Matrix3<real_t> rotation_;               ///< The rotation matrix.
  math::BoundingBox<long> bbox_;                 ///< The bounding box.
};

}  // namespace shapes
}  // namespace filling
}  // namespace nastja
