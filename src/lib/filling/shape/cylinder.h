/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/filling/shape/shape.h"

namespace nastja {
namespace filling {
namespace shapes {

/**
 * Class for filling a cylinder or a cone.
 */
class Cylinder : public Shape {
public:
  enum class Variant { cylinder,
                       cone };
  explicit Cylinder(const json::json_pointer& jptr, config::Config& config) : Shape{jptr, config} {
    /// @key{Filling.\template{field}[].\template{shape=cylinder}.center, Vector3<int>}
    /// The center of the base area.
    base_ = config.getVector3<long>(jptr / "base");

    /// @key{Filling.\template{field}[].\template{shape=cylinder}.radius, uint}
    /// The radius of the base area.
    radius_       = config.getValue<unsigned long>(jptr / "radius");
    radiusSquare_ = radius_ * radius_;

    if (config.contains(jptr / "length")) {  // if defined with on point and one length.
      /// @key{Filling.\template{field}[].\template{shape=cylinder}.length, uint, optional}
      /// The length of the cylinder in z direction.
      auto length = config.getValue<unsigned long>(jptr / "length");
      top_        = base_;
      top_.z() += length - 1;
    } else if (config.contains(jptr / "top")) {  // if defined with 2 points.
      /// @key{Filling.\template{field}[].\template{shape=cylinder}.top, uint, optional}
      /// The top of the cylinder.
      top_ = config.getVector3<long>(jptr / "top");
    } else {
      throw std::runtime_error("wrong definition of the shape");
    }

    if (config.getValue<std::string>(jptr / "shape") == "cylinder") {
      variant_ = Variant::cylinder;
    } else if (config.getValue<std::string>(jptr / "shape") == "cone") {
      variant_ = Variant::cone;
    } else {
      throw std::runtime_error("Unknown shape.");
    }
  }

  /**
   * Cylinder or cone by definition of two points and a radius.
   *
   * @param base     The center of the base area.
   * @param top      The center of the top area.
   * @param radius   The radius.
   * @param variant  The shape variant, cylinder or cone.
   */
  explicit Cylinder(math::Vector3<long> base, math::Vector3<long> top, unsigned long radius, Variant variant)
      : base_{base}, top_{top}, radius_{radius}, radiusSquare_{radius * radius}, variant_{variant} {}

  bool isInside(math::Vector3<long> coordinate) const override {
    auto vectCylinder = top_ - base_;  // Get a basis vector for the cylinder.
    coordinate -= base_;               // Shifts the postition in relation to the cylinder basis.

    long altitudeSqr = dotProduct(vectCylinder, coordinate);  // The height of coordinate compared to the cylinder.
    long highSqr     = vectCylinder.lengthSqr();              // Square of the height of the cylinder.

    if (altitudeSqr > highSqr || altitudeSqr < 0) return false;
    // If vecCylinder is normalized, this gives the distance to the cylinder center, due to the unnormalized
    // vecCylinder this is the distance times highSqr.
    unsigned long radiusDistSqr = crossProduct(coordinate, vectCylinder).lengthSqr();
    if (variant_ == Variant::cylinder) {
      altitudeSqr = 0;
    }

    return radiusDistSqr <= radiusSquare_ * (highSqr - altitudeSqr);
  }

  math::BoundingBox<long> getBoundingBox() const override {
    return math::BoundingBox<long>(base_, top_).extend(radius_);
  }

private:
  math::Vector3<long> base_;  ///< The center of the base area.
  math::Vector3<long> top_;   ///< The center of the top area.

  unsigned long radius_;        ///< The radius.
  unsigned long radiusSquare_;  ///< The squared radius.

  Variant variant_;
};

}  // namespace shapes
}  // namespace filling
}  // namespace nastja
