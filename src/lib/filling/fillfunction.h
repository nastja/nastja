/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/field.h"
#include "lib/filling/pattern/pattern.h"
#include "lib/filling/shape/shape.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include "nlohmann/json.hpp"
#include <cmath>

namespace nastja {
namespace filling {

using json = nlohmann::json;

/**
 * Abstract class for filling functions.
 */
class IFillFunction {
public:
  explicit IFillFunction()                = default;
  virtual ~IFillFunction()                = default;
  IFillFunction(const IFillFunction&)     = delete;
  IFillFunction(IFillFunction&&) noexcept = default;
  IFillFunction& operator=(const IFillFunction&) = delete;
  IFillFunction& operator=(IFillFunction&&) = default;
};

/**
 * Abstract class for filling function.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX>
class FillFunction : public IFillFunction {
public:
  explicit FillFunction()               = default;
  ~FillFunction() override              = default;
  FillFunction(const FillFunction&)     = delete;
  FillFunction(FillFunction&&) noexcept = default;
  FillFunction& operator=(const FillFunction&) = delete;
  FillFunction& operator=(FillFunction&&) = default;

  virtual bool fill(field::Field<T, TSplitX>& field, unsigned int component, pattern::Pattern<T>* pattern, std::vector<unsigned int>& cellTypes, std::map<unsigned int, std::set<T>>* cellTypeMap) const = 0;
};

}  // namespace filling
}  // namespace nastja
