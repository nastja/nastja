/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/field.h"
#include "lib/filling/fillfunction.h"
#include "lib/filling/pattern/pattern.h"
#include "lib/filling/shape/shape.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include "nlohmann/json.hpp"
#include <cmath>

namespace nastja {
namespace filling {

using json = nlohmann::json;

/**
 * Gets the cell type.
 *
 * @param vec    The vector.
 * @param value  The value of the cell ID.
 *
 * @tparam T  Type of the data field
 *
 * @return The cell type.
 */
template <typename T>
int getCellType(const std::vector<unsigned int>& vec, T value) {
  return std::count_if(vec.begin(), vec.end(), [value](unsigned int i) { return i < value; });
}

/**
 * Generic filling function for shapes.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX, typename TShape>
class FillGenericShape : public FillFunction<T, TSplitX> {
public:
  explicit FillGenericShape(const json::json_pointer& jptr, config::Config& config) : shape{jptr, config} {}
  explicit FillGenericShape(TShape&& shape) : shape{std::move(shape)} {}
  ~FillGenericShape() override                  = default;
  FillGenericShape(const FillGenericShape&)     = delete;
  FillGenericShape(FillGenericShape&&) noexcept = default;
  FillGenericShape& operator=(const FillGenericShape&) = delete;
  FillGenericShape& operator=(FillGenericShape&&) = default;

  bool fill(field::Field<T, TSplitX>& field, unsigned int component, pattern::Pattern<T>* pattern, std::vector<unsigned int>& cellTypes, std::map<unsigned int, std::set<T>>* cellTypeMap) const override {
    bool filled = false;

    const auto boundarySize = field.getBoundarySize();
    const auto offset       = field.getOffset();
    const auto blocksize    = field.getSize() - 2 * boundarySize;
    const auto shift        = static_cast<math::Vector3<long>>(offset) - static_cast<math::Vector3<long>>(boundarySize);

    auto blockBB = math::BoundingBox<long>(offset, offset + blocksize);

    auto objectBB = shape.getBoundingBox();
    objectBB.intersect(blockBB);

    if (objectBB.empty()) return false;

    objectBB.translate(-shift);
    auto view = field.template createView<1>(static_cast<math::BoundingBox<unsigned long>>(objectBB));
    for (auto cell = view.begin(); cell != view.end(); ++cell) {
      auto x = cell.coordinates().x();
      auto y = cell.coordinates().y();
      auto z = cell.coordinates().z();
      if (!shape.isInside(static_cast<math::Vector3<long>>(cell.coordinates()) + shift)) continue;

      auto value                        = pattern->calc(x, y, z);
      field.getCell(x, y, z, component) = value;
      if (cellTypeMap) {
        (*cellTypeMap)[getCellType(cellTypes, value)].insert(value);
      }
      filled = true;
    }

    return filled;
  }

  TShape shape;
};

}  // namespace filling
}  // namespace nastja
