/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/config/config.h"
#include "lib/filling/fillfunction.h"
#include "lib/filling/fillingvessel.h"
#include "lib/filling/genericshape.h"
#include "lib/filling/shape/cube.h"
#include "lib/filling/shape/cylinder.h"
#include "lib/filling/shape/ellipsoid.h"
#include "lib/filling/shape/sphere.h"
#include "nlohmann/json.hpp"

namespace nastja {
namespace filling {

using json = nlohmann::json;

template <typename T, unsigned int TSplitX>
class FactoryFilling : public Factory<FillFunction<T, TSplitX>, const json::json_pointer&, config::Config&> {
public:
  /// @factory{FactoryFilling}
  /// "cube", "cylinder", "sphere", "cone", "vessel"
  explicit FactoryFilling() {
    base_t::template registerType<FillGenericShape<T, TSplitX, shapes::Cube>>("cube");
    base_t::template registerType<FillGenericShape<T, TSplitX, shapes::Cylinder>>("cylinder");
    base_t::template registerType<FillGenericShape<T, TSplitX, shapes::Cylinder>>("cone");
    base_t::template registerType<FillGenericShape<T, TSplitX, shapes::Sphere>>("sphere");
    base_t::template registerType<FillGenericShape<T, TSplitX, shapes::Ellipsoid>>("ellipsoid");
    base_t::template registerType<FillFunctionVessel<T, TSplitX>>("vessel");
  }

private:
  using base_t = Factory<FillFunction<T, TSplitX>, const json::json_pointer&, config::Config&>;
};

}  // namespace filling
}  // namespace nastja
