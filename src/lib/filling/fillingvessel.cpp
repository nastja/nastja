/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "fillingvessel.h"
#include "lib/filling/genericshape.h"
#include "lib/filling/shape/cylinder.h"
#include "lib/filling/shape/sphere.h"

namespace nastja {
namespace filling {

using namespace vessel;

/**
 * Fills a blood vessel.
 *
 * This function go through the vessels list to fill the vessel. First, fills cylinders for classic edges and cones for
 * leaves. Then builds spheres on both ends to connect the cylinders more realistic.
 *
 * @param field        The field.
 * @param cellTypeMap  The cellTypeMap.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX>
bool FillFunctionVessel<T, TSplitX>::fill(field::Field<T, TSplitX>& field, unsigned int component, pattern::Pattern<T>* pattern, std::vector<unsigned int>& cellTypes, std::map<unsigned int, std::set<T>>* cellTypeMap) const {
  bool filled = false;
  for (const auto& edge : tree_.getEdges()) {
    if (tree_[edge.dst].isLeave()) {
      filled |= createPartFillFunction(edge, Part::cone)->fill(field, component, pattern, cellTypes, cellTypeMap);
    } else {
      filled |= createPartFillFunction(edge, Part::cylinder)->fill(field, component, pattern, cellTypes, cellTypeMap);
      filled |= createPartFillFunction(edge, Part::sphere2)->fill(field, component, pattern, cellTypes, cellTypeMap);
    }
    filled |= createPartFillFunction(edge, Part::sphere)->fill(field, component, pattern, cellTypes, cellTypeMap);
  }

  return filled;
}

template <typename T, unsigned int TSplitX>
std::unique_ptr<FillFunction<T, TSplitX>> FillFunctionVessel<T, TSplitX>::createPartFillFunction(const EdgeIterator::Edge& edge, Part part) const {
  unsigned int radius         = ceil(std::sqrt(edge.weight * area_));
  math::Vector3<long> center  = buildRootPoint(edge);
  math::Vector3<long> center2 = tree_[edge.dst].coord;

  if (part == Part::cylinder) {
    return std::make_unique<FillGenericShape<T, TSplitX, shapes::Cylinder>>(shapes::Cylinder(center, center2, radius, shapes::Cylinder::Variant::cylinder));
  }

  if (part == Part::cone) {
    return std::make_unique<FillGenericShape<T, TSplitX, shapes::Cylinder>>(shapes::Cylinder(center, center2, radius, shapes::Cylinder::Variant::cone));
  }

  if (part == Part::sphere) {
    center = tree_[edge.src].coord;
    return std::make_unique<FillGenericShape<T, TSplitX, shapes::Sphere>>(shapes::Sphere(center, radius));
  }
  if (part == Part::sphere2) {
    center = tree_[edge.dst].coord;
    return std::make_unique<FillGenericShape<T, TSplitX, shapes::Sphere>>(shapes::Sphere(center, radius));
  }

  throw std::runtime_error("Undefined shape");
}

/**
 * Shift the root node of the vessel to the edge to improve the render.
 *
 * @param edge  The edge.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 *
 * @return The coordinates of the shifted node.
 */
template <typename T, unsigned int TSplitX>
math::Vector3<long> FillFunctionVessel<T, TSplitX>::buildRootPoint(const EdgeIterator::Edge& edge) const {
  auto srcNode = tree_[edge.src];
  auto dstNode = tree_[edge.dst];
  math::Vector3<long> uncenteredNode;
  auto rootChildren  = tree_[0].getChildren();
  bool isFirstVessel = srcNode.parent == -1 && tree_[rootChildren.first].coord == dstNode.coord;

  if (!isFirstVessel && tree_[srcNode.parent].leaveCount * area_ != edge.weight * area_) {
    auto vesselDirection = static_cast<math::Vector3<real_t>>(dstNode.coord - srcNode.coord);

    auto previousVesselDirection = (srcNode.coord - tree_[srcNode.parent].coord).getNormalized();
    previousVesselDirection *= previousVesselDirection.dotProduct(vesselDirection);

    math::Vector3<real_t> offsetVector = vesselDirection - previousVesselDirection;
    if (offsetVector.length() != 0) {
      offsetVector = offsetVector / std::sqrt(offsetVector.dotProduct(offsetVector));
    }
    real_t previousArea = tree_[srcNode.parent].leaveCount * area_;
    real_t nextArea     = edge.weight * area_;
    offsetVector *= (std::sqrt(previousArea) - std::sqrt(nextArea));

    for (unsigned int i = 0; i < 3; ++i) {
      uncenteredNode[i] = srcNode.coord[i] + round(offsetVector[i]);
    }
  } else {
    uncenteredNode = srcNode.coord;
  }

  return uncenteredNode;
}

template class FillFunctionVessel<real_t, 1u>;
template class FillFunctionVessel<real_t, 2u>;
template class FillFunctionVessel<real_t, 4u>;
template class FillFunctionVessel<real_t, 8u>;
template class FillFunctionVessel<real_t, 16u>;
template class FillFunctionVessel<cellid_t, 1u>;
template class FillFunctionVessel<cellid_t, 2u>;
template class FillFunctionVessel<cellid_t, 4u>;
template class FillFunctionVessel<cellid_t, 8u>;
template class FillFunctionVessel<cellid_t, 16u>;

}  // namespace filling
}  // namespace nastja
