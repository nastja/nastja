/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/filling/fillfunction.h"
#include "lib/filling/vessel/edgecontainer.h"
#include "lib/filling/vessel/vesseltree.h"
#include "lib/math/boundingbox.h"
#include "lib/math/random.h"
#include "lib/math/vector.h"
#include "nlohmann/json.hpp"
#include <cmath>
#include <vector>

namespace nastja {
namespace filling {

using json = nlohmann::json;

/**
 * Class for filling a Blood Vessel.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX>
class FillFunctionVessel : public FillFunction<T, TSplitX> {
public:
  explicit FillFunctionVessel(const json::json_pointer& jptr, config::Config& config) : tree_{jptr, config} {
    cellType_ = config.getValue<unsigned int>(jptr / "celltype");
    area_     = config.getValue<unsigned int>(jptr / "area");
  }

  ~FillFunctionVessel() override                    = default;
  FillFunctionVessel(const FillFunctionVessel&)     = default;
  FillFunctionVessel(FillFunctionVessel&&) noexcept = default;
  FillFunctionVessel& operator=(const FillFunctionVessel&) = default;
  FillFunctionVessel& operator=(FillFunctionVessel&&) = default;

  bool fill(field::Field<T, TSplitX>& field, unsigned int component, pattern::Pattern<T>* pattern, std::vector<unsigned int>& cellTypes, std::map<unsigned int, std::set<T>>* cellTypeMap) const override;

  enum class Part { cylinder,
                    cone,
                    sphere,
                    sphere2 };

private:
  std::unique_ptr<FillFunction<T, TSplitX>> createPartFillFunction(const vessel::EdgeIterator::Edge& edge, Part part) const;

  math::Vector3<long> buildRootPoint(const vessel::EdgeIterator::Edge& edge) const;

  int area_;               ///< leaves' area.
  unsigned int cellType_;  ///< The cell type.

  vessel::VesselTree tree_;
};

}  // namespace filling
}  // namespace nastja
