/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "logger_impl.h"
#include "lib/io/writerreader.h"
#include "lib/logger/registry.h"
#include "lib/simdata/mpidata.h"
#include <array>
#include <chrono>
#include <iomanip>
#include <ostream>
#include <sstream>

namespace nastja {
namespace logger {

// clang-format off
#define ANSI_COLOR_BALCK     "\x1b[30m"
#define ANSI_COLOR_BLACKH    "\x1b[30;1m"
#define ANSI_COLOR_RED       "\x1b[31m"
#define ANSI_COLOR_REDH      "\x1b[31;1m"
#define ANSI_COLOR_GREEN     "\x1b[32m"
#define ANSI_COLOR_GREENH    "\x1b[32;1m"
#define ANSI_COLOR_YELLOW    "\x1b[33m"
#define ANSI_COLOR_YELLOWH   "\x1b[33;1m"
#define ANSI_COLOR_BLUE      "\x1b[34m"
#define ANSI_COLOR_BLUEH     "\x1b[34;1m"
#define ANSI_COLOR_MAGENTA   "\x1b[35m"
#define ANSI_COLOR_MAGENTAH  "\x1b[35;1m"
#define ANSI_COLOR_CYAN      "\x1b[36m"
#define ANSI_COLOR_CYANH     "\x1b[36;1m"
#define ANSI_COLOR_WHITE     "\x1b[37m"
#define ANSI_COLOR_WHITEH    "\x1b[37;1m"
#define ANSI_COLOR_BGBLACK   "\x1b[40m"
#define ANSI_COLOR_BGRED     "\x1b[41m"
#define ANSI_COLOR_BGGREEN   "\x1b[42m"
#define ANSI_COLOR_BGYELLOW  "\x1b[43m"
#define ANSI_COLOR_BGBLUE    "\x1b[44m"
#define ANSI_COLOR_BGMAGENTA "\x1b[45m"
#define ANSI_COLOR_BGCYAN    "\x1b[46m"
#define ANSI_COLOR_BGWHITE   "\x1b[47m"
#define ANSI_COLOR_RESET     "\x1b[0m"
// clang-format on

static const std::array<fmt::string_view, 6> level_names{"off", "error", "warning", "info", "debug", "trace"};
static const std::array<fmt::string_view, 6> short_level_names{"off", "err", "warn", "info", "dbg", "trce"};

static const std::array<fmt::string_view, 8> level_color{
    ANSI_COLOR_GREENH,   // off
    ANSI_COLOR_REDH,     // error
    ANSI_COLOR_YELLOW,   // warning
    ANSI_COLOR_WHITEH,   // info
    ANSI_COLOR_BLUE,     // debug
    ANSI_COLOR_MAGENTA,  // trace
    ANSI_COLOR_RESET,    // reset
    ""                   // no_color
};

struct LookupName {
  unsigned int cat;
  char name[8];
};

// clang-format off
const LookupName names[]{
    {Category::None,       "NONE"},
    {Category::CommExch,   "COMM"},
    {Category::CommHS,     "HDSH"},
    {Category::CommFCT,    "FCS_"},
    {Category::CommDel,    "CDEL"},
    {Category::CommAnnc,   "ANNC"},
    {Category::CommInc,    "INCM"},
    {Category::DynBlock,   "DYN_"},
    {Category::DynReachBD, "DYRB"},
    {Category::DynCnstBlk, "CBLK"},
    {Category::DynNewComm, "NEWC"},
    {Category::DynNewBlk,  "BLKN"},
    {Category::DynNewBlkL, "BLKL"},
    {Category::DynDelBlk,  "BLKD"},
    {Category::LoadBal,    "LBAL"},
    {Category::NewField,   "NEWF"},
    {Category::BlkComList, "BCL_"},
    {Category::All,        "????"}};
// clang-format on

std::ostream& Logger::initStream(int groupSize) {
  if (groupSize > 1) {
    return logStream_;
  }

  return logFile_;
}

Logger::Logger(std::ostream& ostream /*= std::cout*/) : ostream_{ostream}, coloredOutput_{true} {
  initializeRank();
}

Logger::Logger(const std::string& filename, int groupSize /*= 0*/, int tagOffset /*= 0*/)
    : ostream_{initStream(groupSize)},
      coloredOutput_{false},
      groupSize_{groupSize},
      tagOffset_{tagOffset} {
  initializeRank();
  target_ = rank_ - ((groupSize == 0) ? 0 : (rank_ % groupSize));
  if (rank_ == target_) {
    WriterReader::checkPath(filename);
    logFile_.open(filename);
  }
}

Logger::~Logger() {
  flush();
  MPI_Wait(&sendRequest_, MPI_STATUS_IGNORE);
  if (!endString_.empty()) logFile_ << endString_;
}

/**
 * Synchronizes a collective logger.
 *
 * This sends and receives with the `MPILoggerFinalize` tag. It waits for all peers to sending a finalize message, when
 * a finalize message is receives all `MPILogger` message from that specific rank are collected before.
 */
void Logger::sync() {
  if (groupSize_ <= 1) return;
  if (rank_ == target_) {
    logLocalBuffer();
    recv();
  } else {
    send(MPILoggerFinalize + tagOffset_);
  }
}

/**
 * Synchronizes a collective logger in a non blocking way.
 *
 * This sends and receives with the `MPILogger` tag. Only if a message is ready it will pe collected.
 */
void Logger::isync() {
  if (groupSize_ <= 1) return;
  if (rank_ == target_) {
    logLocalBuffer();
    iprobe(MPI_ANY_SOURCE, MPILogger + tagOffset_);
  } else {
    send(MPILogger + tagOffset_);
  }
}

/**
 * Sends the log buffer to the target.
 */
void Logger::send(int tag) {
  MPI_Wait(&sendRequest_, MPI_STATUS_IGNORE);
  sendBuffer_ = logStream_.str();
  MPI_Isend(sendBuffer_.data(), sendBuffer_.size(), MPI_CHAR, target_, tag, MPI_COMM_WORLD, &sendRequest_);
  logStream_.str({});
  logStream_.clear();
}

/**
 * Receives the log buffer from peers and write it to the file.
 */
void Logger::recv() {
  unsigned int numOfWorkersInGroup = groupSize_;
  if (mpiSize_ < (rank_ + groupSize_)) {
    numOfWorkersInGroup = mpiSize_ % groupSize_;
  }
  numOfWorkersInGroup--;

  while (numOfWorkersInGroup > 0) {
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPILoggerFinalize + tagOffset_, MPI_COMM_WORLD, &status);
    iprobe(status.MPI_SOURCE, MPILogger + tagOffset_);
    recvLog(status, MPILoggerFinalize + tagOffset_);

    numOfWorkersInGroup--;
  }
}

/**
 * Probs for a message and receives it.
 *
 * @param source  The source rank.
 * @param tag     The MPI tag.
 */
void Logger::iprobe(int source, int tag) {
  while (true) {
    MPI_Status status;
    int flag = 0;
    MPI_Iprobe(source, tag, MPI_COMM_WORLD, &flag, &status);
    if (flag == 0) break;

    recvLog(status, tag);
  }
}

/**
 * Logs a local buffer.
 */
void Logger::logLocalBuffer() {
  logFile_ << logStream_.str();
  logStream_.str({});
  logStream_.clear();
}

/**
 * Receives a log message and write it to the file.
 *
 * @param status  The status.
 * @param tag     The tag.
 */
void Logger::recvLog(MPI_Status& status, int tag) {
  int count = 0;
  MPI_Get_count(&status, MPI_CHAR, &count);
  recvBuffer_.resize(count);

  MPI_Recv(recvBuffer_.data(), count, MPI_CHAR, status.MPI_SOURCE, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  logFile_ << std::string(recvBuffer_.begin(), recvBuffer_.end());
}

/**
 * Gets the currant time string.
 *
 * @return The time stirng.
 */
std::string getTime() {
  auto now   = std::chrono::system_clock::now();
  auto now_c = std::chrono::system_clock::to_time_t(now);

  std::stringstream ss;
  ss << std::put_time(std::localtime(&now_c), "%F %T");
  return ss.str();
}

/**
 * Looks up the string for a given category.
 *
 * @param category  The category.
 *
 * @return The category string.
 */
const char* Logger::lookupName(Category category) {
  if (category == 0x00) return names[0].name;

  int i = 0;
  while ((names[i].cat & category) == 0x00) {
    i++;
  }

  return names[i].name;
}

/**
 * Looks up the color for a given level.
 *
 * @param level  The level.
 *
 * @return The color ascii escape sequence for the given level when colord output is active; an empty string, otherwise.
 */
inline fmt::string_view Logger::lookupColor(int level) const {
  return level_color[coloredOutput_ ? level : 7];
}

/**
 * Preamble for message logger.
 *
 * @param level  The level.
 */
inline void Logger::preamble(Level level) const {
  auto levelNo = static_cast<unsigned int>(level);
  fmt::print(ostream_, level_format_[levelNo], lookupColor(levelNo), short_level_names[levelNo], rank_, getTime(), *timestep_, lookupColor(6), lookupName(Category::All));
}

/**
 * @overload
 *
 * @param level     The level.
 * @param category  The trace category.
 */
inline void Logger::preamble(Level level, Category category) const {
  auto levelNo = static_cast<unsigned int>(level);
  fmt::print(ostream_, level_format_[6], lookupColor(levelNo), short_level_names[levelNo], rank_, lookupName(category), *timestep_);
}

/**
 * The implementation of the message logger.
 *
 * @param level   The level.
 * @param format  The message format string.
 * @param args    The arguments for the format string.
 */
void Logger::logImpl(Level level, fmt::string_view format, fmt::format_args args) const {
  if (level > level_) return;

  preamble(level);
  fmt::vprint(ostream_, format, args);
  fmt::print(ostream_, level_format_[7], lookupColor(6));
}

/**
 * @overload
 *
 * @param level     The level.
 * @param category  The trace category.
 * @param format    The message format string.
 * @param args      The arguments for the format string.
 */
void Logger::logImpl(Level level, Category category, fmt::string_view format, fmt::format_args args) const {
  if (level > level_) return;
  if ((category & debugDetails_) == 0x00) return;

  preamble(level, category);
  fmt::vprint(ostream_, format, args);
  fmt::print(ostream_, level_format_[7], lookupColor(6));
}

/**
 * Gets the level name.
 *
 * @return The level name.
 */
const char* Logger::getLevelName() const {
  auto levelNo = static_cast<unsigned int>(level_);
  return level_names[levelNo].data();
}

}  // namespace logger
}  // namespace nastja
