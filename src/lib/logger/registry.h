/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "fmt/format.h"
#include <fstream>
#include <iostream>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace nastja {
namespace logger {

class Logger;

/**
 * A singelton registry holding loggers. A default logger to std::cout and optional logger to files.
 */
class Registry {
public:
  ~Registry()                   = default;
  Registry(const Registry&)     = delete;
  Registry(Registry&&) noexcept = default;
  Registry& operator=(const Registry&) = delete;
  Registry& operator=(Registry&&) = delete;

  /**
   * Getter to the logger from the registry.
   *
   * @param name  The name.
   *
   * @return Reference to the logger.
   */
  Logger& get(const std::string& name) {
    auto logger = loggers_.find(name);
    if (logger == loggers_.end()) throw std::out_of_range(fmt::format("The logger '{}' is not registered.", name));
    return *(logger->second);
  }

  /**
   * Register a new default logger.
   *
   * @param name     The name of the logger.
   * @param ostream  The output stream.
   *
   * @return The logger.
   */
  Logger& registerLogger(const std::string& name, std::ostream& ostream = std::cout) {
    loggers_[name] = std::make_unique<Logger>(ostream);

    return *loggers_[name];
  }

  /**
   * @overload
   *
   * @param name      The logger name.
   * @param filename  The filename.
   *
   * @return The logger.
   */
  Logger& registerLogger(const std::string& name, const std::string& filename) {
    loggers_[name] = std::make_unique<Logger>(filename);

    return *loggers_[name];
  }

  /**
   * @overload
   *
   * @param name       The logger name.
   * @param filename   The filename.
   * @param groupSize  The collective output group size.
   *
   * @return The logger.
   */
  Logger& registerLogger(const std::string& name, const std::string& filename, unsigned int groupSize) {
    loggers_[name] = std::make_unique<Logger>(filename, groupSize, 2 * tagCounter_);

    if (groupSize > 1) {
      tagCounter_++;
      if (tagCounter_ > 10) {
        throw std::runtime_error("A maximum of 10 collective loggers can be registered, due to a possible MPI tag conflict.");
      }
    }

    return *loggers_[name];
  }

  /**
   * Determines if a logger is registered.
   *
   * @param name  The logger name.
   *
   * @return True if has a logger with name, False otherwise.
   */
  bool hasLogger(const std::string& name) {
    return loggers_.count(name) > 0;
  }

  /**
   * Closes a logger.
   *
   * @param name  The logger name.
   */
  void closeLogger(const std::string& name) {
    loggers_.erase(name);
  }

  /**
   * Closes all logger.
   */
  void closeAll() {
    loggers_.clear();
  }

  void syncAll();
  void isyncAll();

  static Registry& instance() {
    static Registry instance_;  // Guaranteed to be destroyed.
                                // Instantiated on first use.
    return instance_;
  }

private:
  explicit Registry() {
    registerLogger("");
    defaultLogger_ = loggers_[""].get();
  }

  int tagCounter_{0};  ///< increase the MPI tag for sending collective loggers.

  Logger* defaultLogger_;
  std::unordered_map<std::string, std::unique_ptr<Logger>> loggers_;
};

}  // namespace logger
}  // namespace nastja
