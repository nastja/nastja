/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger/registry.h"
#include "fmt/format.h"
#include "fmt/ostream.h"
#include <array>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <ostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

namespace nastja {
namespace logger {

enum class Level { off,
                   error,
                   warning,
                   info,
                   debug,
                   trace };

struct VerboseCategorieEnum {
  enum Category : unsigned int {
    None = 0x00,
    // Communications
    CommExch = 0x1u << 0u,  // 0001
    CommHS   = 0x1u << 1u,  // 0010
    CommFCT  = 0x1u << 2u,  // 0100
    CommDel  = 0x1u << 3u,  // 1000
    CommInc  = 0x1u << 4u,  // 00010000
    CommAnnc = 0x1u << 5u,  // 00100000
    CommALL  = 0xFF,        // 1111
    // Dynamic Blocks
    DynBlock   = 0x100u << 0u,  // 0001
    DynReachBD = 0x100u << 1u,  // 0010
    DynCnstBlk = 0x100u << 2u,  // 0100
    DynNewComm = 0x100u << 3u,  // 1000
    DynNewBlk  = 0x100u << 4u,  // 00010000
    DynNewBlkL = 0x100u << 5u,  // 00100000
    DynDelBlk  = 0x100u << 6u,  // 01000000
    // LoadBalance
    LoadBal = 0x10000,  //0010,6

    // Other
    NewField   = 0x01000000,
    BlkComList = 0x02000000,
    All        = 0xFFFFFFFF
  };
};
using Category = VerboseCategorieEnum::Category;

/**
 * @class Logger
 *
 * Use `logger::get()` to get the object. Provides Logging for each rank or only rank0.
 */
class Logger {
public:
  /// @name Constructors
  /// @{
  explicit Logger(std::ostream& ostream = std::cout);
  explicit Logger(const std::string& filename, int groupSize = 0, int tagOffset = 0);
  /// @}

  ~Logger();
  Logger(const Logger&) = delete;
  Logger(Logger&&)      = default;
  Logger& operator=(const Logger&) = delete;
  Logger& operator=(Logger&&) = delete;

  /// @name Message logger
  /// @{
  template <typename... Args>
  void log(fmt::string_view format, const Args&... args) const {
    logImpl(Level::off, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void error(fmt::string_view format, const Args&... args) const {
    logImpl(Level::error, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void warning(fmt::string_view format, const Args&... args) const {
    logImpl(Level::warning, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void info(fmt::string_view format, const Args&... args) const {
    logImpl(Level::info, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void debug(fmt::string_view format, const Args&... args) const {
    logImpl(Level::debug, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void trace(fmt::string_view format, const Args&... args) const {
    logImpl(Level::trace, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void trace(Category category, fmt::string_view format, const Args&... args) const {
    logImpl(Level::trace, category, format, fmt::make_format_args(args...));
  }
  /// @}

  /// @name Message logger (rank 0)
  /// @{
  template <typename... Args>
  void onelog(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::off, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void oneerror(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::error, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void onewarning(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::warning, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void oneinfo(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::info, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void onedebug(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::debug, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void onetrace(fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::trace, format, fmt::make_format_args(args...));
  }

  template <typename... Args>
  void onetrace(Category category, fmt::string_view format, const Args&... args) const {
    if (rank_ == 0) logImpl(Level::trace, category, format, fmt::make_format_args(args...));
  }
  /// @}

  void flush() {
    sync();
    ostream_.flush();
  }

  void sync();
  void isync();

  /// @name Setup
  /// @{
  /**
   * Gets the logging level.
   *
   * @return The logging level.
   */
  Level getLevel() const { return level_; }

  /**
   * Sets the level.
   *
   * @param level  The level.
   */
  void setLevel(const Level level) { level_ = level; }

  const char* getLevelName() const;

  /**
   * Enable or disable the colored output.
   *
   * @param flag  The flag to indicate enable or disable.
   */
  void setColoredOutput(bool flag) { coloredOutput_ = flag; }

  /**
   * Gets the colored output.
   *
   * @return True, if colored output. False, else.
   */
  bool getColoredOutput() const { return coloredOutput_; }

  /**
   * Register a pointer to the time-step for using it in the output.
   *
   * @param timestep  The time-step.
   */
  void registerTimeStepPtr(unsigned long* timestep) { timestep_ = timestep; }
  /// @}

  /// @name Setup format
  /// @{
  /**
   * Sets the format string.
   * You can use six arg_ids in the format string:
   * arg_id | value
   * -------|--------------------
   * 0      | `level_color`
   * 1      | `short_level_names`
   * 2      | `rank`
   * 3      | `getTime()`
   * 4      | `time-step`
   * 5      | `color_reset`
   *
   * The initial formats are:
   * Level            | Default format
   * -----------------|--------------------------------
   * log/off          | "{0} *{5} {3} "
   * error            | "{0}{1:4} {2:5} "
   * warning          | "{0}{1:4} {2:5} "
   * info             | "{0}{1:4} {2:5} "
   * debug            | "{0}{1:4} {2:5} "
   * trace            | "{0}{1:4} {2:5} "
   * trace (category) | "{0}{1:4} {2:5} {3:4}, {4:5}: "
   *
   * @param level   The level.
   * @param format  The format.
   */
  void setFormat(Level level, fmt::string_view format) {
    level_format_[static_cast<unsigned int>(level)] = format;
  }

  /**
   * Sets the format for trace with category. See setFormat().
   *
   * @param format  The format.
   */
  void setFormatTraceCategory(fmt::string_view format) {
    level_format_[6] = format;
  }

  /**
   * Sets the format for info to trace without category. See setFormat().
   *
   * @param format  The format.
   */
  void setFormatAll(fmt::string_view format) {
    for (unsigned int i = 0; i < 6; i++) {
      level_format_[i] = format;
    }
  }

  void setFormatEnd(fmt::string_view format) {
    level_format_[7] = format;
  }

  void setEndString(const std::string& end) {
    endString_ = end;
  }
  /// @}

private:
  void initializeRank() {
    int initialized;
    int finalized;
    MPI_Initialized(&initialized);
    MPI_Finalized(&finalized);
    if (initialized && !finalized) {
      MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
      MPI_Comm_size(MPI_COMM_WORLD, &mpiSize_);
    }
  }

  std::ostream& initStream(int groupSize);

  inline void preamble(Level level, Category category) const;
  inline void preamble(Level level) const;

  static const char* lookupName(Category category);
  inline fmt::string_view lookupColor(int level) const;

  void logImpl(Level level, fmt::string_view format, fmt::format_args args) const;
  void logImpl(Level level, Category category, fmt::string_view format, fmt::format_args args) const;

  void send(int tag);
  void recv();
  void iprobe(int source, int tag);
  void recvLog(MPI_Status& status, int tag);
  void logLocalBuffer();

  /**
   * Level formats, 0 to 5 gives the log level. 6 is for the trace level with category. 7 is reserved for ending and
   * reset the color.
   *
   * Arg_id 0-5 are defined by: 0:level_color, 1:short_level_names, 2:rank, 3:getTime(), 4:timestep, 5:color_reset
   */
  std::array<fmt::string_view, 8> level_format_{
      "{0} *{5} {3} ",                  // log/off
      "{0}{1:4} {2:5} ",                // error
      "{0}{1:4} {2:5} ",                // warning
      "{0}{1:4} {2:5} ",                // info
      "{0}{1:4} {2:5} ",                // debug
      "{0}{1:4} {2:5} ",                // trace
      "{0}{1:4} {2:5} {3:4}, {4:5}: ",  // trace (category)
      "{}\n"                            // ending, only parameter 0:color_reset
  };

#ifdef NDEBUG
  Level level_{Level::info};  ///< The verbose level.
#else
  Level level_{Level::trace};  ///< The verbose level.
#endif

  unsigned int debugDetails_{Category::All & ~Category::CommExch};  ///< The group of details for the debug level.

  const unsigned long timestep0_{0};            ///< Value for default pointer.
  const unsigned long* timestep_{&timestep0_};  ///< Pointer to a time step.

  std::ostream& ostream_;        ///< The output stream.
  std::ofstream logFile_;        ///< Owns the file stream when `ostream_` points to a file.
  std::stringstream logStream_;  ///< Owns the string stream when `ostream_` points to a string stream.
  bool coloredOutput_;           ///< True when colors are used.
  std::string endString_{};      ///< Optional output at the end.

  int rank_{0};                                ///< The MPI rank.
  int mpiSize_{1};                             ///< The MPI size.
  int target_{0};                              ///< The target of the group.
  int groupSize_{0};                           ///< The group size.
  int tagOffset_{0};                           ///< The tag offset to have unique MPI tags for each collective logger.
  MPI_Request sendRequest_{MPI_REQUEST_NULL};  ///< The send request.
  std::string sendBuffer_;                     ///< Buffer for sending the log output.
  std::vector<char> recvBuffer_;               ///< Buffer for receiving the log output.
};

}  // namespace logger
}  // namespace nastja
