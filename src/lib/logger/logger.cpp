/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "logger_impl.h"
#include "lib/io/writerreader.h"
#include "lib/logger/registry.h"
#include "lib/simdata/mpidata.h"
#include <array>
#include <chrono>
#include <iomanip>
#include <ostream>
#include <sstream>

namespace nastja {
namespace logger {

/**
 * Use `logger::get()` to get a logger.
 *
 * @return The logger.
 */
Logger& get(const std::string& name /*= ""*/) {
  return logger::Registry::instance().get(name);
}

void closeAll() {
  logger::Registry::instance().closeAll();
}

void syncAll() {
  logger::Registry::instance().syncAll();
}

void isyncAll() {
  logger::Registry::instance().isyncAll();
}

/**
 * Register a new default logger.
 *
 * @param name     The logger name.
 * @param ostream  The output stream.
 *
 * @return The logger.
 */
Logger& registerLogger(const std::string& name /*= ""*/, std::ostream& ostream /*= std::cout*/) {
  return logger::Registry::instance().registerLogger(name, ostream);
}

/**
 * @overload
 *
 * @param name       The logger name.
 * @param filename   The filename.
 * @param groupSize  The collective output group size, default 0 for non collective output.
 *
 * @return The logger.
 */
Logger& registerLogger(const std::string& name, const std::string& filename, unsigned int groupSize /*= 0*/) {
  return logger::Registry::instance().registerLogger(name, filename, groupSize);
}

/**
 * Determines if a logger is registered.
 *
 * @param name  The logger name.
 *
 * @return True if has a logger with name, False otherwise.
 */
bool hasLogger(const std::string& name) {
  return logger::Registry::instance().hasLogger(name);
}

/**
 * Closes a logger.
 *
 * @param name  The logger name.
 */
void closeLogger(const std::string& name) {
  logger::Registry::instance().closeLogger(name);
}

}  // namespace logger
}  // namespace nastja
