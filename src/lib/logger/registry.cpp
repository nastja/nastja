/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "registry.h"
#include "lib/logger/logger_impl.h"

namespace nastja {
namespace logger {

/**
 * Synchronizes all loggers.
 */
void Registry::syncAll() {
  for (auto& logger : loggers_) {
    logger.second->sync();
  }
}

/**
 * Synchronizes all loggers in a non blocking way.
 */
void Registry::isyncAll() {
  for (auto& logger : loggers_) {
    logger.second->isync();
  }
}

}  // namespace logger
}  // namespace nastja
