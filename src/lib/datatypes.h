/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <limits>

namespace nastja {

#ifdef NASTJA_USE_ULONG_CELLID
using cellid_t = unsigned long;  ///< The cell ID type.
#else
using cellid_t = unsigned int;  ///< The cell ID type.
#endif

static_assert(std::numeric_limits<cellid_t>::is_specialized &&
                  std::numeric_limits<cellid_t>::is_integer &&
                  !std::numeric_limits<cellid_t>::is_signed,
              "cellid_t must be an unsigned integer type.");

#ifdef NASTJA_USE_SINGLE_FLOATPRECISION
using real_t = float;  ///< The floating point number type.
#else
using real_t   = double;        ///< The floating point number type.
#endif

static_assert(std::numeric_limits<real_t>::is_specialized &&
                  !std::numeric_limits<real_t>::is_integer,
              "real_t must be a float type.");
}  // namespace nastja
