/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/archive.h"
#include "lib/datatypes.h"
#include "lib/block/block.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/field/field.h"
#include "lib/math/nanoflannadapter.h"
#include "lib/math/surface.h"
#include "lib/math/volume.h"
#include "lib/storage/sparestorage.h"
#include "lib/storage/sparestoragecell.h"
#include "robin_hood.h"
#include <array>
#include <bitset>
#include <iostream>
#include <memory>
#include <vector>

namespace nastja {

/**
 * Package for division of cells.
 */
struct DivisionPack {
  void pack(Archive& ar) const {
    ar.pack(theta);
    ar.pack(phi);
    ar.pack(newID);
    ar.pack(newType);
  }

  void unpack(Archive& ar) {
    ar.unpack(theta);
    ar.unpack(phi);
    ar.unpack(newID);
    ar.unpack(newType);
  }

  real_t theta;
  real_t phi;
  cellid_t newID;
  int newType;
};

/**
 * Class for cell block container. Provides additional data for the cellular
 * Potts model for each block.
 *
 * @ingroup datacontainers cells
 */
class CellBlockContainer : public DataContainer {
public:
  explicit CellBlockContainer(block::Block* block);
  ~CellBlockContainer() override {
    MPI_Waitall(26, getSendRequest(), MPI_STATUSES_IGNORE);
  }

  CellBlockContainer(const CellBlockContainer&)     = delete;
  CellBlockContainer(CellBlockContainer&&) noexcept = default;
  CellBlockContainer& operator=(const CellBlockContainer&) = delete;
  CellBlockContainer& operator=(CellBlockContainer&&) = delete;

  Archive& getSendArchive(const unsigned int i = 0) { return sendPackedBuffer_[i]; }
  Archive& getRecvArchive() { return recvPackedBuffer_; }

  MPI_Request* getSendRequest(const int i = 0) { return &(sendRequest_[i]); }

  void packAll(Archive& ar);
  void packPartial(Archive& ar, stencil::Direction side, long range);
  void unpack(Archive& ar);

  void dump(Archive& ar) const override;
  void undump(Archive& ar) override;

  void postReceive();

  void findInactiveCells(const std::unordered_set<cellid_t>& activeCells);
  void activateCells(const std::unordered_set<cellid_t>& activeCells);

  SimData& getSimData() const { return block_->simdata; }
  block::Block* getBlock() const { return block_; }

  /**
   * Activates the nearest neighbor search functions.
   */
  void activateNearestNeighborSearch() { rebuildKdTree_ = true; }
  std::size_t periodicRadiusSearch(const math::Vector3<int>& point, long radiusSqr, std::vector<std::pair<cellid_t, int>>& result) const;

  SpareStorageCell cells{};  ///< The spare storage of cell data.

  SpareStorage<int> moD{};  ///< The spare storage of message of death.

  SpareStorage<int> typeChange{};  ///< The spare storage of message of type change.

  SpareStorage<std::array<long, 4>> centerOfMass{};  ///< Center of mass (x, y, z, number of voxels)

  SpareStorage<DivisionPack> division{};  ///< Division (theta*1e6, phi*1e6, newID)
  cellid_t reservedCellID{};              ///< The reserved cellID for the next new cell in this block.
  cellid_t reservedCellIDMax{};           ///< the maximal reserved cellID in this block.

  std::unique_ptr<math::Surface<>> surface;  ///< Surface calculation depending on the energy surface.
  std::unique_ptr<math::Volume<>> volume;    ///< Volume calculation depending on the energy volume.

private:
  using kdtree_t = math::KDTreeAdaptor<SpareStorageCell, int, 3, nanoflann::metric_L2_Simple, cellid_t>;  // TODO: DistanceType is only int could result in overflows.

  void removeCell(field::Field<cellid_t, 1>& field, cellid_t cellID) const;
  void unpackMoD(Archive& ar);
  void unpackTypeChange(Archive& ar);
  void unpackCoM(Archive& ar, unsigned int blockID);
  void unpackDiv(Archive& ar);
  void unpackCells(Archive& ar, unsigned int blockID);
  void unpackCell(cellid_t cellID, Cell& cell);

  void ensureCellSize();
  void ensureCellVolume();
  void postReceiveMoD();
  void postReceiveCoM();
  void postReceiveTypeChange();

#ifndef __FAST_MATH__
  robin_hood::unordered_map<unsigned int, robin_hood::unordered_map<cellid_t, Cell>> receivedCells_{};  ///< Map of received blocks and map of all received cells
#endif

  std::vector<cellid_t> inactiveCells_{};  ///< Cells that become inactive inside of a block.

  std::array<Archive, 26> sendPackedBuffer_{};  ///< The packed buffer archive for sending.
  Archive recvPackedBuffer_;                    ///< The packed buffer archive for receiving.

  std::array<MPI_Request, 26> sendRequest_{};

  SpareStorage<unsigned int> edgeList_;  ///< Map of cellIDs to edge array fields

  block::Block* block_;  ///< Pointer to the parent block.

  bool rebuildKdTree_{false};       ///< Flag for reindex the kdtree when the CoM is calculated.
  math::BoundingBox<long> domain_;  ///< The bounding box of the whole domain.
  std::bitset<26> isPeriodic_;      ///< The bitset of the 26 possible periodic representations, 1 for check, 0 ignore.
  kdtree_t kdtree_;
};

}  // namespace nastja
