/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/datacontainer/datacontainer.h"
#include <bitset>

namespace nastja {

/**
 * Class for the dynamic block data container. Provides, inter alia
 * border detection, living time, and volume change.
 *
 * @ingroup datacontainers
 */
struct DynamicBlockContainer : public DataContainer {
  explicit DynamicBlockContainer() {}
  ~DynamicBlockContainer() override                       = default;
  DynamicBlockContainer(const DynamicBlockContainer&)     = delete;
  DynamicBlockContainer(DynamicBlockContainer&&) noexcept = default;
  DynamicBlockContainer& operator=(const DynamicBlockContainer&) = delete;
  DynamicBlockContainer& operator=(DynamicBlockContainer&&) = delete;

  void setEmpty() { boundaryDetection.set(0); }
  void setFull() {
    boundaryDetection.set(0);
    boundaryDetection.set(1);
  }

  void dump(Archive& ar) const override { ar.pack(ttl); }
  void undump(Archive& ar) override { ar.unpack(ttl); }

  std::bitset<54> boundaryDetection{0};  ///< Information about neighbor creation or deletion.
                                         ///< A pair of 2 bits is used for each information, the first bit denote the information, the second bit denote if it is an full or empty block
                                         ///< - Bit 0: delete
                                         ///< - Bit 1: when set a filled block is deleted
                                         ///< - Bit 2, 4, 6, 8, 10 and 12: new block on this boundary (L, R, D, U, B, F)
                                         ///< - Bit 3, 5, 7, 9, 11 and 13: when set a filled block is needed

  real_t addedVolume{0};  ///< holds the volume from new created full blocks.
  int ttl{10};            ///< counter for minimum living time
};

}  // namespace nastja
