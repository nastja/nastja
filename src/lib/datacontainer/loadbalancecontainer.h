/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datacontainer/datacontainer.h"

namespace nastja {

/**
 * Class for the load balancing data container. Provides the target.
 *
 * @ingroup datacontainers
 */
struct LoadBalanceContainer : public DataContainer {
  explicit LoadBalanceContainer() {}
  ~LoadBalanceContainer() override                      = default;
  LoadBalanceContainer(const LoadBalanceContainer&)     = delete;
  LoadBalanceContainer(LoadBalanceContainer&&) noexcept = default;
  LoadBalanceContainer& operator=(const LoadBalanceContainer&) = delete;
  LoadBalanceContainer& operator=(LoadBalanceContainer&&) = delete;

  void dump(Archive& /*ar*/) const override {}
  void undump(Archive& /*ar*/) override {}

  int target{-1};  ///< Rank of the target where this block will be moved to.
};

}  // namespace nastja
