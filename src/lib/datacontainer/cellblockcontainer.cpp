/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellblockcontainer.h"
#include "lib/cell.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/config/config.h"
#include "lib/field/fieldproperties.h"
#include "lib/math/arithmetic.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <stdexcept>

namespace nastja {

using namespace nastja::config::literals;

CellBlockContainer::CellBlockContainer(block::Block* block)
    : block_{block},
      domain_{math::Vector3<long>::zero, static_cast<math::Vector3<long>>(block->size * block->blocks)},
      isPeriodic_{block->getPeriodicSides()},
      kdtree_{3, cells} {
  // kdtree_.rebuildIndex();

  sendRequest_.fill(MPI_REQUEST_NULL);
}

/**
 * Packs the cell block data.
 *
 * This are the surface, volume and the delta surface and volume.
 *
 * @param ar  Reference to the archive.
 */
void CellBlockContainer::packAll(Archive& ar) {
  ar.pack(block_->id);
  ar.pack(moD);
  ar.pack(typeChange);
  ar.pack(centerOfMass);
  ar.pack(division);
  ar.pack(cells);
}

/**
 * Packs all cell block data within a range per side.
 *
 * @param ar     The archive.
 * @param side   The side.
 * @param range  The range.
 */
void CellBlockContainer::packPartial(Archive& ar, stencil::Direction side, long range) {
  auto& field = *block_->getFieldByName("cells");

  auto domain = math::BoundingBox<int>{math::Vector3<int>::zero, static_cast<math::Vector3<int>>(block_->blocks * block_->size)};
  auto offset = static_cast<math::Vector3<int>>(field.getOffset()) - static_cast<math::Vector3<int>>(field.getBoundarySize());

  auto bBox = static_cast<math::BoundingBox<int>>(field.getHaloBoundingBox(side, field::HaloWay::both));
  bBox.translate(offset);
  bBox.extend(range - 1);

  auto lambda = [&cells = cells, &domain, &bBox](cellid_t cell) {
    const auto& center = cells(cell).center;
    if (center == math::Vector3<int>{-1, -1, -1}) return false;
    return bBox.isInsidePeriodic(domain, center);
  };

  ar.pack(block_->id);
  ar.pack(moD);
  typeChange.pack(ar, lambda);
  centerOfMass.pack(ar, lambda);
  division.pack(ar, lambda);
  cells.pack(ar, lambda);
}

/**
 * Unpacks the cell block data.
 *
 * Read the delta surface and volume and add this to surface or volume if the cell is active in this block.
 *
 * @param ar  Reference to the archive.
 */
void CellBlockContainer::unpack(Archive& ar) {
  unsigned int blockID{};
  ar.unpack(blockID);
  unpackMoD(ar);
  unpackTypeChange(ar);
  unpackCoM(ar, blockID);
  unpackDiv(ar);
  unpackCells(ar, blockID);
}

/**
 * Packs the cell block data.
 *
 * This are the surface, volume and the delta surface and volume.
 *
 * @param ar  Reference to the archive.
 */
void CellBlockContainer::dump(Archive& ar) const {
  ar.dumpObj(cells);
  ar.pack(reservedCellID);
  ar.pack(reservedCellIDMax);
}

/**
 * Unpacks the cell block data.
 *
 * Read the delta surface and volume and add this to surface or volume if the cell is active in this block.
 *
 * @param ar  Reference to the archive.
 */
void CellBlockContainer::undump(Archive& ar) {
  ar.undumpObj(cells);
  ar.unpack(reservedCellID);
  ar.unpack(reservedCellIDMax);
}

/**
 * Unpacks a single cell.
 *
 * @param cellID   The cellID.
 * @param cell  The cell.
 */
void CellBlockContainer::unpackCell(cellid_t cellID, Cell& cell) {
  auto it = cells.find(cellID);
  if (it == cells.end()) return;

  auto& c = it->second;
  if (c.type == -1 && cell.type != -1) {
    // Set new cell in block
    c.set(cell);
  }

  // Compare volumes and surfaces, and check whether motilityDirection needs to be updated
  c.addDeltas(cell);
}

/**
 * Adds delta values from the archive to the received cells container.
 * This is done because the order of received blocks is not conserved.
 *
 * If the cell is active but the stored value is zero it read the real values.
 *
 * @param ar       Reference to the archive.
 * @param blockID  The block ID.
 */
void CellBlockContainer::unpackCells(Archive& ar, unsigned int blockID) {
  unsigned int size{};

  unsigned int edgeArray = block_->getNeighborDirection(block_->id, blockID);

  ar.unpack(size);
  for (unsigned int i = 0; i < size; i++) {
    cellid_t cellID{};
    ar.unpack(cellID);

#ifdef __FAST_MATH__
    Cell receivedCell;
    ar.unpack(receivedCell);
    unpackCell(cellID, receivedCell);
#else
    ar.unpack(receivedCells_[blockID][cellID]);

    if (!cells.contains(cellID)) {
      receivedCells_[blockID].erase(cellID);
      continue;
    }
#endif

    // This part adds the corresponding edge array to the map via an or
    edgeList_[cellID] |= edgeArray;
  }
}

/**
 * Calls post receive actions.
 *
 * Then unpack the cell values from the received cell container and add them to the storage values:
 * * If the cell is active but not initialized the main value is set.
 * * If cell is active and initialized the deltas are added.
 *
 * All this is done ordered by block ID from which it was received, this ensures predifined addition order and no
 * numerical divergences at float additions.
 */
void CellBlockContainer::postReceive() {
#ifndef __FAST_MATH__
  // Ordered unpacking (by BlockID) of the cells to avoid float rounding errors
  for (auto& recvBlock : receivedCells_) {
    for (auto& recvCell : recvBlock.second) {
      unpackCell(recvCell.first, recvCell.second);
    }
  }
  receivedCells_.clear();
#endif

  // Perform post receive actions
  postReceiveCoM();
  postReceiveMoD();
  postReceiveTypeChange();
  ensureCellSize();
  ensureCellVolume();

  // Delete cells that are not in block
  if (getSimData().timestep > 0) {
    for (const auto& cellID : inactiveCells_) {
      // logger::get().info("Deleted cell {}, in a block, t = {}", cellID, getSimData().timestep);
      cells.erase(cellID);
    }
  }

  // When CoM was send and a kd-tree is needed then build it now after removing the cells outside this block.
  if (!centerOfMass.empty() && rebuildKdTree_) kdtree_.rebuildIndex();

  // Clear message storage
  centerOfMass.clear();
  typeChange.clear();
}

/**
 * Ensures cell size, so that a cell is not overlapping opposite ends of a block (i.e., cell is an invalid
 * configuration).
 *
 * Checks the received information for a particular cell. If the information comes from the upper and lower side a
 * message of death (MoD) is send.
 */
void CellBlockContainer::ensureCellSize() {
  auto& data = getSimData().getData<DataCells>();

  for (auto& pos : edgeList_) {
    cellid_t cellID             = pos.first;
    unsigned int edgeArrayCheck = pos.second;

    if (cellID <= data.liquid) continue;

    const auto& blockSize = block_->blocks;

    if (((blockSize[0] > 3 && (edgeArrayCheck & 0b000011u) == 0b000011u) ||
         (blockSize[1] > 3 && (edgeArrayCheck & 0b001100u) == 0b001100u) ||
         (blockSize[2] > 3 && (edgeArrayCheck & 0b110000u) == 0b110000u))) {
      if (moD.contains(cellID)) continue;
      moD[cellID] = 3;
      logger::get("cellevents").info(R"("block": {}, "event": "MoD", "cell": {}, "info": "ensureCellSize")", block_->id, cellID);
    }
  }

  edgeList_.clear();
}

/**
 * Ensures cell volume, invokes a MoD for cells that are within that block and V < 0.
 */
void CellBlockContainer::ensureCellVolume() {
  auto& data = getSimData().getData<DataCells>();

  for (const auto& i : cells) {
    cellid_t cellID    = i.first;
    const auto& cell   = i.second;
    const auto& center = cell.center;

    if (!(block_->isGlobalCoordinateInBlock(center[0], center[1], center[2]))) continue;
    if (moD.contains(cellID)) continue;

    if (cell.volume < 0 && cellID > data.liquid) {
      moD[cellID] = 3;
      logger::get("cellevents").info(R"("block": {}, "event": "MoD", "cell": {}, "info": "volume < 0")", block_->id, cellID);
    }
    else if (cell.surface < 0 && cellID > data.liquid) {
      moD[cellID] = 3;
      logger::get("cellevents").info(R"("block": {}, "event": "MoD", "cell": {}, "info": "surface < 0")", block_->id, cellID);
    }
  }
}

/**
 * Post receive methode for MoD. Reduce value by one and erase cells that reaches 0.
 */
void CellBlockContainer::postReceiveMoD() {
  auto& field = block_->getFieldByName("cells")->get<cellid_t>();
  for (auto& i : moD) {
    auto cellID = i.first;
    auto value  = i.second;
    if (value == 1) {
      removeCell(field, cellID);
      cells.erase(cellID);
    }
    moD[cellID] = value - 1;
  }

  moD.shrinkStorage();
}

/**
 * Center of Mass (CoM) post receive action.
 *
 * Calculates the the center of mass from the sum of parts from all blocks.
 */
void CellBlockContainer::postReceiveCoM() {
  if (centerOfMass.empty()) return;

  for (const auto& cell : centerOfMass) {
    auto it = cells.find(cell.first);
    if (it == cells.end()) continue;

    auto& center     = it->second.center;
    auto& centerReal = it->second.centerReal;
    for (int i = 0; i < 3; i++) {
      center[i]     = static_cast<int>(math::divRoundHalfUp(cell.second[i], cell.second[3]));
      centerReal[i] = static_cast<real_t>(cell.second[i]) / static_cast<real_t>(cell.second[3]);

      int numberOfVoxel = static_cast<int>(block_->size[i] * block_->blocks[i]);
      if (center[i] < 0) {
        center[i] += numberOfVoxel;
        centerReal[i] += numberOfVoxel;
      }
      if (center[i] >= numberOfVoxel) {
        center[i] -= numberOfVoxel;
        centerReal[i] -= numberOfVoxel;
      }
    }
  }
}

void CellBlockContainer::postReceiveTypeChange() {
  for (const auto& cell : typeChange) {
    auto it = cells.find(cell.first);
    if (it == cells.end()) continue;

    it->second.type = cell.second;
  }
}

/**
 * Sweep through field and delete cell with cellID
 *
 * @param field   pointer to field
 * @param cellID  cellID for deletion
 */
void CellBlockContainer::removeCell(field::Field<cellid_t, 1>& field, cellid_t cellID) const {
  auto& container = block_->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();

  auto view = field.createView(field.getOuterBoundingBox());
  for (auto* cellPtr : view) {
    cellid_t& voxel = view.get(cellPtr);

    if (voxel == cellID) {
      voxel = data.liquid;
    }
  }

  if (container.cells.contains(cellID)) container.cells(cellID).voxelsInBlock = 0;
}

/**
 * Unpacks MoD-messages.
 *
 * @param ar  The archive.
 */
void CellBlockContainer::unpackMoD(Archive& ar) {
  unsigned int size{};
  ar.unpack(size);
  for (unsigned int i = 0; i < size; i++) {
    cellid_t cellID{};
    int value{};
    ar.unpack(cellID);
    ar.unpack(value);

    moD[cellID] = value;
  }
}

/**
 * Unpacks cell type changes.
 *
 * @param ar  The archive.
 */
void CellBlockContainer::unpackTypeChange(Archive& ar) {
  unsigned int size{};
  ar.unpack(size);
  for (unsigned int i = 0; i < size; i++) {
    cellid_t cellID{};
    int newType{};
    ar.unpack(cellID);
    ar.unpack(newType);

    if (!cells.contains(cellID)) continue;

    typeChange[cellID] = newType;
  }
}

/**
 * Adds values from the archive to the storage array if the cell is active.
 *
 * Preserves if periodic.
 *
 * @param ar       Reference to the archive.
 * @param blockID  The block id.
 */
void CellBlockContainer::unpackCoM(Archive& ar, unsigned int blockID) {
  unsigned int size{};

  unsigned int periodic = block_->getNeighborDirection(block_->id, blockID);

  // Unpacking the value
  ar.unpack(size);
  std::array<long, 4> array{};
  for (unsigned int i = 0; i < size; i++) {
    cellid_t cellID{};
    ar.unpack(cellID);
    ar.unpack(array);

    if (!cells.contains(cellID)) continue;

    for (unsigned int dim = 0; dim < 3; dim++) {
      long cells = static_cast<long>(block_->size[dim] * block_->blocks[dim]);

      if (block_->blocks[dim] == 2) {
        if (block_->getCoordinatesFromID(block_->id)[dim] != block_->getCoordinatesFromID(blockID)[dim]) {
          unsigned int blockCenter = math::divRoundClosest(array[dim], array[3]);  // center of mass for the block fraction in global coordinates in i-direction.
          if (block_->getCoordinatesFromID(blockID)[dim] == 1) {                   // the received block is the upper block
            if (blockCenter - block_->size[dim] > block_->size[dim] / 2) {         // use periodic when center is in the upper half of the block
              array[dim] -= cells * array[3];
            }
          } else {                                      // the received block is the lower block
            if (blockCenter < block_->size[dim] / 2) {  // use periodic when center is in the lower half of the block
              array[dim] += cells * array[3];
            }
          }
        }
      } else {
        if ((periodic & (0b1u << (6 + dim))) != 0) {   // periodic bit is set
          if ((periodic & 0b10u << (2 * dim)) != 0) {  // upper side
            array[dim] += cells * array[3];
          } else {  // lower side
            array[dim] -= cells * array[3];
          }
        }
      }
    }

    centerOfMass[cellID] += array;
  }
}

/**
 * Adds values from the archive to the storage array if the cell is active.
 *
 * @param ar  Reference to the archive.
 */
void CellBlockContainer::unpackDiv(Archive& ar) {
  unsigned int size{};
  // Unpacking the value
  ar.unpack(size);
  DivisionPack array{};
  for (unsigned int i = 0; i < size; i++) {
    cellid_t cellID{};
    ar.unpack(cellID);
    ar.unpack(array);
    if (!cells.contains(cellID)) continue;

    division[cellID] = array;
  }
}

/**
 * Finds inactive cells.
 *
 * Finds all cells that have no voxel in the block. Substracts all activeCells from these.
 *
 * @param activeCells  The active cells.
 */
void CellBlockContainer::findInactiveCells(const std::unordered_set<cellid_t>& activeCells) {
  auto& data = getSimData().getData<DataCells>();

  std::unordered_set<cellid_t> inactive{};

  for (const auto& i : cells) {
    cellid_t cellID  = i.first;
    const auto& cell = i.second;

    if (cell.voxelsInBlock > 0 || cellID <= data.liquid) continue;

    inactive.insert(cellID);
  }

  inactiveCells_.clear();
  std::copy_if(inactive.begin(), inactive.end(), std::back_inserter(inactiveCells_), [&activeCells](cellid_t id) { return activeCells.count(id) == 0; });
}

/**
 * Activates cells that are given by activeCells.
 *
 * @param activeCells  The active cells.
 */
void CellBlockContainer::activateCells(const std::unordered_set<cellid_t>& activeCells) {
  for (const auto& i : activeCells) {
    cells[i];
  }
}

/**
 * Searches the neighbors arround point within a radius considering periodicity.
 *
 * @param point      The point.
 * @param radiusSqr  The squared radius.
 * @param result     The result.
 *
 * @return Number of neighbors within the given radius.
 */
std::size_t CellBlockContainer::periodicRadiusSearch(const math::Vector3<int>& point, long radiusSqr, std::vector<std::pair<cellid_t, int>>& result) const {
  FunctionTimer ft("KDtree-radiusSearch");
  if (!rebuildKdTree_) throw std::runtime_error("Use activateNearestNeighborSearch() to enable nearest neighbors search");

  kdtree_.radiusSearch(point.data(), radiusSqr, result);

  auto distanceToPeriodicBorder = math::borderDistancesSqr(domain_, static_cast<math::Vector3<long>>(point));
  for (auto i : stencil::D3C26()) {
    if (!isPeriodic_[i] || (distanceToPeriodicBorder[i] > radiusSqr)) continue;
    std::vector<std::pair<cellid_t, int>> tmpResult;
    // shift point in opposite direction
    auto currentPoint = point + static_cast<math::Vector3<int>>(stencil::D3C26::dir[i] * domain_.upper());
    kdtree_.radiusSearch(currentPoint.data(), radiusSqr, tmpResult);
    result.resize(result.size() + tmpResult.size());
    result.insert(std::end(result), std::begin(tmpResult), std::end(tmpResult));
  }

  return result.size();
}

}  // namespace nastja
