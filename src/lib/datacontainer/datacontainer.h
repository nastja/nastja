/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"

namespace nastja {

/**
 * @defgroup datacontainers Data container
 *
 * Provides data containers to extend the variable for fields and blocks when
 * this is needed by a special simulation method.
 */

struct DataContainer {
  explicit DataContainer()                = default;
  virtual ~DataContainer()                = default;
  DataContainer(const DataContainer&)     = delete;
  DataContainer(DataContainer&&) noexcept = default;
  DataContainer& operator=(const DataContainer&) = delete;
  DataContainer& operator=(DataContainer&&) = delete;

  virtual void dump(Archive& ar) const = 0;
  virtual void undump(Archive& ar)     = 0;
};

}  // namespace nastja
