/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/pool.h"
#include "lib/datacontainer/datacontainer.h"
#include <memory>

namespace nastja {

class DataContainerPool {
public:
  explicit DataContainerPool()                    = default;
  virtual ~DataContainerPool()                    = default;
  DataContainerPool(const DataContainerPool&)     = delete;
  DataContainerPool(DataContainerPool&&) noexcept = default;
  DataContainerPool& operator=(const DataContainerPool&) = delete;
  DataContainerPool& operator=(DataContainerPool&&) = delete;

  /**
   * Gets the data container.
   *
   * @tparam Container  The data container type.
   *
   * @return Reference to the appended data container.
   */
  template <typename Container, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  Container& getDataContainer() {
    if (!dataContainerPool.has<Container>()) {
      auto type = std::type_index(typeid(Container));
      throw std::runtime_error(fmt::format("The DataContainer '{}' is not initialized. Use initData() before getData().", type.name()));
    }

    return dataContainerPool.get<Container>();
  }

  /**
   * Determines if the data container is initialized.
   *
   * @tparam Container  The data container type.
   *
   * @return True if the data container is initialized, False otherwise.
   */
  template <typename Container, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  bool hasDataContainer() const {
    return dataContainerPool.has<Container>();
  }

  /**
   * Initializes the data.
   *
   * @param args  The packed arguments.
   *
   * @tparam Container  The data container type.
   * @tparam ARGs       The parameter pack.
   *
   * @return Reference to the appended data container.
   */
  template <typename Container, typename... ARGs, typename = std::enable_if_t<std::is_base_of<DataContainer, Container>::value>>
  Container& registerDataContainer(ARGs&&... args) {
    return dataContainerPool.init<Container>(std::forward<ARGs>(args)...);
  }

  auto& getDataContainerPool() { return dataContainerPool; }

protected:
  ExtensionStorage<DataContainer> dataContainerPool;  ///< The pool holding the data containers.
};

}  // namespace nastja
