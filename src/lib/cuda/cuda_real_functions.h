/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/cuda/cuda_structs.h"
#include "lib/stencil/direction.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

namespace nastja {

// defines a cuda type containing either 3 floats or 3 doubles
#ifdef NASTJA_USE_SINGLE_FLOATPRECISION
using real3_t = float3;
#else
using real3_t = double3;
#endif

/// overloaded pow function for float
__device__ __inline__ float real_pow(float a, float b) { return powf(a, b); }

/// overloaded pow function for double
__device__ __inline__ double real_pow(double a, double b) { return pow(a, b); }

/// overloaded sqrt function for float
__device__ __inline__ float real_sqrt(float a) { return sqrtf(a); }

/// overloaded sqrt function for double
__device__ __inline__ double real_sqrt(double a) { return sqrt(a); }

/// overloaded log function for float
__device__ __inline__ float real_log(float a) { return logf(a); }

/// overloaded log function for double
__device__ __inline__ double real_log(double a) { return log(a); }

/// overloaded min function for float
__device__ __inline__ float real_min(float a, float b) { return fminf(a, b); }

/// overloaded min function for double
__device__ __inline__ double real_min(double a, double b) { return fmin(a, b); }

/// overloaded max function for float
__device__ __inline__ float real_max(float a, float b) { return fmaxf(a, b); }

/// overloaded max function for double
__device__ __inline__ double real_max(double a, double b) { return fmax(a, b); }

/// overloaded make_real3 function for float
__inline__ __device__ float3 make_real3(float a, float b, float c) { return make_float3(a, b, c); }

/// overloaded make_real3 function for double
__inline__ __device__ double3 make_real3(double a, double b, double c) { return make_double3(a, b, c); }

}  // namespace nastja
