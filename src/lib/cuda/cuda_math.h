/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <array>
#include <cfloat>
#include <cmath>
#include <cuda.h>
#include <cuda_runtime.h>

namespace nastja {
namespace cuda {

__device__ inline real_t interpolate_byPhi(const real_t* array, const real_t phi) {
  return array[0] * phi + array[1] * (real_t(1.0) - phi);
}

__device__ inline real_t h(const real_t phi) {
  real_t phi2 = phi * phi;
  return phi2 * phi * (real_t(6.0) * phi2 - real_t(15.0) * phi + real_t(10.0));
}

__device__ inline real_t dhdphi(const real_t phi) {
  real_t phi2 = phi * phi;
  return real_t(30.0) * phi2 * (phi2 - real_t(2.0) * phi + real_t(1.0));
}

/**
 * checks for equality of two float numbers in cuda device code
 *
 * @param a  First float to compare
 * @param b  Second float to compare
 *
 * @return 1 if a and b are nearly equal considering FLT_EPSILON
 */
__device__ inline bool isNearlyEqual(float a, float b) {
  return (fabsf(a - b) <= FLT_EPSILON);
}

/**
 * checks for equality of two double numbers in cuda device code
 *
 * @param a  First double to compare
 * @param b  Second double to compare
 *
 * @return 1 if a and b are nearly equal considering DBL_EPSILON
 */
__device__ inline bool isNearlyEqual(double a, double b) {
  return (fabs(a - b) <= DBL_EPSILON);
}

/**
 * find the middle of two real_t numbers
 *
 * @param a  First number
 * @param b  Second number
 *
 * @return real_t interpolation of a and b
 */
__device__ inline real_t interpolate(const real_t a, const real_t b) {
  return real_t(0.5) * (a + b);
}

__device__ inline real_t diffu(const real_t center, const real_t upper, real_t dxr) {
  return (upper - center) * dxr;
}

__device__ inline real_t diffl(const real_t center, const real_t lower, real_t dxr) {
  return (center - lower) * dxr;
}

__device__ inline real_t diffc(const real_t lower, const real_t upper, real_t dxr) {
  return real_t(0.5) * (upper - lower) * dxr;
}

__device__ inline real_t calcDerivation(real_t* data, std::size_t size, std::size_t order, real_t value) {
  real_t sum = real_t(0.0);

  for (std::size_t i = size - order - 1; i <= size; i--) {
    std::size_t d = 1;
    for (std::size_t j = (i + 1); j <= i + order; j++) {
      d *= j;
    }
    sum = (sum * value) + real_t(d) * data[i + order];
  }

  return sum;
}

__device__ inline real_t calc(real_t* data, std::size_t size, real_t value) {
  return calcDerivation(data, size, 0, value);
}
__device__ inline real_t calcIntegral(real_t* data, std::size_t size, real_t value) {
  real_t sum = real_t(0.0);

  for (std::size_t i = size - 1; i <= size; i--) {
    sum = (sum + data[i] / real_t(i + 1)) * value;
  }

  return sum;
}

}  // namespace cuda
}  // namespace nastja
