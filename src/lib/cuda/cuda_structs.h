/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include <cuda_runtime.h>
#include <lib/math/vector.h>

namespace nastja {

#define gpuErrchk(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }

/**
 * Function to stop the code after a GPU error occurs, and print some information about the error
 *
 * Should be used in combination with the defined gpuErrchk function makro
 * to include file and line information in the error message
 */
inline void gpuAssert(cudaError_t code, const char* file, int line, bool abort = true) {
  if (code != cudaSuccess) {
    logger::get().info("gpu error: {} {} {} ", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

/**
 * small version of SimData to pass to a kernel.
 * this class contains the configurations needed inside of a kernel
 */
struct CudaSimData {
  CudaSimData(){};
  CudaSimData(real_t dx, real_t dt) : deltax{dx}, deltat{dt}, deltax_R{static_cast<real_t>(1.0) / dx} {}
  real_t deltax{0};
  real_t deltat{0};
  real_t deltax_R{0};
  real_t epsilon{0};
};

/**
 * Packs 2 pointers to a data and an exchange array on the GPU
 * with this we can simplify the parameter passing of data pointers to the gpu
 */
template <typename T>
struct CudaGpuFieldPtrs {
  T* data{nullptr};
  T* exchange{nullptr};
};

/**
 * The CudaArguments struct contains informations about the field that are needed inside a kernel.
 * It is filled after initialization of a field, and should be passed to every kernel which is executed.
 */
struct CudaArguments {
  unsigned long sizeX         = 0;
  unsigned long sizeY         = 0;
  unsigned long sizeZ         = 0;
  unsigned long boundarySizeX = 0;
  unsigned long boundarySizeY = 0;
  unsigned long boundarySizeZ = 0;
  unsigned long strideX       = 0;
  unsigned long strideY       = 0;
  unsigned long strideZ       = 0;

  unsigned long startX = 0;
  unsigned long startY = 0;
  unsigned long startZ = 0;

  bool useExchangeOnBoundary[27];

  void setSize(math::Vector3<unsigned long> a) {
    sizeX = a.x();
    sizeY = a.y();
    sizeZ = a.z();
  }
  void setBoundarySize(math::Vector3<unsigned long> a) {
    boundarySizeX = a.x();
    boundarySizeY = a.y();
    boundarySizeZ = a.z();
  }

  void setStride(math::Vector3<unsigned long> a) {
    strideX = a.x();
    strideY = a.y();
    strideZ = a.z();
  }
};

}  // namespace nastja
