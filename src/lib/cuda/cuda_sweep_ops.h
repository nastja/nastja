/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/utility.h"
#include "lib/cuda/cuda_structs.h"
#include "lib/math/arithmetic.h"
#include "lib/math/vector.h"
#include "lib/stencil/direction.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <type_traits>
#include <utility>

namespace nastja {
namespace cuda {

#ifndef __CUDACC_EXTENDED_LAMBDA__
#error "please compile with --expt-extended-lambda"
#endif
#ifndef __CUDACC_RELAXED_CONSTEXPR__
#error "please compile with --expt-relaxed-constexpr"
#endif

constexpr int cudaBlockSizeX = 32;
constexpr int cudaBlockSizeY = 4;
constexpr int cudaBlockSizeZ = 1;

using namespace stencil;

/**
 * Called by staticFor to call the function with all template parameters from the index_sequence.
 *
 * @param f  The host function.
 *
 * @tparam Is  Contains the sequence of all numbers to be used as a template paramter.
 * @tparam F   Lambda function type.
 */
template <std::size_t... Is, typename F>
void staticFor(std::index_sequence<Is...>, F&& f) {
  // with c++17 we could use
  // (f(std::integral_constant<std::size_t, Is>()),...);
  // workaround for c++11/14
  using empty = int[];
  (void)empty{0, ((void)(f(std::integral_constant<std::size_t, Is>())), 0)...};
}

/**
 * Called by staticFor to call the function with all template parameters from the index_sequence.
 *
 * @param f  The device function.
 *
 * @tparam Is  Contains the sequence of all numbers to be used as a template paramter.
 * @tparam F   Passed lambda function type.
 */
template <std::size_t... Is, typename F>
__device__ void staticForDevice(std::index_sequence<Is...>, F&& f) {
  using empty = int[];
  (void)empty{0, ((void)(f(std::integral_constant<std::size_t, Is>())), 0)...};
}

/**
 * Static iterate over multiple instanciations [0, N[ of a templated host function.
 *
 * @param f  The host function.
 *
 * @tparam N  Upper boundary.
 * @tparam F  Passed lambda function type.
 */
template <std::size_t N, typename F>
void staticFor(F&& f) {
  staticFor(std::make_index_sequence<N>(), std::forward<F>(f));
}

/**
 * Static iterate over multiple instanciations [0, N[ of a templated device function.
 *
 * @param f  The host function.
 *
 * @tparam N  Upper boundary.
 * @tparam F  Passed lambda function type.
 */
template <std::size_t N, typename F>
__device__ void staticForDevice(F&& f) {
  staticForDevice(std::make_index_sequence<N>(), std::forward<F>(f));
}

template <std::size_t N, std::size_t... Seq>
constexpr std::index_sequence<N + Seq...> add(std::index_sequence<Seq...>) { return {}; }

template <std::size_t Min, std::size_t Max>
using make_index_range = decltype(add<Min>(std::make_index_sequence<Max - Min>()));

/**
 * Iterate over a range multiple instanciations [Min, Max[ of a templated host function.
 *
 * @param f  The lambda function to call.
 *
 * @tparam Min  First number in the range.
 * @tparam Max  Last number in the range (excluded).
 * @tparam F    Automatically typed information about the passed function.
 */
template <std::size_t Min, std::size_t Max, typename F>
void staticFor(F&& f) {
  staticFor(make_index_range<Min, Max>(), std::forward<F>(f));
}

/**
 * Iterate over a range multiple instanciations [Min, Max[ of a templated device function.
 *
 * @param f  The lambda function to call.
 *
 * @tparam Min  First number in the range.
 * @tparam Max  Last number in the range (excluded).
 * @tparam F    Automatically typed information about the passed function.
 */
template <std::size_t Min, std::size_t Max, typename F>
__device__ void staticForDevice(F&& f) {
  staticForDevice(make_index_range<Min, Max>(), std::forward<F>(f));
}

/**
 * Checks if the currently executed thread is inside of the field boundaries
 *
 * @param x     Current x value of the thread
 * @param y     Current y value of the thread
 * @param z     Current z value of the thread
 * @param args  CudaArguments with informations about the field boundaries
 *
 * @tparam TDir  Defines which kernel is currently used
 */
template <Direction TDir>
__inline__ __device__ bool checkLimits(unsigned long x, unsigned long y, unsigned long z, const CudaArguments& args) {
  bool i;
  switch (TDir) {
    case Direction::C:
      i = (y < (args.sizeY - args.boundarySizeY - 1) && z < (args.sizeZ - args.boundarySizeZ - 1));
      break;
    case Direction::L:
      i = (y < (args.sizeY - args.boundarySizeY - 1) && z < (args.sizeZ - args.boundarySizeZ - 1));
      break;
    case Direction::R:
      i = (x < (args.sizeX - args.boundarySizeX)) && (y < (args.sizeY - args.boundarySizeY - 1) && z < (args.sizeZ - args.boundarySizeZ - 1));
      break;
    case Direction::D:
    case Direction::U:
      i = (x < (args.sizeX - args.boundarySizeX - 1) && z < (args.sizeZ - args.boundarySizeZ - 1));
      break;
    case Direction::B:
    case Direction::F:
      i = (x < (args.sizeX - args.boundarySizeX - 1) && y < (args.sizeY - args.boundarySizeY - 1));
      break;
    case Direction::DL:
    case Direction::DR:
    case Direction::UL:
    case Direction::UR:
      i = (z < (args.sizeZ - args.boundarySizeZ - 1));
      break;
    case Direction::BL:
    case Direction::BR:
    case Direction::FL:
    case Direction::FR:
      i = (y < (args.sizeY - args.boundarySizeY - 1));
      break;
    case Direction::BD:
    case Direction::BU:
    case Direction::FD:
    case Direction::FU:
      i = (x < (args.sizeX - args.boundarySizeX - 1));
      break;
    default:  // all edges
      i = true;
      break;
  }
  return i;
}

/**
 * get the needed offset in the exchange array for a directional kernel
 *
 * @param args  CudaArguments with informations about the field
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The offset.
 */
template <Direction TDir>
__inline__ __device__ int getOffset(CudaArguments args) {
  // TODO add dimensions for border larger then 1

  int innerSizeX = args.sizeX - 2 * args.boundarySizeX;
  int innerSizeY = args.sizeY - 2 * args.boundarySizeY;
  int innerSizeZ = args.sizeZ - 2 * args.boundarySizeZ;

  unsigned long a = 2 * innerSizeY * innerSizeZ + 2 * innerSizeX * innerSizeZ + 2 * innerSizeX * innerSizeY;
  unsigned long b = a + 4 * innerSizeX + 4 * innerSizeY + 4 * innerSizeZ;

  if (TDir == Direction::L) return 0;
  if (TDir == Direction::R) return innerSizeY * innerSizeZ;
  if (TDir == Direction::D) return 2 * innerSizeY * innerSizeZ;
  if (TDir == Direction::U) return 2 * innerSizeY * innerSizeZ + innerSizeX * innerSizeZ;
  if (TDir == Direction::B) return 2 * innerSizeY * innerSizeZ + 2 * innerSizeX * innerSizeZ;
  if (TDir == Direction::F) return 2 * innerSizeY * innerSizeZ + 2 * innerSizeX * innerSizeZ + innerSizeX * innerSizeY;

  if (TDir == Direction::DL) return a;
  if (TDir == Direction::DR) return a + innerSizeZ;
  if (TDir == Direction::UL) return a + 2 * innerSizeZ;
  if (TDir == Direction::UR) return a + 3 * innerSizeZ;
  if (TDir == Direction::BL) return a + 4 * innerSizeZ;
  if (TDir == Direction::BR) return a + 4 * innerSizeZ + innerSizeY;
  if (TDir == Direction::BD) return a + 4 * innerSizeZ + 2 * innerSizeY;
  if (TDir == Direction::BU) return a + 4 * innerSizeZ + 2 * innerSizeY + innerSizeX;
  if (TDir == Direction::FL) return a + 4 * innerSizeZ + 2 * innerSizeY + 2 * innerSizeX;
  if (TDir == Direction::FR) return a + 4 * innerSizeZ + 3 * innerSizeY + 2 * innerSizeX;
  if (TDir == Direction::FD) return a + 4 * innerSizeZ + 4 * innerSizeY + 2 * innerSizeX;
  if (TDir == Direction::FU) return a + 4 * innerSizeZ + 4 * innerSizeY + 3 * innerSizeX;

  if (TDir == Direction::BDL) return b;
  if (TDir == Direction::BDR) return b + 1;
  if (TDir == Direction::BUL) return b + 2;
  if (TDir == Direction::BUR) return b + 3;
  if (TDir == Direction::FDL) return b + 4;
  if (TDir == Direction::FDR) return b + 5;
  if (TDir == Direction::FUL) return b + 6;
  if (TDir == Direction::FUR) return b + 7;

  return 0;
}

/**
 * get the needed alignment information in the exchange array for a TDirectional kernel
 *
 * @param x     Current x value of the thread
 * @param y     Current y value of the thread
 * @param z     Current z value of the thread
 * @param args  CudaArguments with informations about the field boundaries
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The align in exchange.
 */
template <Direction TDir>
__inline__ __device__ int getAlignInExchange(unsigned long x, unsigned long y, unsigned long z, CudaArguments args) {
  // remove boundary offset from coordinates
  x -= args.boundarySizeX;
  y -= args.boundarySizeY;
  z -= args.boundarySizeZ;

  // TODO add dimensions for border larger then 1
  int innerSizeX = args.sizeX - 2 * args.boundarySizeX;
  int innerSizeZ = args.sizeZ - 2 * args.boundarySizeZ;

  if (TDir == Direction::D) return z * innerSizeX + x;
  if (TDir == Direction::U) return z * innerSizeX + x;
  if (TDir == Direction::B) return y * innerSizeX + x;
  if (TDir == Direction::F) return y * innerSizeX + x;
  if (TDir == Direction::L) return y * innerSizeZ + z;
  if (TDir == Direction::R) return y * innerSizeZ + z;

  if (TDir == Direction::DL) return z;
  if (TDir == Direction::DR) return z;
  if (TDir == Direction::UL) return z;
  if (TDir == Direction::UR) return z;
  if (TDir == Direction::BL) return y;
  if (TDir == Direction::BR) return y;
  if (TDir == Direction::BD) return x;
  if (TDir == Direction::BU) return x;
  if (TDir == Direction::FL) return y;
  if (TDir == Direction::FR) return y;
  if (TDir == Direction::FD) return x;
  if (TDir == Direction::FU) return x;

  return 0;
}

/**
 * Helper function to get data from the exchange array
 *
 * @param exchange  Points to the exchange array
 * @param x         Current x value of the thread
 * @param y         Current y value of the thread
 * @param z         Current z value of the thread
 * @param args      CudaArguments with informations about the field boundaries
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The from exchange.
 */
template <Direction TDir>
__inline__ __device__ real_t& getFromExchange(real_t* exchange, unsigned long x, unsigned long y, unsigned long z, CudaArguments args) {
  return exchange[getOffset<TDir>(args) + getAlignInExchange<TDir>(x, y, z, args)];
}

template <Direction TDir>
class StoreStencilFromExchange {
public:
  __host__ __device__ explicit StoreStencilFromExchange(CudaGpuFieldPtrs<real_t> ptrs, unsigned long x, unsigned long y, unsigned long z, const CudaArguments args, real_t value)
      : exchange{ptrs.exchange},
        x{x},
        y{y},
        z{z},
        args{args},
        value{value} {}

  template <typename T>
  __host__ __device__ void operator()(T d) const {
    if ((math::isSet(stencil::commonSide[toUnderlying(TDir)], d)) &&
        (!(TDir == Direction::L) || x == args.boundarySizeX) &&  // check for Direction::L if x is in the halo
        (!(TDir == Direction::R) || x == args.sizeX - args.boundarySizeX - 1) &&
        args.useExchangeOnBoundary[d.value]) {
      getFromExchange<static_cast<Direction>(d.value)>(exchange, x, y, z, args) = value;
    }
  }

private:
  real_t* exchange;
  unsigned long x;
  unsigned long y;
  unsigned long z;
  const CudaArguments& args;
  real_t value;
};

/**
 * Handels write back of the calculated result in a kernel.
 *
 * Stores the result in the appropriate cell for the calulation, as well as in the exchange array if needed.
 *
 * @param ptrs   Pack with the ptrs to data and exchange arrays.
 * @param x      Current x value of the thread.
 * @param y      Current y value of the thread.
 * @param z      Current z value of the thread.
 * @param args   CudaArguments with informations about the field boundaries.
 * @param value  The value to store.
 *
 * @tparam TDir  Defines the side of the kernel for that the function is specialized.
 */
template <Direction TDir>
__inline__ __device__ void storeStencil(CudaGpuFieldPtrs<real_t> ptrs, unsigned long x, unsigned long y, unsigned long z, CudaArguments args, real_t value) {
  real_t* data    = ptrs.data;
  int alignGlobal = z * args.strideZ + y * args.strideY + x * args.strideX;

  data[alignGlobal] = value;

  auto lambda = StoreStencilFromExchange<TDir>(ptrs, x, y, z, args, value);

  staticForDevice<1, numberOfDirections>(lambda);
}

template <Direction TDir>
class LoadStencilFromExchange {
public:
  __host__ __device__ explicit LoadStencilFromExchange(real_t* cells, CudaGpuFieldPtrs<real_t> ptrs, unsigned long x, unsigned long y, unsigned long z, const CudaArguments args)
      : cells{cells},
        exchange{ptrs.exchange},
        x{x},
        y{y},
        z{z},
        args{args} {}

  template <typename T>
  __host__ __device__ void operator()(T d) const {
    if ((math::isSet(stencil::commonSide[toUnderlying(TDir)], d)) &&
        (!(TDir == Direction::L) || x == args.boundarySizeX) &&  // check for Direction::L if x is in the halo
        (!(TDir == Direction::R) || x == args.sizeX - args.boundarySizeX - 1) &&
        args.useExchangeOnBoundary[d.value]) {
      cells[d.value] = getFromExchange<static_cast<Direction>(d.value)>(exchange, x, y, z, args);
    }
  }

private:
  real_t* cells;
  real_t* exchange;
  unsigned long x;
  unsigned long y;
  unsigned long z;
  const CudaArguments& args;
};

/**
 * Loads different Stencils into a register array.
 *
 * The array needs to be defined in the kernel and passed as first parameter currently support are stencils of size 1
 * (only the center cell), 7 (center cell and TDirect neighbours), 19 (center cell with neighbours on sides and edges, no
 * corners), and 27 (the full stencil).
 *
 * @param cells  Array to hold the loaded data.
 * @param ptrs   Pack with the ptrs to data and exchange arrays.
 * @param x      Current x value of the thread.
 * @param y      Current y value of the thread.
 * @param z      Current z value of the thread.
 * @param args   CudaArguments with informations about the field boundaries.
 *
 * @tparam TStencilSize  Defines which stencil to load.
 * @tparam TDir          Defines the side of the kernel for that the function is specialized.
 */
template <int TStencilSize, Direction TDir = Direction::INVALID>
__inline__ __host__ __device__ void loadStencil(real_t* cells, CudaGpuFieldPtrs<real_t> ptrs, unsigned long x, unsigned long y, unsigned long z, const CudaArguments args) {
  real_t* data    = ptrs.data;
  int alignGlobal = z * args.strideZ + y * args.strideY + x * args.strideX;

  // Fill the stencil with values from the center block.
  for (unsigned int d = 0; d < TStencilSize; d++) {
    cells[d] = data[alignGlobal + cx(d) * args.strideX + cy(d) * args.strideY + cz(d) * args.strideZ];
  }

  auto lambda = LoadStencilFromExchange<TDir>(cells, ptrs, x, y, z, args);

  staticForDevice<1, TStencilSize>(lambda);
}

/**
 * Returns the number of needed Blocks in the X direction for the given directional kernel
 *
 * @param args        CudaArguments with informations about the field boundaries
 * @param blockSizeX  Used blocksize
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The number blocks x.
 */
template <Direction TDir>
unsigned int getNumBlocksX(CudaArguments args, unsigned int blockSizeX) {
  if (TDir == Direction::C) {
    if (args.sizeX > 2 * blockSizeX) {
      if ((args.sizeX - 2 * args.boundarySizeX) % blockSizeX == 0) {
        return (args.sizeX - 2 * args.boundarySizeX) / blockSizeX - 2;
      } else {
        return (args.sizeX - 2 * args.boundarySizeX) / blockSizeX - 1;
      }
    } else {
      return 0;
    }
  }

  if (cx(TDir) == 0) return ((args.sizeX - 2 * args.boundarySizeX) - 2) / blockSizeX + 1;

  return 1;
}

/**
 * Returns the number of needed Blocks in the Y direction for the given directional kernel
 *
 * @param args        CudaArguments with informations about the field boundaries
 * @param blockSizeY  Used blocksize
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The number blocks y.
 */
template <Direction TDir>
unsigned int getNumBlocksY(CudaArguments args, unsigned int blockSizeY) {
  if (cy(TDir) == 0) return ((args.sizeY - 2 * args.boundarySizeY) - 2) / blockSizeY + 1;

  return 1;
}

/**
 * Returns the number of needed Blocks in the Z direction for the given directional kernel
 *
 * @param args        CudaArguments with informations about the field boundaries
 * @param blockSizeZ  Used blocksize
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The number blocks z.
 */
template <Direction TDir>
unsigned int getNumBlocksZ(CudaArguments args, unsigned int blockSizeZ) {
  if (cz(TDir) == 0) return ((args.sizeZ - 2 * args.boundarySizeZ) - 2) / blockSizeZ + 1;

  return 1;
}

/**
 * Returns the appropriate cuda blocksize in the X direction for the given directional kernel
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The block size x.
 */
template <Direction TDir>
unsigned int getBlockSizeX() {
  if (cx(TDir) == 0 || TDir == Direction::L || TDir == Direction::R) return cudaBlockSizeX;

  return 1;
}

/**
 * Returns the appropriate cuda blocksize in the Y direction for the given directional kernel
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The block size y.
 */
template <Direction TDir>
unsigned int getBlockSizeY() {
  if (cy(TDir) == 0) return cudaBlockSizeY;

  return 1;
}

/**
 * Returns the appropriate cuda blocksize in the Z direction for the given directional kernel
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The block size z.
 */
template <Direction TDir>
unsigned int getBlockSizeZ() {
  if (cz(TDir) == 0) return cudaBlockSizeZ;

  return 1;
}

/**
 * Returns the lowest x value of a cell handeled by the given directional kernel
 *
 * @param args        The arguments.
 * @param blockSizeX  Used blocksize
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The start x.
 */
template <Direction TDir>
unsigned long getStartX(CudaArguments args, unsigned int blockSizeX) {
  if (TDir == Direction::C) return blockSizeX + args.boundarySizeX;
  if (cx(TDir) == -1) return args.boundarySizeX;
  if (TDir == Direction::R) return args.boundarySizeX + ((args.sizeX - 2 * args.boundarySizeX) / blockSizeX) * blockSizeX - 1;
  if (cx(TDir) == 1) return args.sizeX - 1 * args.boundarySizeX - 1;
  return args.boundarySizeX + 1;
}

/**
 * Returns the lowest y value of a cell handeled by the given directional kernel
 *
 * @param args  The arguments.
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The start y.
 */
template <Direction TDir>
unsigned long getStartY(CudaArguments args) {
  if (cy(TDir) == -1) return args.boundarySizeY;
  if (cy(TDir) == 1) return args.sizeY - 1 * args.boundarySizeY - 1;
  return args.boundarySizeY + 1;
}

/**
 * Returns the lowest z value of a cell handeled by the given directional kernel
 *
 * @param args  The arguments.
 *
 * @tparam TDir  Defines which kernel is currently used
 *
 * @return The start z.
 */
template <Direction TDir>
unsigned long getStartZ(CudaArguments args) {
  if (cz(TDir) == -1) return args.boundarySizeZ;
  if (cz(TDir) == 1) return args.sizeZ - 1 * args.boundarySizeZ - 1;
  return args.boundarySizeZ + 1;
}

/**
 * Typedef for a general kernel with the needed informations packed in the two given structs
 */
using kernel = void (*)(const CudaArguments, CudaSimData);

/**
 * Gerneralises the launch of a kernel for different directions.
 *
 * Gets the necessary informations like blockSize and amount of blocks from the previously defined helper functions
 *
 * @param a           functionpointer to the kernel
 * @param stream      Stream used for the bulk calculation in middle of the field
 * @param haloStream  Stream used for the calculation in the outer areas of the field
 * @param args        CudaArguments with informations about the field boundaries
 * @param cSimData    SimData packed for the passing to a cuda kernel
 *
 * @tparam TDir  Defines which kernel is to be started
 */
template <Direction TDir>
void launchKernel(kernel a, cudaStream_t stream, cudaStream_t haloStream, CudaArguments args, CudaSimData cSimData) {
  unsigned int blockSizeX = getBlockSizeX<TDir>();
  unsigned int blockSizeY = getBlockSizeY<TDir>();
  unsigned int blockSizeZ = getBlockSizeZ<TDir>();

  unsigned int numBlocksX = getNumBlocksX<TDir>(args, blockSizeX);
  unsigned int numBlocksY = getNumBlocksY<TDir>(args, blockSizeY);
  unsigned int numBlocksZ = getNumBlocksZ<TDir>(args, blockSizeZ);

  args.startX = getStartX<TDir>(args, blockSizeX);
  args.startY = getStartY<TDir>(args);
  args.startZ = getStartZ<TDir>(args);

  dim3 threadsPerBlock(blockSizeX, blockSizeY, blockSizeZ);
  dim3 numBlocks(numBlocksX, numBlocksY, numBlocksZ);
  cudaStream_t launchStream = (TDir == Direction::C) ? stream : haloStream;
  a<<<numBlocks, threadsPerBlock, 0, launchStream>>>(args, cSimData);
}

}  // namespace cuda
}  // namespace nastja
