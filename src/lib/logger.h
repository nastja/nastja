/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger/logger_impl.h"
#include "lib/logger/registry.h"
#include <fstream>
#include <iostream>
#include <ostream>
#include <string>

namespace nastja {
namespace logger {

class Logger;

Logger& get(const std::string& name = "");

Logger& registerLogger(const std::string& name = "", std::ostream& ostream = std::cout);
Logger& registerLogger(const std::string& name, const std::string& filename, unsigned int groupSize = 0);

bool hasLogger(const std::string& name);

void syncAll();
void isyncAll();

void closeAll();
void closeLogger(const std::string& name);

}  // namespace logger
}  // namespace nastja
