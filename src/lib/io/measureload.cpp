/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "measureload.h"
#include <cstdio>
#include <memory>
#include <string>

namespace nastja {

void WriteLoad::execute(const block::BlockRegister& blockRegister) {
  auto& loadFile = logger::get("load");
  loadFile.log("{:5d} {:5d}", getSimData().timestep - offset, blockRegister.size());
  loadFile.flush();
}

}  // namespace nastja
