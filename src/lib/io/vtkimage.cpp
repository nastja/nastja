/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "vtkimage.h"
#include "lib/timing.h"
#include "lib/block/block.h"
#include "lib/config/configutils.h"
#include "lib/field/field.h"
#include <cstdio>
#include <stdexcept>
#include <vector>

namespace nastja {

void VtkImage::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  WriterReader::init(blockRegister, fieldpool);
  auto& config = getSimData().getConfig();
  auto jptr    = config::getJsonPointer("Writers." + getName());
  auto* field  = blockRegister.begin()->second->getFieldByName(getFieldname());

  /// @key{Writers.\template{writer=VtkImage}.steps, int, 1}
  /// Number of each nth steps when the output is written.
  eachNthStep = config.getValue<int>(jptr / "steps", 1);

  /// @key{Writers.\template{writer=VtkImage}.outputfieldname, string, ""}
  /// The output data type.
  outputFieldName_ = config.getValue<std::string>(jptr / "outputfieldname", getFieldname());

  /// @key{Writers.\template{writer=VtkImage}.useBoundary, bool, false}
  /// Include the boundary layer in the output or boundarybox.
  useBoundary_ = config.getValue<bool>(jptr / "useBoundary", false);

  /// @key{Writers.\template{writer=VtkImage}.localbox, Bbox<uint>}
  /// For each blck write all cells within the given local bounding box
  if (config.contains(jptr / "localbox")) {
    if (useBoundary_) {
      // values in config files are absolute values with boundary in mind
      bbox_ = config.getBoundingBox<unsigned long>(jptr / "localbox");
    } else {
      // values in config file are without the boundary layer, translation changes these values to absolute values
      bbox_ = config.getBoundingBox<unsigned long>(jptr / "localbox");
      bbox_.translate(field->getBoundarySize());
    }
    useLocalBbox_ = true;
  } else {
    bbox_ = nastja::math::BoundingBox<unsigned long>((useBoundary_) ? nastja::math::Vector3<unsigned long>{0, 0, 0} : field->getBoundarySize(),
                                                     (useBoundary_) ? field->getSize() : field->getSize() - field->getBoundarySize());
    useLocalBbox_ = false;
  }

  /// @key{Writers.\template{writer=VtkImage}.outputtype, string, "Float32"}
  /// @values{"Float32", "Float64", "UInt32", "UInt64"}
  /// The output data type.
  dataType_ = config.getValue<std::string>(jptr / "outputtype", "Float32");

  if (dataType_ == "Float32") {
    dataSize_ = sizeof(float);
  } else if (dataType_ == "Float64") {
    dataSize_ = sizeof(double);
  } else if (dataType_ == "UInt32") {
    dataSize_ = sizeof(unsigned int);
  } else if (dataType_ == "UInt64") {
    dataSize_ = sizeof(unsigned long);
  } else {
    throw std::invalid_argument(fmt::format("Data type '{}' for output is unknown. Try 'Float32', 'Float64', 'UInt32' or 'UInt64'.", dataType_));
  }

  /// @key{Writers.\template{writer=VtkImage}.component, int[], optional}
  /// Write out only given components, default write out all components.
  auto comp_jptr = jptr / "components";
  if (config.contains(comp_jptr)) {
    if (!config.isArray(comp_jptr)) {
      throw std::invalid_argument(fmt::format("{} must be an array when given.", comp_jptr));
    }
    components_ = config.getVector<unsigned int>(comp_jptr);
  } else {
    // default fill vector from 0 to vectorsize
    const auto vectorSize = field->getVectorSize();
    components_.resize(vectorSize);
    std::iota(components_.begin(), components_.end(), 0);
  }
}

void VtkImage::executeBlock(block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  const auto& boundarySize = field->getBoundarySize();
  const auto& offset       = block->offset;
  auto size                = (useBoundary_) ? field->getSize() : field->getSize() - 2 * boundarySize;
  const auto vectorSize    = components_.size();
  unsigned long length     = size[0] * size[1] * size[2] * vectorSize * dataSize_;

  if (useLocalBbox_) {
    size   = bbox_.upper() - bbox_.lower();
    length = size[0] * size[1] * size[2] * vectorSize * dataSize_;
  }

  std::string filename = makeFilename(outputFieldName_, "vti", frame, block->blockOffset);

  FILE* fp = fopen(filename.c_str(), "w");

  if (fp == nullptr) perror("file");

  fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
  fprintf(fp, "<ImageData WholeExtent=\"%lu %lu %lu %lu %lu %lu\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", offset[0], offset[0] + size[0], offset[1], offset[1] + size[1], offset[2], offset[2] + size[2], getSimData().deltax, getSimData().deltax, getSimData().deltax);
  fprintf(fp, "<CellData Scalars=\"%s\">\n", getFieldname().c_str());
  fprintf(fp, "<DataArray type=\"%s\" Name=\"%s\" format=\"appended\" offset=\"0\" NumberOfComponents=\"%lu\"/>\n", dataType_.c_str(), getFieldname().c_str(), vectorSize);
  fprintf(fp, "</CellData>\n");
  fprintf(fp, "</ImageData>\n");
  fprintf(fp, "<AppendedData encoding=\"raw\">\n");
  fprintf(fp, "_");
  fwrite((unsigned char*)&length, sizeof(long), 1, fp);

  if (auto* field_ = field->getPtr<real_t, true>()) {
    doWrite(fp, *field_);
  } else if (auto* field_ = field->getPtr<real_t, false>()) {
    doWrite(fp, *field_);
  } else if (auto* field_ = field->getPtr<cellid_t, true>()) {
    doWrite(fp, *field_);
  } else if (auto* field_ = field->getPtr<cellid_t, false>()) {
    doWrite(fp, *field_);
  } else {
    throw std::invalid_argument(fmt::format("Field '{}' of unknown type.", getFieldname()));
  }

  fprintf(fp, "\n</AppendedData>\n");
  fprintf(fp, "</VTKFile>\n");
  fclose(fp);
}

template <typename T, unsigned int TSplitX, typename U>
void VtkImage::doWrite(FILE* fp, field::Field<T, TSplitX>& field, U value) const {

#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    field.cuCpDataDeviceToHost();
  }
#endif

  auto view = field.template createView<1>(bbox_);
  for (const auto& cell : view) {
    for (const auto v : components_) {
      value = U(view.getVec(cell, nastja::stencil::Direction::C)[v]);
      fwrite((unsigned char*)&value, sizeof(U), 1, fp);
    }
  }
}

template void VtkImage::doWrite<cellid_t, 1, double>(FILE* fp, field::Field<cellid_t, 1>& field, double value) const;
template void VtkImage::doWrite<cellid_t, 2, double>(FILE* fp, field::Field<cellid_t, 2>& field, double value) const;
template void VtkImage::doWrite<cellid_t, 4, double>(FILE* fp, field::Field<cellid_t, 4>& field, double value) const;
template void VtkImage::doWrite<cellid_t, 8, double>(FILE* fp, field::Field<cellid_t, 8>& field, double value) const;
template void VtkImage::doWrite<cellid_t, 16, double>(FILE* fp, field::Field<cellid_t, 16>& field, double value) const;
template void VtkImage::doWrite<cellid_t, 1, float>(FILE* fp, field::Field<cellid_t, 1>& field, float value) const;
template void VtkImage::doWrite<cellid_t, 2, float>(FILE* fp, field::Field<cellid_t, 2>& field, float value) const;
template void VtkImage::doWrite<cellid_t, 4, float>(FILE* fp, field::Field<cellid_t, 4>& field, float value) const;
template void VtkImage::doWrite<cellid_t, 8, float>(FILE* fp, field::Field<cellid_t, 8>& field, float value) const;
template void VtkImage::doWrite<cellid_t, 16, float>(FILE* fp, field::Field<cellid_t, 16>& field, float value) const;
template void VtkImage::doWrite<cellid_t, 1, unsigned int>(FILE* fp, field::Field<cellid_t, 1>& field, unsigned int value) const;
template void VtkImage::doWrite<cellid_t, 2, unsigned int>(FILE* fp, field::Field<cellid_t, 2>& field, unsigned int value) const;
template void VtkImage::doWrite<cellid_t, 4, unsigned int>(FILE* fp, field::Field<cellid_t, 4>& field, unsigned int value) const;
template void VtkImage::doWrite<cellid_t, 8, unsigned int>(FILE* fp, field::Field<cellid_t, 8>& field, unsigned int value) const;
template void VtkImage::doWrite<cellid_t, 16, unsigned int>(FILE* fp, field::Field<cellid_t, 16>& field, unsigned int value) const;
template void VtkImage::doWrite<cellid_t, 1, unsigned long>(FILE* fp, field::Field<cellid_t, 1>& field, unsigned long value) const;
template void VtkImage::doWrite<cellid_t, 2, unsigned long>(FILE* fp, field::Field<cellid_t, 2>& field, unsigned long value) const;
template void VtkImage::doWrite<cellid_t, 4, unsigned long>(FILE* fp, field::Field<cellid_t, 4>& field, unsigned long value) const;
template void VtkImage::doWrite<cellid_t, 8, unsigned long>(FILE* fp, field::Field<cellid_t, 8>& field, unsigned long value) const;
template void VtkImage::doWrite<cellid_t, 16, unsigned long>(FILE* fp, field::Field<cellid_t, 16>& field, unsigned long value) const;

template void VtkImage::doWrite<real_t, 1, double>(FILE* fp, field::Field<real_t, 1>& field, double value) const;
template void VtkImage::doWrite<real_t, 2, double>(FILE* fp, field::Field<real_t, 2>& field, double value) const;
template void VtkImage::doWrite<real_t, 4, double>(FILE* fp, field::Field<real_t, 4>& field, double value) const;
template void VtkImage::doWrite<real_t, 8, double>(FILE* fp, field::Field<real_t, 8>& field, double value) const;
template void VtkImage::doWrite<real_t, 16, double>(FILE* fp, field::Field<real_t, 16>& field, double value) const;
template void VtkImage::doWrite<real_t, 1, float>(FILE* fp, field::Field<real_t, 1>& field, float value) const;
template void VtkImage::doWrite<real_t, 2, float>(FILE* fp, field::Field<real_t, 2>& field, float value) const;
template void VtkImage::doWrite<real_t, 4, float>(FILE* fp, field::Field<real_t, 4>& field, float value) const;
template void VtkImage::doWrite<real_t, 8, float>(FILE* fp, field::Field<real_t, 8>& field, float value) const;
template void VtkImage::doWrite<real_t, 16, float>(FILE* fp, field::Field<real_t, 16>& field, float value) const;
template void VtkImage::doWrite<real_t, 1, unsigned int>(FILE* fp, field::Field<real_t, 1>& field, unsigned int value) const;
template void VtkImage::doWrite<real_t, 2, unsigned int>(FILE* fp, field::Field<real_t, 2>& field, unsigned int value) const;
template void VtkImage::doWrite<real_t, 4, unsigned int>(FILE* fp, field::Field<real_t, 4>& field, unsigned int value) const;
template void VtkImage::doWrite<real_t, 8, unsigned int>(FILE* fp, field::Field<real_t, 8>& field, unsigned int value) const;
template void VtkImage::doWrite<real_t, 16, unsigned int>(FILE* fp, field::Field<real_t, 16>& field, unsigned int value) const;
template void VtkImage::doWrite<real_t, 1, unsigned long>(FILE* fp, field::Field<real_t, 1>& field, unsigned long value) const;
template void VtkImage::doWrite<real_t, 2, unsigned long>(FILE* fp, field::Field<real_t, 2>& field, unsigned long value) const;
template void VtkImage::doWrite<real_t, 4, unsigned long>(FILE* fp, field::Field<real_t, 4>& field, unsigned long value) const;
template void VtkImage::doWrite<real_t, 8, unsigned long>(FILE* fp, field::Field<real_t, 8>& field, unsigned long value) const;
template void VtkImage::doWrite<real_t, 16, unsigned long>(FILE* fp, field::Field<real_t, 16>& field, unsigned long value) const;

}  // namespace nastja
