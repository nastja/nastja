/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "vtkimageread.h"
#include "lib/field/field.h"
#include <cstdio>

namespace nastja {

#define LINE_MAX_LENGTH 2048

void VtkImageReader::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName(getFieldname())->get<real_t>();

  const auto& boundarySize = field.getBoundarySize();
  const auto& offset       = field.getOffset();
  unsigned int sizeX       = field.getSize(0) - 2 * boundarySize[0];
  unsigned int sizeY       = field.getSize(1) - 2 * boundarySize[1];
  unsigned int sizeZ       = field.getSize(2) - 2 * boundarySize[2];
  unsigned long length     = sizeX * sizeY * sizeZ * sizeof(float);

  std::string filename = makeFilename(getFieldname(), "vti", 0, block->blockOffset);

  FILE* fp = fopen(filename.c_str(), "r");

  char line[LINE_MAX_LENGTH];

  unsigned int startX;
  unsigned int endX;
  unsigned int startY;
  unsigned int endY;
  unsigned int startZ;
  unsigned int endZ;
  double deltax;
  double deltay;
  double deltaz;

  unsigned int lineCnt = 0;
  while (fgets(line, sizeof(line), fp) != nullptr) {
    lineCnt++;
    if (lineCnt == 2) {
      if (sscanf(line, "<ImageData WholeExtent=\"%u %u %u %u %u %u\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", &startX, &endX, &startY, &endY, &startZ, &endZ, &deltax, &deltay, &deltaz) != 9) throw std::out_of_range(fmt::format("Input Error"));
      if (!((startX == offset[0]) && (endX == offset[0] + sizeX) && (startY == offset[1]) && (endY == offset[1] + sizeY) && (startZ == offset[2]) && (endZ == offset[2] + sizeZ))) {
        throw std::range_error(fmt::format("This block ({} {} {} {} {} {}) is not matching {} {} {} {} {} {}.",
                                           startX, endX, startY, endY, startZ, endZ,
                                           offset[0], offset[0] + sizeX, offset[1], offset[1] + sizeY, offset[2], offset[2] + sizeZ));
      }
    } else if (lineCnt == 7) {  // Data coming
      while (fgetc(fp) != '_') {}
      unsigned long dataLength;
      if (!fread(&dataLength, sizeof(long), 1, fp)) throw std::runtime_error("Can't read from file.");
      if (dataLength != length) throw std::length_error(fmt::format("Data length does not match {} != {}.", dataLength, length));

      float value;
      // TODO unsigned int vectorSize = this->field.vectorSize;
      for (unsigned int z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
        for (unsigned int y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
          for (unsigned int x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
            if (!fread((unsigned char*)&value, sizeof(float), 1, fp)) throw std::runtime_error("Can't read from file.");
            field.getCell(x, y, z, 0) = value;
          }
        }
      }
      fclose(fp);
      return;
    }
  }
}

}  // namespace nastja
