/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "writerreader.h"
#include "lib/logger.h"
#include "lib/statusoutput.h"
#include <cerrno>
#include <fstream>
#include <memory>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>

namespace nastja {

/**
 * Test if the path is a directory.
 *
 * @param path The path.
 *
 * @return true if path is a directory.
 */
bool WriterReader::isDir(const std::string& path) {
  struct stat info;

  if (stat(path.c_str(), &info) != 0) {
    return false;
  }
  return (info.st_mode & S_IFDIR) != 0;
}

/**
 * Make a directory.
 *
 * @param path The path to the directory.
 *
 * @return true if the directory was created or already exist as a directory. If false errno is set.
 */
bool WriterReader::makeDir(const std::string& path) {
  mode_t mode = S_IRWXU;  // 0755;

  if (mkdir(path.c_str(), mode) != 0) {
    if (errno != EEXIST) return false;
    return isDir(path);
  }

  return true;
}

/**
 * Make directory recursively.
 *
 * @param path The full path.
 *
 * @return true if the directory was create or already exist as a directory. If false errno is set.
 */
bool WriterReader::makeDirRecursive(const std::string& path) {
  // try to create directory
  if (makeDir(path)) return true;

  // failed, test for missing parent directory
  if (errno != ENOENT) return false;

  // parent didn't exist, try to create it
  int pos = path.find_last_of('/');
  if (pos == (int)std::string::npos) return false;
  if (!makeDirRecursive(path.substr(0, pos))) return false;

  // retry to create directory
  return makeDir(path);
}

/**
 * Get the directory name from a path.
 *
 * @param path The path.
 *
 * @return directory path
 */
std::string WriterReader::getDirName(const std::string& path) {
  int pos = path.find_last_of('/');
  return path.substr(0, pos);
}

/**
 * Check if the path is available or create it.
 *
 * @param filename The full path of the file.
 */
void WriterReader::checkPath(const std::string& filename) {
  if (!makeDirRecursive(getDirName(filename))) {
    throw std::runtime_error(fmt::format("Path '{}' not creatable (errno: {}).", filename.c_str(), errno));
  }
}

/**
 * Creates a path with filename.
 *
 * @param fieldname   Name of the field.
 * @param extension   Extension of the file.
 * @param frame       Number of the frame.
 * @param blockOffset Vector with block offsets.
 *
 * @return full path name.
 */
std::string WriterReader::makeFilename(const std::string& fieldname, const std::string& extension, const int frame, math::Vector3<unsigned long> blockOffset) const {
  auto filename = fmt::format("{}/{:03d}/{:03d}/{:03d}/{}_{}-{:05d}.{}",
                              getBaseDir(), blockOffset[0], blockOffset[1], blockOffset[2], basename_, fieldname, frame, extension);

  checkPath(filename);

  return filename;
}

/**
 * @overload
 *
 * @param fieldname Name of the field.s
 * @param extension Extension of the file.
 * @param frame     Number of the frame.
 * @param rank      Number of the rank.
 */
std::string WriterReader::makeFilename(const std::string& fieldname, const std::string& extension, const int frame, const int rank) const {
  auto filename = fmt::format("{}/{:06d}/{}_{}-{:05d}.{}",
                              getBaseDir(), rank, basename_, fieldname, frame, extension);

  checkPath(filename);

  return filename;
}

/**
 * @overload
 *
 * @param fieldname Name of the field.s
 * @param extension Extension of the file.
 * @param rank      Number of the rank.
 */
std::string WriterReader::makeFilename(const std::string& fieldname, const std::string& extension, const int rank) const {
  auto filename = fmt::format("{}/{:06d}/{}_{}.{}",
                              getBaseDir(), rank, basename_, fieldname, extension);

  checkPath(filename);

  return filename;
}

/**
 * Makes a filename by frame.
 *
 * @param fieldname  The fieldname.
 * @param extension  The extension.
 * @param frame      The frame number.
 *
 * @return The full path name.
 */
std::string WriterReader::makeFilenameFrame(const std::string& fieldname, const std::string& extension, const int frame) const {
  auto filename = fmt::format("{}/{}_{}-{:05d}.{}",
                              getBaseDir(), basename_, fieldname, frame, extension);

  checkPath(filename);

  return filename;
}

/**
 * Makes a filename by frame.
 * @overload
 *
 * @param fieldname  The fieldname.
 * @param extension  The extension.
 * @param frame      The frame number.
 * @param group      The group number.
 *
 * @return The full path name.
 */
std::string WriterReader::makeFilenameFrame(const std::string& fieldname, const std::string& extension, const int frame, const int group) const {
  auto filename = fmt::format("{}/{}_{}_group_{}-{:05d}.{}",
                              getBaseDir(), basename_, fieldname, group, frame, extension);

  checkPath(filename);

  return filename;
}


void WriterReader::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }
}

void WriterReader::execute(const block::BlockRegister& blockRegister) {
  if (isSkipped()) return;

  if (getSimData().timestep == 0) {
    logger::get().onelog("Write {:<20} Frame: {:4} (initial)", getName(), frame);
  } else {
    logger::get().onelog("Write {:<20} Frame: {:4} ({:7})", getName(), frame, getSimData().timestep - offset);
  }

  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }

  frame++;
}

}  // namespace nastja
