/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/io/writerreader.h"

namespace nastja {

/**
 * Class for reading vtk images.
 *
 * @ingroup io io-in
 */
class VtkImageReader : public WriterReader {
public:
  explicit VtkImageReader(const std::string& fieldname)
      : WriterReader{fieldname} {}
  ~VtkImageReader() override                = default;
  VtkImageReader(const VtkImageReader&)     = delete;
  VtkImageReader(VtkImageReader&&) noexcept = default;
  VtkImageReader& operator=(const VtkImageReader&) = delete;
  VtkImageReader& operator=(VtkImageReader&&) = delete;

  void executeBlock(block::Block* block) override;
  bool skip() const override { return isSkipped(); }
};

}  // namespace nastja
