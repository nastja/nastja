/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/io/writerreader.h"

namespace nastja {

/**
 * Class for write centers and other cell properties.
 *
 * @ingroup io io-out cells
 */
class CellInfo : public WriterReader {
public:
  explicit CellInfo(const std::string& /*fieldname*/)
      : WriterReader{"cells"} {}
  ~CellInfo() override          = default;
  CellInfo(const CellInfo&)     = delete;
  CellInfo(CellInfo&&) noexcept = default;
  CellInfo& operator=(const CellInfo&) = delete;
  CellInfo& operator=(CellInfo&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return isSkipped(); }

private:
  std::ofstream openfile(const std::string& filename) const;
  void sendToMaster();
  void collect();

  bool masterOutput_{false};          ///< Flag if master processes should collect the output.
  bool printContactInhibition{false}; ///< Flag if contact inhibition should be printed.
  int groupSize_{};                   ///< The group size for collecting the output.
  std::string outputFieldName_;       ///< option to set the output field name

  Archive ar_;                                 ///< The archive for collection and sending the cell information.
  MPI_Request sendRequest_{MPI_REQUEST_NULL};  ///< The MPI send request handler.

  int neighborSqrRadius_{};                          ///< The squared radius for the neighbor radius search.
  std::vector<std::pair<cellid_t, int>> neighbors_;  ///< Indices for the neighbors.
};

}  // namespace nastja
