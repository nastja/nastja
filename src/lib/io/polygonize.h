/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/io/writerreader.h"
#include "lib/math/triangle.h"
#include "lib/math/vector.h"

namespace nastja {

/**
 * Class for writing stl files using marching cubes for polygonization.
 *
 * @ingroup io io-out
 */
class Polygonize : public WriterReader {
public:
  explicit Polygonize(const std::string& fieldname)
      : WriterReader{fieldname} {}
  ~Polygonize() override            = default;
  Polygonize(const Polygonize&)     = delete;
  Polygonize(Polygonize&&) noexcept = default;
  Polygonize& operator=(const Polygonize&) = delete;
  Polygonize& operator=(Polygonize&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* block) override;
  bool skip() const override { return isSkipped(); }

private:
  void writeTriangles2Buffer(std::vector<float>& buffer) const;
  static math::Vector3<real_t> VertexInterpolation(real_t isolevel, real_t x, real_t y, real_t z, real_t x1, real_t y1, real_t z1, real_t valp1, real_t valp2);
  void sendTriangles2Master();
  void collectTriangles();
  void saveBinaryStl(const std::string& filename, const std::vector<float>& buffer) const;
  void saveBinaryStl(const std::string& filename) const;
  void saveAsciiStl(const std::string& filename) const;

  char header_[80]{0};  ///< The header for ascii stl.

  MPI_Request sendRequest_{MPI_REQUEST_NULL};  ///< The send request.
  std::vector<float> sendBuffer_;              ///< The sendbuffer.
  std::vector<math::Triangle> triangles_;      ///< Vector of triangles.
  int mpiRank_{};                              ///< The mpi rank.
  int mpiSize_{};                              ///< The mpi size.
  int groupSize_{};                            ///< The size of one group.

  bool invertNormals_{false};  ///< Flag for inverting the normals.
  bool normalize_{true};       ///< Flag for normalizing the normals.

  real_t isolevel_{0.5};    ///< The iso-level value.
  long discreteID_{-1};     ///< The ID for the discrete mode
  bool useType_{false};     ///< Flag for use a specific cell type.
  real_t skipTypes_{-1.0};  ///< Skip types with a value <=.
};

}  // namespace nastja
