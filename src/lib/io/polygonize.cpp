/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "polygonize.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/field/field.h"
#include "lib/io/marchTables.h"
#include "lib/simdata/datacells.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

namespace nastja {

using namespace config::literals;

/**
 * Setup writer.
 * In the construction, the name is not set, so getName cannot work.
 */
void Polygonize::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  WriterReader::init(blockRegister, fieldpool);
  auto& config = getSimData().getConfig();
  auto jptr    = config::getJsonPointer("Writers." + getName());

  /// @key{Writers.\template{writer=Polygonize}.steps, int, 1}
  /// Number of each nth steps when the output is written.
  eachNthStep = config.getValue<int>(jptr / "steps", 1);

  /// @key{Writers.\template{writer=Polygonize}.groupsize, int, 1}
  /// Group size for collecting the output from n workers.
  groupSize_ = config.getValue<int>(jptr / "groupsize", 1);

  /// @key{Writers.\template{writer=Polygonize}.discreteID, int, -1}
  /// If not -1 then use this value as output. That mean the surface of a cellID or cell type is calculated.
  /// If this key is not set or -1 the iso-surface 0.5 is calculated.
  discreteID_ = config.getValue<int>(jptr / "discreteID", -1);

  /// @key{Writers.\template{writer=Polygonize}.useCelltype, bool, false}
  /// Flag enables the cell type for the output.
  useType_ = config.getValue<bool>(jptr / "useCelltype", false);

  if (discreteID_ != -1) {  // discrete variant use isolevel 0.5
    isolevel_ = 0.5;
  }

  mpiRank_ = getSimData().mpiData.getRank();
  mpiSize_ = getSimData().mpiData.getSize();

  snprintf(header_, 80, "solver output: %s", getFieldname().c_str());
}

void Polygonize::executeBlock(block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());
  assert(field != nullptr);

  CellBlockContainer* container = nullptr;
  if (block->hasDataContainer<CellBlockContainer>()) {
    container  = &(block->getDataContainer<CellBlockContainer>());
    skipTypes_ = getSimData().getData<DataCells>().liquid;
  }

  const auto& boundarySize = field->getBoundarySize();
  const auto& offset       = block->offset;

  // TODO unsigned int vectorSize = this->field->vectorSize;
  for (unsigned long z = boundarySize[2] - 1; z < field->getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1] - 1; y < field->getSize(1) - boundarySize[1]; y++) {
      for (unsigned long x = boundarySize[0] - 1; x < field->getSize(0) - boundarySize[0]; x++) {
        std::array<real_t, 8> value{};
        // clang-format off
        value[0] = field->getCellAsReal(x,     y + 1, z + 1, 0);
        value[1] = field->getCellAsReal(x + 1, y + 1, z + 1, 0);
        value[2] = field->getCellAsReal(x + 1, y + 1, z,     0);
        value[3] = field->getCellAsReal(x,     y + 1, z,     0);
        value[4] = field->getCellAsReal(x,     y,     z + 1, 0);
        value[5] = field->getCellAsReal(x + 1, y,     z + 1, 0);
        value[6] = field->getCellAsReal(x + 1, y,     z,     0);
        value[7] = field->getCellAsReal(x,     y,     z,     0);
        // clang-format on

        if (useType_) {
          for (real_t& val : value) {
            if (val <= skipTypes_) continue;
            val = container->cells(cellid_t(val)).type;
          }
        }

        if (discreteID_ != -1) {
          for (real_t& val : value) {
            val = (val == discreteID_) ? 0.0 : 1.0;  // 0.0 for outside, 1.0 for inside
          }
        }

        // determine cube index for lookup table
        unsigned int cubeIndex = 0;
        for (int i = 0; i < 8; i++) {
          if (value[i] < isolevel_) {
            cubeIndex |= (1 << i);
          }
        }

        unsigned long globalX = x + offset[0];
        unsigned long globalY = y + offset[1];
        unsigned long globalZ = z + offset[2];

        // no intersection with the isosurface in this cube
        if (edgeTable[cubeIndex] == 0) continue;

        // Find the vertices where the surface intersects the cube
        std::array<math::Vector3<real_t>, 12> verticesList;

        if ((edgeTable[cubeIndex] & 1) != 0) {
          verticesList[0] = VertexInterpolation(isolevel_, globalX, globalY + 1, globalZ + 1, globalX + 1, globalY + 1, globalZ + 1, value[0], value[1]);
        }
        if ((edgeTable[cubeIndex] & 2) != 0) {
          verticesList[1] = VertexInterpolation(isolevel_, globalX + 1, globalY + 1, globalZ + 1, globalX + 1, globalY + 1, globalZ, value[1], value[2]);
        }
        if ((edgeTable[cubeIndex] & 4) != 0) {
          verticesList[2] = VertexInterpolation(isolevel_, globalX + 1, globalY + 1, globalZ, globalX, globalY + 1, globalZ, value[2], value[3]);
        }
        if ((edgeTable[cubeIndex] & 8) != 0) {
          verticesList[3] = VertexInterpolation(isolevel_, globalX, globalY + 1, globalZ, globalX, globalY + 1, globalZ + 1, value[3], value[0]);
        }
        if ((edgeTable[cubeIndex] & 16) != 0) {
          verticesList[4] = VertexInterpolation(isolevel_, globalX, globalY, globalZ + 1, globalX + 1, globalY, globalZ + 1, value[4], value[5]);
        }
        if ((edgeTable[cubeIndex] & 32) != 0) {
          verticesList[5] = VertexInterpolation(isolevel_, globalX + 1, globalY, globalZ + 1, globalX + 1, globalY, globalZ, value[5], value[6]);
        }
        if ((edgeTable[cubeIndex] & 64) != 0) {
          verticesList[6] = VertexInterpolation(isolevel_, globalX + 1, globalY, globalZ, globalX, globalY, globalZ, value[6], value[7]);
        }
        if ((edgeTable[cubeIndex] & 128) != 0) {
          verticesList[7] = VertexInterpolation(isolevel_, globalX, globalY, globalZ, globalX, globalY, globalZ + 1, value[7], value[4]);
        }
        if ((edgeTable[cubeIndex] & 256) != 0) {
          verticesList[8] = VertexInterpolation(isolevel_, globalX, globalY + 1, globalZ + 1, globalX, globalY, globalZ + 1, value[0], value[4]);
        }
        if ((edgeTable[cubeIndex] & 512) != 0) {
          verticesList[9] = VertexInterpolation(isolevel_, globalX + 1, globalY + 1, globalZ + 1, globalX + 1, globalY, globalZ + 1, value[1], value[5]);
        }
        if ((edgeTable[cubeIndex] & 1024) != 0) {
          verticesList[10] = VertexInterpolation(isolevel_, globalX + 1, globalY + 1, globalZ, globalX + 1, globalY, globalZ, value[2], value[6]);
        }
        if ((edgeTable[cubeIndex] & 2048) != 0) {
          verticesList[11] = VertexInterpolation(isolevel_, globalX, globalY + 1, globalZ, globalX, globalY, globalZ, value[3], value[7]);
        }

        for (int i = 0; triTable[cubeIndex][i] != -1; i += 3) {
          math::Triangle triangle;

          triangle.p[0] = verticesList[triTable[cubeIndex][i]];
          triangle.p[1] = verticesList[triTable[cubeIndex][i + 1]];
          triangle.p[2] = verticesList[triTable[cubeIndex][i + 2]];

          if (triangle.calcNormal(invertNormals_, normalize_)) {
            triangles_.push_back(triangle);
          }
        }
      }
    }
  }
}

math::Vector3<real_t> Polygonize::VertexInterpolation(const real_t isolevel, const real_t x, const real_t y, const real_t z, const real_t x1, const real_t y1, const real_t z1, const real_t valp1, const real_t valp2) {
  math::Vector3<real_t> p;
  const real_t eps = 0.00001;
  real_t mu;

  if ((std::fabs(isolevel - valp1) < eps) || (std::fabs(valp1 - valp2) < eps)) {
    mu = 0.0;
  } else if (std::fabs(isolevel - valp2) < eps) {
    mu = 1.0;
  } else {
    mu = ((isolevel - valp1) / (valp2 - valp1));
  }

  p[0] = x + mu * (x1 - x) - 0.5;
  p[1] = y + mu * (y1 - y) - 0.5;
  p[2] = z + mu * (z1 - z) - 0.5;
  return p;
}

void Polygonize::saveAsciiStl(const std::string& filename) const {
  if (triangles_.empty()) return;

  std::ofstream myf;
  myf.open(filename.c_str());
  std::string name = "ascii-" + filename;
  myf << "solid " << name << "\n";

  for (const auto& t : triangles_) {
    myf << "facet normal " << t.n[0] << " " << t.n[1] << " " << t.n[2] << "\n";
    myf << "outer loop\n";
    myf << "vertex " << t.p[0][0] << " " << t.p[0][1] << " " << t.p[0][2] << "\n";
    myf << "vertex " << t.p[1][0] << " " << t.p[1][1] << " " << t.p[1][2] << "\n";
    myf << "vertex " << t.p[2][0] << " " << t.p[2][1] << " " << t.p[2][2] << "\n";
    myf << "endloop\n";
    myf << "endfacet\n";
  }
  myf << "endsolid " << name << "\n";
  myf.close();

  std::cout << "polygonize saved ascii " << filename << " tris: " << triangles_.size() << std::endl;
}

void Polygonize::saveBinaryStl(const std::string& filename) const {
  if (triangles_.empty()) return;

  std::vector<float> buffer;
  writeTriangles2Buffer(buffer);

  saveBinaryStl(filename, buffer);
}

void Polygonize::saveBinaryStl(const std::string& filename, const std::vector<float>& buffer) const {
  if (buffer.empty()) return;

  std::ofstream myFile;
  myFile.open(filename.c_str(), std::ios::binary);
  myFile.write(header_, sizeof(char) * 80);

  short attribute = 0;

  unsigned int numTriangles = buffer.size() / 12;
  myFile.write((char*)&numTriangles, sizeof(int));

  for (unsigned long i = 0; i < numTriangles; i++) {
    myFile.write((char*)&buffer[12 * i], sizeof(float) * 12);
    myFile.write((char*)&attribute, sizeof(short));
  }
  myFile.close();

  std::cout << "polygonize saved binary " << filename << " tris: " << numTriangles << std::endl;
}

void Polygonize::execute(const block::BlockRegister& blockRegister) {
  triangles_.clear();
  WriterReader::execute(blockRegister);

  sendTriangles2Master();

  if (mpiRank_ % groupSize_ == 0) {
    collectTriangles();
  }
}

void Polygonize::collectTriangles() {
  int bufferPos = 0;

  std::vector<float> recvBuffer;

  int numOfWorkersInGroup = groupSize_;
  if (mpiSize_ < (mpiRank_ + groupSize_)) {
    numOfWorkersInGroup = mpiSize_ % groupSize_;
  }

  while (numOfWorkersInGroup > 0) {
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPITriangleExchange, MPI_COMM_WORLD, &status);

    int count;
    MPI_Get_count(&status, MPI_FLOAT, &count);

    recvBuffer.resize(bufferPos + count);
    auto* buffer = recvBuffer.data();
    buffer += bufferPos;
    MPI_Recv(buffer, count, MPI_FLOAT, status.MPI_SOURCE, MPITriangleExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    bufferPos += count;
    numOfWorkersInGroup--;
  }

  std::string filename = makeFilename("triangles", "stl", frame - 1, mpiRank_);
  saveBinaryStl(filename, recvBuffer);
}

void Polygonize::sendTriangles2Master() {
  int rank = mpiRank_ - (mpiRank_ % groupSize_);

  MPI_Wait(&sendRequest_, MPI_STATUS_IGNORE);

  sendBuffer_.clear();
  writeTriangles2Buffer(sendBuffer_);
  MPI_Isend(sendBuffer_.data(), sendBuffer_.size(), MPI_FLOAT, rank, MPITriangleExchange, MPI_COMM_WORLD, &sendRequest_);
}

void Polygonize::writeTriangles2Buffer(std::vector<float>& buffer) const {
  for (const auto& t : triangles_) {
    buffer.push_back((float)t.n[0]);
    buffer.push_back((float)t.n[1]);
    buffer.push_back((float)t.n[2]);

    for (const auto& j : t.p) {
      buffer.push_back((float)j[0]);
      buffer.push_back((float)j[1]);
      buffer.push_back((float)j[2]);
    }
  }
}

}  // namespace nastja
