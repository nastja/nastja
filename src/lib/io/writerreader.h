/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/config/config.h"
#include "lib/simdata/simdata.h"
#include <memory>
#include <string>

namespace nastja {

/**
 * @defgroup io I/O-Action
 * Provides actions for the input and output.
 *
 * @ingroup actions
 */

/**
 * @defgroup io-in Input actions
 * Provides actions for the input.
 *
 * @ingroup io
 */

/**
 * @defgroup io-out Output actions
 * Provides actions for the output.
 *
 * @ingroup io
 */

class WriterReader : public Action {
public:
  explicit WriterReader(const std::string& fieldname) {
    registerFieldname(fieldname);
  }
  ~WriterReader() override              = default;
  WriterReader(const WriterReader&)     = delete;
  WriterReader(WriterReader&&) noexcept = default;
  WriterReader& operator=(const WriterReader&) = delete;
  WriterReader& operator=(WriterReader&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override = 0;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return isSkipped(); }

  std::string makeFilename(const std::string& fieldname, const std::string& extension, int frame, math::Vector3<unsigned long> blockOffset) const;
  std::string makeFilename(const std::string& fieldname, const std::string& extension, int frame, int rank) const;
  std::string makeFilename(const std::string& fieldname, const std::string& extension, int rank) const;
  std::string makeFilenameFrame(const std::string& fieldname, const std::string& extension, int frame) const;
  std::string makeFilenameFrame(const std::string& fieldname, const std::string& extension, const int frame, const int group) const;

  static void checkPath(const std::string& filename);

  void dump(Archive& ar) const override {
    ar.pack(frame);
  }

  void undump(Archive& ar) override {
    ar.unpack(frame);
  }

protected:
  /**
   * Determines if this time-step has to be skipped.
   * Only every nth is not skipped, thereby the offset is considered. In the case that the initial status should be
   * written out, the first time-step will not skipped. The last time-step is always written.
   *
   * @return True if skipped, false otherwise.
   */
  bool isSkipped() const {
    return !((getSimData().timestep == 0 && getSimData().outputFilling) ||                                // initial output
             (getSimData().timestep - offset == getSimData().getTimesteps()) ||                           // last time-step
             ((getSimData().timestep > offset) && (getSimData().timestep - offset) % eachNthStep == 0));  // each n-th time-step
  }

  unsigned int frame{0};  ///< The frame counter for output file.

private:
  static bool isDir(const std::string& path);
  static bool makeDir(const std::string& path);
  static bool makeDirRecursive(const std::string& path);
  static std::string getDirName(const std::string& path);

  const std::string& getBaseDir() const { return getSimData().getCmdLine().outdir; }

  std::string basename_{"output"};  ///< The base name of the output file name.
};

}  // namespace nastja
