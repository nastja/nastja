/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "neighbors.h"
#include "lib/stencil/D3C6.h"
#include <cstdio>

namespace nastja {

using namespace stencil;

void WriteNeighbors::execute(const block::BlockRegister& blockRegister) {
  frame++;
  if (blockRegister.empty()) return;

  int mpiGlobalRank = getSimData().mpiData.getRank();

  std::string filename = makeFilename("neighbors", "json", frame - 1, mpiGlobalRank);

  FILE* fp = fopen(filename.c_str(), "w");

  if (fp == nullptr) perror("file");

  for (const auto& block : blockRegister) {
    fprintf(fp, "%3d: [", block.first);
    for (auto i : D3C6()) {
      if (block.second->getLocalNeighborID(D3C6::dir[i]) == -1) {
        fprintf(fp, "   (   ) ");
      } else {
        fprintf(fp, "%3d(%3d) ", block.second->getLocalNeighborID(D3C6::dir[i]), block.second->getLocalNeighborRank(D3C6::dir[i]));
      }
    }
    fprintf(fp, "] [");
    for (int z = -2; z <= 2; z++) {
      for (int y = -2; y <= 2; y++) {
        for (int x = -2; x <= 2; x++) {
          if (abs(x) + abs(y) + abs(z) >= 3) continue;
          if (block.second->getLocalNeighborID(x, y, z) == -1) {
            fprintf(fp, "   (   ) ");
          } else {
            fprintf(fp, "%3d(%3d) ", block.second->getLocalNeighborID(x, y, z), block.second->getLocalNeighborRank(x, y, z));
          }
        }
      }
    }
    fprintf(fp, "]\n");
  }

  fclose(fp);
}

}  // namespace nastja
