/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "voxelizer.h"
#include "lib/io/voxelstatemachine.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

namespace nastja {

/**
 * read binary STL we don't need it yet
 *
 * @param filename   filename of the file
 * @param triangles  The triangles.
 */
void Voxelizer::readBinaryStl(const std::string& filename, std::vector<math::Triangle>& triangles) {
  std::ifstream stl_file;
  stl_file.open(filename.c_str(), std::ios::in | std::ios::binary);

  if (!stl_file.is_open()) {
    std::cout << "voxelizer : could not read file " << filename << std::endl;
  } else {
    unsigned int through = 0;
    char h[80];
    for (int i = 0; i < 80; i++) {
      stl_file.read((char*)&h[i], sizeof(char));
    }

    unsigned int numOfTriangles;
    stl_file.read((char*)&numOfTriangles, sizeof(int));
    std::cout << "voxelizer reading binary stl " << filename << " numOfTriangles = " << std::to_string(numOfTriangles) << std::endl;

    while (through < numOfTriangles) {
      float values[12];
      short attribute;

      for (float& val : values) {
        stl_file.read((char*)&val, sizeof(float));
      }
      stl_file.read((char*)&attribute, sizeof(short));

      //add triangles
      math::Triangle singleTriangle;
      singleTriangle.n[0] = values[0];
      singleTriangle.n[1] = values[1];
      singleTriangle.n[2] = values[2];

      singleTriangle.p[2][0] = values[3];
      singleTriangle.p[2][1] = values[4];
      singleTriangle.p[2][2] = values[5];
      singleTriangle.p[1][0] = values[6];
      singleTriangle.p[1][1] = values[7];
      singleTriangle.p[1][2] = values[8];
      singleTriangle.p[0][0] = values[9];
      singleTriangle.p[0][1] = values[10];
      singleTriangle.p[0][2] = values[11];

      singleTriangle.normalize();
      triangles.push_back(singleTriangle);

      through++;
    }
  }
  stl_file.close();
}

math::BoundingBox<long> Voxelizer::createBoundingBox(const math::Triangle singleTriangle) {
  math::Vector3<real_t> min = singleTriangle.p[0];
  math::Vector3<real_t> max = singleTriangle.p[0];
  math::Vector3<real_t> a1;
  math::Vector3<real_t> a2;
  math::Vector3<real_t> b1;
  math::Vector3<real_t> b2;

  for (int i = 1; i < 3; i++) {
    min[0] = std::fmin(min[0], singleTriangle.p[i][0]);
    min[1] = std::fmin(min[1], singleTriangle.p[i][1]);
    min[2] = std::fmin(min[2], singleTriangle.p[i][2]);

    max[0] = std::fmax(max[0], singleTriangle.p[i][0]);
    max[1] = std::fmax(max[1], singleTriangle.p[i][1]);
    max[2] = std::fmax(max[2], singleTriangle.p[i][2]);
  }
  a1[0] = min[0] + 0.5 * singleTriangle.n[0];
  a1[1] = min[1] + 0.5 * singleTriangle.n[1];
  a1[2] = min[2] + 0.5 * singleTriangle.n[2];
  a2[0] = min[0] - 0.5 * singleTriangle.n[0];
  a2[1] = min[1] - 0.5 * singleTriangle.n[1];
  a2[2] = min[2] - 0.5 * singleTriangle.n[2];

  b1[0] = max[0] + 0.5 * singleTriangle.n[0];
  b1[1] = max[1] + 0.5 * singleTriangle.n[1];
  b1[2] = max[2] + 0.5 * singleTriangle.n[2];
  b2[0] = max[0] - 0.5 * singleTriangle.n[0];
  b2[1] = max[1] - 0.5 * singleTriangle.n[1];
  b2[2] = max[2] - 0.5 * singleTriangle.n[2];

  min[0] = std::floor(std::fmin(a1[0], a2[0]));
  min[1] = std::floor(std::fmin(a1[1], a2[1]));
  min[2] = std::floor(std::fmin(a1[2], a2[2]));
  max[0] = std::ceil(std::fmax(b1[0], b2[0]));
  max[1] = std::ceil(std::fmax(b1[1], b2[1]));
  max[2] = std::ceil(std::fmax(b1[2], b2[2]));

  math::Vector3<real_t> upperRight{max[0], max[1], max[2]};
  math::Vector3<real_t> lowerLeft{min[0], min[1], min[2]};

  return math::BoundingBox<long>(upperRight, lowerLeft);
}

void Voxelizer::voxelizeSurface(field::Field<real_t, 1>& field) {
  std::vector<math::Triangle> triangles;

  readBinaryStl(stlFileName_, triangles);
  math::BoundingBox<long> bound_box;

  const auto& boundarySize = field.getBoundarySize();
  const auto& offset       = field.getOffset();
  const auto size          = field.getSize() - 2 * boundarySize;

  real_t distance_normal;

  for (auto& triangle : triangles) {
    bound_box = createBoundingBox(triangle);

    bound_box.intersect(math::BoundingBox<long>(offset, offset + size));
    bound_box.translate(-offset[0], -offset[1], -offset[2]);

    for (unsigned int z = bound_box.zMin(); z < bound_box.zMax(); z++) {
      for (unsigned int y = bound_box.yMin(); y < bound_box.yMax(); y++) {
        for (unsigned int x = bound_box.xMin(); x < bound_box.xMax(); x++) {
          if (pointInTriangle(x, y, z, triangle.p[0], triangle.p[1], triangle.p[2])) {
            real_t distance = triangle.getDistance(x + offset[0], y + offset[1], z + offset[2]);
            distance_normal = triangle.dotProductNormal(1, 0, 0);
            if ((distance > -0.866) && (distance <= 0.866)) {
              if (distance_normal < 0) {
                field.getCell(x + boundarySize[0], y + boundarySize[1], z + boundarySize[2]) = 0.25;
              } else if (distance_normal > 0) {
                field.getCell(x + boundarySize[0] - 1, y + boundarySize[1], z + boundarySize[2]) = 0.75;
              }
            }
          }
        }
      }
    }
  }
}

void Voxelizer::voxelizeStructure(field::Field<real_t, 1>& field, std::vector<int>& incomingPlane) {
  const auto& boundarySize = field.getBoundarySize();

  VoxelStateMachine penState;
  for (unsigned long z = boundarySize[2] - 1; z < field.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1] - 1; y < field.getSize(1) - boundarySize[1]; y++) {
      penState.setUp(y, z, field, incomingPlane[z * (field.getSize(1) - 2 * boundarySize[1]) + y]);
      while (penState.getState() != VoxelState::end) {
        penState.execute_state();
      }
    }
  }
}

bool Voxelizer::sameSide(const int x, const int y, const int z, const math::Vector3<real_t> a, const math::Vector3<real_t> b, const math::Vector3<real_t> c) {
  math::Vector3<real_t> v1;
  math::Vector3<real_t> v2;
  math::Vector3<real_t> cp1;
  math::Vector3<real_t> cp2;
  v1    = b - a;
  v2[0] = x + 0.5 - a[0];
  v2[1] = y + 0.5 - a[1];
  v2[2] = z + 0.5 - a[2];
  cp1   = crossProduct(v1, v2);
  v2    = c - a;
  cp2   = crossProduct(v1, v2);
  return dotProduct(cp1, cp2) >= 0;
}

bool Voxelizer::pointInTriangle(const int x, const int y, const int z, const math::Vector3<real_t> a, const math::Vector3<real_t> b, const math::Vector3<real_t> c) {
  return sameSide(x, y, z, a, b, c) && sameSide(x, y, z, b, a, c) && sameSide(x, y, z, c, a, b);
}

/*  There are 2 types of counts. One keeps a count of the number of raises and falls together while the other keeps a count of open ended raises.
 *  Both of these counts are communicated from one block to the next by using MPI.
 *  Firstly, we open receive buffers for all blocks and wait for any block that has processed its count.
 *  Each block processes the two types of counts and sends them to the neighbouring block which receives it.
 *  The leftmost block will not receive anything and will directly open to the right.
 *  The rightmost block does not send anything and will open to the left.
 *  When the last block is reached a final count is calculated from the 2 counts.
 *  Then this final count is sent from the rightmost block to the leftmost in reverse direction being added to each block's first type of count.
 *  This is followed by filling the structure.
*/

void Voxelizer::execute(const block::BlockRegister& blockRegister) {
  int numOfBlocks = 0;
  int index       = 0;

  for (const auto& block : blockRegister) {
    auto& field = block.second->getFieldByName(getFieldname())->get<real_t>();

    voxelizeSurface(field);
  }

  numOfBlocks = blockRegister.size();

  std::vector<MPI_Request> requestWaitingLeft;
  std::vector<MPI_Request> requestWaitingRight;

  leftBuffer_.resize(numOfBlocks);
  rightBuffer_.resize(numOfBlocks);
  int counter = 0;
  for (const auto& block : blockRegister) {
    int size = block.second->size[1] * block.second->size[2];

    leftBuffer_[counter].resize(2 * size, 0);
    rightBuffer_[counter].resize(2 * size, 0);

    if (block.second->blockOffset[0] != 0) {
      MPI_Request request;
      MPI_Irecv(leftBuffer_[counter].data(), leftBuffer_[counter].size(), MPI_INT, block.second->getLocalNeighborRank(stencil::Direction::L), MPICountExchange + block.second->id, MPI_COMM_WORLD, &request);
      requestWaitingLeft.push_back(request);
    } else {
      doRight(rightBuffer_[block.second->id], block.second->id, blockRegister, requestWaitingRight);
    }
    counter++;
  }

  int cnt = requestWaitingLeft.size();
  while (cnt != 0) {
    MPI_Status status;
    MPI_Waitany(requestWaitingLeft.size(), requestWaitingLeft.data(), &index, &status);
    if (index == MPI_UNDEFINED) break;
    int blockID = status.MPI_TAG - MPICountExchange;
    doRight(rightBuffer_[blockID], blockID, blockRegister, requestWaitingRight);
    cnt--;
  }

  // requestWaitingRight[0] = MPI_REQUEST_NULL;
  // requestWaitingRight[1] = MPI_REQUEST_NULL;

  cnt = requestWaitingRight.size();
  while (cnt != 0) {
    MPI_Status status;
    MPI_Waitany(requestWaitingRight.size(), requestWaitingRight.data(), &index, &status);
    if (index == MPI_UNDEFINED) break;
    doLeft(status.MPI_TAG - MPICountExchange, blockRegister);
    cnt--;
  }

  index = 0;
  for (const auto& block : blockRegister) {
    auto& field = block.second->getFieldByName(getFieldname())->get<real_t>();

    voxelizeStructure(field, leftBuffer_[index]);
    index++;
  }
}

/* Here the count is sent on the way back from right to left
 * Left buffer count is incremented by the calculated number in right buffer
 * The right buffer is sent to the block on the left
*/
void Voxelizer::doLeft(const int blockID, const block::BlockRegister& blockRegister) {
  int index   = blockID;  // BUG: Workaround for one core
  auto* block = blockRegister.find(blockID)->second;

  auto& field = block->getFieldByName(getFieldname())->get<real_t>();

  int size = (field.getSize(2) - 2 * field.getBoundarySize(2)) * (field.getSize(1) - 2 * field.getBoundarySize(1));

  for (int i = 0; i < size; i++) {
    leftBuffer_[index][i] += rightBuffer_[index][i];
  }

  leftBuffer_[index].resize(size);
  rightBuffer_[index].resize(size);

  if (block->blockOffset[0] != 0) {
    MPI_Send(rightBuffer_[index].data(), rightBuffer_[index].size(), MPI_INT, block->getLocalNeighborRank(stencil::Direction::L), MPICountExchange + block->getLocalNeighborID(stencil::Direction::L), MPI_COMM_WORLD);
  }
}

/* First the two types of counts are processed
 * If the current block is not the last block, we send the counts to right and open the receive on the right
 * If the current block is the last block, we calculate the number to be put in right buffer and start going backwards by calling doLeft()
*/
void Voxelizer::doRight(std::vector<int>& rightBuffer, const int blockID, const block::BlockRegister& blockRegister, std::vector<MPI_Request>& requestWaitingRight) {
  int index   = blockID;  // BUG: Workaround for one core
  auto* block = blockRegister.find(blockID)->second;

  auto& field = block->getFieldByName(getFieldname())->get<real_t>();

  processCount(leftBuffer_[index], rightBuffer, field);
  int size = (field.getSize(2) - 2 * field.getBoundarySize(2)) * (field.getSize(1) - 2 * field.getBoundarySize(1));

  if (block->blockOffset[0] != (block->blocks[0] - 1)) {
    MPI_Send(rightBuffer.data(), rightBuffer.size(), MPI_INT, block->getLocalNeighborRank(stencil::Direction::R), MPICountExchange + block->getLocalNeighborID(stencil::Direction::R), MPI_COMM_WORLD);
    MPI_Request request;
    MPI_Irecv(rightBuffer.data(), rightBuffer.size() / 2, MPI_INT, block->getLocalNeighborRank(stencil::Direction::R), MPICountExchange + blockID, MPI_COMM_WORLD, &request);
    requestWaitingRight.push_back(request);
  } else {
    for (int i = 0; i < size; i++) {
      rightBuffer[i] = rightBuffer[size + i] - rightBuffer[i];
    }
    doLeft(blockID, blockRegister);
  }
}

/**
 * incoming plane vector is a vector within left buffer and outgoing plane
 * vector is a vector within right buffer The first type of count is stored in
 * first half of right buffer and the second type is stored in second half
 *
 * @param incomingPlane  The incoming plane.
 * @param outgoingPlane  The outgoing plane.
 * @param field          The field.
 */
void Voxelizer::processCount(const std::vector<int>& incomingPlane, std::vector<int>& outgoingPlane, field::Field<real_t, 1>& field) {
  const auto& boundarySize = field.getBoundarySize();

  int size = (field.getSize(2) - 2 * boundarySize[2]) * (field.getSize(1) - 2 * boundarySize[1]);

  for (unsigned long z = boundarySize[2]; z < field.getSize(2) - boundarySize[2]; z++) {
    for (unsigned long y = boundarySize[1]; y < field.getSize(1) - boundarySize[1]; y++) {
      int countRaiseFall = incomingPlane[(z - boundarySize[2]) * (field.getSize(1) - 2 * boundarySize[1]) + (y - boundarySize[1])];
      int countRaise     = incomingPlane[(z - boundarySize[2]) * (field.getSize(1) - 2 * boundarySize[1]) + (y - boundarySize[1]) + size];

      for (unsigned long x = boundarySize[0]; x < field.getSize(0) - boundarySize[0]; x++) {
        if (field.getCell(x, y, z) == 0.25) {
          countRaiseFall++;
          countRaise++;
        } else if (field.getCell(x, y, z) == 0.75) {
          countRaiseFall--;
          countRaise--;
          if (countRaise < 0) { countRaise = 0; }
        }
      }

      outgoingPlane[(z - boundarySize[2]) * (field.getSize(1) - 2 * boundarySize[1]) + (y - boundarySize[1])]        = countRaiseFall;
      outgoingPlane[(z - boundarySize[2]) * (field.getSize(1) - 2 * boundarySize[1]) + (y - boundarySize[1]) + size] = countRaise;
    }
  }
}

void Voxelizer::executeBlock(block::Block* /*block*/) {}

}  // namespace nastja
