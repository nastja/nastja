/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger.h"
#include "lib/io/writerreader.h"
#include <fstream>
#include <sstream>
#include <string>

namespace nastja {

/**
 * Class for writing the load, the number of blocks per process.
 *
 * @ingroup io io-out
 */
class WriteLoad : public WriterReader {
public:
  explicit WriteLoad(const std::string& /*fieldname*/)
      : WriterReader{""} {}

  ~WriteLoad() override       = default;
  WriteLoad(const WriteLoad&) = delete;
  WriteLoad(WriteLoad&&)      = default;
  WriteLoad& operator=(const WriteLoad&) = delete;
  WriteLoad& operator=(WriteLoad&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) override {
    std::string filename = makeFilename("load", "dat", getSimData().mpiData.getRank());

    checkPath(filename);
    logger::registerLogger("load", filename);
    logger::get("load").setFormat(logger::Level::off, "");
  }

  void execute(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); };
  bool skip() const override { return false; }
};

}  // namespace nastja
