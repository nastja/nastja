/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/io/writerreader.h"

namespace nastja {

/**
 * Class for writing parallel vtk image via MPI-I/O.
 *
 * @ingroup io io-out
 */
class ParallelVtkImage : public WriterReader {
public:
  explicit ParallelVtkImage(const std::string& fieldname)
      : WriterReader{fieldname} {}

  ~ParallelVtkImage() override {
    if (info_ != MPI_INFO_NULL) MPI_Info_free(&info_);
    if (subarray_ != MPI_DATATYPE_NULL) MPI_Type_free(&subarray_);
    if (mpiType_ != MPI_DATATYPE_NULL) MPI_Type_free(&mpiType_);
  }

  ParallelVtkImage(const ParallelVtkImage&)     = delete;
  ParallelVtkImage(ParallelVtkImage&&) noexcept = default;
  ParallelVtkImage& operator=(const ParallelVtkImage&) = delete;
  ParallelVtkImage& operator=(ParallelVtkImage&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return isSkipped(); }

private:
  void setupFrame();

  /**
   * Switch function for getting the output data type.
   *
   * @param fh     The file handle.
   * @param field  The field.
   *
   * @tparam T  The data type of the field.
   */
  template <typename T, unsigned int TSplitX>
  void doWrite(MPI_File& fh, field::Field<T, TSplitX>& field) const {
    if (dataType_ == "Float32") {
      doWrite(fh, field, static_cast<float>(0));
    } else if (dataType_ == "Float64") {
      doWrite(fh, field, static_cast<double>(0));
    } else if (dataType_ == "UInt32") {
      doWrite(fh, field, static_cast<unsigned int>(0));
    } else if (dataType_ == "UInt64") {
      doWrite(fh, field, static_cast<unsigned long>(0));
    } else {
      throw std::invalid_argument(fmt::format("Data type '{}' for output is unknown.", dataType_));
    }
  }

  /**
   * Write field to file.
   *
   * @param fh     The file handle.
   * @param field  The field.
   * @param value  A dummy value for distinguish template.
   *
   * @tparam T  The data type of the field.
   * @tparam U  The data type of the output.
   */
  template <typename T, unsigned int TSplitX, typename U>
  void doWrite(MPI_File& fh, field::Field<T, TSplitX>& field, U value) const;

  MPI_Info info_{MPI_INFO_NULL};              ///< The MPI IO info struct.
  MPI_File fh_{};                             ///< The MPI IO file handle
  MPI_Datatype subarray_{MPI_DATATYPE_NULL};  ///< The MPI subarray of the inner of the block.

  int groupSize_{};              ///< The group size for collecting the output.  

  bool printHints_{false};  ///< Flag for printing the MPI IO hints.

  std::string header_;                                     ///< The sting of the header.
  std::string footer_{"\n</AppendedData>\n</VTKFile>\n"};  ///< The sting of the footer.

  unsigned long dataLength_{};  ///< The length of the binary data, this are the first 8 bytes in the raw-data stream.

  std::size_t dataSize_{};                ///< The size of one data element.
  std::string dataType_;                  ///< The vtk data type string.
  std::vector<unsigned int> components_;  ///< vector holding the componants for output
  std::string outputFieldName_;           ///< option to set the output field name

  MPI_Datatype mpiType_{MPI_DATATYPE_NULL};  ///< The MPI data type corresponding to the vtk type.

  MPI_Comm row_comm;///< The MPI communicator into which MPI_COMM_WORLD is split if groupSize_ > 0
  int world_rank, world_size; ///< The MPI rank in MPI_COMM_WORLD
  int row_rank, row_size; ///< The MPI rank in row_comm
  int color; ///< The color determining into which sub_communicators the respective rank will be sorted
  math::Vector3<unsigned long> blockOffsetLocal{0,0,0}; ///< Required for correct file offset
  math::Vector3<unsigned long> blocksLocal{0,0,0}; ///< Required for correct file offset
};

}  // namespace nastja
