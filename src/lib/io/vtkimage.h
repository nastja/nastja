/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/io/writerreader.h"
#include "lib/math/boundingbox.h"
#include <vector>

namespace nastja {

/**
 * Class for writing vtk image.
 *
 * @ingroup io io-out
 */
class VtkImage : public WriterReader {
public:
  explicit VtkImage(const std::string& fieldname)
      : WriterReader{fieldname} {}
  ~VtkImage() override          = default;
  VtkImage(const VtkImage&)     = delete;
  VtkImage(VtkImage&&) noexcept = default;
  VtkImage& operator=(const VtkImage&) = delete;
  VtkImage& operator=(VtkImage&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  bool skip() const override { return isSkipped(); }

private:
  /**
   * Write field to file.
   *
   * @param fp     The file pointer
   * @param field  The field.
   * @param value  A dummy value for distinguish template.
   *
   * @tparam T  The data type of the field.
   * @tparam U  The data type of the output.
   */
  template <typename T, unsigned int TSplitX, typename U>
  void doWrite(FILE* fp, field::Field<T, TSplitX>& field, U value) const;

  /**
   * Wrapper function for getting the output data type
   *
   * @param fp     The file pointer.
   * @param field  The field.
   *
   * @tparam T  The data type of the field.
   */
  template <typename T, unsigned int TSplitX>
  void doWrite(FILE* fp, field::Field<T, TSplitX>& field) const {
    if (dataType_ == "Float32") {
      doWrite(fp, field, static_cast<float>(0));
    } else if (dataType_ == "Float64") {
      doWrite(fp, field, static_cast<double>(0));
    } else if (dataType_ == "UInt32") {
      doWrite(fp, field, static_cast<unsigned int>(0));
    } else if (dataType_ == "UInt64") {
      doWrite(fp, field, static_cast<unsigned long>(0));
    } else {
      throw std::invalid_argument(fmt::format("Data type '{}' for output is unknown.", dataType_));
    }
  }

  std::size_t dataSize_{};                              ///< size of one data element
  std::string dataType_;                                ///< vtk data type string
  std::vector<unsigned int> components_;                ///< vector holding the components for output
  nastja::math::BoundingBox<unsigned long, 3UL> bbox_;  ///< bounding box to specify output
  bool useLocalBbox_;                                   ///< flag indicating the use of a specific bounding box
  bool useBoundary_;                                    ///< flag indicating if the boundary is included in the output/boundingbox
  std::string outputFieldName_;                         ///< option to set the output field name
};

}  // namespace nastja
