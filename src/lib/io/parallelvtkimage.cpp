/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "parallelvtkimage.h"
#include "lib/config/configutils.h"
#include "lib/field/field.h"
#include "fmt/format.h"
#include <cctype>
#include <cstdlib>
#include <numeric>

namespace nastja {

/**
 * Calculates lustre stripe size from string.
 * Lustre minimum block size is 64k blocks, all other blocks have to be a multiple of 64k.
 * Numbers in human-readable format with k, M or G are recognized.
 *
 * @param str  Pointer to the string.
 *
 * @return size in bytes. If nothing was recognized lustre default of 1M is returned.
 */
long getStripeSize(const char* str) {
  char* end_ptr;

  long i = std::strtol(str, &end_ptr, 10);

  // default 1M
  if (i == 0) return 1u << 20u;

  while (std::isspace(static_cast<unsigned char>(*end_ptr)) != 0) end_ptr++;

  long exp = 0;
  switch (std::toupper(static_cast<unsigned char>(*end_ptr))) {
    case 'K': exp = 1; break;
    case 'M': exp = 2; break;
    case 'G': exp = 3; break;
    default: break;
  }

  i *= (1u << (10u * exp));

  // round up to next 64k block
  if ((i % 65536) != 0) {
    i = ((i / 65536) + 1) * 65536;
  }

  return i;
}

void ParallelVtkImage::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  WriterReader::init(blockRegister, fieldpool);
  auto& config = getSimData().getConfig();
  auto jptr    = config::getJsonPointer("Writers." + getName());

  /// @key{Writers.\template{writer=ParallelVtkImage}.steps, int, 1}
  /// Number of each nth steps when the output is written.
  eachNthStep = config.getValue<int>(jptr / "steps", 1);

  /// @key{Writers.\template{writer=ParallelVtkImage}.outputfieldname, string, ""}
  /// The output data type.
  outputFieldName_ = config.getValue<std::string>(jptr / "outputfieldname", getFieldname());


  /// @key{Writers.\template{writer=ParallelVtkImage}.groupsize, int, 0}
  /// Group size for collecting the output from n workers. 0 for master output.
  groupSize_ = config.getValue<int>(jptr / "groupsize", 0);

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  color = groupSize_ > 0 ? world_rank / groupSize_ : 0; // Determine color based on row


  if (groupSize_ > 0){
  // Split the communicator based on the color and use the
  // original rank for ordering
    MPI_Comm_split(MPI_COMM_WORLD, color, world_rank, &row_comm);
  }
  else MPI_Comm_dup(MPI_COMM_WORLD, &row_comm);


  MPI_Comm_rank(row_comm, &row_rank);
  MPI_Comm_size(row_comm, &row_size);

  if (blockRegister.empty()) return;

  auto* block = blockRegister.begin()->second;
  auto* field = block->getFieldByName(getFieldname());

  /// @key{Writers.\template{writer=ParallelVtkImage}.outputtype, string, "Float32"}
  /// @values{"Float32", "Float64", "UInt32", "UInt64"}
  /// The output data type.
  dataType_ = config.getValue<std::string>(jptr / "outputtype", "Float32");
  MPI_Datatype mpiElementType;
  if (dataType_ == "Float32") {
    dataSize_      = sizeof(float);
    mpiElementType = MPI_FLOAT;
  } else if (dataType_ == "Float64") {
    dataSize_      = sizeof(double);
    mpiElementType = MPI_DOUBLE;
  } else if (dataType_ == "UInt32") {
    dataSize_      = sizeof(unsigned int);
    mpiElementType = MPI_UNSIGNED;
  } else if (dataType_ == "UInt64") {
    dataSize_      = sizeof(unsigned long);
    mpiElementType = MPI_UNSIGNED_LONG;
  } else {
    throw std::invalid_argument(fmt::format("Data type '{}' for output is unknown.", dataType_));
  }

  /// @key{Writers.\template{writer=ParallelVtkImage}.component, int[], optional}
  /// Write out only given components, default write out all components.
  auto comp_jptr = jptr / "components";
  if (config.contains(comp_jptr)) {
    if (!config.isArray(comp_jptr)) {
      throw std::invalid_argument(fmt::format("{} must be an array when given.", comp_jptr));
    }
    components_ = config.getVector<unsigned int>(comp_jptr);
  } else {
    // default fill vector from 0 to vectorsize
    const auto vectorSize = field->getVectorSize();
    components_.resize(vectorSize);
    std::iota(components_.begin(), components_.end(), 0);
  }

  const auto vectorSize = components_.size();

  MPI_Type_contiguous(vectorSize, mpiElementType, &mpiType_);
  MPI_Type_commit(&mpiType_);

  const auto& boundarySize = field->getBoundarySize();

  math::Vector3<int> starts{0, 0, 0};
  auto blockSize = static_cast<math::Vector3<int>>(field->getSize() - 2 * boundarySize);
  math::Vector3<int> fullSizes{static_cast<int>(blockSize[0]) * static_cast<int>(block->blocks[0]),
                                     static_cast<int>(blockSize[1]) * static_cast<int>(block->blocks[1]),
                                     static_cast<int>(blockSize[2]) * static_cast<int>(block->blocks[2])};


  if (groupSize_ > 0) {
    std::vector<long> offset_data_x(row_size,0);
    std::vector<long> offset_data_y(row_size,0);
    std::vector<long> offset_data_z(row_size,0);
    std::vector<long> blockoffset_data_x(row_size,0);
    std::vector<long> blockoffset_data_y(row_size,0);
    std::vector<long> blockoffset_data_z(row_size,0);    

    auto offset_x = static_cast<long>(block->offset[0]);
    auto offset_y = static_cast<long>(block->offset[1]);
    auto offset_z = static_cast<long>(block->offset[2]);
    auto block_offset_x = static_cast<long>(block->blockOffset[0]);
    auto block_offset_y = static_cast<long>(block->blockOffset[1]);
    auto block_offset_z = static_cast<long>(block->blockOffset[2]);    
    
    MPI_Allgather(&offset_x, 1, MPI_LONG, &offset_data_x.front(), 1, MPI_LONG, row_comm);
    MPI_Allgather(&offset_y, 1, MPI_LONG, &offset_data_y.front(), 1, MPI_LONG, row_comm);
    MPI_Allgather(&offset_z, 1, MPI_LONG, &offset_data_z.front(), 1, MPI_LONG, row_comm);
    MPI_Allgather(&block_offset_x, 1, MPI_LONG, &blockoffset_data_x.front(), 1, MPI_LONG, row_comm);
    MPI_Allgather(&block_offset_y, 1, MPI_LONG, &blockoffset_data_y.front(), 1, MPI_LONG, row_comm);
    MPI_Allgather(&block_offset_z, 1, MPI_LONG, &blockoffset_data_z.front(), 1, MPI_LONG, row_comm);    

    auto min_val = std::min_element(offset_data_x.begin(),offset_data_x.end());
    auto max_val = std::max_element(offset_data_x.begin(),offset_data_x.end());
    auto min_blockoffset = std::min_element(blockoffset_data_x.begin(),blockoffset_data_x.end());
    auto max_blockoffset = std::max_element(blockoffset_data_x.begin(),blockoffset_data_x.end());
    blockOffsetLocal[0] = block->blockOffset[0] - *min_blockoffset;//block->blockOffset[0] - *min_val/static_cast<int>(blockSize[0]);    
    starts[0] = *min_val;
    blocksLocal[0] = *max_blockoffset - *min_blockoffset + 1 > 0 ?  *max_blockoffset - *min_blockoffset + 1 : 1;//*max_val/static_cast<int>(blockSize[0]) -*min_val/static_cast<int>(blockSize[0]) +1;

    fullSizes[0] = static_cast<int>(blocksLocal[0]) * static_cast<int>(blockSize[0]);

    min_val = std::min_element(offset_data_y.begin(),offset_data_y.end());
    max_val = std::max_element(offset_data_y.begin(),offset_data_y.end());
    min_blockoffset = std::min_element(blockoffset_data_y.begin(),blockoffset_data_y.end());
    max_blockoffset = std::max_element(blockoffset_data_y.begin(),blockoffset_data_y.end());
    blockOffsetLocal[1] = block->blockOffset[1] - *min_blockoffset;
    starts[1] = *min_val;
    blocksLocal[1] = *max_blockoffset - *min_blockoffset + 1 > 0 ?  *max_blockoffset - *min_blockoffset + 1 : 1;//*max_val/static_cast<int>(blockSize[1]) -*min_val/static_cast<int>(blockSize[1]) +1;    
    fullSizes[1] = static_cast<int>(blocksLocal[1]) * static_cast<int>(blockSize[1]);

    min_val = std::min_element(offset_data_z.begin(),offset_data_z.end());
    max_val = std::max_element(offset_data_z.begin(),offset_data_z.end());
    min_blockoffset = std::min_element(blockoffset_data_z.begin(),blockoffset_data_z.end());
    max_blockoffset = std::max_element(blockoffset_data_z.begin(),blockoffset_data_z.end());
    blockOffsetLocal[2] = block->blockOffset[2] - *min_blockoffset;    
    starts[2] = *min_val;
    blocksLocal[2] = *max_blockoffset - *min_blockoffset + 1 > 0 ?  *max_blockoffset - *min_blockoffset + 1 : 1;//*max_val/static_cast<int>(blockSize[2]) -*min_val/static_cast<int>(blockSize[2]) +1;    
    fullSizes[2] = static_cast<int>(blocksLocal[2]) * static_cast<int>(blockSize[2]);

      
  }

  dataLength_ = long(fullSizes[0]) * long(fullSizes[1]) * long(fullSizes[2]) * dataSize_ * vectorSize;

  MPI_Info_create(&info_);

  // Disables ROMIO's data-sieving
  MPI_Info_set(info_, "romio_ds_read", "disable");
  MPI_Info_set(info_, "romio_ds_write", "disable");

  //Enable ROMIO's collective buffering
  MPI_Info_set(info_, "romio_cb_read", "enable");
  MPI_Info_set(info_, "romio_cb_write", "enable");

  /// @key{Writers.\template{writer=ParallelVtkImage}.hints, string[][2], optional}
  /// Array of MPI IO hints.
  auto hints_jptr = jptr / "hints";
  if (config.contains(hints_jptr)) {
    for (unsigned int hint = 0; hint < config.arraySize(jptr / "hints"); hint++) {
      auto hint_jptr = hints_jptr / hint;
      if (config.getValue<std::string>(hint_jptr / 0) == "striping_unit") {
        long stripeSize = getStripeSize(config.getValue<std::string>(hint_jptr / 1).c_str());
        auto stripeUnit = fmt::format("{}", stripeSize);

        MPI_Info_set(info_, config.getValue<std::string>(hint_jptr / 0).c_str(), stripeUnit.c_str());
      } else {
        MPI_Info_set(info_, config.getValue<std::string>(hint_jptr / 0).c_str(), config.getValue<std::string>(hint_jptr / 1).c_str());
      }
      logger::get().oneinfo("Append hint: {}={}", config.getValue<std::string>(hint_jptr / 0), config.getValue<std::string>(hint_jptr / 1));
    }
  }

  /// @key{Writers.\template{writer=ParallelVtkImage}.printhints, bool, false}
  /// Flag enabels the output of the MPI IO hints.
  printHints_ = config.getValue<bool>(jptr / "printhints", false);

  char binLength[8];
  std::memcpy(binLength, &dataLength_, sizeof(dataLength_));

  header_ = fmt::format(
      "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n"
      "<ImageData WholeExtent=\"{} {} {} {} {} {}\" Origin=\"0 0 0\" Spacing=\"{:e} {:e} {:e}\">\n"
      "<CellData Scalars=\"{}\">\n"
      "<DataArray type=\"{}\" Name=\"{}\" format=\"appended\" offset=\"0\" NumberOfComponents=\"{}\"/>\n"
      "</CellData>\n"
      "</ImageData>\n"
      "<AppendedData encoding=\"raw\">\n"
      "_{:c}{:c}{:c}{:c}{:c}{:c}{:c}{:c}",
      starts[0], groupSize_ > 0 ? starts[0] + fullSizes[0] : fullSizes[0],
      starts[1], groupSize_ > 0 ? starts[1] + fullSizes[1] : fullSizes[1],
      starts[2], groupSize_ > 0 ? starts[2] + fullSizes[2] : fullSizes[2],
      getSimData().deltax, getSimData().deltax, getSimData().deltax,
      getFieldname().c_str(), dataType_.c_str(), getFieldname().c_str(), vectorSize,
      binLength[0], binLength[1], binLength[2], binLength[3], binLength[4], binLength[5], binLength[6], binLength[7]);


  if (groupSize_ == 0) MPI_Type_create_subarray(3, fullSizes.data(), blockSize.data(), starts.data(), MPI_ORDER_FORTRAN, mpiType_, &subarray_);
  else {
    math::Vector3<int> starts_local{0, 0, 0};
    MPI_Type_create_subarray(3, fullSizes.data(), blockSize.data(), starts_local.data(), MPI_ORDER_FORTRAN, mpiType_, &subarray_);
  }
  MPI_Type_commit(&subarray_);
}

/**
 * Creates a file for output and writes the header and footer.
 */
void ParallelVtkImage::setupFrame() {
  std::string filename = groupSize_ == 0 ? makeFilenameFrame(outputFieldName_, "vti", frame) : makeFilenameFrame(outputFieldName_, "vti", frame, getSimData().mpiData.getRank() / groupSize_);
  MPI_File_open(row_comm, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE | MPI_MODE_UNIQUE_OPEN, info_, &fh_);

  if (world_rank > 0){
    if (groupSize_ == 0)return;
    else if (world_rank % groupSize_ != 0) return;
  }

  if (printHints_) {
    MPI_Info info_used = MPI_INFO_NULL;
    int nkeys;
    int flag;

    MPI_File_get_info(fh_, &info_used);
    MPI_Info_get_nkeys(info_used, &nkeys);

    char key[MPI_MAX_INFO_KEY];
    char value[MPI_MAX_INFO_VAL];
    logger::get().oneinfo("First open of file with {} MPI hints:", nkeys);
    for (int i = 0; i < nkeys; i++) {
      MPI_Info_get_nthkey(info_used, i, key);
      MPI_Info_get(info_used, key, MPI_MAX_INFO_VAL, value, &flag);
      logger::get().oneinfo("Hint {}: {} = {}", i, key, value);
    }

    MPI_Info_free(&info_used);
    printHints_ = false;
  }

  MPI_File_write_at(fh_, 0, &header_[0], header_.size(), MPI_BYTE, MPI_STATUS_IGNORE);
  MPI_File_write_at(fh_, header_.size() + dataLength_, &footer_[0], footer_.size(), MPI_BYTE, MPI_STATUS_IGNORE);
}

void ParallelVtkImage::executeBlock(block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  const auto& boundarySize = field->getBoundarySize();
  const auto& vectorSize   = components_.size();

  const auto blockSize = static_cast<math::Vector3<long>>(field->getSize() - 2 * boundarySize);

  long elementSize  = dataSize_ * vectorSize;
  long displacement = elementSize * (block->blockOffset[0] * blockSize[0] +
                                     block->blockOffset[1] * blockSize[0] * block->blocks[0] * blockSize[1] +
                                     block->blockOffset[2] * blockSize[0] * block->blocks[0] * blockSize[1] * block->blocks[1] * blockSize[2]) +
                      header_.size();
  if (groupSize_ > 0) {
    displacement =  elementSize * (blockOffsetLocal[0] * blockSize[0] +
                                  blockOffsetLocal[1] * blockSize[0] * blocksLocal[0] * blockSize[1] +
                                  blockOffsetLocal[2] * blockSize[0] * blocksLocal[0] * blockSize[1] * blocksLocal[1] * blockSize[2]) +
                    header_.size();

  }

  MPI_File_set_view(fh_, displacement, mpiType_, subarray_, "native", info_);

  if (auto* field_ = field->getPtr<real_t, true>()) {
    doWrite(fh_, *field_);
  } else if (auto* field_ = field->getPtr<real_t, false>()) {
    doWrite(fh_, *field_);
  } else if (auto* field_ = field->getPtr<cellid_t, true>()) {
    doWrite(fh_, *field_);
  } else if (auto* field_ = field->getPtr<cellid_t, false>()) {
    doWrite(fh_, *field_);
  } else {
    throw std::invalid_argument(fmt::format("Field '{}' of unknown type.", getFieldname()));
  }
}

void ParallelVtkImage::execute(const block::BlockRegister& blockRegister) {
  if (blockRegister.empty()) return;

  setupFrame();

  WriterReader::execute(blockRegister);

  MPI_File_close(&fh_);
}

template <typename T, unsigned int TSplitX, typename U>
void ParallelVtkImage::doWrite(MPI_File& fh, field::Field<T, TSplitX>& field, U value) const {
  const auto& boundarySize     = field.getBoundarySize();
  const auto& size             = field.getSize();
  const auto vectorSize        = components_.size();
  unsigned long vectorElements = (size[0] - 2 * boundarySize[0]) * (size[1] - 2 * boundarySize[1]) * (size[2] - 2 * boundarySize[2]);

  std::vector<U> buffer;
  buffer.reserve(vectorElements * vectorSize);

#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    field.cuCpDataDeviceToHost();
  }
#endif

  for (unsigned int z = boundarySize[2]; z < size[2] - boundarySize[2]; z++) {
    for (unsigned int y = boundarySize[1]; y < size[1] - boundarySize[1]; y++) {
      for (unsigned int x = boundarySize[0]; x < size[0] - boundarySize[0]; x++) {
        for (const auto v : components_) {
          value = U(field.getCell(x, y, z, v));
          buffer.push_back(value);
        }
      }
    }
  }

  MPI_File_write_all(fh, &buffer[0], vectorElements, mpiType_, MPI_STATUS_IGNORE);
}

template void ParallelVtkImage::doWrite<cellid_t, 1, double>(MPI_File& fh, field::Field<cellid_t, 1>& field, double value) const;
template void ParallelVtkImage::doWrite<cellid_t, 2, double>(MPI_File& fh, field::Field<cellid_t, 2>& field, double value) const;
template void ParallelVtkImage::doWrite<cellid_t, 4, double>(MPI_File& fh, field::Field<cellid_t, 4>& field, double value) const;
template void ParallelVtkImage::doWrite<cellid_t, 8, double>(MPI_File& fh, field::Field<cellid_t, 8>& field, double value) const;
template void ParallelVtkImage::doWrite<cellid_t, 16, double>(MPI_File& fh, field::Field<cellid_t, 16>& field, double value) const;
template void ParallelVtkImage::doWrite<cellid_t, 1, float>(MPI_File& fh, field::Field<cellid_t, 1>& field, float value) const;
template void ParallelVtkImage::doWrite<cellid_t, 2, float>(MPI_File& fh, field::Field<cellid_t, 2>& field, float value) const;
template void ParallelVtkImage::doWrite<cellid_t, 4, float>(MPI_File& fh, field::Field<cellid_t, 4>& field, float value) const;
template void ParallelVtkImage::doWrite<cellid_t, 8, float>(MPI_File& fh, field::Field<cellid_t, 8>& field, float value) const;
template void ParallelVtkImage::doWrite<cellid_t, 16, float>(MPI_File& fh, field::Field<cellid_t, 16>& field, float value) const;
template void ParallelVtkImage::doWrite<cellid_t, 1, unsigned int>(MPI_File& fh, field::Field<cellid_t, 1>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<cellid_t, 2, unsigned int>(MPI_File& fh, field::Field<cellid_t, 2>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<cellid_t, 4, unsigned int>(MPI_File& fh, field::Field<cellid_t, 4>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<cellid_t, 8, unsigned int>(MPI_File& fh, field::Field<cellid_t, 8>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<cellid_t, 16, unsigned int>(MPI_File& fh, field::Field<cellid_t, 16>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<cellid_t, 1, unsigned long>(MPI_File& fh, field::Field<cellid_t, 1>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<cellid_t, 2, unsigned long>(MPI_File& fh, field::Field<cellid_t, 2>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<cellid_t, 4, unsigned long>(MPI_File& fh, field::Field<cellid_t, 4>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<cellid_t, 8, unsigned long>(MPI_File& fh, field::Field<cellid_t, 8>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<cellid_t, 16, unsigned long>(MPI_File& fh, field::Field<cellid_t, 16>& field, unsigned long value) const;

template void ParallelVtkImage::doWrite<real_t, 1, double>(MPI_File& fh, field::Field<real_t, 1>& field, double value) const;
template void ParallelVtkImage::doWrite<real_t, 2, double>(MPI_File& fh, field::Field<real_t, 2>& field, double value) const;
template void ParallelVtkImage::doWrite<real_t, 4, double>(MPI_File& fh, field::Field<real_t, 4>& field, double value) const;
template void ParallelVtkImage::doWrite<real_t, 8, double>(MPI_File& fh, field::Field<real_t, 8>& field, double value) const;
template void ParallelVtkImage::doWrite<real_t, 16, double>(MPI_File& fh, field::Field<real_t, 16>& field, double value) const;
template void ParallelVtkImage::doWrite<real_t, 1, float>(MPI_File& fh, field::Field<real_t, 1>& field, float value) const;
template void ParallelVtkImage::doWrite<real_t, 2, float>(MPI_File& fh, field::Field<real_t, 2>& field, float value) const;
template void ParallelVtkImage::doWrite<real_t, 4, float>(MPI_File& fh, field::Field<real_t, 4>& field, float value) const;
template void ParallelVtkImage::doWrite<real_t, 8, float>(MPI_File& fh, field::Field<real_t, 8>& field, float value) const;
template void ParallelVtkImage::doWrite<real_t, 16, float>(MPI_File& fh, field::Field<real_t, 16>& field, float value) const;
template void ParallelVtkImage::doWrite<real_t, 1, unsigned int>(MPI_File& fh, field::Field<real_t, 1>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<real_t, 2, unsigned int>(MPI_File& fh, field::Field<real_t, 2>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<real_t, 4, unsigned int>(MPI_File& fh, field::Field<real_t, 4>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<real_t, 8, unsigned int>(MPI_File& fh, field::Field<real_t, 8>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<real_t, 16, unsigned int>(MPI_File& fh, field::Field<real_t, 16>& field, unsigned int value) const;
template void ParallelVtkImage::doWrite<real_t, 1, unsigned long>(MPI_File& fh, field::Field<real_t, 1>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<real_t, 2, unsigned long>(MPI_File& fh, field::Field<real_t, 2>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<real_t, 4, unsigned long>(MPI_File& fh, field::Field<real_t, 4>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<real_t, 8, unsigned long>(MPI_File& fh, field::Field<real_t, 8>& field, unsigned long value) const;
template void ParallelVtkImage::doWrite<real_t, 16, unsigned long>(MPI_File& fh, field::Field<real_t, 16>& field, unsigned long value) const;

}  // namespace nastja
