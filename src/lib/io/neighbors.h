/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/io/writerreader.h"

namespace nastja {

/**
 * Class for writing neighbors connections.
 *
 * @ingroup io io-out
 */
class WriteNeighbors : public WriterReader {
public:
  explicit WriteNeighbors(const std::string& /*fieldname*/)
      : WriterReader{""} {}
  ~WriteNeighbors() override                = default;
  WriteNeighbors(const WriteNeighbors&)     = delete;
  WriteNeighbors(WriteNeighbors&&) noexcept = default;
  WriteNeighbors& operator=(const WriteNeighbors&) = delete;
  WriteNeighbors& operator=(WriteNeighbors&&) = delete;

  void execute(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); }
  bool skip() const override { return false; }

  using Action::init;
  void init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) override {}
};

}  // namespace nastja
