/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/io/cellinfo.h"
#include "lib/io/dump.h"
#include "lib/io/measureload.h"
#include "lib/io/neighbors.h"
#include "lib/io/parallelvtkimage.h"
#include "lib/io/polygonize.h"
#include "lib/io/vtkimage.h"
#include "lib/io/writerreader.h"

namespace nastja {

class FactoryIO : public Factory<WriterReader, const std::string&> {
public:
  explicit FactoryIO() {
    registerType<Dump>("Dump");
    registerType<CellInfo>("CellInfo");
    registerType<ParallelVtkImage>("ParallelVtkImage");
    registerType<Polygonize>("Polygonize");
    registerType<WriteLoad>("WriteLoad");
    registerType<WriteNeighbors>("WriteNeighbors");
    registerType<VtkImage>("VtkImage");
  }
};

}  // namespace nastja
