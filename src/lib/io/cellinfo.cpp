/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellinfo.h"
#include "lib/cell.h"
#include "lib/timing.h"
#include "lib/config/config.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/field/field.h"
#include "lib/simdata/datacells.h"
#include "fmt/format.h"
#include "fmt/ranges.h"
#include <cstdio>
#include <fstream>

namespace nastja {

struct CellInfoLine {
  cellid_t cellID{};
  std::array<int, 3> center{-1, -1, -1};
  real_t volume{};
  real_t surface{};
  int type{};
  std::array<real_t, numberOfSignals> signal{};
  long age{};
  math::Vector3<real_t> polarity{};
  math::Vector3<real_t> motilityDirection{};
  std::string neighbors{};
  real_t inhibitionStrength{-1};
};

void CellInfo::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  WriterReader::init(blockRegister, fieldpool);
  auto& config = getSimData().getConfig();
  auto jptr    = config::getJsonPointer("Writers." + getName());

  /// @key{Writers.\template{writer=CellInfo}.steps, int, 1}
  /// Number of each nth steps when the output is written.
  eachNthStep = config.getValue<int>(jptr / "steps", 1);

  /// @key{Writers.\template{writer=CellInfo}.printcontactinhibition, int, false}
  /// Flag if contact inhibition should be printed.
  printContactInhibition = config.getValue<bool>(jptr / "printcontactinhibition", false);

  /// @key{Writers.\template{writer=CellInfo}.outputfieldname, string, ""}
  /// The output data type.
  outputFieldName_ = config.getValue<std::string>(jptr / "outputfieldname", getFieldname());

  /// @key{Writers.\template{writer=CellInfo}.groupsize, int, 0}
  /// Group size for collecting the output from n workers. 0 for master output.
  groupSize_ = config.getValue<int>(jptr / "groupsize", 0);
  if (groupSize_ <= 0) {
    groupSize_    = getSimData().mpiData.getSize();
    masterOutput_ = true;
  }

  /// @key{Writers.\template{writer=CellInfo}.neighborradius, uint, 0}
  /// If radius is given a list of all cells whithin this radius is added to the output.
  neighborSqrRadius_ = config.getValue<unsigned int>(jptr / "neighborradius", 0);
  neighborSqrRadius_ *= neighborSqrRadius_;

  if (neighborSqrRadius_ == 0) return;

  for (const auto& block : blockRegister) {
    auto& container = block.second->getDataContainer<CellBlockContainer>();
    container.activateNearestNeighborSearch();
  }
}

std::ofstream CellInfo::openfile(const std::string& filename) const {
  auto& data = getSimData().getData<DataCells>();

  std::ofstream fp{filename.c_str()};

  fmt::print(fp, "#CellID CenterX CenterY CenterZ Volume Surface Type ");
  for (unsigned int s = 0; s < numberOfSignals; s++) {
    fmt::print(fp, "Signal{} ", s);
  }
  fmt::print(fp, "Age PolarityX PolarityY PolarityZ MotilityDirX MotilityDirY MotilityDirZ");
  if (printContactInhibition){
    fmt::print(fp, " inhibitionStrength");
  }
  if (neighborSqrRadius_ > 0) {
    fmt::print(fp, " Neighbors\n");
  } else {
    fmt::print(fp, "\n");
  }

  for (unsigned int i = 0; i <= data.liquid; i++) {
    CellInfoLine line;
    line.cellID = i;
    line.type   = i;

    fmt::print(fp, "{}", line);
    if (neighborSqrRadius_ > 0) {
      fmt::print(fp, " -");
    }
    fmt::print(fp, "\n");
  }

  return fp;
}

void CellInfo::sendToMaster() {
  int rank = getSimData().mpiData.getRank();
  rank -= (rank % groupSize_);

  MPI_Wait(&sendRequest_, MPI_STATUS_IGNORE);

  MPI_Isend(ar_.data(), ar_.size(), MPI_CHAR, rank, MPICSVExchange, MPI_COMM_WORLD, &sendRequest_);
}

void CellInfo::collect() {
  const auto mpiRank   = getSimData().mpiData.getRank();
  const auto mpiSize   = getSimData().mpiData.getSize();
  std::string filename = masterOutput_ ? makeFilenameFrame(outputFieldName_, "csv", frame - 1) : makeFilename(outputFieldName_, "csv", frame - 1, mpiRank);

  auto fp = openfile(filename);

  int numOfWorkersInGroup = groupSize_;
  if (mpiSize < (mpiRank + groupSize_)) {
    numOfWorkersInGroup = mpiSize % groupSize_;
  }

  Archive recvAr;

  while (numOfWorkersInGroup > 0) {
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPICSVExchange, MPI_COMM_WORLD, &status);

    int size{};
    MPI_Get_count(&status, MPI_CHAR, &size);
    recvAr.reset();
    recvAr.resize(size);
    void* recvPtr = recvAr.data();

    MPI_Recv(recvPtr, size, MPI_CHAR, status.MPI_SOURCE, MPICSVExchange, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    while (recvAr.eof() == 0u) {
      CellInfoLine line{};

      recvAr.unpack(line.cellID);
      recvAr.unpack(line.center);
      recvAr.unpack(line.volume);
      recvAr.unpack(line.surface);
      recvAr.unpack(line.type);
      recvAr.unpack(line.signal);
      recvAr.unpack(line.age);
      recvAr.unpack(line.polarity);
      recvAr.unpack(line.motilityDirection);
      if (printContactInhibition){
        recvAr.unpack(line.inhibitionStrength);
      }
      fmt::print(fp, "{}", line);

      if (neighborSqrRadius_ > 0) {
        recvAr.unpack(line.neighbors);
        fmt::print(fp, " {}", line.neighbors);
      }

      fmt::print(fp, "\n");
    }

    numOfWorkersInGroup--;
  }
}

void CellInfo::executeBlock(block::Block* block) {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();

  if (container.cells.empty()) return;

  for (const auto& i : container.cells) {
    cellid_t cellID = i.first;

    const auto& cell   = i.second;
    const auto& center = cell.center;
    const long age     = getSimData().timestep - cell.birth;

    if (cellID <= data.liquid || !block->isGlobalCoordinateInBlock(center[0], center[1], center[2])) continue;

    ar_.pack(cellID);
    ar_.pack(center);
    ar_.pack(cell.volume);
    ar_.pack(cell.surface);
    ar_.pack(cell.type);
    ar_.pack(cell.signal);
    ar_.pack(age);
    ar_.pack(cell.polarity);
    ar_.pack(cell.motilityDirection);
    if(printContactInhibition){
      ar_.pack(cell.inhibitionStrength);
    }

    if (neighborSqrRadius_ > 0) {
      std::string neighbors{};

      if (container.periodicRadiusSearch(cell.center, neighborSqrRadius_, neighbors_) > 0) {
        std::sort(neighbors_.begin(), neighbors_.end());
        std::ostringstream sstream;
        for (const auto& n : neighbors_) {
          sstream << fmt::format("{},", n.first);
        }
        neighbors = sstream.str();
        neighbors.pop_back();
      }

      ar_.pack(neighbors);
    }
  }
}

void CellInfo::execute(const block::BlockRegister& blockRegister) {
  ar_.clear();

  WriterReader::execute(blockRegister);

  sendToMaster();

  const auto mpiRank = getSimData().mpiData.getRank();
  if (mpiRank % groupSize_ == 0) {
    collect();
  }
}

}  // namespace nastja

namespace fmt {
template <>
struct formatter<nastja::CellInfoLine> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const nastja::CellInfoLine& line, FormatContext& ctx) {
    // ctx.out() is an output iterator to write to.
    if (line.inhibitionStrength != -1){
      return format_to(
          ctx.out(),
          "{} {} {:f} {:f} {} {:.4e} {} {:f} {:f} {:f}",
          line.cellID, fmt::join(line.center, " "), line.volume, line.surface, line.type, fmt::join(line.signal, " "), line.age, fmt::join(line.polarity, " "), fmt::join(line.motilityDirection, " "), line.inhibitionStrength);
    }
    else{
      return format_to(
          ctx.out(),
          "{} {} {:f} {:f} {} {:.4e} {} {:f} {:f}",
          line.cellID, fmt::join(line.center, " "), line.volume, line.surface, line.type, fmt::join(line.signal, " "), line.age, fmt::join(line.polarity, " "), fmt::join(line.motilityDirection, " "));
    }
  }
};
}  // namespace fmt
