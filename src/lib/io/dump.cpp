/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "dump.h"
#include "lib/timing.h"
#include "lib/block/blockmanager.h"
#include "lib/field/field.h"
#include <cstdio>

namespace nastja {

void Dump::init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) {
  auto& config = getSimData().getConfig();
  auto jptr    = config::getJsonPointer("Writers." + getName());

  /// @key{Writers.\template{writer=Dump}.steps, int, 1}
  /// Number of each nth steps when the output is written.
  eachNthStep = config.getValue<int>(jptr / "steps", 1);

  /// @key{Writers.\template{writer=Dump}.preserve, int, 2}
  /// Preserve this number of dumped time-steps.
  preserve_ = config.getValue<int>(jptr / "preserve", 2);
}

void Dump::execute(const block::BlockRegister& blockRegister) {
  if (getSimData().timestep == 0) {
    logger::get().onelog("Write {:<20} Frame: {:4} (initial)", getName(), frame % preserve_);
  } else {
    logger::get().onelog("Write {:<20} Frame: {:4} ({:7})", getName(), frame % preserve_, getSimData().timestep - offset);
  }

  std::string filename = makeFilename(getFieldname(), "dump", frame % preserve_, getSimData().mpiData.getRank());

  FILE* fp = fopen(filename.c_str(), "w");

  Archive ar;

  // dump simdata
  getSimData().dump(ar);
  long length = ar.size();
  fwrite(&length, sizeof(long), 1, fp);
  fwrite(ar.data(), ar.size(), 1, fp);

  for (const auto& block : blockRegister) {
    for (const auto& field : block.second->fields.poolSet) {
      auto* ptr = field.second->getCharBasePtr();
      auto size = field.second->getDataSize();

      // dump field
      fwrite(ptr, size, 1, fp);

      // dump field container
      ar.clear();
      field.second->getDataContainerPool().dump(ar);
      length = ar.size();
      fwrite(&length, sizeof(long), 1, fp);
      if (length > 0) {
        fwrite(ar.data(), ar.size(), 1, fp);
      }
    }

    // dump block containers
    ar.clear();
    block.second->getDataContainerPool().dump(ar);
    length = ar.size();
    fwrite(&length, sizeof(long), 1, fp);
    if (length > 0) {
      fwrite(ar.data(), ar.size(), 1, fp);
    }
  }
  fclose(fp);

  frame++;
}

}  // namespace nastja
