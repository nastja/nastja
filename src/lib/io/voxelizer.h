/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/io/writerreader.h"
#include "lib/math/boundingbox.h"
#include "lib/math/triangle.h"
#include "lib/math/vector.h"

namespace nastja {

/**
 * Class for read stl data and voxelize it.
 *
 * @ingroup io io-in
 */
class Voxelizer : public WriterReader {
public:
  explicit Voxelizer(const std::string& fieldname)
      : WriterReader{fieldname} {}
  ~Voxelizer() override           = default;
  Voxelizer(const Voxelizer&)     = delete;
  Voxelizer(Voxelizer&&) noexcept = default;
  Voxelizer& operator=(const Voxelizer&) = delete;
  Voxelizer& operator=(Voxelizer&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override {
    WriterReader::init(blockRegister, fieldpool);

    getSimData().mpiData.getSize();
    getSimData().mpiData.getRank();

    /// @key{Writers.\template{writer=Voxelizer}.FileName, string}
    /// Path of the input stl file.
    stlFileName_ = getSimData().getConfig().getValue<std::string>(config::getJsonPointer(getName() + ".filename"));
  }

  void execute(const block::BlockRegister& blockRegister) override;
  void executeBlock(block::Block* block) override;
  bool skip() const override { return isSkipped(); }

private:
  static void readBinaryStl(const std::string& filename, std::vector<math::Triangle>& triangles);
  static math::BoundingBox<long> createBoundingBox(math::Triangle singleTriangle);
  void voxelizeSurface(field::Field<real_t, 1>& field);
  static void voxelizeStructure(field::Field<real_t, 1>& field, std::vector<int>& incomingPlane);
  static bool sameSide(int x, int y, int z, math::Vector3<real_t> a, math::Vector3<real_t> b, math::Vector3<real_t> c);
  bool pointInTriangle(int x, int y, int z, math::Vector3<real_t> a, math::Vector3<real_t> b, math::Vector3<real_t> c);

  void doRight(std::vector<int>& rightBuffer, int blockID, const block::BlockRegister& blockRegister, std::vector<MPI_Request>& requestWaitingRight);
  void doLeft(int blockID, const block::BlockRegister& blockRegister);
  static void processCount(const std::vector<int>& incomingPlane, std::vector<int>& outgoingPlane, field::Field<real_t, 1>& field);

  std::string stlFileName_;                    ///< The file name of the stl file.
  std::vector<std::vector<int>> rightBuffer_;  ///< The buffer for the right side.
  std::vector<std::vector<int>> leftBuffer_;   ///< The buffer for the left side.
};

}  // namespace nastja
