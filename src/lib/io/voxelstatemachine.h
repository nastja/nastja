/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/communication/statemachine.h"
#include "lib/field/field.h"

namespace nastja {

void initial_func(StateMachine* SM);
void pen_up(StateMachine* SM);
void pen_down(StateMachine* SM);
void count_increment(StateMachine* SM);
void inter_mediate(StateMachine* SM);
void inter_mediate_draw(StateMachine* SM);

/*
+----+
|INIT|-------------------------------------------+
+-+--+                                           |cnt > 0
  |                                              |
  | cnt <= 0                                     |
  |                                cnt > 0       v
  v            +-----------------+-------> +---------+
+------+ cnt=0 |DRAW_INTERMEDIATE|  fall   |PEN_DOWN |
|PEN_UP| <-----+-----------------+ <-------+---------+
+------+                                    |  ^  ^ |
 |  | |         +---------------+ <---------+  |  | |
 |  | +-------> |COUNT_INCREMENT|     raise    |  | |
 |  |   raise   +---------------+--------------+  | |
 |  |                                 direct      | |
 |  |           +---------------+                 | |
 |  +---------> | INTERMEDIATE  +-----------------+ |
 |      fall    +---------------+     direct        |
 |                                                  |
 |                  +-----+                         |
 +----------------> | END | <-----------------------+
                    +-----+
*/

struct StructState {
  enum VoxelState {
    initial,
    penUP,
    penDOWN,
    intermediateDRAW,
    intermediate,
    cntINC,
    end
  };
};
using VoxelState = StructState::VoxelState;

struct EventStruct {
  enum VoxelEvent {
    fall,
    raise,
    direct,
    cnt_greater_zero,
    cnt_less_zero,
    cnt_equal_zero,
    endx
  };
};
using VoxelEvent = EventStruct::VoxelEvent;

// clang-format off
static constexpr TransitionEntry connection_table[] = {
  // Start                       Event                         Next
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::initial,          VoxelEvent::cnt_less_zero,    VoxelState::penUP,            &initial_func      },
  {VoxelState::initial,          VoxelEvent::cnt_greater_zero, VoxelState::penDOWN,          &initial_func      },
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::penUP,            VoxelEvent::raise,            VoxelState::cntINC,           &pen_up            },
  {VoxelState::penUP,            VoxelEvent::fall,             VoxelState::intermediate,     &pen_up            },
  {VoxelState::penUP,            VoxelEvent::endx,             VoxelState::end,              &pen_up            },
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::cntINC,           VoxelEvent::direct,           VoxelState::penDOWN,          &count_increment   },
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::intermediate,     VoxelEvent::direct,           VoxelState::penDOWN,          &inter_mediate     },
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::penDOWN,          VoxelEvent::fall,             VoxelState::intermediateDRAW, &pen_down          },
  {VoxelState::penDOWN,          VoxelEvent::raise,            VoxelState::cntINC,           &pen_down          },
  {VoxelState::penDOWN,          VoxelEvent::endx,             VoxelState::end,              &pen_down          },
  //---------------------------+-----------------------------+-----------------------------+--------------------+
  {VoxelState::intermediateDRAW, VoxelEvent::cnt_greater_zero, VoxelState::penDOWN,          &inter_mediate_draw},
  {VoxelState::intermediateDRAW, VoxelEvent::cnt_equal_zero,   VoxelState::penUP,            &inter_mediate_draw}
};
// clang-format on

static const std::size_t TABLE_SIZE = std::extent<decltype(connection_table)>::value;

class VoxelStateMachine : public StateMachine {
private:
  int start_;
  int count_;
  int x_;
  int y_{};
  int z_{};
  field::Field<real_t, 1>* field_{};

public:
  explicit VoxelStateMachine() : StateMachine{connection_table, TABLE_SIZE} {
    start_ = 0;
    count_ = 0;
    x_     = 0;
  }

  void setUp(const int yval, const int zval, field::Field<real_t, 1>& field_arg, int count_val) {
    y_     = yval;
    z_     = zval;
    count_ = count_val;
    setState(VoxelState::initial);
    field_ = &field_arg;
  }

  int getStart() const { return start_; }
  int getCount() const { return count_; }
  int getx() const { return x_; }
  int gety() const { return y_; }
  int getz() const { return z_; }
  field::Field<real_t, 1>* getField() const { return field_; }

  void setStart(int s) { start_ = s; }
  void setCount(int cnt) { count_ = cnt; }
  void setx(int xval) { x_ = xval; }
};

inline void initial_func(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  newSM->setx(0);
  newSM->setStart(0);
  if (newSM->getCount() <= 0) {
    newSM->process_event(VoxelEvent::cnt_less_zero);
  } else {
    newSM->process_event(VoxelEvent::cnt_greater_zero);
  }
}

inline void pen_up(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  unsigned int x = newSM->getx();
  unsigned int y = newSM->gety();
  unsigned int z = newSM->getz();
  x++;
  newSM->setx(x);
  if (x == (newSM->getField()->getSize(0) - newSM->getField()->getBoundarySize(0))) {
    newSM->process_event(VoxelEvent::endx);
  }
  if (newSM->getField()->getCell(x, y, z) == 0.25) {
    newSM->process_event(VoxelEvent::raise);
  } else if (newSM->getField()->getCell(x, y, z) == 0.75) {
    newSM->process_event(VoxelEvent::fall);
  }
}

inline void pen_down(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  unsigned int x = newSM->getx();
  unsigned int y = newSM->gety();
  unsigned int z = newSM->getz();

  newSM->getField()->getCell(x, y, z) = 1;
  x++;
  newSM->setx(x);
  if (x == (newSM->getField()->getSize(0) - newSM->getField()->getBoundarySize(0))) {
    newSM->process_event(VoxelEvent::endx);
  }
  if (newSM->getField()->getCell(x, y, z) == 0.75) {
    newSM->process_event(VoxelEvent::fall);
  } else if (newSM->getField()->getCell(x, y, z) == 0.25) {
    newSM->process_event(VoxelEvent::raise);
  }
}

inline void count_increment(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  int count = newSM->getCount();
  count++;
  newSM->setCount(count);
  newSM->process_event(VoxelEvent::direct);
}

inline void inter_mediate(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  int tmp;
  tmp = newSM->getStart();
  newSM->setStart(newSM->getx());
  newSM->setx(tmp);
  newSM->process_event(VoxelEvent::direct);
}

inline void inter_mediate_draw(StateMachine* SM) {
  auto* newSM = static_cast<VoxelStateMachine*>(SM);

  int x = newSM->getx();
  int y = newSM->gety();
  int z = newSM->getz();

  newSM->getField()->getCell(x, y, z) = 1;

  int count = newSM->getCount();

  if (count == 0) {
    newSM->setStart(newSM->getx());
  }

  count--;
  newSM->setCount(count);

  if (count < 0) {
    newSM->setCount(0);
    count = 0;
  }

  if (count == 0) {
    newSM->process_event(VoxelEvent::cnt_equal_zero);
  } else {
    newSM->process_event(VoxelEvent::cnt_greater_zero);
  }
}

}  // namespace nastja
