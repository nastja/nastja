/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/field/field.h"
#include "lib/math/random.h"
#include <functional>

namespace nastja {

using namespace config::literals;

/**
 * @defgroup visitors Visitors
 * Provide visitors with different order to visit the voxels.
 *
 * @ingroup cells
 */

template <unsigned int>
class SweepCells;

template <unsigned int DIM = 3>
class IVisitor {
public:
  explicit IVisitor(SimData& simdata) : simdata{simdata} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  virtual ~IVisitor()           = default;
  IVisitor(const IVisitor&)     = delete;
  IVisitor(IVisitor&&) noexcept = default;
  IVisitor& operator=(const IVisitor&) = delete;
  IVisitor& operator=(IVisitor&&) = delete;

  virtual void visit(field::Field<cellid_t, 1>& field, CellBlockContainer& container, SweepCells<DIM>& sweep) = 0;

  virtual void dump(Archive& /*ar*/) const {}
  virtual void undump(Archive& /*ar*/) {}

protected:
  /**
   * Calculates the first step by run pre steps.
   *
   * The probability to touch a voxel is not uniform distributed for the first voxels. This smooth out after 3 to 3.5
   * times the maximum step width.
   *
   * @param uniform  The uniform.
   *
   * @return The first step.
   */
  static int preSteps(math::UniformInt<int>& uniform, std::mt19937_64& generator) {
    int presteps = 3 * uniform.max();
    int precount = 0;
    while (precount < presteps) {
      precount += uniform(generator);
    }
    return precount - presteps;
  }

  SimData& simdata;  ///< The reference to the SimData.
};

}  // namespace nastja
