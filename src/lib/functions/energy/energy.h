/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/utility.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D2C4.h"
#include "lib/stencil/D2C5.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/D3C7.h"
#include "lib/stencil/D2C8.h"
#include "lib/stencil/D2C9.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/direction.h"
#include "lib/storage/reducedarray.h"
#include <array>
#include <type_traits>
#include <utility>

namespace nastja {

/**
 * @defgroup energyfunctions Energy function
 *
 * Provides energy functions for the cellular Potts model.
 *
 * @ingroup cells
 */

enum class EnergyCategory {
  adhesion,
  surface,
  volume,
  potential,
  motility,
  polarity
};

/**
 * Helper struct, payload for the energy.calc functions.
 */
struct EnergyFunctionPack {
  explicit EnergyFunctionPack(cellid_t centerCellID, cellid_t neighborCellID, Cell& centerCell, Cell& neighborCell, CellBlockContainer& container, DataCells& data)
      : centerCellID{centerCellID},
        neighborCellID{neighborCellID},
        centerCell{centerCell},
        neighborCell{neighborCell},
        container{container},
        data{data} {}

  const cellid_t centerCellID;
  const cellid_t neighborCellID;
  const Cell& centerCell;
  const Cell& neighborCell;
  const CellBlockContainer& container;
  const DataCells& data;

  real_t changeCenterSurface{0.0};
  real_t changeNeighborSurface{0.0};
  real_t changeCenterVolume{0.0};
  real_t changeNeighborVolume{0.0};
};

template <unsigned int DIM>
using DxC7 = typename std::conditional<DIM == 3, stencil::D3C7, stencil::D2C5>::type;

template <unsigned int DIM>
using DxC6 = typename std::conditional<DIM == 3, stencil::D3C6, stencil::D2C4>::type;

template <unsigned int DIM>
using DxC27 = typename std::conditional<DIM == 3, stencil::D3C27, stencil::D2C9>::type;

template <unsigned int DIM>
using DxC26 = typename std::conditional<DIM == 3, stencil::D3C26, stencil::D2C8>::type;


template <unsigned int DIM = 3>
class IEnergy {
public:
  explicit IEnergy() = default;

  virtual ~IEnergy()          = default;
  IEnergy(const IEnergy&)     = delete;
  IEnergy(IEnergy&&) noexcept = default;
  IEnergy& operator=(const IEnergy&) = delete;
  IEnergy& operator=(IEnergy&&) = delete;

  virtual void dump(Archive& /*ar*/) const {}
  virtual void undump(Archive& /*ar*/) {}

  virtual void init(DataCells& data, config::Config& config)                                          = 0;
  virtual unsigned int getCategory() const                                                            = 0;
  virtual void setup(const block::Block* block, CellBlockContainer& container, const DataCells& data) = 0;

  virtual real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p)         = 0;
  virtual real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) = 0;
};

void readDefaultAdhesionConfig(DataCells& data, config::Config& config);
void readDefaultPotentialConfig(DataCells& data, config::Config& config);
void readDefaultSurfaceConfig(DataCells& data, config::Config& config);
void readDefaultVolumeConfig(DataCells& data, config::Config& config);
void readDefaultPolarityConfig(DataCells& data, config::Config& config);

}  // namespace nastja
