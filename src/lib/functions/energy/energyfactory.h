/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/factory.h"
#include "lib/functions/energy/energy.h"
#include "lib/functions/energy/energyadhesion00.h"
#include "lib/functions/energy/energyadhesion01.h"
#include "lib/functions/energy/energyadhesion02.h"
#include "lib/functions/energy/energymotility00.h"
#include "lib/functions/energy/energypotential00.h"
#include "lib/functions/energy/energypotential01.h"
#include "lib/functions/energy/energypotential02.h"
#include "lib/functions/energy/energysurface00.h"
#include "lib/functions/energy/energysurface01.h"
#include "lib/functions/energy/energyvolume00.h"
#include "lib/functions/energy/energyvolume01.h"
#include "lib/functions/energy/energypolarity00.h"

namespace nastja {

template <unsigned int DIM = 3>
class FactoryEnergy : public Factory<IEnergy<DIM>> {
public:
  /// @factory{FactoryEnergy}
  /// "Volume00", "Volume01", "Volume02", "Surface00", "Surface01", "Adhesion00", "Adhesion01", "Potential00", "Potential01", "Potential02", "Motility00"
  explicit FactoryEnergy() {
    /*
     * Energy terms for Monte Carlo energy calculations in the Cellular Potts Model
     */

    /*
     * Volume energies calculate the energy change of the change of one voxel from the surrounding stencil.
     * E = lambda * (V - V0)^2    --> Delta E = lambda * ( (Va - V0)^2 - (Vb - V0)^2 )
     *
     * Volume00:
     * Calculation via voxel counting.
     *
     * Volume01:
     * Calculation via Marching cubes
     *
     */
    Factory<IEnergy<DIM>>::template registerType<EnergyVolume00<DIM>>("Volume00");
    Factory<IEnergy<DIM>>::template registerType<EnergyVolume01<DIM>>("Volume01");

    /*
     * Surface Energies
     *
     * Surface00:
     * Surface calculation by side counting (Manhattan Metric)
     *
     * Surface01:
     * Calculation using the marching cube algorithm
     */
    Factory<IEnergy<DIM>>::template registerType<EnergySurface00<DIM>>("Surface00");
    Factory<IEnergy<DIM>>::template registerType<EnergySurface01<DIM>>("Surface01");

    /*
     * Adhesion Energies:
     *
     * Adhesion00:
     * Adhesion area by side counting
     *
     * Adhesion01:
     * Adhesion Area by Marching cubes and averaging of the adhesion constants for all other cells in the cube
     *
     */
    Factory<IEnergy<DIM>>::template registerType<EnergyAdhesion00<DIM>>("Adhesion00");
    Factory<IEnergy<DIM>>::template registerType<EnergyAdhesion01<DIM>>("Adhesion01");
    Factory<IEnergy<DIM>>::template registerType<EnergyAdhesion02<DIM>>("Adhesion02");

    /*
     * Potential Energies:
     *
     * Potential00:
     * Potential only pulling force.
     * Homogeneous direction
     *
     * Potential01:
     * Potential pulls and pushes
     * Homogeneous direction
     *
     * Potential02:
     * Potential towards one point.
     *
     */
    Factory<IEnergy<DIM>>::template registerType<EnergyPotential00<DIM>>("Potential00");
    Factory<IEnergy<DIM>>::template registerType<EnergyPotential01<DIM>>("Potential01");
    Factory<IEnergy<DIM>>::template registerType<EnergyPotential02<DIM>>("Potential02");

    /*
     * Motility Energies:
     *
     * Motility00:
     * Motility force directly proportional to individual cells motility vector
     *
     */
    Factory<IEnergy<DIM>>::template registerType<EnergyMotility00<DIM>>("Motility00");


    /*
     * Polarity Energies:
     *
     * Polarity00:
     * energy depends on scalar product of the cell polarity vector and the vector between the cell center and the cell voxel position
     *
     */
    Factory<IEnergy<DIM>>::template registerType<EnergyPolarity00<DIM>>("Polarity00");

  }

};

}  // namespace nastja
