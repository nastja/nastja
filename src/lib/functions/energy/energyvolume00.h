/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * @defgroup energyvolumes Volume energy
 *
 * @ingroup energyfunctions
 */

/**
 * Class for energy volume 00 - counting.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energyvolumes
 */
template <unsigned int DIM = 3>
class EnergyVolume00 : public IEnergy<DIM> {
public:
  explicit EnergyVolume00()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyVolume00() override                = default;
  EnergyVolume00(const EnergyVolume00&)     = delete;
  EnergyVolume00(EnergyVolume00&&) noexcept = default;
  EnergyVolume00& operator=(const EnergyVolume00&) = delete;
  EnergyVolume00& operator=(EnergyVolume00&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultVolumeConfig(data, config); }

  void setup(const block::Block* block, CellBlockContainer& container, const DataCells& /*data*/) override {
    container.volume = std::make_unique<math::VolumeCount<>>();

    auto& field = block->getFieldByName("cells")->get<cellid_t>();

    auto view = field.createView();
    for (const auto& it : view) {
      container.cells(view.get(it)).deltaVolume += 1.0;
    }
  }

  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in volume energy via counting voxels. 1 voxel = 1 µm^3
   *
   * @param p  The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& /*view*/, FieldIterator& /*it*/, stencil::Direction /*dir*/, EnergyFunctionPack& p) {
    auto& vol0               = p.data.defaultVolume->getStorage();
    auto& lamVol             = p.data.lambdaVolume->getStorage();
    auto& sizeChangeConstant = p.data.changeVolume->getStorage();

    real_t energy    = 0;
    int typeCurrent  = p.centerCell.type;
    int typeNeighbor = p.neighborCell.type;
    //Volume Energy calculation
    if (p.centerCellID > p.data.liquid) {
      real_t goalVolume     = std::max(vol0.getValue(typeCurrent) + sizeChangeConstant.getValue(typeCurrent) * (p.container.getSimData().timestep - p.centerCell.birth), real_t(0));
      real_t currentVolDiff = goalVolume - p.centerCell.volume;
      real_t futureVolDiff  = currentVolDiff + 1.0;

      p.changeCenterVolume += -1.0;

      energy -= lamVol.getValue(typeCurrent) * (currentVolDiff * currentVolDiff - futureVolDiff * futureVolDiff);
    }

    if (p.neighborCellID > p.data.liquid) {
      real_t goalVolume     = std::max(vol0.getValue(typeNeighbor) + sizeChangeConstant.getValue(typeNeighbor) * (p.container.getSimData().timestep - p.neighborCell.birth), real_t(0));
      real_t currentVolDiff = goalVolume - p.neighborCell.volume;
      real_t futureVolDiff  = currentVolDiff - 1.0;

      p.changeNeighborVolume += 1.0;

      energy -= lamVol.getValue(typeNeighbor) * (currentVolDiff * currentVolDiff - futureVolDiff * futureVolDiff);
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::volume};  ///< The category of the energy function.
};

}  // namespace nastja
