/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"
#include "lib/math/calculatesurface.h"
#include "lib/stencil/D3C7.h"

namespace nastja {

/**
 * Class for energy adhesion 01 - marching cube.
 * Adhesion energy between different voxels is calculated based on the marching cubes surfaces between them.
 *
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energyadhesions
 */
template <unsigned int DIM = 3>
class EnergyAdhesion01 : public IEnergy<DIM> {
public:
  explicit EnergyAdhesion01()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyAdhesion01() override                  = default;
  EnergyAdhesion01(const EnergyAdhesion01&)     = delete;
  EnergyAdhesion01(EnergyAdhesion01&&) noexcept = default;
  EnergyAdhesion01& operator=(const EnergyAdhesion01&) = delete;
  EnergyAdhesion01& operator=(EnergyAdhesion01&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultAdhesionConfig(data, config); }
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in adhesion energy via marching cubes algorithm.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction /*dir*/, EnergyFunctionPack& p) {
    real_t energy = 0.0;

    auto& adhesion = p.data.storageAdhesion->getStorage();

    std::map<cellid_t, bool> written;
    for (auto d : DxC7<DIM>()) {
      cellid_t cellID = view.get(*it, stencil::D3C7::dir[d]);
      if (written[cellID]) continue;
      written[cellID] = true;
      int typeD       = p.container.cells(cellID).type;

      real_t actualAdhesion = 0.0;
      int i                 = 0;
      for (auto e : DxC7<DIM>()) {
        if (e == d || view.get(*it, stencil::D3C7::dir[e]) == cellID) continue;
        int typeE = p.container.cells(view.get(*it, stencil::D3C7::dir[e])).type;
        actualAdhesion += adhesion.getValue(typeD, typeE);
        i++;
      }
      actualAdhesion /= i;

      real_t deltaS = math::calcSurfaceMCDiff(view, *it, cellID, p.neighborCellID);
      energy -= actualAdhesion * deltaS;
    }
    return energy;
  }

  EnergyCategory category_{EnergyCategory::adhesion};  ///< The category of the energy function.
};

}  // namespace nastja
