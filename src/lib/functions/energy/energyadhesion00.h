/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * @defgroup energyadhesions Adhesion energy
 *
 * @ingroup energyfunctions
 */

/**
 * Class for energy adhesion 00 - counting.
 * Adhesion energy between different voxels is calculated based on counting the sides of voxels to find the surfaces between them.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energyadhesions
 */
template <unsigned int DIM = 3>
class EnergyAdhesion00 : public IEnergy<DIM> {
public:
  explicit EnergyAdhesion00()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyAdhesion00() override                  = default;
  EnergyAdhesion00(const EnergyAdhesion00&)     = delete;
  EnergyAdhesion00(EnergyAdhesion00&&) noexcept = default;
  EnergyAdhesion00& operator=(const EnergyAdhesion00&) = delete;
  EnergyAdhesion00& operator=(EnergyAdhesion00&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultAdhesionConfig(data, config); }
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in adhesion energy though side counting.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param dir   The direction of the neighbor.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction dir, EnergyFunctionPack& p) {
    real_t energy = 0.0;

    auto& adhesion = p.data.storageAdhesion->getStorage();

    for (auto d : DxC6<DIM>()) {
      cellid_t cellID = view.get(*it, stencil::D3C6::dir[d]);
      if (p.centerCellID != cellID) {
        energy += adhesion.getValue(p.centerCell.type, p.container.cells(cellID).type);
      }
      if (cellID != p.neighborCellID && stencil::D3C6::dir[d] != dir) {
        energy -= adhesion.getValue(p.neighborCell.type, p.container.cells(cellID).type);
      }
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::adhesion};  ///< The category of the energy function.
};

}  // namespace nastja
