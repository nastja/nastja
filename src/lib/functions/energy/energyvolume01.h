/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"
#include "lib/math/calculatecubes.h"
#include "lib/math/calculatevolume.h"

namespace nastja {

/**
 * Class for energy volume 02 - marching cubes.
 * UNTESTED!
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energyvolumes
 */
template <unsigned int DIM = 3>
class EnergyVolume01 : public IEnergy<DIM> {
public:
  explicit EnergyVolume01()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyVolume01() override                = default;
  EnergyVolume01(const EnergyVolume01&)     = delete;
  EnergyVolume01(EnergyVolume01&&) noexcept = default;
  EnergyVolume01& operator=(const EnergyVolume01&) = delete;
  EnergyVolume01& operator=(EnergyVolume01&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultVolumeConfig(data, config); }

  void setup(const block::Block* block, CellBlockContainer& container, const DataCells& data) override {
    container.volume = std::make_unique<math::VolumeMC<>>();

    auto& field = block->getFieldByName("cells")->get<cellid_t>();

    auto view = field.createView();
    for (auto* voxel : view) {
      std::array<cellid_t, 8> cube = field.getCube(view.getIndex(voxel));  // TODO: getCube from view

      for (int b = 0; b < 8; b++) {
        if (cube[b] <= data.liquid || !math::isFirstValueInCube(cube, b)) continue;

        container.cells(cube[b]).deltaVolume += math::calcVolumeMC(cube, cube[b]);
      }
    }
  }

  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in volume energy via marching cube algorithm.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction /*dir*/, EnergyFunctionPack& p) {
    auto& vol0   = p.data.defaultVolume->getStorage();
    auto& lamVol = p.data.lambdaVolume->getStorage();

    real_t energy = 0.0;

    if (p.centerCellID > p.data.liquid) {
      real_t deltaV = math::calcVolumeDiff(view, *it, p.centerCellID, p.neighborCellID);

      p.changeCenterVolume += deltaV;

      //Now, calculate the Energy
      real_t volDiffPre  = vol0.getValue(p.centerCell.type) - p.centerCell.volume;  // S_0 - S_old before the change
      real_t volDiffPost = volDiffPre - deltaV;                                     // S_0 - S_new after the change

      energy -= lamVol.getValue(p.centerCell.type) * (volDiffPre * volDiffPre - volDiffPost * volDiffPost);
    }

    if (p.neighborCellID > p.data.liquid) {
      real_t deltaV = math::calcVolumeDiff(view, *it, p.neighborCellID, p.neighborCellID);

      p.changeNeighborVolume += deltaV;

      //Now, calculate the Energy
      real_t volDiffPre  = vol0.getValue(p.neighborCell.type) - p.neighborCell.volume;  // S_0 - S_old before the change
      real_t volDiffPost = volDiffPre - deltaV;                                         // S_0 - S_new after the change

      energy -= lamVol.getValue(p.neighborCell.type) * (volDiffPre * volDiffPre - volDiffPost * volDiffPost);
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::volume};  ///< The category of the energy function.
};

}  // namespace nastja
