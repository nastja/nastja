/*
 * This file is part of NAStJA.
 *
 * Copyright 2026 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"
#include "lib/math/calculatesurface.h"


namespace nastja {




/**
 * Class for energy adhesion 02 - polarity adhesion.
 * Adhesion energy between different voxels is calculated based on the cell orientation.
 *
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energyadhesions
 */
template <unsigned int DIM = 3>
class EnergyAdhesion02 : public IEnergy<DIM> {
using adhesionStencil = DxC26<DIM>;
public:
  explicit EnergyAdhesion02()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyAdhesion02() override                  = default;
  EnergyAdhesion02(const EnergyAdhesion02&)     = delete;
  EnergyAdhesion02(EnergyAdhesion02&&) noexcept = default;
  EnergyAdhesion02& operator=(const EnergyAdhesion02&) = delete;
  EnergyAdhesion02& operator=(EnergyAdhesion02&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultAdhesionConfig(data, config); }
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Corrects for a possible periodic boundary jump
   *
   * @param block The block
   * @param position The vector to be checked
   */
  void checkForBoundaryJump(block::Block* block, math::Vector3<real_t>& position){
    auto systemSize = static_cast<math::Vector3<long int>>(block->size * block->blocks);
    for (size_t i=0; i< 3;++i){
      if (position[i] > systemSize[i]/2){//NOT block size, we need the overall system size
        position[i] -= systemSize[i];
      }
      else if (position[i] < (-1*systemSize[i]/2)){
        position[i] += systemSize[i];
      }
    }
  }
  /**
   * Calculates the change in adhesion energy via marching cubes algorithm.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction, EnergyFunctionPack& p) {
    if ( p.data.storagePolarityAdhesion == nullptr || p.data.storagePolarityAdhesionThreshold == nullptr ){
      return 0.;
    }
    real_t energy = 0.;
    auto const& adhesion = p.data.storagePolarityAdhesion->getStorage();
    auto const& threshold = p.data.storagePolarityAdhesionThreshold->getStorage();
    auto const& block {p.container.getBlock()};
    math::Vector3<real_t> centerCellPosition {
                                    static_cast<real_t>(block->offset[0] + it.coordinates().x()-1),
                                    static_cast<real_t>(block->offset[1] + it.coordinates().y()-1),
                                    static_cast<real_t>(block->offset[2] + it.coordinates().z()-1)};//{1.,0.,0.};//        

    math::Vector3<real_t> cellDistance{0.,0.,0.};//The vector pointing from the center of the cell with cellID = neighborVoxel to the center of the cell with cellID = centerVoxel
    real_t dotProdCenter{0.};//The scalar product between the polarity vector of the center cell and cellDistance 
    real_t dotProdNeighbor{0.};//The scalar product between the polarity vector of the neighbor cell and cellDistance 

    cellDistance = centerCellPosition - p.centerCell.center;
    //Check for periodic boundaries
    checkForBoundaryJump(block, cellDistance);
    dotProdCenter = abs(math::dotProduct(cellDistance,p.centerCell.polarity));
    real_t volatile absValCenter{sqrt(cellDistance[0]*cellDistance[0] + cellDistance[1]*cellDistance[1] + cellDistance[2]*cellDistance[2])};

    for (auto d : adhesionStencil()) {

      cellid_t cellID = view.get(*it, adhesionStencil::dir[d]);
      if (p.centerCellID != cellID && adhesion.getValue(p.centerCell.type, p.container.cells(cellID).type) > 0.) {

        cellDistance = centerCellPosition - p.container.cells(cellID).center;
        //Check for periodic boundaries
        checkForBoundaryJump(block, cellDistance);        


        dotProdNeighbor = abs(math::dotProduct(cellDistance,p.container.cells(cellID).polarity));
        real_t volatile absValNeighbor{sqrt(cellDistance[0]*cellDistance[0] + cellDistance[1]*cellDistance[1] + cellDistance[2]*cellDistance[2])};

        if (absValCenter > 0 && absValNeighbor > 0){
          if (dotProdCenter / absValCenter > threshold.getValue(p.centerCell.type) || dotProdNeighbor / absValNeighbor > threshold.getValue(p.container.cells(cellID).type)){
            energy +=  (dotProdCenter + dotProdNeighbor) * adhesion.getValue(p.centerCell.type, p.container.cells(cellID).type);
          }
        }

      }
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::adhesion};  ///< The category of the energy function.
};

}  // namespace nastja
