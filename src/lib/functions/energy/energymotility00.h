/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * @defgroup energymotility Motility energy
 *
 * @ingroup energyfunctions
 */

/**
 * Exerts a directional force on cells proportional to their individual motility vector.
 * By pushing and pulling.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energymotility
 */
template <unsigned int DIM = 3>
class EnergyMotility00 : public IEnergy<DIM> {
public:
  explicit EnergyMotility00()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyMotility00() override                  = default;
  EnergyMotility00(const EnergyMotility00&)     = delete;
  EnergyMotility00(EnergyMotility00&&) noexcept = default;
  EnergyMotility00& operator=(const EnergyMotility00&) = delete;
  EnergyMotility00& operator=(EnergyMotility00&&) = delete;

  void init(DataCells& /*data*/, config::Config& /*config*/) override {}
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& /*view*/, FieldIterator& /*it*/, stencil::Direction dir, EnergyFunctionPack& p) {
    const auto& changeSum = p.centerCell.motilityDirection + p.neighborCell.motilityDirection;

    return math::dotProduct(dir, changeSum);
  }

  EnergyCategory category_{EnergyCategory::motility};  ///< The category of the energy function.
};

}  // namespace nastja
