/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * @defgroup energysurfaces Surface energy
 *
 * @ingroup energyfunctions
 */

/**
 * Class for energy surface 00 - counting.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energysurfaces
 */
template <unsigned int DIM = 3>
class EnergySurface00 : public IEnergy<DIM> {
public:
  explicit EnergySurface00()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergySurface00() override                 = default;
  EnergySurface00(const EnergySurface00&)     = delete;
  EnergySurface00(EnergySurface00&&) noexcept = default;
  EnergySurface00& operator=(const EnergySurface00&) = delete;
  EnergySurface00& operator=(EnergySurface00&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultSurfaceConfig(data, config); }

  void setup(const block::Block* block, CellBlockContainer& container, const DataCells& data) override {
    container.surface = std::make_unique<math::SurfaceCount<>>();

    auto& field = block->getFieldByName("cells")->get<cellid_t>();

    auto view = field.createView();
    for (const auto& it : view) {
      cellid_t cell = view.get(it);

      if (cell <= data.liquid) continue;

      container.cells(cell).deltaSurface += (*container.surface)(view, it, cell);
    }
  }

  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in surface energy via side counting.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction /*dir*/, EnergyFunctionPack& p) {
    auto& surf0              = p.data.defaultSurface->getStorage();
    auto& lamSurf            = p.data.lambdaSurface->getStorage();
    auto& sizeChangeConstant = p.data.changeSurface->getStorage();

    real_t energy    = 0;
    int typeCurrent  = p.centerCell.type;
    int typeNeighbor = p.neighborCell.type;

    int cntCurrentSites  = 0;
    int cntNeighborSites = 0;

    //Surface Energy Calculation
    for (auto d : DxC6<DIM>()) {
      if (view.get(*it, stencil::D3C6::dir[d]) == p.neighborCellID) cntNeighborSites++;
      if (view.get(*it, stencil::D3C6::dir[d]) == p.centerCellID) cntCurrentSites++;
    }

    if (p.centerCellID > p.data.liquid) {
      int surfDiff = 2 * cntCurrentSites - 2 * DIM;  // minus: (facets - neighbors) plus: neighbors

      p.changeCenterSurface += surfDiff;

      real_t goalSurface     = std::max(surf0.getValue(p.centerCell.type) + sizeChangeConstant.getValue(p.centerCell.type) * (p.container.getSimData().timestep - p.centerCell.birth), real_t(0));
      real_t currentSurfDiff = goalSurface - p.centerCell.surface;
      real_t futureSurfDiff  = currentSurfDiff - surfDiff;

      energy -= lamSurf.getValue(typeCurrent) * (currentSurfDiff * currentSurfDiff - futureSurfDiff * futureSurfDiff);
    }

    if (p.neighborCellID > p.data.liquid) {
      int surfDiff = 2 * DIM - 2 * cntNeighborSites;  // plus: (facets - neighbors) minus: neighbors

      p.changeNeighborSurface += surfDiff;

      real_t goalSurface     = std::max(surf0.getValue(p.neighborCell.type) + sizeChangeConstant.getValue(p.neighborCell.type) * (p.container.getSimData().timestep - p.neighborCell.birth), real_t(0));
      real_t currentSurfDiff = goalSurface - p.neighborCell.surface;
      real_t futureSurfDiff  = currentSurfDiff - surfDiff;

      energy -= lamSurf.getValue(typeNeighbor) * (currentSurfDiff * currentSurfDiff - futureSurfDiff * futureSurfDiff);
    }

    return energy;
  }
  EnergyCategory category_{EnergyCategory::surface};  ///< The category of the energy function.
};

}  // namespace nastja
