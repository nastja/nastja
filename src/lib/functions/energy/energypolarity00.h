/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"
#include "lib/math/vector.h"
namespace nastja {

/**
 * @defgroup energypolarity Polarity energy
 *
 * @ingroup energyfunctions
 */

/**
 * Exerts a directional force on cell depending on their individual polarity vector.
 * By pushing and pulling.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energypolarity
 */
template <unsigned int DIM = 3>
class EnergyPolarity00 : public IEnergy<DIM> {
public:
  explicit EnergyPolarity00()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyPolarity00() override                         = default;
  EnergyPolarity00(const EnergyPolarity00&)            = delete;
  EnergyPolarity00(EnergyPolarity00&&) noexcept        = default;
  EnergyPolarity00& operator=(const EnergyPolarity00&) = delete;
  EnergyPolarity00& operator=(EnergyPolarity00&&)      = delete;

  void init(DataCells& data, config::Config& config) override {readDefaultPolarityConfig(data,config);}
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:


  void checkForBoundaryJump(block::Block* block, math::Vector3<real_t>& position){
    auto systemSize = static_cast<math::Vector3<long int>>(block->size * block->blocks);
    for (size_t i=0; i< 3;++i){
      if (position[i] > systemSize[i]/2){//NOT block size, we need the overall system size
        position[i] -= systemSize[i];
      }
      else if (position[i] < (-1*systemSize[i]/2)){
        position[i] += systemSize[i];
      }
    }
  }

  template <typename FieldIterator>
  //TODO disable when CellsInSilico.polarity.enabled is set to false in config
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& /*view*/, FieldIterator& it, stencil::Direction, EnergyFunctionPack& p) {


    real_t energy{0.};


    auto& lamPol = p.data.lambdaPolarity->getStorage();

    auto const& block {p.container.getBlock()};
    if (lamPol.getValue(p.centerCell.type) == 0. && lamPol.getValue(p.neighborCell.type) == 0.) return 0.;
    
    math::Vector3<real_t>position{};
    math::Vector3<real_t>offset{1,1,1};

    if (p.centerCellID > p.data.liquid){
      if ( p.centerCell.center == math::Vector3<int>{-1, -1, -1}) //We only calculate this if the cell center has been calculated at least once
        return energy;      
      position = block->offset + it.coordinates() - offset;
      position -= p.centerCell.center;

//      Check for periodic boundaries
      checkForBoundaryJump(block, position);
      real_t magnitude{sqrt(position[0]*position[0] + position[1]*position[1] + position[2]*position[2])};

      auto dotProd{math::dotProduct(position,p.centerCell.polarity)};
      energy -= lamPol.getValue(p.centerCell.type) * sqrt( std::max(static_cast<real_t>(0.), magnitude*magnitude - dotProd*dotProd) );
    }
    if (p.neighborCellID > p.data.liquid) {
      if (p.neighborCell.center == math::Vector3<int>{-1, -1, -1}) //We only calculate this if the cell center has been calculated at least once
        return energy;      
      position = block->offset + it.coordinates() - offset;
      position -= p.neighborCell.center;
      checkForBoundaryJump(block, position);
      real_t magnitude{sqrt(position[0]*position[0] + position[1]*position[1] + position[2]*position[2])};
  
      auto dotProd{math::dotProduct(position,p.neighborCell.polarity)};
      energy += lamPol.getValue(p.neighborCell.type) * sqrt(std::max(static_cast<real_t>(0.), magnitude*magnitude - dotProd*dotProd)  );      
    }
    return energy;
  }
  EnergyCategory category_{EnergyCategory::polarity};  ///< The category of the energy function.
};

}  // namespace nastja
