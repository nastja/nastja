/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "energy.h"

namespace nastja {

using namespace config::literals;

void readDefaultAdhesionConfig(DataCells& data, config::Config& config) {
  /// @key{CellsInSilico.adhesion.matrix, ParameterStorage<real_t>}
  /// The adhesion matrix. Controls the adhesion between different cell types.
  data.storageAdhesion = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.adhesion.matrix"_jptr, config);


  auto polarityEnabled{ config.getValue<bool>("CellsInSilico.adhesion.polarityenabled"_jptr, false)};
  if ( polarityEnabled ){
    /// @key{CellsInSilico.adhesion.polaritymatrix, ParameterStorage<real_t>}
    /// The adhesion matrix for polarity-based adhesion. Controls the adhesion between different cell types.
    data.storagePolarityAdhesion = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.adhesion.polaritymatrix"_jptr, config);  

    /// @key{CellsInSilico.adhesion.polaritythresholds, ParameterStorage<real_t>}
    /// The threshold vector for polarity-based adhesion. Controls the maximum angle between different cells adhering via polarity-based adhesion. Smaller values allow for larger angles
    data.storagePolarityAdhesionThreshold = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.adhesion.polaritythresholds"_jptr, config);  
  }
}

void readDefaultPotentialConfig(DataCells& data, config::Config& config) {
  /// @key{CellsInSilico.potential.coupling, ParameterStorage<real_t>}
  /// The coupling vector. Controls the coupling to the potential per cell typ.
  data.storageCoupling = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.potential.coupling"_jptr, config);

  /// @key{CellsInSilico.potential.direction, Vector3<real_t>}
  /// The potential direction vector, will be normalized.
  data.potentialDirection = config.template getVector3<real_t>("CellsInSilico.potential.direction"_jptr);
  data.potentialDirection.normalize();
}

void readDefaultSurfaceConfig(DataCells& data, config::Config& config) {
  /// @key{CellsInSilico.surface.default, ParameterStorage<real_t>}
  /// The vector of default surfaces per cell types.
  data.defaultSurface = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.surface.default"_jptr, config);

  /// @key{CellsInSilico.surface.lambda, ParameterStorage<real_t>}
  /// The prefactor @f$\lambda@f$ of the quadratic surface term. Controls the cell surface elasticity.
  data.lambdaSurface = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.surface.lambda"_jptr, config);

  /// @key{CellsInSilico.surface.sizechange, ParameterStorage<real_t>, 0}
  /// Factor that alters target surface S0 depending on time: target surface = S0 + sizechange * age. The default is a
  /// constant storage with value 0.
  if (config.contains("CellsInSilico.surface.sizechange"_jptr)) {
    data.changeSurface = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.surface.sizechange"_jptr, config);
  } else {
    data.changeSurface = std::make_unique<ParameterStorageBuilder<real_t>>();
  }
}

void readDefaultVolumeConfig(DataCells& data, config::Config& config) {
  /// @key{CellsInSilico.volume.default, ParameterStorage<real_t>}
  /// The vector of default volumes per cell types.
  data.defaultVolume = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.volume.default"_jptr, config);

  /// @key{CellsInSilico.volume.lambda, ParameterStorage<real_t>}
  /// The prefactor @f$\lambda@f$ of the quadratic volume term. Controls cell compressibility.
  data.lambdaVolume = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.volume.lambda"_jptr, config);

  /// @key{CellsInSilico.volume.sizechange, ParameterStorage<real_t>, 0}
  /// Factor that alters target volume V0 depending on time: target volume = V0 + sizechange * age. The default is a
  /// constant storage with value 0.
  if (config.contains("CellsInSilico.volume.sizechange"_jptr)) {
    data.changeVolume = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.volume.sizechange"_jptr, config);
  } else {
    data.changeVolume = std::make_unique<ParameterStorageBuilder<real_t>>();
  }
}

void readDefaultPolarityConfig(DataCells& data, config::Config& config) {
  /// @key{CellsInSilico.polarity.default, ParameterStorage<real_t>}
  /// The vector of polarity lambdas per cell types.
  data.lambdaPolarity = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.polarity.lambda"_jptr, config);
}


}  // namespace nastja
