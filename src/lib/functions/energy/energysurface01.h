/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"
#include "lib/math/calculatecubes.h"
#include "lib/math/calculatesurface.h"

namespace nastja {

/**
 * Class for energy surface 01 - marching cubes.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energysurfaces
 */
template <unsigned int DIM = 3>
class EnergySurface01 : public IEnergy<DIM> {
public:
  explicit EnergySurface01()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergySurface01() override                 = default;
  EnergySurface01(const EnergySurface01&)     = delete;
  EnergySurface01(EnergySurface01&&) noexcept = default;
  EnergySurface01& operator=(const EnergySurface01&) = delete;
  EnergySurface01& operator=(EnergySurface01&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultSurfaceConfig(data, config); }

  void setup(const block::Block* block, CellBlockContainer& container, const DataCells& data) override {
    container.surface = std::make_unique<math::SurfaceMC<>>();

    auto& field = block->getFieldByName("cells")->get<cellid_t>();

    auto view = field.createView();
    for (auto* voxel : view) {
      std::array<cellid_t, 8> cube = field.getCube(view.getIndex(voxel));  // TODO: getCube from view

      for (int b = 0; b < 8; b++) {
        if (cube[b] <= data.liquid || !math::isFirstValueInCube(cube, b)) continue;

        container.cells(cube[b]).deltaSurface += math::calcSurfaceMC(cube, cube[b]);
      }
    }
  }

  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  /**
   * Calculates the change in surface energy via marching cubes algorithm.
   *
   * @param view  The view.
   * @param it    The iterator.
   * @param p     The energyFunctionPack.
   *
   * @return Energy change.
   */
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& view, FieldIterator& it, stencil::Direction /*dir*/, EnergyFunctionPack& p) {
    auto& surf0              = p.data.defaultSurface->getStorage();
    auto& lamSurf            = p.data.lambdaSurface->getStorage();
    auto& sizeChangeConstant = p.data.changeSurface->getStorage();

    real_t energy = 0.0;
    if (p.centerCellID > p.data.liquid) {
      real_t deltaS = math::calcSurfaceMCDiff(view, *it, p.centerCellID, p.neighborCellID);

      p.changeCenterSurface += deltaS;

      //Now, calculate the
      real_t goalSurface  = std::max(surf0.getValue(p.centerCell.type) + sizeChangeConstant.getValue(p.centerCell.type) * (p.container.getSimData().timestep - p.centerCell.birth), real_t(0));
      real_t surfDiffPre  = goalSurface - p.centerCell.surface;  // S_0 - S_old before the change
      real_t surfDiffPost = surfDiffPre - deltaS;                // S_0 - S_new after the change

      energy -= lamSurf.getValue(p.centerCell.type) * (surfDiffPre * surfDiffPre - surfDiffPost * surfDiffPost);
    }

    if (p.neighborCellID > p.data.liquid) {
      real_t deltaS = math::calcSurfaceMCDiff(view, *it, p.neighborCellID, p.neighborCellID);

      p.changeNeighborSurface += deltaS;

      //Now, calculate the
      real_t goalSurface  = std::max(surf0.getValue(p.neighborCell.type) + sizeChangeConstant.getValue(p.neighborCell.type) * (p.container.getSimData().timestep - p.neighborCell.birth), real_t(0));
      real_t surfDiffPre  = goalSurface - p.neighborCell.surface;  // S_0 - S_old before the change
      real_t surfDiffPost = surfDiffPre - deltaS;                  // S_0 - S_new after the change

      energy -= lamSurf.getValue(p.neighborCell.type) * (surfDiffPre * surfDiffPre - surfDiffPost * surfDiffPost);
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::surface};  ///< The category of the energy function.
};

}  // namespace nastja
