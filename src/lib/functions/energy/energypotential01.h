/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * Class for energy potential 01.
 * Energy offset only on the advancing and trailing edge.
 * Meaning cells are only pulled and pushed.
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energypotentials
 */
template <unsigned int DIM = 3>
class EnergyPotential01 : public IEnergy<DIM> {
public:
  explicit EnergyPotential01()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyPotential01() override                   = default;
  EnergyPotential01(const EnergyPotential01&)     = delete;
  EnergyPotential01(EnergyPotential01&&) noexcept = default;
  EnergyPotential01& operator=(const EnergyPotential01&) = delete;
  EnergyPotential01& operator=(EnergyPotential01&&) = delete;

  void init(DataCells& data, config::Config& config) override { readDefaultPotentialConfig(data, config); }
  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& /*view*/, FieldIterator& /*it*/, stencil::Direction dir, EnergyFunctionPack& p) {
    const auto& coupling           = p.data.storageCoupling->getStorage();
    const auto& potentialDirection = p.data.potentialDirection;

    int typeCenterCell   = p.centerCell.type;
    int typeNeighborCell = p.neighborCell.type;
    return (coupling.getValue(typeNeighborCell) + coupling.getValue(typeCenterCell)) * math::dotProduct(dir, potentialDirection);
  }

  EnergyCategory category_{EnergyCategory::potential};  ///< The category of the energy function.
};

}  // namespace nastja
