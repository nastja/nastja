/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/energy/energy.h"

namespace nastja {

/**
 * Class for energy potential 02.
 * Pulls cells towards a point in space.
 * Coupling set via default coupling
 *
 * @tparam DIM  The dimension.
 *
 * @ingroup energyfunctions energypotentials
 */
template <unsigned int DIM = 3>
class EnergyPotential02 : public IEnergy<DIM> {
public:
  explicit EnergyPotential02()
      : IEnergy<DIM>{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
  }

  ~EnergyPotential02() override                   = default;
  EnergyPotential02(const EnergyPotential02&)     = delete;
  EnergyPotential02(EnergyPotential02&&) noexcept = default;
  EnergyPotential02& operator=(const EnergyPotential02&) = delete;
  EnergyPotential02& operator=(EnergyPotential02&&) = delete;

  void init(DataCells& data, config::Config& config) override {
    readDefaultPotentialConfig(data, config);
    using namespace config::literals;
    /// @key{CellsInSilico.potentialcenter, Vector3<real_t>}
    /// The center vector of the point cells are pulled towards with potential02 (positive coupling = pulling towards point, negative = repulsion).
    potCenter_ = config.template getVector3<real_t>("CellsInSilico.potential.center"_jptr);
  }

  void setup(const block::Block* /*block*/, CellBlockContainer& /*container*/, const DataCells& /*data*/) override {}
  unsigned int getCategory() const override { return toUnderlying(category_); }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCB<cellid_t>>& view, field::FieldIteratorCB<cellid_t>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

  real_t calc(const field::FieldView<cellid_t, 0, 1, field::FieldIteratorCBN<cellid_t, 2>>& view, field::FieldIteratorCBN<cellid_t, 2>& it, stencil::Direction dir, EnergyFunctionPack& p) override {
    return calcImpl(view, it, dir, p);
  }

private:
  template <typename FieldIterator>
  real_t calcImpl(const field::FieldView<cellid_t, 0, 1, FieldIterator>& /*view*/, FieldIterator& /*it*/, stencil::Direction dir, EnergyFunctionPack& p) {
    const auto& coupling = p.data.storageCoupling->getStorage();

    int typeCenterCell  = p.centerCell.type;
    int typeNeighborCell = p.neighborCell.type;

    auto centerCenter  = static_cast<math::Vector3<real_t>>(p.centerCell.center);
    auto centerNeighbor = static_cast<math::Vector3<real_t>>(p.neighborCell.center);

    auto directionVector = stencil::makeVector3<real_t>(dir);
    directionVector.normalize();

    auto potentialDirectionCenter  = potCenter_ - centerCenter;
    auto potentialDirectionNeighbor = potCenter_ - centerNeighbor;

    real_t energy = 0.0;

    if (p.centerCellID > p.data.liquid && potentialDirectionCenter !=  math::Vector3<real_t>::zero) {
      potentialDirectionCenter.normalize();
      energy += coupling.getValue(typeCenterCell) * math::dotProduct(potentialDirectionCenter, directionVector);
    }
    if (p.neighborCellID > p.data.liquid && potentialDirectionNeighbor != math::Vector3<real_t>::zero) {
      potentialDirectionNeighbor.normalize();
      energy += coupling.getValue(typeNeighborCell) * math::dotProduct(potentialDirectionNeighbor, directionVector);
    }

    return energy;
  }

  EnergyCategory category_{EnergyCategory::potential};  ///< The category of the energy function.
  math::Vector3<real_t> potCenter_{};                   ///< The center of the point towards cells are pulled.
};

}  // namespace nastja
