/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/functions/signal/signal.h"
#include "lib/math/random.h"
#include "lib/storage/paramstorage.h"
#include <cmath>
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for setting an external Potential to all cells.
 *
 * |
 * |
 * |--------\        /--------
 * |         \      /
 * |          \    /
 * |           \  /
 * |            \/
 * |
 * _______________________________
 *
 * Sets the signal of all cells depending on their position.
 * Constant to an external value, then a linear descend around one point in a radius, to a certain lowest value.
 *
 * The center of the dip moves with three sinus functions for each dimension wih A * sin(B * (time - C))
 *
 * @ingroup signalfunctions
 */
class ExternalPotential01 : public ISignal {
public:
  explicit ExternalPotential01()                      = default;
  ~ExternalPotential01() override                     = default;
  ExternalPotential01(const ExternalPotential01&)     = delete;
  ExternalPotential01(ExternalPotential01&&) noexcept = default;
  ExternalPotential01& operator=(const ExternalPotential01&) = delete;
  ExternalPotential01& operator=(ExternalPotential01&&) = delete;

  void init(DataCells& /*data*/, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.valueoutsidedip, int, 1}
    /// The constant signal value for all cells outside the dip
    valueOutsideDip_ = config.getValue<int>("CellsInSilico.signaling.valueoutsidedip"_jptr, 1);

    /// @key{CellsInSilico.signaling.dipvalue, int, 1}
    /// The signal value for the lowest part of the dip
    dipValue_ = config.getValue<int>("CellsInSilico.signaling.dipvalue"_jptr, 1);

    /// @key{CellsInSilico.signaling.dipradius, uint, 1}
    /// The radial extent of the dip around the center.
    dipRadius_ = config.getValue<unsigned int>("CellsInSilico.signaling.dipradius"_jptr, 1);

    /// @key{CellsInSilico.signaling.dipcenter, Vector3<uint>}
    /// The center vector of the potential dip.
    dipCenter_ = config.getVector3<unsigned int>("CellsInSilico.signaling.dipcenter"_jptr);

    /// @key{CellsInSilico.signaling.wobbleamplitude, Vector3<real_t>}
    /// The Amplitude of wobble for each dimension
    wobbleAmplitude_ = config.getVector3<real_t>("CellsInSilico.signaling.wobbleamplitude"_jptr);

    /// @key{CellsInSilico.signaling.wobbleomega, Vector3<real_t>}
    /// The omega sin(omega*(time-offset)) of wobble for each dimension
    wobbleOmega_ = config.getVector3<real_t>("CellsInSilico.signaling.wobbleomega"_jptr);

    /// @key{CellsInSilico.signaling.wobbleoffset, Vector3<real_t>}
    /// The offset sin(omega*(time-offset)) of wobble for each dimension
    wobbleOffset_ = config.getVector3<real_t>("CellsInSilico.signaling.wobbleoffset"_jptr);
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    for (auto& i : container.cells) {
      if (i.first <= data.liquid) continue;

      auto& cell   = i.second;
      auto& center = cell.center;

      auto newDipCenter = dipCenter_;
      for (int axis = 0; axis < 3; axis++) {
        newDipCenter[axis] += wobbleAmplitude_[axis] * std::sin(wobbleOmega_[axis] * (container.getSimData().timestep - wobbleOffset_[axis]));
      }

      real_t distanceFromCenter = (center - newDipCenter).length();
      //logger::get().info("Center {}, {}, {},  Dip center {},{},{}, Distance {}", center[0], center[1], center[2], dipcenter.getValue(0), dipcenter.getValue(1), dipcenter.getValue(2), distanceFromCenter);
      if (distanceFromCenter <= dipRadius_) {
        cell.signal[0] = dipValue_ + distanceFromCenter * ((valueOutsideDip_ - dipValue_) / dipRadius_);
      } else {
        cell.signal[0] = valueOutsideDip_;
      }

      cell.deltaSignal[0] = 0;
    }
  }

private:
  math::Vector3<unsigned int> dipCenter_;  ///< The center of the potential dip.
  math::Vector3<real_t> wobbleAmplitude_;
  math::Vector3<real_t> wobbleOmega_;
  math::Vector3<real_t> wobbleOffset_;
  unsigned int dipRadius_{};  ///< The radius for the potential dip.
  real_t valueOutsideDip_{};  ///< The constant value for all cells outside the dip.
  real_t dipValue_{};         ///< The lowest value inside the dip.
};

}  // namespace nastja
