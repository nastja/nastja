/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/simdata/datacells.h"
#include "lib/simdata/simdata.h"

namespace nastja {

/**
 * @defgroup signalfunctions Signal function
 *
 * Provides signaling functions for the cellular Potts model.
 *
 * @ingroup cells
 */

class ISignal {
public:
  explicit ISignal() = default;

  virtual ~ISignal()          = default;
  ISignal(const ISignal&)     = delete;
  ISignal(ISignal&&) noexcept = default;
  ISignal& operator=(const ISignal&) = delete;
  ISignal& operator=(ISignal&&) = delete;

  virtual void init(DataCells& data, config::Config& config, SimData& simdata) = 0;
  virtual void execute(const DataCells& data, CellBlockContainer& container)   = 0;

  virtual void dump(Archive& /*ar*/) const {}
  virtual void undump(Archive& /*ar*/) {}
};

}  // namespace nastja
