/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for signal decay 00 - absolute decay.
 * decreases the signal value of each cell by an absolute value (cell type dependant) each time step.
 *
 * @ingroup signalfunctions
 */
class SignalDecay00 : public ISignal {
public:
  explicit SignalDecay00() = default;

  ~SignalDecay00() override               = default;
  SignalDecay00(const SignalDecay00&)     = delete;
  SignalDecay00(SignalDecay00&&) noexcept = default;
  SignalDecay00& operator=(const SignalDecay00&) = delete;
  SignalDecay00& operator=(SignalDecay00&&) = delete;

  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.absolutedecaysignal0, ParameterStorage<real_t>}
    /// The vector of the absolute decay of signal 0.
    data.absoluteDecaySignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absolutedecaysignal0"_jptr, config);

    /// @key{CellsInSilico.signaling.absolutedecaysignal1, ParameterStorage<real_t>}
    /// The vector of the absolute decay of signal 1.
    data.absoluteDecaySignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absolutedecaysignal1"_jptr, config);

    /// @key{CellsInSilico.signaling.absolutedecaysignal2, ParameterStorage<real_t>}
    /// The vector of the absolute decay of signal 2.
    data.absoluteDecaySignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absolutedecaysignal2"_jptr, config);
    // TODO: change for a variable size of signals.
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    for (auto& i : container.cells) {
      auto& cell        = i.second;
      auto& constantSig = data.constantSignal->getStorage();
      if (constantSig.getValue(cell.type) == 1) continue;
      for (unsigned int sig = 0; sig < numberOfSignals; sig++) {
        auto& decayConst = data.absoluteDecaySignal[sig]->getStorage();
        cell.signal[sig] = cell.signal[sig] - decayConst.getValue(cell.type);
      }
    }
  }
};

}  // namespace nastja
