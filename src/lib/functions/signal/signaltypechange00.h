/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for signal dependant type change.
 *
 * @ingroup signalfunctions
 */
class SignalTypeChange00 : public ISignal {
public:
  explicit SignalTypeChange00() {}

  ~SignalTypeChange00() override                    = default;
  SignalTypeChange00(const SignalTypeChange00&)     = delete;
  SignalTypeChange00(SignalTypeChange00&&) noexcept = default;
  SignalTypeChange00& operator=(const SignalTypeChange00&) = delete;
  SignalTypeChange00& operator=(SignalTypeChange00&&) = delete;

  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.typechangegreater, ParameterStorage<real_t>}
    /// The vector of the array of the type change. The array contains the new type, the signal number, and the threshold value.
    /// If the value above the threshold the cell type changed.
    data.typeChangeGreater = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.typechangegreater"_jptr, config);

    /// @key{CellsInSilico.signaling.typechangesmaller, ParameterStorage<real_t>}
    /// The vector of the array of the type change. The array contains the new type, the signal number, and the threshold value.
    /// If the value below the threshold the cell type changed.
    data.typeChangeSmaller = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.typechangesmaller"_jptr, config);

    /// @key{CellsInSilico.signaling.typechangestart, int}
    /// The start time after which typechange can occur.
    startTime = config.template getValue<long unsigned int>("CellsInSilico.signaling.typechangestart"_jptr, 20000UL);

  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    auto& changeGreater = data.typeChangeGreater->getStorage();
    auto& changeSmaller = data.typeChangeSmaller->getStorage();

    for (auto& i : container.cells) {
      if (container.getSimData().timestep < startTime) continue;
      auto& cell = i.second;

      int signalNr{}, newType{}, signalThreshold{};

      int current_nbr_signals{4};//TODO make this an arbitrary number

      for (int entry = 0; entry < current_nbr_signals; ++entry) {

        if (cell.type == changeSmaller.getValue(entry, 0)) {

          signalNr        = changeSmaller.getValue(entry, 2);
          newType         = changeSmaller.getValue(entry, 1);
          signalThreshold = changeSmaller.getValue(entry, 3);

          if (cell.signal[signalNr] < signalThreshold) {
            cell.type = newType;
          }
        }
      }
      
      for (int entry = 0; entry < current_nbr_signals; ++entry) {
        if (cell.type == changeGreater.getValue(entry, 0)) {
          signalNr        = changeGreater.getValue(entry, 2);
          newType         = changeGreater.getValue(entry, 1);
          signalThreshold = changeGreater.getValue(entry, 3);
          if (cell.signal[signalNr] >= signalThreshold) {
            cell.type = newType;
          }
        }
      }
    }
  }
  long unsigned int startTime{};
};

}  // namespace nastja
