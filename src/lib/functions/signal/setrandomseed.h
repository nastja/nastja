/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/functions/signal/signal.h"
#include <memory>
#include <sys/time.h>

namespace nastja {

using namespace config::literals;

/**
 * Class for signal interaction 00 - interaction between signals
 *
 * @ingroup signalfunctions
 */
class SetRandomSeed : public ISignal {
public:
  explicit SetRandomSeed()                = default;
  ~SetRandomSeed() override               = default;
  SetRandomSeed(const SetRandomSeed&)     = delete;
  SetRandomSeed(SetRandomSeed&&) noexcept = default;
  SetRandomSeed& operator=(const SetRandomSeed&) = delete;
  SetRandomSeed& operator=(SetRandomSeed&&) = delete;

  void init(DataCells& /*data*/, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.randomSeedSettingPoint, uint}
    /// The time-step when the random seed is (re)set.
    randomSeedSettingPoint_ = config.getValue<unsigned long>("CellsInSilico.signaling.randomSeedSettingPoint"_jptr);

    /// @key{CellsInSilico.signaling.seed, uint, 0}
    /// Value the random seed is set to, 0 use the time.
    seed_ = config.getValue<unsigned long>("CellsInSilico.signaling.newRandomSeed"_jptr, 0);
  }

  void execute(const DataCells& /*data*/, CellBlockContainer& container) override {
    auto& simData = container.getSimData();
    auto timestep = simData.timestep;
    if (timestep == randomSeedSettingPoint_) {
      if (seed_ == 0) {
        seed_ = simData.createSeed();
      }
      simData.seedAll(seed_);
      logger::get("cellevents").info(R"("event": "Reset the random seed", "value": {})", seed_);
    }
  }

private:
  unsigned long randomSeedSettingPoint_{};
  unsigned long seed_{};
};

}  // namespace nastja
