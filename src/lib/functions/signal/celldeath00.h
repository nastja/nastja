/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/evaluate/cellextension.h"
#include "lib/evaluate/expressionstorage.h"
#include "lib/functions/signal/signal.h"
#include "lib/math/random.h"
#include "lib/storage/paramstorage.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for cell death.
 * Cell death ist achieved by changing cell type to the 'apoptoticCellType'
 * This cell type has changeSize < 0 so that the volume and surface are gradually lowered to 0 eventually disappearing
 * Cell death is dependant on various conditions: Age, Signals, and rate
 *
 * @ingroup signalfunctions
 */
class CellDeath00 : public ISignal {
public:
  explicit CellDeath00()              = default;
  ~CellDeath00() override             = default;
  CellDeath00(const CellDeath00&)     = delete;
  CellDeath00(CellDeath00&&) noexcept = default;
  CellDeath00& operator=(const CellDeath00&) = delete;
  CellDeath00& operator=(CellDeath00&&) = delete;

  void init(DataCells& /*data*/, config::Config& config, SimData& simdata) override {

    /// @key{CellsInSilico.celldeath.enabled, bool, true}
    /// @widget{enabled}
    /// Determines whether cell death can occur. Enabled per default if Celldeath00 is loaded in config
    enabled_ = config.template getValue <bool>("CellsInSilico.celldeath.enabled"_jptr, true);

    /// @key{CellsInSilico.celldeath.condition, string[]}
    /// @widget{condition}
    /// An evaluate string that determines the condition when a cell dies.
    auto conditions = config.template getVector<std::string>("CellsInSilico.celldeath.condition"_jptr);
    cellEvaluator_  = std::make_unique<evaluate::ExpressionStorage<evaluate::CellExtension>>(simdata, "Cell type {} register death condition '{}'.");
    for (unsigned int i = 0; i < conditions.size(); i++) {
      if (conditions[i].empty()) {
        cellEvaluator_->addCondition(i, "false");
      } else {
        cellEvaluator_->addCondition(i, conditions[i]);
      }
    }

    /// @key{CellsInSilico.celldeath.apoptoticcelltype, int, 1}
    /// The cell type id for the apoptotic cells.
    apoptoticCellType_ = config.template getValue<int>("CellsInSilico.celldeath.apoptoticcelltype"_jptr, 1);
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    if (enabled_ == false) return;
    
    for (auto& i : container.cells) {
      if (i.first <= data.liquid) continue;
      auto& cell      = i.second;

      cellEvaluator_->set(cell);
      if (!cellEvaluator_->condition(cell.type)) continue;

      container.typeChange[i.first] = apoptoticCellType_;
      logger::get("cellevents").info(R"("block": {}, "event": "death", "cell": {}, "type": {}")", container.getBlock()->id, i.first, cell.type);
    }
  }

private:
  std::unique_ptr<evaluate::ExpressionStorage<evaluate::CellExtension>> cellEvaluator_{};
  bool enabled_{false};      ///< Check whether this action is run.
  int apoptoticCellType_{};  ///< The cell type id for the apoptotic cells.
};

}  // namespace nastja
