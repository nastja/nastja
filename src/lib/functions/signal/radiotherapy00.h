/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/celldivision.h"
#include "lib/functions/signal/signal.h"
#include <algorithm>
#include <memory>
#include <vector>

namespace nastja {

using namespace config::literals;

/**
 * Class for setting a signal of a cell Type to an absolute value at a certain time.
 *
 * @ingroup signalfunctions
 */
class RadioTherapy00 : public ISignal {
public:
  explicit RadioTherapy00()                 = default;
  ~RadioTherapy00() override                = default;
  RadioTherapy00(const RadioTherapy00&)     = delete;
  RadioTherapy00(RadioTherapy00&&) noexcept = default;
  RadioTherapy00& operator=(const RadioTherapy00&) = delete;
  RadioTherapy00& operator=(RadioTherapy00&&) = delete;

  void init(DataCells& /*data*/, config::Config& config, SimData& /*simdata*/) override {
    /// @keyignore
    deathAge_ = std::make_unique<ParameterStorageBuilder<unsigned long>>("CellsInSilico.celldeath.age"_jptr, config);

    /// @key{CellsInSilico.radiation.immediateApoptosisRate, real_t}
    /// Rate of the immediate apoptosis.
    immediateApoptosisRate_ = config.getValue<real_t>("CellsInSilico.radiation.immediateApoptosisRate"_jptr);

    /// @key{CellsInSilico.radiation.divisionRateDiminishing, real_t}
    /// Diminishing of the division rate.
    divisionRateDiminishing_ = config.getValue<real_t>("CellsInSilico.radiation.divisionRateDiminishing"_jptr);

    /// @key{CellsInSilico.radiation.times, uint[]}
    /// Vector of times when the radiation is.
    radiationTimes_ = config.getVector<unsigned long>("CellsInSilico.radiation.times"_jptr);

    /// @keyignore
    apoptoticCellType_ = config.template getValue<int>("CellsInSilico.celldeath.apoptoticcelltype"_jptr, 1);
  }

  void execute(const DataCells& /*data*/, CellBlockContainer& container) override {
    auto& data             = container.getSimData().getData<DataCells>();
    unsigned long timeStep = container.getSimData().timestep;

    if (std::count(radiationTimes_.begin(), radiationTimes_.end(), timeStep) == 0) return;

    // Direct Apoptosis those cells die immediately after radiation
    for (auto& i : container.cells) {
      if (i.first <= data.liquid) continue;
      auto& cell = i.second;

      if (deathAge_->getStorage().getValue(cell.type) == 0) continue;  // If age is set to 0, no cell death for that cell type

      if (cell.type <= static_cast<int>(data.liquid) + 1) continue;                            // No immediate apoptosis for surrounding tissue
      if (probability_(container.getSimData().generator) > immediateApoptosisRate_) continue;  // Celldeath is accepted by a death rate (defined for every celltype)

      container.typeChange[i.first] = apoptoticCellType_;
      logger::get("cellevents").info(R"("block": {}, "event": "radiation-damage-apoptosis", "cell": {})", container.getBlock()->id, i.first);
    }

    // Upregulate death rates

    // Downregulate division rates
    data.divisionRateFactor = data.divisionRateFactor / divisionRateDiminishing_;
    logger::get("cellevents").info(R"("event": "Changed divisionRateFactor", "value": {})", data.divisionRateFactor);
  }

private:
  std::vector<unsigned long> radiationTimes_{};  ///< Vector of times.
  real_t immediateApoptosisRate_{};              ///< Rate of the immediate apoptosis.
  real_t divisionRateDiminishing_{};             ///< Diminishing of the division rate.
  int apoptoticCellType_{};                      ///< The cell type id for the apoptotic cells.

  std::unique_ptr<ParameterStorageBuilder<unsigned long>> deathAge_{nullptr};  // TODO: move deathAge to DataCells
  math::UniformReal<real_t> probability_{0, 1};
};

}  // namespace nastja
