/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/cell.h"
#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for signal interaction 00 - interaction between signals
 *
 * @ingroup signalfunctions
 */
class SignalInteraction00 : public ISignal {
public:
  explicit SignalInteraction00() = default;

  ~SignalInteraction00() override                     = default;
  SignalInteraction00(const SignalInteraction00&)     = delete;
  SignalInteraction00(SignalInteraction00&&) noexcept = default;
  SignalInteraction00& operator=(const SignalInteraction00&) = delete;
  SignalInteraction00& operator=(SignalInteraction00&&) = delete;

  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.interactionsignal0, ParameterStorage<real_t>}
    /// The vector of signal interactions for signal 0.
    data.interactionSignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.interactionsignal0"_jptr, config);

    /// @key{CellsInSilico.signaling.interactionsignal1, ParameterStorage<real_t>}
    /// The vector of signal interactions for signal 1.
    data.interactionSignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.interactionsignal1"_jptr, config);

    /// @key{CellsInSilico.signaling.interactionsignal2, ParameterStorage<real_t>}
    /// The vector of signal interactions for signal 2.
    data.interactionSignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.interactionsignal2"_jptr, config);
    // TODO: change for a variable size of signals.
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    for (auto& i : container.cells) {
      auto& cell        = i.second;
      auto& constantSig = data.constantSignal->getStorage();
      if (constantSig.getValue(cell.type) == 1) continue;
      auto& type = cell.type;

      for (unsigned int sig = 0; sig < numberOfSignals; sig++) {
        auto& interactionConst = data.interactionSignal[sig]->getStorage();
        cell.signal[sig] += interactionConst.getValue(sig, type) * cell.signal[sig];
      }
    }
  }
};

}  // namespace nastja
