/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for signal decay 01 - relative decay.
 * decreases the signal value of each cell by a factor (cell type dependant) at each time step.
 *
 * @ingroup signalfunctions
 */
class SignalDecay01 : public ISignal {
public:
  explicit SignalDecay01() {}

  ~SignalDecay01() override               = default;
  SignalDecay01(const SignalDecay01&)     = delete;
  SignalDecay01(SignalDecay01&&) noexcept = default;
  SignalDecay01& operator=(const SignalDecay01&) = delete;
  SignalDecay01& operator=(SignalDecay01&&) = delete;

  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.relativedecaysignal0, ParameterStorage<real_t>}
    /// The vector of the relative decay of signal 0.
    data.relativeDecaySignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.relativedecaysignal0"_jptr, config);

    /// @key{CellsInSilico.signaling.relativedecaysignal1, ParameterStorage<real_t>}
    /// The vector of the relative decay of signal 1.
    data.relativeDecaySignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.relativedecaysignal1"_jptr, config);

    /// @key{CellsInSilico.signaling.relativedecaysignal2, ParameterStorage<real_t>}
    /// The vector of the relative decay of signal 2.
    data.relativeDecaySignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.relativedecaysignal2"_jptr, config);
    // TODO: change for a variable size of signals.
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    for (auto& i : container.cells) {
      auto& cell        = i.second;
      auto& constantSig = data.constantSignal->getStorage();
      if (constantSig.getValue(cell.type) == 1) continue;
      for (unsigned int sig = 0; sig < numberOfSignals; sig++) {
        auto& decayConst = data.relativeDecaySignal[sig]->getStorage();
        cell.signal[sig] = cell.signal[sig] * decayConst.getValue(cell.type);
      }
    }
  }
};

}  // namespace nastja
