/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;

/**
 * Class for setting a signal of a cell Type to an absolute value at a certain time.
 *
 * @ingroup signalfunctions
 */
class SetSignal00 : public ISignal {
public:
  explicit SetSignal00() {}

  ~SetSignal00() override             = default;
  SetSignal00(const SetSignal00&)     = delete;
  SetSignal00(SetSignal00&&) noexcept = default;
  SetSignal00& operator=(const SetSignal00&) = delete;
  SetSignal00& operator=(SetSignal00&&) = delete;

  void init(DataCells& /*data*/, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.signaling.setsignaltimepoint, uint, 0}
    /// Time-step where the signal is set.
    timepoint_ = config.getValue<unsigned int>("CellsInSilico.signaling.setsignaltimepoint"_jptr, 0);

    /// @key{CellsInSilico.signaling.setsignalcell, uint, 0}
    /// CellID for which the signal is set.
    cellID_ = config.getValue<unsigned int>("CellsInSilico.signaling.setsignalcell"_jptr, 0);

    /// @key{CellsInSilico.signaling.setsignalsignr, uint, 0}
    /// Set this signal number.
    signal_ = config.getValue<unsigned int>("CellsInSilico.signaling.setsignalnumber"_jptr, 0);

    /// @key{CellsInSilico.signaling.setsignalvalue, uint, 0}
    /// Set the signal value to this value.
    value_ = config.getValue<unsigned int>("CellsInSilico.signaling.setsignalvalue"_jptr, 0);
  }

  void execute(const DataCells& /*data*/, CellBlockContainer& container) override {
    if (timepoint_ == 0 || container.getSimData().timestep != timepoint_) return;

    if (!container.cells.contains(cellID_)) return;

    container.cells(cellID_).signal[signal_] = value_;
    logger::get("cellevents").info(R"("block": {}, "event": "set signal {}", "cell": {}, "value": {})", container.getBlock()->id, signal_, cellID_, value_);
  }

private:
  unsigned int timepoint_{};  ///< The time-step when the signal is set.
  unsigned int cellID_{};     ///< The cellID for which the signal is set.
  unsigned int signal_{};     ///< The signal which is set.
  unsigned int value_{};      ///< The value of the signal.
};

}  // namespace nastja
