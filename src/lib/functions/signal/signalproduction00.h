/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/functions/signal/signal.h"
#include <memory>

namespace nastja {

using namespace config::literals;


/**
* Class for signal production 00 - absolute production.
* Increases the signal value of each cell by an absolute value (cell type dependent) each Nth step.
*
* @ingroup signalfunctions
**/

class SignalProduction00 : public ISignal {
public:
  explicit SignalProduction00() = default;

  ~SignalProduction00() override                        = default;
  SignalProduction00(SignalProduction00 const& )           = delete;
  SignalProduction00(SignalProduction00&& )     noexcept   = default;
  SignalProduction00& operator=(SignalProduction00 const&) = delete;
  SignalProduction00& operator=(SignalProduction00&&)      = delete;


  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {

    /// @key{CellsInSilico.signaling.absoluteproductionsignal0, ParameterStorage<real_t>}
    /// The vector of the absolute production of signal 0.
    data.absoluteProductionSignal[0] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absoluteproductionsignal0"_jptr, config);

    /// @key{CellsInSilico.signaling.absoluteproductionsignal1, ParameterStorage<real_t>}
    /// The vector of the absolute production of signal 1.
    data.absoluteProductionSignal[1] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absoluteproductionsignal1"_jptr, config);

    /// @key{CellsInSilico.signaling.absoluteproductionsignal2, ParameterStorage<real_t>}
    /// The vector of the absolute production of signal 2.
    data.absoluteProductionSignal[2] = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.signaling.absoluteproductionsignal2"_jptr, config);
    // TODO: change for a variable size of signals.
  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    for (auto& i : container.cells) {
      auto& cell        = i.second;
      auto& constantSig = data.constantSignal->getStorage();
      if (constantSig.getValue(cell.type) == 1) continue;
      for (unsigned int sig = 0; sig < numberOfSignals; sig++) {
        auto& productionConst = data.absoluteProductionSignal[sig]->getStorage();
        cell.signal[sig] += productionConst.getValue(cell.type);
      }
    }
  }


};

} // namespace nastja