/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/functions/signal/signal.h"
#include <algorithm>
#include <memory>
#include <vector>

namespace nastja {

using namespace config::literals;

/**
 * Class for setting a signal of a cell Type to an absolute value at a certain time.
 *
 * @ingroup signalfunctions
 */
class ChemoTherapy00 : public ISignal {
public:
  explicit ChemoTherapy00()                 = default;
  ~ChemoTherapy00() override                = default;
  ChemoTherapy00(const ChemoTherapy00&)     = delete;
  ChemoTherapy00(ChemoTherapy00&&) noexcept = default;
  ChemoTherapy00& operator=(const ChemoTherapy00&) = delete;
  ChemoTherapy00& operator=(ChemoTherapy00&&) = delete;

  void init(DataCells& data, config::Config& config, SimData& /*simdata*/) override {
    /// @key{CellsInSilico.chemotherapy.starttimes, uint[]}
    /// Vector of times when the chemotherapy starts.
    startTimes_ = config.getVector<unsigned long>("CellsInSilico.chemotherapy.starttimes"_jptr);

    /// @key{CellsInSilico.chemotherapy.duration, uint}
    /// Duration of the chemotherapy pulses.
    duration_ = config.getValue<unsigned long>("CellsInSilico.chemotherapy.duration"_jptr);

    /// @key{CellsInSilico.chemotherapy.strength, real_t}
    /// Chemotherapy strength. The provider cell (usually the blood vessels) get a signal of this strength.
    strength_ = config.getValue<real_t>("CellsInSilico.chemotherapy.strength"_jptr);

    /// @key{CellsInSilico.chemotherapy.provider, uint, 1}
    /// Cell type of the provider.
    provider_ = config.getValue<cellid_t>("CellsInSilico.chemotherapy.provider"_jptr, 1);

    /// @key{CellsInSilico.chemotherapy.signal, int, 1}
    /// The signal for the chemotherapeutic drugs.
    signal_ = config.getValue<int>("CellsInSilico.chemotherapy.signal"_jptr, 1);

    for (unsigned int i = 0; i < startTimes_.size() - 1; i++) {
      if (startTimes_[i + 1] < startTimes_[i] + duration_) {
        throw std::invalid_argument(fmt::format("The distance of 'CellsInSilico.chemotherapy.starttimes' must greater than the 'CellsInSilico.chemotherapy.duration'. Check [{}]={}, [{}]={}.",
                                                i, startTimes_[i], i + 1, startTimes_[i + 1]));
      }
    }
    auto& actualDiffusionConstants_ = data.diffusionSignal[signal_]->getStorage();
    for (size_t i = 0; i < data.constantSignal->getStorage().getSize(); ++i) originalDiffusionConstants_.push_back(actualDiffusionConstants_.getValue(provider_,i));

  }

  void execute(const DataCells& data, CellBlockContainer& container) override {
    if (!container.cells.contains(provider_)) return;

    unsigned long timeStep = container.getSimData().timestep;
    auto& actualDiffusionConstants_ = data.diffusionSignal[signal_]->getStorage();

    if (nextend_ > 0) {
      if (nextend_ == timeStep) {
        nextend_                                   = 0;
        container.cells(provider_).signal[signal_] = 0.0;
        for (size_t i = 0; i < data.constantSignal->getStorage().getSize(); ++i){
          actualDiffusionConstants_.setValue(provider_,i,0.0);
          actualDiffusionConstants_.setValue(i,provider_,0.0);
        }
        logger::get("cellevents").info(R"("block": {}, "event": "set signal {}", "cell": {}, "value": 0)", container.getBlock()->id, signal_, provider_);
      }
    } else {
      if (std::count(startTimes_.begin(), startTimes_.end(), timeStep) == 0) return;
      nextend_                                   = timeStep + duration_;
      container.cells(provider_).signal[signal_] = strength_;
      for (size_t i = 0; i < data.constantSignal->getStorage().getSize(); ++i){
        actualDiffusionConstants_.setValue(provider_,i,originalDiffusionConstants_[i]);
        actualDiffusionConstants_.setValue(i,provider_,originalDiffusionConstants_[i]);
      }      
      logger::get("cellevents").info(R"("block": {}, "event": "set signal {}", "cell": {}, "value": {})", container.getBlock()->id, signal_, provider_, strength_);
    }
  }

private:
  std::vector<unsigned long> startTimes_{};  ///< Vector of start times.
  std::vector<real_t> originalDiffusionConstants_{};  ///< Vector of the original diffusion constants
  unsigned long duration_{};                 ///< The duration of a single chemotherapeutic pulse.
  real_t strength_{0.0};                     ///< Chemotherapy strength.
  cellid_t provider_{};                      ///< The provider cell type.
  int signal_{};                             ///< The signal number.
  unsigned long nextend_{0};  ///< The time-step when the current pulse ends.
};

}  // namespace nastja
