/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/datatypes.h"
#include "lib/factory.h"
#include "lib/logger.h"
#include "lib/functions/signal/celldeath00.h"
#include "lib/functions/signal/chemotherapy00.h"
#include "lib/functions/signal/externalPotential00.h"
#include "lib/functions/signal/externalPotential01.h"
#include "lib/functions/signal/radiotherapy00.h"
#include "lib/functions/signal/setrandomseed.h"
#include "lib/functions/signal/setsignal00.h"
#include "lib/functions/signal/signal.h"
#include "lib/functions/signal/signaldecay00.h"
#include "lib/functions/signal/signaldecay01.h"
#include "lib/functions/signal/signalproduction00.h"
#include "lib/functions/signal/signalinteraction00.h"
#include "lib/functions/signal/signaltypechange00.h"

namespace nastja {

class FactorySignal : public Factory<ISignal> {
public:
  /// @factory{FactorySignal}
  /// "Decay00", "Decay01", "Interaction00", "Typechange00", "Celldeath00", "Setsignal00", "Chemotherapy00", "Radiotherapy00", "SetRandomSeed", "ExternalPotential00"
  explicit FactorySignal() {
    registerType<SignalDecay00>("Decay00");
    registerType<SignalDecay01>("Decay01");
    registerType<SignalProduction00>("Produce00");
    registerType<SignalInteraction00>("Interaction00");
    registerType<SignalTypeChange00>("Typechange00");
    registerType<CellDeath00>("Celldeath00");
    registerType<SetSignal00>("Setsignal00");
    registerType<ChemoTherapy00>("Chemotherapy00");
    registerType<RadioTherapy00>("Radiotherapy00");
    registerType<SetRandomSeed>("SetRandomSeed");
    registerType<ExternalPotential00>("ExternalPotential00");
    registerType<ExternalPotential01>("ExternalPotential01");
  }
};

}  // namespace nastja
