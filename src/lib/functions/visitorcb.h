/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/functions/visitor.h"
#include "lib/math/arithmetic.h"
#include "lib/math/random.h"
#include "sweeps/sweep_cells.h"
#include <random>

namespace nastja {

using namespace config::literals;

/**
 * Class for visitor with checkerboard
 *
 * @tparam TColors  The colors per dimension, or 1 for the 2-color checkerboard.
 * @tparam DIM      The dimension.
 *
 * @ingroup visitors cells
 */
template <unsigned int TColors, unsigned int DIM = 3>
class VisitorCB : public IVisitor<DIM> {
public:
  explicit VisitorCB(SimData& simdata)
      : IVisitor<DIM>{simdata}, generator_{simdata.generator} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");

    /// @key{CellsInSilico.visitor.stepwidth, uint, 10}
    /// The mean step width in voxel for the visitor.
    int stepWidth = simdata.getConfig().getValue<unsigned int>("CellsInSilico.visitor.stepwidth"_jptr, 10);

    stepWidth *= 2;
    stepWidth = math::divRoundUp(stepWidth, TColors == 1 ? 2 : TColors * TColors * TColors);

    uniformStep_ = math::UniformInt<int>{1, stepWidth - 1};
  }

  ~VisitorCB() override           = default;
  VisitorCB(const VisitorCB&)     = delete;
  VisitorCB(VisitorCB&&) noexcept = default;
  VisitorCB& operator=(const VisitorCB&) = delete;
  VisitorCB& operator=(VisitorCB&&) = delete;

  void visit(field::Field<cellid_t, 1>& field, CellBlockContainer& container, SweepCells<DIM>& sweep) final {
    auto view     = field.createViewCB<TColors>();
    auto& data    = field.getSimData().getData<DataCells>();
    auto rndSteps = IVisitor<DIM>::preSteps(uniformStep_, generator_);

    auto it = view.begin();
    it += rndSteps;
    while (it != view.end()) {
      auto direction = stencil::D3C6::dir[uniformSide_(generator_)];
      if (view.get(*it, direction) != view.get(*it) && view.get(*it) >= data.liquid) {
        sweep.monteCarlo(view, it, direction, container, data);
      }

      it += uniformStep_(generator_);
    }
  }

private:
  math::UniformInt<int> uniformStep_{1, 10};           ///< The uniform distribution for the step sizes.
  math::UniformInt<int> uniformSide_{0, 2 * DIM - 1};  ///< The uniform distribution for the side sizes.
  std::mt19937_64& generator_;                         ///< The reference to the random number generator.
};

}  // namespace nastja
