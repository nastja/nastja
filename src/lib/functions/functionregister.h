/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datatypes.h"
#include <memory>
#include <vector>

namespace nastja {

template <typename Function>
class FunctionRegister {
public:
  explicit FunctionRegister()                   = default;
  virtual ~FunctionRegister()                   = default;
  FunctionRegister(const FunctionRegister&)     = delete;
  FunctionRegister(FunctionRegister&&) noexcept = default;
  FunctionRegister& operator=(const FunctionRegister&) = delete;
  FunctionRegister& operator=(FunctionRegister&&) = delete;

  Function* registerFunction(std::unique_ptr<Function> function) {
    functionRegister_.push_back(std::move(function));
    return functionRegister_.back().get();
  }

  template <typename... Args>
  void setup(Args&&... args) {
    for (auto& f : functionRegister_) {
      f->setup(std::forward<Args>(args)...);
    }
  }

  template <typename... Args>
  void init(Args&&... args) {
    for (auto& f : functionRegister_) {
      f->init(std::forward<Args>(args)...);
    }
  }

  template <typename... Args>
  void execute(Args&&... args) {
    for (auto& f : functionRegister_) {
      f->execute(std::forward<Args>(args)...);
    }
  }

  template <typename... Args>
  real_t calc(Args&&... args) {
    real_t sum = 0.0;

    for (auto& f : functionRegister_) {
      sum += f->calc(std::forward<Args>(args)...);
    }

    return sum;
  }

  void dump(Archive& ar) const {
    for (auto& f : functionRegister_) {
      f->dump(ar);
    }
  }

  void undump(Archive& ar) {
    for (auto& f : functionRegister_) {
      f->undump(ar);
    }
  }

private:
  std::vector<std::unique_ptr<Function>> functionRegister_;  ///< vector of pointers to all energies
};

}  // namespace nastja
