/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/functions/visitor.h"
#include "lib/functions/visitorcb.h"

namespace nastja {

template <unsigned int DIM = 3>
class FactoryVisitor : public Factory<IVisitor<DIM>, SimData&> {
public:
  /// @factory{FactoryVisitor}
  /// "Visitor00", "Visitor01", "Visitor02", "Visitor04"
  explicit FactoryVisitor() {
    Factory<IVisitor<DIM>, SimData&>::template registerType<VisitorCB<1,DIM>>("00");
    Factory<IVisitor<DIM>, SimData&>::template registerType<VisitorCB<2,DIM>>("01");
  }
};

}  // namespace nastja
