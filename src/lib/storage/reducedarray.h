/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include <array>
#include <limits>
#include <stdexcept>

namespace nastja {

/**
 * Array with index value pairs.
 *
 * @tparam N maximum number of elements
 */
template <unsigned int N>
class ReducedArray {
public:
  explicit ReducedArray() {
    reset();
  }

  /**
   * Reset all values to 0.0 and indices to -1.
   */
  void reset() {
    for (unsigned int i = 0; i < N; i++) {
      array[i].first  = std::numeric_limits<cellid_t>::max();
      array[i].second = 0.0;
    }
  }

  /**
   * Add value to an exist index or insert index in the array.
   *
   * @param index  The index.
   * @param value  The value.
   *
   * @throws std::out_of_range  if there is no space left.
   */
  void add(cellid_t index, real_t value) {
    for (unsigned int i = 0; i < N; i++) {
      if (array[i].first == index) {
        array[i].second += value;
        return;
      }

      if (array[i].first == std::numeric_limits<cellid_t>::max()) {
        array[i].first  = index;
        array[i].second = value;
        return;
      }
    }
    throw std::out_of_range("Reached end of array.");
  }

  /**
   * Insert index and value to the array if the index does not exist.
   *
   * @param index The index.
   * @param value The value.
   *
   * @return false if the index already exist. true if inserted.
   * @throw std::out_of_range if there is no space left.
   */
  bool insertNew(cellid_t index, real_t value) {
    for (unsigned int i = 0; i < N; i++) {
      if (array[i].first == index) {
        return false;
      }

      if (array[i].first == std::numeric_limits<cellid_t>::max()) {
        array[i].first  = index;
        array[i].second = value;
        return true;
      }
    }
    throw std::out_of_range("Reached end of array.");
  }

  /**
   * Checks if cellID is in array.
   *
   * @param cellID  The cellID.
   *
   * @return true if cell ID is in array, false else.
   */
  bool has(cellid_t cellID) {
    for (unsigned int i = 0; i < N; i++) {
      if (array[i].first == cellID) {
        return true;
      }

      if (array[i].first == std::numeric_limits<cellid_t>::max()) {
        return false;
      }
    }

    return false;
  }

  /**
   * Check if index is a valid cellID.
   *
   * @param index The index.
   *
   * @return true if index is valid, false else.
   */
  bool isValid(unsigned int index) {
    return array[index].first != std::numeric_limits<cellid_t>::max();
  }

  std::array<std::pair<cellid_t, real_t>, N> array;  ///< The array for the first N cell ID and value paris.
};

}  // namespace nastja
