/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/storage/paramstorageinterface.h"

namespace nastja {

template <typename T>
class ParameterStorageMapped : public IParameterStorage<T> {
public:
  explicit ParameterStorageMapped(const config::json::json_pointer& jptr, const config::Config& config) : IParameterStorage<T>{} {
    auto storage = config.getValue<std::string>(jptr / "storage", "");
    if (storage != "mapped") {
      throw std::invalid_argument(fmt::format("ParameterStorageMapped needs '{} = mapped'.", jptr / "storage"));
    }

    if (!config.isArray(jptr / "data")) {
      throw std::invalid_argument(fmt::format("No parameter data found, expect array '{}'.", jptr / "data"));
    }

    dimension_ = IParameterStorage<T>::findDimension(jptr / "data", config);
    size_      = config.arraySize(jptr / "data");
    data_      = IParameterStorage<T>::readParameter(jptr / "data", config, dimension_, size_);

    config.load1DArray(mapping_, size_, jptr / "mapping");
    unsigned int sum = 0;
    for (unsigned int i = 0; i < size_; i++) {
      sum += mapping_[i];
      mapping_[i] = sum;
    }
  }

  explicit ParameterStorageMapped(const std::string& key, const config::Config& config)
      : ParameterStorageMapped<T>(config::getJsonPointer(key), config) {}

  ParameterStorageMapped(const ParameterStorageMapped&)     = delete;
  ParameterStorageMapped(ParameterStorageMapped&&) noexcept = default;
  ParameterStorageMapped& operator=(const ParameterStorageMapped&) = delete;
  ParameterStorageMapped& operator=(ParameterStorageMapped&&) = delete;
  ~ParameterStorageMapped()                                   = default;

  T getValue(const unsigned int a) const override { return data_[mapped(a)]; }
  T getValue(const unsigned int a, const unsigned int b) const override { return data_[mapped(a) * size_ + mapped(b)]; }
  T getValue(const unsigned int a, const unsigned int b, const unsigned int c) const override { return data_[(mapped(a) * size_ + mapped(b)) * size_ + mapped(c)]; }

  void setValue(const unsigned int a, T val) override { data_[mapped(a)] = val; }
  void setValue(const unsigned int a, const unsigned int b, T val) override { data_[mapped(a) * size_ + mapped(b)] = val; }
  void setValue(const unsigned int a, const unsigned int b, const unsigned int c, T val) override { data_[(mapped(a) * size_ + mapped(b)) * size_ + mapped(c)] = val; }  

  unsigned int getDimension() const override { return dimension_; }
  unsigned int getSize() const override { return size_; }

  void normalizeRows() override { throw std::runtime_error("Normalize is not supported for mapped ParameterStorages."); }

private:
  unsigned int mapped(const unsigned int index) const {
    for (unsigned int i = 0; i < size_; i++) {
      if (mapping_[i] > index) return i;
    }
    return size_ - 1;
  }

  std::vector<T> data_;                ///< The array of data elements.
  std::vector<unsigned int> mapping_;  ///< The vector of the mapping.
  unsigned int dimension_{};           ///< The dimension.
  unsigned int size_{};                ///< The size of the data elements.
};

}  // namespace nastja
