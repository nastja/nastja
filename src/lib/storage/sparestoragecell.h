/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/cell.h"
#include "lib/datatypes.h"
#include "lib/storage/sparestorage.h"

namespace nastja {

class SpareStorageCell : public SpareStorage<Cell> {
public:
  explicit SpareStorageCell() = default;

  void processDeltas() {
    for (auto& i : getData()) {
      i.second.addDeltas(i.second);
      i.second.clearDeltas();
    }
  }

  void clearNeighbors() {
    for (auto& i : getData()) {
      i.second.neighbors.clear();
    }
  }

  /**
   * Dump the storage data.
   * The first entry is the number of elements.
   *
   * @param ar Reference to the Archive.
   */
  void dump(Archive& ar) const {
    unsigned int size = getData().size();
    ar.pack(size);
    for (const auto& i : getData()) {
      ar.pack(i.first);
      ar.dumpObj(i.second);
    }
  }

  /**
   * Unpack the storage data.
   * This add or replace entry.
   *
   * @param ar Reference to the Archive.
   */
  void undump(Archive& ar) {
    unsigned int size{};
    cellid_t key{};
    ar.unpack(size);
    for (unsigned int i = 0; i < size; i++) {
      ar.unpack(key);
      ar.undumpObj(getData()[key]);
    }
  }
};

}  // namespace nastja
