/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/logger.h"
#include <memory>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

namespace nastja {

/**
 * This class describes an extension storage.
 *
 * @tparam Extension  The base class for all the extensions.
 */
template <typename Extension>
class ExtensionStorage {
public:
  explicit ExtensionStorage() = default;

  /**
   * Gets the extension.
   *
   * @tparam E  The extension type.
   *
   * @return Reference to the extension.
   */
  template <typename E, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  E& get() {
    auto type = std::type_index(typeid(E));

    if (!has<E>()) {
      throw std::runtime_error(fmt::format("The extension '{}' is not initialized. Use init() before get().", type.name()));
    }

    auto* ad = dynamic_cast<E*>(extensions_[type].get());
    assert(ad != nullptr);

    return *ad;
  }

  /**
   * Determines if the extension is registered.
   *
   * @tparam E  The extension type.
   *
   * @return True if the extension is registered, False otherwise.
   */
  template <typename E, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  bool has() const {
    auto type = std::type_index(typeid(E));

    return extensions_.count(type);
  }

  /**
   * Initializes the extension.
   *
   * @param args  The packed arguments.
   *
   * @tparam E     The extension type.
   * @tparam ARGs  The parameter pack.
   *
   * @return Reference to the extension.
   */
  template <typename E, typename... ARGs, typename = std::enable_if_t<std::is_base_of<Extension, E>::value>>
  E& init(ARGs&&... args) {
    auto type = std::type_index(typeid(E));
    if (!has<E>()) {
      extensions_[type] = std::make_unique<E>(std::forward<ARGs>(args)...);
    }

    return get<E>();
  }

  auto begin() const { return extensions_.begin(); }
  auto begin() { return extensions_.begin(); }
  auto end() const { return extensions_.end(); }
  auto end() { return extensions_.end(); }

  void dump(Archive& ar) const {
    for(auto& container : extensions_) {
      container.second->dump(ar);
    }
  }

  void undump(Archive& ar) {
    for(auto& container : extensions_) {
      container.second->undump(ar);
    }
  }
private:
  std::unordered_map<std::type_index, std::unique_ptr<Extension>> extensions_;  ///< Storage for extension objects.
};

}  // namespace nastja
