/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/storage/paramstorageconst.h"
#include "lib/storage/paramstoragedefault.h"
#include "lib/storage/paramstoragemapped.h"
#include <memory>
#include <type_traits>

namespace nastja {

template <typename T>
class ParameterStorageBuilder {
public:
  explicit ParameterStorageBuilder(const config::json::json_pointer& jptr, const config::Config& config) {
    static_assert(std::is_arithmetic<T>::value, "Only integral type or floating-point types are supported.");

    if (config.isArray(jptr)) {
      storage_ = std::make_unique<ParameterStorage<T>>(jptr, config);
    } else if (config.isObject(jptr)) {
      const auto storageType = config.getValue<std::string>(jptr / "storage", "");
      if (storageType == "mapped") {
        storage_ = std::make_unique<ParameterStorageMapped<T>>(jptr, config);
      } else if (storageType == "const") {
        storage_ = std::make_unique<ParameterStorageConst<T>>(jptr, config);
      } else {
        throw std::invalid_argument(fmt::format("Unknown storage '{}' for {}.", storageType, jptr));
      }
    } else {
      storage_ = std::make_unique<ParameterStorageConst<T>>(config.getValue<T>(jptr));
    }
  }

  explicit ParameterStorageBuilder(const std::string& key, const config::Config& config)
      : ParameterStorageBuilder<T>(config::getJsonPointer(key), config) {}

  /**
   * Constructs a new instance with a constant default value.
   *
   * @param value  The devault value.
   */
  explicit ParameterStorageBuilder(T value = T{}) {
    storage_ = std::make_unique<ParameterStorageConst<T>>(value);
  }

  IParameterStorage<T>& getStorage() { return *storage_; }

private:
  std::unique_ptr<IParameterStorage<T>> storage_;  ///< The pointer to the parameter storage.
};

}  // namespace nastja
