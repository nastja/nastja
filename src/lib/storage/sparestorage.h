/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datatypes.h"
#include "absl/container/flat_hash_map.h"
#include "robin_hood.h"
#include <unordered_map>

namespace nastja {

template <typename T, std::size_t N>
std::array<T, N>& operator+=(std::array<T, N>& lhs, const std::array<T, N> rhs) {
  for (unsigned int i = 0; i < N; i++) {
    lhs[i] += rhs[i];
  }

  return lhs;
}

template <typename T, std::size_t N>
std::array<T, N>& operator+=(std::array<T, N>& array, const std::initializer_list<T>& list) {
  int index = 0;
  for (auto value : list) {
    array[index++] += value;
  }

  return array;
}

#ifdef __FAST_MATH__
template <typename T, typename Container = absl::flat_hash_map<cellid_t, T>>
#else
template <typename T, typename Container = robin_hood::unordered_map<cellid_t, T>>
#endif
class SpareStorage {
public:
  explicit SpareStorage() = default;

  T& operator()(cellid_t index) {
    return data_.at(index);
  }

  const T& operator()(cellid_t index) const {
    return data_.at(index);
  }

  T& operator[](cellid_t index) {
    return data_[index];
  }

  auto find(cellid_t key) {
    return data_.find(key);
  }

  bool contains(cellid_t key) const {
    auto ret = data_.find(key);
    return !(ret == data_.end());
  }

  void erase(cellid_t key) {
    data_.erase(key);
  }

  template <typename IteratorType>
  auto erase(IteratorType i) {
    return data_.erase(i);
  }

  const auto& getData() const { return data_; }
  auto& getData() { return data_; }
  std::size_t size() const { return data_.size(); }
  bool empty() const { return data_.empty(); }

  auto begin() const { return data_.begin(); }
  auto begin() { return data_.begin(); }

  auto end() const { return data_.end(); }
  auto end() { return data_.end(); }

  void shrinkStorage() { shrinkStorageImpl<Container>(); }

  void clear() { data_.clear(); }

  /**
   * Pack the storage data.
   * The first entry is the number of elements.
   *
   * @param ar Reference to the Archive.
   */
  void pack(Archive& ar) const {
    unsigned int size = data_.size();
    ar.pack(size);
    for (auto& i : data_) {
      ar.pack(i.first);
      ar.pack(i.second);
    }
  }

  template <typename Functor>
  void pack(Archive& ar, Functor& lambda) const {
    unsigned int size{};
    ar.startArray(size);
    for (auto& i : data_) {
      if (!lambda(i.first)) continue;
      size++;
      ar.pack(i.first);
      ar.pack(i.second);
    }
    ar.endArray(size);
  }

  /**
   * Unpack the storage data.
   * This add or replace entry.
   *
   * @param ar Reference to the Archive.
   */
  void unpack(Archive& ar) {
    unsigned int size{};
    cellid_t key{};
    ar.unpack(size);
    for (unsigned int i = 0; i < size; i++) {
      ar.unpack(key);
      ar.unpack(data_[key]);
    }
  }

private:
  template <typename C>
  typename std::enable_if<std::is_same<C, absl::flat_hash_map<cellid_t, T>>::value, void>::type
  shrinkStorageImpl() {
    for (auto itr = data_.begin(); itr != data_.end(); ++itr) {
      if ((*itr).second == T{}) {
        data_.erase(itr);
      }
    }
  }

  template <typename C>
  typename std::enable_if<!std::is_same<C, absl::flat_hash_map<cellid_t, T>>::value, void>::type
  shrinkStorageImpl() {
    for (auto itr = data_.begin(); itr != data_.end();) {
      if ((*itr).second == T{}) {
        itr = data_.erase(itr);
      } else {
        ++itr;
      }
    }
  }

  Container data_{};  ///< The map of cell ID and value.
};

template <typename T, std::size_t N>
using SpareStorageArray = SpareStorage<std::array<T, N>>;

}  // namespace nastja
