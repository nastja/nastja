/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/storage/paramstorageinterface.h"
#include <stdexcept>
#include <type_traits>

namespace nastja {

template <typename T>
class ParameterStorage : public IParameterStorage<T> {
public:
  explicit ParameterStorage(const config::json::json_pointer& jptr, const config::Config& config) : IParameterStorage<T>{} {
    if (!config.isArray(jptr)) {
      throw std::invalid_argument(fmt::format("No parameter data found, expect array {}.", jptr));
    }
    dimension_ = IParameterStorage<T>::findDimension(jptr, config);
    size_      = config.arraySize(jptr);
    data_      = IParameterStorage<T>::readParameter(jptr, config, dimension_, size_);
  }

  explicit ParameterStorage(const std::string& key, const config::Config& config)
      : ParameterStorage<T>(config::getJsonPointer(key), config) {}

  ~ParameterStorage()                           = default;
  ParameterStorage(const ParameterStorage&)     = delete;
  ParameterStorage(ParameterStorage&&) noexcept = default;
  ParameterStorage& operator=(const ParameterStorage&) = delete;
  ParameterStorage& operator=(ParameterStorage&&) = delete;

  T getValue(const unsigned int a) const override { return data_[a]; }
  T getValue(const unsigned int a, const unsigned int b) const override { return data_[a * size_ + b]; }
  T getValue(const unsigned int a, const unsigned int b, const unsigned int c) const override { return data_[(a * size_ + b) * size_ + c]; }

  void setValue(unsigned int a, T val) override {data_[a] = val;}
  void setValue(unsigned int a, unsigned int b, T val) override {data_[a * size_ + b] = val;}
  void setValue(unsigned int a, unsigned int b, unsigned int c, T val) override {data_[(a*size_ + b) * size_ + c] = val;}

  unsigned int getDimension() const override { return dimension_; }
  unsigned int getSize() const override { return size_; }

  void normalizeRows() override {
    if (!std::is_floating_point<T>::value) {
      throw std::runtime_error("Normalize works only for ParamaterStorages with floating point type");
    }

    for (unsigned int a = 0; a < (dimension_ >= 3 ? size_ : 1); a++) {
      for (unsigned int b = 0; b < (dimension_ >= 2 ? size_ : 1); b++) {
        T sum{};
        for (unsigned int c = 0; c < size_; c++) {
          sum += getValue(a, b, c);
        }
        if (sum == 0.) continue;
        sum = 1.0 / sum;
        for (unsigned int c = 0; c < size_; c++) {
          data_[(a * size_ + b) * size_ + c] *= sum;
        }
      }
    }
  }

private:
  std::vector<T> data_;       ///< The array of data elements.
  unsigned int dimension_{};  ///< The dimension.
  unsigned int size_{};       ///< The size of the data elements.
};

}  // namespace nastja
