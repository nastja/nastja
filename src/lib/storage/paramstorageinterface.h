/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"

namespace nastja {

template <typename T>
class IParameterStorage {
public:
  explicit IParameterStorage()                    = default;
  virtual ~IParameterStorage()                    = default;
  IParameterStorage(const IParameterStorage&)     = delete;
  IParameterStorage(IParameterStorage&&) noexcept = default;
  IParameterStorage& operator=(const IParameterStorage&) = delete;
  IParameterStorage& operator=(IParameterStorage&&) = delete;

  virtual T getValue(unsigned int a) const                                 = 0;
  virtual T getValue(unsigned int a, unsigned int b) const                 = 0;
  virtual T getValue(unsigned int a, unsigned int b, unsigned int c) const = 0;

  virtual void setValue(unsigned int a, T val)                                 = 0;
  virtual void setValue(unsigned int a, unsigned int b, T val)                 = 0;
  virtual void setValue(unsigned int a, unsigned int b, unsigned int c, T val) = 0;  

  virtual unsigned int getDimension() const = 0;
  virtual unsigned int getSize() const      = 0;

  virtual void normalizeRows() = 0;

protected:
  static std::vector<T> readParameter(const config::json::json_pointer& jptr, const config::Config& config, unsigned int dimension, unsigned int size) {
    std::vector<T> data(getElements(dimension, size));

    if (dimension == 1) {
      config.load1DArray(data, size, jptr);
    } else if (dimension == 2) {
      std::array<unsigned int, 2> sizes{size, size};
      config.load2DArray(data, sizes, jptr);
    } else if (dimension == 3) {
      std::array<unsigned int, 3> sizes{size, size, size};
      config.load3DArray(data, sizes, jptr);
    }

    return data;
  }

  static unsigned int getElements(unsigned int dimension, unsigned int size) {
    unsigned int elements = size;
    for (unsigned int i = 1; i < dimension; i++) {
      elements *= size;
    }
    return elements;
  }

  static unsigned int findDimension(const config::json::json_pointer& jptr, const config::Config& config) {
    if (!config.isArray(jptr / 0)) return 1;
    if (!config.isArray(jptr / 0 / 0)) return 2;
    if (!config.isArray(jptr / 0 / 0 / 0)) return 3;

    throw std::invalid_argument(fmt::format("{} has too much dimensions.", jptr));
  }
};

}  // namespace nastja
