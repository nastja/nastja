/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/config/config.h"
#include "lib/storage/paramstorageinterface.h"
#include <stdexcept>
#include <typeinfo>

namespace nastja {

template <typename T>
class ParameterStorageConst : public IParameterStorage<T> {
public:
  explicit ParameterStorageConst(const config::json::json_pointer& jptr, const config::Config& config) : IParameterStorage<T>{} {
    auto storage = config.getValue<std::string>(jptr / "storage", "");
    if (storage != "const") {
      throw std::invalid_argument(fmt::format("ParameterStorageConst needs '{} = const'.", jptr / "storage"));
    }

    data_ = config.getValue<T>(jptr / +"value");
  }

  explicit ParameterStorageConst(const std::string& key, const config::Config& config)
      : ParameterStorageConst<T>(config::getJsonPointer(key), config) {}

  explicit ParameterStorageConst(const T value) : IParameterStorage<T>() {
    data_ = value;
  }

  T getValue(const unsigned int /*a*/) const override { return data_; }
  T getValue(const unsigned int /*a*/, const unsigned int /*b*/) const override { return data_; }
  T getValue(const unsigned int /*a*/, const unsigned int /*b*/, const unsigned int /*c*/) const override { return data_; }

  void setValue(unsigned int /*a*/, T /*val*/) override {}
  void setValue(unsigned int /*a*/, unsigned int /*b*/, T /*val*/) override {}
  void setValue(unsigned int /*a*/, unsigned int /*b*/, unsigned int /*c*/, T /*val*/) override {}

  unsigned int getDimension() const override { return dimension_; }
  unsigned int getSize() const override { return size_; }

  void normalizeRows() override { throw std::runtime_error("Normalize is not supported for const ParameterStorages."); }

private:
  T data_;                     ///< The constant data element.
  unsigned int dimension_{0};  ///< The dimension.
  unsigned int size_{1};       ///< The size of the data elements.
};

}  // namespace nastja
