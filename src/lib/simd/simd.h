/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once


#define NASTJA_SIMD_IS_INCLUDED

#ifdef __AVX__
#include <immintrin.h>
#include "lib/simd/avx.h"
#else
#include "lib/simd/scalar.h"
#endif

namespace nastja {
namespace simd {

template <class T>
struct packed { using type = T; };

template <>
struct packed<float> { using type = float32x8_t; };
template <>
struct packed<double> { using type = float64x4_t; };

template <class T>
struct ipacked { using type = T; };

template <>
struct ipacked<float> { using type = int32x8_t; };
template <>
struct ipacked<double> { using type = int64x4_t; };

}  // namespace simd

using preal_t  = simd::packed<real_t>::type;   ///< packed real type
using pireal_t = simd::ipacked<real_t>::type;  ///< packed corresponding int type

}  // namespace nastja
