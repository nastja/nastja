/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#ifndef NASTJA_SIMD_IS_INCLUDED
#error This file is not for direct inclusion, use simd.h instead.
#endif

#include <immintrin.h>

namespace nastja {
namespace simd {

using float64x4_t = __m256d;
using float32x8_t = __m256;
using int64x4_t   = __m256i;
using int32x8_t   = __m256i;

inline const char* getInstructionSetName() { return "AVX/AVX2"; }

// should be the same for __m256 and __m256d
constexpr std::size_t getInstructionSetRegisterSize() { return sizeof(__m256d); }
constexpr bool getInstructionSetNeedsAlignment() { return true; }

inline float64x4_t zero_pd() { return _mm256_setzero_pd(); }
inline float32x8_t zero_ps() { return _mm256_setzero_ps(); }

inline float64x4_t load(double const* mem_addr) { return _mm256_load_pd(mem_addr); }
inline float32x8_t load(float const* mem_addr) { return _mm256_load_ps(mem_addr); }

inline float64x4_t load_unaligned(double const* mem_addr) { return _mm256_loadu_pd(mem_addr); }
inline float32x8_t load_unaligned(float const* mem_addr) { return _mm256_loadu_ps(mem_addr); }

inline float64x4_t load_gather(double const* base_addr, int64x4_t vindex) { return _mm256_i64gather_pd(base_addr, vindex, 8); }
inline float32x8_t load_gather(float const* base_addr, int32x8_t vindex) { return _mm256_i32gather_ps(base_addr, vindex, 4); }

inline void store(double* mem_addr, float64x4_t a) { _mm256_store_pd(mem_addr, a); }
inline void store(float* mem_addr, float32x8_t a) { _mm256_store_ps(mem_addr, a); }

inline void store_unaligned(double* mem_addr, float64x4_t a) { _mm256_storeu_pd(mem_addr, a); }
inline void store_unaligned(float* mem_addr, float32x8_t a) { _mm256_storeu_ps(mem_addr, a); }

inline float64x4_t set(double e0) { return _mm256_set_pd(e0, e0, e0, e0); }
inline float32x8_t set(float e0) { return _mm256_set_ps(e0, e0, e0, e0, e0, e0, e0, e0); }

inline float64x4_t set(double e3, double e2, double e1, double e0) { return _mm256_set_pd(e3, e2, e1, e0); }
inline float32x8_t set(float e7, float e6, float e5, float e4, float e3, float e2, float e1, float e0) { return _mm256_set_ps(e7, e6, e5, e4, e3, e2, e1, e0); }
inline int64x4_t set(long e3, long e2, long e1, long e0) { return _mm256_set_epi64x(e3, e2, e1, e0); }
inline int32x8_t set(int e7, int e6, int e5, int e4, int e3, int e2, int e1, int e0) { return _mm256_set_epi32(e7, e6, e5, e4, e3, e2, e1, e0); }

inline float64x4_t set_pd(double e0) { return _mm256_set_pd(e0, e0, e0, e0); }
inline float32x8_t set_ps(float e0) { return _mm256_set_ps(e0, e0, e0, e0, e0, e0, e0, e0); }

inline float64x4_t set_pd(double e3, double e2, double e1, double e0) { return _mm256_set_pd(e3, e2, e1, e0); }
inline float32x8_t set_ps(float e7, float e6, float e5, float e4, float e3, float e2, float e1, float e0) { return _mm256_set_ps(e7, e6, e5, e4, e3, e2, e1, e0); }

}  // namespace simd
}  // namespace nastja
