/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#ifndef NASTJA_SIMD_IS_INCLUDED
#error This file is not for direct inclusion, use simd.h instead.
#endif

#include "lib/math/vector.h"

namespace nastja {
namespace simd {

using float64x4_t = math::Vector<double, 4>;
using float32x8_t = math::Vector<float, 8>;
using int64x4_t   = math::Vector<long, 4>;
using int32x8_t   = math::Vector<int, 8>;

inline const char* getInstructionSetName() { return "Scalar"; }

constexpr std::size_t getInstructionSetRegisterSize() { return sizeof(float64x4_t); }
constexpr bool getInstructionSetNeedsAlignment() { return false; }

inline float64x4_t zero_pd() { return float64x4_t{}; }
inline float32x8_t zero_ps() { return float32x8_t{}; }

inline float64x4_t load_unaligned(double const* mem_addr) { return float64x4_t{mem_addr[0], mem_addr[1], mem_addr[2], mem_addr[3]}; }
inline float32x8_t load_unaligned(float const* mem_addr) { return float32x8_t{mem_addr[0], mem_addr[1], mem_addr[2], mem_addr[3], mem_addr[4], mem_addr[5], mem_addr[6], mem_addr[7]}; }

inline float64x4_t load(double const* mem_addr) { return load_unaligned(mem_addr); }
inline float32x8_t load(float const* mem_addr) { return load_unaligned(mem_addr); }

inline float64x4_t load_gather(double const* base_addr, int64x4_t vindex) {
  float64x4_t r{};
  for (std::size_t i = 0; i < 4; i++) {
    r[i] = *(base_addr + vindex[i]);
  }
  return r;
}

inline float32x8_t load_gather(float const* base_addr, int32x8_t vindex) {
  float32x8_t r{};
  for (std::size_t i = 0; i < 8; i++) {
    r[i] = *(base_addr + vindex[i]);
  }
  return r;
}

inline void store_unaligned(double* mem_addr, float64x4_t a) {
  for (unsigned int i = 0; i < 4; i++) {
    mem_addr[i] = a[i];
  }
}

inline void store_unaligned(float* mem_addr, float32x8_t a) {
  for (unsigned int i = 0; i < 8; i++) {
    mem_addr[i] = a[i];
  }
}

inline void store(double* mem_addr, float64x4_t a) { store_unaligned(mem_addr, a); }
inline void store(float* mem_addr, float32x8_t a) { store_unaligned(mem_addr, a); }

inline float64x4_t set(double e0) { return float64x4_t{e0, e0, e0, e0}; }
inline float32x8_t set(float e0) { return float32x8_t{e0, e0, e0, e0, e0, e0, e0, e0}; }

inline float64x4_t set(double e3, double e2, double e1, double e0) { return float64x4_t{e0, e1, e2, e3}; }
inline float32x8_t set(float e7, float e6, float e5, float e4, float e3, float e2, float e1, float e0) { return float32x8_t{e0, e1, e2, e3, e4, e5, e6, e7}; }
inline int64x4_t set(long e3, long e2, long e1, long e0) { return int64x4_t{e0, e1, e2, e3}; }
inline int32x8_t set(int e7, int e6, int e5, int e4, int e3, int e2, int e1, int e0) { return int32x8_t{e0, e1, e2, e3, e4, e5, e6, e7}; }

inline float64x4_t set_pd(double e0) { return float64x4_t{e0, e0, e0, e0}; }
inline float32x8_t set_ps(float e0) { return float32x8_t{e0, e0, e0, e0, e0, e0, e0, e0}; }

inline float64x4_t set_pd(double e3, double e2, double e1, double e0) { return float64x4_t{e0, e1, e2, e3}; }
inline float32x8_t set_ps(float e7, float e6, float e5, float e4, float e3, float e2, float e1, float e0) { return float32x8_t{e0, e1, e2, e3, e4, e5, e6, e7}; }

}  // namespace simd
}  // namespace nastja
