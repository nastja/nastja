/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/simdata/simdata.h"
#include <cxxabi.h>
#include <map>
#include <memory>
#include <typeinfo>

namespace nastja {

// SFINAE test
template <typename T>
auto maybe_setName(T* v, const std::string& n) -> decltype(v->setName(n), void()) {
  return v->setName(n);
}

template <typename T>
void maybe_setName(T& /*v*/, const std::string& /*n*/) {}

template <typename T>
auto maybe_registerSimData(T* v, SimData* const simdata) -> decltype(v->registerSimData(simdata), void()) {
  return v->registerSimData(simdata);
}

template <typename T>
void maybe_registerSimData(T& /*v*/, SimData* const /*simdata*/) {}

template <typename T>
auto maybe_setupHook(T* v) -> decltype(v->setupHook()) {
  return v->setupHook();
}

template <typename T>
void maybe_setupHook(T& /*v*/) {}

template <typename T>
class Pool {
public:
  explicit Pool() = default;
  explicit Pool(SimData& simdata) : simdata_{&simdata} {}
  ~Pool()               = default;
  Pool(const Pool&)     = delete;
  Pool(Pool&&) noexcept = default;
  Pool& operator=(const Pool&) = delete;
  Pool& operator=(Pool&&) = delete;

  /**
   * Register element in pool.
   *
   * @param name    The name of the element.
   * @param element Pointer to the element which is from pool type or derived from it.
   */
  template <typename TDerived>
  void registerElement(const std::string& name, std::unique_ptr<TDerived>&& element) {
    if (poolSet.find(name) != poolSet.end()) {
      throw std::invalid_argument(fmt::format("An element with name '{}' is already registered.", name));
    }

    auto* element_ptr = element.get();
    maybe_setName(element_ptr, name);
    if (simdata_ != nullptr) {
      maybe_registerSimData(element_ptr, simdata_);
    }
    maybe_setupHook(element_ptr);
    poolSet[name] = std::move(element);
  }

  /**
   * Emplace element in pool for base type objects.
   * Derived objects have to use registerElement().
   *
   * @param name The name of the element.
   * @param args Forward this args to the constructor.
   */
  template <typename... Targs>
  void registerEmplace(const std::string& name, Targs&&... args) {
    registerElement(name, std::make_unique<T>(std::forward<Targs>(args)...));
  }

  T* getElement(const std::string& name) const {
    auto it = poolSet.find(name);

    if (it == poolSet.end()) {
      throw std::out_of_range(fmt::format("{} '{}' can not be found in the {}-Pool.",
                                          abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, nullptr), name, abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, nullptr)));
    }
    return it->second.get();
  }

  bool hasElement(const std::string& name) const {
    auto it = poolSet.find(name);

    return !(it == poolSet.end());
  }

  auto begin() const { return poolSet.begin(); }
  auto begin() { return poolSet.begin(); }
  auto end() const { return poolSet.end(); }
  auto end() { return poolSet.end(); }

  std::map<std::string, std::unique_ptr<T>> poolSet;  ///< The map of pointers.

private:
  SimData* simdata_{nullptr};  ///< Pointer to the SimData.
};

}  // namespace nastja
