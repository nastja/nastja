/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <type_traits>

namespace nastja {
namespace details {

// from https://gist.github.com/Jiwan/7a586c739a30dd90d259
template <typename UnnamedType>
struct container {
public:
  // A public operator() that accept the argument we wish to test onto the UnnamedType.
  // Notice that the return type is automatic!
  template <typename Param>
  constexpr auto operator()(const Param& /* unused */) {
    // The argument is forwarded to one of the two overloads.
    // The SFINAE on the 'true_type' will come into play to dispatch.
    return test_validity<Param>(int());
  }

  // Let's put the test in private.
private:
  // We use std::declval to 'recreate' an object of 'UnnamedType'.
  // We use std::declval to also 'recreate' an object of type 'Param'.
  // We can use both of these recreated objects to test the validity!
  template <typename Param>
  constexpr auto test_validity(int /* unused */)
      -> decltype(std::declval<UnnamedType>()(std::declval<Param>()), std::true_type()) {
    // If substitution didn't fail, we can return a true_type.
    return std::true_type();
  }

  template <typename Param>
  constexpr std::false_type test_validity(...) {
    // Our sink-hole returns a false_type.
    return std::false_type();
  }
};

}  // namespace detail

/**
 * Determines whether the specified function is valid for SFINAE.
 *
 * @return std::true_type if the specified function is valid, std::false_type otherwise.
 */
template <typename UnnamedType>
constexpr auto isValid(const UnnamedType& /* unused */) {
  return details::container<UnnamedType>();
}

/**
 * Converts an enum value to its underlying type.
 *
 * @param e  The enum value.
 *
 * @tparam T  The enum class.
 *
 * @return The underlying type value.
 */
template <typename T>
constexpr auto toUnderlying(T e) noexcept {
  return static_cast<std::underlying_type_t<T>>(e);
}

}  // namespace nastja
