/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "centerofmass.h"
#include "lib/field/field.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C8asym.h"
#include "lib/stencil/direction.h"
#include "lib/storage/sparestorage.h"
#include <array>

namespace nastja {

using namespace config::literals;
using namespace stencil;

void CenterOfMass::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  /// @key{CellsInSilico.centerofmass.steps, int, 1}
  /// Number of each n-th steps when the centers are calculated.
  eachNthStep = getSimData().getConfig().getValue<int>("CellsInSilico.centerofmass.steps"_jptr, 1);
}

/**
 * Integrate the mass positions.
 *
 * @param massPositions  The mass positions.
 * @param block          The block.
 */
void CenterOfMass::calcMassPositions(SpareStorageArray<long, 4>& massPositions, block::Block* block) {
  auto& field = block->getFieldByName(getFieldname())->get<cellid_t>();
  auto& data  = getSimData().getData<DataCells>();

  auto view = field.template createView<1>();
  for (auto cell = view.begin(); cell != view.end(); ++cell) {
    cellid_t cellID = **cell;
    if (cellID <= data.liquid) continue;

    long x = cell.coordinates().x();
    long y = cell.coordinates().y();
    long z = cell.coordinates().z();
    massPositions[cellID] += {x, y, z, 1};
  }
}

/**
 * Calculates the mass positions split for lower and upper block part. The wights are also stored per axis.
 *
 * @param lowerUpperPositions  The lower and upper positions.
 * @param block                The block.
 */
void CenterOfMass::calcLowerUpperMassPositions(SpareStorage<std::pair<std::array<long, 6>, std::array<long, 6>>>& lowerUpperPositions, block::Block* block) {
  auto& field      = block->getFieldByName(getFieldname())->get<cellid_t>();
  const auto& data = getSimData().getData<DataCells>();

  for (auto d : D3C8asym()) {
    auto view = field.template createView<1>(field.getInnerBoundingBoxPart(d));
    for (auto cell = view.begin(); cell != view.end(); ++cell) {
      cellid_t cellID = **cell;
      if (cellID <= data.liquid) continue;

      std::array<long, 6> lower{};
      std::array<long, 6> upper{};
      for (int axis = 0; axis < 3; axis++) {
        if (c(axis, D3C8asym::dir[d]) == 0) {  // lower half
          lower[axis]     = cell.coordinates()[axis];
          lower[axis + 3] = 1;
        } else {
          upper[axis]     = cell.coordinates()[axis];
          upper[axis + 3] = 1;
        }
      }

      auto& position = lowerUpperPositions[cellID];
      position.first += lower;
      position.second += upper;
    }
  }
}

/**
 * Merges lower and upper position to a periodic center
 *
 * @param lowerUpper  The lower and upper position.
 * @param block       The block.
 *
 * @return The merged point and weight
 */
std::array<long, 4> CenterOfMass::mergeCenter(const std::pair<std::array<long, 6>, std::array<long, 6>>& lowerUpper, block::Block* block) {
  std::array<long, 4> point{};
  const auto& lower = lowerUpper.first;
  const auto& upper = lowerUpper.second;

  auto& field             = block->getFieldByName(getFieldname())->get<cellid_t>();
  const auto boundarySize = field.getBoundarySize();
  auto size               = block->size * block->blocks;

  point[3] = upper[3] + lower[3];
  for (int axis = 0; axis < 3; axis++) {
    point[axis] = upper[axis] + lower[axis];

    if (block->blocks[axis] > 1 || lower[axis + 3] == 0 || upper[axis + 3] == 0) continue;  // just add lower + upper for more than one block or empty axis

    auto lowerCenter = math::divRoundClosest(lower[axis], lower[axis + 3]) - boundarySize[axis];  // The local center of the lower parts.
    auto upperCenter = math::divRoundClosest(upper[axis], upper[axis + 3]) - boundarySize[axis];  // The local center of the upper parts.

    if (upperCenter - lowerCenter > lowerCenter + size[axis] - upperCenter) {  // periodic distance is smaller, move lower part to periodic
      point[axis] += size[axis] * lower[axis + 3];
    }

    if (point[axis] > static_cast<long>((size[axis] + boundarySize[axis]) * point[3])) {  // center is out of block, so move it back to the block
      point[axis] -= size[axis] * point[3];
    }
  }

  return point;
}

/**
 * Calculates the center of mass considering the periodicity within one block.
 *
 * @param massPositions  The mass positions.
 * @param block          The block.
 */
void CenterOfMass::periodicCenter(SpareStorageArray<long, 4>& massPositions, block::Block* block) {
  SpareStorage<std::pair<std::array<long, 6>, std::array<long, 6>>> lowerUpperPositions;

  calcLowerUpperMassPositions(lowerUpperPositions, block);

  for (const auto& cell : lowerUpperPositions) {
    auto cellID       = cell.first;

    auto point = mergeCenter(cell.second, block);
    massPositions[cellID] += point;
  }
}

/**
 * Detect active cells in the whole block
 *
 * @param block Pointer to the block.
 */
void CenterOfMass::executeBlock(block::Block* block) {
  auto& field     = block->getFieldByName(getFieldname())->get<cellid_t>();
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& center    = container.centerOfMass;

  center.clear();

  if (block->blocks[0] == 1 || block->blocks[1] == 1 || block->blocks[2] == 1) {
    periodicCenter(center, block);
  } else {
    calcMassPositions(center, block);
  }

  const auto offset = static_cast<math::Vector3<long>>(block->offset) - static_cast<math::Vector3<long>>(field.getBoundarySize());
  // add block offset minus boundarySize
  for (auto& a : center) {
    a.second[0] += a.second[3] * offset[0];
    a.second[1] += a.second[3] * offset[1];
    a.second[2] += a.second[3] * offset[2];
  }
}

void CenterOfMass::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
