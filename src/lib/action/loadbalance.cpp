/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @class LoadBalance
 *
 */

#include "loadbalance.h"
#include "lib/archive.h"
#include "lib/logger.h"
#include "lib/block/block.h"
#include "lib/communication/message.h"
#include <climits>
#include <memory>

namespace nastja {

using namespace block;

/**
 * Initialized additional DynamicBlockContainer object to the given Block.
 *
 * @param block Pointer to block.
 */
void LoadBalance::init(Block* block, const Pool<field::FieldProperties>& /*fieldpool*/) {
  block->registerDataContainer<LoadBalanceContainer>();
}

void LoadBalance::init(const BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
}

/**
 * Register all blocks at the communicators.
 */
void LoadBalance::setup(const BlockRegister& /*blockRegister*/) {
  dynamicBlock_ = static_cast<DynamicBlock*>(blockmanager_.getAction("DynBlock"));
}

/**
 * Create set of tuple with min blocks, then maximum offer and rank from connected comms.
 *
 * @return set of tuple3.
 */
std::set<tuple3, Tuple3comp> LoadBalance::getOrderedSetOfTuple() const {
  std::set<tuple3, Tuple3comp> tupleSet;

  auto& commManager = dynamicBlock_->commManager_;

  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    int numOfBlocks   = comm.second->getNumOfBlocks();
    int offeredBlocks = comm.second->getOfferedSpareBlocks();
    int partnerRank   = comm.second->getPartnerRank();

    if (offeredBlocks == 0) continue;

    tupleSet.insert(std::make_tuple(numOfBlocks, offeredBlocks, partnerRank));
  }

  return tupleSet;
}

/**
 * Calculate a score for the given block.
 *
 * @param block       The block to score.
 * @param partnerRank Rank for the base to score.
 *
 * @return Score by number of direct neighbors + fraction of all neighbors.
 */
double LoadBalance::calcBlockScore(const Block* block, const int partnerRank) {
  return block->fractionNeighbors(partnerRank) + block->countNeighbors(partnerRank);
}

/**
 * Create a vector of blocks that have to send to the partner.
 *
 * @param numOfBlocks Number of blocks to distribute.
 * @param partnerRank Rank of the partner that get the blocks.
 */
std::vector<Block*> LoadBalance::getMovingBlocks(int numOfBlocks, const int partnerRank) const {
  std::vector<Block*> blocks;

  std::set<std::pair<double, Block*>, PairDecComp> scoredBlocks;
  for (const auto& block : blockmanager_.getBlockSet()) {
    auto& dataDynBlock    = block.second->getDataContainer<DynamicBlockContainer>();
    auto& dataLoadBalance = block.second->getDataContainer<LoadBalanceContainer>();
    if (dataLoadBalance.target != -1 || dataDynBlock.ttl > 0) continue;

    double score = calcBlockScore(block.second, partnerRank);
    scoredBlocks.insert(std::make_pair(score, block.second));
  }

  for (const auto& block : scoredBlocks) {
    blocks.push_back(block.second);
    auto& dataLoadBalance  = block.second->getDataContainer<LoadBalanceContainer>();
    dataLoadBalance.target = partnerRank;
    numOfBlocks--;
    if (numOfBlocks == 0) break;
  }

  return blocks;
}

/**
 * Create a map with a vector of blocks that has to send to the partner.
 *
 * @return map with a vector of blocks pointer
 */
std::map<int, std::vector<Block*>> LoadBalance::getMovingBlocks() const {
  std::map<int, std::vector<Block*>> blocks;

  auto& commManager = dynamicBlock_->commManager_;

  int groupAverage = commManager.calcGroupAverage();

  int numOfMovingBlocks = blockmanager_.getBlockSet().size() - groupAverage;

  if (numOfMovingBlocks <= 0) return blocks;

  auto tupleSet = getOrderedSetOfTuple();

  for (const auto& t : tupleSet) {
    int offeredBlocks = std::get<1>(t);
    int partnerRank   = std::get<2>(t);

    // Calculate the number of blocks shifted to this partner.
    int cnt = offeredBlocks;

    if (cnt > numOfMovingBlocks) cnt = numOfMovingBlocks;
    numOfMovingBlocks -= offeredBlocks;

    if (cnt > 0) {
      blocks[partnerRank] = getMovingBlocks(cnt, partnerRank);
      if (blocks[partnerRank].empty()) blocks.erase(partnerRank);
    }

    if (numOfMovingBlocks <= 0) break;
  }

  return blocks;
}

/**
 * Create a serialized vector of IDs of all moving blocks.
 * numOfTargets(target, numOfBlocks(ID, ...), ...) the number numOfTargets and numOfBlocks gives the number of elements.
 *
 * @param blocks map of vector of pointer to the moving blocks.
 *
 * @return vector of ints with all block IDs per target.
 */
std::vector<int> LoadBalance::serializeMovingIDs(const std::map<int, std::vector<Block*>>& blocks) {
  std::vector<int> ret;

  ret.push_back(blocks.size());
  for (const auto& m : blocks) {
    ret.push_back(m.first);
    ret.push_back(m.second.size());
    for (const auto& block : m.second) {
      ret.push_back(block->id);
    }
  }

  return ret;
}

/**
 * Find all communications for moving blocks.
 *
 * @param blocks map with vector of moving blocks.
 */
void LoadBalance::findCommunications(const std::map<int, std::vector<Block*>>& blocks) const {
  auto& commManager = dynamicBlock_->commManager_;
  auto& comms       = commManager.getFastComms();

  for (const auto& targetRank : blocks) {
    // get all communication partner for this rank for all blocks
    std::set<int> setRanks;
    for (const auto& block : targetRank.second) {
      auto tmp = block->getRanks();
      setRanks.insert(tmp.begin(), tmp.end());
    }
    setRanks.erase(targetRank.first);  // The communication to me is not disabled.

    // Append pairs if communicator is connected
    for (const auto& commID : setRanks) {
      if (commManager.isConnected(commID)) {
        comms[targetRank.first].insert(commID);
        comms[commID].insert(targetRank.first);
      }
    }
  }
}

/**
 * Prepare message with moved blocks.
 *
 * @param blocks map of vector of moving blocks
 */
void LoadBalance::prepareMessageMove(const std::map<int, std::vector<Block*>>& blocks) const {
  auto& commManager = dynamicBlock_->commManager_;

  auto& movingIDs = commManager.getMoveBlocks();
  movingIDs       = serializeMovingIDs(blocks);
  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    auto& messages = comm.second->sendBuffer;

    messages.clear();
    Message::append(messages, MessageID::moveBlocks, movingIDs);
  }
}

/**
 * Updates the knownID and block neighborhood.
 * If the target is the local process then additionally announce the block.
 *
 * @param blockID  ID of the moved block.
 * @param sourceID ID of the source communicator.
 * @param targetID ID of the target communicator.
 */
void LoadBalance::updateMovedBlock(const int blockID, const int sourceID, const int targetID) const {
  auto& commManager = dynamicBlock_->commManager_;

  commManager.commSet.at(sourceID)->removeBlock(blockID);

  auto target = commManager.commSet.find(targetID);
  if (target == commManager.commSet.end()) return;
  target->second->appendIncomingBlock(blockID);  // updateBlocks will replace the current rank.

  if (targetID == blockmanager_.getMPIRank()) {
    auto& sourceKnownComms = commManager.commSet.at(sourceID)->getKnownComms();
    for (const auto& comm : commManager.commSet) {
      if (!comm.second->isConnected()) continue;

      // if this comm is not in the knownComms from the source announce the block
      if (sourceKnownComms.count(comm.first) == 0) {
        comm.second->appendAnnounceBlock(blockID);
      }
    }
  }
}

/**
 * Read the messages and create a a map of source with a map of target with a set of IDs.
 *
 * @return map of source with a map of target with a set of IDs.
 */
std::map<int, std::map<int, std::set<int>>> LoadBalance::getMovedBlockStruct() const {
  std::map<int, std::map<int, std::set<int>>> moveBlocks;

  auto& commManager = dynamicBlock_->commManager_;

  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    auto& message = comm.second->recvBuffer;

    int start;
    int end;
    std::tie(start, end) = Message::findMessage(message, MessageID::moveBlocks);
    if (start == -1) continue;

    int pos       = start;
    int numOfComm = message[pos++];

    for (int c = 0; c < numOfComm; c++) {
      int commTargetID = message[pos++];
      int numOfBlocks  = message[pos++];
      for (int b = 0; b < numOfBlocks; b++) {
        int blockID = message[pos++];

        moveBlocks[comm.first][commTargetID].insert(blockID);
      }
    }
  }

  return moveBlocks;
}

/**
 * Update knownIDs and Blocks with each moved Block.
 *
 * @param moveBlocks Data of moved blocks.
 */
void LoadBalance::updateMovedBlocks(std::map<int, std::map<int, std::set<int>>>& moveBlocks) const {
  for (const auto& source : moveBlocks) {
    for (const auto& target : source.second) {
      for (const auto& blockID : target.second) {
        updateMovedBlock(blockID, source.first, target.first);
      }
    }
  }
}

/**
 * Search for unconnected targets and check if a block is in the neighborhood of a new block.
 *
 * @param moveBlocks  Data of moved blocks.
 * @param newBlockIDs ID of the new blocks.
 *
 * @return set of missing communicators.
 */
std::set<int> LoadBalance::getMissingCommunicators(std::map<int, std::map<int, std::set<int>>>& moveBlocks, const std::set<int>& newBlockIDs) const {
  auto& commManager = dynamicBlock_->commManager_;

  std::set<int> comms;

  for (const auto& source : moveBlocks) {
    for (const auto& target : source.second) {
      if (commManager.isConnected(target.first)) continue;
      for (const auto& blockID : target.second) {
        if (blockmanager_.isInNeighborhood(blockID, newBlockIDs)) {
          comms.insert(target.first);
        }
      }
    }
  }

  return comms;
}

/**
 * Prepare message with fast connection setup FCS.
 * All information of potential deleted connections and new connections.
 *
 * @param blocks map of vector of moving blocks
 */
void LoadBalance::prepareMessageFCS(const std::map<int, std::vector<Block*>>& blocks) const {
  auto& commManager = dynamicBlock_->commManager_;
  auto& fastComms   = commManager.getFastComms();

  findCommunications(blocks);

  for (const auto& comm : commManager.commSet) {
    std::vector<int>& messages = comm.second->sendBuffer;

    if (!comm.second->isConnected()) continue;

    Message::append(messages, MessageID::newComm, fastComms);
  }
}

/**
 * Remove all moved blocks from the blockmanager_.
 *
 * @param blocks map of vector of moving blocks.
 *
 * @return used Ranks.
 */
std::set<int> LoadBalance::removeBlocks(const std::map<int, std::vector<Block*>>& blocks) const {
  std::set<int> delBlocks;

  for (const auto& m : blocks) {
    for (const auto& block : m.second) {
      delBlocks.insert(block->id);
    }
  }

  auto usedRanks = blockmanager_.deleteBlocks(delBlocks);

  return usedRanks;
}

/**
 * Process blocks in announceBlocks for each connected communicator.
 * Append the announcements of blocks and comms to the message and update neighbors with this blocks.
 */
void LoadBalance::processAnnounceBlocks() const {
  auto& commManager = dynamicBlock_->commManager_;

  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    comm.second->appendMsgAnnouncements();
    comm.second->updateNeighbors(commManager);
  }
}

/**
 * Create moved blocks and unpack the data.
 *
 * @return set of new blocks
 */
std::set<int> LoadBalance::createBlocks() {
  auto& commManager = dynamicBlock_->commManager_;

  std::set<int> blockIDs;

  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    auto& ia = comm.second->getRecvArchive();
    if (ia.size() == 0) continue;
    int numOfMovingBlocks;
    ia.unpack(numOfMovingBlocks);
    for (int b = 0; b < numOfMovingBlocks; b++) {
      unsigned int blockID;

      ia.unpack(blockID);
      blockIDs.insert(blockID);

      logger::get().debug("Received block: {}", blockID);
      Block& block = *blockmanager_.createNewBlock(blockID, commManager);
      ia.unpack(block);
    }
  }

  return blockIDs;
}

/**
 * Send blocks and moving messages.
 *
 * @param blocks map of vector of moving blocks
 */
void LoadBalance::sendBlocks(const std::map<int, std::vector<Block*>>& blocks) const {
  auto& commManager = dynamicBlock_->commManager_;

  commManager.waitCommAllBufferSend();
  for (const auto& comm : commManager.commSet) {
    if (!comm.second->isConnected()) continue;

    auto& oa = comm.second->getSendArchive();
    oa.clear();
    oa.pack(comm.second->sendBuffer);

    const auto& m = blocks.find(comm.first);

    if (m == blocks.end()) {
      int numOfMovingBlocks = 0;
      oa.pack(numOfMovingBlocks);
    } else {
      int numOfMovingBlocks = m->second.size();
      oa.pack(numOfMovingBlocks);
      for (const auto& block : m->second) {
        logger::get().debug("Send block: {}", block->id);
        oa.pack(*block);
      }
    }
    comm.second->sendingBuffer();
  }
}

/**
 * Execute the LoadBalance action.
 */
void LoadBalance::execute(const BlockRegister& /*blockRegister*/) {
  auto& commManager = dynamicBlock_->commManager_;

  auto blocks = getMovingBlocks();

  prepareMessageMove(blocks);
  prepareMessageFCS(blocks);
  processAnnounceBlocks();

  sendBlocks(blocks);

#ifndef NDEBUG
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  removeBlocks(blocks);

  // begin receive side
  commManager.recvBuffer();

#ifndef NDEBUG
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  commManager.updateBlocks();
  commManager.updateNeighbors();

  auto moveBlocks = getMovedBlockStruct();
  updateMovedBlocks(moveBlocks);  // TODO also for missingComms

  // auto unconnectedBlocks = getUnconnectedBlocks(moveBlocks);

  auto newBlockIDs = createBlocks();

  auto missingComms = getMissingCommunicators(moveBlocks, newBlockIDs);
  commManager.connectMissing(missingComms);
  commManager.updateBlocks();
  commManager.updateNeighbors();

  commManager.cleanUnusedComms();

  // clear buffered move blocks
  commManager.clearBufferedMessages();

  // for the next dynblock sending process
  commManager.calcOfferedSpareBlocks();

  blockmanager_.dbgPrintBlockCommInfo();

  commManager.logCommunicators();
}

}  // namespace nastja
