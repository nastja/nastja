/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/pool.h"
#include "lib/block/block.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/simdata.h"
#include <cassert>

namespace nastja {

/**
 * @defgroup actions Action
 * Provides actions that act on blocks.
 *
 * Actions are registered in a list and executed in each time step. Included are the calculation of fields (sweeps),
 * I/O, boundary exchange or other utility functions.
 */

struct ActionTypeEnum {
  enum ActionType { general = 0,
                    io,
                    iodump };
};
using ActionType = ActionTypeEnum::ActionType;

class Action {
public:
  explicit Action()         = default;
  virtual ~Action()         = default;
  Action(const Action&)     = delete;
  Action(Action&&) noexcept = default;
  Action& operator=(const Action&) = delete;
  Action& operator=(Action&&) = delete;

  virtual void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) = 0;
  virtual void init(block::Block* /*block*/, const Pool<field::FieldProperties>& /*fieldpool*/) {}
  virtual void setup(const block::BlockRegister& blockRegister)   = 0;
  virtual void executeBlock(block::Block* block)                  = 0;
  virtual void execute(const block::BlockRegister& blockRegister) = 0;
  virtual bool skip() const                                       = 0;

  virtual void dump(Archive& /*ar*/) const {}
  virtual void undump(Archive& /*ar*/) {}

  const std::string& getName() const { return name_; }
  void setName(const std::string& name) { name_ = name; }

  void setType(ActionType type) { actionType_ = type; }
  ActionType getType() const { return actionType_; }

  void registerSimData(SimData* const simdata) { simdata_ = simdata; }
  SimData& getSimData() const {
    assert(simdata_ != nullptr);
    return *simdata_;
  }

  void registerFieldname(const std::string& fieldname) { fieldnames_.push_back(fieldname); }
  const std::string& getFieldname(const int i = 0) const { return fieldnames_[i]; };
  const std::vector<std::string>& getFieldnames() const { return fieldnames_; };

  void setOffset(unsigned int offset) { this->offset = offset; }

  bool isRegisterd{false};  ///< true, when the action is registerd to the actionlist.

protected:
  unsigned int offset{0};       ///< The offset to the time-step.
  unsigned int eachNthStep{1};  ///< Execute this action every n-th step, else skip it.

private:
  std::string name_;                            ///< The name of the action.
  std::vector<std::string> fieldnames_;         ///< The vector of the fieldnames this action is acting on.
  SimData* simdata_{nullptr};                   ///< Pointer to the SimData object.
  ActionType actionType_{ActionType::general};  ///< The type of this action.
};

}  // namespace nastja
