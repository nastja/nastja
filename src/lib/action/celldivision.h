/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/evaluate/cellextension.h"
#include "lib/evaluate/expressionstorage.h"
#include "lib/evaluate/extension.h"
#include "lib/math/random.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/sparestorage.h"
#include <random>

namespace nastja {

/**
 * Class for Cell divisions
 *
 * does different things:
 * 1. determines whether a cell divides
 * 2. in the next timestep it divides the cell into two daughter cells and re-initializes them
 */
template <unsigned int DIM = 3>
class CellDivision : public Action {
public:
  explicit CellDivision(const std::string& fieldname)
      : Action{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
    registerFieldname(fieldname);
  }

  ~CellDivision() override              = default;
  CellDivision(const CellDivision&)     = delete;
  CellDivision(CellDivision&&) noexcept = default;
  CellDivision& operator=(const CellDivision&) = delete;
  CellDivision& operator=(CellDivision&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  enum class DivisionMode { random,          /// Plane of division is randomly oriented
                            polarityAxis,    /// Plane of division normal vector is set to the polarity axis of the cell
                            longitudinalAxis}; /// Plane of division normal vector is set perpendicular to the polarity axis of the cell                            

 
  DivisionMode determineDivisionMode(unsigned int const& index);
  void determineDivision(CellBlockContainer& container, block::Block* block);
  void divideReceivedCells(field::Field<cellid_t, 1>& field, CellBlockContainer& container, block::Block* block);
  void divideCell(field::Field<cellid_t, 1>& field, CellBlockContainer& container, cellid_t oldID, cellid_t newID, block::Block* block);
  void reinitializeCell(field::Field<cellid_t, 1>& field, CellBlockContainer& container, cellid_t oldID, cellid_t newID, int newType);
  unsigned int chooseMutatedType(unsigned int type);

  bool halveSignals{false}; ///< Determines whether signal values are halved upon celldivision

  real_t calcSurface(std::array<cellid_t, 27>& stencil, cellid_t cell);

  int tumorStemCellType_;  ///< Type of tumor stem cells (Asymetric division)

  std::unique_ptr<ParameterStorageBuilder<real_t>> mutationMatrix_{};  ///< The parameter storage for the mutation matrix. Probabilities to mutate from and to other cell types.

  std::vector<std::string> divisionModes_{};  ///< The vector containing the division modes for different types. This determines how the plane of division is oriented.
  std::vector<real_t> centerOffsetFactors_{};  ///< The vector containing the center offset factors for different types. This determines how far the plane of division is moved along the polarity axis for polarity division.

  math::UniformReal<real_t> uniform_angle_{0, M_PI};  ///< The uniform distribution for the plane that divide the cell.
  math::UniformReal<real_t> uniform_perturbation_{-1, 1};        ///< The uniform distribution of the perturbation of polarity vector for DivisionMode::longitudinalAxis
  math::UniformReal<real_t> probability_{0, 1};        ///< The uniform distribution of the probability of division in percent.

  std::unique_ptr<evaluate::ExpressionStorage<evaluate::CellExtension>> cellEvaluator_{};
};

}  // namespace nastja
