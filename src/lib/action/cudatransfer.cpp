/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cudatransfer.h"
#include "lib/logger.h"
#include "lib/field/field.h"

namespace nastja {

void CudaTransfer::executeBlock(block::Block* block) {
  auto& field = block->getFieldByName(getFieldname())->get<real_t>();
  if (method == 1) {
    field.cuCpDataHostToDevice();
    field.cuCopyHaloToExchange();
  } else if (method == 2) {
    field.cuCheckExchangeForNeighbours();
  }
}

void CudaTransfer::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
