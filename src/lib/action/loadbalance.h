/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/action/dynamicblock.h"
#include "lib/block/blockmanager.h"
#include "lib/datacontainer/loadbalancecontainer.h"

namespace nastja {

using tuple3 = std::tuple<int, int, int>;

struct Tuple3comp {
  /**
   * sort by min, max, min
   */
  bool operator()(const tuple3& lhs, const tuple3& rhs) const {
    if (std::get<0>(lhs) == std::get<0>(rhs)) {
      if (std::get<1>(lhs) == std::get<1>(rhs)) {
        return std::get<2>(lhs) < std::get<2>(rhs);
      }
      return std::get<1>(lhs) > std::get<1>(rhs);
    }
    return std::get<0>(lhs) < std::get<0>(rhs);
  }
};

struct PairDecComp {
  /**
   * sort by max, min
   */
  bool operator()(const std::pair<double, block::Block*>& lhs, const std::pair<double, block::Block*>& rhs) const {
    if (lhs.first == rhs.first) {
      return lhs.second < rhs.second;
    }
    return lhs.first > rhs.first;
  }
};

class LoadBalance : public Action {
public:
  explicit LoadBalance(block::BlockManager& blockmanager)
      : blockmanager_{blockmanager} {}
  ~LoadBalance() override             = default;
  LoadBalance(const LoadBalance&)     = delete;
  LoadBalance(LoadBalance&&) noexcept = default;
  LoadBalance& operator=(const LoadBalance&) = delete;
  LoadBalance& operator=(LoadBalance&&) = delete;

  void init(block::Block* block, const Pool<field::FieldProperties>& /*fieldpool*/) override;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); };
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return false; }

private:
  std::set<tuple3, Tuple3comp> getOrderedSetOfTuple() const;
  std::map<int, std::vector<block::Block*>> getMovingBlocks() const;
  std::vector<block::Block*> getMovingBlocks(int numOfBlocks, int partnerRank) const;
  static std::vector<int> serializeMovingIDs(const std::map<int, std::vector<block::Block*>>& blocks);
  void findCommunications(const std::map<int, std::vector<block::Block*>>& blocks) const;

  std::map<int, std::map<int, std::set<int>>> getMovedBlockStruct() const;
  std::set<int> getMissingCommunicators(std::map<int, std::map<int, std::set<int>>>& moveBlocks, const std::set<int>& newBlockIDs) const;

  void updateMovedBlocks(std::map<int, std::map<int, std::set<int>>>& moveBlocks) const;
  void updateMovedBlock(int blockID, int sourceID, int targetID) const;

  std::set<int> createBlocks();

  void prepareMessageMove(const std::map<int, std::vector<block::Block*>>& blocks) const;
  void prepareMessageFCS(const std::map<int, std::vector<block::Block*>>& blocks) const;
  void sendBlocks(const std::map<int, std::vector<block::Block*>>& blocks) const;
  std::set<int> removeBlocks(const std::map<int, std::vector<block::Block*>>& blocks) const;
  void processAnnounceBlocks() const;
  static double calcBlockScore(const block::Block* block, int partnerRank);

  block::BlockManager& blockmanager_;  ///< Reference to the block::BlockManager.
  DynamicBlock* dynamicBlock_{};       ///< Pointer to the DynamicBlock.
};

}  // namespace nastja
