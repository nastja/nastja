/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <set>
#include <tuple>

namespace nastja {

using BlockTuple = std::tuple<int, int, int, std::set<int>>;
struct BlockTupleEnum {
  enum BlockTupleIndex { rank = 0,
                         type,
                         source,
                         commList };
};
using BlockTupleIndex = BlockTupleEnum::BlockTupleIndex;

}  // namespace nastja
