/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "activecells.h"
#include "lib/field/field.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"

namespace nastja {

using namespace stencil;

void ActiveCellsDetection::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }
}

/**
 * Detect active cells in boundary of the block for side.
 *
 * @note edges and corners are omitted
 *
 * @param field      Reference to the field.
 * @param side       The side to check.
 */
void ActiveCellsDetection::detect(field::Field<cellid_t, 1>& field, const Direction side) {
  const auto haloBox = field.getHaloBoundingBox(side, field::HaloWay::target);
  auto& data         = getSimData().getData<DataCells>();

  auto view = field.createView(haloBox);
  for (auto* voxel : view) {
    cellid_t currentCell = view.get(voxel);
    if (currentCell <= data.liquid) continue;

    cellsInBoundary_.insert(currentCell);
  }
}

/**
 * Detect active cells in the whole block
 *
 * @param field      Reference to the field.
 */
void ActiveCellsDetection::detectAll(field::Field<cellid_t, 1>& field) {
  auto& data = getSimData().getData<DataCells>();
  auto view = field.createView(field.getOuterBoundingBox());
  for (auto* voxel : view) {
    cellid_t currentCell = view.get(voxel);
    if (currentCell > data.liquid) {
      cellsInBoundary_.insert(currentCell);
    }
  }
}

/// check all cells that are stored in the block, if volume is <= 0 then this cell is deleted because it is not physically present in this block anymore
void ActiveCellsDetection::executeBlock(block::Block* block) {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& field     = block->getFieldByName("cells")->get<cellid_t>();

  cellsInBoundary_.clear();

  //detectAll(field);
  for (auto i : D3C26()) {
    detect(field, D3C26::dir[i]);  /// detect only in the exhanged halo, since in the main sweep (sweep_cells) the voxelsInBlock is already incremented.
  }

  container.findInactiveCells(cellsInBoundary_);
  container.activateCells(cellsInBoundary_);
}

void ActiveCellsDetection::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}
}  // namespace nastja
