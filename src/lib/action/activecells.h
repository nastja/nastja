/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/stencil/direction.h"
#include <unordered_set>

namespace nastja {

/**
 * Detects active cells (cells that have voxels in this block).
 * Deletes cells that are not physically present in the block.
 *
 * Actions have to be registered via `registerAction()`.
 *
 * @ingroup actions cells
 */
class ActiveCellsDetection : public Action {
public:
  explicit ActiveCellsDetection(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~ActiveCellsDetection() override                      = default;
  ActiveCellsDetection(const ActiveCellsDetection&)     = delete;
  ActiveCellsDetection(ActiveCellsDetection&&) noexcept = default;
  ActiveCellsDetection& operator=(const ActiveCellsDetection&) = delete;
  ActiveCellsDetection& operator=(ActiveCellsDetection&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  void detect(field::Field<cellid_t, 1>& field, stencil::Direction side);
  void detectAll(field::Field<cellid_t, 1>& field);

  std::unordered_set<cellid_t> cellsInBoundary_{};  ///< Set for the cellIDs in the boundary.
};

}  // namespace nastja
