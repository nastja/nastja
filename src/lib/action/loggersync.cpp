/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "loggersync.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include <stdexcept>

namespace nastja {

using namespace config::literals;

void LoggerSync::init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) {
  /// @key{Settings.logger.steps, int, 100}
  /// Number of each n-th steps when the loggers are syncronized.
  eachNthStep = getSimData().getConfig().getValue<int>("Settings.logger.steps"_jptr, 100);
}

void LoggerSync::executeBlock(block::Block* /*block*/) {
  throw std::runtime_error("Direct call of blockwise exectution is not permitted for the LoggerSync action.");
}

void LoggerSync::execute(const block::BlockRegister& /*blockRegister*/) {
  logger::isyncAll();
}

}  // namespace nastja
