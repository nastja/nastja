/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/functions/functionregister.h"
#include "lib/functions/signal/signal.h"
#include "lib/functions/signal/signalfactory.h"

namespace nastja {

/**
 * Class for signaling action
 *
 * @ingroup cells
 */
class Signaling : public Action {
public:
  explicit Signaling() = default;

  ~Signaling() override           = default;
  Signaling(const Signaling&)     = delete;
  Signaling(Signaling&&) noexcept = default;
  Signaling& operator=(const Signaling&) = delete;
  Signaling& operator=(Signaling&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override{};
  bool skip() const override { return (getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0; }

  void dump(Archive& ar) const override {
    signalFunctions_.dump(ar);
  }

  void undump(Archive& ar) override {
    signalFunctions_.dump(ar);
  }

private:
  FactorySignal factorySignal_;                ///< The factory for the singal functions.
  FunctionRegister<ISignal> signalFunctions_;  ///< The register for signal functions.
  bool useConstantSignaling_ = false;          ///< Determines of constant signalling is used
};

}  // namespace nastja
