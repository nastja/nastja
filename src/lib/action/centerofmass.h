/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/storage/sparestorage.h"

namespace nastja {

class CenterOfMass : public Action {
public:
  explicit CenterOfMass(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~CenterOfMass() override              = default;
  CenterOfMass(const CenterOfMass&)     = delete;
  CenterOfMass(CenterOfMass&&) noexcept = default;
  CenterOfMass& operator=(const CenterOfMass&) = delete;
  CenterOfMass& operator=(CenterOfMass&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override {
    return !(getSimData().timestep == 0) &&
           ((getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0);
  }

private:
  void calcMassPositions(SpareStorageArray<long, 4>& massPositions, block::Block* block);
  void calcLowerUpperMassPositions(SpareStorage<std::pair<std::array<long, 6>, std::array<long, 6>>>& lowerUpperPositions, block::Block* block);
  void periodicCenter(SpareStorageArray<long, 4>& massPositions, block::Block* block);
  std::array<long, 4> mergeCenter(const std::pair<std::array<long, 6>, std::array<long, 6>>& lowerUpper, block::Block* block);
};

}  // namespace nastja
