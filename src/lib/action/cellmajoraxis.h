/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/sparestorage.h"
#include "lib/math/random.h"

namespace nastja {

class CellMajorAxis : public Action {
public:
  explicit CellMajorAxis(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~CellMajorAxis() override              = default;
  CellMajorAxis(const CellMajorAxis&)     = delete;
  CellMajorAxis(CellMajorAxis&&) noexcept = default;
  CellMajorAxis& operator=(const CellMajorAxis&) = delete;
  CellMajorAxis& operator=(CellMajorAxis&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override {
    return !(getSimData().timestep == 0) &&
           ((getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0);
  }

private:
  enum class polarityMode {
                            random,                                ///Polarity vector is randomized at each recalculation
                            randomWalkOnSphere,                    ///The tip of the polarity vector takes a step of a random walk on the surface of a sphere at each recalculation
                            voxelUpdate,                           ///We calculate the "actual" polarity of the cell using all voxels, and update the cell object polarity to match it
                            signalGrowth,                          ///The polarity vector is determined by the signal gradient, which is calculated using next neighbors
                            __invalid_type                         ///Polarity is not updated
                            };               
  

  math::Vector3<real_t> calcGradientPolarity(block::Block* block, cellid_t const& cellID);
  void calcLocalPolarities(block::Block* block, std::map<cellid_t,std::vector<real_t>>& localPolarities);
  void exchangePolarities(block::Block* block, std::map<cellid_t, std::vector<real_t>>& localPolarities, std::vector<std::map<cellid_t, std::vector<real_t>>>& receivedPolarities);



  std::unique_ptr<ParameterStorageBuilder<real_t>> lambdaPolarity_{};  ///< The pointer to the parameter storage for the polarity lambdas between each cell type
  polarityMode polarityMode_{};
  math::UniformReal<real_t> probability_{0, 1};                ///< The uniform distribution of the probability.
  math::UniformReal<real_t> probabilityPlusMinus_{-1, 1};      ///< The uniform distribution of the probability.
  real_t epsilon{0.01};                                        ///< The step size of randomWalkOnSphere
  long unsigned int startTime{};
  std::set<cellid_t> cellTypesToExchange_{};///< Set containing the cell types to be exchanged (those that have > 0 polarity lambda)
  int growthSignal{0};/// The signal used for polarity change along gradient
  std::vector<std::pair<cellid_t, int>> neighbors_;  ///< Indices for the neighbors.
};

}  // namespace nastja
