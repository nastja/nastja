/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "borderdetection.h"
#include "lib/logger.h"
#include "lib/action/dynamicblock.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C6.h"
#include <algorithm>
#include <cassert>

namespace nastja {

using namespace stencil;

void BorderDetection::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();
  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }
}

/**
 * Copy the boundary and checks for values that reached the boundary.
 *
 * @note sum over small values could be problematic.
 *
 * @param field The field.
 * @param side  The side for detection.
 *
 * @return 0 if no boundary reached, 1 if reached in zero and 2 if reached in full.
 */
ReachedBoundary BorderDetection::detectReachedBoundary(field::Field<real_t, 1>& field, const Direction side) {
  const auto haloBox = field.getHaloBoundingBox(side);

  auto start = haloBox.lower();
  auto end   = haloBox.upper();

  // sum inner layer values
  real_t sum = 0;
  for (unsigned int z = start[2]; z < end[2]; z++) {
    for (unsigned int y = start[1]; y < end[1]; y++) {
      for (unsigned int x = start[0]; x < end[0]; x++) {
        for (unsigned int v = 0; v < field.getVectorSize(); v++) {
          unsigned long index = field.getIndex(x, y, z);

          sum += field.getCell(index, v);
        }
      }
    }
  }

  // calculating the inner layer volume
  real_t layerVolume = 1;
  for (int i = 0; i < 3; i++) {
    int length = end[i] - start[i];
    layerVolume *= std::max(1, length);
  }

  // check for full or empty layer
  if (sum == 0 || sum == layerVolume) return ReachedBoundary::none;
  if (sum < 0.5 * layerVolume) return ReachedBoundary::empty;
  return ReachedBoundary::full;
}

void BorderDetection::executeSide(field::IField& field, const Direction side) {
  if (!field.getBlock().hasDataContainer<DynamicBlockContainer>()) return;

  auto& data = field.getBlock().getDataContainer<DynamicBlockContainer>();

  auto& field_ = field.get<real_t>();

  int reachedBoundary = detectReachedBoundary(field_, side);
  if (reachedBoundary != ReachedBoundary::none) {
    logger::get().trace(logger::Category::DynReachBD, "Hey RAND({})! {}-{}", reachedBoundary, field.getID(), getDirectionString(side));

    unsigned int sideBit = 2 * (D3C26::index[static_cast<unsigned int>(side)] + 1);  // odd bits start with 3

    data.boundaryDetection.set(sideBit);
    if (reachedBoundary == ReachedBoundary::full) data.boundaryDetection.set(sideBit + 1);
  }
}

void BorderDetection::executeBlock(block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  int bitset = 0;

  for (auto side : D3C6()) {
    if (!field->boundarySet[side]->isMPI() && !field->boundarySet[side]->isInner()) {
      bitset |= stencil::skipExchange[side];
    }
  }
  std::bitset<26> skipSides(bitset);
  for (unsigned int side = 0; side < numOfSides_; side++) {
    if (!skipSides[side]) {
      if (field->getNeighborID(D3C26::dir[side]) == -1) {
        executeSide(*field, D3C26::dir[side]);
      }
    }
  }
}

void BorderDetection::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
