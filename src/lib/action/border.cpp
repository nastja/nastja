/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "border.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;

void Border::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  if (type_ == BorderType::open) return;  // TODO: for actions that are init befor the sweep, fields are unknown.
  const auto& fieldname = getFieldname();
  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }
}

/**
 * Open (iRecv) all MPI boundaries.
 *
 * @param block  The block.
 */
void Border::doOpenBlock(const block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  unsigned int mask     = 0;
  Boundary* boundaryMPI = nullptr;

  // look for MPI boundaries
  for (auto i : D3C6()) {
    if (field->boundarySet[i]->isMPI()) {
      if (boundaryMPI == nullptr) boundaryMPI = field->boundarySet[i];
    } else if (!field->boundarySet[i]->isInner()) {
      mask |= stencil::skipExchange[i];
    }
  }

  // we have MPI boundaries
  if (boundaryMPI != nullptr) {
    boundaryMPI->preExecute(*field, mask);
  }
}

/**
 * Send (Send) all MPI boundaries.
 *
 * @param block  The block.
 */
void Border::doSendBlock(const block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  unsigned int mask     = 0;
  Boundary* boundaryMPI = nullptr;

  // look for MPI boundaries
  for (auto i : D3C6()) {
    if (field->boundarySet[i]->isMPI()) {
      if (boundaryMPI == nullptr) boundaryMPI = field->boundarySet[i];
    } else if (!field->boundarySet[i]->isInner()) {
      mask |= stencil::skipExchange[i];
    }
  }

  // we have MPI boundaries
  if (boundaryMPI != nullptr) {
#ifdef USE_CUDA
    if (field->getCuDataOnGpu()) {
      // copy exchange array only of there are mpi boundaries, and there is data on the gpu
      field->cuCpExchangeDeviceToHost();
    }
#endif
    boundaryMPI->execute(*field, mask);
  }
}

/**
 * Wait until all open iRecv are complete. Then execute the non-MPI boundaries.
 *
 * @param block  The block.
 */
void Border::doFinalizeBlock(const block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());

  MPI_Waitall(26, field->getRecvRequest(), MPI_STATUSES_IGNORE);

#ifdef USE_CUDA
  if (field->getCuDataOnGpu()) {
    field->cuCpExchangeHostToDevice();
  }
#endif

  // non MPI boundaries
  for (auto i : D3C6()) {
    if (!field->boundarySet[i]->isMPI()) {
      field->boundarySet[i]->execute(*field, D3C6::dir[i]);
    }
  }
}

/**
 * Open (iRecv) all MPI boundaries.
 *
 * @param blockRegister  The block register.
 */
void Border::doOpen(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    doOpenBlock(block.second);
  }
}

/**
 * Send (Send) all MPI boundaries.
 *
 * @param blockRegister  The block register.
 */
void Border::doSend(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    doSendBlock(block.second);
  }
}

/**
 * Wait until all open iRecv are complete. Then execute the non-MPI boundaries.
 *
 * @param blockRegister  The block register.
 */
void Border::doFinalize(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    doFinalizeBlock(block.second);
  }
}

void Border::doFullExchange(const block::BlockRegister& blockRegister) {
  doOpen(blockRegister);
  doSend(blockRegister);
  doFinalize(blockRegister);
}

void Border::executeBlock(block::Block* block) {
  if (type_ == BorderType::fullExchange) {
    throw std::runtime_error("Single block execution is not allowed for fullExchange.");
  } else if (type_ == BorderType::open) {
    doOpenBlock(block);
  } else if (type_ == BorderType::send) {
    doSendBlock(block);
  } else if (type_ == BorderType::finalize) {
    doFinalizeBlock(block);
  }
}

void Border::execute(const block::BlockRegister& blockRegister) {
  if (type_ == BorderType::fullExchange) {
    doFullExchange(blockRegister);
  } else if (type_ == BorderType::open) {
    doOpen(blockRegister);
  } else if (type_ == BorderType::send) {
    doSend(blockRegister);
  } else if (type_ == BorderType::finalize) {
    doFinalize(blockRegister);
  }
}

}  // namespace nastja
