/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellcleaner.h"
#include "lib/field/field.h"
#include "lib/math/boundingbox.h"
#include "lib/math/calculatesurface.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/direction.h"

namespace nastja {

using namespace stencil;
using namespace config::literals;

template <unsigned int DIM>
void CellCleaner<DIM>::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  /// @key{CellsInSilico.cleaner.killDistance, int, 0}
  /// If set to > 0 all voxels outside of this distance to the cell center are killed.
  killDistanceSquare_ = getSimData().getConfig().template getValue<int>("CellsInSilico.cleaner.killdistance"_jptr, 0);
  killDistanceSquare_ *= killDistanceSquare_;

  /// @key{CellsInSilico.cleaner.steps, int, 100}
  /// Number of each n-th steps when the cell cleaner is executed.
  eachNthStep = getSimData().getConfig().template getValue<int>("CellsInSilico.cleaner.steps"_jptr, 100);

  offset = 1;
}

/// Sweeps through the whole Field and looks for individual lost Pixels, those are deleted and the volume and surface of the cell is changed accordingly
/// if a voxel of the cell is more than killDistance away from the cells Center of mass this voxel is deleted and the

template <unsigned int DIM>
void CellCleaner<DIM>::killSinglePixels(field::Field<cellid_t, 1>& field, CellBlockContainer& container) {
  auto& data = getSimData().template getData<DataCells>();

  const auto& boundarySize = field.getBoundarySize();

  auto& block        = field.getBlock();
  const auto& offset = block.offset;

  // Skip voxel on the inner boundary. This avoids a further exchange to be consistence with the border of the neighbor
  // block. When the voxel moves the next cell cleaner call will kill it. Immobile single voxels are never killed, so
  // that if a cell moves away, the MoD will intervene if necessary.
  auto bBox = math::BoundingBox<unsigned long>(field.getBoundarySize() + 1, field.getSize() - field.getBoundarySize() - 1);
  if (DIM == 2) {
    bBox[0].z() = 0;
    bBox[1].z() = 1;
  }

  auto view = field.createView(bBox);
  for (auto it = view.begin(); it != view.end(); ++it) {
    cellid_t currentCell = view.get(*it);
    if (currentCell <= data.liquid) continue;

    int faces = math::calcSurfaceFaces(view, *it, currentCell);

    // check for single pixel
    if (faces == 6) {
      //TODO: find a better way to fix this issue (problem with undump)
      if (container.surface == nullptr){
        std::string fun{"."};
        std::string oor{"OUTOFRANGE"};        
        size_t i{0};
        while (fun != oor){
          auto ptr{config::getJsonPointer("CellsInSilico.energyfunctions." + std::to_string(i))};
          fun = getSimData().getConfig().template getValue<std::string>(ptr, oor);
          if (fun == "Volume00") container.volume = std::make_unique<math::VolumeCount<>>();
          else if (fun == "Volume01") container.volume = std::make_unique<math::VolumeMC<>>();
          else if (fun == "Surface00") container.surface = std::make_unique<math::SurfaceCount<>>();
          else if (fun == "Surface01") container.surface = std::make_unique<math::SurfaceMC<>>();
          ++i;
        }
      }
      container.cells(currentCell).deltaSurface += container.surface->diff(view, *it, currentCell, data.liquid);
      container.cells(currentCell).deltaVolume += container.volume->diff(view, *it, currentCell, data.liquid);
      container.cells(currentCell).voxelsInBlock--;
      view.get(*it) = data.liquid;
      continue;      
    }

    // check for out of sphere around the cell center
    if (killDistanceSquare_ > 0 && container.cells.contains(currentCell)) {
      const auto& center = container.cells(currentCell).center;
      math::Vector3<int> pos{static_cast<int>(it.coordinates().x()), static_cast<int>(it.coordinates().y()), static_cast<int>(it.coordinates().z())};
      pos += offset - boundarySize;
      math::Vector3<int> size{static_cast<int>(block.blocks[0] * block.size[0]),
                              static_cast<int>(block.blocks[1] * block.size[1]),
                              static_cast<int>(block.blocks[2] * block.size[2])};

      long distance = periodicDistanceSquare(pos, center, size);
      if (distance > killDistanceSquare_) {
        container.cells(currentCell).deltaSurface += container.surface->diff(view, *it, currentCell, data.liquid);
        container.cells(currentCell).deltaVolume += container.volume->diff(view, *it, currentCell, data.liquid);
        container.cells(currentCell).voxelsInBlock--;
        view.get(*it) = data.liquid;
      }
    }
  }
}

/**
 * Calculates the square of the distance between p1 and p2 in an periodic cube with size.
 *
 * @tparam T   The data type.
 * @param p1   Point 1.
 * @param p2   Point 2.
 * @param size Periodic cube size.
 * @return Square of the distance.
 */
template <unsigned int DIM>
template <typename T>
T CellCleaner<DIM>::periodicDistanceSquare(const math::Vector3<T>& p1, const math::Vector3<T>& p2, const math::Vector3<T>& size) {
  T sum = 0;

  for (unsigned int dim = 0; dim < DIM; dim++) {
    T delta = abs(p2[dim] - p1[dim]);

    if (delta > size[dim] - delta) {
      delta = size[dim] - delta;
    }

    sum += delta * delta;
  }

  return sum;
}

template <unsigned int DIM>
void CellCleaner<DIM>::executeBlock(block::Block* block) {
  auto& data  = block->getDataContainer<CellBlockContainer>();
  auto& field = block->getFieldByName(getFieldname())->template get<cellid_t>();

  killSinglePixels(field, data);
}

template <unsigned int DIM>
void CellCleaner<DIM>::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

template class CellCleaner<3>;
template class CellCleaner<2>;

}  // namespace nastja
