/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "filling.h"
#include "lib/field/field.h"
#include "lib/filling/fillfunction.h"
#include "lib/math/boundingbox.h"
#include "lib/io/writerreader.h"
#include <algorithm>
#include <numeric>


namespace nastja {

using namespace filling;

using json = nlohmann::json;

void Filling::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  for (const auto& fieldname : getFieldnames()) {
    for (const auto& block : blockRegister) {
      auto* field = block.second->getFieldByName(fieldname);

      if (field == nullptr) {
        throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
      }
    }
  }
}

std::string Filling::getPatternString(const json::json_pointer& jptr) const {
  auto pattern_jptr  = jptr / "pattern";
  const auto& config = getSimData().getConfig();

  return config.getValue<std::string>(pattern_jptr, "const");
}


/**
 * Reads the filling for each block of each field from the filepath indicated in the config
 *
 *
 * @param block  The block
 * @param fieldname  The fieldname
 *
 */
void Filling::readInitFilling(block::Block* block, std::string const& fieldname) {
  auto& field = block->getFieldByName(fieldname)->get<cellid_t>();

  const auto& boundarySize = field.getBoundarySize();
  unsigned int sizeY       = field.getSize(1) - 2 * boundarySize[1];
  unsigned int sizeZ       = field.getSize(2) - 2 * boundarySize[2];
  cellid_t liquid = getSimData().getData<DataCells>().liquid;
  
  /// @key{CellsInSilico.Filling.fieldname.fillfilePrefix, std::string, "."}
  /// The path to the filling files
  std::string filename_ = getSimData().getConfig().getValue<std::string>(config::getJsonPointer("Filling.fillfilePrefix"), ".") + "_" + std::to_string(block->id) + ".raw";


  // Open the file
  std::ifstream inputFile(filename_);

  // Check if the file is open
  if (!inputFile.is_open()) {
      std::cout << "Error opening the file." << '\n';
      return;
  }

  // Read the line from the file
  std::string line;
  std::getline(inputFile, line);



  // Create a stringstream to parse the numbers
  std::istringstream iss(line);

  // Vector to store the numbers
  std::vector<cellid_t> numbers;

  // Read numbers from the stringstream and store them in the vector
  cellid_t num;
  long unsigned int count{0};
  unsigned int xIndex{0};
  unsigned int yIndex{0};
  unsigned int zIndex{0};

  while (iss >> num) {
      xIndex = count / (sizeY*sizeZ);
      yIndex = (count / sizeZ) % sizeY;
      zIndex = count % sizeZ;
      if (num > liquid){
        throw std::out_of_range(fmt::format("Currently only ids smaller than or equal to liquid({}) are allowed.", liquid));
      }
      if (num > 0){
        field.getCell(xIndex+1,yIndex+1,zIndex+1, 0) = num;
      } 
      ++count;
  }
  // Close the file
  inputFile.close();
  std::cout << "Block " << block->id << ": done reading... count = " << count << '\n';
  return;
}




/**
 * Creates a cell type vector, where the index is the cell type and the elements are the largest cell ID corresponding to
 * this cell type.
 *
 * An array `[0,0,2,1,0]` becomes `[0,0,2,3,inf]`, so that the values smaller than n can be counted. A single value `2`
 * becomes `[0,0,inf]`, so that each positive value have 2 entries smaller than 2.
 *
 * @param jptr  The json pointer.
 *
 * @return Vector with maps cell ID to cell type.
 */
std::vector<unsigned int> Filling::createCellTypeVector(const json::json_pointer& jptr) const {
  auto celltype_jptr = jptr / "celltype";
  const auto& config = getSimData().getConfig();

  std::vector<unsigned int> cellType;
  if (config.contains(celltype_jptr)) {
    if (config.isArray(celltype_jptr)) {
      /// @key{Filling.\template{field}[].\template{shape}.celltype, int[]}
      /// Array of number of cells per cell type. [0,0,2,1,0] means, cellID 0 has type 0; cellID 1, 2 have type 2;
      /// cellID 3 have type 3; the last type catch all, so that cellID >= 4 get type 4. The cellIDs are shifted if
      /// there is a `value` indicates the first cell ID. (Alternative: single value)
      cellType = config.getVector<unsigned int>(celltype_jptr);
    } else {
      /// @key{Filling.\template{field}[].\template{shape}.celltype, int, 0}
      /// The cell type (Alternative: array)
      auto singleCellType = config.getValue<unsigned int>(celltype_jptr);
      cellType.insert(cellType.end(), singleCellType + 1, 0);  // The last value becomes max.
    }
  } else {
    cellType.push_back(0);
  }

  auto value = config.getValue<unsigned int>(jptr / "value");
  cellType[0] += value - 1;  // Add the first free ID.
  std::partial_sum(cellType.begin(), cellType.end(), cellType.begin());
  cellType.back() = std::numeric_limits<unsigned int>::max();  // Last type, catches all.

  return cellType;
}

template <typename T, unsigned int TSplitX>
bool Filling::doFilling(const json::json_pointer& jptr, field::Field<T, TSplitX>& field, std::map<unsigned int, std::set<T>>* cellTypeMap /*= nullptr*/) const {
  auto& config = getSimData().getConfig();
  FactoryFilling<T, TSplitX> factoryFilling;  ///< The factory for the filling geometries.
  std::unique_ptr<FillFunction<T, TSplitX>> fillFunction = factoryFilling.create(config.getValue<std::string>(jptr / "shape"), jptr, getSimData().getConfig());

  const auto boundarySize = field.getBoundarySize();
  const auto offset       = field.getOffset();
  const auto blocksize    = field.getSize() - 2 * boundarySize;

  filling::pattern::FactoryPattern<T> factoryPattern;  ///< The factory for the patterns.
  std::unique_ptr<pattern::Pattern<T>> pattern(factoryPattern.create(getPatternString(jptr), jptr, field.getSimData(), offset, boundarySize, blocksize));

  /// @key{Filling.\template{field}[].\template{shape}.component, uint, 0}
  /// The component for filling.
  auto component = config.getValue<unsigned int>(jptr / "component", 0);

  std::vector<unsigned int> cellTypes;
  if (cellTypeMap) {
    cellTypes = createCellTypeVector(jptr);
  }

  return fillFunction->fill(field, component, pattern.get(), cellTypes, cellTypeMap);
}
template bool Filling::doFilling<real_t>(const json::json_pointer& jptr, field::Field<real_t, 1>& field, std::map<unsigned int, std::set<real_t>>* cellTypeMap) const;
template bool Filling::doFilling<real_t>(const json::json_pointer& jptr, field::Field<real_t, 2>& field, std::map<unsigned int, std::set<real_t>>* cellTypeMap) const;
template bool Filling::doFilling<real_t>(const json::json_pointer& jptr, field::Field<real_t, 4>& field, std::map<unsigned int, std::set<real_t>>* cellTypeMap) const;
template bool Filling::doFilling<real_t>(const json::json_pointer& jptr, field::Field<real_t, 8>& field, std::map<unsigned int, std::set<real_t>>* cellTypeMap) const;
template bool Filling::doFilling<real_t>(const json::json_pointer& jptr, field::Field<real_t, 16>& field, std::map<unsigned int, std::set<real_t>>* cellTypeMap) const;
template bool Filling::doFilling<cellid_t>(const json::json_pointer& jptr, field::Field<cellid_t, 1>& field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap) const;
template bool Filling::doFilling<cellid_t>(const json::json_pointer& jptr, field::Field<cellid_t, 2>& field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap) const;
template bool Filling::doFilling<cellid_t>(const json::json_pointer& jptr, field::Field<cellid_t, 4>& field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap) const;
template bool Filling::doFilling<cellid_t>(const json::json_pointer& jptr, field::Field<cellid_t, 8>& field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap) const;
template bool Filling::doFilling<cellid_t>(const json::json_pointer& jptr, field::Field<cellid_t, 16>& field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap) const;

/**
 * Select the correct type of the field for the filling.
 *
 * @param jptr         The json pointer.
 * @param field        Pointer to the field.
 * @param cellTypeMap  An optional map for register cellIDs by cell type.
 *
 * @return True, if something was filled.
 */
bool Filling::doFilling(const json::json_pointer& jptr, field::IField* field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap /*= nullptr*/) const {
  if (auto* field_ = field->getPtr<real_t, false>()) {
    return doFilling(jptr, *field_);
  }
  if (auto* field_ = field->getPtr<real_t, true>()) {
    return doFilling(jptr, *field_);
  }

  if (auto* field_ = field->getPtr<cellid_t, false>()) {
    return doFilling(jptr, *field_, cellTypeMap);
  }
  if (auto* field_ = field->getPtr<cellid_t, true>()) {
    return doFilling(jptr, *field_, cellTypeMap);
  }

  throw std::invalid_argument(fmt::format("Field '{}' of unknown type.", getFieldname()));
}

/**
 * fills the whole domain with a default value
 *
 * @param field_  The pointer to the field.
 * @param value   The default value.
 */
void Filling::fillDomainDefaultValue(field::IField* field_, cellid_t value) {
  auto& field = field_->get<cellid_t>();

  std::fill_n(field.getCellBasePtr(), field.getPaddedCells(), value);
}

/**
 * Default filling for cells.
 *
 * Set up the solid and liquid types. Also the domain is filled by the liquid value.
 *
 * @param block  The block.
 * @param field  The field.
 */
void Filling::defaultFillingCells(block::Block* block, field::IField* field) const {
  cellid_t liquid = getSimData().getData<DataCells>().liquid;
  auto& container = block->getDataContainer<CellBlockContainer>();

  for (cellid_t cellType = 0; cellType <= liquid; ++cellType) {
    container.cells[cellType].type = cellType;
  }

  fillDomainDefaultValue(field, liquid);
}

void Filling::executeBlock(block::Block* block) {
  auto& config = getSimData().getConfig();
  //auto& container = block->getDataContainer<CellBlockContainer>();

  /// @key{Filling.useFillfile, bool, false}
  /// Flag to enable or disable initial filling from raw file. This is set depending on the value of the string
  useInitFilling_ = config.getValue<bool>(config::getJsonPointer("Filling.useFillfile"), false);      


  for (const auto& fieldname : getFieldnames()) {
    auto jptr = config::getJsonPointer("Filling." + fieldname);
    if (!config.contains(jptr)) continue;

    cellid_t firstFreeID = 0;
    bool hasCells        = block->hasDataContainer<CellBlockContainer>();
    auto* field          = block->getFieldByName(fieldname);

    std::unique_ptr<std::map<unsigned int, std::set<cellid_t>>> cellTypeMap;
    cellid_t liquid = 0;
    if (hasCells) {
      defaultFillingCells(block, field);
      liquid = getSimData().getData<DataCells>().liquid;

      firstFreeID = liquid + 1;

      cellTypeMap = std::make_unique<std::map<unsigned int, std::set<cellid_t>>>();
    }

    for (unsigned int fillArrayElement = 0; fillArrayElement < config.arraySize(jptr); fillArrayElement++) {
      // if no value is given use the firstFreeID as value
      auto elementJptr = jptr / fillArrayElement;

      getSimData().globalGenerator.seed(getSimData().getSeed(elementJptr));

      auto& filling     = config.getRawJson();
      if (!config.contains(elementJptr / "value")) {
        filling[elementJptr / "value"] = firstFreeID;
      }

      if (config.contains(elementJptr / "count")) {
        auto numberOfElements = config.getValue<unsigned long>(elementJptr / "count");
        firstFreeID += numberOfElements;
      } else {
        firstFreeID++;
      }

      if (!doFilling(elementJptr, field, cellTypeMap.get())) continue;

      if (hasCells) {
        auto& container = block->getDataContainer<CellBlockContainer>();
        for (const auto& typesets : *cellTypeMap) {
          auto cellType = typesets.first;
          for (auto cellID : typesets.second) {
            if (cellID <= liquid) continue;
            container.cells[cellID].type  = cellType;
            container.cells[cellID].birth = getSimData().timestep;
            if (config.contains(elementJptr / "polarity")) {
              if (container.cells[cellID].newPolarity == math::Vector3<real_t>{0.,0.,0.}){ //We only want to update this once for each cell
                container.cells[cellID].polarity = config.getVector3<real_t>(elementJptr / "polarity");
                container.cells[cellID].polarity = container.cells[cellID].polarity.normalize();
                container.cells[cellID].newPolarity = container.cells[cellID].polarity;
              }
            }
          }
        }
      }
    }
    if ( useInitFilling_ ){
      std::cout << "Reading" << '\n';
      readInitFilling(block, fieldname);
    }

    if (hasCells) {
      auto& container          = block->getDataContainer<CellBlockContainer>();
      container.reservedCellID = firstFreeID;  // First unused ID
    }
  }
}

void Filling::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
