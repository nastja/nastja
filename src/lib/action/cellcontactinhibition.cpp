/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellcontactinhibition.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include "lib/io/writerreader.h"
#include "lib/math/matrix3.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/D2C8.h"
#include "lib/stencil/D2C9.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <string>
#include <algorithm>
#include <vector>

namespace nastja {

using namespace stencil;
using namespace config::literals;

//template <unsigned int DIM>
using DxC26 = typename std::conditional<3 == 3, D3C26, D2C8>::type; //TODO make this actually dependent on the dimension
using DxC27 = typename std::conditional<3 == 3, D3C27, D2C9>::type; //TODO make this actually dependent on the dimension

void CellContactInhibition::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();
  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);
    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }

    auto& container = block.second->getDataContainer<CellBlockContainer>();
    container.activateNearestNeighborSearch();  //TODO: only needed when neighbor search is needed
  }

  auto& config = getSimData().getConfig();


  /// @key{CellsInSilico.contactinhibition.recalculationtime, uint, 10}
  /// Number of each n-th steps when the contact inhibition strengths are calculated.
  eachNthStep = config.template getValue<unsigned int>("CellsInSilico.contactinhibition.recalculationtime"_jptr, 10);

  /// @key{CellsInSilico.contactinhibition.mode, std::string, "distance"}
  /// The mode of the polarity update
  std::string mode = getSimData().getConfig().getValue<std::string>("CellsInSilico.contactinhibition.mode"_jptr, "distance");


  /// @key{CellsInSilico.contactinhibition.inhibitionstrength, ParameterStorage<real_t>}
  /// The matrix of the inhibition strength between each cell type
  inhibitionStrength_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.contactinhibition.inhibitionstrength"_jptr, config);

  /// @key{CellsInSilico.contactinhibition.inhibitiondistance, ParameterStorage<real_t>}
  /// The matrix of the inhibition distance for each cell type
  inhibitionDist_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.contactinhibition.inhibitiondistance"_jptr, config);

  if (mode == "distance"){
    inhibitionCalcMode_ = InhibitionCalcMode::distance;
  }
  else{
    throw std::runtime_error("Unknown inhibitionmode");
  }


  //Fill cellTypesToExchange_
  auto& inhibitionStrength = inhibitionStrength_->getStorage();
  for (cellid_t i = 0; i < inhibitionStrength.getSize(); ++i){
    for (cellid_t j = 0; j < inhibitionStrength.getSize(); ++j){
      if (inhibitionStrength.getValue(i,j) > 0.){
        cellTypesToExchange_.insert(i);
        break;
      }
    }
  }
}

/**
 * Calculates the local inhibition by looking at the distances between cell centers in the local block
 *
 * @param block       The block.
 *
 */
void CellContactInhibition::calculateNeighborTypesByDist(block::Block* block){

  localLinearizedInhibitionData_.clear();
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();
  auto& inhibitionDist = inhibitionDist_->getStorage();
  auto& inhibitionStrength = inhibitionStrength_->getStorage();
  auto systemSize = static_cast<math::Vector3<long int>>(block->size * block->blocks);



  for (auto it: container.cells)  {
    cellid_t cellID = it.first;
    if (cellID <= data.liquid) continue;
    if (cellTypesToExchange_.find(container.cells(cellID).type) == cellTypesToExchange_.end() ) continue;

    math::Vector3<real_t> currentCenter{it.second.center};
    for ( auto itInner : container.cells){
      cellid_t otherID = itInner.first;

      if (otherID <= data.liquid || otherID == cellID) continue;
      if (cellTypesToExchange_.find(container.cells(otherID).type) == cellTypesToExchange_.end() ) continue;



      math::Vector3<real_t> otherCenter{itInner.second.center};
      math::Vector3<real_t> magnitudeVec{currentCenter - otherCenter};

      //Check for periodic boundaries
      for (size_t i=0; i< 3;++i){
        if (magnitudeVec[i] > systemSize[i]/2){
          magnitudeVec[i] -= systemSize[i];
        }
        else if (magnitudeVec[i] < (-1*systemSize[i]/2)){
          magnitudeVec[i] += systemSize[i];
        }
      }      
      real_t distance{sqrt(magnitudeVec[0]*magnitudeVec[0] + magnitudeVec[1]*magnitudeVec[1] + magnitudeVec[2]*magnitudeVec[2])};


      if (distance <= inhibitionDist.getValue(it.second.type) + inhibitionDist.getValue(itInner.second.type)){
        localLinearizedInhibitionData_.push_back(static_cast<real_t>(cellID));
        localLinearizedInhibitionData_.push_back(static_cast<real_t>(otherID));
        localLinearizedInhibitionData_.push_back(inhibitionStrength.getValue(container.cells(cellID).type, container.cells(otherID).type));

      }
    }
  }
}


/**
 * Calculates final inhibition value from linearized vector.
 *
 * @param linearizedData       The vector to unravel. Every 3 entries belong together
 * @param alreadyCountedIDs    The map containing the already counted sourceIDs for a given targetID
 * @param finalInhibitionData  The map containing the inhibitionValues for each targetID
 *
 */
void unravelLinearizedInhibitionContribution(std::vector<real_t> const& linearizedData,
                                             std::map<cellid_t,std::vector<cellid_t>>& alreadyCountedIDs,
                                             std::map<cellid_t, real_t>& finalInhibitionData ){


  for(size_t i=0; i<linearizedData.size(); i+=3){
    cellid_t targetID{static_cast<cellid_t>(linearizedData[i])};
    cellid_t sourceID{static_cast<cellid_t>(linearizedData[i+1])};
    real_t inhibVal{linearizedData[i+2]};
    if (alreadyCountedIDs.find(targetID) == alreadyCountedIDs.end()){
      alreadyCountedIDs[targetID] = std::vector<cellid_t>{};
      finalInhibitionData[targetID] = 0.;
    }
    if(std::find(alreadyCountedIDs[targetID].begin(),alreadyCountedIDs[targetID].end(),sourceID) == alreadyCountedIDs[targetID].end()){
      alreadyCountedIDs[targetID].push_back(sourceID);
      finalInhibitionData[targetID] += inhibVal;
    }
  }      
}


/**
 * Synchronizes the inhibition contributions across neighboring blocks
 *
 * @param block       The block.
 *
 */
void CellContactInhibition::synchronizeNeighborTypes(block::Block* block){


  auto& field = block->getFieldByName("cells")->get<cellid_t>();
  int MPI_size{};
  MPI_Comm_size(MPI_COMM_WORLD,&MPI_size);
  int MPI_rank{};
  MPI_Comm_rank(MPI_COMM_WORLD,&MPI_rank);
  
  int neighborRank{};

  int const NDirections{26};//TODO change this depending on 2d vs 3d
  int send_tag{};
  int recv_tag{};
  std::vector<cellid_t> receivedVectorSizes(NDirections,0);

  std::vector<std::vector<real_t>> receivedLinarizedVecs{};

  MPI_Request requests_send_ints[NDirections];
  MPI_Request requests_recv_ints[NDirections];
  MPI_Request requests_send[NDirections];
  MPI_Request requests_recv[NDirections];

  // Initialize all requests to MPI_REQUEST_NULL (it's not always the case that every entry will be connected to a send/recv operation)
  for (int i = 0; i < NDirections; i++) {
      requests_send_ints[i] = MPI_REQUEST_NULL;
      requests_recv_ints[i] = MPI_REQUEST_NULL;
      requests_send[i] = MPI_REQUEST_NULL;
      requests_recv[i] = MPI_REQUEST_NULL;
  }

  int const NEntriesLocal{static_cast<int>(localLinearizedInhibitionData_.size())};
  std::set<cellid_t> syncedRanks{};
  for (unsigned int dir : DxC26()){
    neighborRank = field.getNeighborRank(DxC26::dir[dir]);
    if (neighborRank == MPI_rank or syncedRanks.find(neighborRank) != syncedRanks.end()) continue;
    syncedRanks.insert(neighborRank);
    send_tag = MPI_rank;
    recv_tag = neighborRank;
    MPI_Issend(&NEntriesLocal,1,MPI_NASTJA_INTEGER, neighborRank,send_tag,MPI_COMM_WORLD,&requests_send_ints[dir]);
    MPI_Irecv(&receivedVectorSizes[dir],1,MPI_NASTJA_INTEGER, neighborRank, recv_tag, MPI_COMM_WORLD, &requests_recv_ints[dir]);
  }

  MPI_Waitall(NDirections,&requests_send_ints[0],MPI_STATUSES_IGNORE);
  MPI_Waitall(NDirections,&requests_recv_ints[0],MPI_STATUSES_IGNORE);

  for (unsigned int dir : DxC26()){
    receivedLinarizedVecs.push_back(std::vector<real_t>(receivedVectorSizes[dir],0.));
  }

// 3) Share cellIDs AND spacing
// 4) Share linearized vector
  syncedRanks.clear();
  for (unsigned int dir : DxC26()){
    neighborRank = field.getNeighborRank(DxC26::dir[dir]);

    if (neighborRank == MPI_rank or syncedRanks.find(neighborRank) != syncedRanks.end()) continue;
    syncedRanks.insert(neighborRank);

    send_tag = MPI_rank;
    recv_tag = neighborRank;


    if ( NEntriesLocal > 0){
      MPI_Issend(&localLinearizedInhibitionData_[0], NEntriesLocal,MPI_NASTJA_REAL, neighborRank,send_tag,MPI_COMM_WORLD,&requests_send[dir]);
    }
  
    if (receivedVectorSizes[dir] > 0){
      MPI_Irecv(&(receivedLinarizedVecs[dir][0]),receivedVectorSizes[dir],MPI_NASTJA_REAL, neighborRank, recv_tag, MPI_COMM_WORLD, &requests_recv[dir]);
    }
  }
  MPI_Waitall(NDirections,&requests_send[0],MPI_STATUSES_IGNORE);
  MPI_Waitall(NDirections,&requests_recv[0],MPI_STATUSES_IGNORE);




// 5) Distribute inhibitions
  std::map<cellid_t,std::vector<cellid_t>> alreadyCountedIDs{};
  syncedRanks.clear();
  finalInhibitionStrength_.clear();

  unravelLinearizedInhibitionContribution(localLinearizedInhibitionData_, alreadyCountedIDs, finalInhibitionStrength_);
  syncedRanks.clear();
  for (unsigned int dir : DxC26()){
    neighborRank = field.getNeighborRank(DxC26::dir[dir]);
    if (neighborRank == MPI_rank or syncedRanks.find(neighborRank) != syncedRanks.end()) continue;
    unravelLinearizedInhibitionContribution(receivedLinarizedVecs[dir],alreadyCountedIDs,finalInhibitionStrength_);
    syncedRanks.insert(neighborRank);
  }
  MPI_Barrier(MPI_COMM_WORLD);//TODO is this necessary?
}

void CellContactInhibition::executeBlock(block::Block* block) {
  if (inhibitionCalcMode_ == InhibitionCalcMode::distance){
    calculateNeighborTypesByDist(block);
  }
  synchronizeNeighborTypes(block);


  auto& container = block->getDataContainer<CellBlockContainer>();
  for (auto iter = finalInhibitionStrength_.begin(); iter != finalInhibitionStrength_.end(); ++iter){
    if(container.cells.find(iter->first) != container.cells.end()){
      container.cells(iter->first).newInhibitionStrength = iter->second;
    }
  }
}

void CellContactInhibition::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
