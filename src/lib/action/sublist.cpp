/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sublist.h"

namespace nastja {

/**
 * Call init for all elements in the ActionRegister for all blocks.
 *
 * @param blockRegister Reference to the block::BlockRegister.
 * @param fieldpool     Reference to the fieldpool.
 */
void Sublist::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  actionRegister_.initBlocks(blockRegister, fieldpool);
}

/**
 * Call init for all elements in the ActionRegister for a single block.
 *
 * @param block     Pointer to the Block.
 * @param fieldpool Reference to the fieldpool.
 */
void Sublist::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  actionRegister_.initBlock(block, fieldpool);
}

/**
 * Execute each action from the ActionRegister on the given Block.
 *
 * @param block Pointer to the Block.
 */
void Sublist::executeBlock(block::Block* block) {
  actionRegister_.executeBlock(block);
}

/**
 * Execute for each block.
 *
 * @param blockRegister  Reference to the block::BlockRegister.
 */
void Sublist::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

/**
 * Register a Action to the ActionRegister.
 *
 * @param action Pointer to the Action.
 */
void Sublist::registerAction(Action* action) {
  actionRegister_.registerAction(action);
}

/**
 * Call setup for all elements in the ActionRegister.
 *
 * @param blockRegister Reference to the block::BlockRegister.
 */
void Sublist::setup(const block::BlockRegister& blockRegister) {
  actionRegister_.setupActions(blockRegister);
}

}  // namespace nastja
