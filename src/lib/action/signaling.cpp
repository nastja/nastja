/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "signaling.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/math/calculatesurface.h"
#include "lib/math/calculatevolume.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <memory>

namespace nastja {

using namespace config::literals;

void Signaling::init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) {
  auto& data   = getSimData().initData<DataCells>();
  auto& config = getSimData().getConfig();

  /// @key{CellsInSilico.signaling.functions, FactorySignal[]}
  /// Register these signaling functions.
  config.registerFunctions(signalFunctions_, factorySignal_, "CellsInSilico.signaling.functions"_jptr);
  signalFunctions_.init(data, config, getSimData());

  /// @key{CellsInSilico.signaling.steps, int, 1}
  /// Each n-th steps execute the signaling.
  eachNthStep = config.getValue<int>("CellsInSilico.signaling.steps"_jptr, 1);

  /// @key{CellsInSilico.signaling.constant, bool, false}
  /// Flag to enable constant signaling (turns off the signalling sweep if true).
  useConstantSignaling_ = config.template getValue<bool>("CellsInSilico.signaling.constant"_jptr, false);
}

void Signaling::executeBlock(block::Block* block) {
  if ((getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0) return;
  auto& data      = getSimData().getData<DataCells>();
  auto& container = block->getDataContainer<CellBlockContainer>();

  signalFunctions_.execute(data, container);
}

void Signaling::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
