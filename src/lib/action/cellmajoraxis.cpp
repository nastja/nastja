/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellmajoraxis.h"
#include "lib/field/field.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C8asym.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/direction.h"
#include "lib/storage/sparestorage.h"
#include <array>
#include <algorithm>

#include <vector>


namespace nastja {

using namespace config::literals;
using namespace stencil;

//template <unsigned int DIM>
using DxC26 = typename std::conditional<3 == 3, D3C26, D3C26>::type; //TODO make this actually dependent on the dimension


/**
   * Corrects for a possible periodic boundary jump
   *
   * @param block The block
   * @param position The vector to be checked
   */
  void checkForBoundaryJump(block::Block* block, math::Vector3<real_t>& position){
    auto systemSize = static_cast<math::Vector3<long int>>(block->size * block->blocks);
    for (size_t i=0; i< 3;++i){
      if (position[i] > systemSize[i]/2){//NOT block size, we need the overall system size
        position[i] -= systemSize[i];
      }
      else if (position[i] < (-1*systemSize[i]/2)){
        position[i] += systemSize[i];
      }
    }
  }


void CellMajorAxis::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  /// @key{CellsInSilico.polarity.recalculationtime, int, 1}
  /// Number of each n-th steps when the polarity vector is updated.
  eachNthStep = getSimData().getConfig().getValue<int>("CellsInSilico.polarity.recalculationtime"_jptr, 1);


  /// @key{CellsInSilico.polarity.mode, std::string, "random"}
  /// The mode of the polarity update
  std::string mode = getSimData().getConfig().getValue<std::string>("CellsInSilico.polarity.mode"_jptr, "randomWalkOnSphere");

  /// @key{CellsInSilico.polarity.epsilon, real_t, 0.01}
  /// The strength of the polarity vector update when mode is set to randomWalkOnSphere, voxelUpdate or signalGrowth
  epsilon = getSimData().getConfig().getValue<real_t>("CellsInSilico.polarity.epsilon"_jptr, 0.01);

  /// @key{CellsInSilico.polarity.lambda, ParameterStorage<real_t>}
  /// The polarity lambda for each cell type
  lambdaPolarity_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.polarity.lambda"_jptr, getSimData().getConfig());


  /// @key{CellsInSilico.polarity.polaritychangestart, long unsigned int}
  /// The start time after which typechange can occur.
  startTime = getSimData().getConfig().getValue<long unsigned int>("CellsInSilico.polarity.polaritychangestart"_jptr, 1000UL);  


  /// @key{CellsInSilico.polarity.growthSignal, int}
  /// The signal used for polarity change along gradient
  growthSignal = getSimData().getConfig().getValue<int>("CellsInSilico.polarity.growthSignal"_jptr, 0);  



  if (mode == "random") {
    polarityMode_ = polarityMode::random;
  } else if (mode == "randomWalkOnSphere") {
    polarityMode_ = polarityMode::randomWalkOnSphere;
  } else if (mode == "voxelUpdate") {
    polarityMode_ = polarityMode::voxelUpdate;    
  } else if (mode == "signalGrowth") {
    polarityMode_ = polarityMode::signalGrowth;    
  }
  else{
    polarityMode_ = polarityMode::__invalid_type;
  }

  //Fill cellTypesToExchange_
  auto& lambdaPolarity = lambdaPolarity_->getStorage();
  for (cellid_t i = 0; i < lambdaPolarity.getSize(); ++i){
    if (lambdaPolarity.getValue(i) > 0){
      cellTypesToExchange_.insert(i);
    }
  }
}

/**
 * Calculate map containing thee partial polarity vectors in the local block
 *
 * @param block Pointer to the block.
 * @param localPolarities The map to be filled
 */
void CellMajorAxis::calcLocalPolarities(block::Block* block, std::map<cellid_t,std::vector<real_t>>& localPolarities){
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& field = block->getFieldByName("cells")->get<cellid_t>();
  auto& data      = getSimData().getData<DataCells>();
  auto view  = field.createView(field.getInnerBoundingBox());
  auto systemSize = static_cast<math::Vector3<long int>>(block->size * block->blocks);
  math::Vector3<real_t> referenceAxis{1,0,0};//This can be an arbitrary vector, but it has to be the same for each block that has cells of ID cellID!


  for ( auto it = view.begin(); it != view.end(); ++it){ //Sweep over entire field to get local polarities
    auto& currentVoxel = **it; 
    if(currentVoxel <= data.liquid ) continue;

    if (cellTypesToExchange_.find(container.cells(currentVoxel).type) == cellTypesToExchange_.end()) continue;//We only exchange cells with lambdaPolarity > 0

    if ( localPolarities.find(currentVoxel) == localPolarities.end() ){
      localPolarities[currentVoxel] = std::vector<real_t>{0.,0.,0.,0.}; //Entries: x,y,z,count, where count is the number of voxels in the block of the respective cellID
    }
    
    math::Vector3<real_t> centerVoxelPosition{
                        static_cast<real_t>(block->offset[0] + it.coordinates().x()-1),
                        static_cast<real_t>(block->offset[1] + it.coordinates().y()-1),
                        static_cast<real_t>(block->offset[2] + it.coordinates().z()-1)};
    math::Vector3<real_t> magnitudeVec {centerVoxelPosition - container.cells(currentVoxel).center};
    //Check for periodic boundaries
    for (size_t i=0; i< 3;++i){
      if (magnitudeVec[i] > systemSize[i]/2){
        magnitudeVec[i] -= systemSize[i];
      }
      else if (magnitudeVec[i] < (-1*systemSize[i]/2)){
        magnitudeVec[i] += systemSize[i];
      }
    }
    for (int i=0; i<3; ++i){
      localPolarities[currentVoxel][i]+= magnitudeVec[i];
    } 
    ++localPolarities[currentVoxel][3];
  }
}


/**
 * Exchange polarity vectors between neighboring blocks
 *
 * @param localPolarities The map containing the partial polarity vectors in the local block
 * @param receivedPolarities The vector containing the polarity vector maps from all neighbors of the local block
 */
void CellMajorAxis::exchangePolarities(block::Block* block, std::map<cellid_t, std::vector<real_t>>& localPolarities, std::vector<std::map<cellid_t, std::vector<real_t>>>& receivedPolarities) {
 
  int MPI_size{};
  MPI_Comm_size(MPI_COMM_WORLD,&MPI_size);
  int MPI_rank{};
  MPI_Comm_rank(MPI_COMM_WORLD,&MPI_rank);


  auto& field = block->getFieldByName("cells")->get<cellid_t>();
  int neighborRank{};
  cellid_t tag{};

  MPI_Request request_{};


  std::vector<cellid_t> neighborCellIDCounts{};//(N_directions,0);
  std::map<cellid_t, std::vector<cellid_t>> neighborIDMap{};

  auto const localCellNumber{localPolarities.size()}; //IMPORTANT: LOCAL POLARITIES HAS TO BE FILLED USING ONLY THE FIELD. OTHERWISE IT MAY CONTAIN NEIGHBOR IDs
  std::vector<cellid_t> localCellIDs{};
  
  int NEntriesPerID{4};//Every 4 entries belong to one cellID
  std::vector<real_t> localPolarityVectorsSerialized{}; //Every 4 entries belong to one cellID

  for (auto const&  it : localPolarities){
    localCellIDs.push_back(it.first);
    for (auto const elem : it.second)
      localPolarityVectorsSerialized.push_back(elem);
  }

  std::vector<cellid_t> hasSent{};
  std::vector<cellid_t> hasRecvd{};
  //============================ SEND AND RECEIVE NUMBER OF CELLS IN LOCALPOLARITIES =========================================
  for (unsigned int dir : DxC26()){
      neighborRank = field.getNeighborRank(DxC26::dir[dir]);
      if (MPI_rank == neighborRank || std::find(hasSent.begin(),hasSent.end(),neighborRank) != hasSent.end()) continue;
      tag = MPI_rank;
      MPI_Issend(&localCellNumber,1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
      hasSent.push_back(neighborRank);
      // tag = neighborRank;
      // MPI_Irecv(&neighborCellIDCounts[dir],1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  for (unsigned int dir : DxC26()){
      neighborRank = field.getNeighborRank(DxC26::dir[dir]);
      if (MPI_rank == neighborRank || std::find(hasRecvd.begin(),hasRecvd.end(),neighborRank) != hasRecvd.end()) continue;
      // MPI_Issend(&N_cells,1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
      tag = neighborRank;
      neighborCellIDCounts.push_back(0);
      MPI_Recv(&neighborCellIDCounts[neighborCellIDCounts.size()-1],1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      hasRecvd.push_back(neighborRank);
  }    

  //============================ SEND AND RECEIVE POLARITY VECTORS =========================================
  hasSent.clear();
  hasRecvd.clear();
  for (unsigned int dir : DxC26()){
      neighborRank = field.getNeighborRank(DxC26::dir[dir]);
      if (MPI_rank == neighborRank || std::find(hasSent.begin(),hasSent.end(),neighborRank) != hasSent.end()) continue;
      tag = MPI_rank;
      MPI_Issend(&localCellIDs[0],localCellIDs.size(),MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
      tag = MPI_rank + MPI_size;
      MPI_Issend(&localPolarityVectorsSerialized[0],localPolarityVectorsSerialized.size(),MPI_NASTJA_REAL, neighborRank,tag,MPI_COMM_WORLD,&request_);      
      hasSent.push_back(neighborRank);
      // tag = neighborRank;
      // MPI_Irecv(&neighborCellIDCounts[dir],1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  int recvCount{0};
  for (unsigned int dir : DxC26()){
      neighborRank = field.getNeighborRank(DxC26::dir[dir]);
      if (MPI_rank == neighborRank || std::find(hasRecvd.begin(),hasRecvd.end(),neighborRank) != hasRecvd.end()) continue;
      std::vector<cellid_t> receivedCellIDs(neighborCellIDCounts[recvCount],0);
      std::vector<real_t> receivedPolarityVectorsSerialized(NEntriesPerID*neighborCellIDCounts[recvCount],0.);
      // MPI_Issend(&N_cells,1,MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,&request_);
      tag = neighborRank;
      MPI_Recv(&receivedCellIDs[0],neighborCellIDCounts[recvCount],MPI_NASTJA_INTEGER, neighborRank,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      tag = neighborRank + MPI_size;
      MPI_Recv(&receivedPolarityVectorsSerialized[0],NEntriesPerID*neighborCellIDCounts[recvCount],MPI_NASTJA_REAL, neighborRank,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      hasRecvd.push_back(neighborRank);
      receivedPolarities.push_back(std::map<cellid_t,std::vector<real_t>>{});
      for(size_t i=0; i<neighborCellIDCounts[recvCount]; ++i){
        std::vector<real_t> newVec{receivedPolarityVectorsSerialized[i*NEntriesPerID],
                                  receivedPolarityVectorsSerialized[i*NEntriesPerID + 1],
                                  receivedPolarityVectorsSerialized[i*NEntriesPerID + 2],
                                  receivedPolarityVectorsSerialized[i*NEntriesPerID + 3]};
        receivedPolarities[receivedPolarities.size()-1][receivedCellIDs[i]] = newVec;
      }
      ++recvCount;
  }    

}

/**
 * Detect active cells in the whole block
 *
 * @param block Pointer to the block.
 */
math::Vector3<real_t> CellMajorAxis::calcGradientPolarity(block::Block* block, cellid_t const& cellID) {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();
  math::Vector3<real_t> newPolarity{0,0,0};

  real_t radiusToCheck{20};//TODO Make this dependent on cell volume OR a constant from config!
  container.periodicRadiusSearch(container.cells(cellID).center, radiusToCheck, neighbors_);
  //for (auto& neighbor : neighbors_) {
  for (auto& elem : container.cells){
    cellid_t neighborID = elem.first;//neighbor.first;
    if (neighborID == cellID || neighborID < data.liquid) continue;
      math::Vector3<real_t> distVec{0,0,0};
      //TODO Fix casting issues
      
      for (int i = 0; i<3; ++i){
        distVec[i] = static_cast<real_t>(container.cells(neighborID).center[i]) - static_cast<real_t>(container.cells(cellID).center[i]);
      }

      checkForBoundaryJump(block,distVec);
      real_t absVal{sqrt(distVec[0]*distVec[0] + distVec[1]*distVec[1] + distVec[2]*distVec[2])};
      if (absVal > radiusToCheck) continue;
      distVec = distVec.normalize();
      newPolarity += epsilon * container.cells(neighborID).signal[growthSignal]*distVec;
  }
  if (neighbors_.size() > 0) newPolarity /= neighbors_.size();
  newPolarity += container.cells(cellID).polarity;
  if (newPolarity[0] != 0 || newPolarity[1] != 0 || newPolarity[2] != 0){
    newPolarity = newPolarity.normalize();
  }
  return newPolarity;
}

/**
 * Detect active cells in the whole block
 *
 * @param block Pointer to the block.
 */
void CellMajorAxis::executeBlock(block::Block* block) {


  auto& container = block->getDataContainer<CellBlockContainer>();
  if (container.getSimData().timestep < startTime) return;

  auto& data      = getSimData().getData<DataCells>();
  auto& generator = getSimData().generator;

  math::Vector3<real_t> newPol{0,0,0};

  if (polarityMode_ == polarityMode::voxelUpdate){

    int MPI_size{};//TODO Remove; only needed for printing here
    MPI_Comm_size(MPI_COMM_WORLD,&MPI_size);
    int MPI_rank{};
    MPI_Comm_rank(MPI_COMM_WORLD,&MPI_rank);

    math::Vector3<real_t> finalPolarity{};
    real_t numberOfVoxselsInCell{};
    std::vector<real_t> receivedPolarity(4,0);
    



    // std::vector<cellid_t> localCellIDs{};
    // std::vector<cellid_t> neighborCellIDCounts(N_directions,0);
    // std::map<cellid_t, std::vector<cellid_t>> neighborIDMap{};

    std::map<cellid_t,std::vector<real_t>> localPolarities{};    

    calcLocalPolarities(block,localPolarities);

    std::vector<std::map<cellid_t, std::vector<real_t>>> receivedPolarities{};

    exchangePolarities(block, localPolarities, receivedPolarities);

    int NVoxelsInCalc{0};
    int NVoxelsInAgents{0};
    for (auto it : localPolarities) {
      finalPolarity = {0.,0.,0.};
      numberOfVoxselsInCell = 0;
      cellid_t cellID = it.first;
      NVoxelsInAgents += container.cells(cellID).volume;
      auto polarity{it.second};
      numberOfVoxselsInCell += polarity[3];
      for (int i=0; i<3; ++i) finalPolarity[i]+= polarity[i];

      for (auto& elem : receivedPolarities){
      //for (unsigned int dir : DxC26()){
        if (elem.find(cellID) != elem.end()){
          numberOfVoxselsInCell += elem[cellID][3];
          NVoxelsInCalc += elem[cellID][3];
          for (int i=0; i<3; ++i) finalPolarity[i]+= elem[cellID][i];
        }
      }
      if (numberOfVoxselsInCell > 0.){
        finalPolarity /= numberOfVoxselsInCell;
      }
      if (finalPolarity[0] != 0. || finalPolarity[1] != 0. || finalPolarity[2] != 0.)
        finalPolarity = finalPolarity.normalize();
      for (int i=0; i<3;++i){
        if (finalPolarity[i] * container.cells(cellID).polarity[i] < 0) finalPolarity[i] *=(-1);//Orient polarity correctly
      }
      finalPolarity *= epsilon;
      finalPolarity += container.cells(cellID).polarity;
      finalPolarity = finalPolarity.normalize();
      container.cells(cellID).newPolarity = finalPolarity;
    }


  }
  else{

    for (auto& it : container.cells) {
      cellid_t cellID = it.first;
      if (cellID <= data.liquid) continue;

      auto& cell = it.second;


      const auto& center = cell.center;
      if (!(block->isGlobalCoordinateInBlock(center[0], center[1], center[2]))){
        continue;
      } 

      
      switch (polarityMode_) {
        case polarityMode::random :
          newPol = {probability_(generator),probability_(generator),probability_(generator)}; //New polarity vector update with random direction
          break;

        case polarityMode::randomWalkOnSphere:
          newPol = cell.polarity + epsilon * math::Vector3<real_t>{probabilityPlusMinus_(generator),probabilityPlusMinus_(generator),probabilityPlusMinus_(generator)}; //Due to later normalization, we remain on the surface of the unit sphere
          break;

        case polarityMode::signalGrowth:
          newPol = calcGradientPolarity(block,cellID);
          break;

        case polarityMode::__invalid_type: 
          throw std::invalid_argument(fmt::format("Invalid polarity mode. Please check your config file."));

        default: 
          throw std::invalid_argument(fmt::format("Invalid polarity mode. Please check your config file."));
      }
      
      if ( newPol != math::Vector3<real_t>{0., 0., 0.} ){  
        newPol = newPol.normalize();
      }
      cell.newPolarity = newPol;
    }
  }
}

void CellMajorAxis::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
