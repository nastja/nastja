/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/io/writerreader.h"

namespace nastja {

/**
 * Class for writing a dump.
 *
 * @ingroup action
 */
class Undump : public WriterReader {
public:
  explicit Undump()
      : WriterReader{"dump"} {}
  ~Undump() override        = default;
  Undump(const Undump&)     = delete;
  Undump(Undump&&) noexcept = default;
  Undump& operator=(const Undump&) = delete;
  Undump& operator=(Undump&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) override {}
  void executeBlock(block::Block* /*block*/) override{};
  void execute(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return false; }
};

}  // namespace nastja
