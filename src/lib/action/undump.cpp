/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "undump.h"
#include "lib/timing.h"
#include "lib/block/blockmanager.h"
#include "lib/field/field.h"
#include "lib/io/writerreader.h"
#include <cerrno>
#include <cstdio>

namespace nastja {

void Undump::execute(const block::BlockRegister& blockRegister) {
  int dumpCnt = getSimData().getCmdLine().resumeFrame;

  std::string filename = makeFilename(getFieldname(), "dump", dumpCnt, getSimData().mpiData.getRank());

  FILE* fp = fopen(filename.c_str(), "r");

  if (fp == nullptr) {
    perror(filename.c_str());
  }
  Archive ar;

  // undump simdata
  long arLength;
  if (!fread(&arLength, sizeof(long), 1, fp)) throw std::runtime_error("Can't read from file.");
  ar.reset();
  ar.resize(arLength);
  if (!fread(ar.data(), arLength, 1, fp)) throw std::runtime_error("Can't read from file.");
  getSimData().undump(ar);

  logger::get().onelog("Resume from dump {} timestep: {:7}", filename.c_str(), getSimData().timestep);

  for (const auto& block : blockRegister) {
    for (const auto& field : block.second->fields.poolSet) {
      auto* ptr = field.second->getCharBasePtr(field.second->getProperties().getNumberOfTimeStepFields() - 1);
      auto size = field.second->getDataSize();

      // dump field
      if (!fread(ptr, size, 1, fp)) throw std::runtime_error("Can't read from file.");

      // dump field container
      long length;
      if (!fread(&length, sizeof(long), 1, fp)) throw std::runtime_error("Can't read from file.");
      ar.reset();
      if (length > 0) {
        ar.resize(length);
        if (!fread(ar.data(), ar.size(), 1, fp)) throw std::runtime_error("Can't read from file.");
        field.second->getDataContainerPool().undump(ar);
      }
    }

    // dump block containers
    long length;
    if (!fread(&length, sizeof(long), 1, fp)) throw std::runtime_error("Can't read from file.");
    ar.reset();
    if (length > 0) {
      ar.resize(length);
      if (!fread(ar.data(), ar.size(), 1, fp)) throw std::runtime_error("Can't read from file.");
      block.second->getDataContainerPool().undump(ar);
    }
  }

  fclose(fp);
}

}  // namespace nastja
