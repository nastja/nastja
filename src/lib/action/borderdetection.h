/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/stencil/direction.h"

namespace nastja {

struct ReachedBoundaryEnum {
  enum ReachedBoundary {
    none = 0,
    full,
    empty
  };
};
using ReachedBoundary = ReachedBoundaryEnum::ReachedBoundary;

class BorderDetection : public Action {
public:
  explicit BorderDetection(const std::string& fieldname, const unsigned int stencil = 26)
      : numOfSides_{stencil} {
    registerFieldname(fieldname);
  }

  ~BorderDetection() override                 = default;
  BorderDetection(const BorderDetection&)     = delete;
  BorderDetection(BorderDetection&&) noexcept = default;
  BorderDetection& operator=(const BorderDetection&) = delete;
  BorderDetection& operator=(BorderDetection&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  void executeSide(field::IField& field, stencil::Direction side);
  static ReachedBoundary detectReachedBoundary(field::Field<real_t, 1>& field, stencil::Direction side);

  const unsigned int numOfSides_;  ///< The number of sides this action has to look for depending on the stencil.
};

}  // namespace nastja
