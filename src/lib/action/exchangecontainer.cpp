/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "exchangecontainer.h"
#include "lib/utility.h"
#include "lib/field/field.h"
#include "lib/math/vector.h"
#include "lib/stencil/D3C26.h"
#include <algorithm>

namespace nastja {

using namespace config::literals;
using namespace stencil;

/**
 * Initializes the exchange range.
 *
 * @param blockRegister  The block register.
 *
 * @return True for full exchange, false otherwise.
 */
bool ExchangeContainer::initExchangeRange(const block::BlockRegister& blockRegister) {
  const auto& blocksize  = blockRegister.begin()->second->size;
  const auto& blockcount = blockRegister.begin()->second->blocks;

  if (blockcount == math::Vector3<long>::one) return false;  // No exchange, full exchange will unnecessarily pack.

  /// @key{CellsInSilico.exchangerange, int, 0}
  /// Additional cell data are exchanged within the given range, i.e., the cell center must be within this range. A
  /// value <= 0 indicates a full block exchange.
  exchangeRange_ = getSimData().getConfig().getValue<long>("CellsInSilico.exchangerange"_jptr, 0);

  if (exchangeRange_ <= 0) {
    exchangeRange_ = 0;
    return true;  // Full exchange.
  }

  for (unsigned int axis = 0; axis < 3; axis++) {
    if (blockcount[axis] == 2) {  // two blocks;
      if (2 * exchangeRange_ > static_cast<long>(blocksize[axis])) {
        throw std::invalid_argument("The ACD exchange range is too large; it overlaps for two blocks in one dimension. Please use the full ACD exchange.");
      }
    } else {
      if (exchangeRange_ > static_cast<long>(blocksize[axis])) {
        throw std::invalid_argument("The ACD exchange range must not be larger than the block size.");
      }
    }
  }

  return false;
}

void ExchangeContainer::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  if (initExchangeRange(blockRegister)) {
    exchangeState_.processEvent(ExchangeEvent::initfull);
  } else {
    exchangeState_.processEvent(ExchangeEvent::initpartial);
  }
}

unsigned int ExchangeContainer::getSkippedExchange(const field::IField& field) const {
  unsigned int bitset = 0;

  // look for MPI boundaries
  for (int i = 0; i < 6; i++) {
    if (!field.boundarySet[i]->isMPI() && !field.boundarySet[i]->isInner()) {
      bitset |= stencil::skipExchange[i];
    }
  }

  if (!exchangeState_.isFullExchange()) return bitset;

  for (int axis = 0; axis < 3; axis++) {
    if (!field.boundarySet[axis * 2]->isMPI() || !field.boundarySet[axis * 2 + 1]->isMPI()) continue;
    int size = field.getBlock().blocks[axis];
    if (size <= 2) {                                  // skip upper side
      bitset |= stencil::skipExchange[2 * axis + 1];  // odds are the upper sides
    }
    if (size <= 1) {                              // skip lower side, too
      bitset |= stencil::skipExchange[2 * axis];  // evens are the lower sides
    }
  }

  return bitset;
}

void ExchangeContainer::recv(field::IField& field, CellBlockContainer& container, const unsigned int mask) {
  int tag = static_cast<int>(field.getID()) + MPIExchangeContainerBase;
  int size{};
  MPI_Status status;

  std::bitset<26> skipSides(mask);
  unsigned long waitingSides = 26 - skipSides.count();

  while (waitingSides > 0) {
    MPI_Probe(MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_CHAR, &size);

    auto& recvPackedBuffer = container.getRecvArchive();
    recvPackedBuffer.reset();
    recvPackedBuffer.resize(size);
    void* recvPtr = recvPackedBuffer.data();

    MPI_Recv(recvPtr, size, MPI_CHAR, status.MPI_SOURCE, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    container.unpack(recvPackedBuffer);
    waitingSides--;
  }
}

void ExchangeContainer::recvContainers(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    auto& field     = *block.second->getFieldByName(getFieldname());
    auto& container = block.second->getDataContainer<CellBlockContainer>();

    unsigned int bitset = getSkippedExchange(field);

    recv(field, container, bitset);
  }
}

void ExchangeContainer::send(field::IField& field, CellBlockContainer& container, const Direction side) {
  int partnerRank = field.getNeighborRank(side);

  if (partnerRank == -1) return;

  int tag = field.getNeighborID(side) + MPIExchangeContainerBase;

  auto& ar = container.getSendArchive(exchangeState_.isFullExchange() ? 0 : D3C26::index[toUnderlying(side)]);

  auto* sendPtr = static_cast<void*>(ar.data());
  auto sendSize = static_cast<int>(ar.size());
  MPI_Issend(sendPtr, sendSize, MPI_CHAR, partnerRank, tag, MPI_COMM_WORLD, container.getSendRequest(D3C26::index[toUnderlying(side)]));
}

void ExchangeContainer::sendContainers(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    auto* field     = block.second->getFieldByName(getFieldname());
    auto& container = block.second->getDataContainer<CellBlockContainer>();

    MPI_Waitall(26, container.getSendRequest(), MPI_STATUSES_IGNORE);

    if (exchangeState_.isFullExchange()) {
      auto& ar = container.getSendArchive(0);
      ar.clear();
      container.packAll(ar);
    }

    unsigned int bitset = getSkippedExchange(*field);
    std::bitset<26> skipSides(bitset);
    for (auto i : D3C26()) {
      if (skipSides[i]) continue;

      if (!exchangeState_.isFullExchange()) {
        auto& ar = container.getSendArchive(i);
        ar.clear();
        container.packPartial(ar, D3C26::dir[i], exchangeRange_);
      }

      send(*field, container, D3C26::dir[i]);
    }

    container.cells.processDeltas();
  }
}

void ExchangeContainer::postReceive(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    auto& container = block.second->getDataContainer<CellBlockContainer>();

    container.postReceive();
  }
}

void ExchangeContainer::execute(const block::BlockRegister& blockRegister) {
  sendContainers(blockRegister);
  recvContainers(blockRegister);
  postReceive(blockRegister);

  exchangeState_.processEvent(ExchangeEvent::next);
}

}  // namespace nastja
