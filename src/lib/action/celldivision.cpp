/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "celldivision.h"
#include "lib/datatypes.h"
#include "lib/field/field.h"
#include "lib/math/surface.h"
#include "lib/math/vector.h"
#include "lib/math/volume.h"
#include "lib/simdata/datacells.h"
#include <cmath>
#include <memory>

namespace nastja {

using namespace config::literals;
using namespace math;

template <unsigned int DIM>
void CellDivision<DIM>::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();
  auto& config          = getSimData().getConfig();

  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);

    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }
  }

  /// @keyignore
  eachNthStep = config.template getValue<int>("CellsInSilico.centerofmass.steps"_jptr, 1);

  /// @key{CellsInSilico.division.halveSignals}
  /// Determines whether signal values are halved upon celldivision
  halveSignals = config.template getValue<bool>("CellsInSilico.division.halveSignals"_jptr, false);

  if (config.contains("CellsInSilico.division.mutationmatrix"_jptr)) {
    /// @key{CellsInSilico.division.mutationmatrix, ParameterStorage<real_t>, optional}
    /// Mutation matrix. Transition probabilites from and to each cell type after cell-division.
    /// Each row will be normalized.
    mutationMatrix_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.division.mutationmatrix"_jptr, config);
    mutationMatrix_->getStorage().normalizeRows();
  }

  if (config.contains("CellsInSilico.division.divisionmode"_jptr)) {
    /// @key{CellsInSilico.division.divisionmode, std::vector<std::string>, optional}
    /// Division mode. This determines how the plane of division is oriented. If this contains only a single entry, this is used for all cell types
    divisionModes_ = config.template getVector<std::string>("CellsInSilico.division.divisionmode"_jptr);
  }

  if (config.contains("CellsInSilico.division.centerOffsetFactor"_jptr)) {
    /// @key{CellsInSilico.division.centerOffsetFactor, std::vector<std::string>, optional}
    /// Offset factor for the plane of division. This determines, which fraction of the voxels belonging to the old cell are allocated to the new one. The actual offset is calculated as centerOffsetFactor*cellSphereRadius
    centerOffsetFactors_ = config.template getVector<real_t>("CellsInSilico.division.centerOffsetFactor"_jptr);
  }  

  /// @key{CellsInSilico.division.condition, string[]}
  /// @widget{condition}
  /// An evaluate string that determines the condition when a cell divides.
  auto conditions = config.template getVector<std::string>("CellsInSilico.division.condition"_jptr);
  cellEvaluator_  = std::make_unique<evaluate::ExpressionStorage<evaluate::CellExtension>>(getSimData(), "Cell type {} register division condition '{}'.");
  for (unsigned int i = 0; i < conditions.size(); i++) {
    if (conditions[i].empty()) {
      cellEvaluator_->addCondition(i, "false");
    } else {
      cellEvaluator_->addCondition(i, conditions[i]);
    }
  }
}




/**
 * Choose the mutated cell type.
 *
 * @param oldType  The old type type.
 *
 * @return The type of the mutated cell.
 */
template <unsigned int DIM>
unsigned int CellDivision<DIM>::chooseMutatedType(unsigned int oldType) {
  if (!mutationMatrix_) return oldType;  // no mutation

  auto& mutation = mutationMatrix_->getStorage();

  // calculate probabilities and return index.
  real_t prob = probability_(getSimData().generator);
  unsigned int index{};
  while (prob >= 0.0 && index < mutation.getSize()) {
    prob -= mutation.getValue(oldType, index);
    ++index;
  }
  if (index <= mutation.getSize() || mutation.getValue(oldType,index)) return index - 1;
  else return oldType;
}

/**
 * Returns the division mode of the current cell
 *
 * @param index The cell type
 */
template <unsigned int DIM>
typename CellDivision<DIM>::DivisionMode CellDivision<DIM>::determineDivisionMode(unsigned int const& index) {
      if (divisionModes_.size() == 0) return DivisionMode::random;
      else if (divisionModes_[index] == "random"){
        return DivisionMode::random;
      }
      else if (divisionModes_[index] == "polarityAxis"){
        return DivisionMode::polarityAxis;
      }
      else if (divisionModes_[index] == "longitudinalAxis"){
        return DivisionMode::longitudinalAxis;
      }
      else {
        throw std::invalid_argument (fmt::format("Unknown divisionMode: '{}'.", divisionModes_[index]));
      }
}

/**
 * Determine cells that gets divided and append information to the container.
 *
 * @param container  The container.
 * @param block      The block
 */
template <unsigned int DIM>
void CellDivision<DIM>::determineDivision(CellBlockContainer& container, block::Block* block) {
  auto& data = getSimData().template getData<DataCells>();

  // loop over all known cells, cells with a saved volume in this block.
  for (const auto& i : container.cells) {
    cellid_t cellID = i.first;

    if (cellID <= data.liquid) continue;

    const auto& cell   = i.second;
    const auto& center = cell.center;

    if (!(block->isGlobalCoordinateInBlock(center[0], center[1], center[2]))) continue;

    cellEvaluator_->set(cell);
    if (!cellEvaluator_->condition(cell.type)) continue;

    // normal in spherical coordinate system, discretized by 1e6.
    real_t theta{};
    real_t phi{};


    auto index = divisionModes_.size() > 1 ? cell.type : 0;
    switch ( CellDivision<DIM>::determineDivisionMode(index) ) {
      case DivisionMode::polarityAxis:
        theta = acos(cell.polarity[2]); //TODO this is sort of redundant, because later we calculate the plane normal vector using theta and phi... 
        phi = atan2(cell.polarity[1],cell.polarity[0]);
        break;

      case DivisionMode::longitudinalAxis:
        {
        math::Vector3<real_t> normalVec{uniform_perturbation_(getSimData().generator),uniform_perturbation_(getSimData().generator),uniform_perturbation_(getSimData().generator)};
        normalVec += cell.polarity;
        normalVec /= sqrt(normalVec[0]*normalVec[0] + normalVec[1]*normalVec[1] + normalVec[2]*normalVec[2]);
        normalVec = math::crossProduct(cell.polarity,normalVec);//normalVec is used for two purposes (did not want second variable)
        theta = acos(normalVec[2]); //TODO this is sort of redundant, because later we calculate the plane normal vector using theta and phi... 
        phi = atan2(normalVec[1],normalVec[0]);        
        break;
        }
        
      case DivisionMode::random:
        theta = uniform_angle_(getSimData().generator);
        phi = uniform_angle_(getSimData().generator) * 2.0;
        break;

      default:
        theta = 0.;
        phi = 0.;      
        break;

    }

    if (container.reservedCellID > container.reservedCellIDMax) {
      logger::get().warning("Maximum reserved cellID reached: {}", container.reservedCellID);
    }
    cellid_t newID = container.reservedCellID;
    container.reservedCellID++;

    // This part mutates the cells to another (tumor) cell type with a probability of p_mutation
    int oldType                = container.cells(cellID).type;
    int newType                = chooseMutatedType(oldType);
    container.division[cellID] = {theta, phi, newID, newType};
    logger::get("cellevents").info(R"("block": {}, "event": "division", "cell": {}, "childA": {}, "typeA": {}, "childB": {}, "typeB": {})", block->id, cellID, cellID, oldType, newID, newType);
  }
}

/**
 * if information about a divided cell is received then sweep over the field and divide the cell and reinitialize the
 * surface and volume
 *
 * @param field      The field.
 * @param container  The container.
 * @param block      The block.
 */
template <unsigned int DIM>
void CellDivision<DIM>::divideReceivedCells(field::Field<cellid_t, 1>& field, CellBlockContainer& container, block::Block* block) {
  auto& data = getSimData().template getData<DataCells>();

  for (const auto& divCell : container.division) {
    if (divCell.first > data.liquid && container.cells.contains(divCell.first)) {
      divideCell(field, container, divCell.first, divCell.second.newID, block);
      reinitializeCell(field, container, divCell.first, divCell.second.newID, divCell.second.newType);
    }
  }

  container.division.clear();
}

/**
 * Divides a single cell.
 *
 * @param field      The field.
 * @param container  The container.
 * @param oldID      The old cellID.
 * @param newID      The new cellID.
 * @param block      The block.
 */
template <unsigned int DIM>
void CellDivision<DIM>::divideCell(field::Field<cellid_t, 1>& field, CellBlockContainer& container, cellid_t oldID, cellid_t newID, block::Block* block) {
  Vector3<real_t> center{container.cells(oldID).center};
  // check for periodic moved center
  for (unsigned int i = 0; i < DIM; i++) {
    int sizeDim = block->size[i] * block->blocks[i];
    if (block->isLowerBound(i) && 2 * center[i] - static_cast<int>(block->size[i]) > sizeDim) {
      center[i] -= sizeDim;
    }
    if (block->isUpperBound(i) && 2 * center[i] + static_cast<int>(block->size[i]) < sizeDim) {
      center[i] += sizeDim;
    }
  }

  real_t theta = container.division(oldID).theta;
  real_t phi   = container.division(oldID).phi;

  Vector3<real_t> n = {std::sin(theta) * std::cos(phi),
                       std::sin(theta) * std::sin(phi),
                       std::cos(theta)};  

  // recolor the old cell ID (also in the halo).
  auto view = field.createView(field.getOuterBoundingBox());
  auto index = divisionModes_.size() > 1 ? container.cells[oldID].type : 0;
  auto divisionMode = CellDivision<DIM>::determineDivisionMode(index);

  real_t cellSphereRadius = std::cbrt(0.75 * container.cells(oldID).volume * M_1_PI);
  real_t centerOffset = centerOffsetFactors_.size() > 0 ? centerOffsetFactors_[container.cells(oldID).type] * cellSphereRadius : 0.; 
  for (auto it = view.begin(); it != view.end(); ++it) {
    cellid_t& currentCell = view.get(*it);

    if (currentCell != oldID) continue;

    // Voxels above plane become part of new cell.
    //TODO find better way to use these variables (We really need to use real_t, not int here, and implicit conversion is giving some problems!)
    Vector3<real_t> coordinatesReal{it.coordinates() + block->offset};
//    Vector3<real_t> offsetReal{block->offset};
    switch ( divisionMode ) {

      case CellDivision<DIM>::DivisionMode::polarityAxis:

        if (dotProduct(n, coordinatesReal - (center + centerOffset*container.cells(currentCell).polarity)) > 0) {
          currentCell = newID;
        }
        break;

      default:
        if (dotProduct(n, coordinatesReal - center) > 0) {
          currentCell = newID;
        }      
        break;

    }
  }



  switch ( divisionMode ){
    case CellDivision<DIM>::DivisionMode::random:
      // Approximate new center by the center of a half sphere
      container.cells[oldID].center -= 0.375 * cellSphereRadius * n;
      container.cells[newID].center += 0.375 * cellSphereRadius * n;
      container.cells(newID).polarity = container.cells(oldID).polarity;      
      break;
    case CellDivision<DIM>::DivisionMode::polarityAxis:
      // Move both centers farther away from each other, as these cells are elongated
      container.cells[oldID].center -= 0.375 * cellSphereRadius * n;
      container.cells[newID].center += 0.375 * cellSphereRadius * n;      
      container.cells(newID).polarity = container.cells(oldID).polarity;
      break;
    case CellDivision<DIM>::DivisionMode::longitudinalAxis:
      // Move only one center.
      container.cells[newID].center += 1.5*cellSphereRadius * n;      
      container.cells(newID).polarity = n;
      break;

    default:
    break;
  }

  container.cells(newID).birth = container.getSimData().timestep;
  container.cells(oldID).birth = container.getSimData().timestep;

  //Increase the generation for both cells
  container.cells(oldID).generation += 1;
  container.cells(newID).generation = container.cells(oldID).generation;


  
  if (halveSignals){
    for (size_t i = 0; i < numberOfSignals; ++i) {  // halve the signals
    container.cells(oldID).signal[i] *= 0.5;
    }
  }
  container.cells(newID).signal = container.cells(oldID).signal;
}

/**
 * Reinitializes the divided cells.
 *
 * @param field      The field.
 * @param container  The container.
 * @param oldID      The old ID.
 * @param newID      The new ID.
 * @param newType    The new type.
 *
 * @tparam DIM  The dimension.
 */
template <unsigned int DIM>
void CellDivision<DIM>::reinitializeCell(field::Field<cellid_t, 1>& field, CellBlockContainer& container, cellid_t oldID, cellid_t newID, int newType) {
  container.cells(oldID).clearGeometry();
  container.cells(oldID).clearDeltas();

  container.cells(newID).type          = newType;
  container.cells(newID).voxelsInBlock = 0;

  auto view = field.createView();
  auto& cellOld = container.cells(oldID);
  auto& cellNew = container.cells(newID);

  for (auto* voxel : view) {
    cellid_t currentCell = view.get(voxel);

    cellOld.deltaSurface += (*container.surface)(view, voxel, oldID);
    cellNew.deltaSurface += (*container.surface)(view, voxel, newID);

    cellOld.deltaVolume += (*container.volume)(view, voxel, oldID);
    cellNew.deltaVolume += (*container.volume)(view, voxel, newID);

    if (currentCell != oldID && currentCell != newID) continue;
    container.cells(currentCell).voxelsInBlock++;
  }
}

template <unsigned int DIM>
void CellDivision<DIM>::executeBlock(block::Block* block) {
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& field     = block->getFieldByName(getFieldname())->template get<cellid_t>();

  divideReceivedCells(field, container, block);

  // TODO Check if CenterOfMass is there
  // if (getSimData().timestep % 100 != 1) return; // Every 100th timestep after cell center exchange: delete single
  // left pixels, NOT part of the Potts model
  determineDivision(container, block);
}

template <unsigned int DIM>
void CellDivision<DIM>::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

template class CellDivision<3>;
template class CellDivision<2>;

}  // namespace nastja
