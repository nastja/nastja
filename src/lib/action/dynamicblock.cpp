/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @class DynamicBlock
 * Create and remove dynamically blocks.
 *
 * Beside the normal halo layer communication, this action holds a local communication group for each block.  Depending on
 * the stencil, a star of local neighbor blocks build this communicator.  For the simplest stencil, which uses all first
 * neighbors, the communicator star holds all first neighbors of the first neighbors.
 *
 * # How does this work?
 * The inner boundary marks the need of new blocks on a specific side.  This is stored in the additional
 * DynamicBlockContainer for each block. The DynamicBlock communication is modeled as simple send/receives communications.
 * In detail, the sending process collects the necessary information and sends this to each process that needs this
 * information.  This information is collected on process level, so each source communicator has a list of blocks that
 * have information for one or more blocks on the target process.  The information, collected for each communicator is
 * sent to the target.  On the target side a probe is looking for new messages from any source.  It collects the messages
 * if there are any and marks the communicator _has received_.  The communication is symmetric (in general), A sends
 * information to B and A waits for the information from B.
 *
 * The first part of the message collect information of each block from the sending side which is inside of any star on
 * the target side.  This message consists of the blockID followed by an integer of flags determining the side of a new
 * block or the deletion or even zero if there is no change.
 *
 * On the receiving side, a list of the new blocks is built from this information `getMapOfNewBlocks()`.  This also
 * calculates the neighbors for each new block and lookup the rank of the neighbors.  Additionally  a check for unknown
 * blocks that sends some information is done, if any is found the neighbors will be updated with this information.
 * Subsequently the local new blocks are determined from the full list of new block `getVectorOfLocalNewBlocks()`.  The
 * first side with an existing block wins and the new block will be created on this process.  So a list of all (knowing)
 * new blocks and a second list of all local new blocks are created.  Then all local blocks are updated with the new ID
 * and rank information `updateBlocks()`.  Finally the BlockManager creates the new local blocks
 * `BlockManager::createNewBlocks()`.
 *
 * # Global consistence
 *
 */

#include "dynamicblock.h"
#include "lib/logger.h"
#include "lib/communication/message.h"
#include "lib/stencil/D3C26.h"
#include <algorithm>
#include <bitset>
#include <iterator>
#include <memory>
#include <set>
#include <sstream>
#include <unistd.h>

namespace nastja {

using namespace block;
using namespace stencil;

/**
 * Initialized additional DynamicBlockContainer object to the given Block.
 *
 * @param block Pointer to block.
 */
void DynamicBlock::init(block::Block* block, const Pool<field::FieldProperties>& /*fieldpool*/) {
  block->registerDataContainer<DynamicBlockContainer>();
}

void DynamicBlock::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
}

/**
 * Register all blocks at the communicators.
 *
 * @param blockRegister The map of all blocks.
 */
void DynamicBlock::setup(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    commManager_.setupCommunicator(block.second);
  }

  commManager_.setupConnections();
}

/**
 * Reset the additional data inside of the DynamicBlockContainer object for all blocks.
 */
void DynamicBlock::cleanAllBlockData() const {
  for (const auto& block : blockmanager_.getBlockSet()) {
    auto& data = block.second->getDataContainer<DynamicBlockContainer>();
    data.boundaryDetection.reset();
  }
}

std::map<int, BlockTuple> DynamicBlock::getNewBlocks() {
  std::map<int, BlockTuple> newBlocks;

  for (const auto& comm : commManager_.commSet) {
    int start;
    int end;

    std::tie(start, end) = Message::findMessage(comm.second->recvBuffer, MessageID::newBlocks);
    if (start == -1) continue;

    std::set<int> commList;
    int cntComms = comm.second->recvBuffer[start++];
    commList.insert(&(comm.second->recvBuffer[start]), &(comm.second->recvBuffer[start + cntComms]));
    start += cntComms;

    for (int i = start; i < end; i += 3) {
      unsigned int newID = comm.second->recvBuffer[i];
      unsigned int mask  = comm.second->recvBuffer[i + 1];
      int rank           = comm.second->recvBuffer[i + 2];

      if ((mask & 0x01) != 0u) continue;  // skip delete blocks

      int newType = mask & 0x02;
      if (newBlocks.find(newID) != newBlocks.end()) {
        // Found previous new block with same ID.
        auto& preList = std::get<BlockTupleIndex::commList>(newBlocks[newID]);
        preList.insert(commList.begin(), commList.end());
        int newRank = std::get<BlockTupleIndex::rank>(newBlocks[newID]);
        if (rank != newRank) {
          throw std::runtime_error(fmt::format("Got new Block {} with two different target ranks ({}, {}).", newID, rank, newRank));
        }
      } else {
        newBlocks[newID] = std::make_tuple(rank, newType, comm.first, commList);
      }
    }
  }

  return newBlocks;
}

/**
 * Create a set with the ID of all deleted blocks.
 *
 * @return A set of deleted blocks.
 */
std::set<int> DynamicBlock::getDeletedBlocks() {
  std::set<int> deletedBlocks;

  for (const auto& comm : commManager_.commSet) {
    int start;
    int end;
    std::tie(start, end) = Message::findMessage(comm.second->recvBuffer, MessageID::delBlocks);
    if (start == -1) continue;

    for (int i = start; i < end; i += 2) {
      unsigned int id   = comm.second->recvBuffer[i];
      unsigned int mask = comm.second->recvBuffer[i + 1];

      std::bitset<32> bits(mask);
      if (bits[0]) {
        deletedBlocks.insert(id);
      }
    }
  }

  return deletedBlocks;
}

/**
 * Create an int-vector pair with all IDs of new blocks on the local rank, paired with the block type.
 *
 * @param newBlockInfo Vector of ID, rank pair.
 *
 * @return int-vector pair of block ID and type.
 */
VectorPairInts DynamicBlock::getLocalNewBlockInfo(const std::map<int, BlockTuple>& newBlockInfo) const {
  VectorPairInts result;
  for (auto& block : newBlockInfo) {
    // is block on the local rank and does the block not exist
    if (std::get<BlockTupleIndex::rank>(block.second) == blockmanager_.getMPIRank() && blockmanager_.getBlockSet().find(block.first) == blockmanager_.getBlockSet().end()) {
      result.emplace_back(block.first, std::get<BlockTupleIndex::type>(block.second) >> 1);
    }
  }

  return result;
}

/**
 * flatten a pair-vector to an int-vector
 *
 * @param pairs a vector of paired ints
 *
 * @return int-vector
 */
std::vector<int> DynamicBlock::flatten2Vector(const VectorPairInts& pairs) {
  std::vector<int> result;

  for (auto& i : pairs) {
    result.push_back(i.first);
  }

  return result;
}

void DynamicBlock::dbgPrintNewBlocks(const std::map<int, BlockTuple>& newBlockInfo) {
  auto key_selector = [](auto pair) { return pair.first; };

  std::vector<int> keys(newBlockInfo.size());
  transform(newBlockInfo.begin(), newBlockInfo.end(), keys.begin(), key_selector);

  std::stringstream result;
  std::copy(keys.begin(), keys.end(), std::ostream_iterator<int>(result, " "));
  logger::get().trace(logger::Category::DynNewBlk, "new blocks: {}", result.str());
}

void DynamicBlock::dbgPrintDeletedBlocks(std::set<int>& delBlocks) {
  std::stringstream result;
  std::copy(delBlocks.begin(), delBlocks.end(), std::ostream_iterator<int>(result, " "));
  logger::get().trace(logger::Category::DynDelBlk, "delete blocks: {}", result.str());
}

void DynamicBlock::dbgPrintLocalNewBlocks(std::vector<int> localNewBlocks) {
  std::stringstream result;
  std::copy(localNewBlocks.begin(), localNewBlocks.end(), std::ostream_iterator<int>(result, " "));
  logger::get().trace(logger::Category::DynNewBlkL, "Local new blocks: {}", result.str());
}

/**
 * Checks the newBlocks and append to the Communicators announce or incoming.
 * <pre>
 * +-----------+ not local  +--------------+ no    +----------------+ no  +------+
 * | New Block +----------->+ comm exist   +------>+ InNeighborhood +---->+ omit |
 * ++----------+ local &&   ++-------------+       ++---------------+     +------+
 *  | local      my Comm     | yes                  | yes
 *  v                        v                      v
 * ++------------+          ++-------------+       ++---------------+
 * | forall Comm +-+        | ADD Incoming |       | Create Comm    |
 * +-------------+ |        +--------------+       | ADD Incoming   |
 *  | in List      | not in                        +----------------+
 *  v              | List
 * ++------------+ |        +--------------+
 * | done        | +------->+ ADD Announce |
 * +-------------+          +--------------+
 * </pre>
 *
 * @param newBlockInfo Info about the new Blocks
 */
void DynamicBlock::registerBlocks(std::map<int, BlockTuple>& newBlockInfo) {
  for (const auto& newBlock : newBlockInfo) {
    int id   = newBlock.first;
    int rank = std::get<BlockTupleIndex::rank>(newBlock.second);

    if (rank == blockmanager_.getMPIRank()) {
      // int source = std::get<BlockTupleIndex::source>(newBlock.second);
      for (const auto& comm : commManager_.commSet) {
        if (!comm.second->isConnected()) continue;

        if (std::get<BlockTupleIndex::commList>(newBlock.second).count(comm.first) == 0) {  // USE local send to comm List
                                                                                            // if (!commManager.isInKnownComm(comm.first, source) || commManager.isInAnnounce(comm.first, source)) {
          comm.second->appendAnnounceBlock(id);
        }
      }
    }
    commManager_.appendIncomingBlock(id, rank);
  }
}

void DynamicBlock::commRemoveBlocks(std::set<int>& delBlocks) {
  for (const auto& blockID : delBlocks) {
    commManager_.removeBlock(blockID);
  }
}

/**
 * Gives the rank for a new to created block
 *
 * @param newID ID of the block
 *
 * @return the rank of the neighbor side with the least blocks or -1.
 */
int DynamicBlock::getRankOfNewBlock(const unsigned int newID) const {
  auto ranks                    = commManager_.getNeighborRanks(newID, numOfSides_);
  unsigned int smallestBlockCnt = -1;
  int rank                      = -1;
  for (auto& i : ranks) {
    if (i >= 0) {
      int blockCnt = commManager_.getLoad(i);
      if (blockCnt >= 0 && ((unsigned int)blockCnt < smallestBlockCnt || ((unsigned int)blockCnt == smallestBlockCnt && i < rank))) {
        smallestBlockCnt = blockCnt;
        rank             = i;
      }
    }
  }

  return rank;
}

/**
 * Prepare broadcast messages to all Communication objects.
 */
void DynamicBlock::prepareMessage() {
  auto& delBlocks       = commManager_.getDelBlocks();
  auto& newBlocksByRank = commManager_.getNewBlocksByRank();

  delBlocks.clear();
  newBlocksByRank.clear();

  for (const auto& block : blockmanager_.getBlockSet()) {
    auto& data = block.second->getDataContainer<DynamicBlockContainer>();

    if (data.boundaryDetection[0]) {                       // delete flag
      int delType = (1 | data.boundaryDetection[1] << 1);  // bit 0: is deleted
                                                           // bit 1: is a full block
      delBlocks.emplace_back(block.first, delType);
    } else {
      for (unsigned int side = 0; side < numOfSides_; side++) {
        if (data.boundaryDetection[2 * (side + 1)]) {
          int newID   = blockmanager_.getRelativeBlockID(block.first, D3C26::dir[side]);
          int newRank = getRankOfNewBlock(newID);
          int newType = data.boundaryDetection[2 * (side + 1) + 1];
          newType <<= 1;

          newBlocksByRank[newRank].emplace_back(newID, newType);
        }
      }
    }
  }

  commManager_.findFastCommunications(newBlocksByRank);
}

/**
 * Execute the DynamicBlock action.
 */
void DynamicBlock::execute(const block::BlockRegister& /*blockRegister*/) {
  commManager_.setLocalLoad(commManager_.calcLocalLoad());
  commManager_.waitCommAllSend();
  prepareMessage();
  commManager_.appendMessages();
  commManager_.clearBufferedFCS();
  commManager_.send();

  // reset block information
  cleanAllBlockData();

#ifndef NDEBUG
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  commManager_.recv();

#ifndef NDEBUG
  MPI_Barrier(MPI_COMM_WORLD);
#endif

  commManager_.clearBufferedMessages();

  // find new blocks
  std::map<int, BlockTuple> newBlockInfo = getNewBlocks();
  VectorPairInts localNewBlockInfo       = getLocalNewBlockInfo(newBlockInfo);  // TODO: new local blocks already exist here?
  std::vector<int> localNewBlocks        = flatten2Vector(localNewBlockInfo);

  std::set<int> delBlocks = getDeletedBlocks();

  messageManager_.send(localNewBlocks);

  registerBlocks(newBlockInfo);

  blockmanager_.createNewBlocks(localNewBlockInfo, commManager_);

  commManager_.updateBlocks();
  commManager_.updateNeighbors();

  dbgPrintNewBlocks(newBlockInfo);
  dbgPrintLocalNewBlocks(localNewBlocks);
  dbgPrintDeletedBlocks(delBlocks);

  if (!delBlocks.empty()) {
    commRemoveBlocks(delBlocks);
    blockmanager_.deleteBlocks(delBlocks);
  }
  commManager_.cleanUnusedComms();

  BlockRankSet blockList = messageManager_.recv(localNewBlocks);
  commManager_.updateCommunications(blockList);

  // LoadBalance
  commManager_.getCorrectedOfferedSpareBlocks(newBlockInfo);  // TODO: only call for LoadBalancing

  commManager_.logCommunicators();
}

}  // namespace nastja
