/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/math/random.h"
#include "lib/stencil/direction.h"
#include "lib/storage/paramstorage.h"
#include <string>

namespace nastja {

/**
 * Handles logging of cell properties (id, cellInBlock, center, oldCenter, motlityDirection, oldMotilityDirection) for each block.
 * This includes information about every cell that has at least one voxel within the current block 
 *
 * The actions have to registered by `registerAction()`.
 *
 * @ingroup actions cells
 */
class LogCellProperties : public Action {
public:
  explicit LogCellProperties(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~LogCellProperties() override                 = default;
  LogCellProperties(const LogCellProperties&)     = delete;
  LogCellProperties(LogCellProperties&&) noexcept = default;
  LogCellProperties& operator=(const LogCellProperties&) = delete;
  LogCellProperties& operator=(LogCellProperties&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return getSimData().timestep % eachNthStep != 0; }

private:
  // no additional private variables

};

}  // namespace nastja
