/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rotatefield.h"
#include "lib/field/field.h"

namespace nastja {

void RotateField::executeBlock(block::Block* block) {
  auto* field = block->getFieldByName(getFieldname());
  if (isBackRotation_) {
    field->rotateback();
  } else {
    field->rotate();
  }
}

void RotateField::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
