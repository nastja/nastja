/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"

namespace nastja {

class LoggerSync : public Action {
public:
  explicit LoggerSync() : Action() {}

  ~LoggerSync() override        = default;
  LoggerSync(const LoggerSync&) = delete;
  LoggerSync(LoggerSync&&)      = default;
  LoggerSync& operator=(const LoggerSync&) = delete;
  LoggerSync& operator=(LoggerSync&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return getSimData().timestep % eachNthStep != 0; }
};

}  // namespace nastja
