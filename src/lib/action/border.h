/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"

namespace nastja {

enum class BorderType : int {
  fullExchange,
  open,
  send,
  finalize
};

class Border : public Action {
public:
  explicit Border(const std::string& fieldname, const BorderType type = BorderType::fullExchange)
      : type_{type} {
    registerFieldname(fieldname);
  }

  ~Border() override        = default;
  Border(const Border&)     = delete;
  Border(Border&&) noexcept = default;
  Border& operator=(const Border&) = delete;
  Border& operator=(Border&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  void doOpenBlock(const block::Block* block);
  void doSendBlock(const block::Block* block);
  void doFinalizeBlock(const block::Block* block);

  void doOpen(const block::BlockRegister& blockRegister);
  void doSend(const block::BlockRegister& blockRegister);
  void doFinalize(const block::BlockRegister& blockRegister);

  void doFullExchange(const block::BlockRegister& blockRegister);

  BorderType type_;  ///< The type of this border.
};

}  // namespace nastja
