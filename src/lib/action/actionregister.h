/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/block/block.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/simdata.h"
#include <cassert>

namespace nastja {

class ActionRegister {
public:
  explicit ActionRegister()                 = default;
  virtual ~ActionRegister()                 = default;
  ActionRegister(const ActionRegister&)     = delete;
  ActionRegister(ActionRegister&&) noexcept = default;
  ActionRegister& operator=(const ActionRegister&) = delete;
  ActionRegister& operator=(ActionRegister&&) = delete;

  void registerAction(Action* action);
  void deregisterAction(const std::string& name);

  void initBlocks(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) const;
  void initBlock(block::Block* block, const Pool<field::FieldProperties>& fieldpool) const;
  void executeBlocks(const block::BlockRegister& blockRegister);
  void executeBlock(block::Block* block);
  static void executeActionOnBlocks(Action* action, const block::BlockRegister& blockRegister);
  void setupActions(const block::BlockRegister& blockRegister);

  void dump(Archive& ar) const {
    for (auto* action : actionRegister_) {
      action->dump(ar);
    }
  }

  void undump(Archive& ar) {
    for (auto* action : actionRegister_) {
      if (action->getName() == "Undump") continue;  // This action is not registerd when during dump
      action->undump(ar);
    }
  }

private:
  std::vector<Action*> actionRegister_;  ///< Vector of pointers to all actions.
};

}  // namespace nastja
