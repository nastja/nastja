/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/math/random.h"
#include "lib/stencil/D2C4.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/direction.h"
#include "lib/storage/paramstorage.h"
#include <string>

namespace nastja {

/**
 * Handles cell contact inhibition
 *
 * Actions have to be registered via `registerAction()`.
 *
 * @ingroup actions cells
 */

class CellContactInhibition : public Action {
public:
  explicit CellContactInhibition(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~CellContactInhibition() override                 = default;
  CellContactInhibition(const CellContactInhibition&)     = delete;
  CellContactInhibition(CellContactInhibition&&) noexcept = default;
  CellContactInhibition& operator=(const CellContactInhibition&) = delete;
  CellContactInhibition& operator=(CellContactInhibition&&) = delete;

  using Action::init;

  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return getSimData().timestep % eachNthStep != 0; }

private:
  enum class InhibitionCalcMode {
                            distance  ///Inhibition strength is calculated using distances between cell centers
                            };               
  void calculateNeighborTypesByDist(block::Block* block);/// < Calculates neighborType information for the block using the distances between cell centers
  void synchronizeNeighborTypes(block::Block* block);/// < Combines neighborType information with that of direct block neighbors

  std::unique_ptr<ParameterStorageBuilder<real_t>> inhibitionStrength_{};  ///< The pointer to the parameter storage for the inhibition strength between each cell type
  std::unique_ptr<ParameterStorageBuilder<real_t>> inhibitionDist_{};  ///< The pointer to the parameter storage for the inhibition distance for each cell type

  std::vector<real_t> localLinearizedInhibitionData_{}; //Stores information about contact inhibition within the local block. Order: source id, target id, inhibition value, i.e. 3 values per contribution

  std::map<cellid_t, real_t> finalInhibitionStrength_{};  ///Map containing the synchronized inhibition values over all blocks
  
  std::set<cellid_t> cellTypesToExchange_{};///< Set containing the cell types to be exchanged (those that have > 0 inhibitionStrength)
  InhibitionCalcMode inhibitionCalcMode_{};
};

}  // namespace nastja
