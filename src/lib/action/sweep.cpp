/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sweep.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include <memory>

namespace nastja {

void Sweep::init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) {
  auto fields = this->getFieldnames();

  for (const auto& name : fields) {
    if (!(block->fields.hasElement(name))) {
      logger::get().trace("Field '{}' does not yet exists in the fieldSet.", name);

      auto* fieldProperties = fieldpool.getElement(name);
      block->fields.registerElement(name, fieldProperties->createField(*block));
    }
  }
}

void Sweep::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) {
  for (const auto& block : blockRegister) {
    init(block.second, fieldpool);
  }
}

}  // namespace nastja
