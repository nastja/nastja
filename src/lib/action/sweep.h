/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/block/block.h"

namespace nastja {

class Block;

/**
 * @defgroup sweeps Sweep
 * Provides actions that sweeps over fields in blocks.
 *
 * Actions are registered in a list and executed in each time step. Included are the calculation of fields (sweeps),
 * I/O, boundary exchange or other utility functions.
 *
 * @ingroup actions
 */

class Sweep : public Action {
public:
  explicit Sweep() {}
  ~Sweep() override       = default;
  Sweep(const Sweep&)     = delete;
  Sweep(Sweep&&) noexcept = default;
  Sweep& operator=(const Sweep&) = delete;
  Sweep& operator=(Sweep&&) = delete;

  void init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) override;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }
};

}  // namespace nastja
