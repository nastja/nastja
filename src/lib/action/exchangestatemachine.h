/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/math/statemachine2.h"

namespace nastja {
/*
+-------+ full   +----+
|initial|------->|full|
+-+-----+        +----+
  |
  | partial
  v
+-----+       +------+       +-------+
|first|------>|second|------>|partial|
+-----+       +------+       +-------+
*/

enum class ExchangeState {
  initial,
  full,
  first,
  second,
  partial,
};

enum class ExchangeEvent {
  next,
  initfull,
  initpartial,
};

class ExchangeStateMachine : public StateMachine2<ExchangeState, ExchangeEvent> {
public:
  explicit ExchangeStateMachine() : StateMachine2{} {
    // clang-format off
    //            Start                    Event                      Next
    //----------------------------------+---------------------------+------------------------
    addTransition(ExchangeState::initial, ExchangeEvent::initfull,    ExchangeState::full);
    addTransition(ExchangeState::initial, ExchangeEvent::initpartial, ExchangeState::first);
    //------------------------+-------------------------------------+------------------------
    addTransition(ExchangeState::first,   ExchangeEvent::next,        ExchangeState::second);
    //------------------------+-------------------------------------+------------------------
    addTransition(ExchangeState::second,  ExchangeEvent::next,        ExchangeState::partial);
    // clang-format on

    setState(ExchangeState::initial);
  }

  bool isFullExchange() const { return getState() != ExchangeState::partial; }
};

}  // namespace nastja
