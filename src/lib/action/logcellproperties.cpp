/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/action/logcellproperties.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include "lib/io/writerreader.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <string>

namespace nastja {

using namespace stencil;
using namespace config::literals;

constexpr unsigned int cellRadius = 50; // TODO: define a cellRadius from config or cell size

void LogCellProperties::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();
  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);
    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }

    auto& container = block.second->getDataContainer<CellBlockContainer>();
    container.activateNearestNeighborSearch();  //TODO: only needed when neighbor serach is needed
  }

  auto& config = getSimData().getConfig();


  /// @key{CellsInSilico.logcellproperties.steps, uint, 1000}
  /// Number of steps between cell property logging.
  eachNthStep = config.template getValue<unsigned int>("CellsInSilico.logcellproperties.steps"_jptr, 1000);

}


void LogCellProperties::executeBlock(block::Block* block) {
//  logger::get("cellevents").info(R"("event": "LogCellProperties_action")");
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();



  bool cellInBlock = true;//True if cell center lies within block, false otherwise
  for (auto& it : container.cells) {
    cellid_t cellID = it.first;
    if (cellID <= data.liquid) continue;

    auto& cell = it.second;
    const auto& center = cell.center;
    
    cellInBlock = true;
    if (!(block->isGlobalCoordinateInBlock(center[0], center[1], center[2]))){
      cellInBlock = false;
    } 

    logger::get("cellevents").info(R"("event": "Cell property log","cellID": {},"cellType": {},"cellInBlock": {}, "cellX": {}, "cellY": {}, "cellZ": {}, "motX": {}, "motY": {}, "motZ": {}, "newmotX": {}, "newmotY": {},"newmotZ": {}, "oldComX" : {}, "oldComY" : {}, "oldComZ" : {})",
      cellID,
      cell.type,
      int(cellInBlock),
      cell.center[0],
      cell.center[1],
      cell.center[2],
      cell.motilityDirection[0],
      cell.motilityDirection[1],
      cell.motilityDirection[2],
      cell.newMotilityDirection[0],
      cell.newMotilityDirection[1],
      cell.newMotilityDirection[2],
      cell.oldCenter[0],
      cell.oldCenter[1],
      cell.oldCenter[2]
      );

  }
}

void LogCellProperties::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
