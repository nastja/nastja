/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once
#include "lib/action/action.h"
#include "lib/action/exchangestatemachine.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/stencil/direction.h"

namespace nastja {

class ExchangeContainer : public Action {
public:
  explicit ExchangeContainer(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~ExchangeContainer() override                   = default;
  ExchangeContainer(const ExchangeContainer&)     = delete;
  ExchangeContainer(ExchangeContainer&&) noexcept = default;
  ExchangeContainer& operator=(const ExchangeContainer&) = delete;
  ExchangeContainer& operator=(ExchangeContainer&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); };
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  unsigned int getSkippedExchange(const field::IField& field) const;
  static void recv(field::IField& field, CellBlockContainer& container, unsigned int mask);
  void recvContainers(const block::BlockRegister& blockRegister);
  void send(field::IField& field, CellBlockContainer& container, stencil::Direction side);
  void sendContainers(const block::BlockRegister& blockRegister);
  static void postReceive(const block::BlockRegister& blockRegister);
  bool initExchangeRange(const block::BlockRegister& blockRegister);

  long exchangeRange_{0};                 ///< The range of cell exchange area. 0 for full block exchange.
  ExchangeStateMachine exchangeState_{};  ///< At least the first two must be an full exchange.
};

}  // namespace nastja
