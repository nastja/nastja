/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"

namespace nastja {

/**
 * Cell Cleaner - prevents cells from getting split up into compartments
 * Deletes isolated voxels
 * deletes voxels that extend too far from center of mass
 */

template <unsigned int DIM = 3>
class CellCleaner : public Action {
public:
  explicit CellCleaner(const std::string& fieldname)
      : Action{} {
    static_assert(DIM == 2 || DIM == 3, "Only dimensions 2 and 3 are implemented.");
    registerFieldname(fieldname);
  }

  ~CellCleaner() override             = default;
  CellCleaner(const CellCleaner&)     = delete;
  CellCleaner(CellCleaner&&) noexcept = default;
  CellCleaner& operator=(const CellCleaner&) = delete;
  CellCleaner& operator=(CellCleaner&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override {
    // Every nth timestep after cell center exchange: delete single left pixels, NOT part of the Potts model
    return (getSimData().timestep <= offset) || (getSimData().timestep - offset) % eachNthStep != 0;
  }

private:
  void killSinglePixels(field::Field<cellid_t, 1>& field, CellBlockContainer& container);

  template <typename T>
  T periodicDistanceSquare(const math::Vector3<T>& p1, const math::Vector3<T>& p2, const math::Vector3<T>& size);

  unsigned int killDistanceSquare_{0};  ///< Square of the distance for killing frazzled voxels, 0 means deactivated.
};

}  // namespace nastja
