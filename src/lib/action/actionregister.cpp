/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "actionregister.h"
#include "lib/logger.h"
#include "lib/timing.h"

namespace nastja {

void ActionRegister::registerAction(Action* action) {
  action->isRegisterd = true;
  actionRegister_.push_back(action);
}

void ActionRegister::deregisterAction(const std::string& name) {
  for (auto it = actionRegister_.begin(); it != actionRegister_.end();) {
    if ((*it)->getName() == name) {
      (*it)->isRegisterd = false;

      it = actionRegister_.erase(it);
    } else {
      ++it;
    }
  }
}

void ActionRegister::initBlocks(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) const {
  for (const auto& action : actionRegister_) {
    action->init(blockRegister, fieldpool);
  }
}

void ActionRegister::initBlock(block::Block* block, const Pool<field::FieldProperties>& fieldpool) const {
  for (const auto& action : actionRegister_) {
    action->init(block, fieldpool);
  }
}

void ActionRegister::executeBlock(block::Block* block) {
  for (const auto& action : actionRegister_) {
    if (action->skip()) continue;
    FunctionTimer ft(action->getName());
    logger::get().debug("Execute {}", action->getName());
    action->executeBlock(block);
  }
}

void ActionRegister::executeBlocks(const block::BlockRegister& blockRegister) {
  for (const auto& action : actionRegister_) {
    if (action->skip()) continue;
    FunctionTimer ft(action->getName());
    logger::get().debug("Execute {}", action->getName());
    action->execute(blockRegister);
  }
}

void ActionRegister::executeActionOnBlocks(Action* action, const block::BlockRegister& blockRegister) {
  if (action->skip()) return;
  FunctionTimer ft(action->getName());
  action->execute(blockRegister);
}

void ActionRegister::setupActions(const block::BlockRegister& blockRegister) {
  for (const auto& action : actionRegister_) {
    logger::get().debug("Setup {}", action->getName());
    action->setup(blockRegister);
  }
}
}  // namespace nastja
