/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"

namespace nastja {

class CudaTransfer : public Action {
private:
  int method;

public:
  explicit CudaTransfer(std::string fieldname, int m)
      : Action() {
    registerFieldname(std::move(fieldname));
    method = m;
  }

  ~CudaTransfer() override          = default;
  CudaTransfer(const CudaTransfer&) = delete;
  CudaTransfer(CudaTransfer&&)      = default;
  CudaTransfer& operator=(const CudaTransfer&) = delete;
  CudaTransfer& operator=(CudaTransfer&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& /*blockRegister*/, const Pool<field::FieldProperties>& /*fieldpool*/) override {}
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }
};

}  // namespace nastja
