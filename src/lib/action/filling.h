/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/filling/factoryfilling.h"
#include "lib/filling/pattern/factorypattern.h"
#include "lib/filling/pattern/pattern.h"
#include "nlohmann/json.hpp"
#include "lib/config/configutils.h"

namespace nastja {

using json = nlohmann::json;

class Filling : public Action {
public:
  explicit Filling(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  explicit Filling(const std::vector<std::string>& fieldnames) {
    for (const auto& fieldname : fieldnames) {
      registerFieldname(fieldname);
    }
  }

  ~Filling() override         = default;
  Filling(const Filling&)     = delete;
  Filling(Filling&&) noexcept = default;
  Filling& operator=(const Filling&) = delete;
  Filling& operator=(Filling&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return false; }

private:
  bool doFilling(const json::json_pointer& jptr, field::IField* field, std::map<unsigned int, std::set<cellid_t>>* cellTypeMap = nullptr) const;

  template <typename T, unsigned int TSplitX>
  bool doFilling(const json::json_pointer& jptr, field::Field<T, TSplitX>& field, std::map<unsigned int, std::set<T>>* cellTypeMap = nullptr) const;

  std::string getPatternString(const json::json_pointer& jptr) const;
  std::vector<unsigned int> createCellTypeVector(const json::json_pointer& jptr) const;

  static void fillDomainDefaultValue(field::IField* field_, cellid_t value);
  void defaultFillingCells(block::Block* block, field::IField* field) const;
  void readInitFilling(block::Block* block, std::string const& fieldname);
  bool useInitFilling_{false};        ///< Flag to enable filling of field from raw file
};

}  // namespace nastja
