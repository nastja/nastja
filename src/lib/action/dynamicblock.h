/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/action/blockTuple_enum.h"
#include "lib/block/blockmanager.h"
#include "lib/communication/communicationmanager.h"
#include "lib/communication/messagemanager.h"
#include "lib/datacontainer/datacontainer.h"
#include "lib/datacontainer/dynamicblockcontainer.h"
#include <bitset>
#include <set>

namespace nastja {

class LoadBalance;

class DynamicBlock : public Action {
public:
  explicit DynamicBlock(block::BlockManager& blockmanager, const unsigned int stencil = 26)
      : blockmanager_{blockmanager},
        commManager_{blockmanager},
        messageManager_{blockmanager},
        numOfSides_{stencil} {}
  ~DynamicBlock() override          = default;
  DynamicBlock(const DynamicBlock&) = delete;
  DynamicBlock(DynamicBlock&&)      = default;
  DynamicBlock& operator=(const DynamicBlock&) = delete;
  DynamicBlock& operator=(DynamicBlock&&) = delete;

  void init(block::Block* block, const Pool<field::FieldProperties>& /*fieldpool*/) override;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* /*block*/) override { throw std::runtime_error("Single block execution is not allowed."); };
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return false; }

  CommManager& getCommManager() { return commManager_; }

  friend LoadBalance;

private:
  void cleanAllBlockData() const;
  std::map<int, BlockTuple> getNewBlocks();
  std::set<int> getDeletedBlocks();
  VectorPairInts getLocalNewBlockInfo(const std::map<int, BlockTuple>& newBlockInfo) const;
  void commRemoveBlocks(std::set<int>& delBlocks);
  void prepareMessage();

  void registerBlocks(std::map<int, BlockTuple>& newBlockInfo);

  int getRankOfNewBlock(unsigned int newID) const;

  static std::vector<int> flatten2Vector(const VectorPairInts& pairs);

  static void dbgPrintNewBlocks(const std::map<int, BlockTuple>& newBlockInfo);
  static void dbgPrintDeletedBlocks(std::set<int>& delBlocks);
  static void dbgPrintLocalNewBlocks(std::vector<int> localNewBlocks);

  block::BlockManager& blockmanager_;  ///< Reference to the BlockManager.
  CommManager commManager_;            ///< Manager for each communication partner the Communicator objects.
  MessageManager messageManager_;      ///< Manager for global communication.

  const unsigned int numOfSides_;  ///< The number of sides this action has to look for depending on the stencil.
};

}  // namespace nastja
