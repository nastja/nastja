/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/action/action.h"
#include "lib/action/actionregister.h"

namespace nastja {

/**
 * This action holds a list of actions that are executed sequentially block-by-block.
 *
 * Actions have to be registered via `registerAction()`.
 *
 * @ingroup actions
 */
class Sublist : public Action {
public:
  explicit Sublist() {}

  ~Sublist() override         = default;
  Sublist(const Sublist&)     = delete;
  Sublist(Sublist&&) noexcept = default;
  Sublist& operator=(const Sublist&) = delete;
  Sublist& operator=(Sublist&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void init(block::Block* block, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& blockRegister) override;
  bool skip() const override { return false; }

  void registerAction(Action* action);

  void dump(Archive& ar) const override {
    actionRegister_.dump(ar);
  }

  void undump(Archive& ar) override {
    actionRegister_.dump(ar);
  }

private:
  ActionRegister actionRegister_;  ///< The register of the sub actions.
};

}  // namespace nastja
