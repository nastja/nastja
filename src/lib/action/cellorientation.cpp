/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "cellorientation.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include "lib/io/writerreader.h"
#include "lib/math/matrix3.h"
#include "lib/math/vector.h"
#include "lib/simdata/datacells.h"
#include "lib/stencil/D3C26.h"
#include "lib/storage/paramstorage.h"
#include "lib/storage/reducedarray.h"
#include <cmath>
#include <string>

namespace nastja {

using namespace stencil;
using namespace config::literals;

constexpr unsigned int cellRadius = 50; // TODO: define a cellRadius from config or cell size

void CellOrientation::init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& /*fieldpool*/) {
  const auto& fieldname = getFieldname();
  for (const auto& block : blockRegister) {
    auto* field = block.second->getFieldByName(fieldname);
    if (field == nullptr) {
      throw std::out_of_range(fmt::format("The Field '{}' is not registered.", fieldname));
    }

    auto& container = block.second->getDataContainer<CellBlockContainer>();
    container.activateNearestNeighborSearch();  //TODO: only needed when neighbor serach is needed
  }

  auto& config = getSimData().getConfig();

  /// @key{CellsInSilico.orientation.motility, string, "random"}
  /// mode of cell motility, possible value is only 'random' so far
  std::string mode = config.template getValue<std::string>("CellsInSilico.orientation.motility"_jptr, "random");
  if (mode == "random") {
    motilityMode_ = MotilityMode::random;
  } else if (mode == "randomWalkOnSphere") {
    motilityMode_ = MotilityMode::randomWalkOnSphere;
  } else if (mode == "selfPropelled") {
    motilityMode_ = MotilityMode::selfPropelled;
  } else if (mode == "persistentRandomWalk") {
    motilityMode_ = MotilityMode::persistentRandomWalk;
  } else if (mode == "polarityDriven") {
    motilityMode_ = MotilityMode::polarityDriven;
  }


  /// @key{CellsInSilico.orientation.recalculationtime, uint, 10}
  /// Number of each n-th steps when the orientations are calculated.
  eachNthStep = config.template getValue<unsigned int>("CellsInSilico.orientation.recalculationtime"_jptr, 10);

  /// @key{CellsInSilico.orientation.motilityamount, ParameterStorage<real_t>}
  /// The vector of the amount of influence of the motility on each cell type.
  motilityAmount_ = std::make_unique<ParameterStorageBuilder<real_t>>("CellsInSilico.orientation.motilityamount"_jptr, config);

  if (motilityMode_ == MotilityMode::randomWalkOnSphere) {
    /// @key{CellsInSilico.orientation.stepwidth, real_t, 0.1}
    /// stepwidth of random walk on sphere surface e [0,1]
    stepwidth_ = config.template getValue<real_t>("CellsInSilico.orientation.stepwidth"_jptr, 0.1);

    /// @key{CellsInSilico.orientation.neighborinfluence, real_t, 0.0}
    /// Influence of the direction of random walk by the touching neighbors e [0,1]
    neighborInfluence_ = config.template getValue<real_t>("CellsInSilico.orientation.neighborinfluence"_jptr, 0.0);
    neighbors_.resize(cellRadius);  // expected number of neighbors;
  }

  if (motilityMode_ == MotilityMode::persistentRandomWalk) {
    /// @key{CellsInSilico.orientation.persistentDecay, real_t, 0.8}
    /// Decay of the persistentce (prefactor of velocity) e [0,1]
    persistentDecay_ = config.template getValue<real_t>("CellsInSilico.orientation.persistentDecay"_jptr, 0.8);

    /// @key{CellsInSilico.orientation.persistenceMagnitude, real_t, 0.5}
    /// Influence of the amount of random walk in persistent random walk e [0,1]: 1 = self propelled, 0 = Random walk
    persistenceMagnitude_ = config.template getValue<real_t>("CellsInSilico.orientation.persistenceMagnitude"_jptr, 0.5);

    /// @key{CellsInSilico.orientation.numRandomNumbers, unsigned int, 5}
    /// The number of random vectors chosen for the wiener process of the random movement part
    wienerProcesRandomNumbers_ = config.template getValue<unsigned int>("CellsInSilico.orientation.numRandomNumbers"_jptr, 5);
  }
}

/**
 * Calculates the moving direction.
 *
 * @param currentCoM  The current CoM.
 * @param oldCoM      The old CoM.
 * @param container   The container.
 *
 * @return The moving direction.
 */
inline math::Vector3<real_t> calcMovDir(const math::Vector3<real_t>& currentCoM, const math::Vector3<real_t>& oldCoM, const CellBlockContainer& container) {
  math::Vector3<real_t> movDir;

  auto sysSize = container.getBlock()->size * container.getBlock()->blocks;

  auto origin = currentCoM - oldCoM;            // Vector in the domain.
  auto upper  = currentCoM - oldCoM - sysSize;  // Upper periodic copy of vector.
  auto lower  = currentCoM - oldCoM + sysSize;  // Lower periodic copy of vector.

  for (int i = 0; i < 3; i++) {
    movDir[i] = std::abs(origin[i]) < std::abs(upper[i]) ? origin[i] : upper[i];
    movDir[i] = std::abs(movDir[i]) < std::abs(lower[i]) ? movDir[i] : lower[i];
  }
  return movDir;
}

/**
 * Calculates the moving direction.
 *
 * @param currentCoM  The current CoM.
 * @param oldCoM      The old CoM.
 * @param container   The container.
 *
 * @return The moving direction.
 */
inline math::Vector3<int> calcMovDir(const math::Vector3<int>& currentCoM, const math::Vector3<int>& oldCoM, const CellBlockContainer& container) {
  math::Vector3<int> movDir;

  auto sysSizeI = container.getBlock()->size * container.getBlock()->blocks;
  math::Vector3<int> sysSize{int(sysSizeI[0]),int(sysSizeI[1]),int(sysSizeI[2])};

  auto origin = currentCoM - oldCoM;            // Vector in the domain.
  auto upper  = currentCoM - oldCoM - sysSize;  // Upper periodic copy of vector.
  auto lower  = currentCoM - oldCoM + sysSize;  // Lower periodic copy of vector.

  for (int i = 0; i < 3; i++) {
    movDir[i] = std::abs(origin[i]) < std::abs(upper[i]) ? origin[i] : upper[i];
    movDir[i] = std::abs(movDir[i]) < std::abs(lower[i]) ? movDir[i] : lower[i];
  }
  return movDir;
}


/**
 * Gets a random point on the unit sphere.
 *
 * @return The vector of random point on sphere.
 */
math::Vector3<real_t> CellOrientation::getRandomPointOnSphere() {
  auto& generator = getSimData().generator;
  real_t phi      = 2.0 * M_PI * probability_(generator);
  real_t z        = 2.0 * probability_(generator) - 1.0;

  return math::Vector3<real_t>{std::cos(phi) * std::sqrt(1.0 - z * z),
                               std::sin(phi) * std::sqrt(1.0 - z * z),
                               z};
}

math::Vector3<real_t> CellOrientation::getRandomStepOnSphere(cellid_t cellID, const Cell& cell, const CellBlockContainer& container) {
  auto oldVector = cell.motilityDirection;

  // Do the motility as walk on sphere
  if (getSimData().timestep == 0 || oldVector.lengthSqr() == 0.0) {  // Initialize at time 0
    return getRandomPointOnSphere();
  }

  // normalize old Vector
  oldVector = oldVector.normalize();

  // Cross product with Z axis
  math::Vector3<real_t> zAxis{0.0, 0.0, 1.0};
  auto vecDiff = crossProduct(oldVector, zAxis);

  // Normalize and scale vecDiff to stepsize
  vecDiff = vecDiff.normalize();
  vecDiff = vecDiff * stepwidth_;

  // Rotate the vecDiff Vector along oldVector with a random angle
  real_t rotationAngle = 2.0 * M_PI * probability_(getSimData().generator);
  auto rotMat          = math::Matrix3<real_t>(oldVector, rotationAngle);
  vecDiff              = rotMat * vecDiff;

  // Add vecDiff to the initial vector
  auto newVec = oldVector + vecDiff;

  long radiusSqr = cellRadius * cellRadius;
  container.periodicRadiusSearch(cell.center, radiusSqr, neighbors_);

  // Go over all neighbors and add their normalized vectors rescaled with neighborInfluence_
  for (auto& neighbor : neighbors_) {
    cellid_t neighborID = neighbor.first;
    if (neighborID == cellID) continue;

    auto vecNeighbor = container.cells(neighborID).motilityDirection;

    if (vecNeighbor.lengthSqr() == 0.0) continue;

    newVec += vecNeighbor.normalize() * neighborInfluence_;
  }

  // Normalize newVec
  return newVec.normalize();
}

/**
 * Gets the purely self propelled movement of the cells. The movement is dependent on the previous movement of the cell.
 *
 * @param cell       The cell.
 * @param container  The container.
 *
 * @return The altered vector for the movement direction of the cell
 */
math::Vector3<real_t> CellOrientation::getSelfPropelled(Cell& cell, const CellBlockContainer& container) {
  auto& currentCoM = cell.centerReal;
  auto& oldCoM     = cell.oldCenterReal;

  math::Vector3<real_t> returnVector;
  if ((oldCoM[0] == -std::numeric_limits<real_t>::infinity()) || (cell.motilityDirection == math::Vector3<real_t>::zero)) {
    // old CoM not found -> random motility direction
    returnVector = getRandomPointOnSphere();
  } else {
    // found old CoM, direction depends on old Com
    // periodic velocity
    auto movDir = calcMovDir(currentCoM, oldCoM, container);

    if (movDir.length() == 0.0) {
      returnVector = cell.motilityDirection;  // IF No Movement, then random value?
    } else {
      returnVector = movDir;
      returnVector.normalize();
    }
  }

  cell.oldCenterReal = currentCoM;

  return returnVector.normalize();
}

/**
 * Gets a mixture of self propelled movement of the cells and random walk of the cells (the magnitude can be set via
 * config). The movement is dependent on the previous movement of the cell and a random walk (wiener process)
 *
 * @param cell       The cell.
 * @param container  The container.
 *
 * @return The altered vector for the movement direction of the cell
 */
math::Vector3<real_t> CellOrientation::getPersistentRW(Cell& cell, const CellBlockContainer& container) {
  auto& currentCoM = cell.center;
  auto& oldCoM     = cell.oldCenter;

  math::Vector3<real_t> returnVector;
  if ( (currentCoM[0] == -1) || (oldCoM[0] == -1) ) {
    // old CoM not found -> random motility direction
    returnVector = getRandomPointOnSphere();// * persistenceMagnitude_;
//    cell.oldCenter = currentCoM;
    return returnVector;
  } else {
    // found old CoM, direction depends on old Com
    // periodic velocity

    //auto movDirI = currentCoM - oldCoM;
    auto movDirI = calcMovDir(currentCoM, oldCoM, container);
    math::Vector3<real_t> movDir {real_t(movDirI[0]),real_t(movDirI[1]),real_t(movDirI[2])};    

    if (movDir.lengthSqr() == 0.0) {
      //returnVector = cell.motilityDirection.normalize();
      returnVector = getRandomPointOnSphere();// * persistenceMagnitude_;
  //    cell.oldCenter = currentCoM;
      return returnVector;
    } else {
      returnVector = movDir.normalize() * persistenceMagnitude_;
    }
  }

  math::Vector3<real_t> randomVector{0, 0, 0};
  for (unsigned int i = 0; i < wienerProcesRandomNumbers_; i++) {
    randomVector += getRandomPointOnSphere();
  }

  returnVector += 0.5 * randomVector * (1.0 - persistenceMagnitude_);  // Gets Distribution around 5 with extent 1
  //cell.oldCenter = currentCoM;

  return returnVector;
}



void CellOrientation::executeBlock(block::Block* block) {
//  logger::get("cellevents").info(R"("event": "Cellorientation_action")");
  auto& container = block->getDataContainer<CellBlockContainer>();
  auto& data      = getSimData().getData<DataCells>();
  auto& motAmount = motilityAmount_->getStorage();




  for (auto& it : container.cells) {
    cellid_t cellID = it.first;
    if (cellID <= data.liquid) continue;

    auto& cell = it.second;


    if (motAmount.getValue(cell.type) == 0) continue;

    const auto& center = cell.center;
    if (!(block->isGlobalCoordinateInBlock(center[0], center[1], center[2]))){
      // The cell's center of mass is not contained within the current block, so we only update its position and not its motility direction
      cell.oldCenter = center; 
      continue;
    } 


    math::Vector3<real_t> newMot{0,0,0}; //New motility vector initialization. Will be set in switch below
    switch (motilityMode_) {
      case MotilityMode::randomWalkOnSphere:
        // Set value in cell
        newMot = motAmount.getValue(cell.type) * getRandomStepOnSphere(cellID, cell, container);
        cell.newMotilityDirection = newMot;
        break;

      case MotilityMode::selfPropelled:
        // Set value in cell
        newMot = motAmount.getValue(cell.type) * getSelfPropelled(cell, container);
        cell.newMotilityDirection = newMot;
        break;

      case MotilityMode::persistentRandomWalk:
        // Set value in cell
        newMot = motAmount.getValue(cell.type) * getPersistentRW(cell, container);
        cell.newMotilityDirection = newMot;
        break;
      case MotilityMode::polarityDriven:
        //Set value in cell
        newMot = motAmount.getValue(cell.type) * cell.polarity;
        cell.newMotilityDirection = newMot;
        break;

      default:
        // Recalculate a new vector everytime for 'random' motility
        newMot = motAmount.getValue(cell.type) * getRandomPointOnSphere();
        cell.newMotilityDirection = newMot;
    }

    // Update the old cell center
    cell.oldCenter=center;

  }
}

void CellOrientation::execute(const block::BlockRegister& blockRegister) {
  for (const auto& block : blockRegister) {
    executeBlock(block.second);
  }
}

}  // namespace nastja
