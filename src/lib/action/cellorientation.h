/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/action/action.h"
#include "lib/datacontainer/cellblockcontainer.h"
#include "lib/math/random.h"
#include "lib/stencil/direction.h"
#include "lib/storage/paramstorage.h"
#include <string>

namespace nastja {

/**
 * Handles directed cell motility for random and directed directions.
 *
 * Actions have to be registered by `registerAction()`.
 *
 * @ingroup actions cells
 */
class CellOrientation : public Action {
public:
  explicit CellOrientation(const std::string& fieldname) {
    registerFieldname(fieldname);
  }

  ~CellOrientation() override                 = default;
  CellOrientation(const CellOrientation&)     = delete;
  CellOrientation(CellOrientation&&) noexcept = default;
  CellOrientation& operator=(const CellOrientation&) = delete;
  CellOrientation& operator=(CellOrientation&&) = delete;

  using Action::init;
  void init(const block::BlockRegister& blockRegister, const Pool<field::FieldProperties>& fieldpool) override;
  void executeBlock(block::Block* block) override;
  void execute(const block::BlockRegister& blockRegister) override;
  void setup(const block::BlockRegister& /*blockRegister*/) override {}
  bool skip() const override { return getSimData().timestep % eachNthStep != 0; }

private:
  enum class MotilityMode { random,
                            randomWalkOnSphere,      /// Possible modes if motility dynamics, random motility
                            selfPropelled,           /// Self propelled movement of cells, direction of motility depends on movement of CoM
                            persistentRandomWalk,    /// Persistent velocity + wiener process
                            polarityDriven};         /// Movement along polarity axis

  math::Vector3<real_t> getRandomPointOnSphere();
  math::Vector3<real_t> getRandomStepOnSphere(cellid_t cellID, const Cell& cell, const CellBlockContainer& container);
  math::Vector3<real_t> getSelfPropelled(Cell& cell, const CellBlockContainer& container);
  math::Vector3<real_t> getPersistentRW(Cell& cell, const CellBlockContainer& container);

  MotilityMode motilityMode_;                                          ///< The selected motility mode.
  std::unique_ptr<ParameterStorageBuilder<real_t>> motilityAmount_{};  ///< The pointer to the parameter storage for the amount of motility per cell Type.
  real_t stepwidth_{};
  real_t neighborInfluence_{};
  real_t persistentDecay_{};
  real_t persistenceMagnitude_{};

  unsigned int wienerProcesRandomNumbers_{5};  ///< The number of random numbers chosen for the wiener process at persistant random movement

  math::UniformReal<real_t> probability_{0, 1};      ///< The uniform distribution of the probability.
  std::vector<std::pair<cellid_t, int>> neighbors_;  ///< Indices for the neighbors.

  std::map<cellid_t, math::Vector3<int>> oldCenters_;
};

}  // namespace nastja
