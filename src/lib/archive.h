/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/math/vector.h"
#include "absl/container/flat_hash_map.h"
#include "robin_hood.h"
#include <array>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

namespace nastja {

class Archive {
public:
  template <typename T>
  void dumpObj(T*& t) {
    t->dump(*this);
  }

  template <typename T>
  void undumpObj(T*& t) {
    t->undump(*this);
  }

  template <typename T>
  void dumpObj(T* const& t) {
    t->dump(*this);
  }

  template <typename T>
  void undumpObj(T* const& t) {
    t->undump(*this);
  }

  template <typename T>
  void dumpObj(const T& t) {
    t.dump(*this);
  }

  template <typename T>
  void undumpObj(T& t) {
    t.undump(*this);
  }

  // pack and unpack of vector with primitives.
  template <typename T>
  void pack(const std::vector<T>& t) {
    int size = t.size();
    pack(size);
    if (size > 0) {
      packImpl(t.data(), t.size() * sizeof(T));
    }
  }

  template <typename T>
  void unpack(std::vector<T>& t) {
    int size{};
    unpack(size);
    if (size > 0) {
      t.resize(size);
      unpackImpl(t.data(), t.size() * sizeof(T));
    }
  }

  // pack and unpack of map with primitives.
  template <typename T, typename U>
  void pack(const std::map<T, U>& t) {
    int size = t.size();
    pack(size);
    for (const auto& i : t) {
      pack(i.first);
      pack(i.second);
    }
  }

  template <typename T, typename U>
  void unpack(std::map<T, U>& t) {
    int size{};
    unpack(size);
    for (int i = 0; i < size; i++) {
      T key;
      U value;
      unpack(key);
      unpack(value);
      t[key] = value;
    }
  }

  // pack and unpack of map with primitives.
  template <typename T, typename U>
  void pack(const std::unordered_map<T, U>& t) {
    int size = t.size();
    pack(size);
    for (const auto& i : t) {
      pack(i.first);
      pack(i.second);
    }
  }

  template <typename T, typename U>
  void unpack(std::unordered_map<T, U>& t) {
    int size{};
    unpack(size);
    for (int i = 0; i < size; i++) {
      T key;
      U value;
      unpack(key);
      unpack(value);
      t[key] = value;
    }
  }

  // pack and unpack of abseil hashmap with primitives.
  template <typename T, typename U>
  void pack(const absl::flat_hash_map<T, U>& t) {
    int size = t.size();
    pack(size);
    for (const auto& i : t) {
      pack(i.first);
      pack(i.second);
    }
  }

  template <typename T, typename U>
  void unpack(absl::flat_hash_map<T, U>& t) {
    int size{};
    unpack(size);
    for (int i = 0; i < size; i++) {
      T key{};
      U value{};
      unpack(key);
      unpack(value);
      t[key] = value;
    }
  }

  // pack and unpack of robin hood hashmap with primitives.
  template <typename T, typename U>
  void pack(const robin_hood::unordered_map<T, U>& t) {
    int size = t.size();
    pack(size);
    for (const auto& i : t) {
      pack(i.first);
      pack(i.second);
    }
  }

  template <typename T, typename U>
  void unpack(robin_hood::unordered_map<T, U>& t) {
    int size{};
    unpack(size);
    for (int i = 0; i < size; i++) {
      T key{};
      U value{};
      unpack(key);
      unpack(value);
      t[key] = value;
    }
  }

  // pack and unpack of array with primitives.
  template <typename T, std::size_t N>
  void pack(const std::array<T, N>& t) {
    packImpl(t.data(), N * sizeof(T));
  }

  template <typename T, std::size_t N>
  void unpack(std::array<T, N>& t) {
    unpackImpl(t.data(), N * sizeof(T));
  }

  // pack and unpack of array with primitives.
  void pack(const std::string& t) {
    int size = t.size();
    pack(size);
    if (size > 0) {
      packImpl(t.data(), t.size());
    }
  }

  void unpack(std::string& t) {
    int size{};
    unpackImpl(&size, sizeof(int));
    if (size > 0) {
      t.assign(&buffer_[position_], size);
      position_ += size;
    }
  }

  // pack and unpack of Vector3.
  template <typename T>
  void pack(const math::Vector3<T>& t) {
    packImpl(t.data(), 3 * sizeof(T));
  }

  template <typename T>
  void unpack(math::Vector3<T>& t) {
    unpackImpl(t.data(), 3 * sizeof(T));
  }

  // pack and unpack of primitives.
  template <typename T>
  typename std::enable_if<std::is_fundamental<T>::value>::type
  pack(const T& t) { packImpl(&t, sizeof(T)); }

  template <typename T>
  typename std::enable_if<std::is_fundamental<T>::value>::type
  unpack(T& t) { unpackImpl(&t, sizeof(T)); }

  // pack and unpack other objects. This objects have to provide pack() and unpack()
  template <typename T>
  typename std::enable_if<!std::is_fundamental<T>::value>::type
  pack(const T& t) { t.pack(*this); }

  template <typename T>
  typename std::enable_if<!std::is_fundamental<T>::value>::type
  unpack(T& t) { t.unpack(*this); }

  // pack and unpack of primitive array
  template <typename T>
  void pack(const T& t, std::size_t n) { packImpl(&t, n * sizeof(T)); }

  template <typename T>
  void unpack(T& t, std::size_t n) { unpackImpl(&t, n * sizeof(T)); }

  std::size_t size() const { return buffer_.size(); }
  void resize(std::size_t count) { buffer_.resize(count); }
  void* data() { return buffer_.data(); }
  void reset() { position_ = 0; }
  void clear() {
    buffer_.clear();
    reset();
  }
  std::size_t eof() const { return static_cast<std::size_t>(buffer_.size() <= position_); }

  // packs a temorary size for an unknown sized array, must be closed by endArray.
  template <typename T>
  typename std::enable_if<std::is_fundamental<T>::value>::type
  startArray(const T& t) {
    arrayStart_ = position_;
    packImpl(&t, sizeof(T));
  }

  // write the size for an array with unknown size.
  template <typename T>
  typename std::enable_if<std::is_fundamental<T>::value>::type
  endArray(const T& t) {
    overwriteImpl(&t, sizeof(T));
    arrayStart_ = 0;
  }

private:
  void packImpl(void const* p, std::size_t l);
  void unpackImpl(void* p, std::size_t l);
  void overwriteImpl(void const* p, std::size_t l);

  std::size_t position_{0};    ///< position in the buffer.
  std::size_t arrayStart_{0};  ///< position for the size of an array with unknown length.
  std::vector<char> buffer_;   ///< internal serialized buffer.
};

}  // namespace nastja
