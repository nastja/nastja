/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "nastja.h"

namespace nastja {

/**
 * Runs the simulation.
 *
 * @return Always 0 to forward to the main return.
 */
int NAStJA::run() {
  if (!application_) {
    if (factory_.isEmpty()) {
      throw std::runtime_error("No application is specified and the factory is empty, use addApplication or register an applications in the factory.");
    }
    /// @key{Application, FactoryApps, "Phasefield"}
    /// Run this application.
    application_ = config_.factoryCreate(factory_, "Application"_jptr, defaultApplication_, blockmanager_, simdata_);
  }

  /// @key{Settings.logger.group, uint, 0}
  /// Size of the group of cores using the same logging file. For smaller than 2 each core has its own output file.
  auto loggingGroupSize = config_.template getValue<unsigned int>("Settings.logger.group"_jptr, 0);

  // First setup every thing, memory, etc., then initialize.
  application_->setup();
  application_->init();

  // The simulation is setup and initialized, no more configure keys to read. Writing the config_save and close the config.
  config_.close();

  // Starts the time loop
  statusOutput_.start();
  while (simdata_.timestep < simdata_.getTimesteps()) {
    FunctionTimer ft("TimeStep");

    simdata_.nextTimestep();

    blockmanager_.executeBlocks();

    statusOutput_.call();
    signalHandler_.fetch();
  }
  blockmanager_.executeBlocksPostLoop();
  statusOutput_.finished();

  FunctionTimer::report(timingPath_, loggingGroupSize);

  return 0;
}

}  // namespace nastja
