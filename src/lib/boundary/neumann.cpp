/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "neumann.h"
#include "lib/config/config.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C6.h"
#ifdef USE_CUDA
#include "boundary_kernels.h"
#endif

namespace nastja {

using namespace stencil;

void NeumannBoundary::setup(SimData& simdata, const config::json::json_pointer& jptr) {
  simdata_ = &simdata;

  /// @key{Fields.\template{field}.boundaries.\template{boundary=Neumann}.value, real_t, 0.0}
  /// The value @f$v@f$ for the Neumann boundary @f$y'(a) = v@f$
  value_ = simdata.getConfig().getValue<real_t>(jptr / "value", 0.0);
}

void NeumannBoundary::calcLayer(field::Field<real_t, 1>& field, const unsigned long dst, const unsigned long src,
                                const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end, const real_t value) {
#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    cuda::neumannBoundary<real_t>(field.getGpuPtrs(1), field.getCuArgs(), field.getCuHaloStream(), dst, src, start.data(), end.data(), value);
  } else {
#endif
    for (unsigned long z = start[2]; z < end[2]; z++) {
      for (unsigned long y = start[1]; y < end[1]; y++) {
        for (unsigned long x = start[0]; x < end[0]; x++) {
          for (unsigned long v = 0; v < field.getVectorSize(); v++) {
            unsigned long index = field.getIndex(x, y, z);

            field.getCell(dst + index + v) = field.getCell(src + index + v) + value;
          }
        }
      }
    }
#ifdef USE_CUDA
  }
#endif
}

void NeumannBoundary::execute(field::IField& field, const Direction side) {
  unsigned int index = D3C6::index[static_cast<unsigned int>(side)];

  if (value_ == 0.0) {
    copyLayer2layer(field, field.offsetDst[index], field.offsetSrc[index], field.start[index], field.end[index]);
  } else {
    auto& field_ = field.get<real_t>();
    calcLayer(field_, field_.offsetDst[index], field_.offsetSrc[index], field_.start[index], field_.end[index],
              simdata_->deltax * value_);
  }
}

}  // namespace nastja
