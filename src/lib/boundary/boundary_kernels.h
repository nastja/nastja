/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/cuda/cuda_structs.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

namespace nastja {
namespace cuda {

void l2lBoundary(CudaGpuFieldPtrs<char> ptrs, std::size_t size, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end);

template <typename T>
void neumannBoundary(CudaGpuFieldPtrs<T> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end, T value);

template <typename T>
void setValue2layer(CudaGpuFieldPtrs<T> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long* start, const unsigned long* end, const T value);

}  // namespace cuda
}  // namespace nastja
