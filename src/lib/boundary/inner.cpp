/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "inner.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C6.h"

namespace nastja {

using namespace stencil;

void InnerBoundary::execute(field::IField& field, const Direction side) {
  unsigned int index = D3C6::index[static_cast<unsigned int>(side)];

  Boundary::copyLayer2layer(field, field.offsetDst[index], field.offsetSrc[index], field.start[index], field.end[index]);
}

}  // namespace nastja
