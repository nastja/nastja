/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "dirichlet.h"
#include "lib/config/config.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C6.h"

namespace nastja {

using namespace stencil;

void DirichletBoundary::setup(SimData& simdata, const config::json::json_pointer& jptr) {
  /// @key{Fields.\template{field}.boundaries.\template{boundary=Dirichlet}.value, real_t, 0.0}
  /// The value @f$v@f$ for the Dirichlet boundary @f$y(a) = v@f$
  value_ = simdata.getConfig().getValue<real_t>(jptr / "value", 0.0);
}

void DirichletBoundary::execute(field::IField& field, const Direction side) {
  auto& field_ = field.get<real_t>();

  unsigned int index = D3C6::index[static_cast<unsigned int>(side)];

  setValue2layer(field_, field.offsetDst[index], value_, field.start[index], field.end[index]);
}

}  // namespace nastja
