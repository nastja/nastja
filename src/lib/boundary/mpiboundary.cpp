/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mpiboundary.h"
#include "lib/logger.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C26.h"
#include <bitset>

namespace nastja {

using namespace stencil;

/**
 * Open Boundaries.
 *
 * @param field  Reference to the field.
 * @param side   ID of side.
 */
void MPIBoundary::MPIopen(field::IField& field, const Direction side) {
  int partnerRank = field.getNeighborRank(side);
  int tag         = getTag(field.getID(), side);

  if (partnerRank == -1) {
    partnerRank = field.getSimData().mpiData.getRank();
  }

#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    // use ifdef and normal if in combination to allow for usual MPI behaviour even with USE_CUDA defined
    MPI_Irecv(field.getExchangeCharBasePtr(1), 1, field.getProperties().getHaloDataType(field::HaloWay::target, side), partnerRank, tag, MPI_COMM_WORLD, field.getRecvRequest(D3C26::index[static_cast<unsigned int>(side)]));
    // logger::get().debug("{:2} recv from {:2}({:2}) side:{}", field.getID(), field.getNeighborID(side), partnerRank, (int)side ^ 0x01);
  } else {
#endif
    // logger::get().debug("{:2} recv from {:2}({:2}) side:{}", field.getID(), field.getNeighborID(side), partnerRank, side ^ 0x01);
    MPI_Irecv(field.getExtendedCharBasePtr(), 1, field.getProperties().getHaloDataType(field::HaloWay::target, side), partnerRank, tag, MPI_COMM_WORLD, field.getRecvRequest(D3C26::index[static_cast<unsigned int>(side)]));
#ifdef USE_CUDA
  }
#endif
}

/**
 * Sends boundary to the given side.
 *
 * @param field  Reference to the field.
 * @param side   The side.
 */
void MPIBoundary::MPIsend(field::IField& field, const Direction side) {
  int partnerRank = field.getNeighborRank(side);
  int tag         = 0;

  if (partnerRank == -1) {
    tag         = getTag(field.getID(), side);
    partnerRank = field.getSimData().mpiData.getRank();
  } else {
    tag = getTag(field.getNeighborID(side), getInverseDirection(side));
  }

#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    // use ifdef and normal if in combination to allow for usual MPI behaviour even with USE_CUDA defined
    MPI_Send(field.getExchangeCharBasePtr(0), 1, field.getProperties().getHaloDataType(field::HaloWay::source, side), partnerRank, tag, MPI_COMM_WORLD);
    //logger::get().debug("{:2} send to   {:2}({:2}) side:{}", field.getID(), field.getNeighborID(side), partnerRank, side);
  } else {
#endif
    // logger::get().debug("{:2} send to   {:2}({:2}) side:{}", field.getID(), field.getNeighborID(side), partnerRank, side);
    MPI_Send(field.getExtendedCharBasePtr(), 1, field.getProperties().getHaloDataType(field::HaloWay::source, side), partnerRank, tag, MPI_COMM_WORLD);
#ifdef USE_CUDA
  }
#endif
}

/**
 * Sends boundary to each side which is not skipped aka handled by an outside boundary condition.
 *
 * @param field  Reference to the field.
 * @param mask   The mask.
 */
void MPIBoundary::execute(field::IField& field, const unsigned int mask) {
  std::bitset<26> skipSides(mask);

  for (unsigned int i = 0; i < field.getNumberOfExchanges(); i++) {
    if (!skipSides[i]) {
      MPIsend(field, D3C26::dir[i]);
    }
  }
}

/**
 * Open receives for all non skipped boundaries.
 *
 * @param field  Reference to the field.
 * @param mask   The mask.
 */
void MPIBoundary::preExecute(field::IField& field, const unsigned int mask) {
  std::bitset<26> skipSides(mask);

  for (unsigned int i = 0; i < field.getNumberOfExchanges(); i++) {
    if (!skipSides[i]) {
      MPIopen(field, D3C26::dir[i]);
    }
  }
}

}  // namespace nastja
