/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/boundary/boundary.h"

namespace nastja {

/**
 * Class for the Dirichlet boundary condition.
 *
 * @ingroup boundarys
 */
class DirichletBoundary : public Boundary {
public:
  explicit DirichletBoundary()                    = default;
  ~DirichletBoundary() override                   = default;
  DirichletBoundary(const DirichletBoundary&)     = delete;
  DirichletBoundary(DirichletBoundary&&) noexcept = default;
  DirichletBoundary& operator=(const DirichletBoundary&) = delete;
  DirichletBoundary& operator=(DirichletBoundary&&) = delete;

  void execute(field::IField& field, stencil::Direction side) override;
  void execute(field::IField& /*field*/, const unsigned int /*mask*/) override { throw std::runtime_error("Mask execution of boundary is not available."); }
  void preExecute(field::IField& /*field*/, const unsigned int /*mask*/) override {}
  void setup(SimData& simdata, const config::json::json_pointer& jptr) override;

private:
  real_t value_{};  ///< The value d of the Dirichlet boundary condition, @f$ y(0) = d @f$.
};

}  // namespace nastja
