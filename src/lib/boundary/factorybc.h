/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/boundary/boundary.h"
#include "lib/boundary/dirichlet.h"
#include "lib/boundary/mpiboundary.h"
#include "lib/boundary/neumann.h"
#include "lib/boundary/wall.h"

namespace nastja {

class FactoryBC : public Factory<Boundary> {
public:
  /// @factory{FactoryBC}
  /// "Dirichlet", "Periodic", "Neumann", "Wall"
  explicit FactoryBC() {
    registerType<DirichletBoundary>("Dirichlet");
    registerType<MPIBoundary>("Periodic");
    registerType<NeumannBoundary>("Neumann");
    registerType<WallBoundary>("Wall");
  }
};

}  // namespace nastja
