/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/boundary/boundary.h"

namespace nastja {

/**
 * Class for wall boundary condition.
 *
 * @ingroup boundarys
 */
class WallBoundary : public Boundary {
public:
  explicit WallBoundary()               = default;
  ~WallBoundary() override              = default;
  WallBoundary(const WallBoundary&)     = delete;
  WallBoundary(WallBoundary&&) noexcept = default;
  WallBoundary& operator=(const WallBoundary&) = delete;
  WallBoundary& operator=(WallBoundary&&) = delete;

  void execute(field::IField& field, stencil::Direction side) override;
  void execute(field::IField& /*field*/, const unsigned int /*mask*/) override { throw std::runtime_error("Mask execution of boundary is not available."); }
  void preExecute(field::IField& /*field*/, const unsigned int /*mask*/) override {}
  void setup(SimData& simdata, const config::json::json_pointer& jptr) override;

private:
  void calcLayer(field::Field<real_t, 1>& field, unsigned long dst, unsigned long src, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end);

  int pairs_{};                ///< Number of pairs, if 0, only value1 is used.
  real_t value1_{};            ///< The value of the contact angle of first lamella.
  real_t value2_{};            ///< The value of the contact angle of second lamella.
  SimData* simdata_{nullptr};  ///< The pointer to the SimData.
};

}  // namespace nastja
