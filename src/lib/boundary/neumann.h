/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/boundary/boundary.h"

namespace nastja {

/**
 * Class for the Neumann boundary condition.
 *
 * @ingroup boundarys
 */
class NeumannBoundary : public Boundary {
public:
  explicit NeumannBoundary()                  = default;
  ~NeumannBoundary() override                 = default;
  NeumannBoundary(const NeumannBoundary&)     = delete;
  NeumannBoundary(NeumannBoundary&&) noexcept = default;
  NeumannBoundary& operator=(const NeumannBoundary&) = delete;
  NeumannBoundary& operator=(NeumannBoundary&&) = delete;

  void execute(field::IField& field, stencil::Direction side) override;
  void execute(field::IField& /*field*/, const unsigned int /*mask*/) override { throw std::runtime_error("Mask execution of boundary is not available."); }
  void preExecute(field::IField& /*field*/, const unsigned int /*mask*/) override {}
  void setup(SimData& simdata, const config::json::json_pointer& jptr) override;

private:
  static void calcLayer(field::Field<real_t, 1>& field, unsigned long dst, unsigned long src, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end, real_t value);

  real_t value_{};  ///< The value n of the von Neumann boundary condition, @f$ y'(0) = n@f$.

  SimData* simdata_{nullptr};  ///< The pointer to the SimData.
};

}  // namespace nastja
