/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/logger.h"
#include "boundary_kernels.h"

namespace nastja {
namespace cuda {

/**
 * Simple copy kernel on the base of char*, so that no information about the actual type is needed at compile-time.
 *
 * @param data    The pointer to the beginning of the dataset
 * @param size    The size of one element in the dataset
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param cellsX  Amount of cells in x direction
 * @param cellsY  Amount of cells in y direction
 * @param cellsZ  Amount of cells in z direction
 * @param dst     Points to the first cell where Data should be copied to
 * @param src     Points to the first cell from where Data should be copied from
 */
__global__ void l2lKernel(char* data, std::size_t size, CudaArguments args, unsigned int cellsX, unsigned int cellsY, unsigned int cellsZ, const unsigned long dst, const unsigned long src) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z;

  if (z < cellsZ && y < cellsY && x < cellsX) {
    unsigned int index = size * (z * args.strideZ + y * args.strideY + x * args.strideX);
    for (std::size_t s = 0; s < size * args.strideX; ++s) {
      data[dst + index + s] = data[src + index + s];
    }
  }
}

/**
 * Calculates the neumann boundary condition
 *
 * @param data    The pointer to the beginning of the dataset
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param cellsX  Amount of cells in x direction
 * @param cellsY  Amount of cells in y direction
 * @param cellsZ  Amount of cells in z direction
 * @param dst     Points to the first cell where Data should be copied to
 * @param src     Points to the first cell from where Data should be copied from
 * @param value   The value to be added to the copied data
 */
template <typename T>
__global__ void neumannKernel(T* data, CudaArguments args, unsigned int cellsX, unsigned int cellsY, unsigned int cellsZ, const unsigned long dst, const unsigned long src, const T value) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z;

  if (z < cellsZ && y < cellsY && x < cellsX) {
    unsigned int index = (z * args.strideZ + y * args.strideY + x * args.strideX);
    for (std::size_t v = 0; v < args.strideX; ++v) {
      T d                   = data[src + index + v];
      data[dst + index + v] = d + value;
    }
  }
}

/**
 * Set a specific value to a border
 *
 * @param data    The pointer to the beginning of the dataset
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param cellsX  Amount of cells in x direction
 * @param cellsY  Amount of cells in y direction
 * @param cellsZ  Amount of cells in z direction
 * @param dst     Points to the first cell where Data should be copied to
 * @param value   The value to be set at the border
 */
template <typename T>
__global__ void setValueKernel(T* data, CudaArguments args, unsigned int cellsX, unsigned int cellsY, unsigned int cellsZ, const unsigned long dst, const T value) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z;

  if (z < cellsZ && y < cellsY && x < cellsX) {
    unsigned int index = (z * args.strideZ + y * args.strideY + x * args.strideX);
    for (std::size_t v = 0; v < args.strideX; ++v) {
      data[dst + index] = value;
    }
  }
}

/**
 * Starts the kernel for the neumann boundary calculations
 *
 * @param ptrs    CudaGpuFieldPtrs with the ptr to the data array on which the kernel should be executed
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param stream  The cudaStream on which the Kernel is supposed to be executed
 * @param dst     Points to the first cell where Data should be copied to
 * @param src     Points to the first cell from where Data should be copied from
 * @param start   The BDL corner of the border
 * @param end     Thr FUR corner of the border
 * @param value   The value to be added to the copied data
 */
template <typename T>
void neumannBoundary(CudaGpuFieldPtrs<T> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end, T value) {
  unsigned long cellsX = end[0] - start[0];
  unsigned long cellsY = end[1] - start[1];
  unsigned long cellsZ = end[2] - start[2];

  // one dimension will always be 0, can this be done better?
  dim3 numBlocks(((cellsX + 3) / 4), ((cellsY + 3) / 4), ((cellsZ + 3) / 4));
  dim3 threadsPerBlock(4, 4, 4);

  neumannKernel<T><<<numBlocks, threadsPerBlock, 0, stream>>>(ptrs.data, args, cellsX, cellsY, cellsZ, dst, src, value);
}

/**
 * Starts the kernel for the layer2layer copy kernel
 *
 * @param ptrs    CudaGpuFieldPtrs with the ptr to the data array on which the kernel should be executed
 * @param size    The size of one element in the dataset
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param stream  The cudaStream on which the Kernel is supposed to be executed
 * @param dst     Points to the first cell where Data should be copied to
 * @param src     Points to the first cell from where Data should be copied from
 * @param start   The BDL corner of the border
 * @param end     The FUR corner of the border
 */
void l2lBoundary(CudaGpuFieldPtrs<char> ptrs, std::size_t size, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end) {
  unsigned long cellsX = end[0] - start[0];
  unsigned long cellsY = end[1] - start[1];
  unsigned long cellsZ = end[2] - start[2];

  // one dimension will always be 0, can this be done better?
  dim3 numBlocks(((cellsX + 3) / 4), ((cellsY + 3) / 4), ((cellsZ + 3) / 4));
  dim3 threadsPerBlock(4, 4, 4);

  l2lKernel<<<numBlocks, threadsPerBlock, 0, stream>>>(ptrs.data, size, args, cellsX, cellsY, cellsZ, size * dst, size * src);
}

/**
 * Starts the kernel to set a value to a layer
 *
 * @param ptrs    CudaGpuFieldPtrs with the ptr to the data array on which the kernel should be executed
 * @param args    CudaArguments with all necessary informations for the kernel execution
 * @param stream  The cudaStream on which the Kernel is supposed to be executed
 * @param dst     Points to the first cell where Data should be copied to
 * @param start   The BDL corner of the border
 * @param end     Thr FUR corner of the border
 * @param value   The value to be set at the border
 */
template <typename T>
void setValue2layer(CudaGpuFieldPtrs<T> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long* start, const unsigned long* end, const T value) {
  //logger::get().debug("value to layer border: {}", value);

  unsigned long cellsX = end[0] - start[0];
  unsigned long cellsY = end[1] - start[1];
  unsigned long cellsZ = end[2] - start[2];

  // one dimension will always be 0, can this be done better?
  dim3 numBlocks(((cellsX + 31) / 32), ((cellsY + 3) / 4), ((cellsZ + 3) / 4));
  dim3 threadsPerBlock(32, 4, 4);

  setValueKernel<T><<<numBlocks, threadsPerBlock, 0, stream>>>(ptrs.data, args, cellsX, cellsY, cellsZ, dst, value);
}

/// explicit definition of neumannBoundary for real_t
template void neumannBoundary<real_t>(CudaGpuFieldPtrs<real_t> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end, real_t value);
/// explicit definition of neumannBoundary for cellid_t
template void neumannBoundary<cellid_t>(CudaGpuFieldPtrs<cellid_t> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long src, const unsigned long* start, const unsigned long* end, cellid_t value);

/// explicit definition of setValue2layer for real_t
template void setValue2layer<real_t>(CudaGpuFieldPtrs<real_t> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long* start, const unsigned long* end, const real_t value);
/// explicit definition of setValue2layer for cellid_t
template void setValue2layer<cellid_t>(CudaGpuFieldPtrs<cellid_t> ptrs, CudaArguments args, cudaStream_t stream, const unsigned long dst, const unsigned long* start, const unsigned long* end, const cellid_t value);

}  // namespace cuda
}  // namespace nastja
