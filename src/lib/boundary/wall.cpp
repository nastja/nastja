/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "wall.h"
#include "lib/config/config.h"
#include "lib/field/field.h"
#include "lib/math/brent.h"
#include "lib/stencil/D3C6.h"

namespace nastja {

using namespace stencil;

void WallBoundary::setup(SimData& simdata, const config::json::json_pointer& jptr) {
  simdata_      = &simdata;
  auto& config  = simdata.getConfig();

  /// @key{Fields.\template{field}.boundaries.\template{boundary=Wall}.pairs, int, 0}
  /// The number of pairs.
  pairs_ = config.getValue<int>(jptr / "pairs", 0);

  /// @key{Fields.\template{field}.boundaries.\template{boundary=Wall}.value, real_t, 0.0}
  /// The first value @f$\frac{\gamma_{1s}-\gamma_{2s}}{\varepsilon \gamma_{12}}@f$.
  value1_ = config.getValue<real_t>(jptr / "value", 0.0);

  /// @key{Fields.\template{field}.boundaries.\template{boundary=Wall}.value2, real_t, 0.0}
  /// The second value @f$\frac{\gamma_{1s}-\gamma_{2s}}{\varepsilon \gamma_{12}}@f$.
  value2_ = config.getValue<real_t>(jptr / "value2", 0.0);
}

void WallBoundary::calcLayer(field::Field<real_t, 1>& field, const unsigned long dst, const unsigned long src,
                             const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end) {
  const real_t lamella1 = -3. * simdata_->deltax * value1_;  // value = (g1s - g2s) / (eps * g12)
  const real_t lamella2 = -3. * simdata_->deltax * value2_;
  real_t value;

  int size     = field.getSize(1);
  int boundary = field.getBoundarySize(1);

  int yoffset = 0;
  int width   = field.getSize(1);

  if (pairs_ > 0) {
    yoffset = (size - 2 * boundary) / (4 * pairs_) + boundary;
    width   = (size - 2 * boundary) / (2 * pairs_);
  }

  for (unsigned long z = start[2]; z < end[2]; z++) {
    for (unsigned long y = start[1]; y < end[1]; y++) {
      // b|...|,,,,,,|...|b where the boundary b belong to type '.' this is one pair
      if ((pairs_ == 0) || ((((width + y - yoffset) / width) % 2) != 0u)) {
        value = lamella1;
      } else {
        value = lamella2;
      }

      for (unsigned long x = start[0]; x < end[0]; x++) {
        for (unsigned long v = 0; v < field.getVectorSize(); v++) {
          unsigned long index = field.getIndex(x, y, z);

          real_t p1 = field.getCell(src + index + v);
          real_t p0 = math::brent([&](real_t p) -> real_t { return (p1 - p) + value * (p1 + p) * (1. - 0.5 * (p1 + p)); }, 0, 1, 1e-6);

          field.getCell(dst + index + v) = p0;
        }
      }
    }
  }
}

void WallBoundary::execute(field::IField& field, const Direction side) {
  auto& field_ = field.get<real_t>();

  unsigned int index = D3C6::index[static_cast<unsigned int>(side)];

  calcLayer(field_, field_.offsetDst[index], field_.offsetSrc[index], field_.start[index], field_.end[index]);
}

}  // namespace nastja
