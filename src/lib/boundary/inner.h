/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/boundary/boundary.h"

namespace nastja {

/**
 * Class for inner boundary. A copy of the outermost inner layer to the halo. It
 * is used for dynamic block simulations where the neighbor block is not part of
 * the simulation.
 *
 * @ingroup boundarys
 */
class InnerBoundary : public Boundary {
public:
  explicit InnerBoundary() {
    _isInner = true;
  }

  ~InnerBoundary() override               = default;
  InnerBoundary(const InnerBoundary&)     = delete;
  InnerBoundary(InnerBoundary&&) noexcept = default;
  InnerBoundary& operator=(const InnerBoundary&) = delete;
  InnerBoundary& operator=(InnerBoundary&&) = delete;

  void execute(field::IField& field, stencil::Direction side) override;
  void execute(field::IField& /*field*/, const unsigned int /*mask*/) override { throw std::runtime_error("Mask execution of boundary is not available."); }
  void preExecute(field::IField& /*field*/, const unsigned int /*mask*/) override {}
  void setup(SimData& /*simdata*/, const config::json::json_pointer& /*jptr*/) override {}
};

}  // namespace nastja
