/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"

namespace nastja {

namespace field {
class IField;
template <typename T, unsigned int TSplitX>
class Field;
}  // namespace field

/**
 * @defgroup boundarys Boundary
 * Provides boundary conditions for updating the halo layer.
 */

class Boundary {
public:
  explicit Boundary()
      : _isMPI{false}, _isInner{false} {}
  virtual ~Boundary()           = default;
  Boundary(const Boundary&)     = delete;
  Boundary(Boundary&&) noexcept = default;
  Boundary& operator=(const Boundary&) = delete;
  Boundary& operator=(Boundary&&) = delete;

  virtual void execute(field::IField& field, stencil::Direction side)          = 0;
  virtual void execute(field::IField& field, unsigned int side)                = 0;
  virtual void preExecute(field::IField& field, unsigned int mask)             = 0;
  virtual void setup(SimData& simdata, const config::json::json_pointer& jptr) = 0;

  bool isMPI() const { return _isMPI; }
  bool isInner() const { return _isInner; }

protected:
  static void copyLayer2layer(field::IField& field, unsigned long dst, unsigned long src, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end);
  static void setValue2layer(field::Field<real_t, 1>& field, unsigned long dst, real_t value, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end);

  bool _isMPI;    ///< Flag set if it is an MPI boundary.
  bool _isInner;  ///< Flag set if it is an inner boundary.
};

}  // namespace nastja
