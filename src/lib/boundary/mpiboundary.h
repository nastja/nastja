/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/boundary/boundary.h"

namespace nastja {

/**
 * Class for mpi boundary neighbor exchange. This boundary use an MPI_iRecv to
 * open the halo-layer for all needed boundary sides. There, later an MPI_Send
 * can send the halo exchange.
 *
 * @ingroup boundarys
 */
class MPIBoundary : public Boundary {
public:
  explicit MPIBoundary() { _isMPI = true; }

  ~MPIBoundary() override             = default;
  MPIBoundary(const MPIBoundary&)     = delete;
  MPIBoundary(MPIBoundary&&) noexcept = default;
  MPIBoundary& operator=(const MPIBoundary&) = delete;
  MPIBoundary& operator=(MPIBoundary&&) = delete;

  void execute(field::IField& /*field*/, const stencil::Direction /*side*/) override { throw std::runtime_error("Directional execution of MPIBoundary is not available."); }
  void execute(field::IField& field, unsigned int /*mask*/) override;
  void preExecute(field::IField& field, unsigned int mask) override;
  void setup(SimData& /*simdata*/, const config::json::json_pointer& /*jptr*/) override {}

private:
  /**
   * Gets the MPI tag for halo side exchange.
   *
   * @param id    The identifier of the block.
   * @param side  The side.
   *
   * @return The tag.
   */
  static int getTag(unsigned int id, const stencil::Direction side) {
    return static_cast<int>(id + (static_cast<unsigned int>(side) << MPITagBitsForBlockID));
  }

  static void MPIopen(field::IField& field, stencil::Direction side);
  static void MPIsend(field::IField& field, stencil::Direction side);
};

}  // namespace nastja
