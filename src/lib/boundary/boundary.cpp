/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/boundary/boundary.h"
#include "lib/field/field.h"
#include <cstring>
#ifdef USE_CUDA
#include "boundary_kernels.h"
#endif

namespace nastja {

void Boundary::copyLayer2layer(field::IField& field, const unsigned long dst, const unsigned long src,
                               const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end) {
  unsigned long lineSize = (end[0] - start[0]) * field.getVectorSize() * field.getElementSize();

#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    // TODO compare this with async memcpys on device likewise to the host code above
    // with non async memcpy the kernel was much faster
    cuda::l2lBoundary(field.getCharGpuPtrs(0), field.getElementSize(), field.getCuArgs(), field.getCuHaloStream(), dst, src, start.data(), end.data());
  } else {
#endif
    for (unsigned long z = start[2]; z < end[2]; z++) {
      for (unsigned long y = start[1]; y < end[1]; y++) {
        unsigned long index = field.getIndex(start.x(), y, z);
        std::memcpy(field.getCharBasePtr() + (dst + index) * field.getElementSize(),
                    field.getCharBasePtr() + (src + index) * field.getElementSize(), lineSize);
      }
    }
#ifdef USE_CUDA
  }
#endif
}

void Boundary::setValue2layer(field::Field<real_t, 1>& field, const unsigned long dst, const real_t value,
                              const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end) {
#ifdef USE_CUDA
  if (field.getCuDataOnGpu()) {
    cuda::setValue2layer(field.getGpuPtrs(0), field.getCuArgs(), field.getCuHaloStream(), dst, start.data(), end.data(), value);
  } else {
#endif
    for (unsigned long z = start[2]; z < end[2]; z++) {
      for (unsigned long y = start[1]; y < end[1]; y++) {
        for (unsigned long x = start[0]; x < end[0]; x++) {
          for (unsigned long v = 0; v < field.getVectorSize(); v++) {
            unsigned long index            = field.getIndex(x, y, z);
            field.getCell(dst + index + v) = value;
          }
        }
      }
    }
#ifdef USE_CUDA
  }
#endif
}

}  // namespace nastja
