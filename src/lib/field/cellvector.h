/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, j. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

namespace nastja {
namespace field {

/**
 * This class describes a cell vector.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX = 1>
class CellVector {
public:
  explicit CellVector(T* cellPtr) : cellPtr_{cellPtr} {}

  const T& operator[](unsigned int i) const { return cellPtr_[i * TSplitX]; }
  T& operator[](unsigned int i) { return cellPtr_[i * TSplitX]; }

private:
  T* cellPtr_;
};

}  // namespace field
}  // namespace nastja
