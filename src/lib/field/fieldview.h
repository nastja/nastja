/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/cellvector.h"
#include "lib/field/field.h"
#include "lib/field/fielditerator.h"
#include "lib/field/gatheroffsets.h"
#include "lib/simd/simd.h"
#include "lib/stencil/direction.h"
#include <array>

namespace nastja {
namespace field {

/**
 * A view to the field.
 *
 * This is used to iterate over a field with a step size that is used for SIMD vectorization. Therefore it provides
 * several data access operation for SIMD. It is also usefull for iteration over only a part of the field.
 *
 * @tparam T        Data type of the field.
 * @tparam TStep    The length of the SIMD register, this is the step size used for the SIMD dimension.
 * @tparam TSplitX  Number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TStep = 1, unsigned int TSplitX = 1, typename TIterator = FieldIterator<T, TStep, TSplitX>>
class FieldView {
public:
  /**
   * Constructs a new instance.
   *
   * @param field  The field.
   * @param start  The start.
   * @param end    The end.
   */
  explicit FieldView(Field<T, TSplitX>& field, math::Vector3<unsigned long> start, math::Vector3<unsigned long> end)
      : field_{field},
        start_{start},
        end_{end},
        offsetTimeStepPtr_{0, field.getProperties().getNumberOfTimeStepFields() > 1 ? field.getCellBasePtr(OLD) - field.getCellBasePtr(NEW) : 0},
        directionalOffsets_{field.getDirectionalOffsets()},
        xGatherOffsets_{field.getVectorSize(), TSplitX} {}

  TIterator begin() { return TIterator{field_, start_, end_}; }
  TIterator end() { return TIterator{}; }

  template <unsigned int TS = NEW>
  T& get(T* cellPtr, const stencil::Direction dir = stencil::Direction::C) const {
    return *(getPtr<TS>(cellPtr, dir));
  }

  template <unsigned int TS = NEW>
  CellVector<T, TSplitX> getVec(T* cellPtr, const stencil::Direction dir = stencil::Direction::C) const {
    return CellVector<T, TSplitX>{getPtr<TS>(cellPtr, dir)};
  }

  template <unsigned int TS = NEW>
  auto load(T* cellPtr, const stencil::Direction dir = stencil::Direction::C, const unsigned int j = 0) const {
    return simd::load(getPtr<TS>(cellPtr, dir, j));
  }

  template <unsigned int TS = NEW>
  auto load(T* cellPtr, const unsigned int j) const {
    return load<TS>(cellPtr, stencil::Direction::C, j);
  }

  template <unsigned int TS = NEW>
  auto load_unaligned(T* cellPtr, const unsigned int j) const {
    return load_unaligned<TS>(cellPtr, stencil::Direction::C, j);
  }

  template <unsigned int TS = NEW>
  auto load_unaligned(T* cellPtr, const stencil::Direction dir = stencil::Direction::C, const unsigned int j = 0) const {
    static_assert(TSplitX == 1 || TSplitX == 4 || TSplitX == 8, "load_gather is only for 4 and 8 packed numbers implemented.");
    if (TSplitX > 1 && stencil::cx(dir) == -1) {
      return simd::load_gather(getPtr<TS>(cellPtr, dir, j, true), xGatherOffsets_[0]);
    }
    if (TSplitX > 1 && stencil::cx(dir) == 1) {
      return simd::load_gather(getPtr<TS>(cellPtr, dir, j, true), xGatherOffsets_[1]);
    }
    return simd::load_unaligned(getPtr<TS>(cellPtr, dir, j));
  }

  template <unsigned int TS = NEW, typename U = typename simd::packed<T>::type>
  void store(T* cellPtr, U value) const {
    simd::store(getPtr<TS>(cellPtr), value);
  }

  template <unsigned int TS = NEW, typename U = typename simd::packed<T>::type>
  void store_unaligned(T* cellPtr, U value) const {
    simd::store_unaligned(getPtr<TS>(cellPtr), value);
  }

  template <unsigned int TS = NEW, typename U = typename simd::packed<T>::type>
  void store(T* cellPtr, U value, const unsigned int j) const {
    simd::store(getPtr<TS>(cellPtr, stencil::Direction::C, j), value);
  }
  template <unsigned int TS = NEW, typename U = typename simd::packed<T>::type>
  void store_unaligned(T* cellPtr, U value, const unsigned int j) const {
    simd::store_unaligned(getPtr<TS>(cellPtr, stencil::Direction::C, j), value);
  }

  unsigned long getIndex(T* cellPtr) {
    return cellPtr - field_.getCellBasePtr();
  }

  bool isBoundary(const TIterator& it, stencil::Direction dir) {
    auto coord = it.coordinates() + dir;
    for (unsigned int i = 0; i < 3; i++) {
      if (coord[i] < start_[i] || coord[i] >= end_[i]) return true;
    }
    return false;
  }

private:
  template <unsigned int TS = NEW>
  T* getPtr(T* cellPtr, const stencil::Direction dir = stencil::Direction::C, const unsigned int j = 0, bool onlyYZ = false) const {
    long offset{0};
    if (TSplitX > 1 && !onlyYZ) {
      long index = cellPtr - field_.getCellBasePtr(TS);
      offset     = field_.getXOffset(index, dir);
    }

    return cellPtr + offsetTimeStepPtr_[TS] + directionalOffsets_[static_cast<unsigned int>(dir)] + offset + j * TSplitX;
  }

  Field<T, TSplitX>& field_;                      ///< The pointer to the field.
  const math::Vector3<unsigned long> start_;      ///< Vector of start coordinates.
  const math::Vector3<unsigned long> end_;        ///< Vector of end coordinates (excluded).
  const std::array<long, 2> offsetTimeStepPtr_;   ///< Pointer difference to the old field.
  const std::array<long, 27> directionalOffsets_;  ///< The offsets for each direction in the stencil.
  const detail::GatherOffsets<T> xGatherOffsets_;  ///< The offsets for the x direction.
};

}  // namespace field
}  // namespace nastja
