/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/stencil/direction.h"
#include "field_kernels.h"

// this must be include in the cu file, the kernel launch operation is in there
#include "lib/cuda/cuda_sweep_ops.h"

namespace nastja {
namespace cuda {

using namespace stencil;  // for direction

__constant__ CudaGpuFieldPtrs<real_t> gpuPtr;

/**
 * Kernel to copy the halo data of the field to the exchange array
 *
 * This is needed in the beginning of a simulation to initially fill the exchange array
 */
template <Direction dir, typename T>
__global__ void haloToExchangeKernel(CudaArguments args, CudaSimData /*cSimData*/) {
  unsigned long x = (blockIdx.x * blockDim.x) + threadIdx.x + args.startX;
  unsigned long y = (blockIdx.y * blockDim.y) + threadIdx.y + args.startY;
  unsigned long z = (blockIdx.z * blockDim.z) + threadIdx.z + args.startZ;

  if (checkLimits<dir>(x, y, z, args)) {
    real_t cells[1];
    loadStencil<1, dir>(cells, gpuPtr, x, y, z, args);
    real_t cell = cells[toUnderlying(Direction::C)];
    storeStencil<dir>(gpuPtr, x, y, z, args, cell);
  }
}

/**
 * Launches the kernels to copy all data to the exchange array for an initiall filling
 *
 * This is needed in the beginning of a simulation to initially fill the exchange array
 */
template <typename T>
void copyHaloToExchange(CudaArguments args, CudaGpuFieldPtrs<T> h_oldGpuPtr, cudaStream_t stream) {
  for (int i = 0; i < numberOfDirections; ++i) {
    // fill all exchanges with 1 to initially fill the exchange array
    args.useExchangeOnBoundary[i] = 1;
  }

  gpuErrchk(cudaMemcpyToSymbol(gpuPtr, &h_oldGpuPtr, sizeof(CudaGpuFieldPtrs<T>)));

  CudaSimData cSimData;  ///< default values good enough for this kernel

  staticFor<numberOfDirections>([&](auto i) { launchKernel<static_cast<Direction>(i())>(haloToExchangeKernel<static_cast<Direction>(i()), T>, stream, stream, args, cSimData); });

  cudaDeviceSynchronize();
}

/// explicit declaration of copyHaloToExchange for real_t types
template void copyHaloToExchange<real_t>(CudaArguments args, CudaGpuFieldPtrs<real_t> h_oldGpuPtr, cudaStream_t stream);

}  // namespace cuda
}  // namespace nastja
