/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/field.h"
#include "lib/math/arithmetic.h"
#include "lib/math/vector.h"
#include "lib/stencil/direction.h"
#include <stdexcept>

namespace nastja {
namespace field {

/**
 * Class for field iterator.
 *
 * @tparam T        Type of the field data.
 * @tparam TStep    The length of the SIMD register, this is the step size used for the SIMD dimension.
 * @tparam TSplitX  Number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, std::size_t TStep = 1, std::size_t TSplitX = 1>
class FieldIterator {
public:
  /**
   * Constructs a default instance, points to the nullptr.
   */
  explicit FieldIterator()
      : start_{0, 0, 0},
        length_{} {}

  /**
   * Constructs the iterator.
   *
   * @param field  Pointer to the field.
   * @param start  Vector of start coordinates.
   * @param end    Vector of end coordinates.
   */
  explicit FieldIterator(Field<T, TSplitX>& field, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end)
      : start_{start},
        length_{end - start},
        jumpX_{(TSplitX == 1) ? TStep * field.getVectorSize() : TSplitX * (field.getVectorSize() - 1)},  // TODO this is the stepwidth
        jumpY_{(field.getPaddedSize(0) - math::divRoundUp(length_[0], TStep) * TStep) * field.getVectorSize()},
        jumpZ_{(field.getPaddedSize(1) - length_[1]) * field.getPaddedSize(0) * field.getVectorSize()},
        cellPtr_{field.getCellBasePtr() + field.getIndex(start[0], start[1], start[2])} {
    xfaststart_ = math::mod(start.x() - field.getBoundarySize(0), TSplitX);
    xfast_      = xfaststart_;

    // check xstart 0,2,4... for TStep = 2 and TSplit = 4
    if (TSplitX > 1 && math::mod(xfaststart_, TStep) != 0) {
      throw std::runtime_error(fmt::format("The x start ({}) is not aligned with the field view step width of {} for this field with a splitX of {}", start.x(), TStep, TSplitX));
    }
  }

  /// @name Operators
  /// @{

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  const FieldIterator& operator++() {
    coord_.x() += TStep;
    if (TSplitX > 1) {
      xfast_ += TStep;
      cellPtr_ += TStep;

      if (xfast_ < TSplitX && coord_.x() < length_.x()) return *this;
      xfast_ = 0;
    }
    cellPtr_ += jumpX_;

    if (coord_.x() < length_.x()) return *this;
    xfast_     = xfaststart_;
    coord_.x() = 0;
    coord_.y()++;
    cellPtr_ += jumpY_;

    if (coord_.y() < length_.y()) return *this;
    coord_.y() = 0;
    coord_.z()++;
    cellPtr_ += jumpZ_;

    if (coord_.z() < length_.z()) return *this;
    cellPtr_ = nullptr;

    return *this;
  }

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  FieldIterator operator++(int) {
    auto tmp(*this);
    operator++();
    return tmp;
  }

  /**
   * Addition assignment operator.
   *
   * @param step  The number of basic steps, i.e., step = 3 and TStep = 2 results in a total jump of 6.
   *
   * @return The result of the addition assignment.
   */
  const FieldIterator& operator+=(const unsigned long step) {
    static_assert(TSplitX == 1, "Fields with split x coordinates are not supported.");

    coord_.x() += TStep * step;
    cellPtr_ += TStep * step * jumpX_;

    if (coord_.x() < length_.x()) return *this;
    do {
      coord_.x() -= length_.x();
      coord_.y()++;
      cellPtr_ += jumpY_;
    } while (coord_.x() >= length_.x());

    if (coord_.y() < length_.y()) return *this;
    do {
      coord_.y() -= length_.y();
      coord_.z()++;
      cellPtr_ += jumpZ_;
    } while (coord_.y() >= length_.y());

    if (coord_.z() < length_.z()) return *this;
    cellPtr_ = nullptr;

    return *this;
  }

  /**
   * Equality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the equality.
   */
  bool operator==(const FieldIterator& other) const { return cellPtr_ == other.cellPtr_; }

  /**
   * Inequality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the inequality.
   */
  bool operator!=(const FieldIterator& other) const { return cellPtr_ != other.cellPtr_; }
  /// @}

  /// @name Access functions
  /// @{

  /**
   * Access the current cell.
   *
   * @return Pointer to the current cell.
   */
  T* operator*() { return cellPtr_; }
  /// @overload
  const T* operator*() const { return cellPtr_; }

  /**
   * Access the vector component of the current cell.
   *
   * @param i  The component.
   *
   * @return Reference to the component.
   */
  T& operator[](unsigned int i) { return cellPtr_[i * TSplitX]; }
  /// @overload
  const T& operator[](unsigned int i) const { return cellPtr_[i * TSplitX]; }

  /**
   * Access the coordinates.
   *
   * @return Vector of coordinates.
   */
  math::Vector3<unsigned long> coordinates() const { return coord_ + start_; }
  /// @}

private:
  const math::Vector3<unsigned long> start_;  ///< The start coordinates.
  math::Vector3<unsigned long> length_;       ///< The length of the field in each direction.
  const unsigned long jumpX_{};               ///< The number of elements jump over to reach the next element when x changes.
  const unsigned long jumpY_{};               ///< The number of elements jump over to reach the next element when y changes.
  const unsigned long jumpZ_{};               ///< The number of elements jump over to reach the next element when z changes.

  math::Vector3<unsigned long> coord_{0, 0, 0};  ///< The coordinates to the current cell.
  unsigned long xfaststart_{0};                  ///< The start of the fast x coordinate.
  unsigned long xfast_{0};                       ///< The position in the fast x coordinate.

  T* cellPtr_{nullptr};  ///< The pointer to the current cell.
};

}  // namespace field
}  // namespace nastja
