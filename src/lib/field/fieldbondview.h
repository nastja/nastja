/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2023 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 *
 */

#pragma once

#include "lib/field/field.h"
#include "lib/stencil/direction.h"

namespace nastja {

namespace field {

/**
 * A view of a field that allows you to store data "between" pairs of cells.
 * The stencil identified by DIM and SIZE determines what directions can be used
 * in FieldBondView::getBond and FieldBondView::setBond.
 * When this class is instantiated, it populates its FieldBondView::directionMap_ such that
 *
 *     directionMap_[toUnderlying(dir)] != -1
 *
 * for `SIZE / 2` directions and that for all directions
 *
 *     directionMap_[toUnderlying(dir)] != -1 <==> directionMap_[toUnderlying(getInverseDirection(dir))] == -1
 *
 * The underlying Field needs to have `getVectorSize() >= SIZE / 2`. The vector members
 * of this field are then used to store the values at all the bonds between `pos` and `pos + dir`
 * where `directionMap_[toUnderlying(dir)] != -1`. The values for the other bonds are stored
 * in the adjacent cells.
 * This example shows the FieldBondView::directionMap_ for the D2C8 stencil:
 *
 *     D2C8::dir = {L, R, D, U, DL, DR, UL, UR};
 * 
 *     FieldBondView<T, 2, 8>::directionMap_ = {
 *       L  =>  0,
 *       D  =>  1,                      UL U UR
 *       DL =>  2,                        \│/
 *       DR =>  3,                       L─O─R
 *       _  => -1                         /│\
 *     };                               DL D DR
 * 
 * 
 * 
 *        ┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐
 *      2 │ │ │ │8│ │7│ │ │ │ │6│ │      8 7 6
 *     ───┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┤       \│/
 *      1 │ │ │ │ │1│3│2│4│5│ │ │ │      1─O─5
 *     ───┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┤       /│\
 *      0 │ │ │ │ │ │ │ │ │ │ │ │ │      2 3 4
 *     ───┼─┴─┴─┴─┼─┴─┴─┴─┼─┴─┴─┴─┘
 *        │   0   │   1   │   2
 *
 * We store the values 1-8 at the eight bonds adjacent to (1, 1).
 * The L, D, DL, and DR bond values are stored at (1, 1) itself
 * and the rest is stored at the adjacent cells.
 *
 * \note
 * The FieldBondView::directionMap_ should be `constexpr` but we'd have to jump through a few hoops for that.
 *
 * @tparam T        Data type of the field.
 * @tparam DIM      Dimension of the stencil.
 * @tparam SIZE     Number of stencil sides.
 * @tparam TSplitX  Number of x elements in the vector that are split from the x coordinate.
 *
 */

template <typename T, unsigned int DIM, unsigned int SIZE, unsigned int TSplitX = 1>
class FieldBondView {
public:
  FieldBondView(Field<T, TSplitX>&);

  /**
   * Get a list of all stencil::Direction that are stored in a cell.
   */
  std::array<stencil::Direction, SIZE / 2> getLocallyStored() const;

  T getBond(math::Vector3<unsigned long>, stencil::Direction) const;

  /**
   * \note
   * Note that for cells adjacent to the boundary it is only safe to use FieldBondView::setBond
   * for bonds that are stored in the cell itself, since setting other bonds might write into
   * the boundary.
   */
  void setBond(math::Vector3<unsigned long>, stencil::Direction, T);

private:
  const std::array<ssize_t, stencil::numberOfDirections> directionMap_;
  const std::array<stencil::Direction, SIZE / 2> locallyStored_;

  Field<T, TSplitX>& field_;
};

} // namespace field

} // namespace nastja
