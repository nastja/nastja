/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/field.h"
#include "lib/math/arithmetic.h"
#include "lib/math/vector.h"
#include "lib/stencil/direction.h"
#include <stdexcept>

namespace nastja {
namespace field {

/**
 * Class for field iterator with multi-colored checkerboard.
 *
 * @tparam T        Type of the field data.
 * @tparam TColors  The number of colors per dimenstion in the checkerboard.
 */
template <typename T, std::size_t TColors>
class FieldIteratorCBN {
public:
  /**
   * Constructs a default instance, points to the nullptr.
   */
  explicit FieldIteratorCBN()
      : coord_{0, 0, 0},
        start_{0, 0, 0},
        length_{0, 0, 0} {}

  /**
   * Constructs the iterator.
   *
   * @param field   Pointer to the field.
   * @param start  Vector of start coordinates.
   * @param end     Vector of end coordinates.
   */
  explicit FieldIteratorCBN(Field<T, 1>& field, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end)
      : coord_{0, 0, 0},
        start_{start + calcStart(start + field.getOffset() - field.getBoundarySize(), field.getSimData().timestep)},
        length_{calcLength(start_, end)},
        jumpX_{(TColors == 1 ? 2 : TColors) * field.getVectorSize()},
        jumpY_{(field.getPaddedSize(0) - length_[0] +
                (TColors - 1) * field.getPaddedSize(0)) *
               field.getVectorSize()},
        jumpZ_{((field.getPaddedSize(1) - length_[1]) * field.getPaddedSize(0) +
                (TColors - 1) * (field.getPaddedSize(1)) * field.getPaddedSize(0)) *
               field.getVectorSize()},
        cellPtr_{(start_[0] >= end[0] || start_[1] >= end[1] || start_[2] >= end[2]) ? nullptr : field.getCellBasePtr() + field.getIndex(start_[0], start_[1], start_[2])} {}

  /// @name Operators
  /// @{

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  const FieldIteratorCBN& operator++() {
    coord_.x() += TColors;
    cellPtr_ += jumpX_;
    if (coord_.x() < length_.x()) return *this;
    coord_.x() = 0;
    nextY();

    if (coord_.y() < length_.y()) return *this;
    coord_.y() = 0;
    nextZ();

    if (coord_.z() < length_.z()) return *this;
    cellPtr_ = nullptr;

    return *this;
  }

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  FieldIteratorCBN operator++(int) {
    auto tmp(*this);
    operator++();
    return tmp;
  }

  /**
   * Addition assignment operator.
   *
   * @param step  The number of basic steps, i.e., step = 3 and TColors = 2 results in a total jump of 6.
   *
   * @return The result of the addition assignment.
   */
  const FieldIteratorCBN& operator+=(const unsigned long step) {
    coord_.x() += TColors * step;
    cellPtr_ += step * jumpX_;

    if (coord_.x() < length_.x()) return *this;
    do {
      do {
        coord_.x() -= length_.x();
        nextY();
      } while (coord_.x() >= length_.x());

      if (coord_.y() < length_.y()) return *this;
      do {
        coord_.y() -= length_.y();
        nextZ();
      } while (coord_.y() >= length_.y());
    } while (coord_.z() < length_.z() && coord_.x() >= length_.x());  // This happens, when nextZ add one to x.

    if (coord_.z() < length_.z()) return *this;
    cellPtr_ = nullptr;

    return *this;
  }

  /**
   * Equality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the equality.
   */
  bool operator==(const FieldIteratorCBN& other) const { return cellPtr_ == other.cellPtr_; }

  /**
   * Inequality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the inequality.
   */
  bool operator!=(const FieldIteratorCBN& other) const { return cellPtr_ != other.cellPtr_; }
  /// @}

  /// @name Access functions
  /// @{

  /**
   * Access the current cell.
   *
   * @return Pointer to the current cell.
   */
  T* operator*() { return cellPtr_; }
  /// @overload
  const T* operator*() const { return cellPtr_; }

  /**
   * Access the vector component of the current cell.
   *
   * @param i  The component.
   *
   * @return Reference to the component.
   */
  T& operator[](unsigned int i) { return cellPtr_[i]; }
  /// @overload
  const T& operator[](unsigned int i) const { return cellPtr_[i]; }

  /**
   * Access the coordinates.
   *
   * @return Vector of coordinates.
   */
  math::Vector3<unsigned long> coordinates() const { return coord_ + start_; }
  /// @}

private:
  /**
   * Proceeds a next y step.
   */
  void nextY() {
    coord_.y() += TColors;
    cellPtr_ += jumpY_;
  }

  /**
   * Proceeds a next z step.
   */
  void nextZ() {
    coord_.z() += TColors;
    cellPtr_ += jumpZ_;
  }

  /**
   * Calculates the start, depending on the time-step.
   *
   * @param offset    The offset.
   * @param timeStep  The time-step.
   *
   * @return The start.
   */
  math::Vector3<unsigned long> calcStart(math::Vector3<unsigned long> offset, unsigned long timeStep) const {
    offset = (TColors - (offset % TColors)) % TColors;  // Offset of first color in current block.

    // Add offset to the current color.
    offset.x() += (timeStep - 1) % TColors;
    offset.y() += ((timeStep - 1) / TColors) % TColors;
    offset.z() += ((timeStep - 1) / (TColors * TColors)) % TColors;

    return offset % TColors;  // Back to first color voxel.
  }

  /**
   * Calculates the length of the iterator domain.
   *
   * @param start  The start.
   * @param end    The end.
   *
   * @return The length.
   */
  math::Vector3<unsigned long> calcLength(math::Vector3<unsigned long> start, math::Vector3<unsigned long> end) const {
    math::Vector3<unsigned long> length = end - start;

    length.x() = math::divRoundUp(length.x(), TColors) * TColors;
    length.y() = math::divRoundUp(length.y(), TColors) * TColors;
    length.z() = math::divRoundUp(length.z(), TColors) * TColors;

    return length;
  }

  math::Vector3<unsigned long> coord_;  ///< The coordinates to the current cell.

  const math::Vector3<unsigned long> start_;   ///< The start coordinates.
  const math::Vector3<unsigned long> length_;  ///< The length of the field in each direction.
  const unsigned long jumpX_{};                ///< The number of elements jump over to reach the next element when x changes.
  const unsigned long jumpY_{};                ///< The number of elements jump over to reach the next element when y changes.
  const unsigned long jumpZ_{};                ///< The number of elements jump over to reach the next element when z changes.

  T* cellPtr_{};  ///< The pointer to the current cell.
};

}  // namespace field
}  // namespace nastja
