/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/cuda/cuda_structs.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

namespace nastja {
namespace cuda {

template <typename T>
void copyHaloToExchange(CudaArguments args, CudaGpuFieldPtrs<T> oldGpuPtr, cudaStream_t stream);

}  // namespace cuda
}  // namespace nastja
