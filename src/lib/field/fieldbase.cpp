/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "fieldbase.h"
#include "lib/logger.h"
#include "lib/block/block.h"
#include "lib/field/fieldproperties.h"
#include "lib/stencil/D3C6.h"

namespace nastja {
namespace field {

using namespace stencil;

IField::IField(block::Block& block, FieldProperties& fieldProperties)
    : block{block},
      fieldProperties{fieldProperties},
      simdata{block.simdata},
      vectorSize{fieldProperties.getVectorSize()},
      boundarySize{fieldProperties.getBoundarySize()},
      size{block.size + 2 * boundarySize},
      paddedSize{calcPadding(fieldProperties)},
      paddingPreOffset{calcPaddingOffset(fieldProperties)},
      stride{vectorSize, paddedSize[0] * vectorSize, paddedSize[1] * paddedSize[0] * vectorSize},
      numberOfSides{fieldProperties.getNumberOfSides()},
      numberOfCells{fieldProperties.hasComplexPadding() ? (size[0] / 2 + 1) * size[1] * size[2] : size[0] * size[1] * size[2]},
      numberOfPaddedCells{fieldProperties.hasComplexPadding() ? numberOfCells : paddedSize[0] * paddedSize[1] * paddedSize[2]} {
  calculateBoundaryGeometry();
  setupAllBoundaries();

  logger::get().trace(logger::Category::NewField,
                      "Block {} @ {} create {}-field for an alignment of {}. Padded size {} results in strides {}.",
                      block.id, block.blockOffset, fieldProperties.getName(), fieldProperties.getAlignment(), paddedSize, stride);
}

/**
 * Calculates the padding.
 *
 * The start of each x-line of the inner field data (Bx, ., .) must be aligned. When needed, a padding is added, between
 * the upper Bx boundary and the following lower Bx boundary.
 *
 * The padding is given by x elements, thus vectors if the vector length > 1. Note, that in case of vectors not the
 * padded size but size * vector length is a multiple of the alignment.
 *
 * A field with splitX is handled like a non-vector field, meaning that size is filled up to a multiple of splitX aka
 * the alignment.
 *
 * @param fieldProperties  The field properties.
 *
 * @return The padding.
 */
math::Vector3<unsigned long> IField::calcPadding(const FieldProperties& fieldProperties) const {
  math::Vector3<unsigned long> padding{};
  unsigned int alignElements = fieldProperties.getAlignment();  // 1 if it can not be padded or not match with the vectorsize

  if (vectorSize > 1 && fieldProperties.hasFlag(FieldType::splitX)) {
    // For the MPI subarray we need a splitX vector left and right
    padding[0] = 2 * ((alignElements - (boundarySize[0] % alignElements)) % alignElements);  // second modulo for size[0] % alignElements) equals 0
  } else {
    padding[0] = (alignElements - ((size[0] * vectorSize) % alignElements)) % alignElements;
  }

  return size + padding;
}

/**
 * Calculates the padding offset.
 *
 * The start of each x-line of the inner field data (Bx, ., .) must be aligned. The field holds the first boundary
 * elements before (Bx, 0, 0), that are Bx * vector size elements. For vector length > 1, this is aligned because,
 * alignment is only allowed when the vector length is a multiple of the alignment. For vector length = 1, a preOffset
 * is calculated.
 *
 * Having a field with splitX, a whole splitX * vector length bundle have to be added in front of (Bx, 0, 0). While
 * splitX is a multiple of the alignment, it is aligned. Note, this is valid as long as splitX >= Bx.
 *
 * @param fieldProperties  The field properties.
 *
 * @return The padding offset.
 */
unsigned int IField::calcPaddingOffset(const FieldProperties& fieldProperties) const {
  unsigned int alignElements = fieldProperties.getAlignment();  // 1 if it can not be padded or not match with the vectorsize

  unsigned int preOffset;

  if (vectorSize > 1 && fieldProperties.hasFlag(FieldType::splitX)) {
    preOffset = (alignElements - (boundarySize[0] % alignElements)) % alignElements;
    preOffset *= vectorSize;
  } else {
    preOffset = (alignElements - ((boundarySize[0] * vectorSize) % alignElements)) % alignElements;
  }
  return preOffset;
}

std::size_t IField::calcPaddedSize() const {
  return paddedSize[0] * paddedSize[1] * paddedSize[2] * vectorSize * elements;
}

/**
 * calculates the size needed for the exchange array
 */
std::size_t IField::calcBoundarySize() const {
  int innerSizeX = size[0] - 2 * boundarySize[0];
  int innerSizeY = size[1] - 2 * boundarySize[1];
  int innerSizeZ = size[2] - 2 * boundarySize[2];

  return (2 * innerSizeX * innerSizeY * boundarySize[2] +          // B F
          2 * innerSizeX * innerSizeZ * boundarySize[1] +          // U D
          2 * innerSizeY * innerSizeZ * boundarySize[0] +          // L R
          4 * innerSizeX * boundarySize[1] * boundarySize[2] +     // UF UB DF DB
          4 * innerSizeY * boundarySize[0] * boundarySize[2] +     // BL BR FL FR
          4 * innerSizeZ * boundarySize[0] * boundarySize[1] +     // UL UR DL DR
          8 * boundarySize[0] * boundarySize[1] * boundarySize[2]  // corners
          ) * vectorSize +
         26;  // flags
}

void IField::calculateBoundaryGeometry() {
  // Boundary sides
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 3; j++) {
      start[i][j] = 0;
      end[i][j]   = size[j];
    }
  }

  // upper x has no padding, because the values are not at the edge

  // lowerX
  offsetSrc[0] = stride[0];
  offsetDst[0] = 0;
  end[0][0]    = 1;
  // upperX
  offsetSrc[1] = (size[0] - 2) * stride[0];
  offsetDst[1] = (size[0] - 1) * stride[0];
  end[1][0]    = 1;

  // lowerY
  offsetSrc[2] = stride[1];
  offsetDst[2] = 0;
  end[2][1]    = 1;
  // upperY
  offsetSrc[3] = (size[1] - 2) * stride[1];
  offsetDst[3] = (size[1] - 1) * stride[1];
  end[3][1]    = 1;

  // lowerZ
  offsetSrc[4] = stride[2];
  offsetDst[4] = 0;
  end[4][2]    = 1;
  // upperZ
  offsetSrc[5] = (size[2] - 2) * stride[2];
  offsetDst[5] = (size[2] - 1) * stride[2];
  end[5][2]    = 1;
}

void IField::setupBoundary(const Direction side) {
  const unsigned int index     = D3C6::index[static_cast<unsigned int>(side)];
  const unsigned int dimension = index / 2;
  const bool isLower           = (dimension * 2 == index);

  if ((isLower && block.blockOffset[dimension] == 0) ||
      (!isLower && block.blockOffset[dimension] == block.blocks[dimension] - 1)) {
    boundarySet[index] = fieldProperties.getBoundary(index);
  } else {
    boundarySet[index] = fieldProperties.getMPIBoundary();
  }
  if (boundarySet[index]->isMPI() && getNeighborRank(side) == -1) {
    boundarySet[index] = fieldProperties.getInnerBoundary();
  }
}

void IField::updateAllBoundaries() {
  setupAllBoundaries();
}

void IField::setupAllBoundaries() {
  for (auto i : D3C6()) {
    setupBoundary(D3C6::dir[i]);
  }

  // initialize Request
  for (int i = 0; i < 26; i++) {
    sendRequest[i] = MPI_REQUEST_NULL;
    recvRequest[i] = MPI_REQUEST_NULL;
  }
}

unsigned long IField::calcBoundaryCells() const {
  return 2 * (size[0] * size[1] + size[1] * size[2] + size[0] * size[2]);
}

real_t IField::getVolume() const {
  return (size[0] - 2 * boundarySize[0]) * simdata.deltax *
         (size[1] - 2 * boundarySize[1]) * simdata.deltax *
         (size[2] - 2 * boundarySize[2]) * simdata.deltax;
}

int IField::getNeighborRank(const Direction side) const {
  return block.getLocalNeighborRank(side);
}

int IField::getNeighborID(const Direction side) const {
  return block.getLocalNeighborID(side);
}

unsigned int IField::getID() const {
  return block.id;
}

math::BoundingBox<unsigned long> IField::getHaloBoundingBox(const Direction side, const HaloWay way /*= HaloWay::source*/) const {
  return fieldProperties.getHaloBoundingBox(size, side, way);
}

}  // namespace field
}  // namespace nastja
