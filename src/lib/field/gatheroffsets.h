/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/cellvector.h"
#include "lib/field/field.h"
#include "lib/field/fielditerator.h"
#include "lib/simd/simd.h"
#include "lib/stencil/direction.h"
#include <array>

namespace nastja {
namespace field {
namespace detail {

/**
   * This class describes the gather offsets.
   *
   * It is a helper class to define 8 offsets for float or 4 offset for double-based SIMD vectorization.
   *
   * @tparam U  The data type.
   */
template <typename U>
class GatherOffsets {
public:
  explicit GatherOffsets(std::size_t /*vectorSize*/, std::size_t /*splitX*/) {}

private:
  std::array<typename simd::ipacked<U>::type, 2> offsets_;
};

template <>
class GatherOffsets<double> {
public:
  explicit GatherOffsets(std::size_t vectorSize, std::size_t splitX)
      : offsets_{simd::set(2l, 1l, 0l, -vectorSize * splitX + splitX - 1), simd::set(vectorSize * splitX, 3l, 2l, 1l)} {}

  const auto& operator[](unsigned int i) const { return offsets_[i]; }
  auto& operator[](unsigned int i) { return offsets_[i]; }

private:
  std::array<simd::ipacked<double>::type, 2> offsets_;
};

template <>
class GatherOffsets<float> {
public:
  explicit GatherOffsets(std::size_t vectorSize, std::size_t splitX)
      : offsets_{simd::set(vectorSize * splitX, 7, 6, 5, 4, 3, 2, 1), simd::set(6, 5, 4, 3, 2, 1, 0, -vectorSize * splitX + splitX - 1)} {}

  const auto& operator[](unsigned int i) const { return offsets_[i]; }
  auto& operator[](unsigned int i) { return offsets_[i]; }

private:
  std::array<simd::ipacked<float>::type, 2> offsets_;
};

}  // namespace detail
}  // namespace field
}  // namespace nastja
