/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2023 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <cassert>

#include "fieldbondview.h"

namespace nastja {

namespace field {

namespace { // anonymous
  template <unsigned int DIM, unsigned int SIZE>
  std::array<ssize_t, stencil::numberOfDirections> makeDirectionMap_() {
    std::array<ssize_t, stencil::numberOfDirections> directionMap {};
    for (size_t i = 0; i < directionMap.size(); i++) {
      directionMap[i] = -1;
    }

    size_t i = 0;
    auto stencil = stencil::Stencil<DIM, SIZE> {};
    for (auto it = stencil.begin(); it != stencil.end(); it++) {
      auto dir = it.direction();
      if (directionMap[toUnderlying(getInverseDirection(dir))] == -1) {
        directionMap[toUnderlying(dir)] = i;
        i++;
      }
    }
    assert(i * 2 == SIZE);

    return directionMap;
  }

  template <unsigned int DIM, unsigned int SIZE>
  std::array<stencil::Direction, SIZE / 2> makeLocallyStored_(
    std::array<ssize_t, stencil::numberOfDirections> directionMap
  ) {
    std::array<stencil::Direction, SIZE / 2> locallyStored {};
    
    size_t i = 0;
    auto stencil = stencil::Stencil<DIM, SIZE> {};
    for (auto it = stencil.begin(); it != stencil.end(); it++) {
      auto dir = it.direction();
      if (directionMap[toUnderlying(dir)] != -1) {
        locallyStored[i] = dir;
        i++;
      }
    }
    assert(i == locallyStored.size());

    return locallyStored;
  }
}

template<typename T, unsigned int DIM, unsigned int SIZE, unsigned TSplitX>
FieldBondView<T, DIM, SIZE, TSplitX>::FieldBondView(Field<T, TSplitX>& field)
: directionMap_(makeDirectionMap_<DIM, SIZE>())
, locallyStored_(makeLocallyStored_<DIM, SIZE>(directionMap_))
, field_(field)
{
  assert(field_.getVectorSize() >= SIZE / 2);
}

template<typename T, unsigned int DIM, unsigned int SIZE, unsigned TSplitX>
std::array<stencil::Direction, SIZE / 2> FieldBondView<T, DIM, SIZE, TSplitX>::getLocallyStored() const {
  return locallyStored_;
}

template<typename T, unsigned int DIM, unsigned int SIZE, unsigned TSplitX>
T FieldBondView<T, DIM, SIZE, TSplitX>::getBond(math::Vector3<unsigned long> pos, stencil::Direction dir) const {
  auto dirIndex = directionMap_[toUnderlying(dir)];

  if (dirIndex == -1) {
    pos = pos + dir;
    dirIndex = directionMap_[toUnderlying(getInverseDirection(dir))];
  }

  return field_.getCell(pos.x(), pos.y(), pos.z(), dirIndex);
}

template<typename T, unsigned int DIM, unsigned int SIZE, unsigned TSplitX>
void FieldBondView<T, DIM, SIZE, TSplitX>::setBond(
  math::Vector3<unsigned long> pos,
  stencil::Direction dir,
  T value
) {
  auto dirIndex = directionMap_[toUnderlying(dir)];

  if (dirIndex == -1) {
    pos = pos + dir;
    dirIndex = directionMap_[toUnderlying(getInverseDirection(dir))];
  }

  field_.getCell(pos.x(), pos.y(), pos.z(), dirIndex) = value;
}

// Explicit instantiations
template class FieldBondView<real_t, 3, 26>;

} // namespace field

} // namespace nastja
