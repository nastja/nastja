/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, j. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/block/block.h"
#include "lib/field/fieldbase.h"
#include "lib/field/fielditeratorcb.h"
#include "lib/field/fielditeratorcbn.h"
#include "lib/field/fieldproperties.h"
#include "lib/field/fieldview.h"
#include "lib/field/memory.h"
#include "lib/math/arithmetic.h"
#include "lib/math/boundingbox.h"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/D3C8asym.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <cstring>

#ifdef USE_CUDA
#include "lib/cuda/cuda_structs.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include "field_kernels.h"
#endif

namespace nastja {
namespace field {

constexpr unsigned int SimdStepSize = -1;  ///< Use this in a createView() when the auto step width detection should use
                                           ///< the simd register size also for the scalar fallback.

/**
 * This class describes a field.
 *
 * @tparam T        The underlying data type of the field.
 * @tparam TSplitX  The number of x elements in the vector that are split from the x coordinate.
 */
template <typename T, unsigned int TSplitX = 1>
class Field : public IField {
public:
  explicit Field(block::Block& block, FieldProperties& fieldProperties)
      : IField{block, fieldProperties} {
    elementSize = sizeof(T);
    if (fieldProperties.hasComplexPadding()) {
      elements = 2;
    }

    data_.resize(fieldProperties.getNumberOfTimeStepFields());
#ifdef USE_CUDA
    // resize the vectors to fit the amount of fields
    exchangeData.resize(fieldProperties.getNumberOfTimeStepFields());
    data_gpu.resize(fieldProperties.getNumberOfTimeStepFields());
    exchangeData_gpu.resize(fieldProperties.getNumberOfTimeStepFields());
    gpuPtrs.resize(fieldProperties.getNumberOfTimeStepFields());
    // TODO this would be nicer, but is at the moment not possible, because of the difficulties with the nvcc compiler (10.1)
    //cuSimData = CudaSimData(getSimData());
    cuSimData = CudaSimData(getSimData().deltax, getSimData().deltat);
#endif

    stencil::initializeFieldOffsets(directionOffsets, paddedSize, vectorSize, TSplitX);

    allocateMemory();
  }

  ~Field() override {
    for (auto& ptr : data_) {
      deallocate(ptr);
    }

#ifdef USE_CUDA
    if (fieldProperties.isCudaDatatype()) {
      for (std::size_t i = 0; i < gpuPtrs.size(); ++i) {
        cudaFree(data_gpu[i]);
        cudaFree(exchangeData_gpu[i]);
        cudaFreeHost(exchangeData[i]);
      }

      cudaStreamDestroy(cuMainStream);
      cudaStreamDestroy(cuHaloStream);
    }
#endif
  }

  Field(const Field&)     = delete;
  Field(Field&&) noexcept = default;
  Field& operator=(Field) = delete;
  Field& operator=(Field&&) = delete;

  template <unsigned int TStep = 0>
  auto createView() {
    return createView<TStep>(boundarySize, size - boundarySize);
  }

  template <unsigned int TStep = 0>
  auto createView(const math::BoundingBox<unsigned long>& bBox) {
    return createView<TStep>(bBox.lower(), bBox.upper());
  }

  /**
   * Creates a field view.
   *
   * If TStep is set, this value is used as a step width. If the default value TStep = 0 is used, the TSplitX width is
   * used. If TSplitX is also 0 then the instruction set register size is used when the instruction set needs an
   * alignment. Otherwise a TStep of 1 is used. Using TStep = SimdStepSize forced the use of a simd step width when the
   * instruction set does not need an alignment and TSplitX is 0. Use this, when simd functions are used and the scalar
   * fallback should do the same.
   *
   * @param lower  The lower edge.
   * @param upper  The upper edge.
   *
   * @tparam TStep  The step width for the view.
   *
   * @return The field view.
   */
  template <unsigned int TStep = 0>
  auto createView(math::Vector3<unsigned long> lower, math::Vector3<unsigned long> upper) {
    const unsigned int step = (TStep > 0 && TStep != SimdStepSize) ? TStep : (TSplitX > 1) ? TSplitX : (TStep == SimdStepSize) ? simd::getInstructionSetRegisterSize() / sizeof(T) : 1;
    return FieldView<T, step, TSplitX>(*this, lower, upper);
  }

  template <unsigned int TColor>
  auto createViewCB() {
    return createViewCB<TColor>(boundarySize, size - boundarySize);
  }

  template <unsigned int TColor>
  auto createViewCB(const math::BoundingBox<unsigned long>& bBox) {
    return createViewCB<TColor>(bBox.lower(), bBox.upper());
  }

  /**
   * Creates a field view with checkerboard.
   *
   * @param lower  The lower edge.
   * @param upper  The upper edge.
   *
   * @tparam TColor  The number of colors per dimension, or 1 for the two-color board.
   *
   * @return The field view.
   */
  template <unsigned int TColor, std::enable_if_t<TColor != 1, int> = 0>
  auto createViewCB(math::Vector3<unsigned long> lower, math::Vector3<unsigned long> upper) {
    return FieldView<T, 0, TSplitX, FieldIteratorCBN<T, TColor>>(*this, lower, upper);
  }

  template <unsigned int TColor, std::enable_if_t<TColor == 1, int> = 0>
  auto createViewCB(math::Vector3<unsigned long> lower, math::Vector3<unsigned long> upper) {
    return FieldView<T, 0, TSplitX, FieldIteratorCB<T>>(*this, lower, upper);
  }

  /**
   * Gets the outer bounding box, including the halo.
   *
   * @return The outer bounding box of the data field including halo.
   */
  auto getOuterBoundingBox() const {
    return math::BoundingBox<unsigned long>{math::Vector3<unsigned long>::zero, size};
  }

  /**
   * Gets the inner bounding box.
   *
   * @return The inner bounding box of the data field without boundaries.
   */
  auto getInnerBoundingBox() const {
    return math::BoundingBox<unsigned long>{boundarySize, size - boundarySize};
  }

  /**
   * Gets a part of the inner bounding box.
   *
   * @param d  The direction of the D3C8asym stencil.
   *
   * @return The inner bounding box part.
   */
  auto getInnerBoundingBoxPart(unsigned int d) const {
    auto bBox   = getInnerBoundingBox();
    auto offset = bBox.lower();
    bBox.translate(-offset);

    for (int axis = 0; axis < 3; axis++) {
      if (c(axis, stencil::D3C8asym::dir[d]) == 0) {  // cut lower half
        bBox[1][axis] /= 2;
      } else {  // cut upper half
        bBox[0][axis] = bBox[1][axis] / 2;
      }
    }

    return bBox.translate(offset);
  }

  char* getCharBasePtr(const unsigned int timeStepIndex = NEW) const override { return (char*)(data_[timeStepIndex] + paddingPreOffset); }

  char* getExtendedCharBasePtr(const unsigned int timeStepIndex = NEW) const override {
    if (TSplitX > 1) {
      return (char*)(data_[timeStepIndex]);
    }

    return getCharBasePtr(timeStepIndex);
  }

  T* getCellBasePtr(const unsigned int timeStepIndex = NEW) const { return data_[timeStepIndex] + paddingPreOffset; }

  real_t getCellAsReal(const unsigned long x, const unsigned long y, const unsigned long z, const unsigned long j) const override {
    return (real_t)getCell(x, y, z, j);
  }

  unsigned long getIndex(const unsigned long x, const unsigned long y, const unsigned long z) const final {
    if (TSplitX == 1) return z * stride[2] + y * stride[1] + x * stride[0];

    unsigned long xf = math::mod(x - boundarySize[0], TSplitX);                                                          // Mathematic modulo -1 mod 4 = 3
    long xs          = (static_cast<long>(x) - static_cast<long>(boundarySize[0])) >> math::countTailingZeros(TSplitX);  // Division by TSplitX, rounding down even negative numbers

    return z * stride[2] + y * stride[1] + xs * stride[0] * TSplitX + boundarySize[0] * stride[0] + xf;
  }

  unsigned long getXOffset(unsigned long index, const stencil::Direction dir) const {
    long xdir = stencil::cx(dir);

    if (TSplitX == 1 || xdir == 0) return 0;

    long xf = (index - boundarySize[0] * vectorSize) % TSplitX;
    if ((xf == TSplitX - 1 && xdir == 1) || (xf == 0 && xdir == -1)) {  // next/previous vector
      return xdir * (vectorSize * TSplitX - TSplitX + 1);
    }

    return xdir;  // next/previous in vector
  }

  template <unsigned int TS = NEW>
  T& getCell(const unsigned long index, const unsigned long j = 0) const {
    return data_[TS][paddingPreOffset + index + j * TSplitX];
  }

  template <unsigned int TS = NEW>
  T& getCell(const unsigned long x, const unsigned long y, const unsigned long z, const unsigned long j = 0) const {
    return getCell<TS>(getIndex(x, y, z), j);
  }

  template <unsigned int TS = NEW>
  T& getCell(const unsigned long index, const stencil::Direction dir, const unsigned long j = 0) const {
    return getCell<TS>(index + getDirectionalOffset(dir) + getXOffset(index, dir), j);
  }

  template <unsigned int TS = NEW>
  std::array<T, 27> getStencilCopy(const unsigned long index) const {
    std::array<T, 27> stencil{};

    for (int i = 0; i < 27; i++) {
      stencil[stencil::s27map_[i]] = getCell<TS>(index, stencil::D3C27::dir[i]);
    }

    return stencil;
  }

  template <unsigned int TS = NEW>
  std::array<T, 8> getCube(const unsigned long index) const {
    std::array<T, 8> cube{};

    cube[0] = getCell<TS>(index, stencil::Direction::C);
    cube[1] = getCell<TS>(index, stencil::Direction::R);
    cube[2] = getCell<TS>(index, stencil::Direction::U);
    cube[3] = getCell<TS>(index, stencil::Direction::UR);
    cube[4] = getCell<TS>(index, stencil::Direction::F);
    cube[5] = getCell<TS>(index, stencil::Direction::FR);
    cube[6] = getCell<TS>(index, stencil::Direction::FU);
    cube[7] = getCell<TS>(index, stencil::Direction::FUR);

    return cube;
  }

  /**
   * Rotate the fields <New, Old, Old2> becomes <Old, Old2, New>.
   */
  void rotate() override {
    std::rotate(data_.rbegin(), data_.rbegin() + 1, data_.rend());

#ifdef USE_CUDA
    if (fieldProperties.isCudaDatatype()) {
      // also rotate the gpuPtrs
      std::rotate(gpuPtrs.rbegin(), gpuPtrs.rbegin() + 1, gpuPtrs.rend());
    }
#endif
  }

  /**
   * Rotate the fields back <New, Old, Old2> becomes <Old2, New, Old>.
   */
  void rotateback() override {
    std::rotate(data_.begin(), data_.begin() + 1, data_.end());

#ifdef USE_CUDA
    if (fieldProperties.isCudaDatatype()) {
      // also rotate the gpuPtrs
      std::rotate(gpuPtrs.begin(), gpuPtrs.begin() + 1, gpuPtrs.end());
    }
#endif
  }

  real_t sumBoundary(const unsigned int j) const {
    real_t sum = 0.0;
    for (unsigned int side = 0; side < 6; side++) {
      sum += sumLayer(offsetDst[side], start[side], end[side], j);
    }
    return sum;
  }

  void pack(Archive& ar) const override {
    ar.pack(getCellBasePtr(NEW)[0], calcPaddedSize());
  }

  void unpack(Archive& ar) override {
    ar.unpack(getCellBasePtr(NEW)[0], calcPaddedSize());
  }

#ifdef USE_CUDA
  T* getExchangeBasePtrGpu(const unsigned int index = 0) const { return gpuPtrs[index].exchange; };

  CudaGpuFieldPtrs<T> getGpuPtrs(const unsigned int index = 0) const { return gpuPtrs[index]; }

  /// creates char pointers to the data and exchange array for access without knowledge of the actual type
  CudaGpuFieldPtrs<char> getCharGpuPtrs(const unsigned int index = 0) const override {
    CudaGpuFieldPtrs<char> tmp;
    tmp.data     = (char*)gpuPtrs[index].data;
    tmp.exchange = (char*)gpuPtrs[index].exchange;
    return tmp;
  }

  T* getExchangeBasePtr(const unsigned int index = 0) const { return exchangeData[index]; };
  char* getExchangeCharBasePtr(const unsigned int index = 0) const override { return (char*)exchangeData[index]; }

  cudaStream_t getCuMainStream() const { return cuMainStream; }
  cudaStream_t getCuHaloStream() const override { return cuHaloStream; }

  bool getCuDataOnGpu() const override { return cuDataOnGpu; }
  bool getCuDirty() const { return cuDirty; }
  void setCuDirty(bool dirty) { cuDirty = dirty; }

  CudaArguments getCuArgs() override { return cuArgs; }
  CudaSimData& getCuSimData() { return cuSimData; }

  void cuCpDataHostToDevice() {
    logger::get().debug("Data Host to Device copy");
    for (std::size_t i = 0; i < gpuPtrs.size(); ++i) {
      gpuErrchk(cudaMemcpy(gpuPtrs[i].data, (data_[i] + paddingPreOffset), (calcPaddedSize()) * sizeof(T), cudaMemcpyHostToDevice));
    }
    cuDataOnGpu = true;
  }

  void cuCpDataDeviceToHost() {
    logger::get().debug("Data Device to Host copy");
    for (std::size_t i = 0; i < gpuPtrs.size(); ++i) {
      gpuErrchk(cudaMemcpy((data_[i] + paddingPreOffset), gpuPtrs[i].data, (calcPaddedSize()) * sizeof(T), cudaMemcpyDeviceToHost));
    }
  }

  void cuCpExchangeHostToDevice() override {
    logger::get().debug("Exchange Host to Device copy");
    // NEW because of swap afterwards,
    gpuErrchk(cudaMemcpyAsync(gpuPtrs[NEW].exchange, exchangeData[OLD], calcBoundarySize() * sizeof(T), cudaMemcpyHostToDevice, cuHaloStream));
    //cudaDeviceSynchronize();
  }

  void cuCpExchangeDeviceToHost() override {
    logger::get().debug("Exchange Device to Host copy");
    gpuErrchk(cudaMemcpyAsync(exchangeData[NEW], gpuPtrs[NEW].exchange, (calcBoundarySize() - 26) * sizeof(T), cudaMemcpyDeviceToHost, cuHaloStream));
    cudaStreamSynchronize(cuHaloStream);
  }

  void cuSynchronize() {
    cudaDeviceSynchronize();
  }

  void cuCopyHaloToExchange() {
    cuda::copyHaloToExchange<T>(cuArgs, gpuPtrs[NEW], cuMainStream);

    cuCpExchangeDeviceToHost();
    cudaDeviceSynchronize();

    int offset = calcBoundarySize() - 26;
    for (int i = 0; i < 26; ++i) {
      ((T*)exchangeData[OLD])[offset + i] = T(0);
      ((T*)exchangeData[NEW])[offset + i] = T(1);
    }
  }

  /**
   * Checks which sides are received after a MPI exchange.
   *
   * This information is then used in the kernels to find out when to read out the exchange array
   */
  void cuCheckExchangeForNeighbours() {
    int offset = calcBoundarySize() - 26;
    logger::get().debug("exchanging MPI exchange infos");

    for (int i = 0; i < 26; ++i) {
      if (((real_t*)exchangeData[OLD])[offset + i] == real_t(1)) {
        logger::get().debug("Will receive boundary {}", i + 1);
        cuArgs.useExchangeOnBoundary[i + 1] = true;

      } else {
        cuArgs.useExchangeOnBoundary[i + 1] = false;
      }
    }
  }

#endif

private:
  void allocateMemory() {
    for (auto& ptr : data_) {
      // Allocate field memory aligned
      ptr = allocate<T>(paddingPreOffset + calcPaddedSize());

      std::fill_n(ptr, paddingPreOffset + calcPaddedSize(), T(0));
    }

#ifdef USE_CUDA
    if (fieldProperties.isCudaDatatype()) {
      int cuNumDevices;
      int rank;
      int cuHighPrio;
      int cuLowPrio;

      MPI_Comm_rank(MPI_COMM_WORLD, &rank);

      gpuErrchk(cudaGetDeviceCount(&cuNumDevices));
      cuAssignedGpu = rank % cuNumDevices;

      gpuErrchk(cudaSetDevice(cuAssignedGpu));

      gpuErrchk(cudaDeviceGetStreamPriorityRange(&cuLowPrio, &cuHighPrio));
      gpuErrchk(cudaStreamCreateWithPriority(&cuMainStream, 0, cuLowPrio));
      gpuErrchk(cudaStreamCreateWithPriority(&cuHaloStream, 0, cuHighPrio));

      for (std::size_t i = 0; i < gpuPtrs.size(); ++i) {
        // By default 256 byte aligned
        gpuErrchk(cudaMalloc((void**)&data_gpu[i], (paddingPreOffset + calcPaddedSize()) * sizeof(T)));
        gpuErrchk(cudaMalloc((void**)&exchangeData_gpu[i], calcBoundarySize() * sizeof(T)));
        gpuPtrs[i].data     = data_gpu[i] + paddingPreOffset;
        gpuPtrs[i].exchange = exchangeData_gpu[i];
      }

      for (auto& ptr : exchangeData) {
        gpuErrchk(cudaMallocHost((void**)&ptr, calcBoundarySize() * sizeof(T)));
      }
      cudaDeviceSynchronize();
      cuArgs.setSize(size);
      cuArgs.setBoundarySize(boundarySize);
      cuArgs.setStride(stride);
    }
#endif
  }

  real_t sumLayer(const unsigned long src, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end, const unsigned long j) const {
    real_t sum = 0;
    for (unsigned long z = start[2]; z < end[2]; z++) {
      for (unsigned long y = start[1]; y < end[1]; y++) {
        for (unsigned long x = start[0]; x < end[0]; x++) {
          unsigned long index = getIndex(x, y, z);

          sum += getCell(src + index, j);
        }
      }
    }

    return sum;
  }

  std::vector<T*> data_;  ///< The vector holding the time-steps.

#ifdef USE_CUDA
  std::vector<T*> exchangeData;  ///< exchange array on CPU

  std::vector<T*> data_gpu;                  ///< Represents data on the GPU
  std::vector<T*> exchangeData_gpu;          ///< exchange array on GPU
  std::vector<CudaGpuFieldPtrs<T>> gpuPtrs;  ///< combines vectors to data_gpu and exchangeData_gpu per time-step
  cudaStream_t cuMainStream;                 ///< CUDA stream for the bulk of the data in the middle of the field
  cudaStream_t cuHaloStream;                 ///< CUDA stream for handling the halo regions

  CudaArguments cuArgs{};  ///< holds informations about the field (like size,...)
  CudaSimData cuSimData;   ///< holds simulation informations like dx or dt

  bool cuDirty{true};       ///< defines if memory on GPU is dirty (newer than the data on the host)
  bool cuDataOnGpu{false};  ///< defines if a the data resides and should be calculated on the GPU
  int cuAssignedGpu{-1};    ///< shows the assigned GPU on the host (-1 means no GPU assigned)

#endif
};

}  // namespace field
}  // namespace nastja
