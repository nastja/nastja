/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/factory.h"
#include "lib/field/field.h"
#include "lib/field/fieldbase.h"
#include "lib/field/fieldproperties.h"

namespace nastja {
namespace field {

class FactoryField : public Factory<IField, block::Block&, FieldProperties&> {
public:
  explicit FactoryField() {
    registerType<Field<real_t, 1>>("Real1");
    registerType<Field<real_t, 2>>("Real2");
    registerType<Field<real_t, 4>>("Real4");
    registerType<Field<real_t, 8>>("Real8");
    registerType<Field<real_t, 16>>("Real16");
    registerType<Field<cellid_t, 1>>("Int1");
    registerType<Field<cellid_t, 2>>("Int2");
    registerType<Field<cellid_t, 4>>("Int4");
    registerType<Field<cellid_t, 8>>("Int8");
    registerType<Field<cellid_t, 16>>("Int16");
  }
};

}  // namespace field
}  // namespace nastja
