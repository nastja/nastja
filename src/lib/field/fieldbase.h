/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datatypes.h"
#include "lib/block/block.h"
#include "lib/datacontainer/datacontainerpool.h"
#include "lib/field/fieldproperties.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"
#ifdef USE_CUDA
#include "lib/cuda/cuda_structs.h"
#endif

namespace nastja {

namespace block {
class Block;
}

class Boundary;

namespace field {

constexpr unsigned int NEW = 0;  // timestep i
constexpr unsigned int OLD = 1;  // timestep i-1

/**
 * This class describes a field with elements or vectors of elements.
 *
 * ```
 *                        +-------+
 *                        |<- O ->|
 * +-------------------------+----+
 * |     ^                   |    |
 * |     | By                |    |
 * |     v                   |    |
 * | Bx +---------------+ Bx | Px |
 * |<-->| Nx  ^         |<-->|<-->|
 * |    |<----+-------->|    |    |
 * |    |     |         |    |    |
 * |    |     | Ny      |    |    |
 * |    |     v         |    |    |
 * |    +---------------+    |    |
 * |     ^                   |    |
 * |     | By                |    |
 * |     v                   |    |
 * +-------------------------+----+
 * ```
 *
 * - `Bx` - boundary layer size in x direction
 * - `By` - boundary layer size in y direction
 * - `Nx` - inner field size in x direction
 * - `Ny` - inner field size in y direction
 * - `Px` - `padding` in x direction, unused memory, to ensure that each x line starts at an aligned position
 * - `O`  - pre offset for padding `paddingPreOffset, to ensure that the first line starts on an aligned position
 *
 * `Px` is chosen such that `Nx + 2 Bx + Px` is a multiple of the alignment. `O` is chosen such that `O + Bx` is a
 * multiple of the alignment.
 */
class IField : public DataContainerPool {
public:
  explicit IField(block::Block& block, FieldProperties& fieldProperties);
  ~IField() override               = default;
  IField(const IField&)            = delete;
  IField(field::IField&&) noexcept = default;
  IField& operator=(const IField&) = delete;
  IField& operator=(field::IField&&) = delete;

  unsigned long getBoundarySize(const unsigned int dim) const { return boundarySize[dim]; }
  math::Vector3<unsigned long> getBoundarySize() const { return boundarySize; }
  math::Vector3<unsigned long> getOffset() const { return block.offset; }
  unsigned long getVectorSize() const { return vectorSize; }
  unsigned long getCells() const { return numberOfCells; }
  unsigned long getPaddedCells() const { return numberOfPaddedCells; }
  unsigned long getSize(const unsigned int dim) const { return size[dim]; }
  unsigned long getPaddedSize(const unsigned int dim) const { return paddedSize[dim]; }
  math::Vector3<unsigned long> getSize() const { return size; }
  math::Vector3<unsigned long> getStride() const { return stride; }

  void updateAllBoundaries();

  int getNeighborRank(stencil::Direction side) const;
  int getNeighborID(stencil::Direction side) const;

  real_t getVolume() const;
  unsigned long calcBoundaryCells() const;

  std::size_t getElementSize() const { return elementSize; }

  /**
   * Gets the data size of the field, including boundary and alignment padding.
   *
   * @return The data size.
   */
  std::size_t getDataSize() const { return calcPaddedSize() * elementSize; }

  virtual char* getCharBasePtr(unsigned int timeStepIndex = 0) const         = 0;
  virtual char* getExtendedCharBasePtr(unsigned int timeStepIndex = 0) const = 0;

  FieldProperties& getProperties() const { return fieldProperties; }

  /// @bug virtual functions are too cost full for getter
  virtual real_t getCellAsReal(unsigned long x, unsigned long y, unsigned long z, unsigned long v) const = 0;
  virtual unsigned long getIndex(unsigned long x, unsigned long y, unsigned long z) const                = 0;

  virtual void rotate()     = 0;
  virtual void rotateback() = 0;

  virtual void pack(Archive& ar) const = 0;
  virtual void unpack(Archive& ar)     = 0;

#ifdef USE_CUDA
  virtual bool getCuDataOnGpu() const     = 0;
  virtual void cuCpExchangeHostToDevice() = 0;
  virtual void cuCpExchangeDeviceToHost() = 0;

  virtual CudaArguments getCuArgs()            = 0;
  virtual cudaStream_t getCuHaloStream() const = 0;

  virtual CudaGpuFieldPtrs<char> getCharGpuPtrs(const unsigned int index = 0) const = 0;
  virtual char* getExchangeCharBasePtr(const unsigned int timeStepIndex = 0) const  = 0;
#endif

  /**
   * Dynamic cast from base class field to templated type.
   *
   * @tparam T        The data type of the field.
   * @tparam TSplitX  If true, instruction set register size is used for the splitX size. If the field is created with a
   *                  different alignment use your own dynamic_cast.
   *
   * @return pointer of type Field<T>* or nullptr if not matched
   */
  template <typename T, bool TSplitX = false>
  auto* getPtr() { return dynamic_cast<Field<T, TSplitX ? (simd::getInstructionSetRegisterSize() / sizeof(T)) : 1>*>(this); }

  template <typename T, bool TSplitX = false>
  auto& get() { return dynamic_cast<Field<T, TSplitX ? (simd::getInstructionSetRegisterSize() / sizeof(T)) : 1>&>(*this); }

  MPI_Request* getSendRequest(const int i = 0) { return &(sendRequest[i]); }
  MPI_Request* getRecvRequest(const int i = 0) { return &(recvRequest[i]); }

  block::Block& getBlock() { return block; }
  const block::Block& getBlock() const { return block; }
  unsigned int getID() const;
  unsigned int getNumberOfExchanges() const { return numberOfSides; }

  SimData& getSimData() const { return simdata; }

  math::BoundingBox<unsigned long> getHaloBoundingBox(stencil::Direction side, HaloWay way = HaloWay::source) const;

  const std::array<long, stencil::numberOfDirections>& getDirectionalOffsets() const { return directionOffsets; }

  long getDirectionalOffset(stencil::Direction dir) const { return directionOffsets[static_cast<unsigned int>(dir)]; }

  //Boundary Data
  std::array<math::Vector3<unsigned long>, 6> start;
  std::array<math::Vector3<unsigned long>, 6> end;
  std::array<unsigned long, 6> offsetSrc{};
  std::array<unsigned long, 6> offsetDst{};
  std::array<Boundary*, 6> boundarySet{};

protected:
  std::size_t calcSize() const { return numberOfCells * vectorSize * elements; }
  std::size_t calcPaddedSize() const;
  std::size_t calcBoundarySize() const;
  void calculateBoundaryGeometry();
  void setupBoundary(stencil::Direction side);
  void setupAllBoundaries();

  block::Block& block;
  FieldProperties& fieldProperties;
  SimData& simdata;

  const unsigned long vectorSize;
  const math::Vector3<unsigned long> boundarySize;
  const math::Vector3<unsigned long> size;
  const math::Vector3<unsigned long> paddedSize;
  const unsigned long paddingPreOffset{0};
  const math::Vector3<unsigned long> stride;
  const unsigned int numberOfSides;       ///< The number of halo exchanges sides.
  const std::size_t numberOfCells;        ///< Number of cells in the field including boundary.
  const std::size_t numberOfPaddedCells;  ///< Number of cells in the field including boundary and padding.

  std::size_t elementSize{};  ///< Size of one element
  std::size_t elements = 1;   ///< Aligned elements (2 for complex)

  std::array<MPI_Request, 26> sendRequest{};
  std::array<MPI_Request, 26> recvRequest{};

  std::array<long, stencil::numberOfDirections> directionOffsets{};

private:
  math::Vector3<unsigned long> calcPadding(const FieldProperties& fieldProperties) const;
  unsigned int calcPaddingOffset(const FieldProperties& fieldProperties) const;
};

}  // namespace field
}  // namespace nastja
