/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "fieldproperties.h"
#include "lib/boundary/inner.h"
#include "lib/boundary/mpiboundary.h"
#include "lib/config/config.h"
#include "lib/field/fieldbase.h"
#include "lib/field/fieldfactory.h"
#include "lib/math/arithmetic.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C6.h"
#include "lib/stencil/direction.h"
#include <memory>
#include <string>

namespace nastja {
namespace field {

using namespace stencil;

FieldProperties::~FieldProperties() {
  delete innerBoundary_;
  delete mpiBoundary_;

  for (auto& i : outerBoundarySet_) {
    if (i != mpiBoundary_) delete i;
  }
}

/**
 * Creates a field.
 *
 * @param block  The block.
 *
 * @return The field.
 */
std::unique_ptr<IField> FieldProperties::createField(block::Block& block) {
  FactoryField factory{};

  std::string field{};
  if (hasFlag(FieldType::real)) {
    field = "Real";
  } else if (hasFlag(FieldType::integer)) {
    field = "Int";
  }

  unsigned int split = 1;
  if (vectorSize_ > 1 && hasFlag(FieldType::splitX)) {
    split = getAlignment();
  }
  field += std::to_string(split);

  return factory.create(field, block, *this);
}

void FieldProperties::setupHook() {
  mpiBoundary_   = new MPIBoundary();
  innerBoundary_ = new InnerBoundary();

  // Default periodic Boundary
  for (auto& i : outerBoundarySet_) {
    i = mpiBoundary_;
  }

  for (unsigned int side = 0; side < 6; side++) {
    /// Set the boundary for the 6 sides.
    /// @todo we can use a factory here.
    /// @schemaFactoryDefault{Fields.\template{field}.boundaries.\template{boundary}, Periodic}
    std::string bc = simdata_->getConfig().getValue<std::string>("Fields"_jptr / name_ / "boundaries" / boundaryNames_[side] / "boundary", "Periodic");

    if (bc.find("Periodic") == 0) {  // starts with "Periodic" means a MPI boundary
      registerBoundary(side, mpiBoundary_);
    } else {
      registerBoundary(side, factoryBC_.create_ptr(bc));  // TODO: smart pointer for boundary? shared performance?
    }
  }

  for (unsigned int i = 0; i < 3; i++) {
    if (getBoundary(2 * i)->isMPI() != getBoundary(2 * i + 1)->isMPI()) {
      throw std::invalid_argument(fmt::format("Opposite boundaries must be both from MPI type or none."));
    }
  }
#ifdef USE_CUDA
  if (isCudaDatatype()) {
    setupGpuExchange();
  } else {
#endif
    setupHalo();
#ifdef USE_CUDA
  }
#endif
}

MPI_Datatype FieldProperties::getMPIType() const {
  if (hasFlag(FieldType::real)) return MPI_NASTJA_REAL;
  if (hasFlag(FieldType::complex)) return MPI_DOUBLE;
  if (hasFlag(FieldType::integer)) return MPI_NASTJA_INTEGER;

  throw std::invalid_argument(fmt::format("Unknown FieldType."));
}

/**
 * Used to set up the indexed mpi types for an exchange using a linear array
 *
 * this is currently only used if the data is stored on a gpu, to minimize needed
 * communications between the host and device for a mpi boundary exchange
 */
void FieldProperties::setupGpuExchange() {
  if (boundarySize_[0] == 0 && boundarySize_[1] == 0 && boundarySize_[2] == 0) return;

  /// @todo reread config
  auto tmpBlockSize = simdata_->getConfig().getVector3<int>("Geometry.blocksize"_jptr);

  int v          = vectorSize_;
  int innerSizeX = tmpBlockSize[0];
  int innerSizeY = tmpBlockSize[1];
  int innerSizeZ = tmpBlockSize[2];

  // blocklength for each side, edge, corner
  // second parameter for flag, that is used to show the receiver which sides are received
  int blocklength[26][2] = {
      {innerSizeY * innerSizeZ * v, 1},  // L    Left               (LowerX)
      {innerSizeY * innerSizeZ * v, 1},  // R    Right              (UpperX)
      {innerSizeX * innerSizeZ * v, 1},  // D    Down               (LowerY)
      {innerSizeX * innerSizeZ * v, 1},  // U    Up                 (UpperY)
      {innerSizeX * innerSizeY * v, 1},  // B    Back               (LowerZ)
      {innerSizeX * innerSizeY * v, 1},  // F    Forward            (UpperZ)
      {innerSizeZ * v, 1},               // DL   Down-Left          (LowerXLowerY)
      {innerSizeZ * v, 1},               // DR   Down-Right         (UpperXLowerY)
      {innerSizeZ * v, 1},               // UL   Up-Left            (LowerXUpperY)
      {innerSizeZ * v, 1},               // UR   Up-Right           (UpperXUpperY)
      {innerSizeY * v, 1},               // BL   Back-Left          (LowerXLowerZ)
      {innerSizeY * v, 1},               // BR   Back-Right         (UpperXLowerZ)
      {innerSizeX * v, 1},               // BD   Back-Down          (LowerYLowerZ)
      {innerSizeX * v, 1},               // BU   Back-Up            (UpperYLowerZ)
      {innerSizeY * v, 1},               // FL   Forward-Left       (LowerXUpperZ)
      {innerSizeY * v, 1},               // FR   Forward-Right      (UpperXUpperZ)
      {innerSizeX * v, 1},               // FD   Forward-Down       (LowerYUpperZ)
      {innerSizeX * v, 1},               // FU   Forward-Up         (UpperYUpperZ)
      {1 * v, 1},                        // BDL  Back-Down-Left     (LxLyLz)
      {1 * v, 1},                        // BDR  Back-Down-Right    (UxLyLz)
      {1 * v, 1},                        // BUL  Back-Up-Left       (LxUyLz)
      {1 * v, 1},                        // BUR  Back-Up-Right      (UxUyLz)
      {1 * v, 1},                        // FDL  Forward-Down-Left  (LxLyUz)
      {1 * v, 1},                        // FDR  Forward-Down-Right (UxLyUz)
      {1 * v, 1},                        // FUL  Forward-Up-Left    (LxUyUz)
      {1 * v, 1}                         // FUR  Forward-Up-Right   (UxUyUz)
  };

  std::array<int, 4> subSizes{};
  std::array<int, 4> starts{};

  // set full vectorSize
  subSizes[0] = vectorSize_;
  starts[0]   = 0;

  MPI_Datatype baseType = getMPIType();

  int displacement[2] = {0, v * (2 * innerSizeY * innerSizeZ + 2 * innerSizeX * innerSizeZ + 2 * innerSizeX * innerSizeY + 4 * innerSizeX + 4 * innerSizeY + 4 * innerSizeZ + 8)};
  for (unsigned int j = 0; j < 2 * D3C26::size; j++) {
    if (j == D3C26::size) {
      // reset for receive
      displacement[0] = 0;
      displacement[1] = v * (2 * innerSizeY * innerSizeZ + 2 * innerSizeX * innerSizeZ + 2 * innerSizeX * innerSizeY + 4 * innerSizeX + 4 * innerSizeY + 4 * innerSizeZ + 8);
    }
    MPI_Datatype& subarray = haloArray_[j];
    //displacement[0] = sendDisplacement[i];
    MPI_Type_indexed(2, blocklength[j % D3C26::size], displacement, baseType, &subarray);
    displacement[0] += blocklength[j % D3C26::size][0];
    displacement[1] += 1;  // for the flags at the end
    MPI_Type_commit(&subarray);
  }
}

void FieldProperties::setupHalo() {
  if (!hasHalo()) return;

  unsigned int alignElements = getAlignment();

  /// @todo reread config
  auto size = simdata_->getConfig().getVector3<unsigned long>("Geometry.blocksize"_jptr);
  if (vectorSize_ > 1 && hasFlag(FieldType::splitX) && size.x() % alignElements != 0) {
    throw std::invalid_argument(fmt::format("The field size in x direction ({}) must be a multiplier of the alignment({}).", size.x(), alignElements));
  }
  size += 2 * boundarySize_;

  // @TODO: do not repate code from IField::calcPadding, can we use the field?
  math::Vector3<unsigned long> padding{};

  if (vectorSize_ > 1 && hasFlag(FieldType::splitX)) {
    // For the MPI subarray we need a splitX vector left and right
    padding[0] = 2 * (alignElements - (boundarySize_[0] % alignElements) % alignElements);
  } else {
    padding[0] = (alignElements - ((size[0] * vectorSize_) % alignElements)) % alignElements;
  }

  unsigned int xindex = 1;
  if (vectorSize_ > 1 && hasFlag(FieldType::splitX)) {
    xindex = 2;
  }

  math::Vector<int, 5> blockSize{};
  blockSize[xindex - 1] = vectorSize_;
  for (int i = 0; i < 3; i++) {
    blockSize[xindex + i] = size[i] + padding[i];
  }
  if (vectorSize_ > 1 && hasFlag(FieldType::splitX)) {
    blockSize[0] = alignElements;
    blockSize[xindex] /= alignElements;
  }

  math::Vector<int, 5> subSizes{};
  math::Vector<int, 5> starts{};

  // set full vectorSize
  subSizes[xindex - 1] = vectorSize_;
  starts[xindex - 1]   = 0;

  MPI_Datatype baseType = getMPIType();

  for (int j = 0; j < 2; j++) {  // outer, inner
    auto way = static_cast<HaloWay>(j);
    for (auto dir : D3C26()) {
      auto halo = getHaloBoundingBox(size, D3C26::dir[dir], way);
      for (int i = 0; i < 3; i++) {
        starts[xindex + i]   = halo.lower()[i];
        subSizes[xindex + i] = halo.upper()[i] - halo.lower()[i];
      }

      if (vectorSize_ > 1 && hasFlag(FieldType::splitX)) {
        unsigned int shift = alignElements - boundarySize_[0];
        starts[xindex] += shift;
        starts[0] = starts[xindex] % alignElements;
        starts[xindex] /= alignElements;
        if (subSizes[xindex] == static_cast<int>(boundarySize_[0])) {
          subSizes[0]      = subSizes[xindex];
          subSizes[xindex] = 1;
        } else {
          subSizes[0] = alignElements;
          subSizes[xindex] /= alignElements;
        }
      }
      MPI_Datatype& subarray = getHaloDataType(way, D3C26::dir[dir]);
      MPI_Type_create_subarray(3 + xindex, blockSize.data(), subSizes.data(), starts.data(), MPI_ORDER_FORTRAN, baseType, &subarray);
      MPI_Type_commit(&subarray);
    }
  }
}

MPI_Datatype& FieldProperties::getHaloDataType(const HaloWay way, const Direction side) {
  unsigned int index = static_cast<unsigned int>(way) * D3C26::size + D3C26::index[static_cast<unsigned int>(side)];
  return haloArray_[index];
}

/**
 * Gets the halo bounding box.
 *
 * @param fieldSize  The field size, including boundaries.
 * @param side       The halo side.
 * @param way        The way, inner or outer, or both including the extend to the full side.
 *
 * @return The halo bounding box.
 */
math::BoundingBox<unsigned long> FieldProperties::getHaloBoundingBox(const math::Vector3<unsigned long> fieldSize, const Direction side, const HaloWay way) const {
  math::Vector3<unsigned long> start{};
  math::Vector3<unsigned long> width{};

  int inner = (way == HaloWay::source) ? 1 : 0;
  int both  = (way == HaloWay::both) ? 2 : 1;
  auto dir  = static_cast<unsigned int>(side);
  for (int axis = 0; axis < 3; axis++) {
    if (c(axis, dir) == 0) {
      start[axis] = (2 - both) * boundarySize_[axis];
      width[axis] = fieldSize[axis] - (2 - both) * 2 * boundarySize_[axis];
    } else if (c(axis, dir) == -1) {
      start[axis] = inner * boundarySize_[axis];
      width[axis] = both * boundarySize_[axis];
    } else if (c(axis, dir) == 1) {
      start[axis] = fieldSize[axis] - (inner + both) * boundarySize_[axis];
      width[axis] = both * boundarySize_[axis];
    }
  }

  return math::BoundingBox<unsigned long>{start, start + width};
}

void FieldProperties::setAlignment(unsigned int elements) {
  if (!isAlignable()) {
    throw std::invalid_argument(fmt::format("This field can not be padded. Setting an alignment is not allowed."));
  }

  auto simdAlignment = simd::getInstructionSetNeedsAlignment() ? simd::getInstructionSetRegisterSize() / elementSize_ : 1;

  if (elements % simdAlignment != 0) {
    throw std::invalid_argument(fmt::format("Alignment must be a multiply of the SIMD vector size of {} elements.", simdAlignment));
  }

  alignment_ = elements;

  checkAlignment();
}

}  // namespace field
}  // namespace nastja
