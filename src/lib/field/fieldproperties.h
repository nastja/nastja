/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/block/block.h"
#include "lib/boundary/boundary.h"
#include "lib/boundary/factorybc.h"
#include "lib/simd/simd.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"
#include "lib/stencil/stencil.h"
#include <array>
#include <cassert>
#include <string>

namespace nastja {
namespace field {

struct FieldTypeEnum {
  enum FieldType : unsigned int {
    real    = 0x01,  // A floating point number field: real_t.
    integer = 0x02,  // An integer number field: cellid_t.
    complex = 0x11,  // A flag for complex number field, this is always a real type.

    aligned = 0x20,  // Denotes a field with aligned memory.
    cuda    = 0x40,  // Denotes a cuda field, has always aligned memory.
    splitX  = 0x80,  // Split the x dimension, has always aligned memory.
  };
};
using FieldType = FieldTypeEnum::FieldType;

enum class HaloWay {
  source = 0,  // The inner boundary
  target,      // The outer boundary
  both         // Both boundaries
};

using namespace config::literals;

/**
 * This class describes the properties of a field.
 */
class FieldProperties {
  friend class TESTING_FieldProperties;

public:
  template <unsigned int DIM, unsigned int SIZE>
  explicit FieldProperties(stencil::Stencil<DIM, SIZE> stencil, unsigned int fieldTypeFlags, unsigned int vectorSize = 1, unsigned int timeStepFields = 2)
      : boundarySize_{SIZE > 1 ? math::Vector3<unsigned int>{1, 1, 1} : math::Vector3<unsigned int>{0, 0, 0}},
        vectorSize_{vectorSize},
        fieldTypeFlags_{fieldTypeFlags},
        numberOfSides_{SIZE - (stencil.index[0] == 0)},  // Remove 1, if stencil has a center
        numberOfTimeStepFields_{timeStepFields},
        elementSize_{hasFlag(FieldType::integer) ? sizeof(cellid_t) : sizeof(real_t)},
        alignment_{calcAlignment()} {
    if (!std::is_same<real_t, double>::value && hasFlag(FieldType::complex)) {
      throw std::invalid_argument(fmt::format("Complex fields need real_t = double."));
    }

    if (hasFlag(FieldType::splitX) && hasFlag(FieldType::cuda)) {
      throw std::invalid_argument(fmt::format("Cuda fields does not support splitting of the x coordinate."));
    }
    if (vectorSize == 1 && hasFlag(FieldType::splitX)) {
      logger::get().onewarning("Splitting the x coordinate for a vector of length one will be ignored. You can not use ifield.get<T, true>.");
    }

    checkAlignment();
  }

  ~FieldProperties();
  FieldProperties(const FieldProperties&) = delete;
  FieldProperties(FieldProperties&&)      = default;
  FieldProperties& operator=(const FieldProperties&) = delete;
  FieldProperties& operator=(FieldProperties&&) = delete;

  constexpr bool hasFlag(FieldType ftID) const {
    return (fieldTypeFlags_ & ftID) == ftID;
  }

  std::unique_ptr<IField> createField(block::Block& block);

  void setupHook();

  void registerBoundary(const unsigned int side, Boundary* boundary) {
    outerBoundarySet_[side] = boundary;
    boundary->setup(*simdata_, "Fields"_jptr / name_ / "boundaries" / boundaryNames_[side]);
  }

  Boundary* getBoundary(const unsigned int side) const { return outerBoundarySet_[side]; }
  Boundary* getMPIBoundary() const { return mpiBoundary_; }
  Boundary* getInnerBoundary() const { return innerBoundary_; }

  const std::string& getName() const { return name_; }
  void setName(const std::string& name) { name_ = name; }

  void registerSimData(SimData* const simdata) { simdata_ = simdata; }

  SimData& getSimData() {
    assert(simdata_ != nullptr);
    return *simdata_;
  }

  unsigned int getAlignment() const { return alignment_; }
  void setAlignment(unsigned int alignment);

  math::Vector3<unsigned int> getBoundarySize() const { return boundarySize_; }
  unsigned int getVectorSize() const { return vectorSize_; }

  unsigned int getNumberOfTimeStepFields() const { return numberOfTimeStepFields_; }

  MPI_Datatype& getHaloDataType(HaloWay way, stencil::Direction side);

  math::BoundingBox<unsigned long> getHaloBoundingBox(math::Vector3<unsigned long> fieldSize, stencil::Direction side, HaloWay way) const;
  unsigned int getNumberOfSides() const { return numberOfSides_; }

  MPI_Datatype getMPIType() const;

  constexpr bool hasComplexPadding() const { return hasFlag(FieldType::complex); }

  constexpr bool isAlignable() const { return !hasFlag(FieldType::complex) && needsAlignment(); }
  constexpr bool isCudaDatatype() const { return hasFlag(FieldType::cuda); }
  constexpr bool needsAlignment() const { return hasFlag(FieldType::splitX) || isCudaDatatype() || hasFlag(FieldType::aligned); }

  /**
   * Determines if this field properties include a halo.
   *
   * @return True if it has a halo, false otherwise.
   */
  bool hasHalo() const { return (boundarySize_ != math::Vector3<unsigned int>::zero); }

private:
  /**
   * Calculates the alignment depending on the field properties.
   *
   * Cuda field always has an alignment of 32 elements. Otherwise for fields that can be padded the alignment is
   * calculated from the SIMD register size and the field base type. An alignment of one element is used when there is
   * no SIMD support.
   *
   * @return The alignment in elements.
   */
  unsigned int calcAlignment() const {
    if (isCudaDatatype()) return 32;
    if (!isAlignable()) return 1;

    return simd::getInstructionSetRegisterSize() / elementSize_;
  }

  /**
   * Checks the alignment.
   *
   * For vector fields there are two options, either the x cordinate can be split or the vector must fit to the
   * alignment. This must be done on aplication side, e.g. add a forth unused vector component. Otherwise the alignment
   * is ignored.
   */
  void checkAlignment() {
    if (vectorSize_ > 1 && !hasFlag(FieldType::splitX) && vectorSize_ % alignment_ != 0) {
      logger::get().onewarning("A vector of length {0} can not aligned by {1} elements. Either split the x coordinate or increase the vector length to a multiply of {1}. Alignment will be ignored.", vectorSize_, alignment_);
      alignment_ = 1;
    }
  }

  void setupHalo();
  void setupGpuExchange();

  const math::Vector3<unsigned int> boundarySize_;  ///< The number of boundary layer for each direction.
  const unsigned int vectorSize_;                   ///< The number of vector components in each spatial (x, y, z) position.
  const unsigned int fieldTypeFlags_;               ///< The flags of the field type.
  const unsigned int numberOfSides_;                ///< The number of halo exchange sides.
  const unsigned int numberOfTimeStepFields_;       ///< The number of time-steps the field holds.
  const std::size_t elementSize_;                   ///< The size of one element in bytes.
  unsigned int alignment_;                          ///< The alignment of elements in the fastest coordinate. Default is given by the SIMD alignment.

  Boundary* mpiBoundary_{};
  Boundary* innerBoundary_{};
  std::array<Boundary*, 6> outerBoundarySet_{};

  std::string name_;
  SimData* simdata_ = nullptr;

  const std::array<std::string, 6> boundaryNames_{"lowerx", "upperx", "lowery", "uppery", "lowerz", "upperz"};

  std::array<MPI_Datatype, 52> haloArray_{};

  /*static*/ FactoryBC factoryBC_;
};

}  // namespace field
}  // namespace nastja
