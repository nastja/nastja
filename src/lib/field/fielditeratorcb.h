/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/field/field.h"
#include "lib/math/arithmetic.h"
#include "lib/math/vector.h"
#include "lib/stencil/direction.h"
#include <stdexcept>

namespace nastja {
namespace field {

/**
 * Class for field iterator with two-colored checkerboard.
 *
 * @tparam T   Type of the field data.
 */
template <typename T>
class FieldIteratorCB {
public:
  /**
   * Constructs a default instance, points to the nullptr.
   */
  explicit FieldIteratorCB()
      : coord_{0, 0, 0},
        length_{0, 0, 0},
        start_{0, 0, 0} {}

  /**
   * Constructs the iterator.
   *
   * @param field  Pointer to the field.
   * @param start  Vector of start coordinates.
   * @param end    Vector of end coordinates.
   */
  explicit FieldIteratorCB(Field<T, 1>& field, const math::Vector3<unsigned long> start, const math::Vector3<unsigned long> end)
      : coord_{0, 0, 0},
        length_{end - start},
        start_{start},
        offsetX_{calcOffset(start + field.getOffset() - field.getBoundarySize(), field.getSimData().timestep)},
        shiftX_{xOffset()},
        jumpX_{2 * field.getVectorSize()},
        jumpY_{(field.getPaddedSize(0) - length_[0]) * field.getVectorSize()},
        jumpZ_{((field.getPaddedSize(1) - length_[1]) * field.getPaddedSize(0)) * field.getVectorSize()},
        cellPtr_{field.getCellBasePtr() + field.getIndex(start[0] + offsetX_, start[1], start[2])} {
    if (length_.x() == 0 || length_.y() == 0 || length_.z() == 0) {
      cellPtr_ = nullptr;
      return;
    }

    coord_.x() = shiftX_;
    // check for small domains
    if (coord_.x() < length_.x()) return;
    coord_.x() = (length_.x() % 2 == 0) ? shiftX_ : 1 - shiftX_;  // This is removed in nextY.
    nextY();

    if (coord_.y() < length_.y()) return;
    coord_.y() = 0;
    nextZ();

    if (coord_.z() < length_.z()) return;
    cellPtr_ = nullptr;
  }

  /// @name Operators
  /// @{

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  const FieldIteratorCB& operator++() {
    coord_.x() += 2;
    cellPtr_ += jumpX_;

    return ensurePosition();
  }

  /**
   * Increment operator.
   *
   * @return The result of the increment.
   */
  FieldIteratorCB operator++(int) {
    auto tmp(*this);
    operator++();
    return tmp;
  }

  /**
   * Addition assignment operator.
   *
   * @param step  The number of basic steps, i.e., step = 3 and CB = 2 results in a total jump of 6.
   *
   * @return The result of the addition assignment.
   */
  const FieldIteratorCB& operator+=(const unsigned long step) {
    coord_.x() += 2 * step;
    cellPtr_ += step * jumpX_;

    return ensurePosition();
  }

  /**
   * Equality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the equality.
   */
  bool operator==(const FieldIteratorCB& other) const { return cellPtr_ == other.cellPtr_; }

  /**
   * Inequality operator.
   *
   * @param other  The other iterator.
   *
   * @return The result of the inequality.
   */
  bool operator!=(const FieldIteratorCB& other) const { return cellPtr_ != other.cellPtr_; }
  /// @}

  /// @name Access functions
  /// @{

  /**
   * Access the current cell.
   *
   * @return Pointer to the current cell.
   */
  T* operator*() { return cellPtr_; }
  /// @overload
  const T* operator*() const { return cellPtr_; }

  /**
   * Access the vector component of the current cell.
   *
   * @param i  The component.
   *
   * @return Reference to the component.
   */
  T& operator[](unsigned int i) { return cellPtr_[i]; }
  /// @overload
  const T& operator[](unsigned int i) const { return cellPtr_[i]; }

  /**
   * Access the coordinates.
   *
   * @return Vector of coordinates.
   */
  math::Vector3<unsigned long> coordinates() const { return coord_ + start_; }
  /// @}

private:
  /**
   * Ensures pointer position and coordinates, when x moved over length.
   *
   * @return This iterator.
   */
  const FieldIteratorCB& ensurePosition() {
    if (coord_.x() < length_.x()) return *this;
    do {
      do {
        coord_.x() -= length_.x();
        nextY();
      } while (coord_.x() >= length_.x());

      if (coord_.y() < length_.y()) return *this;
      do {
        coord_.y() -= length_.y();
        nextZ();
      } while (coord_.y() >= length_.y());
    } while (coord_.z() < length_.z() && coord_.x() >= length_.x());  // This happens, when nextZ add one to x.

    if (coord_.z() < length_.z()) return *this;
    cellPtr_ = nullptr;

    return *this;
  }

  /**
   * Proceeds a next y step.
   */
  void nextY() {
    coord_.y()++;
    cellPtr_ += jumpY_;

    if (length_.x() % 2 == 0) {
      coord_.x() -= shiftX_;
      shiftX_ = xOffset();
      coord_.x() += shiftX_;
      if (shiftX_ == 1) {
        cellPtr_ += jumpX_ / 2;
      } else {
        cellPtr_ -= jumpX_ / 2;
      }
    } else {
      coord_.x() -= (1 - shiftX_);
      shiftX_ = xOffset();
      coord_.x() += shiftX_;
    }
  }

  /**
   * Proceeds a next z step.
   */
  void nextZ() {
    coord_.z()++;
    cellPtr_ += jumpZ_;

    if (length_.y() % 2 == 0) {
      coord_.x() -= shiftX_;
      shiftX_ = xOffset();
      coord_.x() += shiftX_;
      if (shiftX_ == 1) {
        cellPtr_ += jumpX_ / 2;
      } else {
        cellPtr_ -= jumpX_ / 2;
      }
    }
  }

  /**
   * Calculates the offset of the first position.
   *
   * @param offset    The offset.
   * @param timeStep  The time-step.
   *
   * @return The offset.
   */
  unsigned long calcOffset(math::Vector3<unsigned long> offset, unsigned long timeStep) const {
    if ((offset.x() + offset.y() + offset.z()) % 2 == timeStep % 2) {
      return 1;
    }

    return 0;
  }

  /**
   * Gets the offset in the current line.
   *
   * @return The position of the first voxel on the checker board
   */
  unsigned long xOffset() {
    return (coord_.y() + coord_.z() + offsetX_) % 2;
  }

  math::Vector3<unsigned long> coord_;  ///< The coordinates to the current cell.

  const math::Vector3<unsigned long> length_;  ///< The length of the field in each direction.
  const math::Vector3<unsigned long> start_;   ///< The start coordinates.
  const unsigned long offsetX_{};              ///< The offset for the 2-color checkerboard block start.
  unsigned long shiftX_{};                     ///< The offset for the 2-color checkerboard current line.
  const unsigned long jumpX_{};                ///< The number of elements jump over to reach the next element when x changes.
  const unsigned long jumpY_{};                ///< The number of elements jump over to reach the next element when y changes.
  const unsigned long jumpZ_{};                ///< The number of elements jump over to reach the next element when z changes.

  T* cellPtr_{};  ///< The pointer to the current cell.
};

}  // namespace field
}  // namespace nastja
