/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/simd/simd.h"
#include <algorithm>
#include <new>
#include <stdexcept>
#include <type_traits>

namespace nastja {
namespace field {

template <typename T>
typename std::enable_if<std::is_fundamental<T>::value, T*>::type allocate(std::size_t size) {
  const std::size_t alignment = simd::getInstructionSetNeedsAlignment() ? simd::getInstructionSetRegisterSize() : sizeof(void*);

  void* raw_ptr{nullptr};

  if (posix_memalign(&raw_ptr, alignment, size * sizeof(T))) {
    throw std::bad_alloc();
  }

  return static_cast<T*>(raw_ptr);
}

template <typename T>
typename std::enable_if<std::is_fundamental<T>::value>::type deallocate(T* ptr) noexcept {
  if (ptr != nullptr) free(ptr);
}

}  // namespace field
}  // namespace nastja
