/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/logger.h"
#include "fmt/format.h"
#include <chrono>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

namespace nastja {

using TimingTuple = std::tuple<std::size_t, std::size_t, std::size_t, std::size_t, std::size_t>;
struct TimingTupleEnum {
  enum TimingTupleIndex { calls = 0,
                          totalTime,
                          totalSqTime,
                          minTime,
                          maxTime };
};
using TimingTupleIndex = TimingTupleEnum::TimingTupleIndex;

class FunctionTimer {
public:
  explicit FunctionTimer(std::string name)
      : name_{std::move(name)},
        startTime_{static_cast<std::size_t>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count())} {
  }

  ~FunctionTimer() {
    std::size_t usedTime = static_cast<std::size_t>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count()) - startTime_;
    TimingTuple& tuple   = functionTimes[name_];
    std::get<TimingTupleIndex::calls>(tuple)++;
    std::get<TimingTupleIndex::totalTime>(tuple) += usedTime;
    std::get<TimingTupleIndex::totalSqTime>(tuple) += usedTime * usedTime;
    if (std::get<TimingTupleIndex::calls>(tuple) == 1) {
      std::get<TimingTupleIndex::minTime>(tuple) = usedTime;
      std::get<TimingTupleIndex::maxTime>(tuple) = usedTime;
    } else {
      std::get<TimingTupleIndex::minTime>(tuple) = std::min(std::get<TimingTupleIndex::minTime>(tuple), usedTime);
      std::get<TimingTupleIndex::maxTime>(tuple) = std::max(std::get<TimingTupleIndex::maxTime>(tuple), usedTime);
    }
  }
  FunctionTimer(const FunctionTimer&)     = delete;
  FunctionTimer(FunctionTimer&&) noexcept = delete;
  FunctionTimer& operator=(const FunctionTimer&) = delete;
  FunctionTimer& operator=(FunctionTimer&&) = delete;

  static void report(const std::string& path = "./", int groupSize = 0) {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    auto filename = fmt::format("{}timing-{:05d}.dat", path, rank);
    auto& logger  = logger::registerLogger("timings", filename, groupSize);
    logger.setFormatAll("");  // Timestep
    logger.info(reportString().c_str());
  }

private:
  static std::string reportString() {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    std::string output = fmt::format("{:<25} {:>4} {:>10} {:>10} {:>13} {:>8} {:>8} {:>13}\n",
                                     "#Name", "Rank", "Calls", "TotalTime", "AverageTime",
                                     "MinTime", "MaxTime", "Variance");
    for (const auto& i : functionTimes) {
      double average  = (double)std::get<TimingTupleIndex::totalTime>(i.second) / std::get<TimingTupleIndex::calls>(i.second);
      double variance = (double)std::get<TimingTupleIndex::totalSqTime>(i.second) / std::get<TimingTupleIndex::calls>(i.second) - average * average;
      output += fmt::format("{:<25} {:4} {:10} {:10} {:13.5f} {:8} {:8} {:13.5f}\n",
                            i.first,
                            rank,
                            std::get<TimingTupleIndex::calls>(i.second),
                            std::get<TimingTupleIndex::totalTime>(i.second),
                            average,
                            std::get<TimingTupleIndex::minTime>(i.second),
                            std::get<TimingTupleIndex::maxTime>(i.second),
                            variance);
    }

    return output;
  }

  std::string name_;       ///< The name of thise function timer.
  std::size_t startTime_;  ///< The time when timer is started.

  static std::map<std::string, TimingTuple> functionTimes;  ///< The map of function timer names and timing data.
};

}  // namespace nastja
