/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/datatypes.h"
#include "lib/logger.h"
#include "lib/math/vector.h"
#include "absl/container/flat_hash_map.h"
#include "robin_hood.h"
#include <array>
#include <cmath>
#include <unordered_map>

namespace nastja {

constexpr unsigned int numberOfSignals = 3;

struct Cell {
  /**
   * Sets the current cell values from another cell.
   *
   * @param other  The other cell.
   */
  void set(const Cell& other) {
    volume            = other.volume;
    surface           = other.surface;
    signal            = other.signal;
    center            = other.center;
    oldCenter         = other.oldCenter;
    type              = other.type;
    birth             = other.birth;
    polarity          = other.polarity;
    motilityDirection = other.motilityDirection;
    generation        = other.generation;
    inhibitionStrength = other.inhibitionStrength; //TODO add this to cellinfo
  }

  /**
   * Updates the cell values that are determined on the block with cell center from the other cell.
   *
   * @param other  The other.
   */


  /**
   * Adds delta values to the current cell values, and checks whether motilityDirection or inhibitionStrength needs to be updated.
   *
   * @param other  The other cell.
   */
  void addDeltas(const Cell& other) {
    volume += other.deltaVolume;
    surface += other.deltaSurface;

    for (unsigned int j = 0; j < numberOfSignals; j++) {
      real_t deltaSignal = other.deltaSignal[j];
      if (std::abs(deltaSignal) < 500) {  // TODO This is a fix that catches flux values in an unrealistic magnitude, should not happen but does, WHERE DOES THOSE FRAGMENTS COME FROM?
        signal[j] += deltaSignal;
      } else {
        signal[j] += 0.;
        //logger::get("cellevents").info(R"("info": "Caught abs(deltaSignal) > 500, do not use that value.")");
        //throw std::invalid_argument( "Caught abs(deltaSignal) > 500." );
      }
      if ( signal[j] < 0 ){
        signal[j] = 0;
        //logger::get("cellevents").info(R"("info": "Warning: caught signal < 0, setting to 0 instead. Make sure that your diffusion constant is not too large!")");
        // throw std::invalid_argument( "Caught signal < 0." );
      }
    }



    // Update motilityDireciton if necessary
    math::Vector3<real_t> compareVector{-9999., -9999., -9999. };

      if(other.newMotilityDirection != compareVector){
        motilityDirection = other.newMotilityDirection;
    }

    if (other.newInhibitionStrength != -1.) {
      inhibitionStrength = other.newInhibitionStrength;
    }
    newInhibitionStrength = -1.;
    
    // Set newMotilityDirection to default value
    newMotilityDirection = {-9999.,-9999.,-9999.};

    // Update polarity if necessary
    compareVector = {0., 0., 0. };
    if(other.newPolarity != compareVector){
        polarity = other.newPolarity;
    }
    newPolarity = {0., 0., 0. };



  }

  void pack(Archive& ar) const {
    ar.pack(volume);
    ar.pack(deltaVolume);
    ar.pack(surface);
    ar.pack(deltaSurface);
    ar.pack(signal);
    ar.pack(deltaSignal);
    ar.pack(center);
    ar.pack(oldCenter);
    ar.pack(centerReal);
    ar.pack(oldCenterReal);
    ar.pack(type);
    ar.pack(birth);
    ar.pack(motilityDirection);
    ar.pack(newMotilityDirection);
    ar.pack(polarity);
    ar.pack(generation);
    ar.pack(inhibitionStrength);
    ar.pack(newInhibitionStrength);
  }

  void unpack(Archive& ar) {
    ar.unpack(volume);
    ar.unpack(deltaVolume);
    ar.unpack(surface);
    ar.unpack(deltaSurface);
    ar.unpack(signal);
    ar.unpack(deltaSignal);
    ar.unpack(center);
    ar.unpack(oldCenter);
    ar.unpack(centerReal);
    ar.unpack(oldCenterReal);
    ar.unpack(type);
    ar.unpack(birth);
    ar.unpack(motilityDirection);
    ar.unpack(newMotilityDirection);
    ar.unpack(polarity);
    ar.unpack(generation);
    ar.unpack(inhibitionStrength);
    ar.unpack(newInhibitionStrength);
  }

  void dump(Archive& ar) const {
    ar.pack(volume);
    ar.pack(surface);
    ar.pack(signal);
    ar.pack(type);
    ar.pack(birth);
    ar.pack(center);
    ar.pack(oldCenter);
    ar.pack(centerReal);
    ar.pack(oldCenterReal);
    ar.pack(neighbors);
    ar.pack(voxelsInBlock);
    ar.pack(motilityDirection);
    ar.pack(newMotilityDirection);
    ar.pack(polarity);
    ar.pack(generation);
    ar.pack(inhibitionStrength);
    ar.pack(newInhibitionStrength);    
  }

  void undump(Archive& ar) {
    ar.unpack(volume);
    ar.unpack(surface);
    ar.unpack(signal);
    ar.unpack(type);
    ar.unpack(birth);
    ar.unpack(center);
    ar.unpack(oldCenter);
    ar.unpack(centerReal);
    ar.unpack(oldCenterReal);
    ar.unpack(neighbors);
    ar.unpack(voxelsInBlock);
    ar.unpack(motilityDirection);
    ar.unpack(newMotilityDirection);
    ar.unpack(polarity);
    ar.unpack(generation);
    ar.unpack(inhibitionStrength);
    ar.unpack(newInhibitionStrength);
  }

  void clearDeltas() {
    deltaVolume  = 0.0;
    deltaSurface = 0.0;
    deltaSignal.fill(0.0);
  }

  void clearGeometry() {
    volume        = 0.0;
    surface       = 0.0;
    voxelsInBlock = 0;
  }

  real_t volume{};                                    ///< The volume.
  real_t deltaVolume{};                               ///< The delta of the volume.
  real_t surface{};                                   ///< The surface.
  real_t deltaSurface{};                              ///< The delta of the surface.
  long voxelsInBlock{};                               ///< The number of voxels in the current block
  std::array<real_t, numberOfSignals> signal{0.};       ///< The signals.
  std::array<real_t, numberOfSignals> deltaSignal{0.};  ///< The delta of the signals.

#ifdef __FAST_MATH__
  absl::flat_hash_map<cellid_t, int> neighbors;  ///< The local fraction of the common surface with the neighbor.
#else
  robin_hood::unordered_map<cellid_t, int> neighbors;  ///< The local fraction of the common surface with the neighbor.
#endif

  long birth{0};              ///< The time-step of the birth.
  int type{-1};               ///< The cell type.
  int generation{0}; ///< The generation of the cell (how many cell divisions)
  real_t inhibitionStrength{0.};///< The strength of contact inhibition exhibited onto this cell by its neighbors.
  real_t newInhibitionStrength{-1.};///< The strength of contact inhibition exhibited onto this cell by its neighbors.

  math::Vector3<int>
      center{-1, -1, -1};  ///< The last calculated center.
  math::Vector3<int>
      oldCenter{-1, -1, -1};  ///< The last calculated center.

  math::Vector3<real_t> centerReal{-std::numeric_limits<real_t>::infinity(),
                                   -std::numeric_limits<real_t>::infinity(),
                                   -std::numeric_limits<real_t>::infinity()};  ///< The last calculated center as real.

  math::Vector3<real_t> oldCenterReal{-std::numeric_limits<real_t>::infinity(),
                                      -std::numeric_limits<real_t>::infinity(),
                                      -std::numeric_limits<real_t>::infinity()};  ///< Old calculated center is updated by cellorientation if activated.

  math::Vector3<real_t> polarity{0, 1., 0};              ///< Direction Vector of the polarity (cell shape extension).
  math::Vector3<real_t> newPolarity{0, 0, 0};           ///< Direction Vector of the polarity (cell shape extension).

  math::Vector3<real_t> motilityDirection{0, 0, 0};     ///< Direction Vector of the Motility (cell movement).
  math::Vector3<real_t> newMotilityDirection{-9999,-9999,-9999};  ///< Direction Vector of the Motility (cell movement).
};

}  // namespace nastja
