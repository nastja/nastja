/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/datatypes.h"
#include "lib/pool.h"
#include "lib/action/action.h"
#include "lib/action/actionregister.h"
#include "lib/block/block.h"
#include "lib/config/cmdline.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"
#include <bitset>
#include <list>
#include <set>
#include <tuple>
#include <vector>

namespace nastja {

class CommManager;

using BlockRankSet   = std::set<std::pair<int, int>>;
using VectorPairInts = std::vector<std::pair<int, int>>;

namespace block {

class BlockManager {
public:
  explicit BlockManager(SimData& simdata);
  ~BlockManager();
  BlockManager(const BlockManager&)     = delete;
  BlockManager(BlockManager&&) noexcept = default;
  BlockManager& operator=(const BlockManager&) = delete;
  BlockManager& operator=(BlockManager&&) = delete;

  Block* createBlock(unsigned int x, unsigned int y, unsigned int z);
  Block* createBlock(unsigned int index);
  Block* createInitialBlock(unsigned int index, const std::vector<int>& blockIDArray, const std::vector<int>& blockRankArray);

  Block* createNewBlock(int blockID, const CommManager& commManager);
  void createNewBlocks(const VectorPairInts& newBlockIDTypes, const CommManager& commManager);
  std::set<int> deleteBlocks(const std::set<int>& delBlockIDs);

  void registerAction(const std::string& name, unsigned int offset = 0, bool addToPostLoop = false);
  void deregisterAction(const std::string& name);
  void registerActionWriters(const std::string& fieldname = "", unsigned int offset = 0, bool addToPostLoop = false);

  void initBlocks() const;
  void executeBlocks();
  void executeBlocksPostLoop();

  void executeActionOnBlocks(const std::string& name);
  void executeActionWriterOnBlocks();
  void setupActions();

  int getBlocks(const unsigned int dimension) const { return blocks_[dimension]; }

  unsigned int getRelativeBlockID(unsigned int base, stencil::Direction side) const;
  unsigned int getRelativeBlockID(unsigned int base, int x, int y, int z) const;

  void getCoordinatesFromID(unsigned int id, unsigned int* x, unsigned int* y, unsigned int* z) const;
  std::tuple<unsigned int, unsigned int, unsigned int> getCoordinatesFromID(unsigned int id) const;
  unsigned int getIDFromCoordinates(unsigned int x, unsigned int y, unsigned int z) const;

  std::tuple<int, int, int> getRelativeDifference(unsigned int id1, unsigned int id2) const;

  bool isRankInNeighborhood(int rank) const;

  std::set<Block*> updateBlocks(unsigned int id, int rank, const CommManager& commManager);
  std::set<Block*> removeBlock(unsigned int id);

  void collectNeighbors(Block* newBlock, const CommManager& commManager);
  void updateNeighbors(int blockID, const CommManager& commManager);
  std::set<int> getUnusedRanks(const std::set<int>& usedRanks) const;

  bool isInNeighborhood(unsigned int id) const;
  bool isInNeighborhood(unsigned int id, const std::set<int>& list) const;

  static bool isInsideStar(int x, int y, int z);
  bool isInsideStar(const std::tuple<int, int, int>& coordinates) const;
  bool isInsideStar(unsigned int id1, unsigned int id2) const;

  block::BlockRegister& getBlockSet() { return blockRegister_; };

  int getMPISize() const { return simdata_.mpiData.getSize(); }
  int getMPIRank() const { return simdata_.mpiData.getRank(); }
  long getTimestep() const { return simdata_.timestep; }
  const config::CommandLine& getCmdLine() const { return simdata_.getCmdLine(); }

  ActionRegister& getActionRegister() { return actionRegister_; }
  Pool<field::FieldProperties>& getFieldPool() { return fieldpool_; }
  Pool<Action>& getActionPool() { return actionpool_; }

  Action* getAction(const std::string& name) { return actionpool_.getElement(name); }
  bool hasAction(const std::string& name) { return actionpool_.hasElement(name); }

  void dbgPrintBlockCommInfo() const;

  void dump(Archive& ar) const {
    actionRegister_.dump(ar);
  }

  void undump(Archive& ar) {
    actionRegister_.undump(ar);
  }

private:
  SimData& simdata_;                                  ///< Reference to the SimData.
  Pool<field::FieldProperties> fieldpool_{simdata_};  ///< The pool of all available field types.
  Pool<Action> actionpool_{simdata_};                 ///< The pool of all available actions.

  math::Vector3<unsigned int> blockSize_;  ///< The Vector of voxels per block in each direction.
  math::Vector3<unsigned int> blocks_;     ///< The Vector of blocks in each direction.

  block::BlockRegister blockRegister_;     ///< The register of local blocks.
  ActionRegister actionRegister_;          ///< The register of all actions that are performed in a time-step.
  ActionRegister actionRegisterPostLoop_;  ///< The register of all actions that have to be performed after the last time-step to be in sync.
};

}  // namespace block
}  // namespace nastja
