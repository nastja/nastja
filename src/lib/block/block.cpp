/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "block.h"
#include "lib/datacontainer/dynamicblockcontainer.h"
#include "lib/field/field.h"
#include "lib/stencil/D3C6.h"
#include <algorithm>

namespace nastja {
namespace block {

using namespace stencil;

constexpr unsigned int neighborOffsetZ = 25;
constexpr unsigned int neighborOffsetY = 5;
constexpr unsigned int neighborCount   = 125;

Block::Block(SimData& simdata)
    : simdata{simdata} {
  std::fill(localNeighborRank_.begin(), localNeighborRank_.end(), -1);
  std::fill(localNeighborID_.begin(), localNeighborID_.end(), -1);
}

field::IField* Block::getFieldByName(const std::string& name) const {
  return fields.getElement(name);
}

void Block::setNeighbor(const std::vector<int>& blockIDArray, const std::vector<int>& blockRankArray) {
  for (unsigned int zlocal = 0; zlocal < 5; zlocal++) {
    for (unsigned int ylocal = 0; ylocal < 5; ylocal++) {
      for (unsigned int xlocal = 0; xlocal < 5; xlocal++) {
        unsigned int xglobal = ((xlocal + blockOffset[0] - 2) + blocks[0]) % blocks[0];
        unsigned int yglobal = ((ylocal + blockOffset[1] - 2) + blocks[1]) % blocks[1];
        unsigned int zglobal = ((zlocal + blockOffset[2] - 2) + blocks[2]) % blocks[2];

        unsigned int blockIndex = zglobal * (blocks[0] * blocks[1]) + yglobal * blocks[0] + xglobal;

        localNeighborID_[zlocal * neighborOffsetZ + ylocal * neighborOffsetY + xlocal]   = blockIDArray[blockIndex];
        localNeighborRank_[zlocal * neighborOffsetZ + ylocal * neighborOffsetY + xlocal] = blockRankArray[blockIndex];
      }
    }
  }
}

/**
 * Update local neighborhood with information of specific id and rank.
 *
 * @param x    relative x
 * @param y    relative y
 * @param z    relative z
 * @param id   id
 * @param rank rank
 */
void Block::updateLocalNeighbor(const int x, const int y, const int z, const unsigned int id, const int rank) {
  localNeighborID_[(z + 2) * neighborOffsetZ + (y + 2) * neighborOffsetY + (x + 2)]   = id;
  localNeighborRank_[(z + 2) * neighborOffsetZ + (y + 2) * neighborOffsetY + (x + 2)] = rank;
}

bool Block::removeBlockFromLocalNeighbor(const unsigned int id) {
  bool found = false;

  for (unsigned int i = 0; i < neighborCount; i++) {
    if (localNeighborID_[i] != (int)id) continue;

    localNeighborID_[i]   = -1;
    localNeighborRank_[i] = -1;

    found = true;
  }

  return found;
}

void Block::updateFieldBoundaries() {
  for (const auto& field : fields.poolSet) {
    field.second->updateAllBoundaries();
  }
}

/**
 * Gives a set of all ranks of this block.
 *
 * @return set of all ranks.
 */
std::set<int> Block::getRanks() const {
  std::set<int> ranks;

  for (auto rank : localNeighborRank_) {
    if (rank >= 0) ranks.insert(rank);
  }

  return ranks;
}

/**
 * Checks for the rank in the neighborhood.
 *
 * @param rank The rank to check.
 *
 * @return true if found, false else.
 */
bool Block::isRankInNeighborhood(const int rank) const {
  return std::any_of(localNeighborRank_.begin(), localNeighborRank_.end(), [&](int r) { return r == rank; });
}

/**
 * Gives the local Rank from the local data field relative to the given parameter.
 *
 * @param  side relative side
 * @return      relative rank
 */
int Block::getLocalNeighborRank(const Direction side) const {
  return getLocalNeighborRank(0, 0, 0, side);
}

/**
 * @overload
 * @param  x relative x
 * @param  y relative y
 * @param  z relative z
 * @return   relative rank
 */
int Block::getLocalNeighborRank(const int x, const int y, const int z) const {
  return localNeighborRank_[(z + 2) * neighborOffsetZ + (y + 2) * neighborOffsetY + (x + 2)];
}

/**
 * @overload
 * @param  x    relative x
 * @param  y    relative y
 * @param  z    relative z
 * @param  side relative side
 * @return      relative rank
 */
int Block::getLocalNeighborRank(const int x, const int y, const int z, const Direction side) const {
  return getLocalNeighborRank(x + cx(side), y + cy(side), z + cz(side));
}

/**
 * Gives the local ID from the local data field relative to the given parameter.
 *
 * @param  side relative side
 * @return      relative ID
 */
int Block::getLocalNeighborID(const Direction side) const {
  return getLocalNeighborID(0, 0, 0, side);
}

/**
 * @overload
 * @param  x    relative x
 * @param  y    relative y
 * @param  z    relative z
 * @return      relative ID
 */
int Block::getLocalNeighborID(const int x, const int y, const int z) const {
  return localNeighborID_[(z + 2) * neighborOffsetZ + (y + 2) * neighborOffsetY + (x + 2)];
}

/**
 * @overload
 * @param  x    relative x
 * @param  y    relative y
 * @param  z    relative z
 * @param  side relative side
 * @return      relative ID
 */
int Block::getLocalNeighborID(const int x, const int y, const int z, const Direction side) const {
  return getLocalNeighborID(x + cx(side), y + cy(side), z + cz(side));
}

/**
 * Count the number of direct neighbors on the given rank.
 *
 * @param rank Rank for counting.
 *
 * @return number of direct neighbors on rank.
 */
int Block::countNeighbors(const int rank) const {
  int sum = 0;

  for (auto side : D3C6()) {
    if (getLocalNeighborRank(D3C6::dir[side]) == rank) sum++;
  }

  return sum;
}

/**
 * Count the number of neighbors on the given rank and give the fraction of this.
 *
 * @param rank Rank for counting.
 *
 * @return fraction of neighbors on rank.
 */
double Block::fractionNeighbors(const int rank) const {
  int sum = 0;

  for (auto r : localNeighborRank_) {
    if (r == rank) return sum++;
  }

  return (double)sum / (double)neighborCount;
}

/**
 * Pack the block data.
 * The first part is the ID, this is read outside.
 *
 * @param ar Reference to the Archive.
 */
void Block::pack(Archive& ar) const {
  ar.pack(id);
  auto& field = *getFieldByName("phasefield");
  ar.pack(field);
}

/**
 * Unpack the block data.
 *
 * @param ar Reference to the Archive.
 */
void Block::unpack(Archive& ar) {
  auto& field = *getFieldByName("phasefield");
  ar.unpack(field);

  // TODO: pack and unpack DynamicData
  auto& dataDynBlock = getDataContainer<DynamicBlockContainer>();
  dataDynBlock.ttl   = 2;  // BUG: this block will not be deleted directly, wait for the first boundary in the current field.
}

/**
 * Gets the coordinates from id.
 *
 * @param id  The identifier.
 *
 * @return A vector of the coordinates from id.
 */
math::Vector3<unsigned int> Block::getCoordinatesFromID(const unsigned int id) const {
  math::Vector3<unsigned int> v;
  unsigned int index{};

  v[2]  = id / (blocks[0] * blocks[1]);
  index = id - blocks[0] * blocks[1] * v[2];
  v[1]  = index / blocks[0];
  v[0]  = index % blocks[0];

  return v;
}

/**
 * Gives a bit field of periodic and neighborhood from two block IDs.
 *
 * @note Only for periodic boundary
 *
 * @param baseID      The ID of the basis block
 * @param neighborID  The ID of the second block
 *
 * @return bit pattern 0brqpZzYzXx: xlower (x), xupper(X), x-periodic(p), …
 */
unsigned int Block::getNeighborDirection(const unsigned int baseID, const unsigned int neighborID) const {
  auto base     = getCoordinatesFromID(baseID);
  auto neighbor = getCoordinatesFromID(neighborID);

  unsigned int ret{};

  for (unsigned int axis = 0; axis < 3; axis++) {
    if (base[axis] == neighbor[axis]) continue;

    if (base[axis] - 1 == neighbor[axis] || (base[axis] == 0 && (neighbor[axis] == blocks[axis] - 1))) {
      ret |= (0b01u << (2 * axis));
    } else if (base[axis] + 1 == neighbor[axis] || ((base[axis] == blocks[axis] - 1) && neighbor[axis] == 0)) {
      ret |= (0b10u << (2 * axis));
    } else if (base[axis] != neighbor[axis]) {
      return 0;
    }
  }

  for (unsigned int axis = 0; axis < 3; axis++) {
    if (base[axis] == neighbor[axis]) continue;

    if ((base[axis] == 0 && (neighbor[axis] == blocks[axis] - 1)) || ((base[axis] == blocks[axis] - 1) && neighbor[axis] == 0)) {
      ret |= 0b1u << (6 + axis);
    }
  }

  return ret;
}

/**
 * check for coordinate inside of the block
 *
 * @param x x-coordinate
 * @param y y-coordinate
 * @param z z-coordinate
 *
 * @return true if inside, else false
 */
bool Block::isGlobalCoordinateInBlock(const unsigned int x, const unsigned int y, const unsigned int z) const {
  return x >= offset[0] && x < offset[0] + size[0] &&
         y >= offset[1] && y < offset[1] + size[1] &&
         z >= offset[2] && z < offset[2] + size[2];
}

}  // namespace block
}  // namespace nastja
