/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "blockmanager.h"
#include "lib/logger.h"
#include "lib/timing.h"
#include "lib/action/dynamicblock.h"
#include "lib/field/field.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <cassert>
#include <iterator>
#include <sstream>
#include <stdexcept>

namespace nastja {

using namespace stencil;
using namespace config::literals;

namespace block {

BlockManager::BlockManager(SimData& simdata)
    : simdata_{simdata} {
  simdata.registerBlockmanager(this);

  auto& config = simdata.getConfig();

  /// @key{Geometry.blocksize, Vector3<uint>}
  /// Size of one block.
  config.load1DArray(blockSize_, "Geometry.blocksize"_jptr);

  /// @key{Geometry.blockcount, Vector3<uint>}
  /// Number of blocks.
  config.load1DArray(blocks_, "Geometry.blockcount"_jptr);

  int defaultBlock = 1;
  /// @key{Geometry.blockdefault, string, "fill"}
  /// @values{"empty", "full"}
  /// Default of a block.
  if (config.getValue<std::string>("Geometry.blockdefault"_jptr, "fill") == "empty") {
    defaultBlock = 0;
  }

  unsigned int numOfBlocks = blocks_[0] * blocks_[1] * blocks_[2];
  if (numOfBlocks > simdata.mpiData.getMaxBlockID()) {
    throw std::runtime_error(fmt::format("The simulation needs {} block IDs, MPITagBitsForBlockID = {} only supports {}. You should consider increasing it.", numOfBlocks, MPITagBitsForBlockID, simdata.mpiData.getMaxBlockID()));
  }

  std::vector<int> blockarray(numOfBlocks);

  /// @key{Geometry.blocktype, int[][][], Geometry.blockdefault}
  /// Initial array of blocks.
  config.load3DArray(blockarray, blocks_, "Geometry.blocktype"_jptr, defaultBlock);

  unsigned int numOfActiveBlocks = 0;
  for (unsigned int i = 0; i < numOfBlocks; i++) {
    if (blockarray[i] != 0) numOfActiveBlocks++;
  }

  unsigned int blocksPerWorker = (numOfActiveBlocks) / simdata.mpiData.getSize();
  if (simdata.mpiData.getSize() * blocksPerWorker < (numOfActiveBlocks)) {
    blocksPerWorker++;
  }

  logger::get().oneinfo("Start simulation with {} processes on {}x{}x{} ({}) blocks with {} active blocks with {} blocks/process.", simdata.mpiData.getSize(), blocks_[0], blocks_[1], blocks_[2], numOfBlocks, numOfActiveBlocks, blocksPerWorker);

  // initial distribute blocks to worker
  std::vector<int> blockRankArray(numOfBlocks);
  std::vector<int> blockIDArray(numOfBlocks);

  unsigned int blockCnt    = 0;
  unsigned int workerIndex = 0;
  for (unsigned int index = 0; index < numOfBlocks; index++) {
    if (blockarray[index] != 0) {  // active block
      if (blockCnt >= blocksPerWorker) {
        workerIndex++;
        blockCnt = 0;
      }
      blockCnt++;
      blockRankArray[index] = workerIndex;
      blockIDArray[index]   = index;
    } else {
      blockRankArray[index] = -1;
      blockIDArray[index]   = -1;
    }
  }

  // register local blocks
  for (unsigned int index = 0; index < numOfBlocks; index++) {
    if (blockRankArray[index] == simdata.mpiData.getRank()) {
      createInitialBlock(index, blockIDArray, blockRankArray);
    }
  }
}

BlockManager::~BlockManager() {
  for (auto block : blockRegister_) {
    delete block.second;
  }
}

Block* BlockManager::createBlock(const unsigned int x, const unsigned int y, const unsigned int z) {
  auto block = new Block(simdata_);

  block->offset[0] = x * blockSize_[0];
  block->offset[1] = y * blockSize_[1];
  block->offset[2] = z * blockSize_[2];

  block->blockOffset[0] = x;
  block->blockOffset[1] = y;
  block->blockOffset[2] = z;

  block->size[0] = blockSize_[0];
  block->size[1] = blockSize_[1];
  block->size[2] = blockSize_[2];

  block->blocks[0] = blocks_[0];
  block->blocks[1] = blocks_[1];
  block->blocks[2] = blocks_[2];

  block->id                = getIDFromCoordinates(x, y, z);
  blockRegister_[block->id] = (block);

  return block;
}

Block* BlockManager::createBlock(const unsigned int index) {
  unsigned int x;
  unsigned int y;
  unsigned int z;
  std::tie(x, y, z) = getCoordinatesFromID(index);

  return createBlock(x, y, z);
}

Block* BlockManager::createInitialBlock(const unsigned int index, const std::vector<int>& blockIDArray, const std::vector<int>& blockRankArray) {
  unsigned int x;
  unsigned int y;
  unsigned int z;
  std::tie(x, y, z) = getCoordinatesFromID(index);

  Block* block = createBlock(x, y, z);
  block->setNeighbor(blockIDArray, blockRankArray);

  return block;
}

/**
 * Registers the given action from the actionPool to the actionRegister.
 *
 * @param name           The name of the action.
 * @param offset         An optional offset for the timestep.
 * @param addToPostLoop  Flag, add additional to the post loop.
 */
void BlockManager::registerAction(const std::string& name, const unsigned int offset /*= 0*/, const bool addToPostLoop /*= false*/) {
  auto* action = actionpool_.getElement(name);

  action->setOffset(offset);

  actionRegister_.registerAction(action);

  if (addToPostLoop) {
    actionRegisterPostLoop_.registerAction(action);
  }
}

/**
 * Registers all io actions from the actionPool. Io actions already registered are skipped. The iodump action is
 * registerd last.
 *
 * @param fieldname      Optional filter by the fieldname.
 * @param offset         An optional offset for the timestep.
 * @param addToPostLoop  Flag, add additional to the post loop.
 */
void BlockManager::registerActionWriters(const std::string& fieldname /*= ""*/, const unsigned int offset /*= 0*/, const bool addToPostLoop /*= false*/) {
  for (auto& action : actionpool_) {
    if (action.second->getType() != ActionType::io) continue;
    if (action.second->isRegisterd) continue;

    if (!fieldname.empty() && fieldname != action.second->getFieldname()) continue;

    registerAction(action.second->getName(), offset, addToPostLoop);
  }

  // Looking for iodump when no filter is set
  if (!fieldname.empty()) return;

  for (auto& action : actionpool_) {
    if (action.second->getType() != ActionType::iodump) continue;
    if (action.second->isRegisterd) continue;

    registerAction(action.second->getName(), offset, addToPostLoop);
  }
}

void BlockManager::deregisterAction(const std::string& name) {
  actionRegister_.deregisterAction(name);
}

void BlockManager::initBlocks() const {
  actionRegister_.initBlocks(blockRegister_, fieldpool_);
}

/**
 * Collect the IDs and ranks for neighboring blocks.
 * If the commManager knows the ID and rank, this will be added to the localNeighbor of the block.  But only if the
 * block is not in the announce list of the communicator.
 *
 * @param newBlock    The new block
 * @param commManager Reference to CommManager
 */
void BlockManager::collectNeighbors(Block* const newBlock, const CommManager& commManager) {
  for (int z = -2; z <= 2; z++) {
    for (int y = -2; y <= 2; y++) {
      for (int x = -2; x <= 2; x++) {
        int id = getRelativeBlockID(newBlock->id, x, y, z);  // search for this ID

        int rank = commManager.getRankByID(id);

        if (rank >= 0 && !commManager.isInAnnounce(newBlock->id, rank)) {
          newBlock->updateLocalNeighbor(x, y, z, id, rank);
        }
      }
    }
  }
}

/**
 * Update the neighboring blocks by collect the IDs and ranks.
 * If the commManager knows the ID and rank, this will be added to the localNeighbor of the block.  But only if the
 * block is not in the announce list of the communicator.  Use this for announced block.  It also update the boundaries.
 *
 * @param blockID     The ID of the block
 * @param commManager Reference to CommManager
 */
void BlockManager::updateNeighbors(const int blockID, const CommManager& commManager) {
  if (blockRegister_.find(blockID) == blockRegister_.end()) {
    logger::get().error("Block {} does not exist anymore.", blockID);  // BUG: this will crash.
  }
  for (int z = -2; z <= 2; z++) {
    for (int y = -2; y <= 2; y++) {
      for (int x = -2; x <= 2; x++) {
        int id = getRelativeBlockID(blockID, x, y, z);  // search for this ID

        int rank = commManager.getRankByID(id);

        if (rank >= 0 && !commManager.isInAnnounce(blockID, rank)) {
          blockRegister_[blockID]->updateLocalNeighbor(x, y, z, id, rank);
        }
      }
    }
  }

  blockRegister_[blockID]->updateFieldBoundaries();
}

/**
 * Create a new Block with given ID.
 *
 * @param blockID     ID of the new block.
 * @param commManager Reference to the CommManager.
 *
 * @return Pointer to the Block.
 */
Block* BlockManager::createNewBlock(const int blockID, const CommManager& commManager) {
  Block* newBlock = createBlock(blockID);

  collectNeighbors(newBlock, commManager);

  actionRegister_.initBlock(newBlock, fieldpool_);

  return newBlock;
}

void BlockManager::createNewBlocks(const VectorPairInts& newBlockIDTypes, const CommManager& commManager) {
  for (const auto& block : newBlockIDTypes) {
    Block* newBlock = createNewBlock(block.first, commManager);

    // TODO It's only a hack
    if (block.second == 1) {
      auto& field = newBlock->getFieldByName("phasefield")->get<real_t>();

      if (newBlock->hasDataContainer<DynamicBlockContainer>()) {  //TODO sehr unschön
        auto& data       = newBlock->getDataContainer<DynamicBlockContainer>();
        data.addedVolume = field.getVolume();
      }

      for (unsigned int z = 0; z < field.getSize(2); z++) {
        for (unsigned int y = 0; y < field.getSize(1); y++) {
          for (unsigned int x = 0; x < field.getSize(0); x++) {
            for (unsigned int v = 0; v < field.getVectorSize(); v++) {
              field.getCell(x, y, z, v) = 1.0;
            }
          }
        }
      }
    }
  }
}

/**
 * Delete Blocks.
 *
 * @param delBlockIDs set of all IDs to be deleted.
 *
 * @return set of ranks used by this blocks.
 */
std::set<int> BlockManager::deleteBlocks(const std::set<int>& delBlockIDs) {
  std::set<int> ranks;

  for (const auto& blockID : delBlockIDs) {
    auto block = blockRegister_.find(blockID);
    if (block != blockRegister_.end()) {
      auto tmpRanks = block->second->getRanks();
      ranks.insert(tmpRanks.begin(), tmpRanks.end());

      delete block->second;
      blockRegister_.erase(block);
    }
  }

  return ranks;
}

/**
 * Checks for the rank in the neighborhood of any block.
 *
 * @param rank The rank to check.
 *
 * @return true if found, false else.
 */
bool BlockManager::isRankInNeighborhood(const int rank) const {
  for (const auto& block : blockRegister_) {
    if (block.second->isRankInNeighborhood(rank)) return true;
  }

  return false;
}

void BlockManager::executeBlocks() {
  actionRegister_.executeBlocks(blockRegister_);
}

void BlockManager::executeBlocksPostLoop() {
  simdata_.timestep++;
  actionRegisterPostLoop_.executeBlocks(blockRegister_);
}

void BlockManager::executeActionOnBlocks(const std::string& name) {
  actionRegister_.executeActionOnBlocks(actionpool_.getElement(name), blockRegister_);
}

/**
 * Executes all registered io actions.
 */
void BlockManager::executeActionWriterOnBlocks() {
  for (auto& action : actionpool_) {
    if (action.second->getType() != ActionType::io) continue;
    if (!action.second->isRegisterd) continue;

    actionRegister_.executeActionOnBlocks(action.second.get(), blockRegister_);
  }
}

void BlockManager::setupActions() {
  actionRegister_.setupActions(blockRegister_);
}

unsigned int BlockManager::getIDFromCoordinates(const unsigned int x, const unsigned int y, const unsigned int z) const {
  return (z * blocks_[1] + y) * blocks_[0] + x;
}

void BlockManager::getCoordinatesFromID(const unsigned int id, unsigned int* x, unsigned int* y, unsigned int* z) const {
  unsigned int index;
  *z    = id / (blocks_[0] * blocks_[1]);
  index = id - blocks_[0] * blocks_[1] * *z;
  *y    = index / blocks_[0];
  *x    = index % blocks_[0];
}

std::tuple<unsigned int, unsigned int, unsigned int> BlockManager::getCoordinatesFromID(const unsigned int id) const {
  unsigned int x;
  unsigned int y;
  unsigned int z;

  getCoordinatesFromID(id, &x, &y, &z);

  return std::make_tuple(x, y, z);
}

/**
 * Gives the relative coordinate difference from two block IDs.
 *
 * @note this is not working in a periodic manner
 *
 * @param  id1 The ID of the basis block
 * @param  id2 The ID of the second block
 * @return coordinate tuple, id2-id1
 */
std::tuple<int, int, int> BlockManager::getRelativeDifference(const unsigned int id1, const unsigned int id2) const {
  unsigned int z1    = id1 / (blocks_[0] * blocks_[1]);
  unsigned int index = id1 - blocks_[0] * blocks_[1] * z1;
  unsigned int y1    = index / blocks_[0];
  unsigned int x1    = index % blocks_[0];

  unsigned int z2 = id2 / (blocks_[0] * blocks_[1]);
  index           = id2 - blocks_[0] * blocks_[1] * z2;
  unsigned int y2 = index / blocks_[0];
  unsigned int x2 = index % blocks_[0];

  return std::make_tuple(x2 - x1, y2 - y1, z2 - z1);
}

/**
 * Return a block ID relatively to the given coordinates
 *
 * @param  base blockID of the base block
 * @param  x    relative x
 * @param  y    relative y
 * @param  z    relative z
 *
 * @return      blockID relative to base
 *
 * @bug If x + x0 + block[0] < 0 modulo is wrong
 */
unsigned int BlockManager::getRelativeBlockID(const unsigned int base, const int x, const int y, const int z) const {
  unsigned int x0;
  unsigned int y0;
  unsigned int z0;

  getCoordinatesFromID(base, &x0, &y0, &z0);
  x0 = (blocks_[0] + x0 + x) % blocks_[0];
  y0 = (blocks_[1] + y0 + y) % blocks_[1];
  z0 = (blocks_[2] + z0 + z) % blocks_[2];

  return getIDFromCoordinates(x0, y0, z0);
}

unsigned int BlockManager::getRelativeBlockID(const unsigned int base, const Direction side) const {
  return getRelativeBlockID(base, cx(side), cy(side), cz(side));
}

/**
 * Check if a block is inside of the neighborhood star of a any block in the list (Default: blockRegister).
 *
 * @param id the ID to check
 *
 * @return true if the ID is inside of a neighborhood.
 */
bool BlockManager::isInNeighborhood(const unsigned int id) const {
  for (const auto& block : blockRegister_) {
    if (isInsideStar(block.first, id)) return true;
  }
  return false;
}

/**
 * @overload
 * @param id   The ID to check.
 * @param list The list to check.
 *
 * @return true if the ID is inside of a neighborhood.
 */
bool BlockManager::isInNeighborhood(const unsigned int id, const std::set<int>& list) const {
  for (const auto& block : list) {
    if (isInsideStar(block, id)) return true;
  }
  return false;
}

/**
 * Gives a set of ranks that are not longer used by any blocks.
 *
 * @return set of unused ranks.
 */
std::set<int> BlockManager::getUnusedRanks(const std::set<int>& usedRanks) const {
  std::set<int> delComms;

  for (const auto& rank : usedRanks) {
    if (!isRankInNeighborhood(rank)) delComms.insert(rank);
  }

  return delComms;
}

/**
 * Updates all blocks in blockRegister with the given rank of the block with id.
 *
 * Run over the star of the block in blockRegister, check for id, if there
 * update.
 *
 * @param id           ID of the block
 * @param rank         rank of the process where the block with ID is.
 * @param commManager  The communications manager.
 *
 * @return A set of updated blocks.
 */
std::set<Block*> BlockManager::updateBlocks(const unsigned int id, const int rank, const CommManager& commManager) {
  std::set<Block*> blocks;

  for (const auto& block : blockRegister_) {
    for (int z = -2; z <= 2; z++) {
      for (int y = -2; y <= 2; y++) {
        for (int x = -2; x <= 2; x++) {
          if (id == getRelativeBlockID(block.first, x, y, z)) {
            if (!commManager.isInAnnounce(block.first, rank)) {
              block.second->updateLocalNeighbor(x, y, z, id, rank);
              blocks.insert(block.second);
            }
          }
        }
      }
    }
  }

  // update boundaries for updated blocks
  // TODO: alternatively this can be done for the needed boundary in updateLocalNeighbor, directly
  for (const auto& block : blocks) {
    block->updateFieldBoundaries();
  }

  return blocks;
}

std::set<Block*> BlockManager::removeBlock(const unsigned int id) {
  std::set<Block*> blocks;

  for (const auto& block : blockRegister_) {
    if (block.second->removeBlockFromLocalNeighbor(id)) {
      blocks.insert(block.second);
    }
  }

  // update boundaries for updated blocks
  // TODO: alternatively this can be done for the needed boundary in updateLocalNeighbor, directly
  for (const auto& block : blocks) {
    block->updateFieldBoundaries();
  }

  return blocks;
}
/**
 * Check if the given position is inside of a star of all direct neighbors of the direct neighbors of a center block.
 *
 * The five layer of the star:
 *
 *                         06
 *            01        07 08 09        19
 *     00  02 03 04  10 11 12 13 14  20 21 22  24
 *            05        15 16 17        23
 *                         18
 *
 * @param  x x-position relative to the center block
 * @param  y y-position relative to the center block
 * @param  z z-position relative to the center block
 * @return true if inside else false
 */
bool BlockManager::isInsideStar(const int x, const int y, const int z) {
  return abs(x) < 3 && abs(y) < 3 && abs(z) < 3;
}

bool BlockManager::isInsideStar(const std::tuple<int, int, int>& coordinates) const {
  int x;
  int y;
  int z;
  std::tie(x, y, z) = coordinates;
  return isInsideStar(x, y, z);
}

/**
 * @overload
 * @note This overload works periodic, but do not check if the boundary condition is periodic.
 * @param  id1 ID of base block.
 * @param  id2 ID of related block.
 * @return     relative coordinates from id2-id1
 */
bool BlockManager::isInsideStar(const unsigned int id1, const unsigned int id2) const {
  int x;
  int y;
  int z;
  std::tie(x, y, z) = getRelativeDifference(id1, id2);

  x = std::min((int)blocks_[0] - abs(x), abs(x));
  y = std::min((int)blocks_[1] - abs(y), abs(y));
  z = std::min((int)blocks_[2] - abs(z), abs(z));

  return isInsideStar(x, y, z);
}

void BlockManager::dbgPrintBlockCommInfo() const {
  std::map<int, std::set<int>> map;
  for (auto block : blockRegister_) {
    for (int commID = 0; commID < getMPISize(); commID++) {
      if (block.second->isRankInNeighborhood(commID)) {
        map[commID].insert(block.first);
      }
    }
  }

  std::stringstream result;
  for (auto comm : map) {
    result << "\t[" << comm.first << "]: ";
    std::copy(comm.second.begin(), comm.second.end(), std::ostream_iterator<int>(result, " "));
  }
  logger::get().trace(logger::Category::BlkComList, "BlockConnection: {}", result.str());
}

}  // namespace block
}  // namespace nastja
