/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "lib/archive.h"
#include "lib/pool.h"
#include "lib/datacontainer/datacontainerpool.h"
#include "lib/math/vector.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"
#include <bitset>
#include <set>

namespace nastja {

namespace field {
class IField;
}

namespace block {

class Block : public DataContainerPool {
public:
  explicit Block(SimData& simdata);
  ~Block() override       = default;
  Block(const Block&)     = delete;
  Block(Block&&) noexcept = default;
  Block& operator=(const Block&) = delete;
  Block& operator=(Block&&) = delete;

  field::IField* getFieldByName(const std::string& name) const;

  void setNeighbor(const std::vector<int>& blockIDArray, const std::vector<int>& blockRankArray);
  void updateLocalNeighbor(int x, int y, int z, unsigned int id, int rank);

  bool removeBlockFromLocalNeighbor(unsigned int id);
  void updateFieldBoundaries();
  std::set<int> getRanks() const;
  bool isRankInNeighborhood(int rank) const;

  int getLocalNeighborRank(stencil::Direction side) const;
  int getLocalNeighborRank(int x, int y, int z) const;
  int getLocalNeighborRank(int x, int y, int z, stencil::Direction side) const;

  int getLocalNeighborID(stencil::Direction side) const;
  int getLocalNeighborID(int x, int y, int z) const;
  int getLocalNeighborID(int x, int y, int z, stencil::Direction side) const;

  math::Vector3<unsigned int> getCoordinatesFromID(unsigned int id) const;
  unsigned int getNeighborDirection(unsigned int baseID, unsigned int neighborID) const;

  int countNeighbors(int rank) const;
  double fractionNeighbors(int rank) const;

  bool isGlobalCoordinateInBlock(unsigned int x, unsigned int y, unsigned int z) const;
  bool isGlobalCoordinateInBlock(math::Vector3<int> v) const { return isGlobalCoordinateInBlock(v.x(), v.y(), v.z()); }

  bool isLowerBound(const unsigned int dim) const { return blockOffset[dim] == 0; }
  bool isUpperBound(const unsigned int dim) const { return blockOffset[dim] == blocks[dim] - 1; }

  std::bitset<26> getPeriodicSides() const {
    std::bitset<26> isPeriodic{};

    for (int dim = 0; dim < 3; dim++) {
      if (!isLowerBound(dim)) isPeriodic |= stencil::skipExchange[2 * dim + 0];
      if (!isUpperBound(dim)) isPeriodic |= stencil::skipExchange[2 * dim + 1];
    }

    return ~isPeriodic;
  }

  unsigned int id{};                         ///< The ID of the block.
  math::Vector3<unsigned long> size;         ///< The vector of the size of a block.
  math::Vector3<unsigned long> offset;       ///< The vector with the offset to this block in voxels.
  math::Vector3<unsigned long> blockOffset;  ///< The vector with the offset to this block.
  math::Vector3<unsigned long> blocks;       ///< The vector with the total number of blocks per dimension. TODO unschön hier
  Pool<field::IField> fields;                ///< The pool of all fields allocated in this block.
  SimData& simdata;                          ///< Reference to the SimData.

private:
  friend class nastja::Archive;
  void pack(Archive& ar) const;
  void unpack(Archive& ar);

  std::array<int, 125> localNeighborID_{};    ///< The IDs of all local neighbors.
  std::array<int, 125> localNeighborRank_{};  ///< The ranks of all local neighbors.
};

using BlockRegister = std::map<unsigned int, Block*>;

}  // namespace block
}  // namespace nastja
