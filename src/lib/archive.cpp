/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "archive.h"
#include <cassert>
#include <cstring>

namespace nastja {

void Archive::packImpl(void const* p, const std::size_t l) {
  auto const* ptr = reinterpret_cast<char const*>(p);
  buffer_.insert(buffer_.end(), ptr, ptr + l);
  position_ += l;  // position is not necessary for pack
}

void Archive::overwriteImpl(void const* p, const std::size_t l) {
  auto const* ptr = reinterpret_cast<char const*>(p);
  std::copy(ptr, ptr + l, buffer_.begin() + arrayStart_);
}

void Archive::unpackImpl(void* p, const std::size_t l) {
  assert(position_ + l <= buffer_.size());

  if (l != 0u) {
    std::memcpy(p, &buffer_[position_], l);
  }

  position_ += l;
}

}  // namespace nastja
