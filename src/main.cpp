/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "apps/cells.h"
#include "apps/droplet.h"
#include "apps/heat.h"
#ifdef USE_PFC
#include "apps/pfc.h"
#endif
#include "apps/phasefield.h"
#include "apps/phasefield2.h"
#include "apps/phasefield_temp.h"
#ifdef USE_CUDA
#include "apps/phasefield_temp_cuda.h"
#endif
#include "apps/sleep.h"
#include "lib/factory.h"
#include "lib/nastja.h"

int main(int argc, char* argv[]) {
  nastja::NAStJA nastja{argc, argv};

  auto& factory = nastja.getFactory();
  /// @factory{FactoryApps}
  /// "Cells", "Droplet", "Heat", "PFC", "Phasefield", "Phasefield2", "PhasefieldTemperature", "Sleep"
  factory.registerType<AppCells>("Cells");
  factory.registerType<AppDroplet>("Droplet");
  factory.registerType<AppHeat>("Heat");
#ifdef USE_PFC
  factory.registerType<AppPFC>("PFC");
#endif
  factory.registerType<AppPhasefield>("Phasefield");
  factory.registerType<AppPhasefield2>("Phasefield2");
  factory.registerType<AppPhasefieldTemperature>("PhasefieldTemperature");
#ifdef USE_CUDA
  factory.registerType<AppPhasefieldTemperatureCuda>("PhasefieldTemperatureCuda");
#endif
  factory.registerType<AppSleep>("Sleep");

  return nastja.run();
}
