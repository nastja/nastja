# Developer

## NAStJA Core Developers
* Marco Berghoff
* Jakob Rosenbauer
* Nikolai Pfisterer

## Contributions
* Aashima Singh: polygonizer, voxelizer
* Bastien Druot: voronoi filling
* Alexander Dick
* Teodor Nikolov
