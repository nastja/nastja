\page testing Testing
[TOC]

CTest from CMake is used for running the tests.

# Requirements

* `md5sum` to check against the md5 files.
* `jq` to parse the json test files.

## On the clusters
The `md5sum` should be installed.

Please install `jq` and provide it to your `$PATH`. Make sure, that your `$HOME/bin` exist and it is in your path. Find in your `.bashrc` something similar to
```
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi
```
And download `jq`
```
wget -O ~/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
chmod +x ~/bin/jq
```

## On a Mac
The simplest way is to install both programs via `brew`
```
brew install md5sha1sum
brew install jq
```

# Running tests
Use `make test`, `ninja test` or `ctest` to execute the unit tests and simulation tests.

## Unit tests
The unit tests using the Catch2 framework.

## Simulation tests
For each json file in `test/data` a simulation is running and compared to the md5 checksum of the output saved in corresponding md5 files.
On success, the test output is deleted. Otherwise, it can be found in `tmp_testname`.

### Platform dependency
Not all output is bit identic. These are

* The stream output of the internal state of a pseudo-random number engine depends on the platform. This affected the dump output.

In this case, we provide an md5 checksum for each platform in the format `testname.platform.md5`.

### Keeping test results, even on success
Set the environment variable `NASTJAKEEP=1` to keep the test results.
These tests can be used to compare with `vtidiff`, when the result is not bit identic.
