\page key Config Keys
[TOC]

# Access to the Config
The config keys are hierarchically grouped by modules in the config file.
The key `CellsInSilico.celldeath.rate` is in the module `CellsInSilico`, this is the top-level of the config keys.
In this module, there is a submodule `celldeath`; this submodule has a key `rate`. The period `.` is used to structure the keys.

There are several functions to get specific values from the config file, see config::Config for details.
Values of simple data types are included, as well as vectors.
All these functions expect a JSON pointer.
For a JSON pointer to the submodule `celldeath` use either `config::getJsonPointer("CellsInSilico.celldeath")` or `"CellsInSilico.celldeath"_jptr`, the latter literal conversion is defined in the `namespace config::literals`.
You can append a hierarchical level by the `/` operator: `jptr / "rate"`.

# Annotations
For the code annotation, we have two extra special commands `key` and `dependency`.
Use the `key` to annotate in the code the read of config file keys. The command needs two to three arguments.
First, the `Config_key_path` using a dot (`.`) to separate the levels; second, the `Type`. The third argument is optional and gives the default value.
Use the `depends` command with a boolean expression of `Config_keys_path`s to describe the dependency, if any exist.
After the special command, use a description to explain the key for the users.

## Example
```
/// @ key{Config_key_path, Type, optional default value}
/// @ depends{!Config_key_path1 && Config_key_path2}
/// @ values{A list of allowed values}
/// @ title{An extra title}
/// @ widget{An extra widget type}
/// @ order{A number to order the keys}
/// The explanation of Config_key_path.
```
Note: The space after `@` is to prevent Doxygen to replace the annotation.

## Config Key Paths with Named Tags
Some path are defined by special names, as `Writer` this should be changes

## Config Key Types
Array with fix length `int[3]`, undefined `int[]` or defined by another key `int[key]`. Multidimensional arrays `int[][]`.

One can use generic basic types:

* `bool`   for booleans,
* `int`    for all (signed) integer types (int or long),
* `uint`   for all unsigned integer types,
* `real_t` for all floating types (float or double),
* `sting`  for strings.

Or special types depending on these basic types and specify by `<>`, e.g., `Vector3<uint>`

* `Vector3<>`          for Vector3
* `Bbox<>`             for Boundingbox
* `ParameterStorage<>` for ParameterStorage

For Factory types use
```
/// @ factory{FactoryVisitor}
/// "Visitor00", "Visitor01", "Visitor02", "Visitor04"
```
to define an enum. The type `FactoryVisitor` will be replaced by type `string` with the enum values of the second line.
For an array of factory values, use `FactoryVisitor[]` instead.

The type `group` allows to give a description to groups.

The `optional` type allows to have optional arguments without default value.

## Multiple use of keys
Use `@ keyignore` to annotate keys that are reread from the config file. The full documentation annotation is provided there. For an annotation checker tool this config key should handled as it has a correct annotation.

## Config key paths with dynamic parts

The property name of a module inside a config path is not necessarily fixed.
You can define a "config template" that denotes a segment of the config key path to be variable.
These segments are identifiable by being enclosed in `\template{key_path_segment}` tags.

### The base case

`@ key{Writers.\template{writer}.field, string, ""}`

This defines a config key `field` of type string.
Its value is defined to be found via the path `Writes > [Some user string] > field`.
A valid configuration is able to contain multiple instances of the defined config key.
This works by using separate strings in the `\template{writer}` part.

### Defining different template variants

Lets say that you have defined a whole object by a group of config keys.
You can then use a config template like in the example above to allow multiple instances of this object.
Maybe you now want to have different variations of this object, that have conflicting key paths.
You can model your different "types" by using a notation like this:

Example:

```
@ key{Writers.\template{writer}field, string, ""}
@ key{Writers.\template{writer=CellInfo}.steps, int, 1}
@ key{Writers.\template{writer=CellInfo}.outputfieldname, string, ""}
@ key{Writers.\template{writer=CellInfo}.groupsize, int, 0}
@ key{Writers.\template{writer=Dump}.steps, int, 1}
@ key{Writers.\template{writer=Dump}.preserve, int, 2}
```

This example has achieved three things we will now explain.

First, we have defined two objects, we identify as `CellInfo` and `Dump`.

Second, we defined a config key `field` that is present in all `writer` variations.

Third, we implicitly defined the config key `Writers.\template{writer}.writer`.
This is a string enum and contains the values `CellInfo` or `Dump`.
This mechanism allows application code to identify which object template is instantiated in the config.
Please do not add `@key` comments that explicitly define this config key.

Note: The string in between the `\template{...}` tags are presented to users of GUI configuration editors as a mean to select which config template they wish to instantiate.
In the above example the user would be presented with the task to select between `CellInfo` or `Dump` for a `writer`.

#### Declaring a subtype as default

If you have definitions:

```
@ key{Fields.\template{field}.boundaries.\template{boundary=Dirichlet}.foo, real_t, 0.0}
@ key{Fields.\template{field}.boundaries.\template{boundary=Periodic}.bar, real_t, 0.0}
```

You can use this notation...

```
@ schemaFactoryDefault{Fields.\template{field}.boundaries.\template{boundary}, Periodic}
```

...To specify, that if the `boundary` property is missing it is not interpreted as an error, but as being set to `Periodic`.

### Defining an array of templates

A little bit specific is the notation to define an array of modules.
It looks like this:

```
@key{Filling.\template{field}[].\template{pattern=const}.value, real_t}
@key{Filling.\template{field}[].\template{pattern=voronoi}.size, uint}
```

This defines a config structure like this:

```json
{
    "Filling": {
        "{\template{field}-KEY-NAME}": [
            {
                "value": "{real_t-VALU}",
                "pattern": "const"
            },
            {
                "size": "{uint-VALUE"},
                "pattern": "voronoi"
            },
            {
                "size": "{uint-VALUE}",
                "pattern": "voronoi"
            }
        ]
    }
}
```

**Important:** If you use an `\template{something}[]` part in the config key path, it must be followed by another (non-array) "em" part.
The array definition "sucks in" the property key of the next part.
The module is not identified by a string.
It instead becomes the array index.
Thus it makes no sense to have a key path defined by a concrete string.

**Important:** It is not allowed to have more than one em-key on the same level:

```
@key{Filling.\template{key_a}[].\template{pattern=const}.value, real_t}
@key{Filling.\template{key_b}[].\template{pattern=voronoi}.size, uint}
```

*key_a* can not be distinguished from *key_b*.
There **must not** be a collision between array definitions in the config key path.

# Overview of all Config Keys
