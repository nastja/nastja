\page splitX 2020-03 Split the x coordinate for SIMD vectorization
[TOC]

# Requirements
Support of SIMD vectorization, for register lengths of at least 2, 4, 8, 16, depending on base type (float32, float64) and instruction set extension, CPU support 128bit (sse), 256bit (avx/avx2) and 512bit (avx-512).
Depending on the application, a different data schema in the memory could bring some performance benefits, mostly by reusing caches.

NAStJA currently supports multiple fields and fields of vectors. Assuming that applications use the same data type, this corresponds to the layouts of struct of arrays (SoA) and an array of structs (AoS), respectively.
Typically, a vectorization along the vector is hard. However, this can be useful, when the vector has a length directly corresponds to the register length, and the calculation for each component is similar.  In other cases, the spatial coordinate x is the better choice for the vectorization, i.e., an SoA layout can be chosen. SoA has the drawback that for each vector component, a separate field is needed, and the distance from v[0] to v[1] holds the full zyx domain.

The first idea is to swap the dimensions x and v thus that the distance between v[0] and v[1] is only one x line. Applications using vectors normally do calculations with the whole vector on a specific spatial position, so it can be an advantage to bring the vector even closer together. At each position, the vector holds x elements of the size of the vector register length. Let N be the vector register length then the x-axis is split to X = size_x / N and x' = N. It is size_x = X * x' assuming that size_x is a multiply of N.

Using zyXvx' with x' a vector register data type meaning that each operation access the field must handle the most inner coordinate as a vector register or the ScalarVector representative without vector support. This makes the code inflexible and results in a lot of changes and code duplications. Using the field data type T as a basis of the x' vector will not change the field at all, only the interpretation of the data position is changes. Getter functions can transparently cover this.

We have to support the following cases:

## (1) Three dimensional scalar data
Vectorization along x
```
+-------------+         vector_length = 1
|zyx          |         padding: x
+-------------+         stride_x = 1
                        stride_y = stride_x * padded_size_x
                        stride_z = stride_y * size_y
```
## (2) Three dimensional vector data
Vectorization as vector
(a) vector length = n * N
(b) n * vector length = N
(c) vector length < n * N
```
+-------------+  +-+    vector_length = v
|zyxv       | |  | |    padding: v
+-------------+  | |    stride_x = padded_vector_size
             |   | |    stride_y = stride_x * size_x
             --->+-+    stride_z = stride_y * size_y
```
## (3) Three dimensional vector data
Vectorization along x that is split to x = X * x', with x' = N
```
+-------------+  +----+ vector_length = v
|zyXvx'     | |  |    | padding: X
+-------------+  |    | stride_x = v * N
             |   |    | stride_y = stride_x * padded_size_X
             --->+----+ stride_z = stride_y * size_y
```
Currently, (1) and (2a) supports vectorization. For v=1, (2) is equivalent to (1) and (3) falls back to (1).

Current influences by introducing N to the field:
(1) no change in data layout, getIndex works,
    a field view becomes stride_x = N,
    access to the elements by cell + 1, cell + 2, …,
    Direction::L/R gives cell left/right over load_unaligned
(2) no change in data layout, getIndex works,
    a field view has stride_x = v
    access to the elements by getVec and cell[i],
    Direction::L/R gives vector left/right
(3) data layout changes, getIndex won't work,
    a field view has a stride_x = v * N
    access to the elements by getVec(*cell + 1)[i]
    Direction::L/R data are not consecutively in the memory

For (3) we have to adapt getIndex, field view get and Direction::L/R get.

N must be a power of 2,
FieldViews can have a step width W <= N also power of 2

# Field accesses
```
getIndex
 z * stride_z + y * stride_y + x * stride_x;
->
 z * stride_z + y * stride_y + X * stride_x * N + x';
```
```
getCell(index, j)
data[TS][paddingPreOffset + index + j];
->
data[TS][paddingPreOffset + index + j * N];
```
```
getCell(index, dir, j)
getCell<TS>(index + getDirectionalOffset(dir), j);
->
getCell<TS>(index + getNextVectorOffset(index, dir) + getDirectionalOffset(dir), j);
getDirectionalOffset(dir)
```
z and y, as currently
The left and right direction does not longer linear in the memory, so we need another access pattern.
x depends on the position of the index, ±1 or ±v, vector length.
```
R: +----+ +----+ +----+
   |    | |111v| |    |
   +----+ +----+ +----+
x' = N - 1
L: +----+ +----+ +----+
   |    | |v111| |    |
   +----+ +----+ +----+
x' = 0
x' = index % N
```
AVX2 supports with
```
__m256d _mm256_i64gather_pd (double const* base_addr, __m256i vindex, const int scale)
+----+ +----+ +----+
|    | |cccc| |    |
+----+ +----+ +----+
+----+ +----+ +----+
|   l| |lll | |    |
+----+ +----+ +----+
+----+ +----+ +----+
|    | | rrr| |r   |
+----+ +----+ +----+
```
a access pattern. Currently, we do not need support for AVX anymore.

## Influences

* Field now holds a second template parameter TSplitX that gives the number of the x elements in each vector component.
* The FieldProperties has a new FieldType::splitX that must be set when the x coordinate should be split.
* The alignment gives the number of elements in a SIMD vector. This is currently 4 double for both, avx and scalar.
* Only avx needs aligned memory, this is handled by the allocate function.
* The FieldView transparently load left and right direction values with the load_unaligned.
* The default creator of a FieldView returns a step size of the vector register size when the field has the splitX flag.
* The FieldIterator iterates internally over a fast x, a slow x, y, and z. The fast and slow x builds the x coordinate.
* Due to the splitX and the step width, there are some restrictions: the steps must not jump over a vector border. A FieldView with step size 1 is always possible.
* The MPI boundary use for fields with splitX flag a five dimension array, with the boundary on the left side in a vector on the right side and on the right side the left column(s) in the vector, meaning we have padding left and right.
* Other boundaries do not work with splitX fields.

