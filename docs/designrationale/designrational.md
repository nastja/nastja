\page splitx Design rationales
[TOC]

# Design rationale
This page list the design rationales which explicitly documents the reasons behind decisions made for the design or redesign of parts in the NAStJA framework. Each substantial design decision should be listed here.

\subpage splitX
\subpage hashmap
