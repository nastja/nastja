\page hashmap 2020-09 Hashmaps
[TOC]

# Requirements
CellID dependent parameter as stored in SpareStorage and also the Cell data itself stored in SpareStorageCell uses the CellID as the key. The cellID_t is an unsigned int or long, it is equally distributed over all blocks. But any block holds only the cell information of the cells in the block and the neighborhood. So that the data are spare. Therefore a hashmap with a theoretical access complexity with O(1). During cell division, a sequence of new cellIDs are created in each block. Since the hash function for int numbers are normally the identity, these sequences will overlap and then O(1) can not longer hold due to the hash clashes, i.e., buckets have more than one matching key. Probing of the keys is then expensive.

Generally, two kinds of hash maps are exiting, first, like the std::unordered_map, that holds a list in each bucket, and secondly open addressing tables, where the key then goes to the next bucket to hold a dense table. Both have linear probing, sometimes the latter use quadratic probing.

The std::unordered_map uses a modulo operation to find the bucket and has a prime number of buckets so that this operation is very expensive. The clang implementation of the stdlib uses as default power of 2 bucket sizes. This code is in the gcc stdlib included but is not active, it is obviously faster with just a mask operation to figure out the correct bucket. But it is not compatible.

Opentable with linear probing is faster for perfect hashes, but very slow for overlapping sequences, as shown with the emilib::hashmap. An own implementation with quadratic probing is way better but for overlapping sequences not faster than the std::unordered_map.
Linear probing with a better hash as used in the robin_hood hashtable implementation are a way faster.
The Swiss table implemented in abseil uses a cache optimization opentable. This one is the fastest of the tested hashmaps.

# Alternative Way
Use local cellIDs that can be stored in a std::vector. But then a mapping from global to local IDs is needed. This mapping will be needed in the border exchange for each voxel in each time step. So that this seems not a good alternative.

# Reproducibility
The absl::flat_hash_map uses a hash seed based on the pointer position. The iterating order hence are not reproducible.
The std::unordered_map show also a non reproducible iterating order for different implementations (gcc, clang) and even for different versions. It can only be used if the version and implementation is fixed.

# Implementation
Hence, absl::flat_hash_map is so used in the FAST_MATH mode because it is the fastest. The reproducibility is not given because of speed in the FAST_MATH mode.
For reproducibility the robin_hood::unordered_map is used outside of the FAST_MATH mode.
The performance of absl vs. robin_hood is in the same range for the non-FAST_MATH mode. But, absl is around 10% faster in FAST_MATH.
