
# Konsistenz
**Paarweise Konsistenz:** zwei Prozesse A, B heißen konsistent, wenn B die Blöcke und Kommunikatoren von A kennt. Und umgekehrt A die Blöcke und Kommunikatoren von B kennt. Zwischen A und B gibt die zwei verbundenen Kommunikatoren A, B (Kommunikatorpaar A-B).

Die für die Definition der Konsistenz berücksichtigten Eigenschaften werden in den Schritten DynBlock und LoadBalance geändert und zwar nur in diesen Schritten (abgesehen von der Initialisierung.)

Neben dieser paarweisen Konsistent definieren wir die **Gebietskonsistenz**. Das Simulationsgebiet heißt konsistent, wenn alle Blöcke die Randaustausch betreiben müssen, Randaustausch betreiben. Benachbarte Blöcke, die an ihrem gemeinsamen Rand eine nicht konstante Phase haben müssen ihren Rand austauschen. Konstante Phasen sind hiervon zunächst ausgenommen.

Um die Gebietskonsistenz nicht zu verletzten, sollten benachbarte Blöcke so schnell wie möglich ihren gemeinsamen Rand austauschen. Damit es zu keinem Deadlock kommt müssen die Nachbarn zur gleichen Zeit anfangen ihren Rand auszutauschen. Für diese Entscheidung sind die Kommunikatoren und insbesondere deren paarweise Konsistenz nötig.

## Nachrichten
Die Kommunikatoren kommunizieren, indem sie verschiedene Nachrichten austauschen. Einige Nachrichten führen Aktionen aus, bei denen die Konsistenz geändert werden kann, andere sorgen für den Erhalt bzw. der Wiederherstellung der Konsistenz. Wichtig ist, dass Aktionen keinen Deadlock, wie oben beschreiben, verursachen. Daher muss darauf geachtet werden, dass jede Aktion nur in einem (lokal) konsistenten Zustand ausgeführt wird. Was das genau bedeutet wird im späteren Beispiel klar, schauen wir uns zunächst die möglichen Nachrichten an:

- Aktionen
    + an Alle
        * ⬚ neuer Block
        * ☒ gelöschter Block
        * ⍄ verschobener Block (nur Lastausgleich)
- Konsistenz-Wiederherstellung
    + an Alle
        * ◌ neuer Kommunikator
        * ⦻ gelöschter Kommunikator
    + an Einzelne
        * ⇄ FCS
        * ☐ announced Block

Die Erklärung und Anwendung der einzelnen Nachrichten wird im nächsten Abschnitt erläutert.

## Reihenfolge der Bearbeitung im DynBlock-Schritt
Wir beschränken uns zunächst auf dem DynBlock-Schritt. Die Teilschritte in einem Zeitschritt sehen wie folgt aus:

- **Nachrichten erstellen**: Informationen für Aktions-Nachrichten werden gesammelt (z.B. aus den Blockdaten) und in `newBlocksByRank` sowie `delBlocks` vom CommunicationManager gespeichert. Aus diesen Daten werden die Nachrichten zusammengestellt und jedem (verbundenen) Kommunikator in den sendingBuffer gelegt. Zusätzliche werden hier noch Konsistenz-Wiederherstellung-Nachrichten angehängt.
    + Neue und gelöschte Kommunikatoren kommen aus `announceComms` und `deletedComms`, diese sollten gleich sein für alle Kommunikatoren.
    + Zusätzlich werden die zurückgehaltenen Blöcke, die dem Partner noch bekannt gemacht werden müssen, angehängt. Diese sind in `announceBlocks` gespeichert und abhängig vom jeweiligem Kommunikator.
    + Eine spezielle Art sind die `Fast Connection Setup (FCS)`-Nachrichten. Hier wird geschaut, welche Nachbarkommunikationen nach einer Aktion benötigt werden. Den beteiligten Kommunikatoren wird dann jeweils der andere Kommunikator mitgeteilt.
- **Nachrichtenaustausch**: Nachrichten werden gesendet und empfangen, die Nachrichten für die Konsistenz-Wiederherstellung werden auch gleich verarbeitet.
    + **Konsistenz Wiederherstellung umsetzten**: Nachrichten der Konsistenz-Wiederherstellung werden bearbeitet, d.h. Blockliste aktualisiert und ggf. Randaustausch aktiviert. Die Kommunikatorenliste wird aktualisiert. Nach diesem Schritt sind die Kommunikatoren paarweise konsistent.
    + **FCS Zusatzkommunikation und etablieren**: Kommunikatorpaare werden aufgebaut, indem der Partner erstellt wird und `setConnected` aufgerufen wird, damit werden Daten zum Nachbar gesendet und der aktuelle Kommunikator auf `received=false` gesetzt. Für Details siehe unten. Nach diesem Schritt ist das neue Kommunikatorpaar konsistent. Die Kommunikatorenliste der anderen ist jedoch inkonsistent. Dies muss für die Aktionen berücksichtigt werden.
- **Nachrichten bearbeiten**: Aktionen werden umgesetzt, dadurch entstehen erneut Inkonsistenzen in den Blöcken, die über `announceBlocks` im nächsten Stritt aufgehoben werden.

## Herkömmlicher Kommunikatoraufbau.
Neben den oben beschriebenen FCS-Nachrichten, wo ein Prozesse von einem Dritten über die Existenz eines neuen Block erfährt und dann einen Kommunikator zu dem Host dieses Prozesses aufbaut, gibt es auch Prozesse, die diesen Dritten nicht kennen. Diese Prozesse bekommen irgendwann die Existenz eines Blockes in ihrer Nachbarschaft über das Manhattannetz mit. Sie starten ein Handshake-Algorithmus mit der Heimat dieses Blockes. Beim Handshake werden nur Nachrichten mit SYN und ACK ausgetauscht um sicherzustellen, dass beide im gleichen Zeitschritt synchronisiert sind. In dieser Phase sind die Kommunikatoren als nicht verbunden markiert und nehmen nicht an sonstigen Nachrichtenaustausch teil. Ist der Zeitschritt synchronisiert, so wird für die Kommunikatoren wie oben `setConnected` aufgerufen und `received=false` gesetzt.

## FCS Zusatzkommunikation
Zwei Prozesse A, B bekommen eine FCS-Anweisung. Zunächst wird der Kommunikator angelegt (und bei dem eigenen Kommunikator der Partner in der Liste aufgenommen). Sie schicken an den jeweiligen Partner eine Liste mit Blöcken und bekannten Kommunikatoren. Damit ist die Konsistenz zwischen den Beiden (A, B) ohne die Konsistenz-Wiederherstellungs-Nachrichten gegeben. Wichtig ist nun, dass bereits gesendete Aktions-Nachrichten, die den jeweiligen Sende-Prozess betreffen und nur diese dem Partner mitgeteilt werden. Dadurch ist sichergestellt, das die beiden Neuen die Aktionen ausführen, das ist z.B. nötig für gelöschte Blöcke. Aber ein Dritter C muss die Aktion, die er von B bekommen hat so ausführen, dass er davon ausgeht, dass A sie nicht bekommen hat, denn die Kommunikatorliste zwischen A und C ist nicht konsistent. Daher wird die Aktion an C von B nicht an A gesendet.

## Initialisierung:
Über den FCS-Mechanismus genügt es zur Initialisierung alle Kommunikatoren auf `setConnected` zu setzten und ein normalen Receive-Schritt durchzuführen, mit anschließenden update der Blöcke.

### Beispiel:
A initiiert einen FCS von B und C oder aber B, C haben einen Handshake abgeschlossen, danach sind B und C in einen konsistenten Zustand. Die Kommunikatorenliste werden direkt angepasst.
- Nachrichten die von B an B oder C an C gesendet werden, werden jeweils Nachträglich an C bzw. B gesendet. Dadurch bekommt B mit, wenn auf C ein Block erstellt oder gelöscht wird. Das Löschen ist nötig, da es keine nachträgliche Löschnachricht geben kann, sonst müsste der Block noch vorgehalten werden und erst später gelöscht werden.
- Nachrichten die von A gesendet und B betreffen, hat C ebenfalls erreicht und werden von B und C bearbeitet, unabhängig von dem FCS.
- Nachrichten die von B gesendet und A betreffen, hat C nicht bekommen, da A nichts von der Verbindung B-C weiß, darf C diese Nachricht nicht bekommen.

Beim FCS-Austausch müssen also Nachrichten die einen Partner selbst betreffen dem Anderen mitgeteilt werden und zwar nur diesem.

## Besonderheiten für die Lastverteilung. (TODO)
Hier werden nur Aktionen für verschobene Blöcke gesendet und die Wiederherstellungs-Nachrichten vom vorhergegangenen DynBlock-Schritt, damit ist nach deren Abarbeitung alles Konsistent. FCS werden immer mitgeschickt, da zum Zeitpunkt des Verschiebens nicht sichergestellt werden kann, ob zwei Nachbarn ihre Verbindung gerade aufgelöst haben. Nach dem Abarbeiten der Wiederherstellungs-Nachrichten sind die Kommunikatorenlisten wieder konsistent, so dass die FCS nur ausgeführt werden müssen, wenn die Kommunikatoren noch nicht existieren.
Blöcke dürfen erst verschoben werden, wenn alle Nachbarn aktiviert sind, was nach x Zeitschritten sein sollte, genau wie bei den zu löschenden Blöcken.
Damit bekommen alle Nachbarn die Nachricht über das Verschieben.
Es kann aber nicht festgestellt werden, ob alle Nachbarn verbunden sind. Nachbarblöcke die später erstellt werden und sich dann mit dem zu verschiebenden Block verbinden gehen über diese x Zeitschritte hinaus.

# TODO
- FCS nur ausführen wenn nicht existent.
- Nachbarblock -> Verschieben Block – was tun?

✔
❓
✘

## Probleme mit der Lastverteilung
Ausgehend von Problemen der Lastverteilung muss die Konsistenz erhöht werden. Die Konsistenz lässt sich weiter mit den kommunizierenden Nachbarblöcken messen. Im Detail passieren diese Schritte:
- senden
- empfangen
- FCS Nachschritt.

Für die Lastverteilung sollte die Konsistenz nach dem Aufbau der FCSs wiederhergestellt sein, d.h. dass alle Nachrichten alle erreicht haben sollten so als wenn die FCS vorher schon da waren. Blöcke haben schon all ihre Nachbarn, wenn sie verschoben werden.
### Neue Kommunikatoren werden in den Listen ergänzt.
- Jeder Empfänger weiß an wem es gesendet wurde.
- Ergänzt seine lokale Liste mit dem neuen Paar.
- Direkt beteidigte, machen FCS und announcen an alle die, die Nachricht nicht bekommen haben: aka die, die nicht in der Liste vom Sender sind.

### Nachgesendete Nachrichten
-(FCS?)
-Lastverteilung
-gelöschte Blöcke
-Neue Blöcke garnicht. Denn bei zwei Quellen für denselben FCS kann nicht entschieden werden welche Nachrichten gesendet werden und welche nicht. Dafür wird aber eine Liste der anderen Empfänger für diesen Block dem Blockersteller mit geschickt. **Alternative:** Zunächst wird Empfangen, ohne FCS-Nachrichten, diese werden anschließend in einem 2. Schritt gesendet und Empfangen. Sollte die Nachrichtenlage etwas verlangsammen, aber bleibt konsistent bei verschiedenen FCS-Nachrichten. TODO!

# Problem X Zusammenfassung:
Kommunikatoren, die erstellt werden, dann aber keine verbundenen Blöcke mehr haben können gelöscht werden. Und müssen gelöscht werden.

A erzeugt Block 1 und announced diesen global. B sieht ihn hat einen Nachbarblock 2 und beginnt mit dem Verbindungs Handshake A-B.
Bevor der FCS gesendet wird ist Block 2 nicht mehr bei B, dadurch gibt es keine verbindende Blöcke zwischen A-B. Die Verbindung könnte gelöscht werden.
Da das Verbindungslöschen im DynBlock-Schritt nur durch gelöschte Blöcke getriggert wird, benötigt es so eine Nachricht. Haben nun beide das Löschen von einem Block 3 auf A mit bekommen, so schauen beide nach A und löschen den Kommunikator zu A wenn er nicht mehr benötigt wird, d.h. nur B löscht die Verbindung zu A. Auf A wird das Löschen von der Verbindung zu B nicht getriggert. Dass nun gleichzeitig auf B noch ein Block 4 erstellt wird, der in der Nachbarschaft mit einem Block auf A liegt tut nichts zu Sache, solange A nichts von Block 4 weiß.

# Problem XI Zusammenfassung:
Im DynBlock-Schritt wird ein Kommunikator A-B gelöscht, diese Nachricht erreicht die Nachbarn von A und B im nachfolgenden Lastverteilungsschritt.
Wird nun gleichzeitig ein Block verschoben, der die Kommunikation A-B benötigt, dies geschieht, wenn B Empfänger eines Blockes von C ist. Verschiebt nun A ebenfalls einen Block, an D. So sendet A diese Information im FCS auch an B. Damit wären B und D konsistent, jedoch bekommt D von A und B noch die Nachricht, das A-B gelöscht wird. Diese Nachricht sollte vor dem FCS A-B verarbeitet werden. Das ist jedoch nicht sichergestellt, da D von C eher empfangen kann als von A oder B.

D.h. in diesem Teilschritt erstellte Kommunikatoren, dürfen nicht von einer Kommunikatorlöschnachricht gelöscht werden.

## Lösungsmöglichkeiten:
1. FCS nachträglich: Verzögerungen durch 2. Send/Receive-Schritt.
2. Zeitstempel: Saubere Lösung, aber aufwendig, wenn jeder Wert einen Zeitstempel bekommt.
3. Zwischenspeichern: Erdacht als zusätzlicher Speicher der angibt ob eine Kommunikationsverbindung neu (in diesem Schritt erstellt ist) oder nicht. Entspricht eigentlich dem Zeitstempel.

Also wird der Zwischenspeichern genommen:

Problem ist das Löschen von schon (im Sinne von in diesem Schritt) erstelle Kommunikationen. Prinzipiell erst Löschen dann Erstellen, so dass das Löschen im Zeitschritt TS x.a und das Erstellen in TS x.b geschieht. Ist eine Verbindung in TS x.b erstellt worden, wird sie nicht mehr gelöscht da TS x.a davor gewesen wäre.

Es macht den Anschein, dass die Kommunikatorenliste das einzige Problem dieser Art ist.

- Kommt eine Kommunikationspartner-Lösch-Nachricht, so wird diese ignoriert, wenn der Partner in diesem Schritt schon angelegt wurde, dazu wird in einen Buffer geschaut. Ist er nicht im Buffer, wird gelöscht.
- Am Ende muss der Buffer wieder geleert werden.
- Eine eingehende FCS Nachricht füllt alle Buffer auf.
- Der Buffer kann je Kommunikator sein, da andere ignoriert werden und FCS noch aufgefüllt werden, und dabei keine Löschnachricht von dem Partner bekommen.

