# A solver for dynamic distributed blocks
## Preable
Das Projekt wird getrieben durch die Idee ein flexibles Framework für Berechnungen/Simulationen auf HPC-Systeme, auch der nächsten Generation.
Es soll für Konzeptentwicklungen verwendet werden, wo nicht eine Menge an Abhängigkeiten berücksichtigt werden muss.

Grundlegend basiert dies auf gleichgroße Blöcke, die dann an das große System angepasst wird.

Dieses Dokument listet Konzepte und gedanken zu Konzepten auf, Gedanken enhalten in der Überschrift `Draft`.
Details der Implementierung befinden sich in der durch doxygen generieten Dokumentation.

## Initialization (TODO)
Blockmanager.init = crateBlocks

App.setup registerActions, registerFields
registerActionList of Blockmanager

App.init Blockmanager.init, Action.init - create Fields
Blockmanager.setupActions All action.setup

Blockmanager.execute Actions

## Simulation
Die Aktion werden nacheinander für alle lokalen Blöcke durchgeführt.

## Dynamische Blöcke
Die Aktion „dynamische Blöcke“ regelt das Erstellen und Löschen von Blöcken.
Blöcke mit konstanten Werten brauchen nicht gerechnet werden und somit ist es auch nicht nötig diese überhaupt anzulegen, bzw. vorzuhalten.
Es gibt volle Blöcke alle Werte 1 und leere Blöcke alle Werte 0.

### Auslöser
Neue Blöcke werden erstellt, wenn die letzte innere Schicht nicht konstant 0 bzw. konstant 1 ist.
Hier Zählen auch die Randschichten hinzu.
Der Test läuft vor dem Randaustausch ab, so dass der Rand vom alten Zeitschritt genommen wird.
Anmerkung: Für den D3C7 wäre der Rand nicht nötig, für größere Sterne schon.
Ob der Block voll oder leer erstellt wird, entscheidet das Volumen dieser Schicht.
Voll wenn mehr als die Hälfte gefüllt ist, sonst Leer.

Alte Blöcke werden entfernt wenn sie inklusive Rand konstant 0 oder 1 sind.
Zusätzlich müssen sie mindestens 10 Zeitschritte leben.
Anmerkung: dies ist nötig, damit kein Randaustausch erstellt wird wenn der Block schon wieder gelöscht ist (s.u.).

### Blocknachbarschaft
Blöcke kommunizieren mit ihren Nachbarn und deren Nachbarn, so bildet sich je nach Diskretisierungsstern andere Nachbarschaften.
Anmerkung: Viele Kommunikationen sind nur Einseitig nötig, neue Blöcke, nur zu den Nachbarn der neuen Blöcke.
Block löschen muss nur zu den Nachbarn gesendet werden.
Hier wird aber immer zur gesamten Nachbarschaft kommuniziert.

### Kommunikatoren
Kommuniziert wird über Kommunikatoren, jeder Prozess hat für jeden anderen Prozess, mit dem er kommuniziert einen eigenen Kommunikator.
Den Kommunikatoren sind die Blöcke zugeteilt, welche einen Block in ihrer Nachbarschaft haben, der auf dem Zielprozess des Kommunikators lebt.

### Wo werden Blöcke erstellt?
Der auslösende Block schickt an alle Prozesse mit einem Block aus seiner Nachbarschaft das Ereignis, neuer Block.
Blöcke werden dort erstellt, wo der erste existierende Nachbar x-, x+, y-, y+, z- oder z+ lebt.
Anmerkung: dies ist in einem global wissendem Gebiet eindeutig, wenn der Stern vollständig ist, auch lokal.
Bleibt zu prüfen ob es schief gehen kann. Anmerkung Ende.
Ein Problem ist, wenn mehrere neue Blöcke nebeneinander erstellt werden (siehe Bild?), dann kann es passieren, dass einer den Prozess des Anderen eindeutig bestimmen kann, der andere jedoch nicht.
Um dies zu verhindern, schicken alle Prozesse ihre lokal neu anzulegende Blöcke an die jeweiligen Kommunikationspartner.
Anmerkung: Es passiert, dass ein neuer Kommunikator (einseitig) angelegt wird und somit kann er nichts empfangen. Zu Prüfen: Das sollte unerheblich sein. Anmerkung Ende.

### Update
Jeder Prozess updatet die lokale Nachbarschaft mit den Informationen neuer Blöcke.
Randbedingungen werden ausgeführt wenn ein Nachbarblock bekannt ist, so dass dieses immer synchron geschehen muss.

### globaler Austausch
Damit alle Blöcke von einem neuen Block erfahren, insbesondere dann, wenn der Erzeuger nicht in der lokalen Nachbarschaft ist, gibt es einen globalen Austausch.
Hier werden BlockID und Rank per mehrdimensionalem Manhattan-Street-Network auf alle Prozesse verteilt.
Dabei werden Nachrichten in einem Puffer gesammelt.
Neu eintreffende unbekannte Nachrichten bzw. Kenntnisse über neue Blöcke werden dem Puffer hinzugefügt und an nur wenige Prozesse verschickt.
Jede Nachricht hat einen Lebenszeitzähler, wenn dieser Abgelaufen ist, ist die Information im gesamten Gebiet bekannt und wird zum synchronen update der Nachbarn benutzt.
