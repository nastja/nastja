# Das Wissen der Kommunikatoren (Draft)
Jeder Prozess hat Blöcke und für jeden Partnerprozess, mit dem er Kommuniziert, also seine Blöcke einen Randaustausch haben einen Kommunikator. Die Kommunikatoren sind in einem Kommunikationsmanager zusammen gefasst. Blöcke benutzen diesen als Zwischenschicht um zu schauen, wo der Randaustausch kommuniziert werden muss.

Die Kommunikatoren haben einen synchronisierten Stand der bekannten Blöcke. Dazu hat jeder Kommunikator 4 Listen:
- knownBlocks
- incomingBlocks
- announceBlocks
- announcedBlocks

Neu erstellte Blöcke werden in `announceBlocks` aller Kommunikatoren abgelegt. Im nächsten Austauschschritt werden diese dann zu `announcedBlocks` verschoben.
Empfangende Blöcke werden in `incomingBlocks` gespeichert. Die Blöcke in `announceBlocks` werden bei dem Randaustausch zurück gehalten, so dass dieser Randaustausch dann aufgebaut wird, wenn der Block in `announcedBlocks` angekommen ist. Anschließend kann der Block aus `announcedBlocks` entfernt werden. Blöcke in `incomingBlocks` (auf dem Remote sind dies gerade die `announcedBlocks`) werden als neue Blöcke zum Randaustausch eingetragen und gelangen dann in `knownBlocks`.

Da `announceBlocks` und `announcedBlocks` für alle Kommunikatoren gleich sind könnte man diese zusammenlegen auf dem Kommunikationsmanager.

Damit besteht ein Kommunikator im wesentlichen aus den bekannten Blöcken (`knownBlocks`) seines Partners. Diese werden beim Aufbau des Kommunikators ausgetauscht, hierzu wird `announceBlocks` und `announcedBlocks` nicht benötigt, da die Blöcke direkt im `ACK`-Schritt versendet werden.

Randaustausch erfolgt erst bei komplett verbundenen Kommunikatoren und dann nur, wenn beiden Partnern die Blöcke bekannt sind. Das sind die Blöcke aus `knownBlocks` ohne die Zurückgehaltenen in `announceBlocks`.

## Verbindungsaufbau
Merkt ein Prozess, dass er mit einem Anderen kommunizieren muss, so wird ein Kommunikator zu diesem Aufgebaut (per SYN/ACK-Handshake), dabei werden gegenseitig die Blöcke ausgetauscht. So dass am Ende jeder eine Liste der Blöcke auf dem Partner in `knownBlocks` hat.

## schneller Verbindungsaufbau (neues Konzept)
Verbundene Kommunikatoren benötigen gegenseitig die Liste `knownBlocks`. Wird diese beiden Partnern zugestellt, so kann direkt eine Verbindung aufgebaut werden, ohne, dass die beiden Kommunikatoren einen Handshake ausführen müssen. Dies ist insbesondere für das Verschieben von Blöcken nötig und auch möglich, wenn der Block zu einem Partner verschoben wird. Der Quellprozess hat alle Informationen und kann somit den beteiligten Kommunikatoren die Liste `knownBlocks` des jeweils anderen mitteilen.

### Beispiel
A möchte einen Block nach B verschieben, dieser Block hat Nachbarblöcke auf A und C. B und C kommunizieren nicht.
Damit muss der Kommunikator B->C und C->B auf gebaut werden. Da A, die Liste `knownBlocks` von B und C hat kann er B die Liste von C und C die Liste von A senden.

A kennt alle Partner mit denen B für den verschobenen Block kommunizieren muss. Kann also diese Information an B und den jeweiligen Partnern die Information zu B schicken.
- A kann B Informationen zu allen potentiellen Kommunikatoren schicken und den Partnern diese Informationen von B schicken. Oder
- A schickt nur Informationen an relevante neuen Kommunikatoren, die für B neu sind. Dazu muss A zusätzlich wissen, welche Kommunikatoren B kennt. Für neue Blöcke ist es jetzt schon nötig mitzubekommen, welcher Kommunikator diese Information mitbekommen hat. (Benötigt wird dies wenn direkt ein Randaustausch ohne neues Bekanntmachen des Blocks initiiert wird.) Daher gibt es einen Austausch über die Kommunikatoren.

#### Zustände Block
1. **A und B sind synchronisiert**: A sendet `knownBlocks` zu B und C, diese können dann B->C und C->B aufbauen und sind ebenfalls synchron.
- **A announced einen Block**: A sendet `knownBlocks` und einen neuen Block zu B und C. Es kann B->C und C->B aufbauen werden, diese sind synchron. Der neue Block wird auf B und C dem Kommunikator A zu geteilt und berührt die Kommunikation B-C nicht.
- **B announced einen Block**: A sendet `knownBlocks` zu B und C. B sendet einen neuen Block zu A. Es kann B->C und C->B aufbauen werden. Der neue Block muss C allerdings separat (von B) mit geteilt werden. D.h. dieser Block muss von `announcedBlocks` (auf A?) "zurückgelegt" werden in `announceBlocks` von C.
- **C announced einen Block**: Das gleiche wie zuvor, B und C vertauscht.

#### Zustände Kommunikator
1. **Kommunikatoren sind synchron**: alles gut.
- **A announced comm C**: Die Information ist irrelevant für B, da B auch direkt die `knownBlocks` von C bekommt.
- **Andere Fälle .. announced to A**: Das ist irrelevant, ausschlaggebend ist das Wissen von A. Im schlimmsten Fall wird zu viel mitgeteilt.


### Fragen
- muss man die anderen Kommunikatoren kennen?
- was ist mit incoming, announce, etc.

### Umsetzung
- Liste von Kommunikatoren
- Senden von knownBlocks
- Kommunikatoren direkt aufbauen.

CommList: ID,NumBlocks,BlockList,CommList

## Problem 1:
Prozess A erstellt einen neuen Block 1 auf Prozess B, Block 1 hat Nachbarn zusätzlich auf Prozess C, wenn B und C noch keinen Kommunikator haben, wird dieser mit den Informationen von A erstellt.
Soweit so gut.
Angenommen, C bekommt auch einen neuen Block, nennen wir ihn 2. Ah dann wird 2 an alle bekannt gegeben.
Das Problem ist also wenn C, 2 in Schritt n erstellt und 1 in Schritt n+1 erstellt wird.
Genau, für den Nachrichtenaustausch in n+1 sendet C dann an A den neuen Block 2, A sendet gleichzeitig an C Infos über 1 und den Kommunikator zu B und an B Infos über 1 und den Kommunikator zu C. Damit weiß aber B nichts von dem Block 2 und das wird auch nie gesendet.
Also wie löse ich das? C muss B den neuen Block noch mitteilen.
1. Entweder muss C 2 in einem weiteren Puffer halten und dann explizit an B senden.
2. Oder A sendet C alle Blöcke, die A an B gesendet hat, dann kann C die Differenz mit seinen Blöcken raus finden und an B senden.
Zu 1, den 1. Puffer der dann kopiert werden muss gibt es nicht, dieser ist für jeden Kommunikator vorhanden und wie man jetzt sieht auch unterschiedlich.
Zu 2, die Nachricht wird größer, die Differenz kann aber hier direkt bestimmt. werden.

2 ist logischer da alles in die pack- und unpack-Funktion rein passt. zu 1 müsste ein announced Zwischenspeicher auf CommMaster ebene erstellt werden.



# Kommunikatoren
Jeder Kommunikator beinhaltet die Liste der Blöcke und Kommunikatoren des Partner-Kommunikators.
Für aufgebaute Kommunikatoren werden neue Blöcke (über announce), neue und gelöschte Kommunikatoren gesendet und auf der Empfangsseite entsprechend behandelt. Gelöschte Blöcke werden genau so wie regulär erstellte Blöcke später behandelt.
Kommunikatoren im Aufbau werden anders behandelt. Hier wird eine Liste aller Blöcke und eine Liste aller Kommunikatoren bei jedem ACK-Schritt gesendet. Mit dieser Liste werden die knownBlocks und knownComms ersetzt. Das Ersetzten ist wichtig, da damit sichergestellt ist, dass gelöschte Blöcke oder Kommunikatoren nicht in einem vorherigen ACK-Schritt kommuniziert wurden und dann fälschlicherweise in der Liste auftauchen.

Der Kommunikatoraufbau erfolgt teilweise über einen separaten TAG. Die Nachrichten die ein SYN schicken.
Wichtig ist, dass beide Kommunikatoren zur gleichen Zeit in den Zustand establisched wechseln und zuvor noch ein ACK (mit den Listen) gesendet haben.

- ACK senden ist der letzte Schritt vor `established`.
- Nachrichten mit SYN,ACK müssen vor der eigentlichen Nachricht empfangen werden, oder ggf. verworfen werden.
Damit ist sichergestellt, dass ein Kommunikator der zu `established` wechselt eine aktuelle Block- und Kommunikator-Liste enthält. (Die Block-Liste wird über den announce-Mechanismus weiter aktualisiert.)
- Im Aufbau werden die Listen ersetzt und nicht angehangen.

