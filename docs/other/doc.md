# Was im Prinzip geschehen soll (Draft)
Jeder Prozess hat einen Blockmanager, der Blöcke hält und verschiedene Aktionen auf diese anwendet. Aktionen sind Interface die mit den gegebenen Daten (Blöcken) agieren.

Eine Aktion ist 'DynBlock', dies sorgt für das Erstellen und Löschen neuer Blöcke. Zuvor haben die Blöcke entschieden ob sie gelöscht werden wollen oder ob ein Nachbar benötigt wird. DynBlock hat verschiedene Kommunikatoren um die sich der Kommunikationsmanager kümmert. Für jeden Prozess mit dem kommuniziert werden soll gibt es einen Kommunikator.

## Schwierigkeiten / Bedingungen
- Blöcke müssen Eindeutig erstellt werden.
- keine globale Kommunikation.
- Nachbarblöcke müssen sich für einen Randaustausch immer einander kennen.
- Kommunikatoren müssen dynamisch angelegt werde.

## Was gerade schief geht
Die Kommunikatoren sammeln Nachrichten über neue Blöcke ein und versenden diese an alle Kommunikatoren. Auf der Empfangsseite werde die Daten verarbeitet und ein neuer Kommunikator erstellt (z.B. A->B) [Wieso eigentlich?] da nun ein Block auf B erstellt wird, der ein Nachbar auf A hat. Dieser neue Kommunikator kennt zwar nun B aber B kennt noch nicht A und der Kommunikator B->A gibt es auch noch nicht. Daher wird auf diese Kommunikation nicht gewartet. Sollte also B Daten von A empfangen wird B->A gebaut und alles ist gut. Hat B aber Daten von allen anderen bekannten Kommunikatoren empfangen, kann er nicht mehr auf weitere warten.

## Wie kommt es zur Asynchronität?
Über das Nachrichtenbackendkommunikationssystem erhält 6 einen erweiterten Nachbarblock auf 0, daraufhin erzeugt er den Kommunikator 6->0 und sendet im nächsten Schritt []()zu diesem. 0 kann nun Daten unerwartet von 6 empfangen, muss dies aber nicht zwangsweise. Empfängt er, werden die Daten von 6 verarbeitet und ab dem nächsten Mal Daten von 6 erwartet und Daten zu 6 gesendet.

# Versuch einer Lösung (Nummer 1337)
## Ziele
- Es soll Funktionieren
- Blöcke dummer machen
- Kommunikator stärken

Der Kommunikationsmanager erstellt Nachrichten für jeden Kommunikator und versenden diese an diesen. Kommunikatoren sind dabei die Kommunikationseinheiten vom Kommunikationsmanager auf Prozess A zu einem weiteren Prozess (B). Der Kommunikator wäre dann also A->B.
Auch wenn sich Aktoren nicht durchgesetzt haben, sind die Kommunikationsmanager mehr oder weniger Aktoren. Es wird versucht diese weiter in ein Aktorenmodell zu quetschen. Er wird schlauer, so dass er weiß welcher Kommunikator welche Blöcke kennt.

### Mal in Aktoren gedacht:
Aktoren reagieren auf eingehende Nachrichten mit drei verschiedene Reaktionen:

1. Nachrichten an sich selbst oder andere Aktoren verschicken.
- Neue Aktoren erzeugen.
- Das eigene Verhalten ändern.

Normalerweise werden Nachrichten zunächst im Posteingang gespeichert und nach dem FIFO-Prinzip abgearbeitet.
Der Nachrichtenaustausch erfolgt asynchron eine Bestätigung nach dem Senden gibt es nicht. Das Aktorenmodell legt nicht fest, wie lange die Vermittlung dauert oder in welcher Reihenfolge die Nachrichten ankommen.

#### Doch ist vieles anders.
Die Kommunikationsmanager (das wären die Aktoren) sollten innerhalb von einem Zeitschritt alle nötigen Nachrichten von ihren Kommunikatoren bearbeiten. Jeder Kommunikator erwartet eine Nachrichten von seinem Partner Kommunikationsmanager. Damit reagiert der Kommunikationsmanager nicht mit Nachrichten auf Nachrichten. Das FIFO-Prinzip ist vernachlässigbar, da die Reihenfolge innerhalb eines Zeitschrittes egal ist, vielmehr wird daraus eh eine Nachricht gemacht. Spätestens beim Randaustausch muss alles synchron sein, so dass nicht auf die Asynchronität gesetzt werden kann. Neue Aktoren können nicht angelegt werden, es können neue Kommunikationskanäle aufgemacht werden, demzufolge wären die Kommunikatoren die Aktoren, aber die sind ganz dumm.

#### Wie muss es denn sein?
Der Kommunikationsmanager erledigt die Kommunikationen für den Blockmanager. Er holt sich Informationen von den Blöcken und baut daraus Nachrichten. Er sollte für die Synchronität sorgen. Sorgen wir für die Synchronität, indem die Partner übereinander Bescheid wissen und gegebenenfalls ein pending einbauen. Dann sollte es folgende Listen geben:
- Blöcke, die der Kommunikationsmanager/Blockmanager lokal bereitstellt.
- Blöcke, die der auf dem jeweiligen Kommunikationspartner lokal sind.

A | A  | B | B
---|---|---|---
 e | a | b | f
   | c | d |

**Beispiel 1:** A hat Block a kurz geschrieben A(a) und B(b), a und b sind Nachbarn wie in der Tabelle oben gezeigt. Die Initialisierung: für jeden Block auf A werden Nachbarn gesucht, das ist für a, b, so dass dann ein Kommunikator A->B aufgebaut wird, dieser kennt nun die lokalen Blöcke von A (a) und Blöcke die B hat {b} bei der Initialisierung kann zusätzlich noch davon ausgegangen werden, dass alles symmetrisch ist und somit auch B->A(b){a} angelegt wird.

Wir bleiben bei der Schreibweise: A->B(a){b}. Auf Kommunikationsmanager A gibt es den Kommunikator zu B, A kennt (a) und der Kommunikator A->B weiß dass B {b} hat.

**Beispiel 2:** A->B(a){b} und B->A(b){a}. Nun will A c anlegen. A bestimmt wo dies geschehen soll, auf A. Diese Nachricht `neu:c(A)` wird an alle relevanten Nachbarn geschickt also an B. Die Betroffenen Kommunikatoren sind dann A->B(a,c){b} und B->A(b){a,c}

**Beispiel 3:** A->B(a){b} und B->A(b){a}. Nun will A c anlegen und B d. A, B bestimmt jeweils wo dies geschehen soll. A->B sendet `neu:c(A)`, B->A sendet `neu:d(B)`. Die Betroffenen Kommunikatoren sind dann A->B(a,c){b,d} und B->A(b,d){a,c}

**Beispiel 4:** A->B(e){} und B->A(f){}. Nun will A a anlegen und B b. A, B bestimmt jeweils wo dies geschehen soll. A->B sendet (aus welchem Grund auch immer) `neu:a(A)`, B->A sendet `neu:b(B)`. Die Betroffenen Kommunikatoren sind dann A->B(e,a){b} und B->A(f,b){a}

A | A  | B  | B
---|---|--- |---
 e |   |    | f
 g |*c* |**d** | h
**Beispiel 5:** A(e,g), ein Kommunikator (zu B) existiert nicht und B(f,h) hat auch keinen Kommunikator (zu A). Legt B nun d an, wird erst mal nichts gesendet. Allerdings sind g und d im erweiterten Nachbarschaftsstern. Über das globale Kommunikationsnetz wird die Nachricht `neu:d(B)` versendet. Irgendwann erfährt A davon. Es wird dann A->B(e,g){d} erzeugt.
1. Im nächsten Schritt sendet dann A seine Blöcke zu B und B erstellt B->A(f,h,d){e,g} und im übernächsten Schritt sendet auch B seine Blöcke zu A, damit wird A->B(e,g){f,h,d} ergänzt. Synchronität hergestellt.
2. Im nächsten Schritt erzeugt A c und sendet `neu:c(A)` an B, damit werden A->B(e,g,c){d}
B->A(f,h,d){e,g,c}. Das ist zwar nicht Synchron, aber macht keine Probleme, im übernächsten Schritt hat auch B seine Blöcke an A mitgeteilt und Synchronität hergestellt.

A | A  | B  | B
---|---|--- |---
 e |   |**b** | f
 g |*c* |*d* | h
**Beispiel 6:** A(g), ein Kommunikator (zu B) existiert nicht und B(f) hat auch keinen Kommunikator (zu A).
1. Nun wird b(B) erzeugt und irgendwann bei A über das globale Kommunikationsnetz angekündigt, daraufhin wird A->B(e,g){b} erstellt.
- Direkt im nächsten Schritt werden c(A) und d(B) erstellt. A->B sendet also die Nachricht `neu:c(A)`. B->A existiert noch nicht und kann demzufolge auch nichts senden.
- Auf B wird also B->A(f,h,b,d){c} erzeugt. Der Kommunikator auf A wird zu A->B(e,g,c){b}. Das ist nicht synchron, da B nun c kennt, aber A nicht d und c und d Randaustausch betreiben :bomb: :cry:

Angenommen es läuft anders ab.
1. A->B(e,g){b}[] damit weiß A, dass B nichts von A weiß.
-  A->B(e,g,c){b}[e,g,c] B->A(b,d,f,h){e,g,c}[] hier, kennt A d nicht B kennt c aber weiß, dass A d nicht kennt.
- Synchronisation wird durch die erste Kommunikation B->A erreicht: A->B(e,g,c){b}[e,g,c] B->A(b,d,f,h){e,g,c}[b,d,f,h].
Erkenntnis: [] ist leer oder gleich (), das ist *fast* pending.

#### Wie es sein muss!
Alles ist synchron. A->B kommuniziert einen neuen Block

##### Neuer Kommunikator
1. Zeitschritt n
  1. A->B wird erstellt.
  - A sendet zu B.
  - A erwartet nichts von B.
  - B erwartet nichts von A
  - A kann etwas von B empfangen, aber nur wenn B->A auch erstellt wird.
  - B kann aber etwas von A empfangen, wenn die Nachricht von A schneller da ist als die Letzte von den Anderen.
- Zeitschritt n+1 (Fall A hat nichts von B und B nichts von A empfangen)
  1. A sendet zu B
  - A erwartet nichts von B.
  - B hat nun mindestens die Nachricht aus Zeitschritt n empfangen und erstellt B->A
- Zeitschritt n+2
  1. Nun ist synchron


1. Zeitschritt n
  - wie gehabt
2. Zeitschritt n+1 (Fall A hat nichts von B und B hat von A empfangen)
  1. A sendet zu B
  - B sendet zu A
  - A erwartet nichts von B.
  - B empfängt von A. Sicherstellen, dass er Nachricht aus Zeitschritt n+1 empfängt.
  - A kann etwas von B empfangen, wenn die Nachricht von B schneller da ist als die Letzte von den Anderen. Wenn er empfängt, ist es schon synchron.
- Zeitschritt n+2
  1. Nun empfängt A von B, muss sicherstellen, dass es die Nachricht aus Zeitschritt n+1 ist.
  - Synchronität hergestellt.

1. Zeitschritt n
  - wie gehabt
2. Zeitschritt n+1 (Fall A hat von B und B hat von A empfangen)
  1. A sendet zu B
  - B sendet zu A
  - Beide erwarten Nachrichten, Synchronität vorhanden.

Verhalten von B:
- B wartet auf Nachrichten
- Eintreffende Nachricht ist von bekanntem Kommunikator -> bearbeiten.
- Eintreffende Nachricht ist von unbekanntem Kommunikator ->

#### Kommunikator Handshake nach TCP Vorbild.
Kommunikatoren senden bei jedem Zeitschritt. Für etablierte Verbindungen erwartet der Kommunikationspartner in jedem Zeitschritt eine Nachricht und wartet auf diese.
Für neue, also noch nicht etablierte, Verbindungen wird nicht auf das Eingehen von Nachrichten gewartet. Eine neue Nachricht muss also beim Kommunikationspartner nicht zwingend ankommen. Das passiert, wenn alle Nachrichten der anderen Kommunikatoren eher eintreffen. Sollte dies so sein wird davon ausgegangen, dass die Nachricht im nächsten Zeitschritt im Puffer liegt und somit dann als erstes empfangen wird.
Bei der ersten neuen ankommenden Nachricht wird ein Kommunikator erzeugt, der dann auf eingehende Nachrichten wartet. Wird der Zeitschritt mitgesendet, so kann der neue Kommunikator auf die Nachricht des aktuellen Zeitschrittes warten.

Ein neuer Kommunikator (A) sendet eine SYN-Anfrage (Bedeutung: Ich möchte mit dir reden, bitte nimm mich in deine Liste auf.). Daraufhin antwortet der Empfänger (B) mit einem ACK (Bedeutung: Du willst mit mir reden ich habe dich aufgenommen und warte auf Nachrichten von dir.) und stellt ebenfalls eine SYN-Anfrage. Da A noch nicht auf Nachrichten von B wartet geschieht hier das Gleiche wie zuvor. Falls A nicht die Nachricht bekommt, wird sie im nächsten Zeitschritt empfangen. Damit B auch erfährt, dass A die Nachricht empfangen hat sendet auch A ein ACK. Zu dem Zeitpunkt wo A das ACK sendet und B dieses im selben Zeitschritt empfängt sind beide synchron und können mit dem Randaustausch beginnen.

Sonderfall, wenn A und B gleichzeitig eine SYN-Anfrage schicken, antworten beide nach Empfang mit einem ACK. Wurde das ACK empfangen und gesendet, so warten beide auf Nachrichten des Anderen.

# Zusatz Kommunikation
**Beispiel 1:**

1|
---|---|---
a | b | c
d | *e* | *f*
g | *h* |

A(a,b,c) und B(d,g) neu: e(A), f(A), h(B). A bekommt nicht mit, dass h angelegt werden soll. Problem ist h kennt e, aber nicht umgekehrt.


# Neues System
Kommunikatoren bauen sich auf und wissen gemeinsam, wann beide mit einander kommunizieren!

**Fall 1: Aufbau:**
A sendet seine Blöcke an B, B sendet seine Blöcke an A. Damit wissen beide von den Blöcken der anderen.

**Fall 2: neuer Block (A und B bekommen die Nachricht):**
A erstellt den Block, B weiß, dass A den Block hat.

**Fall 3: neuer Block (nur A bekommen die Nachricht):**
A erstellt den Block, B weiß von nichts. Vor dem Randaustausch muss B Bescheid wissen. A sendet den neuen Block an B.

Fall 1: einmaliges senden der lokalen Blöcke und speichern der Nachbarblöcke
Fall 2: hinzufügen zu den Nachbarblöcken
Fall 3: A muss den neuen Block noch bekannt machen und B speichert den dann später ab. Dazu muss aber A wissen, dass B noch nichts mitbekommen hat.

## Nachrichten:
- state: Zustand des Verbindungsaufbaus,
- blockIDs: Liste mit den IDs von den Blöcken,
- newBlocks: Liste mit neuen Blöcken und mehr Informationen,
- Communicators: Liste der Kommunikatoren,
- delBlocks: Liste gelöschter Blöcke,
- localBlocks: wird nicht mehr benötigt,
- numBlocks: wird nicht mehr benötigt.

Bei neuen Blöcken muss dann noch mitgeschickt werden, wer mitbekommen hat wo sie erstellt werden um zwischen Fall 2 und 3 zu unterscheiden.

Zwei Listen Blöcke des Partners und Blöcke die noch zum Partner geschickt werden müssen.

- Liste mit noch zusendenen Blöcken senden
- Blöcke aus dieser Liste entfernen und Randaustausch aktivieren
- neue Nachrichten bearbeiten, ggf. Randaustausch aktivieren, oder Blöcke zu der Liste hinzufügen.





