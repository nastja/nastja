# Dynamisch Blöcke erstellen und löschen
Blöcke, deren Werte sich durch einen Rechenschritt nicht ändern, müssen nicht gerechnet werden. Das passiert, wenn alle Werte gleich sind, wir nennen so einen Block *konstanter Block*. Ist ein Block konstant, kann er gelöscht werden. Ändert sich der Rand eines Nachbars zu einem konstanten Block, so ist der konstante Block nicht länger konstant und muss gerechnet werden.

Im Folgenden genügt es zwei Blöcke mit einem gemeinsamen Rand zu betrachten. Seien A, B benachbarte Blöcke mit gemeinsamen Rand, so besteht der Rand von A aus einer Schicht von B und umgekehrt.

## Erkennen von neu zu erstellenden Blöcken an der Nachbarposition.
Sei B konstant und daher nicht vorhanden. B konstant bedeutet, dass auch der Rand konstant ist, daraus folgt, dass die Randschicht in A ebenfalls konstant ist. Ob sie konstant 0 oder konstant 1 ist, ist OE unerheblich.

Der Rand liegt in A vor, so kann die innerer Randschicht von A darüber entscheiden ob ein neuer Block erstellt wird. Da B konstant ist, ist die Schicht auch konstant. Ändert sich die Schicht ist B nicht mehr konstant und muss angelegt werden. Änderungen passieren langsam genug, so dass keine Zelle "übersprungen" wird. Damit reicht ein Zeitschritt für die Erkennung aus.

Der Block B wird voll oder leer erstellt je nachdem ob der Rand konstant 0 oder konstant 1 war. Danach wird der Rand ausgetauscht. Damit ist der erste Rand, der nicht mehr konstant ist also direkt bei dem neu erstellten Block B vorhanden, so geht keine Information verloren.

## Erkennen von vollen oder leeren Blöcken die gelöscht werden können.
Ist ein Block konstant, so kann er gelöscht werden. Die Erkennung muss jedoch vor dem Randaustausch stattfinden, da der Randaustausch nach dem DynBlock-Schritt wieder für Konsistenz sorgt. Die Randschicht in dem Block ist vom Zeitschritt n-2. Die Randschicht im anderen Feld ist vom Zeitschritt n-1. So kann über ein konstanten Block nur mit Hilfe der alter Zeitschritte entschieden werden. Nach einem Verschieben ist der Rand leer, so dass 2 Zeitschritte gewartet werden muss, bevor entschieden werden kann ob der Block konstant ist.

- Was ist mit dem Zeitschritt n-1, dann müsste man nur ein Feld warten.
- Der Rand kann sich mittlerweile geändert haben, so dass ein Block konstant wird, dessen Rand nicht mehr konstant ist, dann wird für einen Zeitschritt mit der Falschen Randbedingung gerechnet.
- MPI Ränder könnten eine Vorhersage treffen, ob der Rand konstant bleibt und dies mitteilen. Dann kann auf konstant im Inneren + konstant in den Rändern untersucht werden.
