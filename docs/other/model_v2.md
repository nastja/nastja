# Ausgangslage (Draft)
Das aktuelle Modell hat eine Zwischenschicht – die Kommunikatoren, hier wird die Verbindung und die Blockinformationen aufbewahrt. Die Ränder der Blöcke kommunizieren nur, wenn die Kommunikatoren eine aufgebaute Verbindung haben. Der Verbindungsaufbau braucht 3+ Zeitschritte. Bei neuen Nachbarblöcken werden diese auf dem Nachbar (in der lokalen Nachbarschaft) mit den wenigsten Blöcken erstellt.
## Problem
Es können Blöcke mehrfach erstellt werden, dann aus unterschiedlichen Richtungen.

### Fall 1
<pre>
012
  5
</pre>
- 0 und 5 existieren auf den Prozessen `A` bzw. `C`, sie sind aber in keiner direkten Nachbarschaft.
- zunächst wird 1 von 0 angelegt und verbreitet, so dass 5 dies später mitbekommt und einen Kommunikator `A-C` aufbaut.
- wird dann 2 von 1 angelegt, so wird auf den Randaustausch gewartet bis der Kommunikator `A-C` aufgebaut ist.
- legt nun 5 ebenfalls 2 an, obwohl der Kommunikator noch nicht da ist, gibt es den Block 2 doppelt.

Das ist das Problem, zu speziell? Da ja 1 komplett 'durchwachsen' werden muss. Nein, siehe nächstes Beispiel:

### Fall 2
<pre>
012
 45
</pre>
- 0, 4 und 5 existieren auf den Prozessen `A`, `B` bzw. `C`, damit gibt es die Kommunikatoren `A-B` und `B-C` aber in keine direkte Verbindung zwischen 0 und 5 aka Kommunikator `A-C`.
- 4 legt 1 an, dieser wird auf `A` erstellt. Damit bekommt 5 mit, dass  Kommunikator `A-C` benötigt wird und baut ihn auf.
- nun wird 2 von 1 angelegt, man denkt an die untere rechte ecke, die kann sehr schnell durchwachsen. A kommuniziert dies zu B wo 4 es mit bekommt aber nicht zu C, da der Kommunikator noch nicht da ist, somit weiß 5 davon nichts. Der Randaustausch wird zurückgehalten, bis der Kommunikator `A-C` aufgebaut ist.setName
- und jetzt kommt's, 5 legt ebenfalls 2 an, da er noch nichts von 2 weiß so, gibt es den Block doppelt.

# Mögliche Lösungen
## Im Gebiet triggern
Idee: nicht der Rand sondern n Schichten vorher wird nachgeschaut, ob ein neuer Block benötigt wird. **Bringt nichts für Fall 2**

## Doppelte Blöcke zulassen und mergen
Macht Fehler in der Berechnung, wie merged man. Mehrfacher Randaustausch.

## Blöcke vordefiniert platzieren.
oder global platzieren `mod size`? Global geht nicht, da dann alle Kommunikatoren da sein müssen oder es zu einer starken Lastunausgeglichenheit kommt.

# Voraussetzungen (keine Fehler)
- konsistente Ränder, d.h. sobald ein Block den Rand berührt muss der nächste Block da sein. (nicht erst Kommunikator Aufbau)
- Die Wachstumsgeschwindigkeit ist beschränkt, die Zeit die es braucht durch einen Block zu wachsen muss ausreichen um die nächsten Nachbarblöcke/Kommunikatoren zu finden/aufzubauen.
- Krummes – L förmiges – Wachstum ist davon ausgeschlossen. D.h. diese Blöcke müssen direkt mit angelegt werden. Wenn keine Richtungsabhängigkeit berücksichtigt werden soll müssen 3x3x3 Blöcke um den Ursprünglichen da sein, um diese zu verlassen benötigt man dann die Zeit eines Durchwachsen. Bis dahin sollten neue 3x3x3 Blöcke gefunden worden sein.
- Neue Blöcke brauchen zunächst nur die Information über Blöcke des ursprünglichen Blocks, also genug Zeit um die Umgebung aufzubauen.
- Für den Randaustausch werden nur direkte Nachbarn benötigt.

## Global vordefinierte Zuordnungen?

- Wenn die Blöcke später verschoben werden, wie kann sichergestellt werden, dass die dann auch gefunden werden?
- Kommunikatoren zwischen Blockbereichen?

# Umsetzung
- Bekannte Blöcke 5x5x5, dann ist immer die Zeit da um durch einen kompletten Block zu wachsen.

