# Löschen von Kommunikatoren (Draft)
Nachdem nun Kommunikatoren aufgebaut werden, entweder über den Handshakemechanismus oder über das FastCommunicationSetup wächst die Anzahl der Kommunikatoren pro Prozess stetig an. Eine Lastverteilung lässt diese Zahl noch schneller steigen. Daher wird zunächst, vor der Lastverteilung das Löschen von Kommunikatoren gemacht. Insbesondere stellt sich an dieser Stelle auch die Frage wie muss eine Lastverteilung aussehen, die möglichst Wenige Kommunikatoren benötigt.

## Detektieren des überflüssigen Kommunikators
Wenn alle lokalen Blöcke keinen Block des Kommunikators in ihrer Nachbarschaft haben ist dieser überflüssig. Dies kann dann festgestellt werden,
1. wenn von Remote ein Löschauftrag von einem Block von diesem Kommunikator bearbeitet wird. Es werden alle Nachbarschaften der lokalen Blöcke durchsucht und nach dem Kommunikationsrank untersucht. Ist der Rank da, wird der Kommunikator noch benötigt.
2. wenn ein lokaler Block gelöscht wird. Dann fallen einige Blöcke aus der Nachbarschaft weg. Tauchen deren Ranks in keiner anderen Blocknachbarschaft auf, kann dieser Kommunikator gelöscht werden.

Dies ist symmetrisch, so sind beide Kommunikatoren gleichzeitig weg. Dennoch haben die Anderen eine CommList, wo dieses ebenfalls entfernt werden muss.

## Konsistenz der lokalen Nachbarschaft
Eine neue Nachrichtenart mit der Löschabsicht an alle Kommunikationspartner muss verschickt werden. Und die Kommunikatoren aus der Liste entfernt werden.

1. Erster Schritt: A möchte Block 1 löschen und sendet dieses
- Alle Partner von A bekommen diese Nachricht.
  - prüfen nach weiteren gemeinsame Blöcken und identifizieren den überflüssigen Kommunikator, in diesem Beispiel sei B überflüssig.
  - A löscht den Block und findet ebenfalls B als überflüssigen Kommunikator.
  - A und B bereiten die Nachricht zum Austragen aus den anderen Kommunikatoren vor. Und löschen B bzw. A.
- Nächster Schritt: A und B senden die Nachricht.
- A und B werden bei den anderen ausgetragen.

Was ist, wenn nun aber C einen Block auf A erstellen möchte und dieser Block mit B kommuniziert, dann wird A-B wieder benötigt.

Die Liste der bekannten Kommunikatoren wird verwendet:
- zur Erstellen der schnellen Kommunikator Verbindung
- zur Feststellung ob der Randaustauschpartner ebenfalls die Nachricht bekommen hat.

### Fälle
A, B senden jeweils zu C, dass sie keine Kommunikation mehr haben, behalten ihre Kommunikation aber noch für einen Zeitschritt
1. **keine Weiteren Nachrichten.** alles gut, im nächsten Schritt wird A-B gelöscht.
- **C erstellt Block auf A mit Nachbar B (C kennt A,B)**
  - sind die Informationen über die jeweilige Bekanntheit nicht mehr da, so wird die Kommunikation mitgeschickt.
  - sind die Informationen noch da, werden sie nicht mitgeschickt. In dem Fall müssen A, B weiter Kommunizieren. Oder den Kommunikator ggf. neu aufbauen.
- **D erstellt Block auf A mit Nachbar B (D kennt nur A)** A sollte mit B kommunizieren, dies könnte aber auch über einen neuen Kommunikatoraufbau passieren.
- **A erstellt Block auf C mit Nachbar B** Dies ist der einzig kritische Fall. Das Wissen von C über das senden der gleichen Information muss mit dem senden der Information konsistent sein. D.h. entweder bleibt die Kommunikation A-B einen Schritt länger bestehen, oder bevor C Informationen verarbeitet muss für ihn das gegenseitig kennen von A und B entfernt werden.

### Umsetzung
A, B löschen den Kommunikator und senden eine delComm Nachricht. Die Kommunikatoren werden gegenseitig aus der Kommunikatorenliste genommen. Danach wird fortgefahren, so dass es so aussieht als ob der Kommunikator A-B nie bestand. Ggf. wird der Kommunikator über einen Handshake neue aufgebaut.

Problem: Lastverteilung geht davon aus, dass der Kommunikator besteht. Muss dann neue mit gesendet werden. Ein Wiederherstellen würde voraussetzen, dass A-B weiterhin konsistent ist, dass kann aber durch den fehlenden Austausch nicht sichergestellt werden. Eine Erleichterung gibt es aber, wurde im vorherigen Schritt dem Versender keine Löschnachricht mitgeteilt, so kann auch kein Kommunikator gelöscht worden sein.

WICHTIG: Was passiert mit Kommunikatoren die während eines schnellen Kommunikatoraufbaus erstellt werden, bekommt der Partner diese mit?

Wenn 2 gleichzeitig gelöscht werden, bekommen dann alle die Informationen?
TODO readannouncement in communicator for delComm