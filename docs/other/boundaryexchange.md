# Randaustausch
- BC BorderType::inner
- DynBlock-Stuff
- BC BorderType::notinner

- preExecute für inner border
- DynBlock-Stuff
- preExecute für nicht inner border
- per dim:
    + preExecute
    + postexecute

MPI:
pre  = open
exec = copy & send
post = recv & copy

Inner:
pre  = dedect Border
exec = copy
post = none

Andere:
pre  = none
exec = calc/copy
post = none

# Randaustausch für Communication-Hiding
Alle Randwerte müssen gleichzeitig verschickt werden können.
Es darf nicht auf anderen Rand gewartet werden, was den X-Y-Z-Austausch ausschließt.

Die Feldrandbedingung muss auf das verteilte Gebiet genauso wirken wie sie auf ein nicht verteiltes Gebiet wirken würde.
An Kanten und Ecken können also mehrere (auch unterschiedliche) Randbedingungen aufeinander stoßen.
Dies wird hier erst Mal nicht berücksichtigt, kann aber für große Stempel wichtig werden.

Die rote Zelle in [boundary] kann aus dem berechneten Rand von A (blau) kopiert werden oder aus dem kopierten Rand von A (grün) berechnet werden.
Für die zweite Methode ist kein besonderer Randaustausch für Blöcke mit Gebietsrand nötig, hier muss der Gebietsrand nach dem MPI-Rand berechnet werden.

Die innere Randbedingung prüft im vorgelagerten Schritt nur auf Randerreichen und verhält sich ansonsten wie eine Gebietsrandbedingung.

# Definition vom Rand
6 Flächen
12 Kanten
8 Ecken

26 Randaustausche.
MPI-Tag ist integer 32bit, wovon bis jetzt 2 bit für den Basistyp vergeben sind.
Bleiben 2^30 verschiedene Tags für jede BlockID werde 26 benötigt macht 4mio oder 345 Blöcke je Dimension.
Im ersten Schritt sollte dies reichen, da es jedoch beschränkt, kann eine lokale ID je Nachbarworker erstellt werden.
A und B wissen jeweils wie viele Ränder sie austauschen.
Sender- und Empfängerblöcke liegen sortiert vor, so muss nur einer die Reihenfolge also ID berechnen. (Bei 20bit BaseID bleiben >40.000 lokale IDs)

## MPI_Type
int MPI_Type_create_subarray(
  int ndims,
  int array_of_sizes[],
  int array_of_subsizes[],
  int array_of_starts[],
  int order,
  MPI_Datatype oldtype,
  MPI_Datatype *newtype
);

Typen sind feldspezifisch


[boundary]: images/boundary.jpeg "Randaustausch"

