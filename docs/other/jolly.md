
in meiner dunklen Erinnerung habe ich "jolly" bei dem Namensvorschlag für Ubuntu uralt in der Diskussion gesehen. Es ging damals in Richtung "hervorragend, großartig, mächtig" was mich dazu bewegt hat es für die Vielfältigkeit der möglichen Algorithmen im Solver zu verwenden.

Zu den Übersetzungen:
Google kennt es als Adverb "mächtig", "ganz schön"
dict.cc sagt als Adverb "sehr" oder als Adjektiv dann "jolly good" als "prima" "hervorragend"
linguee.de übersetzt es als Adjektiv mit "mächtig" "prächtig" "fidel", als Adverb wieder "jolly good" mit sehr
dictionary.com kennt es als Adjektiv in den Bedeutungen:
- Herrlich / Charmant
- great, thorough also Prima: großartig, gründlich, umfassend, solide, perfekt...
und wieder als Adverb "sehr"
Wobei sich das wohl eher auf das britische Englisch bezieht.

Das Ubuntu hieß damals dann übrigens jaunty, was aber auch eher nicht so super passt.

