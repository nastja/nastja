# Loadbalancing (Draft)
Das Ziel ist eine möglichst gleichmäßige Verteilung der Blöcke bzw. später auch der Last, wenn Blöcke unterschiedlich viel rechnen müssen.
Prinzipiell macht ein Umschichten nur dann Sinn, wenn der Block mit der meisten Last entlastet wird. Da dieser den Takt vorgibt, kein andere Prozess kann schneller rechnen. Welcher Prozess die meiste Last hat ist nur global festzustellen. Als Maß einer (erfolgreichen) Lastverteilung kann mehr Blöcke als der Durchschnitt dienen, wobei der Durchschnitt ebenfalls nur global ermittelt werden kann.

Laplace- und Poissongleichung auf Graphen sind gängige Methoden um die Last zu verteilen. Das sind einfache Diffusionsprozesse. Auf nicht regulären Graphen dient dazu einfach der Durchschnitt der Nachbarn. D.h. die neue Last auf einem Knoten ist gleich die Summe aller Nachbarlasten / Anzahl der Nachbarn. (Hier beinhaltet Nachbarn den Knoten selbst.) Diese lokale Diffusion konvergiert zum globalen Durchschnitt.

Die Verteilung, also welcher Knoten welchem Knoten wie viel Last gibt, lässt sich nur global berechnen, es ist leicht zu sehen, dass zumindest die neue Last aller Nachbarn bekannt sein muss. Schon dies ist lokal gesehen nicht möglich. Dies ist in etwa äquivalent zu einem FEM-Ansatz wo die Verteilung durch ein globales Gleichungssystem gelöst wird.

D.h. die Verschiebungen der Last kann nicht lokal optimal bestimmt werden. Ein Teilen der Überlast in potentielle Partnerstücke funktioniert nur für viel Überlast und wenige Partner, da sonst die Teile < 1 werden und damit nicht mehr verteilt werden können. Ein Runden ist also nicht möglich. Ein Abschneiden und zuteilen des Rest kann aber nur funktionieren, wenn die Zuweisung in der lokalen Nachbarschaft und die Zuweisungen der Nachbarn bekannt ist, dies kann nicht ohne Kommunikation/globales Wissen geschehen.

## Lösungsansatz
Statt einer Zuteilung wird die Methode umgedreht. Ein Prozesse (A) mit weniger Last macht ein Angebot, dieses Angebot kann bei dem DynamicBlock-Austausch mitgesendet werden. Auf Empfängerseite (B) kann diese Angebot korrigiert werden. Gelöschte Blöcke auf A bekommt der B mit. Neue Blöcke auf A bekommt B nur teilweise mit, genau dann wenn der Ersteller des neuen Blocks in der gemeinsamen Nachbarschaft von A und B ist. B kann in diesem Fall die Anzahl der neuen Blöcke schätzen. Alternativ kann A schon ein reduziertes Angebot stellen. B kann das Angebot annehmen, teilweise erfüllen oder ablehnen.
Wird ein Angebot gestellt muss eine Antwort folgen, selbst wenn kein Block übermittelt wird.

### Details der Berechnung
- Angebot unterbreiten:
  - mögliche Blöcke gleichmäßig an alle Maxima anbieten
  - Danach nach Rank oder Zufall oder Rotiren
- Angebot annehmen:
  - zunächst minimale Anzahl an Blöcke
  - Dann Maximales Angebot
  - Dann Rank oder Zufall oder Rotiren

## Datenübermittlung
Da nicht voraus gesagt werden kann, welcher Prozess einen Block verschiebt, oder erhalten soll muss jeder Kommunikator an der Kommunikation teilnehmen.
Jeder muss also senden und empfangen. Mindestens eine leere Lister der verschobenen Blöcke mit Ziel.

### Zustand des Kommunikatorsystems
Kommunikatoren sind aufgebaut oder befinden sich im Aufbau. Selbst Kommunikatoren, die in dem DynBlock Schritt aufgebaut wurden, zählen als vollständig aufgebaut. Daher wird bei der Lastverteilung nur aufgebaute Kommunikatoren berücksichtigt. Nicht aufgebaute Kommunikatoren nehmen eh noch nicht am Randaustausch teil, so dass hier auch keine Verbindung zu Blöcken besteht. Der reguläre Aufbau und die dabei enthaltende Block/Comm Synchronisation reicht aus um verschobene Blöcke mitzubekommen, da hier die ganze Blockliste gesendet wird.

Wie sehen die Zustände nach dem DynBlock-Schritt aus?
Der Local möchte einen Block verschieben.

- Partner sind synchron: OK
- Local hat einen neuen Block, remote kennt diesen schon: OK
- Remote hat einen neuen Block, local kennt diesen schon: OK
- Local hat einen neuen Block, remote kennt diesen noch nicht: siehe Block-Problem
- Remote hat einen neuen Block, local kennt diesen noch nicht: siehe Block-Problem
- Local hat einen Block gelöscht, remote ist dies schon bekannt: OK, schon alles erledigt, Block taucht nicht mehr auf.
- Remote hat einen Block gelöscht, local ist dies schon bekannt: OK, schon alles erledigt, Block taucht nicht mehr auf.

- neuer Kommunikator: OK, befindet sich im Aufbau und wird nicht berücksichtigt.
- gelöschter Kommunikator: Kommunikatoren werden symmetrisch gelöscht, so dass nicht mehr an einen gesendet wird, der nicht mehr empfangen möchte. ABER siehe Kommunikator-Problem.

Neue Blöcke, also Blöcke mit gesetztem ttl-counter sind von der Verschiebung ausgenommen.

### Kommunikator-Problem
A möchte einen Block an B senden, B und C haben ihren Kommunikator abgebaut. A hat davon noch nichts mitbekommen und denkt weiter, dass B C kennt und umgekehrt. Der Block treibt Randaustausch mit C (mit B auch, da er sonst vermutlich nicht zu B geschoben wird). Randaustausch ist hier zu speziell, die Nachbarschaft reicht schon aus, da hier nicht mehr davon ausgegangen werden kann, dass es genügend Zeitschritte für einen Verbindungsaufbau geben kann. Die Frage ist also ob, B und C ihren Kommunikator abbauen können, wenn es  einen Block auf A gibt, der beide kennt. Ja:
```
B·XC
·A··
```
X ist auf C und wurde grade gelöscht, so braucht B-C keine Verbindung mehr. A-B und A-C Kommunizieren aber weiter. Um Konsistenz wieder herzustellen muss also der Kommunikator B-C wieder aufgebaut werden, also als FastCommunicationSetup-mitgeschickt werden. Da A nicht weiß ob B-C noch existiert müssen alle Kommunikatoren als FCS mit geschickt werden, es sei denn man kann er vorher bestimmen, dass dieser Kommunikator noch sicher da ist.

### Neuer, nicht bekannter Block-Problem
A sendet einen Block an B. B hat einen neuen Block, den A noch nicht kennt. Würde der Block nicht verschoben werden wird er später in die lokale Nachbarschaft aufgenommen, das kann nach der Verschiebung direkt passieren. Dabei entsteht aber kein Problem. Es muss nur diese Block Verbindung dann aufgebaut werden auf B. Es ist unklar ob die Standardmechanismen hier greifen, eher nicht. Es sollte genügen die announceBlock-Liste von A auf B dem verschobenen Block zu zuführen (dies geschieht lokal auf B).

A sendet einen Block an B. A hat einen neuen Block, den B noch nicht kennt. Dies kann auftreten, wenn A-B gerade neu ist und A den neuen Block selbst erstellt oder ein C den Block auf A erstellt aber B-C nicht existiert. In beiden Fällen kann ein direkter Randaustausch möglich sein, d. h. B muss von dem neuen Block wissen. Es sollte genügen die announceBlock-Liste für B auf A mit zusenden und dann verarbeiten.

### Aufbau der Lastverteilungsnachricht.
1. Liste von BlockID, neue Position: an Alle
- Kommunikator FCS-Informationen über T: an alle Prozesse aus P
- Kommunikator FCS-Informationen über P: an T
- neue Blöcke aus announceBlock an T
- Daten: an T

T = Zielprozess
P = Menge aller Prozesse, die sich in der Nachbarschaft vom zu verschiebenen Block befinden ohne das Ziel selbst.


### Konsistenz bei neuen FCS.
#### gelöschte Blöcke.
A schickt ein FCS an B, C.

- Wenn A löscht, bekommen B,C dies direkt mit.
- Wenn B Block a löscht, bekommt A dies mit, jedoch C nicht. (Fall C symmetrisch zu B).
B löscht erst dann, wenn sichergestellt ist, dass alle Blöcke, die a in ihrer Nachbarschaft haben, die Existenz von a mitbekommen haben. Falls also C a benötigt wüsste es C schon.
Die Inkonsistenz besteht also darin, dass C den Block a zu den knownIDs von B zugeordnet hat.
Welche Probleme können Auftreten? Reicht ein Löschen im nächsten Schritt? Dann genügt es wenn B gelöschte Blöcke zu neuen Kommunikatoren hinzufügt. Bzw. da nur im DynBlock aber nicht in LoadBalance die gelöschten Blöcke bekannt sind eine Liste aller Blöcke, oder so.
Schlimmster Fall: a ist in der direkten Nachbarschaft eines neuen Blocks b für C. Block b kann verschoben von A sein, oder von einem anderen Prozess auf C erstellt werden. C kann ihn nicht selbst erstellen, da sonst a schon bekannt gewesen sein muss.



