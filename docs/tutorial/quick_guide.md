\page quick_guide Quick Guide

[TOC]

This is a quick and short introduction into NAStJA. It's not recommended for beginners!\n
Nevertheless this document can be used for a quick peak into NAStJA for checking _What is NAStJA?_.

## Prerequisits
- CMake 3.10+
- C++14 compliant compiler:
  - Clang 6, 7, 8, 9
  - GCC 7, 8
- Message Passing Interface (MPI) library


## Quick Build
To build NAStJA, use the standard CMake procedure
```
mkdir build
cd build
cmake ..
make
```


## Run NAStJA
You can run NAStJA as follows
```
<PATH_TO_BUILD_DIR>/nastja -o <OUTPUT_FILE> -c <CONFIG_FILE>
```

You can finde example Config-Files in ``` test/data/ ```


## Config Keys
Please see the full list of Config Keys [here](./key.html).\n
You can use them by manipulating the ```<CONFIG_FILE>``` directly, or use the Config String Option.

### Config String Parameter
You can specifiy the ```<CONFIG_FILE>``` by using the option ```-C <CONFIG_STRING>```.\n
The ```<CONFIG_STRING>``` is organized as follows:
```
"{'CellsInSilico':{'defaultvolume':{'value': <Value>}}}"
```
for the Key ```CellsInSilico.defaultvolume.value```


## Output for Cellular Potts Model
Please use for ```Writers```
 - *CellInfo*: general Cell Infos, like center of mass, volume, surface, ...
 - *ParallelVtkImage*: for visualization with ParaView

It's also possible to use both writers simultaneously.

