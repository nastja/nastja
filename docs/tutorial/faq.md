\page faq FAQ

# Frequently asked Questions

## Q: What is NAStJA?
A: NAStJA stands for Neoteric Autonomous Stencil code for Jolly Algorithms and provides an easy way to enable massively parallel simulations for applications based on stencil algorithms.

## Q: What is Cells in Silico?
A: Cells in Silico, or in short CiS, is an application to simulate biological cells.

## Q: How to start NAStJA?
A: Make sure you've built NAStJA correctly, then run ```make test``` in the build-folder to test.\n
Now you can run NAStJA with: 
```
	./nastja -o <OUTPUT_FILE> -c <CONFIG_FILE> -C <CONFIG_STRING>
```
Hint: Output-File and Config-String are optional, for example run ```./nastja -c ../test/data/cells_01.json```

## Q: Whats is a "Config-String"?
A: A Config-String is supplement for the Config-File. The String has the same syntax as a normal .json file, however it's not a file rather a simple string.\n
For example: ``` "{'CellsInSilico':{'defaultvolume':{'value': <Value>}}}" ```