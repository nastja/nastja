\page introduction Introduction

[TOC]

## NAStJA
First of all, what is NAStJA?

NAStJA is an acronym for *Neoteric Autonomous Stencil code for Jolly Algorithms*.

That means, NAStJA is a framework providing an easy way to enable massively parallel simulations for a wide range of multi-physics applications based on stencil algorithms.
It supports the development of parallel strategies for high-performance computing.
Modern C++ and the usage of template meta programming achieve excellent performance without losing ﬂexibility and usability.

One example of such a multi-physics application is the simulation of biological cells. However, there's a variety of possible applications, other ones are Phase Field Crystal Simulations or StaRMAP (Staggered grid Radiation Moment Approximation),
a simple method to solve spherical harmonics moment systems, such as the time-dependent PN equations, of radiative transfer.

## Cells in Silico
Cells in Silico, short CiS, is one application of NAStJA.\n
Similar to [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) (GoL), CiS simulates cells via stencil algorithms, although a little bit more complex than Conway's game. Nevertheless, you can compare GoL to CiS. Both simulations use stencil algorithms to compute. In GoL it's quite easy: you see a specific pattern and then you replace it with another. For example, you have a text and you replace every "th" with a "ht". That's the basic idea of GoL. CiS does something similar, but in a more complex way.

So now you know the idea behind NAStJA, let's get started. Next Topic: \ref set_up
