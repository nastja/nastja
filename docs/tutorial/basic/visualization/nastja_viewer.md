\page nastja_viewer NAStJA Viewer

[TOC]
The Viewer is a small and fast viewer for 3D image data. It was developed specific for fast and efficient look at and analyze volumetric data, like our cells. As the name suggests, the NAStJA Viewer arises from the NAStJA framework.

The NAStJA Viewer documentation is very well done, so only the essential features will be treated.

## Set Up
Requirements: [FLTK](https://www.fltk.org/doc-1.3/intro.html)\n
Please make sure you have installed all requirements.

You can build the viewer as followed:
```
git https://gitlab.com/nastja/viewer.git
cd viewer/
mkdir build
cd build
cmake ..
make

```
you can start the viewer with ```./nastjaviewer <folder>```.


## Visualize the simulation
For a detailed manual, please see the [full documentation of NAStJA Viewer](https://gitlab.com/nastja/viewer)
You can start the viewer with
```
./nastjaviewer <PATH_TO_OUTPUTFOLDER>
```

Let's assume you have installed the NAStJA Viewer along your NAStJA-Framework. So your file path looks approximately so:
```
.
|
|____ nastja
| |____ build
|   |____ .nastja
|
|____ viewer
| |____ build
|   |____ .nastjaviewer
```

So tab into the build folder from NAStJA Viewer, so you can start the viewer.\n
If you're right (or you want to try to start the viewer from another source) let's start the viewer without an input file:
```
./nastjaviewer
```
You should now get an error which starts with: ```No filename specified.```.

Now let's head over and open some simulations. If you're still in the nastjaviewerfolder you can open your simulation as followed:
```
./nastjaviewer ./../../nastja/build/outdir
```
Please pay attention, that you don't need to specify one specific file, instead you give over a folder! The NAStJA-Viewer recognizes automatically

For those who want to start the viewer from other folders and also specify the outputfolder can use: ```PATH_TO_VIEWER/nastjaviewer OUTPUT_FOLDER```

You can navigate with the arrow keys and produce cuts through the whole simulation area. With `[+]` and `[-]` you can move through the timesteps.\n
With ```[V]``` you can switch between 1D and 3D presentation. In 3D ou can rotate the cuboid wit ```[Ctrl] + [ArrowKey]```.

Let's play a bit around and try all the features. A list of all features can be found in the task bar under.\n
Do you know how the cells transform exactly in this simulation? How many cells are there? To check if you're right, head over to the next chapter.\n

Hint: If you want to see the initial output, that means the very first frame with no simulationsteps, you need to activate "initialoutput". You can do this by simply adding the line ``` "initialoutput": true``` as followed:
```
"Filling": {
  "initialoutput": true,
  "cells": [
    ...
  ]
}
```


Oh, just one more thing: The NAStJA-Viewer is a very basic viewer. It's good enough for quick check your simulations, but for fancy pictures it is not appropiated.\n
So, let's head over to \ref paraview
