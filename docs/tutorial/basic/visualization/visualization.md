\page visualization Visualization

Theoretical there are unlimited ways how to visualize the data. If you know one and feel comfortable, go on, use your own way and you can skip this chapter. If not, then you are right here.
We will learn two different types of visualization, a fast and simple one and a fancy, but quiet complicated one. Which one you use is up to you.

- \subpage nastja_viewer
- \subpage paraview

Let's get started with the simpler one: \ref nastja_viewer
