\page paraview ParaView

[TOC]
ParaView is an application for interactive, scientific visualizations. It is very powerful, and you can do a lot of things with it, but this is quite complex.\n
So we want to just focus on the basics and how to visualize our cells.\n

## Installation
Let's start by installing ParaView. Please see the [official Download Page from ParaView](https://www.paraview.org/Wiki/ParaView:Build_And_Install).

## First Steps
Okay, now you've installed ParaView successfullylet's start it.\n
I'm using ```ParaView 5.7.0-RC2```, so maybe your version looks slightly different, but it shouldn't make a huge difference.

The first step is opening the files. Like the NAStJA Viewer ParaView also auto detect the files he needs and sums up files that belong together. You'll see what that means in a moment.\n
Let's open a file by clicking on ```File``` in the upper left. Then ```Open```. Or just use the shortcut  ```[Ctrl] + [O]```. A new window opens and there you can navigate and select files. Let's head over to our ```outdir```-folder.\n
There you should find a file named: ```output_cells-..vti```. Actually this is not a single file, rather it's a collection of files that belong together. To see what does that means, click the little ```+``` next to the file. A whole bunch of files should unfold, and all files end with ```<number>.vti```. Like the NAStJA Viewer ParaView automatically detect's that these files belong together and so we can just open all belonging files by opening ```output_cells-..vti``` instead of opening every single file by hand.

Okay let's open it and click apply. As you can see, you don't see anything. Okay let's head over so the ```Pipeline Browser``` and the to our file. On the left there's a little eye which is closed. That means, that this file is not shown. Click that eye, it should open, and oh, as you can see, you don't see anything again. Well, the outlining of a cuboid, but that doesn't count.

\image html ParaView_Image_1.png

## Visualization
To see the cells, we need the so-called "Threshold". For that, click once in the Pipeline Browser the output_cells file, then click on "Threshold". This is a little image which should be grayed out if you've selected nothing and should be greenish after you clicked the file. On the image below you can see where to find it. \n
The next step is to remove the blue cuboid. But before we do that, what is that blue thing? On the right lower corner you can see a scale, which is named cells, and the value 0.0 is blue, 1.0 grayish and 2.0 red. Now the blue thing is kinda of a cell. It's part of the simulation, but in this specific case, it's just the liquid or fertile soil where the cells grow. The cells we are interested in have the values 1.0 and 2.0. Maybe you remember the chapter about the first simulation (if not, click [here](first_simulation)). There we defined the CellIDs. We had the CellIDs 1 and to and also the kinda wired CellID 0. The values, which are represented with the color bar are a kind of reference to the CellIDs. So we can determine all the bluish cubes as CellID 0 - the space where the cells grows. The CellIDs 1 and 2 refers to the values 1.0 and 2.0. \n
But how can we erase the blue liquid? That's quiet simple. After you clicked "Treshold", a new entry in the Pipline Browser was generated. Choose that. It should named ```Threshold1``` and head down to properties. We are interested into the first to slides, named Minimum and Maximum. These slides determine the range of values which are represented. Go to Minimum and choose a value between zero and one. The blue cube should disappear and now you should see some strange thing in the colors gray and red.

\image html ParaView_image_2.png

Congratulation! Your first two cells.

The gray thing is you cell with ID 1 and the red with ID 2. You can change the perspective by drag and drop de blue window and also zoom in and out with the mouse wheel.\n
If you're just interested in the Cell 1 also the gray one, the head over to the slide named Maximum and decrease the value. Or if you are interested in the Cell 2 the raise the value of the minimum. The graphics always shown the values between Minimum and Maximum.

So play a bit around with your first to cells.

If you're interested in simulating something more exciting, go on and click \ref volume_surface.
