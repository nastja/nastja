\page solution_2 Solution:Extra

```
{
  "#Potential": {
    "description": "Tutorial Config for Potential"
  },
  "Application": "Cells",
  "Geometry": {
    "blocksize": [25, 10, 10],
    "blockcount": [1, 1, 1]
  },
  "Settings": {
    "timesteps": 25000
  },
  "Writers": {
    "ParallelVTK_Cells": {
      "writer": "ParallelVtkImage",
      "outputtype": "UInt32",
      "field": "cells",
      "steps": 250
    }
  },
  "Filling": {
    "initialoutput": true,
    "cells": [
      {
	"shape": "cube",
        "box":[[0, 0, 0], ["Nx", "Ny", "Nz"]],
        "value": 0,
        "celltype": 0
      },
      {
	"shape": "cube",
        "box":[[0, 2, 2], ["Nx", 7, 7]],
        "value": 1,
        "celltype": 1
      },
      {
        "shape": "cube",
        "box": [[19, 4, 4], [21, 6, 6]],
        "celltype": 2
      },
      {
        "shape": "cube",
        "box": [[5, 5, 4], [5, 5, 4]],
        "celltype": 3
      },
      {
        "shape": "cube",
        "box": [[5, 5, 6], [5, 5, 6]],
        "celltype": 4
      }
    ]
  },
  "CellsInSilico": {
    "liquid": 1,
    "potential": {
      "direction": [-1, 0, 0],
      "coupling": [0, 0, 150, 0, 0]
    },
    "adhesion": {
      "matrix": [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 25, 50],
        [0, 0, 25, 0, 50],
        [0, 0 ,50, 50, 0]
      ]
    },
    "temperature": 15,
    "volume": {
      "default": {
        "storage": "const",
        "value": 33
      },
      "lambda": {
        "storage": "const",
        "value": 5
      }
    },
    "surface": {
      "default": {
        "storage": "const",
        "value": 70
      },
      "lambda": {
        "storage": "const",
        "value": 10
      }
    },
    "checkerboard": "00",
    "energyfunctions": ["Volume00", "Surface00", "Potential00", "Adhesion00"]
  },
  "WriteActions": ["ParallelVTK_Cells"]
}
```
