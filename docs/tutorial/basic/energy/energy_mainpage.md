\page energy_mainpage Energy

NAStJA follows certain models. The nature. More precisely the energy minimization of processes.
The simples example is a ball. If you hold a ball, the ball has potential energy. If you let go the ball it drops down, because on the floor he has less potential energy. The ball will never fly up, or remain in peace, ecause the ball wants to minimize his energy.

NAStJA also tries to minimize the "energy". But which "energy" exactly?
You already know an example: volume and surface!
In the config file you've predefined a target value you want to achieve. This value is in our case "the lowest energy state" - or in case of the ball the floor. The energy calculated for the volume and surface are the difference between the actual value and the target value. This difference is processed in a next step to a energy like value. That means, for example, is the surface to small, but the volume to big, this two values don't cancel out, the rather add together. An action reducing the volume while increasing at the same time the surface is a good step towards the energy minimum. But only reducing the volume or only increasing the surface are also ok.
The hard part is when there's an action increasing the surface, but also increasing the volume. In such cases the pros and cons, or in energy-terms: the resulting energy, is weighed and a decission is made based on the resulting energy.

But one big problem is still remaining. For example, the ball rolls down a hill and stops in a valley. Now he has less potential energy than at the top of the hill. But how does the ball know that he has the least energy? Maybe there's another deeper valley?
Now, in orders to physics, the ball has bad luck, he'll never find out, because he can't suddenly start rolling up a hill. But in the nature, more exactly in biology, it's possible that things happen, that are not the best action related to the energy. Such actions against the law of energy minimization are called fluctuations.
These fluctuations are imitated by NAStJA. That's the reason why the cells don't rest after a period of time, although they should have the "optimal" energy state.

That's the basic idea of "energy".

- \subpage potential
- \subpage adhesion

Now let's go on and see which other energy-functions exist: \ref potential
