\page adhesion Adhesion

Adhesion is the tendency of cells to cling together. \n
A real world example are your bones and muscles. Your muscle cells cling together, and you bone cells too. But they don't mix up, nevertheless it's hard to seperate them. \n
This phenomenon can be realized in NAStJA whitch the adhesion-energy. As a parameter you hand over a adhesion-matrix, which determines the individual adhesion forces between the cells. In general the adhesion Matrix is build up as followed:

<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="400">
<tr>
    <td height="19" width="20%" align="center"> </td>
    <td height="19" width="20%" align="center"> <b> Liquid </b> </td>
    <td height="19" width="20%" align="center"> <b> CellType 1 </b> </td>
    <td height="19" width="20%" align="center"> <b> CellType 2 </b> </td>
<td height="16" width="20%" align="center"> <b> ⋯ </b> </td>
</tr>
<tr>
    <td height="19" width="20%" align="center"> <b> Liquid </b> </td>
    <td height="16" width="20%" align="center">a[0,0]</td>
    <td height="16" width="20%" align="center">a[0,1]</td>
    <td height="16" width="20%" align="center">a[0,2]</td>
    <td height="16" width="20%" align="center"> ⋯ </td>
</tr>
<tr>
    <td height="19" width="20%" align="center"> <b> CellType1</b> </td>
    <td height="16" width="20%" align="center">a[1,0]</td>
    <td height="16" width="20%" align="center">a[1,1]</td>
    <td height="16" width="20%" align="center">a[1,2]</td>
    <td height="16" width="20%" align="center"> ⋯ </td>
</tr>
<tr>
    <td height="19" width="20%" align="center"> <b> CellType1</b> </td>
    <td height="16" width="20%" align="center">a[2,0]</td>
    <td height="16" width="20%" align="center">a[2,1]</td>
    <td height="16" width="20%" align="center">a[2,2]</td>
    <td height="16" width="20%" align="center"> ⋯ </td>
</tr>
<tr>
    <td height="19" width="20%" align="center"> <b> CellType1</b> </td>
    <td height="16" width="20%" align="center">a[2,0]</td>
    <td height="16" width="20%" align="center">a[2,1]</td>
    <td height="16" width="20%" align="center">a[2,2]</td>
    <td height="16" width="20%" align="center"> ⋯ </td>
</tr>
<tr>
    <td height="19" width="20%" align="center"> <b> ⋮ </b> </td>
    <td height="16" width="20%" align="center"> ⋮ </td>
    <td height="16" width="20%" align="center"> ⋮ </td>
    <td height="16" width="20%" align="center"> ⋮ </td>
    <td height="16" width="20%" align="center"> ⋱ </td>
</tr>
</table>

The ```a[i,j]``` are integers and they determine how strong the adhesion between Celltype _i_ and Celltype _j_ is. The higher the value, the more the the cells "glue" together.

Let's see an example code. Again, first play with it and then read on, how to exactly modify it.
```
{
  "#Potential": {
    "description": "Tutorial Config for Potential"
  },
  "Application": "Cells",
  "Geometry": {
    "blocksize": [25, 25, 25],
    "blockcount": [1, 1, 1]
  },
  "Settings": {
    "timesteps": 25000
  },
  "Writers": {
    "ParallelVTK_Cells": {
      "writer": "ParallelVtkImage",
      "outputtype": "UInt32",
      "field": "cells",
      "steps": 1000
    }
  },
  "Filling": {
    "initialoutput": true,
    "cells": [
      {
        "shape": "cube",
        "pattern": "voronoi",
        "count": 100,
        "box": [[5, 5, 5],[20, 20, 20]],
        "celltype" : [0, 50, 50]
      }
    ]
  },
  "CellsInSilico": {
    "adhesion": {
      "matrix": [
        [0, 0, 0],
        [0, 50, 5],
        [0, 5, 50]
      ]
    },
    "temperature": 15,
    "volume": {
      "default": {
        "storage": "const",
        "value": 33
      },
      "lambda": {
        "storage": "const",
        "value": 5
      }
    },
    "surface": {
      "default": {
        "storage": "const",
        "value": 70
      },
      "lambda": {
        "storage": "const",
        "value": 10
      }
    },
    "checkerboard": "00",
    "energyfunctions": ["Volume00", "Surface00", "Adhesion00"]
  },
  "WriteActions": ["ParallelVTK_Cells"]
}
```

Like in the example of the potential you need to modify two parts of the code: First you need to define the adhesion matrix, next you need to activet the adhesion-energy.

Let's start with the matrix:
```
"adhesion": {
      "matrix": <<adhesion-matrix>>
    }
```
The ```<<adhesion-matrix>>``` is a matrix like structure, as obove. Please consider that the adhesion to the liquid is always zero, if you modify it, it will set to the default value 0. If your liquid hasn't cellID 0, then you need to choose higher CellIDs and also use a larger adhesion matrix.

We need to add the adhesion-energy to the energyfunctions, which is shown here:
```
"energyfunctions": ["Volume00", "Surface00", "Adhesion00"]
```

Now you can start your own simulations, play around and also add new cells! Or combine it with a potential! \n

<div style="background-color: #aedcd4;">
  <div style="background-color: #20b9a7;">
    Task: Adhesion and Potential
  </div>
  In the previous chapters you've get known to adhesion and potential.\n
  &nbsp;\n
  Task: Place two cells _i_ and _j_ next to each other with an adhesion coeffizient of ```a[i,j] = a[j,i] = 25```. Place a third cell _k_ above with adhesion coeffizints of ```a[i,k]=a[j,k]=5```. Now let's create a potential, which only affects cell _k_ and pulls it toward the to cells sticking together. Repeat the experiment with adhesion coeffizints of ```a[i,k]=a[j,k]=50```. (\subpage solution_1)\n
  &nbsp;\n
  Extra: Maybe you've recognized, that the cell from above doesn't move through the two cells, rather moving around. You can solve this, by simply reducing the free area by filling up with solids. Repeat the experiment and watch the cell from the top pushing the other two down, or squeezing itself through them. (\subpage solution_2)
</div>

For further help and information please see the official documentation [here](https://nastja.gitlab.io/nastja/docs/index.html).
