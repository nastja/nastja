\page potential The Potential

The potential is similar to a force pushing or pulling an object. For example a ball is pulled down by gravity.

In case of the cells the potential refers rather to a nutrient field. But you can also simulate global potentials, for example you can simulate gravity on cells.

For implementing a potential we need to modify to parts of the config. Let's show an example config, and then discuss what that config does. Feel free to visualize ist and play a bit around:
```
{
  "#Potential": {
    "description": "Tutorial Config for example Potential"
  },
  "Application": "Cells",
  "Geometry": {
    "blocksize": [50, 20, 35],
    "blockcount": [1, 1, 1]
  },
  "Settings": {
    "timesteps": 20000
  },
  "Writers": {
    "ParallelVTK_Cells": {
      "writer": "ParallelVtkImage",
      "outputtype": "UInt32",
      "field": "cells",
      "steps": 200
    }
  },
  "Filling": {
    "initialoutput": true,
    "cells": [
      {
        "shape": "cube",
        "box": [[10,10,10], [10, 10, 10]],
        "celltype": 1
      },
      {
        "shape": "cube",
        "box": [[10,10,25], [10, 10, 25]],
        "celltype": 2
      }
    ]
  },
  "CellsInSilico": {
    "potential": {
      "direction": [1, 0, 0],
      "coupling": [0, 150, 50]
    },
    "temperature": 15,
    "volume": {
      "default": {
        "storage": "const",
        "value": 100
      },
      "lambda": {
        "storage": "const",
        "value": 5
      }
    },
    "surface": {
      "default": {
        "storage": "const",
        "value": 160
      },
      "lambda": {
        "storage": "const",
        "value": 10
      }
    },
    "checkerboard": "00",
    "energyfunctions": ["Volume00", "Surface00", "Potential00"]
  },
  "WriteActions": ["ParallelVTK_Cells"]
}
```

The first part we need to modify, is ```CellsInSilicio```. We need to define potential, before we could have one. You can just add a codeblock like:
```
"potential": {
      "direction": <<vec>>,
      "coupling": <<array>>
    }
```
with ```<<vec>>``` a 3d vector showing into the direction of the potential. Please note, the vector is normed on legth 1. So don't use the vector ```[0, 0, 0]``` and also the vectors ```[0.001, 0, 0]``` and  ```[100, 0, 0]``` describe the same vector.

The ```<<array>>``` defines the force on the cells by the potential. The higher the value the faster the cells move. The first element of the array belongs to the cells with ID 0, the second for Cells with ID 1 and so on. Please consider that, Cells with ID0 are "liquid" cells. That means their potential and other energy functions are set to a default value. That's why we need cell typs greater than 0. If our liquid is for example 10, then we need celltypes with a value greater 10.
If you want that a cell isn't affected by the potential use ```0```. And if you want that the cell is repelled by the potential use a negative value.

But it's not enough to define a potential. We need to add the potential-energy to the energyfunctions, which is shown here:
```
"energyfunctions": ["Volume00", "Surface00", "Potential00"]
```

Now you can start your own simulations, play around and also add new cells!


Let's go on and get to know an other energy: \ref adhesion
