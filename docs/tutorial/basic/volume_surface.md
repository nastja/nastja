\page volume_surface Volume and Surface

## General
As mentioned before, you can compare NAStJA to a set of LEGO. But to have fun with LEGO we need different kinds of bricks. NAStJA has something similar to bricks: parameters or attributes.\n
In this chapter we want to understand two very basic parameters: the volume and the surface.

# Volume
Volume is the quantity of three-dimensional space enclosed by a closed surface. In our cell simulation the volume is the number of cubes, or also called voxels of which the cell is made. Or in LEGO-Language the number of bricks needed. \n
So in a simple term, the volume is a measurement of how big the cell is.

What does that visually mean?\n
Let's open the config file with a text editor of your choice. You can find the file under ```nastja/test/data/tutorial.json```. Head over to CellsInSilicio where you find this:
```
"CellsInSilico": {
    "temperature": 15,
    "volume": {
      "default": {
        "storage": "const",
        "value": 100
      },
      "lambda": {
        "storage": "const",
        "value": 5
      }
    },
    "surface": {
      "default": {
        "storage": "const",
        "value": 160
      },
      "lambda": {
        "storage": "const",
        "value": 10
      }
    }
```

This is a simple .json file. To manipulate the value "volume", we only look at the entry starting with "volume". For now we don't need the lambda volume, so only look at:
```
"volume": {
      "default": {
        "value": 100
      }
```
That means that the current default value for our cells is \f$100\f$. If we want larger cells - cells with more volume - we replace the \f$100\f$ with a number we want. For example \f$250\f$\n
Save the file und run NAStJA. For example when you're currently in the build folder:
```
./nastja -c ./../test/data/tutorial.json
```
Your current outdir directory should be overwritten. Let's open ParaView and open the .vti files and look at the cells. They're slightly bigger, aren't they?\n
To be honest, the difference isn't huge, maybe you even can't see it! To control if the new image is correct, head over to the .csv files and open them. There you can investigate the volume of your current cells.

For reference let's start a third simulation: Change the default volume to \f$10000\f$ and start the simulation again. Maybe you want to save this simulation in another file, so use the command:
```
./nastja -c ./../test/data/tutorial.json -o ./huge_cells_simulation
```
or whatever you want to name that simulation, a handy and descriptive name is advantageous and good practice for later.\n
So maybe use the name ```cis_vol_10000``` for CellsInSilicio, and volume = \f$10000\f$, or something similar, that's up to you.

Now head over again to ParaView and just open the new generated .vti files. Now the cells are larger. You can see the process of growth by stepping through the single timesteps.\n
But have these cells a volume of \f$10000\f$? Let's check that by looking it up in the .csv files.

So now, why does these cells don't have the volume we want?\n
This can have different reasons:
	- Maybe the cells are too large for the area where they can grow (fertile soil)?
	- So first at all, we see that these cells grow, so maybe they aren't mature?
	- We have also a surface, maybe the surface limits the growth?

And yes, all three of these are true. But to understand what each parameter does we want to examine each parameter one by one.


## Area to grow
Let's start with examining the area where the cells growth / CellID 0 / fertile soil.\n
Open our tutorial.json and in "Geometry" is the parameter we are searching for:
```
"Geometry": {
    "blocksize": [20, 20, 20],
    "blockcount": [1, 1, 1]
  }
```
We have two different entries. "Blocksize" and "Blockcount". To understand what each of them means, we use LEGO again. So the blocksize is in general how big your bricks are, for example in LEGO, there are \f$1 \times 1\f$ bricks and \f$2 \times 2\f$ bricks or the classic \f$ 2 \times 4\f$ bricks, so the blocksize is how big a singe brick is. Meanwhile, "Blockcount" is how many bricks you use.\n
So for example you can build a Rubik's Cube with LEGO. For that you need \f$26\f$ (+ the one in the center) \f$ 2 \times 2\f$ LEGO bricks (blocksize) and you arrange them in a \f$3 \times 3 \times 3\f$ cube (blockcount).

For now it's not important why there is a difference between blocksize and blockcount. For now it's enough if we play around with the Blocksize.

Currently, we have an \f$20\times 20\times 20\f$ are, that means that the volume of the area is \f$20\cdot 20\cdot 20 = 8000\f$. By that we define one volume element as a \f$1\times 1\times 1\f$ area.\n
Let's do some math:
<div style="background-color: #f3f3f3; padding: 10px;">
	Current Volume of Area: \f$20\cdot20\cdot20 = 8000\f$

	Current Volume of Cell: \f$10000\f$
	Number of Cells: \f$2\f$
	Total Volume of all Cells: \f$2\cdot10000=20000\f$

	Needed Volume of Area: \f$20000\f$
</div>

So now we know that the two cells need at least a volume of \f$20000\f$ to get full size. Our current area has only a volume of \f$8000\f$. So we need to chose a bigger Blocksize.\n
One option is simply using ```"blocksize": [100, 100, 100]"```. That's enough. But that's also a very big area and the bigger the area the longer the computation time, so we want a smaller block size.\n
If we use a cube, we can get the edge length by calculate the third root of the volume, thats round about \f$141.42\f$. The problem, we can't use decimals in blocksize, we need whole numbers. You can't build anything with a \f$1.5\times1.5\f$ LEGO brick. So because we want' that our Cells fit into the area we need at least. But then we run in another problem. The cell touches everywhere the border, that's a big problem, but for now we just notice, that we don't want that a cell touches more than two borders.

## Surface
For determining the volume, we proceed analogous to the volume. So please specify:
```
"surface": {
    "default": {
    "value": 160
  }
}
```
Here we need to pay attention again. We have to choose the surface suitable to the volume. Mathematically the surface can't be smaller than the ratio between volume and surface of a sphere. But NAStJA uses cubes for calculating surface and volume. Volume is the number of cubes, and Surface is the number of sides touching not a cube of the same cell. For that a cube has the perfect ratio between surface and volume. For a given volume we can calculate the optimal surface by the following formula:\n
<div style="background-color:#dfe5f1; text-align: center;"> \f$S = 6 \cdot {\sqrt[3]{V}}^2\f$ </div>

In general you can use a formula like this, with \f$r\f$ any real number as scaling factor:
<div style="background-color:#ffd4d4; text-align: center; color: red;"> \f$S = 6 \cdot s \cdot {\sqrt[3]{V}}^2\f$ </div>

In general you use for \f$s\f$ a value between \f$1.2\f$ and \f$2\f$. That gives a good match between surface and volume, so you get a nice elliptical cell.

Now let's move on how you can place cells. Next \ref initial_filling_mainpage
