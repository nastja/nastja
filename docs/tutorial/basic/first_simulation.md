\page first_simulation The first Simulation

Now you have installed NAStJA successfully, let's start with a first CiS simulation.

Simply type in the build folder:
```
./nastja -c ./../test/data/tutorial.json
```
For now don't mind what's that file or NAStJA does exactly, just notice `tutorials.json` is like a blueprint and NAStJA puts it all together. It's like a set of LEGO. The `.json` file is the manual and you are NAStJA, assembling the LEGO-Set.

You should now find a folder called `outdir` with several files and folders in it. This is the output-directory produced by NAStJA.\n
Maybe you want to specify your output directory, so simply use instead
```
./nastja -c ./../test/data/tutorial.json -o <OUTPUT_DIR>
```

Congratulations, you've run your first simulation. But what exactly did you run?\n
Tap into the output directory. You'll find a folder "timing" and some folders starting with zeros, for now you can just ignore them. the ```.vti``` and ```.csv``` files are, for now, the essential output. Let's start with the ```.csv```.

## .csv
csv stands for comma-seperated values and this is a standard, wide range data format, mostly used for tables. Maybe you know them from Excel.\n
Let's open one of those files. It doesn't matter if you open ```output_cells-000000.csv``` or ```output_cells-000004.csv```, just open any of them. The numbers stand for which timestep they are from. So the file with 000000 is the oldest one and 000001 is one timestep later.

The first line is the headline and describes which data was collected, like in a normal table. There are three Cell ID. Let's start discussing Cell ID 1.\n
Cell ID 1 is the ID for Cell one. The next three numbers can vary a bit, but they all should be around 10. These three numbers are the center of the cell in kartesian coordinates.

The center coordinates are followed by the volume. The volume can reach from small values, up to around 100, depending on which file you've chosen. The volume is a measure for the size of the cell.

The next entry is the surface. The surface also reaches from quiet small numbers up to around 160. It is a measure for the surface area of the cell.

Then theres the Type or cell type. Every cell has a specific cell type. If you compare it with the last row, or the cell with ID 2, both have the same type. There are several different cell types with different characteristics and features.

The next three entries stand for signals. Signals could be something like nutrients, drugs, or something like this, in general just some specific values which affect the growth of the cell.

The last entry is the age. This is obviously the age of the cell. It's a measure of how long the cells live since they first appeared.

For Cell ID 2 you can go along analogous. Theoretical you can have unlimited Cell IDs and therefore unlimited cells. In this simulation the cells are limited to two.

We skipped Cell ID 0. This "Cell" has quiet strange values, that's therefore that this isn't a cell. Cell ID 0 is the space where the cells grow. It's the space or fertile soil where the cells grow.

Don't worry if you don't understand this at all, for now it's enough to know, that there's a output format, which describes the crucial parameters of the simulation and the cells.

Let's head over to the .vti files.

## .vti
.vti is a VTK image data type. VTK stands for _Visualization Toolkit_. This data file is for humans not so informative than .csv, but ParaView and other visualization tools can use this data to visualize the cells.

One big disadvantage of .csv is that you don't know how the cells look like. You know the center and the volume, but you have no more information. Is it a cube? A sphere? Or something completly different? To figure this out, you can use .vti files and visualize your data.

In the next chapter you will learn how to visualize the cells, \ref visualization.
