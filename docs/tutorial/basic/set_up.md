\page set_up Set Up

First at all you need to Set Up NAStJA and all it's dependencys.

Please pay attention, that NAStJA supports only Linux, macOS and UNIX systems.

To start it's enough to install. You can easily follow the linked guides:
 - [CMake 3.10+](https://cmake.org/install/)
 - C++14 compliant compilter:
    - [Clang 6, 7, 8, 9](https://clang.llvm.org/get_started.html)
    - [GCC 7, 8](https://gcc.gnu.org/install/)
 - [Message Passing Interface (MPI) library](https://www.open-mpi.org/faq/?category=building)

Probably you can install these things just with `apt`, `pacman`, `brew`, ...

Now let's start installing NAStJA:
 1. Check out the NAStJA project:
    - Change directory to where you want the NAStJA directory placed
    - ```git clone https://gitlab.com/nastja/nastja.git```
 2. Build NAStJA
    - ```cd nastja```
    - ```mkdir build```
    - ```cd build```
    - ```cmake ..```
    - ```make``` (please note, you can also use ```make -j <NUM_PROC>```, with ```<NUM_PROC>``` is the number of processor + 1, to speed things up)

Please note: RAM > 4GB is recommended, otherwise if make doesn't succeed, try using Swap.

If you run in problems withe the ```make``` command, the module "ninja" is a workaround:
 1. Make sure you have installed "ninja"
 2. Build NAStJA
    - ```cd nastja```
    - ```mkdir build```
    - ```cd build```
    - ```cmake .. -G "Ninja"```
    - ```ninja -j 1```

Let's check if everything is set up correctly:
 1. Make sure you are in the build folder
 2. Run: ```make test```
	This may take a while, but it should run with 100% tests passed, if not, retry, or visit FAQ.

Let's go on and run the first simulation: \ref first_simulation
