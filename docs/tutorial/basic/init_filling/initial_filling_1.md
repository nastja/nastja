\page initial_filling_1 Initial Filling - Part 1: Basic Shapes

Now we've already seen how to manipulate single cells, but how can we place or spawn cells? Or other things?

There are several options placing structures in different geometric shapes:
 -# Box
 -# Sphere
 -# Cylinder
 -# Cone

But first at all, at our current simulation we have two cells. (No solids or other stuff)\n
Now, how can we add or reduce cells? Simply by adding or reducing code blocks of form:
```
{
    "shape": "cube",
    "box": [[5, 5, 5], [15, 15, 15]],
    "celltype": 1
}
```

The celltype defines which type of cell is placed. But more about that later.

For now just consider the different geometric shapes:

## Box
The Box is the simples shape, it's a cube or in more general a cuboid. It has the form:
```
"shape": "cube",
"box": <<BoundinBox>>,
"celltype": <<int>>
```
The BoundingBox is the blueprint for a cuboid, and as an input it gets two corners: The coordinates of the first corner and the coordinatesof the opposite corner along the diagonal. \n
A typical BoundingBox is for example: ```[[1, 2, 3], [7, 8, 9]]```. In this connection ```[1, 2, 3]``` are the coordinates of the first corner and ```[7, 8, 9]``` of the opposite corner.
Please pay attention, that the BoundinBox ```[[7, 8, 9], [1, 2, 3]]``` will give the same result.


## Sphere
To define a sphere we need two parameters: The center of the spehre and his radius. It's definied as followed:
```
"shape": "sphere",
"center": <<vector>>,
"radius": <<int>>,
"celltype": <<int>>
```
According to this a valid input for the center is for example ```[1, 2, 3]```. A vector in this context is just the coordinates of a specific point, like the corners of the cube. For the radius you can just use a positive integer.


## Cylinder
A more complex shape is the cylinder. It's defined as followed:
```
"shape": "cylinder",
"base": <<vector>>,
"top": <<vector>>,
"radius": <<int>>,
"celltype": <<int>>
```
Vector and int is defined as above. With that syntax you can place a cylinder in the three dimensional room as you wish, horizontal, diagonal, ...\n
If you just want to expand your cylinder into the Z-direction then you can replace ```top``` with ```"length": <<int>>```. Then you get a cylinder which is perpendicular to the X-Y-plane.\n
So the method with two vectors is more powerfull but also a bit more complex.


## Cone
The cone is defined like the cylinder, so you can define it with:
```
"shape": "cone",
"base": <<vector>>,
"top": <<vector>>,
"radius": <<int>>,
"celltype": <<int>>
```
And again you can replace ```top``` with ```length```. Cones have the same orientation like cylinders.

## Appendix

More complex fillings will be discussed soon. It covers inter alia: periodic boundary, checkerboard-filling and random fillings.

But for now you can run your own simulations, place geometric shapes and also manipulate the area to grow on. That's the basic stuff needed to dive into more complicated topics. For example learning the difference between solids and cells.\n

Let's head over to the next chapter, and learn how to place cells or solids: \ref initial_filling_2.












