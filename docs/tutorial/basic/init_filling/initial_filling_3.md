\page initial_filling_3 Initial Filling - Part 3: More Complex Fillings

In the last part you've constructed a bone. But for a simulation one big part is missing. The cells. In this chapter you'll learn how to place cells automatically and fill whole areas with cells.

## Voronoi
Voronoi is a partition of a plane into regions close to each given set of points. This partition can be expanded into the three dimensional space.\n
The idea is that you expand from each point a circle (2D) or sphere (3D) until it touches the expanded circle/sphere of another point. A very good visualization can be found on wikipedia: [voronoi growth](https://en.wikipedia.org/wiki/Voronoi_diagram#/media/File:Voronoi_growth_euclidean.gif).

There are different ways how to implement the voronoi partition. Pleas note: voronoi is just a starting configuration for the cells. It does not affect the growht of the cells. Theoretically you could place all the cells by hand, but that's laborious.

### Voronoi-Cube
If you want to fill a cube, or the complete area with cells, this option is right.\n
First we look at the code, then we'll discuss it.
```
{
  "shape": "cube",
  "pattern": "voronoi",
  "count": 20,
  "size": 5,
  "box": [[0, 0, 0],[10, 20, 20]],
  "celltype" : 1
}
```
The first line ```"shape": "cube"``` is similar to the "normal" cells. We want a cuboid, so we need cube as the chape. With ```"box": [[0, 0, 0],[10, 20, 20]]``` we define the area, or exact, the bottom left corner and the upper right, diagonal corner, again similar to the normal cells. ```"celltype": 1``` again, refers to the cell type, how we learned it with the normal cells.\n
New is the pattern keyword: ```"pattern": "voronoi"```. This line say, that we want to fill the cuboid in a voronoi-style. Or rather we disassemble the cuboid into a voronoi partition.\n
```"count": 20``` determines the number of cells within the cuboid. Alternatively you can say, the voronoi partition consists of twenty partitions.\n
```"size": 5``` determines the size of the cells. The cells are spherical with radius 5 - if possible.

The best way to understand this is by playing around with these. Try out what happens, if you set size to 1, or count to 2...\n

Hint: "size" is an optional parameter. You can miss out this line. Try out, what happens?\n
Hint: If you want to fill the complete arear use for the box: ```"box": [[0, 0, 0],["Nx", "Ny", "Nz"]],```.

### Voronoi-Sphere
You can also create a sphere instead of a cube.\n
The sphere is simply constructed by simulating a voronoi cube and the cutting out a sphere.
```
{
  "shape": "sphere",
  "pattern" : "voronoi",
  "count" : 15,
  "center": [10, 10, 10],
  "radius": 10,
  "box": [[0, 0, 0],[20, 20, 20]],
  "celltype" : 1
}
```
"center" and "radius" refer to the sphere, which is cutted out. Please note, that it's possible that the sphere is not (fully) in the cuboid made by ```"box": [[0, 0, 0],[20, 20, 20]]```. In this case, the cuboid is expanded over the hole area and then the sphere is cuted out.\n
Try it! What happens if you coose as box ```"box": [[0, 0, 0],[0, 0, 0]]```.

Can you figure out how you can construct a cone instead of a sphere? Try it on yourself. If you need help, just consider the general structure of a voronoi partition: "normal" cell + pattern/count/box.

Solution:
```
{
        "shape": "ccone",
        "pattern" : "voronoi",
	"base": [25, 25, 0],
        "top": [25, 25, 50],
	"radius": 10,
        "count" : 500,
	"size": 4,
        "box": [[0, 0, 0],[50, 50, 50]],
        "celltype" : 1
}
```
<div style="background-color: #aedcd4;">
  <div style="background-color: #20b9a7;">
    Task: Initial Filling - Bone with cells
  </div>
  In the previous chapter you've constructed a bone. Now we want to add cells. We want to simulate a thigh. A thigh is simply a bone and cells in form of a cylinder are around the bone.\n
  &nbsp;\n
  Task: Place a bone and make sure that it's a solid. Create a cylinder with voronoi filling around it. Make sure, that the cells don't replace the bone.\n
  &nbsp;\n
  Hint: Cells placed later will replace cells which were placed earlier!
</div>

Congratulations. You've now completed how to place cells and solids or fill hole areas.

Let's move on to the energy functions: \ref energy_mainpage.












