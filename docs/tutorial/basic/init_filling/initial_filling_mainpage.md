\page initial_filling_mainpage Initial Filling

Now we have learned how to visualize our simulations and what "volume" and "surface" is. But we have no real examples on how cells interact. \n
In this chapter we learn how to manipulate the initial filling - or in other words, how to place cells and solids.

 - \subpage initial_filling_1
 - \subpage initial_filling_2
 - \subpage initial_filling_3

Let's start by getting to know the basic geometrical shapes: \ref initial_filling_1.
