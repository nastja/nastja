\page initial_filling_2 Initial Filling - Part 2: CellIDs, Liquid, and Solids

In the last chapter we have got to know the geometrical shapes. They were code blocks of the form:
```
"shape": <<shape>>,
"parameters": <<parameters>>
"celltype": <<int>>
```

You can place cells simply by adding such code blocks. By manipulating the parameter "celltype" you can change the celltype of the cell.

## Solids
All cells with a DellID smaller than the ID of the liquid are solids. The problem now is, that the CellID of liquid is 0 and negative CellIDs are not allowed.\n
The fact that CellID 0 refers to liquid is not quite true. The standard value for liquid is 0, but you can change that.

<div style="background-color: #f3f3f3">
  If you're not familiar with the .json Syntax, please see a tutorial, like [https://www.json.org/json-en.html](https://www.json.org/json-en.html)
</div>

Liquid is an argument of CellsInSilicio, in short CellsInSilicio::liquid. That means we need to add ``` "liquids" : 1 ``` into the code block of CellsInSilicio. Then the liquid has CellID 1, and we can use CellId 0 for solids.\n
It looks like this:
```
...
"CellsInSilicio":{
  ...
  "liquid": <<int>>,
  ...
},
...
```

Now we can head over to Filling::cells. In this place we have defined our cells, and now we can add a solid. Add a shape, for example a box in the center. \n
Look out where to define the CellID. Can you find it?

The short answer is no, theres missing the definition of the CellID. If you just want to place cells, that's not a problem. NAStJA assigns automatically CellIDs in the order the cells are defined, starting at the liquid CellID. \n
Try on yourself if you find the command to define the CellID. _Hint: it's not cellid._\n
Look in the documentation. If you can't find it, look in the example files in```nastja/test/```. The files starting with cells refer to CiS. _Hint: maybe cells_07 is helpful_?

The Answer is ```"value" : <<int>>```.\n
If the int is smaller than the liquid it's a solid. If it's greater, then it's a cell (with your predefined CellID). If it's equal to the liquid, it's just the liquid.

With that in mind you can construct nearly everything you want. Please note, that the cells you place later overwrite the earlier ones.

Can you imagine what this is:
```
"Filling": {
  "cells": [
    {
      "shape": "cylinder",
      "value": 0,
      "base": [25, 32, 25],
      "top": [25, 42, 25],
      "radius": 2,
      "celltype": 0
    },
    {
      "shape": "sphere",
      "center": [25, 25, 25],
      "radius": 10,
      "celltype": 2
    },
    {
      "shape": "sphere",
      "value": 1,
      "center": [18, 25, 20],
      "radius": 7,
      "celltype": 1
    }
  ]
},
  "CellsInSilico": {
    "liquid" : 1
}
```

Hint: You can run a simulation with this setup and then use ParaView to visualize. The problem is, now the liquid is not 0 but 1. Nevertheless you can visualize the simulation, by adjusting the minimum to 1 and maximum to 1.1 and the activate "Invert". That does the thing.

With a little bit of fantasy (okay, a lot), this is a bitten apple.\n
First the stalk (assumed as dead cells, or solid) was placed, then the apple was placed above. The part of the stalk which reached in the apple was replaced by apple cells. Then "the bite was taken". A sphere made out of the liquid was placed, so that the intersection between apple and liquid sphere was overwritten to liquid.

With this method you can construct almost everything you can imagine. Simply by placing and overwriting.

For example a spherical shell can be constructed by placing a spherical cell, and after that placing a sphere made out of liquid into the middle.

Another feature is that you can place an object somewhere and somewhere else another object, but both with the same value. Then these two objects have the same CellID, that means the belong together. For cells this may be not so useful, but for other simulations for sure.

<div style="background-color: #aedcd4;">
  <div style="background-color: #20b9a7;">
    Task: Initial Filling - Bone
  </div>
  Now you know how to place solids and cells. \n
  You can use this knowledge to place a bone. A bone is simplified a non destructible solid by cells. The shape of the bone is very similar to a dumbbell. \n
  A bone is made up of thwo Spheres connected by a cylinder. A more complex bone is if you use insted of one sphere on the two sides of the cylinder two overlapping spheres.\n
  &nbsp;\n
  Task: Place a bone and make sure that it's a solid.
</div>

Let's head over to the next chapter, and learn how to place cells automatically: \ref initial_filling_3.












