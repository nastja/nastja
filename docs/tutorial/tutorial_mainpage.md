\page tutorial Tutorial

[TOC]

## The NAStJA Framework

Neoteric Autonomous Stencil code for Jolly Algorithms

The NAStJA framework provides an easy way to enable massively parallel simulations for a wide range of multi-physics applications based on stencil algorithms.
It supports the development of parallel strategies for high-performance computing.\n
Modern C++ and the usage of template meta programming achieve excellent performance without losing ﬂexibility and usability.

## Purpose
The NAStJA Tutorial is an introductory and comprehensive tutorial for beginners.\n
It is broken up into beginner, intermediate, and advanced sections, as well as sections covering specific topics.\n
Although it's named NAStJA Tutorial, it mostly features Cells in Silicio. Basic commands of NAStJA are featured, but mainly the tutorial aims to teach Cells in Silico.

For a Quick Guide please check [Quick Guide to NAStJA](./quick_guide.html).\n
For further information please check the [full documentation](./index.html).\n
If you just want to discover the possibilities of NAStJA, click [here](./introduction.html) for some amazing pictures and a short introduction.

## Let's get started
To get a quick peak about this tutorial, here's a quick overview.

 - \subpage introduction
 - \subpage set_up
 - \subpage first_simulation
 - \subpage visualization
   - \ref nastja_viewer
   - \ref paraview
 - \subpage volume_surface
 - \subpage initial_filling_mainpage
   - \ref initial_filling_1
   - \ref initial_filling_2
   - \ref initial_filling_3
 - \subpage energy_mainpage
   - \ref potential
   - \ref adhesion

It's not absolutely necessarily to follow this order, but it's recommended.

