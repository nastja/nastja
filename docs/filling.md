\page filling Filling
[TOC]

Filling is an array of fill objects. The following fill objects are available:

\todo add objects

# BoundingBox
The BoundingBox is a box
```
"box"   : [[x_start,y_start,z_start],[x_end,y_end,z_end]]
```
`[x_start, y_start, z_start]` is the start, note the domain starts with `[0, 0, 0]`.
And `[x_end, y_end, z_end]` is the end, the last coordinate that is included in the BoundingBox. The the domain ends with `[Nx-1, Ny-1, Nz-1]`.
\note This describes the config key BoundingBox, internally the BoundingBox uses `start <= inside < end`. A convertion from config file to internaly is done by `.extend(0,1)` this is automatically applied when getValue<BoundingBox<T>> is used.

# Blood vessel
Sets a blood vessel in a box.

This blood vessel develop itself to hypoxic area. Those area are defining by a list of target.

First, build vessel from a defined point to different target. It split several time until there is one vessel for each target.
It develops itself in several parts with decreasing length.
Once the target is closer than the diffusion length, the corresponding vessel stop to grow.

Secondly, build capillaries on the top of every vessel.
It is built with as much part as given. the length of each part is calculated depending on its diffusion length.
Each time the capillary split randomly according to its own rate
Once it has split as much as its own weight, it just keep growing but never split again.

Finally, calculate the area of every part starting from the top which get a minimum area and the other have an area depending on the part on top of it.

 - Celltype is specified by "typ"
 - The area of the smallest part of the vessel is defined by "area"
 - The first point of the vessel is defined by "root"
 - The list of all the target is defined by "target_list"
 - The number split of every target is defined by "target_list_weight"
 - The probability to split for every capillary is defined by "split_rate_capillaries"
 - The diffusion length for every target is defined by "diffusion"
 - The number of part for every capillary is defined by "num_part_capillaries"
 - The length of every part of the vessel is defined by "length_vessel"
    - First is the maximum length
    - Second is the minimum length
    - Last is the decrement
 - The probability for a vessel to split is defined by "split"

```
{
  "type"                  : "vessel",
  "box"                   : [[5,5,5],[895,895,895]],
  "typ"                   : 2,
  "area"                  : 5,
  "root"                  : [400,50,400],
  "target_list"           : [[400,700,400],[700,600,700],[400,650,700]],
  "target_list_weight"    : [7,4,4],
  "split_rate_capillaries": [0.7, 0.3, 0.2],
  "diffusion"             : [300,200,200],
  "num_part_capillaries"  : 25,
  "length_vessel"         : [65,25,1],
  "split_vessel"          : 0.5
}
```
