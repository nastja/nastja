\page dump Dump

NAStJA is able to dump the current simulation state and resume this later.

Config data are read again and can changes this way, all other state data are stored by `dump()` function. `dump()` is
called for each container and actions, not all of them must store a state.
Bellow is the call graph of all `dump()`, `+` marks a function that can store data and `#` marks the data.

```
Simdata
| # timestep
| # generator
+ appendData
| + dataContainer(simdata)
|   + DataCells
|   + DataPFM
+ blockmanager
  + actionRegister
    + ActiveCellsDetection
    + Border
    + BorderDetection
    + CellCleaner
    + CellDivision
    + CellOrientation
    + CenterOfMass
    + CudaSync
    + CudaTransfer
    + DynamicBlock
    + ExchangeContainer
    + Filling
    + LoadBalance
    + LoggerSync
    + RotateField
    + Signaling
    | + signalFunctions
    |   + Decay00
    |   + Decay01
    |   + Interaction00
    |   + Typechange00
    |   + Celldeath00
    |   + Chemotherapy00
    |   + Radiotherapy00
    |   + SetRandomSeed
    |   + ExternalPotential00
    |   + ExternalPotential01
    + Sublist
    | + _recursiv: can be each element from actionRegister_
    + WriteCudaExchange
    + SweepCells : Sweep
    | + energyFunctions
    |   + Volume00
    |   + Volume01
    |   + Surface00
    |   + Surface01
    |   + Adhesion00
    |   + Adhesion01
    |   + Potential00
    |   + Potential01
    |   + Potential02
    |   + Motility00
    | + visitor
    |   + visitorCB
    + SweepHeat : Sweep
    + SweepHeatAVX : Sweep
    + SweepHeatCuda : Sweep
    + SweepHeatIter : Sweep
    + SweepPFC : Sweep
    + SweepPfVp : Sweep
    | # initialVolume
    | # deltaHiddenVolume
    | # increaseVolume
    + SweepPhaseField : Sweep
    + SweepPhaseFieldTemperature : Sweep
    + SweepPhaseFieldTemperatureCuda : Sweep
    + SweepSignalDiffusion : Sweep
    + SweepTemperature : Sweep
    + SweepTemperatureCuda : Sweep
    + WriteReader
    | # frame
    + CellInfo : WriteReader
    + Dump : WriteReader
    + ParallelVtkImage : WriteReader
    + Polygonize : WriteReader
    + Undump : WriteReader
    + Voxelizer : WriteReader
    + VtkImage : WriteReader
    + VtkImageReader : WriteReader
    + WriteLoad : WriteReader
    + WriteNeighbors : WriteReader
block loop
| field loop
| | # field data
| | + dataContainer(field)
| end
| + dataContainer(block)
|   + CellBlockContainer
|   | # cells
|   | # reservedCellID
|   | # reservedCellIDMax
|   + DynamicBlockContainer
|   | # ttl
|   + LoadBalanceContainer
end
```
