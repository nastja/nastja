\page models Models
[TOC]

# Phase-field models

\subpage pf-model-dendrite  ([git: Phase-field model dendrite](models/phase-field-dendrite.md))

# Phase-field crystal models

# Cellular Potts models

\subpage potts-model  ([git: Cellular Potts model](models/potts.md))
