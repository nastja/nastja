\page potts-model Cellular Potts model
[TOC]

Contains the parametrization of the cellular Potts model
```
  "PottsModel" :{}
```
Specifies the liquid phase in the simulation. All indices between 0 and  < liquid are solids.
The shape of solids are not changed during the simulation. Cell types 0 .. liquid are reserved and should not be used for actual cells but only for liquid and solid properties.
```
  "liquid" : 0,
```

```
  "temperature" : 50 ,
  "killDistance" : 1000,
  "checkerboard" : "01",
  "visitor" : "Visitor00",
  "visitorstepwidth": 25,
```

# Visitors

The visitor defines (together with the checkerboard) the order of one sweep through the field.

A Monte Carlo Move is only attempted at voxels that are at the edge of a cell, within a homogeneous area of one index, nothing is done.

* __Visitor00__ goes over the field in an orderly way (for loop over x,y,z), but skipping visitorstepwidth edge voxels between every application of the monte carlo step.
* __Visitor01__ goes over the field in an orderly way (for loop over x,y,z) without any skipping
* __Visitor02__ randomly chooses points in the field, without skipping

# Checkerboards
The checkerboards make sure that when calculating the surfaces no previous action in that sweep could have influenced that surface.

This is necessary to have consistent behaviour in block border regions.
* For __Surface00__ only a 2-Color checkerboard is needed, since it only uses the 6-(4)-nearest Neighbors to calculate the surface.
* For __Surface01__ an 8-Color Checkerboard is needed since the surface calculation needs all 26 neighboring voxels.
Checkerboard is shifted each timestep, so that isotrop sampling of the field is ensured after 2/8 timesteps.

checkerboard:
* "00" : 2 Color Checkerboard (Checkerboard00)
* "01" : 8 Color Checkerboard (Checkerboard01)

```
  "CenterOfMass" : {
    "Steps" : 1
  },
  "Division" : {
    "value": false,
    "Volume" : [0,8000,790,750,750,750]
  }
```

# Energy Funcions
The Hamiltonian of the Cellular Potts model is defined as follows:
\f$H_{CPM} = \sum_{cells}{\sum_{neighbors}{J_{\tau\left(\sigma\left(\vec{x}\right)\right)\tau\left(\sigma\left(\vec{x}'\right)\right)}\left(1-\delta_{\sigma\left(\vec{x}\right)\sigma\left(\vec{x}'\right)}\right)}}   \\
+ \sum_{cells}{\lambda_{V}\left(V\left(\sigma\right)-V_{0}\left(\sigma\right)\right)^{2}} \\
+ \sum_{cells}{\lambda_{S}(S(\sigma)-S_{0}(\sigma))^{2}}
\f$

The Energy functions used in the Monte Carlo acceptance criterion are defined here:
```
  "energyFunctions" : [
    "Volume00",
    "Surface01",
    "Adhesion01",
    "Potential00"
  ],
```

## Adhesion Energy

```
  "map_adhesion" :  [[ 0, 0, 0, 0, 0, 0],
                     [ 0,15, 5, 3, 3, 3],
                     [ 0, 5,13, 3, 3, 3],
                     [ 0, 3, 3, 6, 3, 3],
                     [ 0, 3, 3, 3, 6, 3],
                     [ 0, 3, 3, 3, 3, 6]],
```
Available Options:
* __EnergyAdhesion00__: Calculate Adhesive surfaces by side counting
* __EnergyAdhesion01__: Calculate Adhesive surfaces by marching cubes

## Potential Energy
Defines a Potential energy of the cells along one direction, which is defined in with the x-y-z coordinates in potentialDirection.

The coupling of the cells is defined in map_coupling by cell type. (Only works for type > liquid).
```
  "map_coupling" : [0,0,0,0,0,0] ,
  "potentialDirection" : [1,1,1],
```
Available Options:

* __EnergyPotential00__
* __EnergyPotential01__

## Volume Energy

Defines the Volumes of the cells by cell type (only used for type > liquid), as well as the coupling \f$ \lambda_{V} \f$ sizeChange changes the defaultValue over time so the volume \f$ V_0 = defaultVolume + sizeChange * age \f$
```
  "defaultVolume" : [0,1000,1000,750,750,750] ,
  "lambdaVolume"  : [0,3,3,3,3,5],
  "sizeChange"    : [0,0,0,0,0,0,0,0],
```
Available Options:
* __EnergyVolume00__ Counting Voxels
* __EnergyVolume01__ Counting edges (Makes no difference to above??) DEVELOP
* __EnergyVolume01__ Marching cubes (Not Working?) DEVELOP

## Surface Energy

Defines the Surfaces of the cells by cell type (only used for type > liquid), as well as the coupling \f$ \lambda_{S} \f$ sizeChange changes the defaultSurface over time so the Surface \f$ S_0 = defaultSurface + sizeChange * age \f$ sizeChange is the same for Volume and Surface and needs to be specified only once
```
  "defaultSurface" : [0,520,520,700,900,900] ,
  "lambdaSurface" : [0,1,1,1,1,1],
  ("sizeChange" : [0,0,0,0,0,0,0,0],)
```
* __Surface00__ calculates the Surface by counting the sides of a voxel -> manhattan metric (EnergySurface00)
* __Surface01__ calculates the Surface by a marching cubes algorithm -> euclidean metric (EnergySurface01)

# Signalling

```json
  "Signalling": {
    "Functions" : ["Decay00", "Decay01", "Interaction00"],
    "Value" : true,
    "Steps" : 1
  }
```

Initiating the signal at simulation start with the values by cell type.
```
  "startSignal0" : [0,20,0, 0,0,0,0],
  "startSignal1" : [0, 0,0, 0,0,0,0],
  "startSignal2" : [0, 0,0, 0,0,0,0],
```
If constant Signal is 1 for a cell type, the signal doesnt change from the StartSignal.
```
  "constantSignal" : [1,1,0,0,0,0,0,0,0],
```
Decay coefficients of signaly by type.
* __relative Decay__ multiplies the factor to the signal of each cell.
* __absolute Decay__ adds the factor to the signal.
```
  "relativeDecaySignal0" : [0,1,0.999,1,0,0,0],
  "relativeDecaySignal1" : [0,1,1,1,0,0,0],
  "relativeDecaySignal2" : [0,1,0.999,1,0,0,0],
  "absoluteDecaySignal0" : [0,0,0,0,0,0,0],
  "absoluteDecaySignal1" : [0,0,0,0,0,0,0],
  "absoluteDecaySignal2" : [0,0,0,0,0,0,0],
```
Diffusion coefficients between cells by type.
```
  "diffusionSignal0": [[0,  0,  0,0,0,0],
                       [0,  0,0.1,0,0,0],
                       [0,0.1,0.1,0,0,0],
                       [0,  0,  0,0,0,0],
                       [0,  0,  0,0,0,0],
                       [0,  0,  0,0,0,0]],

  "diffusionSignal1": [[0,0,0,0,0,0],
                       [0,0,0,0,0,0],
                       [0,0,0,0,0,0],
                       [0,0,0,0,0,0],
                       [0,0,0,0,0,0],
                       [0,0,0,0,0,0]],

  "diffusionSignal2": [[0,  0,  0,0,0,0],
                       [0,  0,0.1,0,0,0],
                       [0,0.1,0.1,0,0,0],
                       [0,  0,  0,0,0,0],
                       [0,  0,  0,0,0,0],
                       [0,  0,  0,0,0,0]],
```
Signal Interaction defines the production of a signal when another signal is present in one cell.
So in this example, Cell type 1 gets added A * Signal0 (its own Signal0) to its Signal1.
```
  "interactionSignal0":[[0,0,0,0,0,0],
                        [0,0,0,0,0,0],
                        [0,0,0,0,0,0]],
  "interactionSignal1":[[0,A,0,0,0,0],
                        [0,0,0,0,0,0],
                        [0,0,0,0,0,0]],
  "interactionSignal2":[[0,0,0.001,0,0,0],
                        [0,0,0    ,0,0,0],
                        [0,0,0    ,0,0,0]]
```

## Signal based Type change
Celltype A changes to Celltype B if Signal C is Greater/Smaller than D
```
  "typeChangeGreater": [[4,5,2,7],[5,6,2,9],[0,0,0,0],[0,0,0,0]],
  "typeChangeSmaller": [[A,B,C,D],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
```
