\page pf-model-dendrite Phase-field model for dendritic solidification
[TOC]

Two fields are calculated, one for the phase-field and the other for the temperature.

# Phase-field
The phase-field field stores an order parameter. For the calculation of the order parameter, a forward Euler method with a D3C27 stencil is used (SweepPhaseFieldTemperature). Therefore the values of the temperature field in the old time-step are read.

# Temperature
The temperature field is also calculated using the forward Euler method with a D3C27 stencil (SweepTemperature). Here, the phase-field values of the old and new time-step are needed.
