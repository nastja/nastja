\page publilcations Publications
[TOC]

* __Non-Collective Scalable Global Network Based on Local Communications__
Berghoff, M.; Kondov, I.
_9th Workshop on Latest Advances in Scalable Algorithms for Large-Scale Systems (ScalA)_, Dallas, TX, USA, November 12, 2018, 25–32, IEEE, Piscataway (NJ). 2018 [doi:10.1109/ScalA.2018.00007](https://doi.org/10.1109/ScalA.2018.00007)
* __Massively Parallel Stencil Code Solver with Autonomous Adaptive Block Distribution__
Berghoff, M.; Kondov, I.; Hötzer, J.
_IEEE transactions on parallel and distributed systems_. 2018. [doi:10.1109/TPDS.2018.2819672](https://doi.org/10.1109/TPDS.2018.2819672)
* __Crystal-melt interface mobility in bcc Fe: Linking molecular dynamics to phase-field and phase-field crystal modeling__
Guerdane, M.; Berghoff, M.
_Physical review / B_, 97 (14), 144105. 2018. [doi:10.1103/PhysRevB.97.144105](https://doi.org/10.1103/PhysRevB.97.144105)
* __A Global Network for Non-Collective Communication in Autonomous Systems__
Berghoff, M.; Kondov, I.
_ICCS 2017, The International Conference on Computational Science_, Zürich, Switzerland, 12th - 14th June 2017. 2017. [doi:10.5445/IR/1000079498](https://doi.org/10.5445/IR/1000079498)
