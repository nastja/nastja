\page general General
[TOC]

User Information for the NAStJA Framework
# Introduction

# Installation

## Prerequisites:

```
cmake
GSL
c++ compiler with c++14
```

## Build

```
git clone git@gitlab.com:mb1248/nastja.git
cd nastja
mkdir build
cd build
cmake ..
make
```

Sometimes the submodule paths changes and the automatic update process huggles, you can try `git submodule update --init --recursive --remote` to resolve this issue.

# Options

- `mpirun -n x ./nastja`  starts nastja with x cores
- `-c configfile.json` Determines the configfile, essential
- `-o outdir`            Specifies the output directory, default ./outdir/


# Cofiguring general Simulation

Each subsection is its own json scope with `"Title": { options }` options are stated within the subsection

## Application

"Cells"

## Geometry

```
{
    "Blocksize": [30,30,25],
    "Blockcnt": [1,1,1]
}
```

## Settings

```
{
    "Timesteps": 300,
    "randomSeed": 42
}
```

## Writers
Different `Writers` can be defined in the `Writers` object.
Each block writes a .csv file with the properties of all cells
```
"Writers" : {
    "WriteCenters": {
        "writer": "WriteCenters",
        "field": "cells",
        "steps": 60
    }
```

One .vti file is written out, with the vtk file format specified in OutputType
```
    "ParallelVTK_Cells": {
        "writer": "ParallelVtkImage",
        "outputType": "UInt32",
        "field": "cells",
        "steps": 60
    }
```

Each block writes out its individual .vti file
```
    "VTK_Cells": {
        "writer": "VtkImage",
        "field": "cells",
        "steps": 60
    }
}
```

To activate `Writers` use
```
 "WriteActions": [
    "WriteCenters",
    "ParallelVTK_Cells",
    "VTK_Cells"
 ]
```
to specify the order.

## Filling

Sets a rectangular cell in the intervals, the cell type is specified by "celltype"
```
"cells" : [
    {
        "shape": "cube",
        "box": [[x_start,y_start,z_start],[x_end,y_end,z_end]]
        "celltype": 2
    },
    ...
]
```

See \subpage filling
