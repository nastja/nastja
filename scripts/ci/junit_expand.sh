#!/bin/bash
file="$1"
XMLSTARLET=$(which xmlstarlet)
BASE64=$(which base64)
ZLIBFLATE=$(which zlib-flate)

if [[ -z $XMLSTARLET ]]; then echo "xmlstarlet not found"; exit 1; fi
if [[ -z $BASE64 ]]; then echo "base64 not found"; exit 1; fi
if [[ -z $ZLIBFLATE ]]; then echo "zlib-flate not found"; exit 1; fi

count=$($XMLSTARLET sel -t -v "count(//testcase)" "$file")

for (( tc = 1; tc <= $count; tc++ )); do
  for tag in system-out failure skipped; do
    pack=$($XMLSTARLET sel -t -v "//testcase[$tc]/$tag" "$file")
    if [[ ! -z "$pack" ]]; then
      unpack="$(echo $pack | sed -e "s/====$//" | $BASE64 -d | $ZLIBFLATE -uncompress | sed 's/\x1b/\\x1B/g')"
      $XMLSTARLET ed --inplace -u "//testcase[$tc]/$tag" -v "$unpack" "$file"
    fi
  done
done

