#!/bin/bash
binary=$(which llvm-cov-6.0)
if [ -z "$binary" ]; then
  binary=$(which llvm-cov-5.0);
fi

exec $binary gcov "$@"
