#!/bin/bash
genhtml coverage_clean.info -o coverage --num-spaces 2 --legend --demangle-cpp --title "$CI_COMMIT_REF_SLUG"
