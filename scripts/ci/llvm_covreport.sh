#!/bin/bash
# generate llvm report
# llvm-cov-9 show ./nastja unittest/unittest -instr-profile=coverage.profdata -format=html -output-dir=cov_html -ignore-filename-regex="/external/" --Xdemangler=c++filt

# generate lcov report
llvm-cov-9 export ./unittest/unittest -object ./nastja -instr-profile=coverage.profdata --format=lcov -ignore-filename-regex="/external/" > coverage.lcov
genhtml coverage.lcov -o coverage --num-spaces 2 --legend --demangle-cpp --title "$CI_COMMIT_REF_SLUG"
