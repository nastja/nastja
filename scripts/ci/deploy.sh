#!/bin/bash

echo Starting deployment...

mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host tucana.uberspace.de\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

eval $(ssh-agent -s)
echo "${DEPLOY_KEY}" | ssh-add -

echo Key length: "${#DEPLOY_KEY}"
ssh-add -l

mkdir -p deploy

branch=$(echo $CI_COMMIT_REF_SLUG | cut -d"-" -f1)

target="mb1248@tucana.uberspace.de:html/nastja/"

echo "Start syncing branch $CI_COMMIT_REF_SLUG ($branch) to target $target..."

#syncing website tmplate to site. Use RewriteRules in .htaccess (a copy have to be in the parent folder)
rsync -a --delete ../docs/webtemplate/. $target/site/
if [ "$branch" == "master" ]; then
  #master: syncing documenataion and coverage
  rsync -a --delete docs/html/. $target/doxygen/
  rsync -a --delete coverage/. $target/coverage/
else
  #other branch: syncing coverage
  rsync -a --delete coverage/. $target/coverage-$branch/
fi
