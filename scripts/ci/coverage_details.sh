#!/bin/bash
for i in $(seq $(ctest -N | tail -1 | cut -d":" -f2)); do
  lcov --zerocounters --directory CMakeFiles/
  ctest -I $i,$i
  lcov --capture --directory CMakeFiles/ --base-directory . --gcov-tool ../scripts/ci/llvm-gcov.sh --test-name "$(ctest -N -I $i,$i | sed -n "2p")" -o coverage_$i.info
  lcov --remove coverage_$i.info /usr/\* external/\* *evaluator.stdout* *configfile.stdout* -o coverage_clean_$i.info
done

genhtml coverage_clean_*.info -o coverage --num-spaces 2 --legend --demangle-cpp --show-details --title master
