#!/bin/bash
#compress data needed to trace coverage in testing
find . -name "*.gcno" > files
echo "nastja" >> files
echo "unittest/unittest" >> files
echo "CTestTestfile.cmake" >> files
echo "unittest/CTestTestfile.cmake" >> files
echo "test/CTestTestfile.cmake" >> files
tar czf artifacts.tar.gz --files-from files
ls -sh artifacts.tar.gz
