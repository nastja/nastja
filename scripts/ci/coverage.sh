#!/bin/bash
lcov --capture --directory CMakeFiles/ --base-directory . --gcov-tool ../scripts/ci/llvm-gcov.sh -o coverage.info
lcov --remove coverage.info /usr/\* \*/external/\* *evaluator.stdout* *configfile.stdout* -o coverage_clean.info

#Regex: \s*lines\.*:\s*(\d+.\d+\%)
