#!/bin/bash
# collect profdata
llvm-profdata-9 merge unittest/ut*.profraw test/st*.profraw -o coverage.profdata

# Write total coverage
echo "Code Coverage:"
llvm-cov-9 report ./unittest/unittest -object ./nastja -instr-profile=coverage.profdata -ignore-filename-regex="/external/" | tail -1

#Regex: ^TOTAL.* (\d+\.\d+\%)
