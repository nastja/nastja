#!/bin/bash

# head(key0) list(key1) tail(key2)
csplit -sf key -n 1 key.html '/<dl/'+1 '/<\/dl/'

# remove headings
sed -i '/^<dt/d' key1

# remove line breaks
sed -i -r ':a;N;$!ba;s%(<br */>|</?ul>|</li>)\n%\1%g' key1

# remove dd and p
sed -i 's/^<dd>//' key1
sed -i 's%\s*</dd>$%%' key1
sed -i '/^$/d' key1

sed -i -r 's/^<p( class="(start|inter|end)dd")*>//' key1
sed -i 's%\s*</p>$%%' key1

# put anchor to end
sed -i -r 's%(^(<a class="anchor" id="_key[0-9]{6}"></a> )?<span class="key">)(.*)%\3\1%' key1

# remove factories for the first
sed -i '/title="Factory Interface."/d' key1

# remove links from keys
sed -i -r 's%^<a class="el" href="[^>]*>([a-zA-Z0-9]*)</a>%\1%' key1

# sort
sed -i 's%</span>% </span>%' key1
sort -o key1 key1
sed -i 's% </span>%</span>%' key1

# make look-up table
sed -r 's/(^[^.<]*).*/\1/' key1 | uniq -c > key_cnt

TOC=""
keycnt=0
keyline=0
while read -r line; do
  if [[ keycnt -eq 0 ]]; then
    # loockup count for next key group
    (( keyline++ ))
    keys=$(sed "${keyline}q;d" key_cnt |  awk '{print $1}')
    key=$(sed "${keyline}q;d" key_cnt |  awk '{print $2}')
    keycnt=$keys
    echo "<dt><a class=\"anchor\" id=\"autotoc_$key\"></a>$key</dt>"
    TOC="$TOC<li class="level2"><a href="#autotoc_$key">$key</a></li>"
  fi
  entry=$(echo "$line" | sed -r 's%(.*)((<a class="anchor" id="_key[0-9]{6}"></a> )?<span class="key">$)%\2\1%')

  # annotate entry
  if [[ $keys -eq 1 ]]; then
    echo "<dd>$entry</dd>"
  elif [[ $keycnt -eq $keys ]]; then
    echo "<dd>"
    echo "<p class="startdd">$entry</p>"
  elif [[ $keycnt -eq 1 ]]; then
    echo "<p class="enddd">$entry</p>"
    echo "</dd>"
  else
    echo "<p class="interdd">$entry</p>"
  fi

  (( keycnt-- ))

done < key1 > key1_


mv key1_ key1
rm key_cnt

# combine
output="key.html"
cat key? > $output

# extend TOC
TOC="<ul>$TOC</ul>"
sed -i "s%Overview of all Config Keys</a></li>%Overview of all Config Keys</a>$TOC</li>%" $output
