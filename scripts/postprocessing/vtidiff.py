#!/usr/bin/env python3
import vtk
import vtk.util.numpy_support as VN
import numpy
import argparse
from pathlib import Path

#command line interface
parser = argparse.ArgumentParser()
parser.add_argument('file0')
parser.add_argument('file1')
parser.add_argument('output', nargs='?')
args = parser.parse_args()

if not Path(args.file0).is_file():
  print("'{}' does not exists.".format(args.file0))
  exit()

if not Path(args.file1).is_file():
  print("'{}' does not exists.".format(args.file1))
  exit()

# create a reader
reader = vtk.vtkXMLImageDataReader()

# read file0
reader.SetFileName(args.file0)
reader.Update()
extend0 = reader.GetOutput().GetExtent()
spacing0 = reader.GetOutput().GetSpacing()
origin0 = reader.GetOutput().GetOrigin()
phi0 = VN.vtk_to_numpy(reader.GetOutput().GetCellData().GetAbstractArray(0))

# read file1
reader.SetFileName(args.file1)
reader.Update()
extend1 = reader.GetOutput().GetExtent()
spacing1 = reader.GetOutput().GetSpacing()
origin1 = reader.GetOutput().GetOrigin()
phi1 = VN.vtk_to_numpy(reader.GetOutput().GetCellData().GetAbstractArray(0))

# check similarity of file geometry
if spacing0 != spacing1:
  print("Spacings are not equal {} != {}.".format(spacing0, spacing1))
else:
  print("Spacing: {}".format(spacing0))

if origin0 != origin1:
  print("Origins are not equal {} != {}.".format(origin0, origin1))
else:
  print("Origin:  {}".format(origin0))

if extend0 != extend1:
  print("Extends are not equal {} != {}.".format(extend0, extend1))
else:
  print("Extend:  {}".format(extend0))

# calculates the difference
diff = phi0 - phi1

if args.output:
  imageData = vtk.vtkImageData()
  imageData.SetOrigin(origin0)
  imageData.SetSpacing(spacing0)
  imageData.SetDimensions(reader.GetOutput().GetDimensions())
  imageData.GetCellData().SetScalars(VN.numpy_to_vtk(diff))
  imageData.SetExtent(extend0)

  writer = vtk.vtkXMLImageDataWriter()
  writer.SetDataModeToAppended()
  writer.SetEncodeAppendedData(False)
  writer.SetCompressorTypeToNone()
  writer.SetHeaderTypeToUInt64()
  writer.SetFileName(args.output)
  writer.SetInputData(imageData)
  writer.Write()

print()
print('Average                  {}'.format(numpy.mean(diff)))
print('Maximum                  {}'.format(numpy.max(diff)))
print('Minimum                  {}'.format(numpy.min(diff)))
print('Count of different cells {} of {} ({}%)'.format(numpy.count_nonzero(diff), numpy.size(diff), 100 * numpy.count_nonzero(diff)/float(numpy.size(diff))))
print('Absolute sum             {}'.format(numpy.sum(abs(diff))))
print('Absolute average         {}'.format(numpy.mean(abs(diff))))

if diff == 0:
	sys.exit(0)
