#!/bin/bash

BASE="output_$1"

firstfile=$(find . -name "${BASE}*-00000.vti" | head -1)

if [[ ! -f $firstfile ]]; then
  echo "Can not find a file named '${BASE}*-00000.vti'"
  exit 1
fi

OUTPUT=${BASE}.pvd
echo '<VTKFile type="Collection" version="1.0" byte_order="LittleEndian" header_type="UInt64">' > $OUTPUT
echo '<Collection>' >> $OUTPUT
find . -name "${BASE}*.vti" | sed -r -e 's/\.\/(.*([0-9]{5})\..*)/<DataSet timestep="\2" file="\1"\/>/' >> $OUTPUT
echo '</Collection>' >> $OUTPUT
echo '</VTKFile>' >> $OUTPUT
echo " Done."
