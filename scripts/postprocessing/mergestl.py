#!/usr/bin/env python
import math
import stl
from stl import mesh
import numpy
import sys
import argparse
import glob

#command line interface
parser = argparse.ArgumentParser()
parser.add_argument("-f", action = "store", dest = "foldername", help = "Store the root folder's name")
parser.add_argument("-o", action = "store", dest = "merged_filename", help = "Store the combined stl filename")
results = parser.parse_args()

sublist = []
counter = 0
largelist = glob.glob('./'+ results.foldername + '/[0-9][0-9][0-9][0-9][0-9][0-9]/output_triangles-[0-9][0-9][0-9][0-9][0-9].stl')
listlength = len(largelist)

while (listlength != 0) :
  #substring created padded with zeroes
  substr = str(counter).zfill(5)
  sublist = [k for k in largelist if ("-" + substr[-5 : ]) in k]
  listlength = listlength - len(sublist)

  #merge the stl files in each sublist
  combined = mesh.Mesh.from_file(sublist[0])
  for i in range(1, len(sublist)) :
    mesh_new = mesh.Mesh.from_file(sublist[i])
    combined = mesh.Mesh(numpy.concatenate([combined.data, mesh_new.data]))

  combined.save(results.foldername + "/" + results.merged_filename + substr[-5 : ] + '.stl', mode=stl.Mode.BINARY)
  counter = counter + 1
