#!/bin/bash

IFS=$'\n' read -r -d '' -a columns < <(sed -n '2,$p' $1/timing-00000.dat | cut -f1 -d" ") # && printf '\0' )

echo "#Name Calls Average Min Max"
for c in ${columns[@]}; do
  echo $c $(grep "$c " $1/timing-00000.dat | awk '{print $3}')  $(grep $c $1/* | awk '{print $5}' | awk '
  {
    if(min=="") {
      min=max=$1
    };
    if($1>max) {max=$1};
    if($1<min) {min=$1};
    total+=$1; count+=1
  }
  END{print total/count, min, max}'
  )
done | column -t
