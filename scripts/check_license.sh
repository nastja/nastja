#/bin/bash

tmp=$(mktemp)

find .  \( -name "*.h" -o -name "*.cpp" -o -name "*.cu" \)  | while read file; do
  diff ../scripts/license.template <(head -8 $file) > /dev/null || echo $file
done
