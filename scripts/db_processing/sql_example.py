#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
import sqlite3
conn = sqlite3.connect("data2.db")

df = pd.read_sql_query("""SELECT
  SUM(CASE typ WHEN 5 THEN volume ELSE 0 END) AS volume5,
  SUM(CASE typ WHEN 6 THEN volume ELSE 0 END) AS volume6,
  SUM(CASE typ WHEN 7 THEN volume ELSE 0 END) AS volume7,
  SUM(CASE typ WHEN 8 THEN volume ELSE 0 END) AS volume8,
  SUM(CASE typ WHEN 9 THEN volume ELSE 0 END) AS volume9,
  SUM(CASE typ WHEN 10 THEN volume ELSE 0 END) AS volume10,
  SUM(CASE typ WHEN 11 THEN volume ELSE 0 END) AS volume11,
  SUM(CASE typ WHEN 4 THEN volume ELSE 0 END) AS volume4
  FROM cellinfo INNER JOIN siminfo
  ON cellinfo.simid = siminfo.rowid
  WHERE siminfo.info='Case 5'
  GROUP BY time;""", conn)
df.plot.area()

plt.show()
