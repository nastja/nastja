#!/bin/bash

if [ "$#" -ne 3 ]; then
  echo "Missing Argument!"
  echo "Usage:"
  echo "  $0 database info_string csv_folder"
  exit 1
fi

database="$1"

if [ ! -f $database ]; then
echo "Create new database..."
sqlite3 -batch $1 <<EOF
create table siminfo (path TEXT, info TEXT);
create table cellinfo (simid INT, time INT, cellid INT, centerx INT, centery INT, centerz INT, volume REAL, surface REAL, typ INT, signal0 REAL, signal1 REAL, signal2 REAL, age INT);
EOF
fi

importfolder="$3"

siminfo="$2"
simpath=$(cd "$(dirname "$importfolder")"; pwd -P)
simid=$(sqlite3 -batch $database "insert into siminfo (path, info) values (\"$simpath\", \"$siminfo\");select last_insert_rowid();")
echo "Simulation '$siminfo' at path '$simpath' was insert at row $simid."
for csv in $(ls $importfolder/*.csv); do
  echo -ne "\rImport $csv"
  timestep=$(sed -e 's/.*-\([0-9]*\).csv/\1/' <<< $csv)
  awk -v id=$simid -v t=$timestep '/^[0-9]* [0-9]/{print id, t, $0}' $csv |\
    cat <(echo -e ".mode csv\n.separator ' '\n.import /dev/stdin cellinfo\n") - |\
    sqlite3 $database
done
echo ""
