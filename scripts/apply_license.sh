#/bin/bash

tmp=$(mktemp)

find .  \( -name "*.h" -o -name "*.cpp" \)  | while read file; do
  cat ../scripts/license.template $file > $tmp
  mv $tmp $file
done
