#!/usr/bin/env python
import codecs
import json
import numpy
import fnmatch
import os

matches = []
for root, dirnames, filenames in os.walk('.'):
  for filename in fnmatch.filter(filenames, '*00000.vti'):
    matches.append(os.path.join(root, filename))

splits = []
for match in matches:
  splits.append(match.split('/'))

a = numpy.asarray(splits)[:,[1,2,3]].astype(int)

virtualdomain = [7,13,10]
blocks = numpy.zeros([virtualdomain[2], virtualdomain[1], virtualdomain[0]])

for r in range (0, a.shape[0]):
  blocks[a[r,2], a[r,1], a[r,0]] = 1

print "min:", numpy.amin(a, axis=0)
print "max:", numpy.amax(a, axis=0)

json_decoded = json.loads('{}')
json_file = "blockconfig_respawn.json" ## your path variable
json_decoded['Blocktype'] = blocks.astype(int).tolist()
with open(json_file, 'w') as jsonfile:
  json.dump(json_decoded, jsonfile, separators=(',', ':'))
