#!/usr/bin/env python
import codecs
import json
import numpy

blocksize = 80
interfacewidth = 7
virtualdomain = [64,128,100]
sphere_center = numpy.array([0,0,1000]) #height = r/2
sphere_radius = 5000

def distance(coord):
  return numpy.linalg.norm(coord - sphere_center)

#test all 8 edges with + and -interfacewidth
#if some in some outside we have the interface
def inside(x, y, z) :
  xl = x * blocksize
  yl = y * blocksize
  zl = z * blocksize
  xu = (x + 1) * blocksize - 1
  yu = (y + 1) * blocksize - 1
  zu = (z + 1) * blocksize - 1

  edges=numpy.array(
        [[xl,yl,zl],
         [xl,yl,zu],
         [xl,yu,zl],
         [xl,yu,zu],
         [xu,yl,zl],
         [xu,yl,zu],
         [xu,yu,zl],
         [xu,yu,zu]])

  cnt = 0;
  for i in range(0, 8):
    dist = distance(edges[i])
    if dist <= sphere_radius + interfacewidth:
      cnt += 1
    if dist <= sphere_radius - interfacewidth:
      cnt += 1

  return cnt > 0 and cnt < 16

blocks = numpy.zeros([virtualdomain[2], virtualdomain[1], virtualdomain[0]])

for z in range (0, virtualdomain[2]):
  print z
  for y in range (0, virtualdomain[1]):
    for x in range (0, virtualdomain[0]):
      if inside(x, y, z):
        blocks[z, y, x] = 1

json_decoded = json.loads('{}')
json_file = "blockconfig_test.json" ## your path variable
json_decoded['Blocktype'] = blocks.astype(int).tolist()
with open(json_file, 'w') as jsonfile:
  json.dump(json_decoded, jsonfile, separators=(',', ':'))
