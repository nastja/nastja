#!/usr/bin/env python
import argparse
import sys

numX = 12
numY = 12
numZ = 12

class color:
  PURPLE = '\033[95m'
  CYAN = '\033[96m'
  DARKCYAN = '\033[36m'
  BLUE = '\033[94m'
  GREEN = '\033[92m'
  YELLOW = '\033[93m'
  RED = '\033[91m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'
  END = '\033[0m'

def getNeighborID(id, dx, dy, dz):
  x,y,z = getcoords(id)
  X = (numX + x + dx) % numX
  Y = (numY + y + dy) % numY
  Z = (numZ + z + dz) % numZ
  return getid(X, Y, Z)

def getcoords(id):
  z = id // (numX * numY)
  id = id - numX * numY * z
  y     = id // numX
  x     = id % numX
  return x, y, z

def getid(x, y, z):
  return z * numY * numX + y * numX + x

parser = argparse.ArgumentParser(description='Calc block IDs.')
parser.add_argument('id', metavar='N', type=int, nargs='*', help='The baseID.')
parser.add_argument('-f', type=int, help='The baseID.')
args = parser.parse_args()

if args.f:
  # full mode
  id = args.f
else:
  id = args.id[0]


if args.f:
  for z in range(-2, 3):
    for y in range(-2, 3):
      for x in range(-2, 3):
        neighbor = getNeighborID(id,x,y,z)
        if neighbor in args.id:
          print color.BOLD + str(neighbor) + color.END,
        else:
          print neighbor,
      print
    print

else:

  print getNeighborID(id,-1,0,0)
  print getNeighborID(id,+1,0,0)
  print getNeighborID(id,0,-1,0)
  print getNeighborID(id,0,+1,0)
  print getNeighborID(id,0,0,-1)
  print getNeighborID(id,0,0,+1)