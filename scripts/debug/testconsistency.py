#!/usr/bin/env python
import re

DL = 0

def getblocks(rank, time):
  with open('{:06d}/comm.log'.format(rank)) as origin_file:
    list = []
    for line in origin_file:
      found = re.findall(r'^\s*{} BLKL.*'.format(time), line)
      if not found:
        continue;
      list.append(found[0])
    return list[DL].split(':')[1].strip().split(' ')

def getcomms(rank, time):
  with open('{:06d}/comm.log'.format(rank)) as origin_file:
    list = []
    for line in origin_file:
      found = re.findall(r'^\s*{} COMM.*'.format(time), line)
      if not found:
        continue;
      list.append(found[0])
    return list[DL].split(':')[1].strip().split(' ')

def getknownblocks(rank, source, time):
  with open('{:06d}/comm.log'.format(rank)) as origin_file:
    list = []
    for line in origin_file:
      found = re.findall(r'^\s*{} KNWB,{:3d}.*'.format(time, source), line)
      if not found:
        continue;
      list.append(found[0])
    return list[DL].split(':')[1].strip().split(' ')

def getannblocks(rank, time):
  with open('{:06d}/comm.log'.format(rank)) as origin_file:
    list = []
    for line in origin_file:
      found = re.findall(r'^\s*{} ANNB.*'.format(time), line)
      if not found:
        continue;
      list.append(found[0])
    return list[DL].split(':')[1].strip()



def checksource(source):
  org_blocks = getblocks(source, frame)
  comms = getcomms(source, frame)
  # comms = [map(int, x) for x in comms]
  print_ann = False
  for i in comms:
    # print '\r', i,
    blocks = getknownblocks(int(i), source, frame)
    if blocks != org_blocks:
      print_ann = True
      print i, set(org_blocks) - set(blocks), set(blocks) - set(org_blocks)

  if print_ann:
    print getannblocks(source, frame)
    print source, org_blocks
    print comms
    print

for frame in range(0, 200):
  print "FRAME ", frame
  for source in range (0,22):
    checksource(source)