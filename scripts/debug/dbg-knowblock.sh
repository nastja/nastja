#!/bin/bash
NUMOFFILE=$(($(ls -1 output_neighbors-00000-*.json | wc -l)-1))
tmpfile="tmplist"
for i in  $(seq -f %05.f 0 $NUMOFFILE); do
  echo $i: $(
  for f in output_neighbors-*-$i.json; do
   cat $f | sed -r -e 's/\s*[0-9]*: \[[^[]*\] \[([^[]*)\]/\1/' -e 's/\([ 0-9]{3}\)//g' | tr ' ' '\n' | sort -gu | tr '\n' ' '
   # | cut -d: -f1 | tr '\n' ',')
  # $(cat output_neighbors-*-$i.json | cut -d: -f1 | tr '\n' ',')
  done
  )
done | uniq -s6 > $tmpfile-known.list
