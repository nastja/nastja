#!/bin/bash
NUMOFFILE=$(($(ls -1 output_neighbors-00000-*.json | wc -l)-1))
tmpfile="tmplist"
for i in  $(seq -f %05.f 0 $NUMOFFILE); do
  echo $i: $(cat output_neighbors-*-$i.json | cut -d: -f1 | tr '\n' ',')
done | uniq -s6 > $tmpfile.list
