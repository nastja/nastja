#must blocks
for i in {0..693}; do echo $i; ../../scripts/blocks.py $i -n > must/$i.txt; done

#have blocks
for i in {0..693}; do j=$(printf "%05d" $i); cat output_neighbors-*-$j.json | sort -n | sed -r -e 's/\[[^[]*\]/[]/' > have/$i.txt; done

#count line of code
find ../src \( -name "*.cpp" -o -name "*.h" \) -exec wc -l {} +
git ls-files ../src/ ../unittest/ | while read f; do git blame --line-porcelain $f | grep '^author '; done | sort -f | uniq -ic | sort -n | awk '{sum+=$1; print $0} END {print sum}'
#without whitespace changes
git ls-files ../src/ ../unittest/ | while read f; do git blame -w -M -C -C --line-porcelain $f | grep '^author '; done | sort -f | uniq -ic | sort -n | awk '{sum+=$1; print $0} END {print sum}'

echo "#lines contributor"
git ls-files ../src/ ../unittest/ | while read f; do git blame -w -M -C -C --line-porcelain $f | grep '^author '; done | sort -f | uniq -ic | awk '{i = tolower($3); lines[i] = $1; if(name[i] == ""){name[i] = $4}} END {for (i in lines){ printf "%6d ", lines[i]; print toupper(substr(i, 1, 1))substr(i, 2), name[i]}}' | sort -nr

#delete many files
find . -name "*.vti" -delete

#move many files to target
find . -name "*json" -exec mv -t ../results/wokerdist_x-y-z/ {} +

#make md5 test
find . -name "*.vti"  -exec md5sum "{}" + > test.md5

#block count sum
find . -name "output_phasefield-*.vti" | sed -r -e 's/\.\/(.*([0-9]{5})\..*)/\2/' | sort | uniq -c > block_cnt.txt

#blocks per proc
find . -name "*.json" -exec wc -l {} \; | perl -pe 's/(\d*).*\/(\d*)\/.*-(\d*).*/\3 \2 \1/g' | sort | gawk 'BEGIN{delete arr[0]; last=-1}
      {
        if (last != $1) {
          printf "%d", last;
          for (i=1; i<=length(arr); ++i) printf " %d", arr[i];
          print "";
          last=$1;
          delete arr;
          arr[length(arr)+1]=$3
        } else {
          arr[length(arr)+1]=$3
        }
      }' > blocksperworker_lb2.txt

#blocks per proc
find . -name "*.json" -exec wc -l {} \; | perl -pe 's/(\d*).*\/(\d*)\/.*-(\d*).*/\3 \2 \1/g' | sort

#exit comms
frame=48205;for j in {0..7}; do for i in {0..7}; do grep -q "(  $i)" ./00000$j/output_neighbors-$frame.json; if [ $? -eq 0 ]; then echo -n "$j-$i "; else echo -n "    "; fi done; echo; done


#min, max
awk '{min=$2;max=$2;for(i=2;i<=NF;i++){if($i<min)min=$i;if($i>max)max=$i}print min, max}'
cat blocksperworker_lb2.txt | awk '{sum=0;min=$2;max=$2;for(i=2;i<=NF;i++){if($i<min)min=$i;if($i>max)max=$i;sum+=$i}print $1, min, max, sum}' > blocksperworker_min_lb2_stat.txt

#Änderungen
cat blocksperworker_min6.txt | awk 'NR<=2{for (i=2; i<=9; ++i) {old[i]=$i}}{printf "%d", $1;for (i=2; i<=9; ++i){printf " %d", $i-old[i]; old[i]=$i}printf"\n"}' > blocksperworker_min6_diff.txt

p [0:50000] 'blocksperworker.txt' u 1:2                         title "rank0" w l,\
            'blocksperworker.txt' u 1:($2+$3)                   title "rank1" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4)                title "rank2" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4+$5)             title "rank3" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4+$5+$6)          title "rank4" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4+$5+$6+$7)       title "rank5" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4+$5+$6+$7+$8)    title "rank6" w l,\
            'blocksperworker.txt' u 1:($2+$3+$4+$5+$6+$7+$8+$9) title "rank7" w l

file="bpw.txt"
p  file u 1:2 title "rank0" w l,\
            file u 1:3 title "rank1" w l,\
            file u 1:4 title "rank2" w l,\
            file u 1:5 title "rank3" w l,\
            file u 1:6 title "rank4" w l,\
            file u 1:7 title "rank5" w l,\
            file u 1:8 title "rank6" w l,\
            file u 1:9 title "rank7" w l

# performance lose
p [0:50000] file u 1:(1/($4/8/$3))  w l

#runs
#mpirun -np 8 ./nastja  > >(tee stdout.log) 2> >(tee stderr.log >&2)
#mpirun -np 8 ./nastja   2>&1 > /dev/null  | egrep "^[0-9]*-[0-9]*" -A1

clang-tidy -list-checks -checks="*"
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .
find src/ -name '*.cpp' -exec clang-tidy -p build --header-filter=src/ {} \;
find src/ -name '*.cpp' -exec clang-tidy -p build --header-filter=src/ -checks=modernize-use-default -fix {} \;
parallel -m clang-tidy-9 -p build --header-filter=src/ ::: $(find src/  -name '*.cpp') | tee tidy.txt
cat tidy.txt | egrep \]$ | sed -re 's/.*(\[.*\])/\1/g' | sort | uniq -c | sort -r

# Manhattan
w(N,d) = ceil(N**(1./d))
dia(N,d) = ceil(((d-1) * w(N,d) + ceil(N/(w(N,d)**(d-1))))*0.5)
plot [0:10000] dia(x,6), dia(x,5), dia(x,7), dia(x,8)

# include-fixer
# in build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
ln -s $PWD/compile_commands.json ../
cd ..
/usr/local/Cellar/llvm/5.0.1/share/clang/run-find-all-symbols.py -binary /usr/local/Cellar/llvm/5.0.1/bin/find-all-symbols
find src -name "*.cpp" -exec clang-include-fixer -db=yaml {} +
