#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys

numX = 12
numY = 12
numZ = 12
CPUs = 32
ROWS = 6
COLS = 2

def plotit(layer):
  data = rank_[layer:layer+1].reshape(numY,numZ)
  # heatmap = plt.imshow(data,interpolation='nearest')
  heatmap = plt.pcolormesh(data, edgecolors='gray',vmin=-1, vmax=(CPUs-1))
  plt.axis('off')
  for y in range(data.shape[0]):
    for x in range(data.shape[1]):
      if rank_[layer,y, x] > -1:
        plt.text(x + 0.5, y + 0.5, '%d' % dat_id[layer,y, x],
                 horizontalalignment='center',
                 verticalalignment='center',
                 )

def getLocalNeighborID(x, y, z, dx, dy, dz):
  X = (numX + x + dx) % numX
  Y = (numY + y + dy) % numY
  Z = (numZ + z + dz) % numZ
  return dat[Z][Y][X]

def getLocalNeighborRank(x, y, z, dx, dy, dz):
  X = (numX + x + dx) % numX
  Y = (numY + y + dy) % numY
  Z = (numZ + z + dz) % numZ
  return rank[Z][Y][X]

def print_neighbor():
  for z in range(0,numZ):
    for y in range(0,numY):
      for x in range(0,numX):
        if getLocalNeighborRank(x,y,z,0,0,0) == -1:
          continue

        sys.stdout.write("%3d: [" % getLocalNeighborID(x,y,z,0,0,0))
        # for (unsigned int i = 0; i < 6; i++) {
        #   if (block.second->getLocalNeighborID(i) == -1) {
        #     sys.stdout.write("   (   ) ")
        #   } else {
        #     sys.stdout.write("%3d(%3d) ", block.second->getLocalNeighborID(i), block.second->getLocalNeighborRank(i))
        #   }
        # }
        sys.stdout.write("] [")
        for dz in range(-2,3):
          for dy in range(-2,3):
            for dx in range(-2,3):
              if (abs(dx) + abs(dy) + abs(dz) >= 3):
                continue
              if getLocalNeighborRank(x, y, z, dx, dy, dz) == -1:
                sys.stdout.write("   (   ) ")
              else:
                sys.stdout.write("%3d(%3d) " % (getLocalNeighborID(x, y, z, dx, dy, dz), getLocalNeighborRank(x, y, z, dx, dy, dz)))
        print "]"


parser = argparse.ArgumentParser(description='Show blocks.')
parser.add_argument('frame', metavar='N', type=int, nargs=1, help='The frame')
parser.add_argument('-n', action='store_true')
args = parser.parse_args()


dat=np.arange(numX*numY*numZ).reshape(numZ,numY,numX)
dat_id=np.swapaxes(dat,0,2)[:,:,::-1]
# rank=np.random.randint(8,size=(6,6,6))
rank = np.zeros(numX*numY*numZ)
rank -= 1
for i in range(0, CPUs):
  filename = "%06d/output_neighbors-%05d.json" % (i,args.frame[0])
  liste = np.loadtxt(filename, delimiter=':', usecols=(0,), dtype=("int"))
  if isinstance(liste, list):
    for r in liste:
      rank[r] = i
  else:
    rank[liste] = i

rank = rank.reshape(numZ,numY,numX)
rank_ = np.swapaxes(rank,0,2)[:,:,::-1]

print_neighbor()

if args.n:
  exit(0)
plt.rcParams['toolbar'] = 'None'
plt.figure("Frame " + str(args.frame[0]), figsize=(4,6))
for i in range (0, min(numX, ROWS*COLS)):
  px = i % ROWS
  py = i / ROWS
  pid = px*COLS+py+1;
  plt.subplot(ROWS, COLS, pid)
  plotit(i)
# plt.subplot(312)
# plotit(1)
# plt.subplot(313)
# plotit(2)
# plt.subplot(312)
# heatmap = plt.pcolor(data1)
# plt.subplot(313)
# heatmap = plt.pcolor(data1)


# plt.colorbar()

plt.show()
