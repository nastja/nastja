# Contributing to NAStJA

## General workflow
1. Create an Issue, describing the problem or the feature.
2. Pick an issue by assigning it to you.
3. Use gitlab to create a new branch and merge request.
   * For the issue #42 `new feature` gitlab use the name `42-new-feature`, please add `issue/` or another prefix, see below. It is mandatorily that all branch names are lowercase. You can shorten the name after a few words.
   * The merge request is related to the issue and is marked as work in progress by the `WIP:`prefix.
   * Use a todo list in the merge-request to track the progress.
3. Add code and test for new features.
5. When finished, rebase your branch on top of master and remove the `WIP:` add a reviewer by assigning it to someone else.
6. When all annotations are removed, and the code is approved, the merge request can be merged.

## Git branches

Our primary branch is `master`. Only fast-forward merge requests are allowed. Also, all tests must be successful `ctest`.

Please ensure that you rebase your work on top of the `master` branch. For a linear history, use rebase instead of merging. It is fine if you rebase your branch, and you are the only developer on it. It is allowed to use force push, `git push --force-with-lease`.
In the case of a shared branch, use an appendix number for each rebasing.

You can find many documents about linear git history on the web, e.g., [http://www.bitsnbites.eu/a-tidy-linear-git-history/](http://www.bitsnbites.eu/a-tidy-linear-git-history/).

Use default action
```
git config --global pull.rebase true
```

Please follow the following naming scheme when pushing a topic branch:

- `issue/foo-bar` for general issues.
- `feature/foo-bar` for new features in the core of NAStJA
- `cells/feature/foo-bar` for new features in the scientific engine of NAStJA, here `cells` stands for the Cellular Potts Model
- `fix/foo-bar` for bug fixes
- `tests/foo-bar` for change only concerns the test suite
- `clean/foo-bar` for clean the code style
- `docs/foo-bar` for updating the documentation

These topic branches should be small and should early merge into the `master`. Complicated topics that are not approvable for the master can be collected in develop branches as `cells/develop` for the Cellular Potts Model.

If you have to merge manually, use
```
git merge --no-ff my-topic-branch
```
to preserve the history of a feature.

## Style guide
Use the provided clang-format style to prevent whitespace changes.
