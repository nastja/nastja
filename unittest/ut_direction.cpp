/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/D3C7.h"
#include "lib/stencil/direction.h"
#include <cstring>

using namespace nastja;

TEST_CASE("Direction", "[unit]") {
  SECTION("skipbitmap") {
    using namespace stencil;
    //                           corners edges       sides
    REQUIRE(skipExchange[0] == 0b01010101000100010101000001);
    REQUIRE(skipExchange[1] == 0b10101010001000101010000010);
    REQUIRE(skipExchange[2] == 0b00110011010001000011000100);
    REQUIRE(skipExchange[3] == 0b11001100100010001100001000);
    REQUIRE(skipExchange[4] == 0b00001111000011110000010000);
    REQUIRE(skipExchange[5] == 0b11110000111100000000100000);
  }

  SECTION("bitmap") {
    using namespace stencil;
    //                                           corners edges       sides C
    REQUIRE(genCommonBitset(Direction::C) == 0b000000000000000000000000001);
    REQUIRE(genCommonBitset(Direction::L) == 0b000000000000000000000000010);
    REQUIRE(genCommonBitset(Direction::DL) == 0b000000000000000000010001010);
    REQUIRE(genCommonBitset(Direction::BDL) == 0b000000010000010100010101010);
  }

  SECTION("relative coordinates") {
    using namespace stencil;
    for (unsigned int i = 0; i < numberOfDirections; i++) {
      REQUIRE(cx(D3C27::dir[i]) == c(0, i));
    }
  }

  SECTION("D3C7") {
    using namespace stencil;

    int sum = 0;
    for (unsigned int i = 0; i < D3C7::size; ++i) {
      sum += cx(D3C7::dir[i]);
      printf("%s, ", directionString[i].c_str());
    }
    printf("\n");
    REQUIRE(sum == 0);

    sum = 0;
    for (auto dir = D3C7::begin(); dir != D3C7::end(); dir++) {
      sum += dir.cy();
      printf("%s, ", dir.directionString().c_str());
    }
    printf("\n");
    REQUIRE(sum == 0);

    sum = 0;
    for (const auto& i : D3C7()) {
      sum += cz(D3C7::dir[i]);
      printf("%s, ", directionString[i].c_str());
    }
    printf("\n");
    REQUIRE(sum == 0);

    CHECK(std::strcmp((const char*)D3C7::name, "D3C7") == 0);  // workaround, why is the cast necessary?
  }
}
