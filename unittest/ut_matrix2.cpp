/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/format.h"
#include "lib/math/matrix2.h"
#include <sstream>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Rotation Matrix2", "[unit][class][math]") {
  SECTION("operator") {
    double array[4]{0, 1, 2, 3};
    Vector2<double> e1{1.0, 0.0};
    Vector2<double> e2{0.0, 1.0};

    Matrix2<int> a;
    Matrix2<int> b(2);
    Matrix2<int> c(1, 2, 3, 4);
    Matrix2<double> d(e1, e2);
    Matrix2<double> e(array);
    Matrix2<double> f(b);
    Matrix2<double> g{};
    g.set(1, 2, 3, 4);

    CHECK(c == g);

    CHECK(f == 2.0);
    f *= 0.5;
    CHECK(f == 1.0);
    e += f;
    CHECK(e == c);

    CHECK(b == 2);
    CHECK(2 == b);
    CHECK(b != 1);
    CHECK(1 != b);

    CHECK(b != c);

    a = b + c;
    CHECK(a[0] == 3);

    a = b - c;
    CHECK(a[0] == 1);

    a = b * 2;
    CHECK(a[0] == 4);

    a = b / 2;
    CHECK(a[0] == 1);

    a = -b;
    CHECK(a[0] == -2);

    a /= 2;
    a -= b;
    CHECK(a == -3);

    auto h = a * d;
    CHECK(h(1, 1) == -3);

    h = -1 * h;
    CHECK(h(1, 1) == 3);

    h *= d;

    b[3] = 1;
    CHECK(b != 2);
    f *= 2;
    CHECK(b != f);
  }

  SECTION("data") {
    const Matrix2<double> a(1.0);
    Matrix2<double> b;

    auto a_ptr = a.data();

    CHECK(a_ptr[0] == 1.0);
    CHECK(a(0, 0) == 1.0);

    auto b_ptr = b.data();
    b_ptr[0]   = 1.0;
    b_ptr[1]   = 1.0;
    b_ptr[2]   = 1.0;
    b_ptr[3]   = 1.0;

    CHECK(b == 1.0);
  }

  SECTION("print") {
    Matrix2<int> a(1, 0, 0, 1);
    std::stringstream str;
    str << a;
    CHECK(str.str() == "[[1,0]\n [0,1]]");
    CHECK(fmt::format("{}", a) == "[[1,0],\n [0,1]]");
  }

  auto rotation = Matrix2<double>().setRotation(0.5 * M_PI);

  SECTION("rotate") {
    Vector2<double> v{0.0, 1.0};
    v = rotation * v;
    REQUIRE(v[0] == Approx(-1.0));
    REQUIRE(v[1] == Approx(0.0).margin(1e-16));
  }

  SECTION("inverse rotate") {
    Vector2<double> v{0.0, 1.0};
    v = rotation.multiplyT(v);
    REQUIRE(v[0] == Approx(1.0));
    REQUIRE(v[1] == Approx(0.0).margin(1e-16));
  }
}
