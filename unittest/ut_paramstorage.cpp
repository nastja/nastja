/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/storage/paramstorage.h"

using namespace nastja;

TEST_CASE("Parameter Storage", "[unit][class]") {
  config::Config config;
  config.loadString(R"(
{
  "vector" : [-1, 2, 3],
  "vector_u" : [1, 2, 3],
  "vector_d" : [-1.2, 2, 3],
  "matrix" : [[1, 2, 3],[4, 5, 6],[7, 8, 9]],
  "tensor" : [[[  0,   1,  2],
               [ 10,  11,  12],
               [ 20,  21,  22]],
              [[100, 101, 102],
               [110, 111, 112],
               [120, 121, 122]],
              [[200, 201, 202],
               [210, 211, 212],
               [220, 221, 222]]],
  "map_matrix" : {
    "storage": "mapped",
    "mapping" : [1,4,10],
    "data" : [[1,2,3],
              [4,5,6],
              [7,8,9]]
  },
  "map_vector" : {
    "storage": "mapped",
    "mapping" : [1,4,10],
    "data" : [1,2,3]
  },
  "map_tensor" : {
    "storage": "mapped",
    "mapping" : [1,4],
    "data" : [[[1,2],
              [4,5]],
              [[3,6],
              [7,8]]]
  },
  "const" : {
    "storage": "const",
    "value" : 13.37
  },
  "const2" : 42
}
  )");

  SECTION("Type of Parameter") {
    auto vector  = ParameterStorage<int>("vector", config);
    auto vectoru = ParameterStorage<unsigned int>("vector_u", config);
    auto vectord = ParameterStorage<double>("vector_d", config);

    REQUIRE(vector.getValue(0) == -1);
    REQUIRE(vectoru.getValue(0) == 1);
    REQUIRE(vectord.getValue(0) == -1.2);
  }

  SECTION("1D Parameter") {
    auto vector = ParameterStorage<int>("vector", config);
    REQUIRE(vector.getDimension() == 1);
    REQUIRE(vector.getSize() == 3);

    REQUIRE(vector.getValue(2) == 3);
  }

  SECTION("2D Parameter") {
    auto matrix = ParameterStorage<int>("matrix", config);
    REQUIRE(matrix.getDimension() == 2);
    REQUIRE(matrix.getSize() == 3);

    REQUIRE(matrix.getValue(0, 2) == 3);
    REQUIRE(matrix.getValue(2, 2) == 9);
  }

  SECTION("3D Parameter") {
    auto tensor = ParameterStorage<int>("tensor", config);
    REQUIRE(tensor.getDimension() == 3);
    REQUIRE(tensor.getSize() == 3);

    REQUIRE(tensor.getValue(0, 1, 0) == 10);
    REQUIRE(tensor.getValue(1, 1, 1) == 111);
    REQUIRE(tensor.getValue(2, 0, 1) == 201);
  }

  SECTION("2D Parameter Direct Mapped") {
    auto matrix = ParameterStorageMapped<int>("map_matrix", config);
    REQUIRE(matrix.getDimension() == 2);
    REQUIRE(matrix.getSize() == 3);

    REQUIRE(matrix.getValue(0, 0) == 1);
    REQUIRE(matrix.getValue(1, 1) == 5);
    REQUIRE(matrix.getValue(2, 2) == 5);
    REQUIRE(matrix.getValue(4, 16) == 6);
    REQUIRE(matrix.getValue(16, 4) == 8);
    REQUIRE(matrix.getValue(115, 140) == 9);
  }

  SECTION("1D Parameter Builder") {
    auto storage = ParameterStorageBuilder<int>("map_vector", config);
    auto& matrix = storage.getStorage();
    REQUIRE(matrix.getDimension() == 1);
    REQUIRE(matrix.getSize() == 3);

    REQUIRE(matrix.getValue(0) == 1);
    REQUIRE(matrix.getValue(1) == 2);
    REQUIRE(matrix.getValue(4) == 2);
  }

  SECTION("2D Parameter Builder") {
    auto storage = ParameterStorageBuilder<int>("map_matrix", config);
    auto& matrix = storage.getStorage();
    REQUIRE(matrix.getDimension() == 2);
    REQUIRE(matrix.getSize() == 3);

    REQUIRE(matrix.getValue(0, 0) == 1);
    REQUIRE(matrix.getValue(1, 1) == 5);
    REQUIRE(matrix.getValue(2, 2) == 5);
    REQUIRE(matrix.getValue(4, 16) == 6);
    REQUIRE(matrix.getValue(16, 4) == 8);
    REQUIRE(matrix.getValue(115, 140) == 9);
  }

  SECTION("3D Parameter Builder") {
    auto storage = ParameterStorageBuilder<int>("map_tensor", config);
    auto& matrix = storage.getStorage();
    REQUIRE(matrix.getDimension() == 3);
    REQUIRE(matrix.getSize() == 2);

    REQUIRE(matrix.getValue(0, 0, 0) == 1);
    REQUIRE(matrix.getValue(1, 1, 1) == 8);
    REQUIRE(matrix.getValue(8, 9, 0) == 7);
  }

  SECTION("Const Parameter Builder") {
    auto storage     = ParameterStorageBuilder<double>("const", config);
    auto& constParam = storage.getStorage();
    REQUIRE(constParam.getDimension() == 0);
    REQUIRE(constParam.getSize() == 1);

    REQUIRE(constParam.getValue(0, 0) == 13.37);
    REQUIRE(constParam.getValue(4, 2) == 13.37);
  }

  SECTION("direct const Parameter Builder") {
    auto storage     = ParameterStorageBuilder<int>("const2", config);
    auto& constParam = storage.getStorage();
    REQUIRE(constParam.getDimension() == 0);
    REQUIRE(constParam.getSize() == 1);

    REQUIRE(constParam.getValue(0, 0) == 42);
    REQUIRE(constParam.getValue(4, 2) == 42);
  }
}
