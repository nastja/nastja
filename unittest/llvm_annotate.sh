#!/usr/bin/env bash
profile_file="ut_${2// /_}.profraw"
profile_file="${profile_file//\//_}"
LLVM_PROFILE_FILE="$profile_file" "$@"
