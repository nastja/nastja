/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/format.h"
#include "lib/math/vector.h"
#include <sstream>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Vector3", "[unit][math][class]") {
  SECTION("operator") {
    int array[3]{0, 1, 2};
    Vector3<int> a;
    Vector3<int> b{2, 2, 2};
    Vector3<int> c{1, 2, 3};
    Vector3<int> d{1, 0, -1};
    Vector3<int> e{array};
    Vector3<double> f{b};

    CHECK(b * c == Vector3<int>{2, 4, 6});

    CHECK(f == 2.0);

    CHECK(b == 2);
    CHECK(2 == b);
    CHECK(b != 1);
    CHECK(1 != b);

    CHECK(b != c);
    c += d;
    CHECK(b == c);
    CHECK(a == Vector3<int>::zero);
    CHECK(b / 2 == Vector3<int>::one);

    a = b + c;
    CHECK(a[0] == 4);

    a = b - c;
    CHECK(a[0] == 0);

    a = b * c;
    CHECK(a[0] == 4);

    a = b / c;
    CHECK(a[0] == 1);

    a = b + 2;
    CHECK(a[0] == 4);

    a = b - 2;
    CHECK(a[0] == 0);

    a = b * 2;
    CHECK(a[0] == 4);

    a = b / 2;
    CHECK(a[0] == 1);

    a = 2 + b;
    CHECK(a[0] == 4);

    a = 2 - b;
    CHECK(a[0] == 0);

    a = 2 * b;
    CHECK(a[0] == 4);

    a = 2 / b;
    CHECK(a[0] == 1);

    a = -b;
    CHECK(a[0] == -2);

    b *= 2;
    b -= c;
    CHECK(b == c);

    b += b;
    b /= 2;
    CHECK(b == c);

    b -= 1;
    b *= c;
    CHECK(b == c);

    b += 2;
    b /= c;
    CHECK(b == c);

    auto g = 1.5 * b;
    CHECK(g == 3.0);

    CHECK(g.size() == 3);
  }

  SECTION("operator%") {
    Vector3<int> a{1, 2, 3};
    Vector3<int> a2{1, 2, 3};
    Vector3<int> b{2, 2, 2};
    Vector3<int> r{1, 0, 1};
    auto c = a % b;
    auto d = a % 2;
    CHECK(c == r);
    CHECK(d == r);
    a %= 2;
    CHECK(a == r);
    a2 %= b;
    CHECK(a2 == r);
  }

  SECTION("normalize") {
    Vector3<double> f{5.0, 0.0, 0.0};

    auto g = f.getNormalized();
    CHECK(f[0] == 5.0);
    CHECK(g[0] == 1.0);

    f.normalize();
    CHECK(f[0] == 1.0);

    f.set({2, 2, 0});
    std::stringstream str;
    str << f;
    CHECK(str.str() == "[2,2,0]");
  }

  SECTION("crossProduct") {
    Vector3<double> a{1.0, 0.0, 0.0};
    Vector3<double> b{0.0, 1.0, 0.0};
    Vector3<double> c{0.0, 0.0, 1.0};

    auto d = crossProduct(a, b);
    CHECK(c == d);
    a.crossProduct(b);
    CHECK(a == d);
  }

  SECTION("dotProduct") {
    Vector3<double> a{1.0, 1.0, 0.0};
    Vector3<double> b{0.0, 1.0, 0.0};

    CHECK(dotProduct(a, b) == 1.0);
  }

  SECTION("data") {
    const Vector3<double> a{1.0, 1.0, 0.0};
    Vector3<double> b;

    auto a_ptr = a.data();

    CHECK(a_ptr[0] == 1.0);
    CHECK(a_ptr[2] == 0.0);

    auto b_ptr = b.data();
    b_ptr[0]   = 1.0;
    b_ptr[1]   = 1.0;
    b_ptr[2]   = 1.0;

    CHECK(b == 1.0);
  }

  SECTION("access") {
    const Vector3<double> a{1.0, 2.0, 3.0};
    Vector3<double> b;

    b.x() = a.x();
    b.y() = a.y();
    b.z() = a.z();

    CHECK(a == b);
    CHECK(b.x() == 1.0);
  }

  SECTION("min max") {
    const Vector3<double> a{1.0, 2.0, 3.0};
    const Vector3<double> b{2.0, 2.0, 2.0};

    auto max = maxComponent(a, b);
    auto min = minComponent(a, b);

    CHECK(min.x() == 1.0);
    CHECK(max.x() == 2.0);
    CHECK(min.z() == 2.0);
    CHECK(max.z() == 3.0);
  }

  SECTION("print") {
    Vector3<int> a({1, 0, 2});
    std::stringstream str;
    str << a;
    CHECK(str.str() == "[1,0,2]");
    CHECK(fmt::format("{}", a) == "[1,0,2]");
  }
}
