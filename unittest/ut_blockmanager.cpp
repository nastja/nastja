/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;
using namespace nastja::stencil;

extern MPIData mpiData;

TEST_CASE("Blockmanager calculations", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[20, 20, 20], "blockcount":[10, 10, 10]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  SECTION("getRelativeDifference") {
    REQUIRE(std::get<0>(blockmanager.getRelativeDifference(14, 15)) == 1);
    REQUIRE(std::get<1>(blockmanager.getRelativeDifference(14, 15)) == 0);
    REQUIRE(std::get<2>(blockmanager.getRelativeDifference(14, 15)) == 0);

    REQUIRE(std::get<0>(blockmanager.getRelativeDifference(15, 14)) == -1);

    REQUIRE(std::get<0>(blockmanager.getRelativeDifference(9, 0)) == -9);
  }

  SECTION("getBlocks") {
    REQUIRE(blockmanager.getBlocks(0) == 10);
    REQUIRE(blockmanager.getBlocks(1) == 10);
    REQUIRE(blockmanager.getBlocks(2) == 10);
  }

  SECTION("getIDFromCoordinates") {
    REQUIRE(blockmanager.getIDFromCoordinates(0, 0, 0) == 0);
    REQUIRE(blockmanager.getIDFromCoordinates(1, 0, 0) == 1);
    REQUIRE(blockmanager.getIDFromCoordinates(0, 1, 0) == 10);
    REQUIRE(blockmanager.getIDFromCoordinates(0, 0, 1) == 100);
    REQUIRE(blockmanager.getIDFromCoordinates(1, 1, 1) == 111);
  }

  SECTION("getCoordinatesFromID") {
    REQUIRE(blockmanager.getCoordinatesFromID(0) == std::make_tuple(0, 0, 0));
    REQUIRE(blockmanager.getCoordinatesFromID(1) == std::make_tuple(1, 0, 0));
    REQUIRE(blockmanager.getCoordinatesFromID(10) == std::make_tuple(0, 1, 0));
    REQUIRE(blockmanager.getCoordinatesFromID(100) == std::make_tuple(0, 0, 1));
    REQUIRE(blockmanager.getCoordinatesFromID(111) == std::make_tuple(1, 1, 1));
  }

  SECTION("isInsideStar") {
    REQUIRE(blockmanager.isInsideStar(0, 0, 0) == true);
    REQUIRE(blockmanager.isInsideStar(2, 0, 0) == true);
    REQUIRE(blockmanager.isInsideStar(1, 1, 1) == true);
    REQUIRE(blockmanager.isInsideStar(0, 0, 3) == false);

    // Count all cells inside of the star
    int cnt = 0;
    for (int x = -3; x <= 3; x++) {
      for (int y = -3; y <= 3; y++) {
        for (int z = -3; z <= 3; z++) {
          if (blockmanager.isInsideStar(x, y, z))
            cnt++;
        }
      }
    }
    REQUIRE(cnt == 125);

    REQUIRE(blockmanager.isInsideStar(blockmanager.getRelativeDifference(42, 43)) == true);
    REQUIRE(blockmanager.isInsideStar(blockmanager.getRelativeDifference(23, 26)) == false);
  }

  SECTION("getRelativeBlockID") {
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::L) == 41);
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::R) == 43);
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::D) == 32);
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::U) == 52);
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::B) == 942);
    REQUIRE(blockmanager.getRelativeBlockID(42, Direction::F) == 142);

    REQUIRE(blockmanager.getRelativeBlockID(42, -1, 0, 0) == 41);
    REQUIRE(blockmanager.getRelativeBlockID(42, 1, 0, 0) == 43);
    REQUIRE(blockmanager.getRelativeBlockID(42, 0, -1, 0) == 32);
    REQUIRE(blockmanager.getRelativeBlockID(42, 0, 1, 0) == 52);
    REQUIRE(blockmanager.getRelativeBlockID(42, 0, 0, -1) == 942);
    REQUIRE(blockmanager.getRelativeBlockID(42, 0, 0, 1) == 142);

    REQUIRE(blockmanager.getRelativeBlockID(0, Direction::L) == 9);
  }
}
