/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/communication/messagemanager.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;

extern MPIData mpiData;

namespace nastja {

class TESTING_MessageManager {
public:
  TESTING_MessageManager(MessageManager& messageManager) : messagemanager_(messageManager) {}

  int getIndex(const std::vector<int>& coordinate, const std::vector<int>& size) {
    return messagemanager_.getIndex(coordinate, size);
  }

  std::vector<int> getCoordinate(const int index, const std::vector<int>& size) {
    return messagemanager_.getCoordinate(index, size);
  }

  std::vector<int> getDimensions(const int dimension) {
    return messagemanager_.getDimensions(dimension);
  }

  int getLower(const std::vector<int>& coord, const int d, const std::vector<int>& n) {
    return messagemanager_.getLower(coord, d, n);
  }

  int getUpper(const std::vector<int>& coord, const int d, const std::vector<int>& n) {
    return messagemanager_.getUpper(coord, d, n);
  }

  void setCores(const int cores) { messagemanager_.totalCores_ = cores; }

private:
  MessageManager& messagemanager_;
};

}  // namespace nastja

TEST_CASE("MessageManager calculations", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[20, 20, 20], "blockcount":[10, 10, 10]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);
  MessageManager messagemanager(blockmanager);

  TESTING_MessageManager mmtesting(messagemanager);

  SECTION("Manhattan Street Network dimensions") {
    mmtesting.setCores(120);
    std::vector<int> size = mmtesting.getDimensions(4);
    REQUIRE(size[0] == 4);
    REQUIRE(size[1] == 4);
    REQUIRE(size[2] == 3);
    REQUIRE(size[3] == 3);
  }

  SECTION("get neighbors") {
    mmtesting.setCores(32);
    std::vector<int> n     = mmtesting.getDimensions(3);
    std::vector<int> coord = mmtesting.getCoordinate(0, n);
    REQUIRE(mmtesting.getLower(coord, 0, n) == 3);
    REQUIRE(mmtesting.getUpper(coord, 0, n) == 1);
    REQUIRE(mmtesting.getLower(coord, 1, n) == 8);
    REQUIRE(mmtesting.getUpper(coord, 1, n) == 4);
    REQUIRE(mmtesting.getLower(coord, 2, n) == 24);
    REQUIRE(mmtesting.getUpper(coord, 2, n) == 12);
  }

  SECTION("Coordinate/Index") {
    std::vector<int> size({5, 4, 3, 2});
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(31, size), size) == 31);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(64, size), size) == 64);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(36, size), size) == 36);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(30, size), size) == 30);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(60, size), size) == 60);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(10, size), size) == 10);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(8, size), size) == 8);
    REQUIRE(mmtesting.getIndex(mmtesting.getCoordinate(0, size), size) == 0);
  }
}
