/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/boundingbox.h"
#include "lib/math/format.h"
#include <sstream>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("BoundingBox", "[unit][math][class]") {
  SECTION("operator") {
    Vector3<long> v0{1, 1, 1};
    Vector3<long> v1{5, 5, 5};
    BoundingBox<long> a(v0, v1);
    BoundingBox<long> b(v1, v0);

    CHECK(a == b);

    CHECK(a.isInside(v0) == true);
    CHECK(a.isInside(v1) == false);
    CHECK(a.isInside(10, 2, 2) == false);

    b.translate(1, 2, 3);

    std::stringstream sstr;
    sstr << b;
    CHECK(sstr.str() == "[[2,3,4],[6,7,8]]");

    CHECK(fmt::format("{}", b) == "[[2,3,4],[6,7,8]]");

    a.intersect(b);

    sstr.str("");
    sstr << a;
    CHECK(sstr.str() == "[[2,3,4],[5,5,5]]");

    CHECK(fmt::format("{}", a) == "[[2,3,4],[5,5,5]]");

    auto c = intersect(a, b);
    CHECK(a == c);

    c.intersect(a.translate(10, 10, 10));

    CHECK(c.empty());

    a = b;
    b.translate(-v1).translate(v1);
    CHECK(a == b);
  }

  SECTION("border distance") {
    Vector3<long> v0{1, 1, 1};
    Vector3<long> v1{6, 6, 6};
    Vector3<long> p{2, 3, 4};
    BoundingBox<long> bb(v0, v1);

    auto toBorder = borderDistancesSqr(bb, p);

    CHECK(toBorder[0] == 1);
    CHECK(toBorder[1] == 9);
    CHECK(toBorder[2] == 4);
    CHECK(toBorder[3] == 4);
    CHECK(toBorder[4] == 9);
    CHECK(toBorder[5] == 1);
    CHECK(toBorder[6] == 5);
    CHECK(toBorder[25] == 14);
  }

  SECTION("periodic isInside") {
    BoundingBox<long> domain({0, 0, 0}, {100, 100, 100});
    BoundingBox<long> left({-1, 0, 0}, {1, 100, 100});
    left.extend(10);

    CHECK(left.isInsidePeriodic(domain, {1, 1, 1}));
    CHECK(left.isInsidePeriodic(domain, {95, 1, 1}));
  }
}
