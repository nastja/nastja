/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/storage/sparestorage.h"

using namespace nastja;

TEST_CASE("Spare Storage", "[unit][class]") {
  SECTION("Default Value") {
    auto test1 = SpareStorage<int>();
    auto test3 = SpareStorage<double>();

    REQUIRE(test1[0] == 0);
    REQUIRE(test3[4] == 0.0);
  }

  SECTION("Set Values") {
    auto test = SpareStorage<int>();

    test[1] = 1;
    test[2] = 2;
    REQUIRE(test(1) == 1);
    REQUIRE(test(2) == 2);

    int sum = 0;
    for (const auto& a : test) {
      sum += a.second;
    }

    REQUIRE(sum == 3);
  }

  SECTION("Add Values") {
    auto test = SpareStorage<int>();

    test[1] = 1;
    test[2] = 2;

    test[1] += 1;
    test[2] += -1;
    test[3] += 9;

    REQUIRE(test(1) == 2);
    REQUIRE(test(2) == 1);
    REQUIRE(test(3) == 9);
  }

  SECTION("Shrink Storage") {
    auto test = SpareStorage<int>();

    test[1] = 1;
    test[2] = 2;
    test[3] = 3;
    REQUIRE(test.size() == 3);
    test[1] += -1;
    test[2] = 0;
    REQUIRE(test.size() == 3);
    test.shrinkStorage();
    REQUIRE(test.size() == 1);
  }
}
