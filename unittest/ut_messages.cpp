/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/communication/message.h"

using namespace nastja;

TEST_CASE("Messages pack/unpack", "[unit][class]") {
  SECTION("basics") {
    std::vector<int> data;
    int int_in1 = 42, int_in2 = 23, int_out1, int_out2;

    Message::append(data, MessageID::newComm, int_in1);
    Message::read(data, MessageID::newComm, int_out1);
    REQUIRE(int_in1 == int_out1);

    int_out1 = -1;
    REQUIRE(Message::read(data, MessageID::newBlocks, int_out1) == false);
    REQUIRE(int_out1 == -1);

    Message::append(data, MessageID::newBlocks, int_in1, int_in2);
    Message::read(data, MessageID::newBlocks, int_out1, int_out2);
    REQUIRE(int_in1 == int_out1);
    REQUIRE(int_in2 == int_out2);
  }

  SECTION("vector") {
    std::vector<int> data;
    std::vector<int> vec, vec2;
    vec.push_back(42);
    vec.push_back(23);
    vec.push_back(84);
    Message::append(data, MessageID::newComm, vec);
    Message::read(data, MessageID::newComm, vec2);
    REQUIRE(vec2[0] == vec[0]);
    REQUIRE(vec2[1] == vec[1]);
    REQUIRE(vec2[2] == vec[2]);
  }

  SECTION("set") {
    std::vector<int> data;
    std::set<int> set, set2;
    set.insert(42);
    set.insert(23);
    set.insert(84);
    Message::append(data, MessageID::newComm, set);
    Message::read(data, MessageID::newComm, set2);
    REQUIRE(set2 == set);
  }

  SECTION("map of vectors") {
    std::vector<int> data;
    std::map<int, std::vector<int>> map, map2;
    map[1].push_back(42);
    map[2].push_back(23);
    map[2].push_back(84);
    Message::append(data, MessageID::newComm, map);
    Message::read(data, MessageID::newComm, map2);
    REQUIRE(map2[1][0] == map[1][0]);
    REQUIRE(map2[2][0] == map[2][0]);
    REQUIRE(map2[2][1] == map[2][1]);
  }
}
