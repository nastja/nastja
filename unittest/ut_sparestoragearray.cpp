/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/storage/sparestorage.h"

using namespace nastja;

template <typename T>
struct TD;

TEST_CASE("Spare Storage Array", "[unit][class]") {
  SECTION("Default Value") {
    auto test1 = SpareStorageArray<int, 2>();
    auto test3 = SpareStorageArray<double, 3>();

    REQUIRE(test1[0][0] == 0);
    REQUIRE(test3[4][0] == 0.0);
  }

  SECTION("Set Values") {
    auto test = SpareStorageArray<int, 3>();

    test[1] = {1};
    test[2] = {1, 2, 3};
    REQUIRE(test(1)[0] == 1);
    REQUIRE(test(2)[1] == 2);
    REQUIRE(test(2)[2] == 3);

    int sum = 0;
    for (const auto& a : test) {
      sum += a.second[0];
    }

    REQUIRE(sum == 2);

    for (auto& a : test) {
      // TD<decltype(a)> td;
      a.second[0] *= 2;
    }

    sum = 0;
    for (const auto& a : test) {
      sum += a.second[0];
    }

    REQUIRE(sum == 4);
  }

  SECTION("Add Values") {
    auto test = SpareStorageArray<int, 2>();

    std::array<int, 2> array1 = {2, 2};
    std::array<int, 2> array2 = {-1, -1};

    test[1] = {1, 1};
    test[2] = array1;

    test[1] += {1, 1};
    test[2] += array2;
    test[3] += {0, 9};

    REQUIRE(test(1)[0] == 2);
    REQUIRE(test(1)[1] == 2);
    REQUIRE(test(2)[0] == 1);
    REQUIRE(test(3)[1] == 9);
  }

  SECTION("Shrink Storage") {
    auto test = SpareStorageArray<int, 2>();

    test[1] = {1, 1};
    test[3] = {1, 3};
    REQUIRE(test.size() == 2);
    test[1] += {-1, -1};
    test[3] = {0, 0};
    REQUIRE(test.size() == 2);
    test.shrinkStorage();
    REQUIRE(test.size() == 0);
  }

  SECTION("Clear Storage") {
    auto test = SpareStorageArray<int, 2>();

    test[1] = {1, 1};
    test[3] = {1, 3};
    REQUIRE(test.size() == 2);
    test.clear();
    REQUIRE(test.size() == 0);
  }
}
