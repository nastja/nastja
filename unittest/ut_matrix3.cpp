/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/format.h"
#include "lib/math/matrix3.h"
#include <sstream>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Rotation Matrix3", "[unit][class][math]") {
  SECTION("operator") {
    double array[9]{0, 1, 2, 3, 4, 5, 6, 7, 8};
    Vector3<double> e1{1.0, 0.0, 0.0};
    Vector3<double> e2{0.0, 1.0, 0.0};
    Vector3<double> e3{0.0, 0.0, 1.0};

    Matrix3<int> a;
    Matrix3<int> b(2);
    Matrix3<int> c(1, 2, 3, 4, 5, 6, 7, 8, 9);
    Matrix3<double> d(e1, e2, e3);
    Matrix3<double> e(array);
    Matrix3<double> f(b);
    Matrix3<double> g{};
    g.set(1, 2, 3, 4, 5, 6, 7, 8, 9);

    CHECK(c == g);

    CHECK(f == 2.0);
    f *= 0.5;
    CHECK(f == 1.0);
    e += f;
    CHECK(e == c);

    CHECK(b == 2);
    CHECK(2 == b);
    CHECK(b != 1);
    CHECK(1 != b);

    CHECK(b != c);

    a = b + c;
    CHECK(a[0] == 3);

    a = b - c;
    CHECK(a[0] == 1);

    a = b * 2;
    CHECK(a[0] == 4);

    a = b / 2;
    CHECK(a[0] == 1);

    a = -b;
    CHECK(a[0] == -2);

    a /= 2;
    a -= b;
    CHECK(a == -3);

    auto h = a * d;
    CHECK(h(1, 1) == -3);

    h = -1 * h;
    CHECK(h(1, 1) == 3);

    h *= d;

    b[8] = 1;
    CHECK(b != 2);
    f *= 2;
    CHECK(b != f);
  }

  SECTION("data") {
    const Matrix3<double> a(1.0);
    Matrix3<double> b;

    auto a_ptr = a.data();

    CHECK(a_ptr[0] == 1.0);
    CHECK(a(0, 0) == 1.0);

    auto b_ptr = b.data();
    b_ptr[0]   = 1.0;
    b_ptr[1]   = 1.0;
    b_ptr[2]   = 1.0;
    b_ptr[3]   = 1.0;
    b_ptr[4]   = 1.0;
    b_ptr[5]   = 1.0;
    b_ptr[6]   = 1.0;
    b_ptr[7]   = 1.0;
    b_ptr[8]   = 1.0;

    CHECK(b == 1.0);
  }

  SECTION("print") {
    Matrix3<int> a(1, 0, 0, 0, 1, 0, 0, 0, 1);
    std::stringstream str;
    str << a;
    CHECK(str.str() == "[[1,0,0]\n [0,1,0]\n [0,0,1]]");
    CHECK(fmt::format("{}", a) == "[[1,0,0],\n [0,1,0],\n [0,0,1]]");
  }

  Matrix3<double> rotationx(0.5 * M_PI, 0.0, 0.0);
  Matrix3<double> rotationy(0.0, 0.5 * M_PI, 0.0);
  Matrix3<double> rotationz(0.0, 0.0, 0.5 * M_PI);
  Matrix3<double> rotation(0.5 * M_PI, 0.5 * M_PI, 0.5 * M_PI);

  SECTION("rotatex") {
    Vector3<double> v{0.0, 1.0, 0.0};
    v = rotationx * v;
    REQUIRE(v[0] == Approx(0.0));
    REQUIRE(v[1] == Approx(0.0).margin(1e-16));
    REQUIRE(v[2] == Approx(1.0));
  }

  SECTION("rotatey") {
    Vector3<double> v{1.0, 1.0, 0.0};
    v = rotationy * v;
    REQUIRE(v[0] == Approx(0.0).margin(1e-16));
    REQUIRE(v[1] == Approx(1.0));
    REQUIRE(v[2] == Approx(-1.0));
  }

  SECTION("rotatez") {
    Vector3<double> v{1.0, 1.0, 0.0};
    v = rotationz * v;
    REQUIRE(v[0] == Approx(-1.0));
    REQUIRE(v[1] == Approx(1.0));
    REQUIRE(v[2] == Approx(0.0));
  }

  SECTION("rotate") {
    Vector3<double> v{0.0, 1.0, 0.0};
    v = rotation * v;
    REQUIRE(v[0] == Approx(0.0));
    REQUIRE(v[1] == Approx(1.0));
    REQUIRE(v[2] == Approx(0.0).margin(1e-16));
  }

  SECTION("rotate-x") {
    Vector3<double> v{0.0, 0.0, 1.0};
    v = rotationx.multiplyT(v);
    REQUIRE(v[0] == Approx(0.0));
    REQUIRE(v[1] == Approx(1.0));
    REQUIRE(v[2] == Approx(0.0).margin(1e-16));
  }

  SECTION("rotate-axis") {
    Vector3<double> axis{0.0, 0.0, 1.0};
    Matrix3<double> rot{axis, 0.5 * M_PI};
    Vector3<double> v{1.0, 0.0, 0.0};
    v = rot * v;
    REQUIRE(v[0] == Approx(0.0).margin(1e-16));
    REQUIRE(v[1] == Approx(1.0));
    REQUIRE(v[2] == Approx(0.0));
  }
}
