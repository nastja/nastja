/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/polynomial.h"

using namespace nastja;

TEST_CASE("polynomial", "[unit][math]") {
  SECTION("calc") {
    math::Polynomial p{2.0, 3.0, 4.0};

    REQUIRE(p.calc(0.0) == 2.0);
    REQUIRE(p.calc(1.0) == 9.0);
    REQUIRE(p.calc(-1.0) == 3.0);
  }

  SECTION("diff") {
    math::Polynomial p{0.0, 2.0, 3.0};

    REQUIRE(p.calcDerivation(1, 1.0) == 8.0);
    REQUIRE(p.calcDerivation(2, 1.0) == 6.0);
  }

  SECTION("int") {
    math::Polynomial p{0.0, 2.0, 3.0};

    REQUIRE(p.calcIntegral(1.0) == 2.0);
    REQUIRE(p.calcIntegral(-1.0) == 0.0);
    REQUIRE(p.calcIntegral(2.0) == 12.0);
  }
}
