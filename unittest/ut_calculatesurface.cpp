/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/datatypes.h"
#include "lib/math/calculatecubes.h"
#include "lib/math/calculatesurface.h"

using namespace nastja;

TEST_CASE("calculatesurface", "[unit][math]") {
  SECTION("stencil") {
    double surface;
    std::array<cellid_t, 27> stencil{};

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == 0.0);

    surface = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == 0.0);

    double singleTriangle = 0.21650635094610965;
    stencil[26]           = 1;
    surface               = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == singleTriangle);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == singleTriangle);

    stencil[0] = 1;
    surface    = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == 2 * singleTriangle);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == 2 * singleTriangle);

    stencil[0]  = 0;
    stencil[26] = 0;
    stencil[18] = 1;
    stencil[20] = 1;

    surface = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == 2 * singleTriangle);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == 2 * singleTriangle);

    double oneLongSide = 0.7071067811865476;
    stencil[19]        = 1;

    surface = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == 2 * oneLongSide);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == 2 * oneLongSide);

    stencil[18] = 0;
    stencil[19] = 0;
    stencil[20] = 0;
    stencil[5]  = 1;
    stencil[8]  = 1;
    stencil[7]  = 1;

    double case5 = 1.149519052838329;

    surface = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == case5 + 2 * singleTriangle);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == case5 + 2 * singleTriangle);

    //stencil[17]  = 1;
    // double case9 = 1.299038105676658;

    //surface = math::calcSurf(stencil, 0);
    //REQUIRE(surface == case9 + 3 * singleTriangle);
    //
    //surface = math::calcSurf(stencil, 1);
    //REQUIRE(surface == case9 + 3 * singleTriangle);

    stencil[0] = 1;
    stencil[1] = 1;
    stencil[2] = 1;
    stencil[3] = 1;
    stencil[4] = 1;
    stencil[6] = 1;

    surface = math::calcSurfaceMC(stencil, 0);
    REQUIRE(surface == 4.0);

    surface = math::calcSurfaceMC(stencil, 1);
    REQUIRE(surface == 4.0);

    /*    stencil[0]  = 0;
    stencil[1]  = 0;
    stencil[2]  = 0;
    stencil[3]  = 0;
    stencil[4]  = 1;
    stencil[5]  = 0;
    stencil[6]  = 1;
    stencil[7]  = 1;
    stencil[8]  = 0;
    stencil[15] = 1;

    double case10 = 1.573132184971;

    surface = math::calcSurf(stencil, 0);
    REQUIRE(surface == case10 + oneLongSide + 3 * singleTriangle);

    surface = math::calcSurf(stencil, 1);
    REQUIRE(surface == case10 + oneLongSide + 3 * singleTriangle);*/
  }
}
