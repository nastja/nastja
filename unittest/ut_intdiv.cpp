/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/arithmetic.h"

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Integer division", "[unit][math]") {
  SECTION("positive positive") {
    REQUIRE(divRoundClosest(0, 1) == 0);
    REQUIRE(divRoundClosest(11, 4) == 3);
    REQUIRE(divRoundClosest(4, 11) == 0);
    REQUIRE(divRoundClosest(10, 4) == 3);
    REQUIRE(divRoundClosest(9, 4) == 2);
    REQUIRE(divRoundClosest(7, 4) == 2);
  }

  SECTION("negative positive") {
    REQUIRE(divRoundClosest(-0, 1) == -0);
    REQUIRE(divRoundClosest(-11, 4) == -3);
    REQUIRE(divRoundClosest(-4, 11) == -0);
    REQUIRE(divRoundClosest(-10, 4) == -3);
    REQUIRE(divRoundClosest(-9, 4) == -2);
    REQUIRE(divRoundClosest(-7, 4) == -2);
  }

  SECTION("positive negative") {
    REQUIRE(divRoundClosest(0, -1) == -0);
    REQUIRE(divRoundClosest(11, -4) == -3);
    REQUIRE(divRoundClosest(4, -11) == -0);
    REQUIRE(divRoundClosest(10, -4) == -3);
    REQUIRE(divRoundClosest(9, -4) == -2);
    REQUIRE(divRoundClosest(7, -4) == -2);
  }

  SECTION("negative negative") {
    REQUIRE(divRoundClosest(-0, -1) == 0);
    REQUIRE(divRoundClosest(-11, -4) == 3);
    REQUIRE(divRoundClosest(-4, -11) == 0);
    REQUIRE(divRoundClosest(-10, -4) == 3);
    REQUIRE(divRoundClosest(-9, -4) == 2);
    REQUIRE(divRoundClosest(-7, -4) == 2);
  }
}
