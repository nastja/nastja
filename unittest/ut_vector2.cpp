/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/format.h"
#include "lib/math/vector.h"
#include <sstream>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Vector2", "[unit][math][class]") {
  SECTION("operator") {
    int array[2]{0, 1};
    Vector2<int> a;
    Vector2<int> b{2, 2};
    Vector2<int> c{1, 2};
    Vector2<int> d{1, 0};
    Vector2<int> e{array};
    Vector2<double> f{b};

    CHECK(b * c == Vector2<int>{2, 4});

    CHECK(f == 2.0);

    CHECK(b == 2);
    CHECK(2 == b);
    CHECK(b != 1);
    CHECK(1 != b);

    CHECK(b != c);
    c += d;
    CHECK(b == c);
    CHECK(a == Vector2<int>::zero);
    CHECK(b / 2 == Vector2<int>::one);

    a = b + c;
    CHECK(a[0] == 4);

    a = b - c;
    CHECK(a[0] == 0);

    a = b * 2;
    CHECK(a[0] == 4);

    a = b / 2;
    CHECK(a[0] == 1);

    a = -b;
    CHECK(a[0] == -2);

    b *= 2;
    b -= c;
    CHECK(b == c);

    b += b;
    b /= 2;
    CHECK(b == c);

    auto g = 1.5 * b;
    CHECK(g == 3.0);

    CHECK(g.size() == 2);
  }

  SECTION("normalize") {
    Vector2<double> f{5.0, 0.0};

    auto g = f.getNormalized();
    CHECK(f[0] == 5.0);
    CHECK(g[0] == 1.0);

    f.normalize();
    CHECK(f[0] == 1.0);

    f.set({2, 2});
    std::stringstream str;
    str << f;
    CHECK(str.str() == "[2,2]");
  }

  SECTION("dotProduct") {
    Vector2<double> a{1.0, 1.0};
    Vector2<double> b{0.0, 1.0};

    CHECK(dotProduct(a, b) == 1.0);
  }

  SECTION("data") {
    const Vector2<double> a{1.0, 2.0};
    Vector2<double> b;

    auto a_ptr = a.data();

    CHECK(a_ptr[0] == 1.0);
    CHECK(a_ptr[1] == 2.0);

    auto b_ptr = b.data();
    b_ptr[0]   = 1.0;
    b_ptr[1]   = 1.0;

    CHECK(b == 1.0);
  }

  SECTION("access") {
    const Vector2<double> a{1.0, 2.0};
    Vector2<double> b;

    b.x() = a.x();
    b.y() = a.y();

    CHECK(a == b);
    CHECK(b.x() == 1.0);
  }

  SECTION("print") {
    Vector2<int> a({1, 0});
    std::stringstream str;
    str << a;
    CHECK(str.str() == "[1,0]");
    CHECK(fmt::format("{}", a) == "[1,0]");
  }
}
