/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;

extern MPIData mpiData;

TEST_CASE("Blockneighbor calculations", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[20, 20, 20], "blockcount":[4, 4, 4]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  SECTION("getOneNeighbor") {
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 1) & 0b111111) == 0b10);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(1, 0) & 0b111111) == 0b1);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(1, 5) & 0b111111) == 0b1000);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(1, 13) & 0b111111) == 0b100);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(14, 2) & 0b111111) == 0b1000);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 16) & 0b111111) == 0b100000);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 48) & 0b111111) == 0b10000);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 32) & 0b111111) == 0);
  }

  SECTION("getMoreNeighbors") {
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 5) & 0b111111) == 0b1010);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 6) & 0b111111) == 0);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(5, 16) & 0b111111) == 0b100101);
  }

  SECTION("periodic") {
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 3) & 0b111 << 6) == 0b1 << 6);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(2, 14) & 0b111 << 6) == 0b10 << 6);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 6) & 0b111 << 6) == 0);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(5, 53) & 0b111 << 6) == 0b100 << 6);
    REQUIRE((blockmanager.getBlockSet().begin()->second->getNeighborDirection(0, 63) & 0b111 << 6) == 0b111 << 6);
  }
}
