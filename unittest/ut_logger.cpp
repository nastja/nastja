/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/logger.h"

using namespace nastja;

TEST_CASE("Logger", "[unit]") {
  auto& logger = logger::get();

  SECTION("Level1") {
    logger.trace("Trace");
    logger.trace(logger::Category::DynBlock, "Trace");
    logger.debug("Debug");
    logger.info("Info");
    logger.warning("Warning");
    logger.error("Error");
    logger.onelog("Output message.");
  }

  SECTION("Level2") {
    logger.setLevel(logger::Level::debug);
    CHECK_THAT(logger.getLevelName(), Catch::Equals("debug"));

    logger.setLevel(logger::Level::trace);
    logger.trace("Trace");
    logger.trace(logger::Category::DynBlock, "Trace");
    logger.debug("Debug");
    logger.info("Info");
    logger.warning("Warning");
    logger.error("Error");
    logger.log("Just logging");
  }

  SECTION("rank0") {
    logger.setLevel(logger::Level::trace);
    logger.onetrace("Trace");
    logger.onetrace(logger::Category::DynBlock, "Trace");
    logger.onedebug("Debug");
    logger.oneinfo("Info");
    logger.onewarning("Warning");
    logger.oneerror("Error");
  }

  SECTION("timestep") {
    unsigned long timestep{0};
    logger.registerTimeStepPtr(&timestep);
    logger.setLevel(logger::Level::trace);
    logger.trace(logger::Category::DynBlock, "Trace");
    timestep = 42;
    logger.trace(logger::Category::DynBlock, "Warning");
  }

  SECTION("dual logging") {
    logger::registerLogger("nocolor", std::cerr);
    auto& nocolor = logger::get("nocolor");
    nocolor.setColoredOutput(false);

    logger.warning("Color");
    nocolor.warning("Nocolor logging to std::cerr");
  }

  SECTION("file logging") {
    logger::registerLogger("filelog", "./logger.log");
    REQUIRE(logger::hasLogger("filelog") == true);
    auto& filelog = logger::get("filelog");
    filelog.setFormat(logger::Level::warning, "");
    filelog.warning("To file");
    filelog.flush();

    logger::closeLogger("filelog");
  }
}
