/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/archive.h"
#include "lib/storage/sparestorage.h"
#include "absl/container/flat_hash_map.h"
#include <map>

using namespace nastja;

TEST_CASE("Archive", "[unit][class]") {
  SECTION("SpareStorageArray") {
    auto test = SpareStorageArray<int, 3>();

    test[1] = {1};
    test[2] = {1, 2, 3};

    Archive ar;

    test.pack(ar);

    test.clear();
    REQUIRE(test.size() == 0);
    ar.reset();

    test.unpack(ar);

    REQUIRE(test(1)[0] == 1);
    REQUIRE(test(2)[1] == 2);
    REQUIRE(test(2)[2] == 3);
  }

  SECTION("SpareStorage") {
    auto test = SpareStorage<int>();

    test[1] = 2;
    test[2] = 1;

    Archive ar;

    test.pack(ar);

    test.clear();
    REQUIRE(test.size() == 0);
    ar.reset();

    test.unpack(ar);

    REQUIRE(test(1) == 2);
    REQUIRE(test(2) == 1);
  }

  SECTION("String") {
    std::string test1 = "Hello";
    std::string test2 = "42";
    std::string test1r;
    std::string test2r;

    Archive ar;
    ar.pack(test1);
    ar.pack(test2);

    ar.reset();

    ar.unpack(test1r);
    ar.unpack(test2r);

    REQUIRE(test1 == test1r);
    REQUIRE(test2 == test2r);
  }

  SECTION("Map") {
    std::map<cellid_t, real_t> map;
    map[5]         = 20.;
    map[6]         = 20.;
    map[7]         = 20.;
    map[50]        = 20.;
    map[204751654] = 20.;
    map[5] += 2.;
    for (int i = 0; i < 10000; i++) { map[i + 51] += 28.6; }

    Archive ar;
    ar.pack(map);
    ar.reset();
    std::map<cellid_t, real_t> unpackMap;
    ar.unpack(unpackMap);

    REQUIRE(unpackMap[5] == 22.);
    REQUIRE(unpackMap[6] == 20.);
    REQUIRE(unpackMap[7] == 20.);
    REQUIRE(unpackMap[50] == 20.);
    REQUIRE(unpackMap[204751654] == 20.);
  }

  SECTION("HashMap") {
    absl::flat_hash_map<cellid_t, real_t> map;
    map[5]         = 20.;
    map[6]         = 20.;
    map[7]         = 20.;
    map[50]        = 20.;
    map[204751654] = 20.;
    map[5] += 2.;
    for (int i = 0; i < 10000; i++) { map[i + 51] += 28.6; }

    Archive ar;
    ar.pack(map);
    ar.reset();
    absl::flat_hash_map<cellid_t, real_t> unpackMap;
    ar.unpack(unpackMap);

    REQUIRE(unpackMap[5] == 22.);
    REQUIRE(unpackMap[6] == 20.);
    REQUIRE(unpackMap[7] == 20.);
    REQUIRE(unpackMap[50] == 20.);
    REQUIRE(unpackMap[204751654] == 20.);
  }

  SECTION("Unkown Size") {
    Archive ar;
    ar.pack(1);
    ar.startArray(0);
    ar.pack(2);
    ar.pack(3);
    ar.pack(4);
    ar.endArray(3);
    ar.pack(5);
    ar.reset();
    int val1;
    int val2;
    std::vector<int> v;
    ar.unpack(val1);
    ar.unpack(v);
    ar.unpack(val2);

    REQUIRE(v[0] == 2);
    REQUIRE(v[1] == 3);
    REQUIRE(v[2] == 4);
    REQUIRE(val1 == 1);
    REQUIRE(val2 == 5);
  }
}
