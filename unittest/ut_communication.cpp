/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/communication/communicator.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;

extern MPIData mpiData;

namespace nastja {

class TESTING_Communicator {
public:
  TESTING_Communicator(Communicator& comm) : comm_(comm) {}
  int calcSendState(int a) { return comm_.calcSendState(a); }

private:
  Communicator& comm_;
};

}  // namespace nastja

TEST_CASE("Communication calculations", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[20, 20, 20], "blockcount":[10, 10, 10]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);
  CommManager commmanager(blockmanager);
  Communicator comm(blockmanager, commmanager);

  TESTING_Communicator commTesting(comm);

  SECTION("sendState") {
    REQUIRE(commTesting.calcSendState(0x31) == 0x11);
    REQUIRE(commTesting.calcSendState(0x64) == 0x11);
    REQUIRE(commTesting.calcSendState(0x36) == 0x01);
    REQUIRE(commTesting.calcSendState(0x30) == 0x10);
    REQUIRE(commTesting.calcSendState(0x60) == 0x10);
    REQUIRE(commTesting.calcSendState(0x10) == 0x10);
    REQUIRE(commTesting.calcSendState(0x08) == 0x00);
    REQUIRE(commTesting.calcSendState(0x00) == 0x00);
  }
}
