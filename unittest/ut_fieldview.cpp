/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/datatypes.h"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/field/field.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;
using namespace nastja::field;
using namespace nastja::stencil;

extern MPIData mpiData;

TEST_CASE("Fieldview vector length 1", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[8, 3, 3], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  FieldProperties fieldProperties{stencil::D3C27{}, FieldType::real | FieldType::aligned};
  fieldProperties.setAlignment(4);
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t, 1> f1{*(blockmanager.getBlockSet().begin()->second), fieldProperties};
  field::Field<real_t, 4> f4{*(blockmanager.getBlockSet().begin()->second), fieldProperties};

  auto* f1ptr     = f1.getCellBasePtr();
  auto* f1ptr_old = f1.getCellBasePtr(1);
  auto* f4ptr     = f4.getCellBasePtr();
  auto* f4ptr_old = f4.getCellBasePtr(1);

  for (unsigned int index = 0; index < 10 * 5 * 5; index++) {
    f1ptr[index]     = index;
    f1ptr_old[index] = index;
    f4ptr[index]     = index;
    f4ptr_old[index] = index;
  }

  /*
  Index for N=1, 4, value:
  +-----------+ +-----------+ +-----------+
  |69 70 71 72| |73 74 75 76| |77 78 79 80|
  +-----------+ +-----------+ +-----------+
  */

  SECTION("getIndex for N=1") {
    REQUIRE(f1.getIndex(0, 1, 1) == 72);
    REQUIRE(f1.getIndex(1, 1, 1) == 73);
    REQUIRE(f1.getIndex(2, 1, 1) == 74);
    REQUIRE(f1.getIndex(3, 1, 1) == 75);
    REQUIRE(f1.getIndex(4, 1, 1) == 76);
    REQUIRE(f1.getIndex(5, 1, 1) == 77);
    REQUIRE(f1.getCell(f1.getIndex(0, 1, 1)) == 72);
    REQUIRE(f1.getCell(f1.getIndex(1, 1, 1)) == 73);
    REQUIRE(f1.getCell(f1.getIndex(2, 1, 1)) == 74);
    REQUIRE(f1.getCell(f1.getIndex(3, 1, 1)) == 75);
    REQUIRE(f1.getCell(f1.getIndex(4, 1, 1)) == 76);
    REQUIRE(f1.getCell(f1.getIndex(5, 1, 1)) == 77);

    REQUIRE(f1.getCell(73, Direction::R) == 74);
    REQUIRE(f1.getCell(73, Direction::L) == 72);

    REQUIRE(f1.getCell<1>(1, 1, 1) == 73);
    REQUIRE(f1.getIndex(1, 2, 1) == 85);
    REQUIRE(f1.getIndex(1, 1, 2) == 133);
  }

  SECTION("getIndex for N=4") {
    REQUIRE(f4.getIndex(0, 1, 1) == 72);
    REQUIRE(f4.getIndex(1, 1, 1) == 73);
    REQUIRE(f4.getIndex(2, 1, 1) == 74);
    REQUIRE(f4.getIndex(3, 1, 1) == 75);
    REQUIRE(f4.getIndex(4, 1, 1) == 76);
    REQUIRE(f4.getIndex(5, 1, 1) == 77);
    REQUIRE(f4.getCell(f4.getIndex(0, 1, 1)) == 72);
    REQUIRE(f4.getCell(f4.getIndex(1, 1, 1)) == 73);
    REQUIRE(f4.getCell(f4.getIndex(2, 1, 1)) == 74);
    REQUIRE(f4.getCell(f4.getIndex(3, 1, 1)) == 75);
    REQUIRE(f4.getCell(f4.getIndex(4, 1, 1)) == 76);
    REQUIRE(f4.getCell(f4.getIndex(5, 1, 1)) == 77);

    REQUIRE(f4.getCell(72, Direction::R) == 73);
    REQUIRE(f4.getCell(73, Direction::L) == 72);
    REQUIRE(f4.getCell(76, Direction::R) == 77);
    REQUIRE(f4.getCell(72, Direction::L) == 71);

    REQUIRE(f4.getCell<1>(1, 1, 1) == 73);
    REQUIRE(f4.getIndex(1, 2, 1) == 85);
    REQUIRE(f4.getIndex(1, 1, 2) == 133);
  }

  SECTION("fieldview 1") {
    auto view = f4.createView<1>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 73);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 73);
        REQUIRE(view.getIndex(cell) == 73);
        REQUIRE(view.get(cell, Direction::R) == 74);
        REQUIRE(view.get(cell, Direction::L) == 72);
        REQUIRE(view.get<1>(cell) == 73);
      } else if (cnt == 1) {
        REQUIRE(*cell == 74);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 74);
        REQUIRE(view.getIndex(cell) == 74);
        REQUIRE(view.get(cell, Direction::R) == 75);
        REQUIRE(view.get(cell, Direction::L) == 73);
      } else if (cnt == 3) {
        REQUIRE(*cell == 76);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 76);
        REQUIRE(view.getIndex(cell) == 76);
        REQUIRE(view.get(cell, Direction::R) == 77);
        REQUIRE(view.get(cell, Direction::L) == 75);
      } else if (cnt == 4) {
        REQUIRE(*cell == 77);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 77);
        REQUIRE(view.getIndex(cell) == 77);
        REQUIRE(view.get(cell, Direction::R) == 78);
        REQUIRE(view.get(cell, Direction::L) == 76);
      } else if (cnt == 8) {
        REQUIRE(view.get(cell) == 85);
      } else if (cnt == 24) {
        REQUIRE(view.get(cell) == 133);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 2") {
    auto view = f4.createView<2>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 73);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 73);
        REQUIRE(view.getIndex(cell) == 73);
        REQUIRE(view.get(cell, Direction::R) == 74);
        REQUIRE(view.get(cell, Direction::L) == 72);
      } else if (cnt == 1) {
        REQUIRE(*cell == 75);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 75);
        REQUIRE(view.getIndex(cell) == 75);
        REQUIRE(view.get(cell, Direction::R) == 76);
        REQUIRE(view.get(cell, Direction::L) == 74);
      } else if (cnt == 2) {
        REQUIRE(*cell == 77);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 77);
        REQUIRE(view.getIndex(cell) == 77);
        REQUIRE(view.get(cell, Direction::R) == 78);
        REQUIRE(view.get(cell, Direction::L) == 76);
      } else if (cnt == 4) {
        REQUIRE(view.get(cell) == 85);
      } else if (cnt == 12) {
        REQUIRE(view.get(cell) == 133);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 4") {
    auto view = f4.createView<4>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 73);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 73);
        REQUIRE(view.getIndex(cell) == 73);
        REQUIRE(view.get(cell, Direction::R) == 74);
        REQUIRE(view.get(cell, Direction::L) == 72);
      } else if (cnt == 1) {
        REQUIRE(*cell == 77);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 77);
        REQUIRE(view.getIndex(cell) == 77);
        REQUIRE(view.get(cell, Direction::R) == 78);
        REQUIRE(view.get(cell, Direction::L) == 76);
      } else if (cnt == 2) {
        REQUIRE(view.get(cell) == 85);
      } else if (cnt == 6) {
        REQUIRE(view.get(cell) == 133);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview coordinates") {
    auto view = f1.createView<1>();

    int cnt = 0;
    for (auto cell = view.begin(); cell != view.end(); ++cell) {
      if (cnt == 0) {
        REQUIRE(cell.coordinates().x() == 1);  // (x, y, z) = (1, 1, 1)
        REQUIRE(cell.coordinates().y() == 1);
        REQUIRE(cell.coordinates().z() == 1);
      } else if (cnt == 1) {
        REQUIRE(cell.coordinates().x() == 2);
        break;
      }
      cnt++;
    }
  }
}

TEST_CASE("Fieldview steps", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[8, 3, 3], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  FieldProperties fieldProperties{stencil::D3C27{}, FieldType::real | FieldType::aligned, 1};
  fieldProperties.setAlignment(4);
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t, 1> f1{*(blockmanager.getBlockSet().begin()->second), fieldProperties};

  auto* f1ptr = f1.getCellBasePtr();

  for (unsigned int index = 0; index < 12 * 5 * 5; index++) {
    f1ptr[index] = index;
  }

  /*
  Index for N=1, 4, value:
  +-----------------------+ +-------------------------------+
  |61 62 63 64 65 66 67 68| |111 112 113 114 115 116 117 118|
  |71 72 73 74 75 76 77 78| |121 122 123 124 125 126 127 128|
  |81 82 83 84 85 86 87 88| |131 132 133 134 135 136 137 138|
  +-----------------------+ +-------------------------------+
  +-----------------------+ +-------------------------------+
  |73 74 75 76 77 78 79 80| |133                         140|
  |85                   92| |145 146 147 148 149 150 151 152|
  |97                  104| |157                         164|
  +-----------------------+ +-------------------------------+
  */
  SECTION("fieldview steps") {
    auto view = f1.createView<1>();
    auto cell = view.begin();
    REQUIRE(**cell == 73);
    cell += 1;
    REQUIRE(**cell == 74);
    cell += 2;
    REQUIRE(**cell == 76);
    cell += 5;
    REQUIRE(**cell == 85);
    cell += 2;
    REQUIRE(**cell == 87);
    cell += 25;
    REQUIRE(**cell == 148);
    cell += 4;
    REQUIRE(**cell == 152);
    cell += 1;
    REQUIRE(**cell == 157);
  }
}

TEST_CASE("Fieldview vector length 2", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[8, 3, 3], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  FieldProperties fieldProperties{stencil::D3C27{}, FieldType::real | FieldType::aligned, 2};
  fieldProperties.setAlignment(4);
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t, 1> f1{*(blockmanager.getBlockSet().begin()->second), fieldProperties};
  field::Field<real_t, 4> f4{*(blockmanager.getBlockSet().begin()->second), fieldProperties};

  auto* f1ptr = f1.getCellBasePtr();
  auto* f4ptr = f4.getCellBasePtr();

  for (unsigned int index = 0; index < 500; index++) {
    f1ptr[index] = index;
    f4ptr[index] = index;
  }

  /*
  Index for N=1, value:
  +---------------+ +---------------+ +---------------+
  |114 116 118 120| |122 124 126 128| |130 132 134 136|
  |115            | |123            | |131            |
  +---------------+ +---------------+ +---------------+
  Index for N=4, value:
  +---------------+ +---------------+ +---------------+
  |114 115 116 117| |122 123 124 125| |130 131 132 133|
  |118            | |126            | |134            |
  +---------------+ +---------------+ +---------------+
  */

  SECTION("getIndex for N=1") {
    REQUIRE(f1.getIndex(0, 1, 1) == 120);
    REQUIRE(f1.getIndex(1, 1, 1) == 122);
    REQUIRE(f1.getIndex(2, 1, 1) == 124);
    REQUIRE(f1.getIndex(3, 1, 1) == 126);
    REQUIRE(f1.getIndex(4, 1, 1) == 128);
    REQUIRE(f1.getIndex(5, 1, 1) == 130);
    REQUIRE(f1.getCell(f1.getIndex(0, 1, 1)) == 120);
    REQUIRE(f1.getCell(f1.getIndex(1, 1, 1)) == 122);
    REQUIRE(f1.getCell(f1.getIndex(2, 1, 1)) == 124);
    REQUIRE(f1.getCell(f1.getIndex(3, 1, 1)) == 126);
    REQUIRE(f1.getCell(f1.getIndex(4, 1, 1)) == 128);
    REQUIRE(f1.getCell(f1.getIndex(5, 1, 1)) == 130);

    REQUIRE(f1.getCell(122, Direction::R) == 124);
    REQUIRE(f1.getCell(122, Direction::L) == 120);

    REQUIRE(f1.getCell(122, 1) == 123);
    REQUIRE(f1.getCell(122, Direction::R, 1) == 125);
  }

  SECTION("getIndex for N=4") {
    REQUIRE(f4.getIndex(0, 1, 1) == 117);
    REQUIRE(f4.getIndex(1, 1, 1) == 122);
    REQUIRE(f4.getIndex(2, 1, 1) == 123);
    REQUIRE(f4.getIndex(3, 1, 1) == 124);
    REQUIRE(f4.getIndex(4, 1, 1) == 125);
    REQUIRE(f4.getIndex(5, 1, 1) == 130);
    REQUIRE(f4.getCell(f4.getIndex(0, 1, 1)) == 117);
    REQUIRE(f4.getCell(f4.getIndex(1, 1, 1)) == 122);
    REQUIRE(f4.getCell(f4.getIndex(2, 1, 1)) == 123);
    REQUIRE(f4.getCell(f4.getIndex(3, 1, 1)) == 124);
    REQUIRE(f4.getCell(f4.getIndex(4, 1, 1)) == 125);
    REQUIRE(f4.getCell(f4.getIndex(5, 1, 1)) == 130);

    REQUIRE(f4.getCell(122, Direction::R) == 123);
    REQUIRE(f4.getCell(123, Direction::L) == 122);
    REQUIRE(f4.getCell(125, Direction::R) == 130);
    REQUIRE(f4.getCell(122, Direction::L) == 117);

    REQUIRE(f4.getCell(122, 1) == 126);
    REQUIRE(f4.getCell(122, Direction::R, 1) == 127);
  }

  SECTION("fieldview 1") {
    auto view = f4.createView<1>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 122);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 122);
        REQUIRE(view.getIndex(cell) == 122);
        REQUIRE(view.get(cell, Direction::R) == 123);
        REQUIRE(view.get(cell, Direction::L) == 117);

        REQUIRE(view.getVec(cell)[1] == 126);
      } else if (cnt == 1) {
        REQUIRE(*cell == 123);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 123);
        REQUIRE(view.getIndex(cell) == 123);
        REQUIRE(view.get(cell, Direction::R) == 124);
        REQUIRE(view.get(cell, Direction::L) == 122);
      } else if (cnt == 3) {
        REQUIRE(*cell == 125);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 125);
        REQUIRE(view.getIndex(cell) == 125);
        REQUIRE(view.get(cell, Direction::R) == 130);
        REQUIRE(view.get(cell, Direction::L) == 124);
      } else if (cnt == 4) {
        REQUIRE(*cell == 130);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 130);
        REQUIRE(view.getIndex(cell) == 130);
        REQUIRE(view.get(cell, Direction::R) == 131);
        REQUIRE(view.get(cell, Direction::L) == 125);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 2") {
    auto view = f4.createView<2>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 122);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 122);
        REQUIRE(view.getIndex(cell) == 122);
        REQUIRE(view.get(cell, Direction::R) == 123);
        REQUIRE(view.get(cell, Direction::L) == 117);

        REQUIRE(view.getVec(cell)[1] == 126);
      } else if (cnt == 1) {
        REQUIRE(*cell == 124);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 124);
        REQUIRE(view.getIndex(cell) == 124);
        REQUIRE(view.get(cell, Direction::R) == 125);
        REQUIRE(view.get(cell, Direction::L) == 123);
      } else if (cnt == 2) {
        REQUIRE(*cell == 130);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 130);
        REQUIRE(view.getIndex(cell) == 130);
        REQUIRE(view.get(cell, Direction::R) == 131);
        REQUIRE(view.get(cell, Direction::L) == 125);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 4") {
    auto view = f4.createView<4>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 122);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 122);
        REQUIRE(view.getIndex(cell) == 122);
        REQUIRE(view.get(cell, Direction::R) == 123);
        REQUIRE(view.get(cell, Direction::L) == 117);

        REQUIRE(view.getVec(cell)[1] == 126);
      } else if (cnt == 1) {
        REQUIRE(*cell == 130);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 130);
        REQUIRE(view.getIndex(cell) == 130);
        REQUIRE(view.get(cell, Direction::R) == 131);
        REQUIRE(view.get(cell, Direction::L) == 125);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview coordinates") {
    auto view = f1.createView<1>();

    int cnt = 0;
    for (auto cell = view.begin(); cell != view.end(); ++cell) {
      if (cnt == 0) {
        REQUIRE(cell.coordinates().x() == 1);  // (x, y, z) = (1, 1, 1)
        REQUIRE(cell.coordinates().y() == 1);
        REQUIRE(cell.coordinates().z() == 1);
      } else if (cnt == 1) {
        REQUIRE(cell.coordinates().x() == 2);
        break;
      }
      cnt++;
    }
  }
}

TEST_CASE("Fieldview vector length 3", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[8, 3, 3], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  FieldProperties fieldProperties{stencil::D3C27{}, FieldType::real | FieldType::aligned, 3};
  fieldProperties.setAlignment(4);
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t, 1> f1{*(blockmanager.getBlockSet().begin()->second), fieldProperties};
  field::Field<real_t, 4> f4{*(blockmanager.getBlockSet().begin()->second), fieldProperties};

  auto* f1ptr = f1.getCellBasePtr();
  auto* f4ptr = f4.getCellBasePtr();

  for (unsigned int index = 0; index < 750; index++) {
    f1ptr[index] = index;
    f4ptr[index] = index;
  }

  /*
  Index for N=1, value:
  +---------------+ +---------------+ +---------------+
  |171 174 177 180| |183 186 189 192| |195 198 201 204|
  |172            | |184            | |196            |
  |173            | |185            | |197            |
  +---------------+ +---------------+ +---------------+
  Index for N=4, value:
  +---------------+ +---------------+ +---------------+
  |171 172 173 174| |183 184 185 186| |195 196 197 198|
  |175            | |187            | |199            |
  |176            | |191            | |203            |
  +---------------+ +---------------+ +---------------+
  */

  SECTION("getIndex for N=1") {
    REQUIRE(f1.getIndex(0, 1, 1) == 180);
    REQUIRE(f1.getIndex(1, 1, 1) == 183);
    REQUIRE(f1.getIndex(2, 1, 1) == 186);
    REQUIRE(f1.getIndex(3, 1, 1) == 189);
    REQUIRE(f1.getIndex(4, 1, 1) == 192);
    REQUIRE(f1.getIndex(5, 1, 1) == 195);
    REQUIRE(f1.getCell(f1.getIndex(0, 1, 1)) == 180);
    REQUIRE(f1.getCell(f1.getIndex(1, 1, 1)) == 183);
    REQUIRE(f1.getCell(f1.getIndex(2, 1, 1)) == 186);
    REQUIRE(f1.getCell(f1.getIndex(3, 1, 1)) == 189);
    REQUIRE(f1.getCell(f1.getIndex(4, 1, 1)) == 192);
    REQUIRE(f1.getCell(f1.getIndex(5, 1, 1)) == 195);

    REQUIRE(f1.getCell(183, Direction::R) == 186);
    REQUIRE(f1.getCell(183, Direction::L) == 180);

    REQUIRE(f1.getCell(183, 1) == 184);
    REQUIRE(f1.getCell(183, Direction::R, 1) == 187);
  }

  SECTION("getIndex for N=4") {
    REQUIRE(f4.getIndex(0, 1, 1) == 174);
    REQUIRE(f4.getIndex(1, 1, 1) == 183);
    REQUIRE(f4.getIndex(2, 1, 1) == 184);
    REQUIRE(f4.getIndex(3, 1, 1) == 185);
    REQUIRE(f4.getIndex(4, 1, 1) == 186);
    REQUIRE(f4.getIndex(5, 1, 1) == 195);
    REQUIRE(f4.getCell(f4.getIndex(0, 1, 1)) == 174);
    REQUIRE(f4.getCell(f4.getIndex(1, 1, 1)) == 183);
    REQUIRE(f4.getCell(f4.getIndex(2, 1, 1)) == 184);
    REQUIRE(f4.getCell(f4.getIndex(3, 1, 1)) == 185);
    REQUIRE(f4.getCell(f4.getIndex(4, 1, 1)) == 186);
    REQUIRE(f4.getCell(f4.getIndex(5, 1, 1)) == 195);

    REQUIRE(f4.getCell(183, Direction::R) == 184);
    REQUIRE(f4.getCell(184, Direction::L) == 183);
    REQUIRE(f4.getCell(186, Direction::R) == 195);
    REQUIRE(f4.getCell(186, Direction::L) == 185);

    REQUIRE(f4.getCell(183, 1) == 187);
    REQUIRE(f4.getCell(183, Direction::R, 1) == 188);
  }

  SECTION("fieldview 1") {
    auto view = f4.createView<1>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 183);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 183);
        REQUIRE(view.getIndex(cell) == 183);
        REQUIRE(view.get(cell, Direction::R) == 184);
        REQUIRE(view.get(cell, Direction::L) == 174);

        REQUIRE(view.getVec(cell)[1] == 187);
      } else if (cnt == 1) {
        REQUIRE(*cell == 184);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 184);
        REQUIRE(view.getIndex(cell) == 184);
        REQUIRE(view.get(cell, Direction::R) == 185);
        REQUIRE(view.get(cell, Direction::L) == 183);
      } else if (cnt == 3) {
        REQUIRE(*cell == 186);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 186);
        REQUIRE(view.getIndex(cell) == 186);
        REQUIRE(view.get(cell, Direction::R) == 195);
        REQUIRE(view.get(cell, Direction::L) == 185);
      } else if (cnt == 4) {
        REQUIRE(*cell == 195);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 195);
        REQUIRE(view.getIndex(cell) == 195);
        REQUIRE(view.get(cell, Direction::R) == 196);
        REQUIRE(view.get(cell, Direction::L) == 186);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 2") {
    auto view = f4.createView<2>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 183);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 183);
        REQUIRE(view.getIndex(cell) == 183);
        REQUIRE(view.get(cell, Direction::R) == 184);
        REQUIRE(view.get(cell, Direction::L) == 174);

        REQUIRE(view.getVec(cell)[1] == 187);
      } else if (cnt == 1) {
        REQUIRE(*cell == 185);  // (x, y, z) = (2, 1, 1)
        REQUIRE(view.get(cell) == 185);
        REQUIRE(view.getIndex(cell) == 185);
        REQUIRE(view.get(cell, Direction::R) == 186);
        REQUIRE(view.get(cell, Direction::L) == 184);
      } else if (cnt == 2) {
        REQUIRE(*cell == 195);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 195);
        REQUIRE(view.getIndex(cell) == 195);
        REQUIRE(view.get(cell, Direction::R) == 196);
        REQUIRE(view.get(cell, Direction::L) == 186);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview 4") {
    auto view = f4.createView<4>();

    int cnt = 0;
    for (const auto& cell : view) {
      if (cnt == 0) {
        REQUIRE(*cell == 183);  // (x, y, z) = (1, 1, 1)
        REQUIRE(view.get(cell) == 183);
        REQUIRE(view.getIndex(cell) == 183);
        REQUIRE(view.get(cell, Direction::R) == 184);
        REQUIRE(view.get(cell, Direction::L) == 174);

        REQUIRE(view.getVec(cell)[1] == 187);
      } else if (cnt == 1) {
        REQUIRE(*cell == 195);  // (x, y, z) = (4, 1, 1)
        REQUIRE(view.get(cell) == 195);
        REQUIRE(view.getIndex(cell) == 195);
        REQUIRE(view.get(cell, Direction::R) == 196);
        REQUIRE(view.get(cell, Direction::L) == 186);
        break;
      }
      cnt++;
    }
  }

  SECTION("fieldview coordinates") {
    auto view = f1.createView<1>();

    int cnt = 0;
    for (auto cell = view.begin(); cell != view.end(); ++cell) {
      if (cnt == 0) {
        REQUIRE(cell.coordinates().x() == 1);  // (x, y, z) = (1, 1, 1)
        REQUIRE(cell.coordinates().y() == 1);
        REQUIRE(cell.coordinates().z() == 1);
      } else if (cnt == 1) {
        REQUIRE(cell.coordinates().x() == 2);
        break;
      }
      cnt++;
    }
  }
}
