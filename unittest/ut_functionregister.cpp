/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/factory.h"
#include "lib/functions/functionregister.h"

using namespace nastja;

class IFunc {
public:
  virtual ~IFunc() = default;

  virtual double calc(const unsigned int x, int& r) = 0;
};

class Func1 : public IFunc {
public:
  double calc(const unsigned int x, int& r) override {
    r += 2;
    return 1.0 * x;
  };
};

class Func2 : public IFunc {
public:
  double calc(const unsigned int x, int& r) override {
    r += 1;
    return 2.0 * x;
  };
};

class FactoryFunc : public Factory<IFunc> {
public:
  explicit FactoryFunc() {
    registerType<Func1>("1");
    registerType<Func2>("2");
  }
};

TEST_CASE("FunctionRegister", "[unit][math]") {
  FactoryFunc factory;
  FunctionRegister<IFunc> fr;
  fr.registerFunction(factory.create("1"));
  fr.registerFunction(factory.create("2"));
  fr.registerFunction(factory.create("1"));

  SECTION("Calculation") {
    int r = 0;
    REQUIRE(fr.calc(1, r) == 4.0);
    REQUIRE(r == 5);
    REQUIRE(fr.calc(2, r) == 8.0);
    REQUIRE(r == 10);
  }
}

TEST_CASE("Factory", "[unit]") {
  FactoryFunc factory;

  SECTION("Throws") {
    REQUIRE_THROWS_AS(factory.create("blub"), std::invalid_argument);
    REQUIRE_THROWS_AS(factory.create_ptr("blub"), std::invalid_argument);
  }

  SECTION("Names") {
    REQUIRE(factory.getNamesFormatted() == " * 1\n * 2\n");
  }
}

