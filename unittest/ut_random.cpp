/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/random.h"
#include <iostream>
#include <random>

using namespace nastja;

TEST_CASE("Random Distributions", "[unit][math][class]") {
  SECTION("random") {
    auto r_int   = math::UniformInt<int>{0, 5};
    auto r_float = math::UniformReal<float>{0, 1};

    std::mt19937_64 e;
    e.seed(42);

    int sum = 0;
    for (int cnt = 0; cnt < 1e6; cnt++) {
      sum += r_int(e);
    }
    REQUIRE((double)sum / 1e6 == Approx(2.5).epsilon(0.01));

    float sum_f = 0;
    for (int cnt = 0; cnt < 1e6; cnt++) {
      sum_f += r_float(e);
    }
    REQUIRE(sum_f / 1e6 == Approx(0.5).epsilon(0.01));
  }
}
