/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/arithmetic.h"

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Arithmetic", "[unit]") {
  SECTION("tailing zero count") {
    REQUIRE(countTailingZeros(0) == 32);
    REQUIRE(countTailingZeros(1) == 0);
    REQUIRE(countTailingZeros(4) == 2);
    REQUIRE(countTailingZeros(6) == 1);
  }

  SECTION("power of two") {
    REQUIRE(isPowerOfTwo(0) == false);
    REQUIRE(isPowerOfTwo(1) == true);
    REQUIRE(isPowerOfTwo(4) == true);
    REQUIRE(isPowerOfTwo(6) == false);
    REQUIRE(isPowerOfTwo(1024) == true);
  }

  SECTION("natural modulo") {
    REQUIRE(mod(4, 3) == 1);
    REQUIRE(mod(-1, 4) == 3);
    REQUIRE(mod(-1, 3) == 2);
    REQUIRE(mod(-3, 4) == 1);
    REQUIRE(mod(3, 4) == 3);
  }

  SECTION("log2") {
    REQUIRE(countLeadingZeros(0) == 32);
    REQUIRE(countLeadingZeros(1) == 31);
    REQUIRE(countLeadingZeros(2) == 30);
    REQUIRE(countLeadingZeros(2147483648) == 0);
    REQUIRE(countLeadingZeros(2147483647) == 1);
  }
}
