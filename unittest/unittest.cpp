/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"
#include "lib/simdata/mpidata.h"

using namespace nastja;

MPIData mpiData;

int main(int argc, char* argv[]) {
  mpiData.init(argc, argv);
  return Catch::Session().run(argc, argv);
}
