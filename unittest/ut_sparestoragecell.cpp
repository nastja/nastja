/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/archive.h"
#include "lib/cell.h"
#include "lib/datatypes.h"
#include "lib/storage/sparestoragecell.h"

using namespace nastja;

template <typename T>
struct TD;

TEST_CASE("Spare Storage Cell", "[unit][class]") {
  SECTION("Default Value") {
    auto test = SpareStorageCell();

    CHECK(test[0].volume == 0.0);
    CHECK(test[5].signal[2] == 0.0);
  }

  SECTION("Set Values") {
    auto test = SpareStorageCell();

    test[0].volume = 1.0;
    test[2].volume = 2.0;
    test[2].signal = {0.1, 0.2, 0.3};
    test(0).neighbors[5] += 25.;

    CHECK(test(0).volume == 1.0);
    CHECK(test(2).volume == 2.0);
    CHECK(test(2).signal[1] == 0.2);
    CHECK(!test.contains(1));
    CHECK(test.size() == 2);
    CHECK(test[1].volume == 0.0);
    CHECK(test.contains(1));
    CHECK(test.size() == 3);
  }

  SECTION("Pack/unpack") {
    auto test  = SpareStorageCell();
    auto test2 = SpareStorageCell();

    test[0].volume = 1.0;
    test[2].volume = 2.0;
    test[2].signal = {0.1, 0.2, 0.3};

    test[3].neighbors[20] += 25.0;
    test[3].neighbors[30] += 15.0;

    CHECK(test(3).neighbors[20] == 25.0);
    CHECK(test(3).neighbors[30] == 15.0);

    Archive ar;

    ar.pack(test);
    ar.reset();
    ar.unpack(test2);

    CHECK(test2(0).volume == 1.0);
    CHECK(test2(2).volume == 2.0);
    CHECK(test2(2).signal[1] == 0.2);
    CHECK(!test2.contains(1));

    CHECK(test2(3).neighbors[20] == 0.0);
    CHECK(test2(3).neighbors[30] == 0.0);
  }

  SECTION("iterator") {
    auto test      = SpareStorageCell();
    test[0].volume = 1.0;
    test[2].volume = 2.0;

    for (auto& cell : test) {
      cell.second.volume *= 2;
    }

    long sum = 0;
    for (const auto& cell : test) {
      sum += cell.second.volume;
    }
    CHECK(sum == 6);
  }
}
