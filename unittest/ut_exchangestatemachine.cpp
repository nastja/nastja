/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/action/exchangestatemachine.h"

using namespace nastja;

TEST_CASE("ExchangeStateMachine", "[unit][statemachine]") {
  SECTION("states") {
    ExchangeStateMachine esm;

    REQUIRE(esm.getState() == ExchangeState::initial);

    esm.processEvent(ExchangeEvent::initfull);
    REQUIRE(esm.isFullExchange());

    esm.setState(ExchangeState::initial);
    esm.processEvent(ExchangeEvent::initpartial);
    REQUIRE(esm.isFullExchange());
    esm.processEvent(ExchangeEvent::next);
    REQUIRE(esm.isFullExchange());
    esm.processEvent(ExchangeEvent::next);
    REQUIRE(!esm.isFullExchange());
  }
}
