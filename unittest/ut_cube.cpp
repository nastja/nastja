/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/calculatesurface.cpp"
#include "lib/math/calculatesurface.h"
#include <bitset>

using namespace nastja;
using namespace nastja::math;

TEST_CASE("Surface", "[unit][math]") {
  SECTION("simple rotations") {
    REQUIRE(bitRotationX(0) == 0);
    REQUIRE(bitRotationX(255) == 255);
    REQUIRE(bitRotationX(0b00001011) == 0b10001100);
    REQUIRE(bitRotationY(0) == 0);
    REQUIRE(bitRotationY(255) == 255);
    REQUIRE(bitRotationY(0b00000101) == 0b01010000);
    REQUIRE(bitRotationY(0b10010110) == 0b01101001);
    REQUIRE(bitRotationZ(0) == 0);
    REQUIRE(bitRotationZ(255) == 255);
    REQUIRE(bitRotationZ(0b10010110) == 0b01101001);
  }

  SECTION("id rotation") {
    for (unsigned int i = 0; i < 256; i++) {
      REQUIRE(bitRotationX(bitRotationX(bitRotationX(bitRotationX(i)))) == i);
      REQUIRE(bitRotationY(bitRotationY(bitRotationY(bitRotationY(i)))) == i);
      REQUIRE(bitRotationZ(bitRotationZ(bitRotationZ(bitRotationZ(i)))) == i);
    }
  }

  SECTION("inverse area") {
    for (int i = 0; i < 128; i++) {
      printf("%d -> %d (%la %.16lf)\n", i, (i ^ 0xFF), calcSurfaceMCLookup(i), calcSurfaceMCLookup(i));
      REQUIRE(calcSurfaceMCLookup(i) == calcSurfaceMCLookup(i ^ 0xFF));
    }
  }
}
