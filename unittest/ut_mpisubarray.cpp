/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/D3C26.h"
#include "lib/stencil/D3C27.h"
#include "lib/stencil/direction.h"
#include <algorithm>
#include <mpi.h>

using namespace nastja;
using namespace nastja::field;
using namespace nastja::stencil;

extern MPIData mpiData;

namespace nastja {
namespace field {

class TESTING_FieldProperties {
public:
  TESTING_FieldProperties(FieldProperties& fieldProperties) : fieldProperties_(fieldProperties) {}

  void setupHalo() { fieldProperties_.setupHalo(); }

private:
  FieldProperties& fieldProperties_;
};

}  // namespace field
}  // namespace nastja

// sends inner border to the correct position at outer
void callBoundary(const std::vector<double>& input, std::vector<double>& output, Direction side, FieldProperties* fieldtype) {
  MPI_Request request;
  MPI_Irecv(output.data(), 1, fieldtype->getHaloDataType(HaloWay::target, getInverseDirection(side)), 0, 42, MPI_COMM_WORLD, &request);
  MPI_Send(input.data(), 1, fieldtype->getHaloDataType(HaloWay::source, side), 0, 42, MPI_COMM_WORLD);
  MPI_Wait(&request, MPI_STATUS_IGNORE);
}

int testBoundary(std::vector<double>& input, std::vector<double>& output, Direction side, FieldProperties* fieldtype) {
  std::fill_n(output.begin(), output.size(), 0.0);
  callBoundary(input, output, side, fieldtype);
  return std::count_if(output.begin(), output.end(), [](int i) { return i == 1.0; });
}

int getIndex(int x, int y, int z) { return ((z * 8) + y) * 8 + x; }

TEST_CASE("MPI create subarray calculations", "[unit][class][mpi]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1},"Geometry":{"blocksize":[6, 6, 6]}})");
  SimData simdata(config, mpiData);

  auto fieldProperties = new FieldProperties(stencil::D3C27{}, field::FieldType::real);
  fieldProperties->registerSimData(&simdata);
  TESTING_FieldProperties ftTesting(*fieldProperties);
  ftTesting.setupHalo();

  const int size = 8 * 8 * 8;
  std::vector<double> input(size, 1.0);
  std::vector<double> output(size);

  std::fill_n(output.begin(), size, 0.0);

  SECTION("Boundary exchange") {
    // faces
    CHECK(testBoundary(input, output, Direction::L, fieldProperties) == 36);
    CHECK(output[getIndex(7, 1, 1)] == 1.0);
    CHECK(testBoundary(input, output, Direction::R, fieldProperties) == 36);
    CHECK(output[getIndex(0, 1, 1)] == 1.0);
    CHECK(testBoundary(input, output, Direction::D, fieldProperties) == 36);
    CHECK(testBoundary(input, output, Direction::U, fieldProperties) == 36);
    CHECK(testBoundary(input, output, Direction::B, fieldProperties) == 36);
    CHECK(testBoundary(input, output, Direction::F, fieldProperties) == 36);

    // edges
    CHECK(testBoundary(input, output, Direction::DL, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::DR, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::UL, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::UR, fieldProperties) == 6);
    CHECK(output[getIndex(0, 0, 1)] == 1.0);  // UR -> DL
    CHECK(testBoundary(input, output, Direction::BL, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::BR, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::BD, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::BU, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::FL, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::FR, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::FD, fieldProperties) == 6);
    CHECK(testBoundary(input, output, Direction::FU, fieldProperties) == 6);
    CHECK(output[getIndex(1, 0, 0)] == 1.0);  // FU -> BD

    // corner
    CHECK(testBoundary(input, output, Direction::BDL, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::BDR, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::BUL, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::BUR, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::FDL, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::FDR, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::FUL, fieldProperties) == 1);
    CHECK(testBoundary(input, output, Direction::FUR, fieldProperties) == 1);
    CHECK(output[getIndex(0, 0, 0)] == 1.0);
  }
}
