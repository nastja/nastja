/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/math/brent.h"
#include <cmath>

using namespace nastja;
using namespace nastja::math;

double id(double x) {
  return x;
}

double f(double x) {
  return exp(-x) * log(x);
}

TEST_CASE("brent", "[unit][math]") {
  SECTION("bracket") {
    double res;

    res = brent(id, -1.0, 1.0, 1e-10);
    REQUIRE(res == 0.0);

    res = brent(id, -1.0, -0.1, 1e-10);
    REQUIRE(res == 0.0);

    res = brent(id, -0.1, 1.0, 1e-10);
    REQUIRE(res == 0.0);
  }

  SECTION("brent exp and log") {
    double res;

    res = brent(f, 0.05, 1.7, 1e-20);
    REQUIRE(res == 1.0);
  }
}
