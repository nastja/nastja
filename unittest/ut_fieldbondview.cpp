/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/datatypes.h"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/field/field.h"
#include "lib/field/fieldbondview.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"
#include "lib/stencil/direction.h"

using namespace nastja;
using namespace nastja::stencil;

extern MPIData mpiData;

TEST_CASE("FieldBondView", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1},"Geometry":{"blocksize":[9, 8, 7], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);

  auto stencil = stencil::D3C26{};
  field::FieldProperties fieldProperties {
    stencil,
    field::FieldType::real | field::FieldType::aligned,
    stencil.size / 2
  };
  fieldProperties.setAlignment(4);
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t> field {
    *(blockmanager.getBlockSet().begin()->second),
    fieldProperties
  };

  field::FieldBondView<real_t, 3, 26> fbv(field);

  /* First, let's look at a concrete example
   *
   * y
   * ▲
   * │  \   │   /
   * │  42  1  18
   * │    \ │ /
   * │     \│/
   * │ ─11──O──12─
   * │     /│\
   * │    / │ \
   * │   6 303 19
   * │  /   │   \
   * │
   * └───────────►x
   *
   * We set the bonds on the xy-plane around (3, 3, 3) to these values
   * and then check whether getting the bond values from the surrounding
   * cells yields the correct results.
   */
  SECTION("simple example") {
    auto v = math::Vector3<unsigned long> { 3, 3, 3 };

    fbv.setBond(v, Direction::UL, 42);
    fbv.setBond(v, Direction::U, 1);
    fbv.setBond(v, Direction::UR, 18);
    fbv.setBond(v, Direction::R, 12);
    fbv.setBond(v, Direction::DR, 19);
    fbv.setBond(v, Direction::D, 303);
    fbv.setBond(v, Direction::DL, 6);
    fbv.setBond(v, Direction::L, 11);

    REQUIRE(fbv.getBond(v + Direction::UL, Direction::DR) == 42);
    REQUIRE(fbv.getBond(v + Direction::U, Direction::D) == 1);
    REQUIRE(fbv.getBond(v + Direction::UR, Direction::DL) == 18);
    REQUIRE(fbv.getBond(v + Direction::R, Direction::L) == 12);
    REQUIRE(fbv.getBond(v + Direction::DR, Direction::UL) == 19);
    REQUIRE(fbv.getBond(v + Direction::D, Direction::U) == 303);
    REQUIRE(fbv.getBond(v + Direction::DL, Direction::UR) == 6);
    REQUIRE(fbv.getBond(v + Direction::L, Direction::R) == 11);
  }

  // Now, fill the field with some data such that every bond has a different value
  // Note: We are using the default view created by createView which does not include the boundary
  auto fv = field.createView();
  real_t x = 0;
  for (auto it = fv.begin(); it != fv.end(); it++) {
    auto pos = it.coordinates();
    auto index = field.getIndex(pos.x(), pos.y(), pos.z());

    for (unsigned long i = 0; i < field.getVectorSize(); i++) {
      field.getCell(index, i) = x;
      x++;
    }
  }

  SECTION("getBond(pos, dir) == getBond(pos + dir, -dir)") {
    for (auto it = fv.begin(); it != fv.end(); it++) {
      auto pos = it.coordinates();

      for (auto sIt = stencil.begin(); sIt != stencil.end(); sIt++) {
        auto dir = sIt.direction();

        REQUIRE(fbv.getBond(pos, dir) == fbv.getBond(pos + dir, stencil::getInverseDirection(dir)));
      }
    }
  }
}
