/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/datatypes.h"
#include "lib/pool.h"
#include "lib/block/blockmanager.h"
#include "lib/field/field.h"
#include "lib/field/fieldproperties.h"
#include "lib/simdata/mpidata.h"
#include "lib/simdata/simdata.h"

using namespace nastja;
using namespace nastja::field;
using namespace nastja::stencil;

extern MPIData mpiData;

TEST_CASE("Fieldview with checkerboard", "[unit][class]") {
  config::Config config;
  config.loadString(R"({"Settings":{"timesteps":1000},"Geometry":{"blocksize":[4, 5, 3], "blockcount":[1, 1, 1]}})");
  SimData simdata(config, mpiData);
  block::BlockManager blockmanager(simdata);
  simdata.timestep++;  // The first time-step.

  FieldProperties fieldProperties{stencil::D3C27{}, FieldType::integer, 1, 1};
  fieldProperties.registerSimData(&simdata);
  fieldProperties.setName("test");
  fieldProperties.setupHook();

  field::Field<real_t, 1> field{*(blockmanager.getBlockSet().begin()->second), fieldProperties};

  SECTION("2-color") {
    auto cbview = field.createViewCB<1>();
    long cnt    = 0;
    for (const auto& cell : cbview) {
      cbview.get(cell) = 1;
      cnt++;
    }
    REQUIRE(cnt == 30);

    cnt = 0;
    for (auto cell = cbview.begin(); cell != cbview.end(); cell += 2) {
      cbview.get(*cell) = 1;
      cnt++;
    }
    REQUIRE(cnt == 15);

    auto fullview = field.createView();
    for (auto cell = fullview.begin(); cell != fullview.end(); cell++) {
      REQUIRE(fullview.get(*cell) == (cell.coordinates().x() + cell.coordinates().y() + cell.coordinates().z()) % 2);
    }
  }

  SECTION("8-color") {
    auto cbview = field.createViewCB<2>();
    long cnt    = 0;
    for (const auto& cell : cbview) {
      cbview.get(cell) = 1;
      cnt++;
    }
    REQUIRE(cnt == 12);

    cnt = 0;
    for (auto cell = cbview.begin(); cell != cbview.end(); cell += 2) {
      cbview.get(*cell) = 1;
      cnt++;
    }
    REQUIRE(cnt == 6);

    auto fullview = field.createView();
    for (auto cell = fullview.begin(); cell != fullview.end(); cell++) {
      unsigned long sum = 0;
      // In checkerboard-world (0,0,0) is (1,1,1) for a boundary size of 1.
      sum += ((cell.coordinates().x() - 1) % 2) << 0;
      sum += ((cell.coordinates().y() - 1) % 2) << 1;
      sum += ((cell.coordinates().z() - 1) % 2) << 2;

      unsigned long res = (sum % 8 == 0);

      REQUIRE(fullview.get(*cell) == res);
    }
  }
}
