/*
 * This file is part of NAStJA.
 *
 * Copyright 2016 - 2021 Marco Berghoff and the NAStJA Core Developers
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "catch2/catch.hpp"
#include "lib/config/config.h"
#include "lib/math/boundingbox.h"
#include "lib/math/vector.h"
#include <array>
#include <iostream>

using namespace nastja;
using namespace nastja::config::literals;

TEST_CASE("Configfile", "[unit][class]") {
  config::Config config;
  config.loadString(R"(
{
  "Geometry": {
    "blocksize": [5, 5, 5],
    "blockcount": [12, 12, 12]
  },
  "Settings": {
    "timesteps": 1000
  },
  "Writers": {
    "VTK_Phi": {
      "writer": "VtkImage",
      "field": "phasefield",
      "steps": 100
    },
    "Load": {
      "writer": "WriteLoad",
      "field": "phasefield"
    }
  },
  "Main": 42,
  "float": 1.0,
  "float2": 1.3,
  "wrongtype": "text",
  "wronguint": -1,
  "Bool1": false,
  "ArrayBool": [true, false, 1.0],
  "Elements": {
    "Vector3": [3, 2, 1],
    "Box": [[1, 2, 3], [4, 5, 6]]
  }
}
  )");

  SECTION("read config") {
    REQUIRE(config.getValue<int>("Writers.VTK_Phi.steps"_jptr, 42) == 100);
    REQUIRE(config.getValue<int>("Default.nixda"_jptr, 42) == 42);
    REQUIRE(config.getValue<int>("Main"_jptr) == 42);
    REQUIRE(config.getValue<int>("Main2"_jptr, 42) == 42);
    CHECK_THROWS(config.getValue<int>("float"_jptr));
    REQUIRE(config.getValue<real_t>("float"_jptr) == 1.0);
    CHECK_THROWS(config.getValue<unsigned int>("float"_jptr));
    CHECK_THROWS(config.getValue<long>("float"_jptr));
    CHECK_THROWS(config.getValue<unsigned long>("float"_jptr));
    REQUIRE(config.getValue<bool>("Bool1"_jptr) == false);
    CHECK_THROWS(config.getValue<bool>("float"_jptr));
    CHECK_THROWS(config.getValue<int>("float2"_jptr));
    CHECK_THROWS(config.getValue<int>("wrongtype"_jptr, 42));
    CHECK_THROWS(config.getValue<unsigned int>("wronguint"_jptr));
    CHECK_THROWS(config.getValue<unsigned long>("wronguint"_jptr));
    CHECK_THROWS(config.getValue<int>("Bool1"_jptr));
    CHECK_THROWS(config.getValue<unsigned int>("Bool1"_jptr));
    CHECK_THROWS(config.getValue<long>("Bool1"_jptr));
    CHECK_THROWS(config.getValue<unsigned long>("Bool1"_jptr));
    CHECK_THROWS(config.getValue<real_t>("Bool1"_jptr));
    CHECK_THROWS(config.getValue<unsigned int>("Bool1"_jptr));
    std::array<int, 3> array;
    config.load1DArray(array, "Geometry.blockcount"_jptr);
    REQUIRE(array[0] == 12);
    std::array<bool, 3> flags;
    CHECK_THROWS(config.load1DArray(flags, "ArrayBool"_jptr));

    REQUIRE(config.getValue<bool>("Bool1"_jptr, true) == false);
    REQUIRE(config.getValue<bool>("Bool2"_jptr, true) == true);

    auto& json = config.getRawJson();
    auto ptr   = config::json::json_pointer("/bla/blub");
    json[ptr]  = 1;
    REQUIRE(config.getValue<int>("bla.blub"_jptr) == 1);
  }

  SECTION("append config") {
    config.loadString(R"(
{
  "Geometry": {
    "blocksize": [6, 6, 6]
  },
  "Main": 21,
  "Test": 42,
  "Writers": {
    "VTK_Phi": {
      "steps": 10
    }
  }
}
    )");
    REQUIRE(config.getValue<int>("Main"_jptr) == 21);
    std::array<int, 3> array;
    config.load1DArray(array, "Geometry.blocksize"_jptr);
    REQUIRE(array[0] == 6);
    REQUIRE(config.getValue<int>("Test"_jptr) == 42);
    // std::cout << config.getJSONFile().toStyledString() << std::endl;
  }

  SECTION("Config evaluation") {
    config.loadString(R"RAW(
{
  "DefineConstants": [
    "testvar =2*3"
  ],
  "DefineFunctions": [
    "mul(x,y) =x *y"
  ],
  "null": null,
  "empty": {},
  "string": "mul(testvar,7)"
}
    )RAW");

    REQUIRE(config.getValue<int>("null"_jptr, 42) == 42);
    REQUIRE(config.getValue<int>("empty"_jptr, 42) == 42);
    REQUIRE(config.getValue<int>("nonexist"_jptr, 42) == 42);
    REQUIRE(config.getValue<int>("string"_jptr, 42) == 42);
  }

  SECTION("Config evaluation throw") {
    std::string str = R"RAW(
{
  "DefineConstants": [
    "test =6",
    "test= 7"
  ]
}
    )RAW";
    REQUIRE_THROWS_WITH(
        config.loadString(str),
        Catch::Contains("readd constant"));

    str = R"RAW(
{
  "DefineConstants": [
    "testx=x7"
  ]
}
    )RAW";
    REQUIRE_THROWS_WITH(
        config.loadString(str),
        Catch::Contains("Invalid expression"));
  }

  SECTION("Arrays") {
    config.loadString(R"RAW(
{
  "array1": [0, 2],
  "array2": ["2*3", "7*3", -1]
}
    )RAW");

    std::array<int, 3> array;
    std::vector<int> vec;
    config.load1DArray(vec, 2, "array1"_jptr);
    REQUIRE(vec[1] == 2);
    config.load1DArray(array, "array1"_jptr, -1);
    REQUIRE(array[2] == -1);
    config.load1DArray(array, "array2"_jptr, -1);
    REQUIRE(array[1] == 21);

    auto vec2 = config.getVector<int>("array1"_jptr);
    REQUIRE(vec2[1] == 2);
    REQUIRE(vec2.size() == 2);
  }

  SECTION("Arrays 2D") {
    config.loadString(R"RAW(
{
  "array-2D": [[2, "2*3"], [1]]
}
    )RAW");

    std::vector<int> array;
    std::array<unsigned int, 2> n{2, 2};
    config.load2DArray(array, n, "array-2D"_jptr, -1);
    REQUIRE(array[2 * 0 + 0] == 2);
    REQUIRE(array[2 * 0 + 1] == 6);
    REQUIRE(array[2 * 1 + 0] == 1);
    REQUIRE(array[2 * 1 + 1] == -1);
  }

  SECTION("Arrays 3D") {
    config.loadString(R"RAW(
{
  "array-3D": [[[2, "2*3"], [1]],
                [[4, 5]]]
}
    )RAW");

    std::vector<int> array;
    std::array<unsigned int, 3> n{2, 2, 2};
    config.load3DArray(array, n, "array-3D"_jptr, -1);
    REQUIRE(array[2 * 2 * 0 + 2 * 0 + 0] == 2);
    REQUIRE(array[2 * 2 * 0 + 2 * 0 + 1] == 6);
    REQUIRE(array[2 * 2 * 0 + 2 * 1 + 0] == 1);
    REQUIRE(array[2 * 2 * 0 + 2 * 1 + 1] == -1);
    REQUIRE(array[2 * 2 * 1 + 2 * 0 + 0] == 4);
    REQUIRE(array[2 * 2 * 1 + 2 * 0 + 1] == 5);
    REQUIRE(array[2 * 2 * 1 + 2 * 1 + 0] == -1);
    REQUIRE(array[2 * 2 * 1 + 2 * 1 + 1] == -1);
  }

  SECTION("Domainsize") {
    config.loadString(R"RAW(
{
  "sizeX": "Nx",
  "sizeY": "Ny",
  "sizeZ": "Nz"
}
    )RAW");

    REQUIRE(config.getValue<int>("sizeX"_jptr) == 60);
    REQUIRE(config.getValue<int>("sizeY"_jptr) == 60);
    REQUIRE(config.getValue<int>("sizeZ"_jptr) == 60);

    config.loadString(R"(
{
  "Geometry": {
    "blocksize": [6, 4, 3]
  },
  "half": "Nx/2",
  "Settings": {
    "deltat": 0.02
  },
  "calc": "dt*dx*5"
}
    )");

    REQUIRE(config.getValue<int>("sizeX"_jptr) == 72);
    REQUIRE(config.getValue<int>("sizeY"_jptr) == 48);
    REQUIRE(config.getValue<int>("sizeZ"_jptr) == 36);
    REQUIRE(config.getValue<int>("half"_jptr) == 36);
    REQUIRE(config.getValue<real_t>("calc"_jptr) == 0.1);
  }

  SECTION("Random") {
    config.loadString(R"RAW(
{
  "val1": "rnd()",
  "val2": "rnd()",
  "val3": "rnd()"
}
    )RAW");
    auto v1 = config.getValue<double>("val1"_jptr);
    auto v2 = config.getValue<double>("val2"_jptr);
    auto v3 = config.getValue<double>("val3"_jptr);
    REQUIRE(v1 >= 0.0);
    REQUIRE(v1 < 1.0);
    REQUIRE(v2 >= 0.0);
    REQUIRE(v2 < 1.0);
    REQUIRE(v3 >= 0.0);
    REQUIRE(v3 < 1.0);
    REQUIRE(v3 != v1);
    REQUIRE(v2 != v1);
  }

  SECTION("Random") {
    config.loadString(R"RAW(
{
  "v2": ["4 / 2",  3],
  "v3": [0, 1, 3],
  "v3_2": ["2 * 3", 1, 3],
  "bb": [[0, 1, 2], ["5 + 3", 1, 2]]
}
    )RAW");
    auto v0 = config.getVector2<int>("v2"_jptr);
    auto v1 = config.getVector3<int>("v3"_jptr);
    auto v2 = config.getVector3<int>("v3_2"_jptr);
    auto bb = config.getBoundingBox<int>("bb"_jptr);
    REQUIRE(v0[0] == 2);
    REQUIRE(v1[1] == 1);
    REQUIRE(v2[0] == 6);
    REQUIRE(bb[0][2] == 2);
    REQUIRE(bb[1][0] == 9);
  }

  SECTION("CommandLine") {
    int argc = 3;
    char* argv[3];
    argv[0] = (char*)"nastja";
    argv[1] = (char*)"-C";
    argv[2] = (char*)"{\"cmdtest\": 5, 'cmdtest2': 4}";

    config.parseCmdLine(argc, argv);
    config.loadString(config.getCmdLine().configstring);
    REQUIRE(config.getValue<int>("cmdtest"_jptr) == 5);
    REQUIRE(config.getValue<int>("cmdtest2"_jptr) == 4);
  }
}
