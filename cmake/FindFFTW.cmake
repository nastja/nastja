# - Find FFTW
# Find the native FFTW includes and library

find_path (FFTW_INCLUDES fftw3.h ${FFTW_INC_SEARCHPATH})

if(FFTW_INCLUDE_PATH)
  set(FFTW_INCLUDE ${FFTW_INCLUDE_PATH})
endif(FFTW_INCLUDE_PATH)

if(FFTW_INCLUDE)
  include_directories( ${FFTW_INCLUDE})
endif(FFTW_INCLUDE)

find_library (FFTW_LIBRARIES_OMP fftw3_omp)
find_library (FFTW_LIBRARIES fftw3)

# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (FFTW DEFAULT_MSG FFTW_LIBRARIES FFTW_LIBRARIES_OMP FFTW_INCLUDES)

