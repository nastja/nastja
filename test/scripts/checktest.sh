#!/usr/bin/env bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

if [[ $# -eq 1 ]]; then
  TESTDIR="$(dirname "$1")"
else
  TESTDIR="$SCRIPTPATH/.."
fi

MD5SUM="$(which md5sum)"

# if [[ $# -eq 1 ]]; then
#   pushd $1 > /dev/null
# else
#   pushd . > /dev/null
# fi

file=$(find . -maxdepth 1 -name '*.json')

# check for uniq json-file
cnt=$(echo $file | wc -w)
if [[ $cnt -ne 1 ]]; then
  echo "Need one *.json file."
  popd > /dev/null
  exit 1
fi

# check if this is a test
if [ ! -f $TESTDIR/$file ]; then
  echo "'$file' not found in test dir '$TESTDIR'."
  popd > /dev/null
  exit 1
fi

# list of all possible platforms
platforms=("darwin" "linux")
# current platform
platform=$(uname | tr '[:upper:]' '[:lower:]')
# remove current platform from list
platforms=(${platforms[*]/$platform})

instructionSets=("scalar" "avx")
instructionSet=$(grep \"instructionSet\": run*.out | head -1 | cut -d \" -f4 | cut -d '/' -f1 | tr '[:upper:]' '[:lower:]')
instructionSets=(${instructionSets[*]/$instructionSet})

# get all testfiles
testfilebase=$(basename $file .json)
testfiles=($(ls $TESTDIR/hashes/$testfilebase.md5))

#test.avx.darwin.md5
#test.darwin.md5
#test.avx.md5
#test.md5
#remove other platform from testfiles
for element in "${platforms[@]}"; do
  testfiles=(${testfiles[@]/*.${element}.md5})
done

#remove other instruction sets from testfiles
for element in "${instructionSets[@]}"; do
  testfiles=(${testfiles[@]/*.${element}*.md5})
done

if [[ ${#testfiles[@]} -eq 0 ]]; then
  popd > /dev/null
  echo "Can't find testfiles"
  exit 1
fi

# check all testfiles
ret=0
for file in "${testfiles[@]}"; do
  echo "Checking $file":
  $MD5SUM -c $file | grep -v "OK$"

  #get failor return
  currentret=${PIPESTATUS[0]}
  if [[ currentret -ne 0 ]]; then
    ret=$currentret
  fi
done

popd > /dev/null

exit $ret
