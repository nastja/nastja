
import glob
import json
from run import parse_config
from run import check_dict

class colors:
  GREEN = '\033[32;1m'
  RED = '\033[31;1m'
  ENDC = '\033[0m'

def print_diffs(test_path, tmp_path, verbose=False, failed_only=False):
  error_files = glob.glob(tmp_path + "/**/*_diff.json", recursive=True)
  error_boundaries = ""
  files_passed = 0

  error_files = sorted(error_files, key=lambda x: int(x.split("_")[-3].split("-")[-1]))

  for file in error_files:
    with open(file) as f:
      diff = json.load(f)
    with open(test_path) as f:
      config = parse_config(test_path)
      suffix = file[-13:-10]
      if config.get("error boundaries"):
        error_boundaries = config.get("error boundaries").get(suffix) or []
      else:
        if suffix == "csv":
          error_boundaries = {key: {
            "Deviations": 0,
            "MSE": 0,
            "MAE": 0,
            "Max": 0
          } for key in diff}
        elif suffix == "stl":
          error_boundaries = {key: {
            "Deviations": 0,
            "MSE": 0,
            "MAE": 0,
            "Max": 0
          } for key in diff}
        else:
          error_boundaries = {
            "Deviations": 0,
            "MSE": 0,
            "MAE": 0,
            "Max": 0
          }
    if check_dict(diff, error_boundaries, verbose=False) == 0:
      files_passed += 1
      if not failed_only and not verbose:
        print(file + colors.GREEN + " [Passed]" + colors.ENDC)
    if (verbose and not failed_only) or check_dict(diff, error_boundaries, verbose=False) != 0:
      print(file + ":")
      print_dict(diff, error_boundaries, verbose)
  print()
  if len(error_files) > 0:
    print(str(files_passed) + "/" + str(len(error_files)) + " files passed")
  else:
    print("No diff files were found. Please check that the test case exists in the testrepo and rerun the test.")

def print_dict(diff, boundary, verbose, left_padding="  "):
  for key in diff:
    color_start = ""
    color_end = ""

    if key in boundary:
      if type(diff[key]) is dict:
        if verbose or check_dict(diff[key], boundary[key], verbose=False) != 0:
          print(left_padding + key + ":")
          print_dict(diff[key], boundary[key], verbose, left_padding + "  ")
      else:
        if diff[key] > boundary[key]:
          color_start = colors.RED
        else:
          color_start = colors.GREEN
        color_end = colors.ENDC
        if verbose or color_start == colors.RED:
          print(left_padding + key + ":" + color_start + str(diff[key]) + color_end)
    elif verbose:
      if type(diff[key]) is dict:
        print(left_padding + key + ":")
        print_dict(diff[key], boundary, verbose, left_padding + "  ")
      else:
        if diff[key] == 0:
          color_start = colors.GREEN
          color_end = colors.ENDC

        print(left_padding + key + ":" + color_start + str(diff[key]) + color_end)
