#!/usr/bin/env python3
from pathlib import Path
import shutil
import subprocess
import sys
import os
import glob

_, new_data, old_data, test_case_path = sys.argv

data_name = Path(old_data).name
test_data_path = Path(old_data).parent

subprocess.run(["git checkout master"], shell=True, cwd=test_data_path)
subprocess.run(["git checkout -B " + data_name], shell=True, cwd=test_data_path)
subprocess.run(["git pull origin " + data_name], shell=True, cwd=test_data_path)

#replace data
if os.path.isdir(old_data):
  shutil.rmtree(old_data)
shutil.copytree(new_data, old_data)

os.remove(old_data + "/run1.err")
os.remove(old_data + "/run1.out")
os.remove(old_data + "/outdir/config_save.json")
shutil.rmtree(old_data + '/outdir/timing', ignore_errors=True)

error_files = glob.glob(old_data + "/**/*_diff.json", recursive=True)
for file in error_files:
  os.remove(file)

subprocess.run(["git add " + data_name], shell=True, cwd=test_data_path)
subprocess.run(["git commit -m \"updated test data: " + data_name + "\" -- " + data_name], shell=True, cwd=test_data_path)
subprocess.run(["git push --set-upstream origin " + data_name], shell=True, cwd=test_data_path)

#replace sha
sha_path = Path(test_case_path).joinpath("hashes")
print(sha_path)
sha = subprocess.run(["git rev-parse HEAD"], stdout=subprocess.PIPE, shell=True, cwd=test_data_path).stdout.decode("ascii")
with open(sha_path.joinpath(data_name + ".sha"), "w+") as f:
  f.write(sha)

#replace md5
subprocess.run([test_case_path + "/scripts/accepttest.sh"], cwd=old_data, shell=True)

subprocess.run(["git checkout master"], shell=True, cwd=test_data_path)
