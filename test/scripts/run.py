#!/usr/bin/env python3
import sys
import os
import shutil
from pathlib import Path
import json
import subprocess
from compareCSV import diff as csv_diff
from compareSTL import diff as stl_diff
from compareVTI import diff as vti_diff
import glob
import time
import configparser

def run(solver, test_path, skip_test=False, sha="", skip_md5=False, skip_nearly=False):
  tmp_path = Path("tmp_" + test_path.stem).absolute()
  tmp_path.mkdir(parents=True, exist_ok=True)
  shutil.copyfile(test_path, tmp_path.joinpath(test_path.name))
  if not tmp_path.joinpath("configinclude/").exists:
    shutil.copytree(test_path.parent.joinpath("configinclude/"), tmp_path.joinpath("configinclude/"))

  config = parse_config(test_path)
  print(config["description"])

  run_nastja(solver, test_path, tmp_path, config)

  if "pretestexecution" in config:
    subprocess.run(config["pretestexecution"], cwd=tmp_path, stdout=subprocess.PIPE, shell=True)

  #MD5 test
  if not skip_test:
    if not skip_md5:
      result = subprocess.run([test_path.parent.joinpath("scripts/checktest.sh")], cwd=str(tmp_path)).returncode
    else:
      result = 1

    if skip_nearly:
      return result

    conf_path =  Path(os.getcwd() + "/test.conf")
    if not conf_path.is_file():
      conf_path = Path.home().joinpath(".config/nastja/test.conf")
      if not conf_path.is_file():
        print(str(conf_path) + " does not exist.")
        return result
    parser = configparser.ConfigParser()
    parser.read(conf_path)
    data_path = Path(parser["DEFAULT"]["DataPath"]).joinpath("testdata")

    if not data_path.is_dir():
        if not data_path.parent.is_dir():
          os.makedirs(data_path.parent)
        subprocess.run(["git clone https://gitlab.com/nastja/testdata.git"], cwd=data_path.parent, stdout=subprocess.PIPE, shell=True)

    if result != 0:
      if sha == "":
        sha_path = test_path.parent.joinpath("hashes/" + test_path.stem + ".sha")
        if not sha_path.is_file():
          print("No sha file found")
          return 1

        with open(sha_path, "r") as f:
          sha = f.read()
      result    = nearly_equal_test(test_path, tmp_path, config, data_path, sha)
    return result
  else:
    return 1


def parse_config(test_path):
  with open(test_path) as f:
    config = json.load(f)["#Testing"]
  if "preset" in config:
    with open(test_path.parent.joinpath("presets", config["preset"] + ".json")) as f:
      preset = json.load(f)

      def parse_preset(preset, config):
        result = config
        for key in preset:
          if key not in config:
            result[key] = preset[key]
            continue

          if type(preset[key]) is dict:
            result[key] = parse_preset(preset[key], config[key])
            
        return result

      config = parse_preset(preset, config)
  return config


def run_nastja(solver, test_path, tmp_path, config):
  mpi_processes = 0
  if "mpi" in config:
    mpi_processes = int(config["mpi"])
  mpi_run = ""
  if mpi_processes > 0:
    parameter = ""
    if os.geteuid() == 0:
      parameter += " --allow-run-as-root"
    if "Open MPI" in subprocess.run(["mpirun", "-V"], stdout=subprocess.PIPE).stdout.decode("ascii"):
      parameter += " --oversubscribe"
    mpi_run = "mpirun" + parameter + " -n " + str(mpi_processes)

  i = 1
  runs = config.get("flags") or [""]
  for flags in runs:
    print("Run: " + str(i) + "/" + str(len(runs)))
    print(" ".join([mpi_run, str(solver), "-c", str(test_path), flags]))
    result = subprocess.run(" ".join([mpi_run, str(solver), "-c", str(test_path), flags]), cwd=str(tmp_path), shell=True)
    print("Calling:" + result.args)
    with open(tmp_path.joinpath("run" + str(i) + ".out"), "w+") as f:
      f.write(str(result.stdout))
    with open(tmp_path.joinpath("run" + str(i) + ".err"), "w+") as f:
      f.write(str(result.stderr))
    i += 1


def nearly_equal_test(test_path, tmp_path, config, data_path, sha):
  subprocess.run(["git fetch "], cwd=data_path, stdout=subprocess.PIPE, shell=True)
  if subprocess.run(["git checkout " + sha], cwd=data_path, stdout=subprocess.PIPE, shell=True).returncode != 0:
    if subprocess.run(["git fetch"], cwd=data_path, stdout=subprocess.PIPE, shell=True).returncode != 0:
      print("git fetch failed")
      return 1
    # retry checkout after successful fetch
    if subprocess.run(["git checkout " + sha], cwd=data_path, stdout=subprocess.PIPE, shell=True).returncode != 0:
      print("git checkout failed")
      return 1

  result = 0
  filePaths = [(path, data_path.joinpath(test_path.stem, str(path).replace(str(tmp_path), "")[1:])) for path in tmp_path.rglob("*.stl")]
  filePaths.extend([(path, data_path.joinpath(test_path.stem, str(path).replace(str(tmp_path), "")[1:])) for path in tmp_path.rglob("*.vti")])
  filePaths.extend([(path, data_path.joinpath(test_path.stem, str(path).replace(str(tmp_path), "")[1:])) for path in tmp_path.rglob("*.csv")])
  for file in filePaths:
    print("Check difference between:")
    print(file[0])
    print(file[1])
    suffix = file[0].suffix[1:]
    if suffix == "stl":
      start = time.time()
      stl_diff(file[0], file[1])
      end = time.time()
      print("%.2f s" % (end-start))
    elif suffix == "vti":
      start = time.time()
      vti_diff(file[0], file[1])
      end = time.time()
      print("%.2f s" % (end-start))
    elif suffix == "csv":
      start = time.time()
      csv_diff(file[0], file[1])
      end = time.time()
      print("%.2f s" % (end-start))

    diff_path = Path(file[0]).with_name(str(Path(file[0]).stem) + "_" + suffix + "_diff.json")
    if not diff_path.is_file():
      print("Couldn't find " + str(diff_path))
      return 1
    with open(diff_path, "r") as f:
      error_values = json.load(f)

    if config.get("error boundaries"):
      error_boundaries = config.get("error boundaries").get(suffix) or []
      print(result)
      result = max(check_dict(error_values, error_boundaries), result)
    else:
      result = 1
  subprocess.run(["git checkout master"], cwd=data_path, shell=True)
  return result

def check_dict(data, boundary, verbose=True):
      result = 0
      for key in boundary:
        if type(data[key]) is dict:
          result = max(check_dict(data[key], boundary[key], verbose), result)
        else:
          if data[key] > boundary[key]:
            if verbose:
              print(key + " test failed with an error of " + str(data[key]))
            result = 1
      return result

if __name__ == '__main__':
  _, solver, test_path, enable_nearly_tests = sys.argv
  solver = Path(os.path.abspath(solver))
  test_path = Path(os.path.abspath(test_path))

  result = run(solver, test_path, skip_nearly=(enable_nearly_tests == "OFF"))
  tmp_path = Path(solver.parent.joinpath("test", "tmp_" + test_path.stem)).absolute()
  print(tmp_path)
  if result == 0:
    print("Test passed, delete test data...")
    shutil.rmtree(tmp_path)

  sys.exit(result)
