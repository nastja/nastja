#!/usr/bin/env bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
TESTDIR="$SCRIPTPATH/.."

if [[ $# -eq 1 ]]; then
  pushd tmp_$(basename $1 .json)
else
  pushd .
fi

file=$(find . -maxdepth 1 -name '*.json')

# check for uniq json-file
cnt=$(echo $file | wc -w)
if [[ $cnt -ne 1 ]]; then
  echo "Need one *.json file."
  popd
  exit 1
fi

# check if this is a test
if [ ! -f $TESTDIR/$file ]; then
  echo \'$file\' not found in test dir \'$TESTDIR\'.
  popd
  exit 1
fi

# make md5 file and save it.
file=$(basename $file json)md5
# only non zero byte files and not the config_save.json
find outdir -type f ! -path 'outdir/timing/*' ! -name 'config_save.json' ! -name 'run.out' ! -name 'run.err' ! -name '*.log' ! -size 0 -print0 | sort -z | xargs -0 md5sum > $TESTDIR/hashes/$file
echo $file updated.
popd
