import pandas as pd
import numpy as np
from pathlib import Path
import json

#calculates the error for every column
def diff(sim_path, test_path):
  sim_data = pd.read_csv(sim_path, sep = " ")
  test_data = pd.read_csv(test_path, sep = " ")
  result = {}
  for key in sim_data:
    if (sim_data[key].dtype == 'object'):
      difference = (sim_data[key] != test_data[key])
      percentage_difference = 0
    else:
      difference = sim_data[key] - test_data[key]
      percentage_difference = difference / sim_data[key]
    result[key] = {
      "Deviations": len([x for x in difference if x != 0]) / len(sim_data[key]),
      "MSE": float(np.square(difference).mean()),
      "MAE": float(np.absolute(difference).mean()),
      "MAPE": float(np.absolute(percentage_difference).mean()),
      "Max": float(max(difference))
    }
  with open(Path(sim_path).with_name(Path(sim_path).stem + "_csv_diff.json"), "w+") as f:
    json.dump(result, f)
