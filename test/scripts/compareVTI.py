import glob, os
import subprocess
import vtk
import vtk.util.numpy_support as VN
import numpy as np
import sys
from pathlib import Path
import json

def diff(sim_path, test_path, verbose=False):
  # create a reader
  reader = vtk.vtkXMLImageDataReader()

  # read file0
  reader.SetFileName(str(sim_path))
  reader.Update()
  extend0 = reader.GetOutput().GetExtent()
  spacing0 = reader.GetOutput().GetSpacing()
  origin0 = reader.GetOutput().GetOrigin()
  phi0 = VN.vtk_to_numpy(reader.GetOutput().GetCellData().GetAbstractArray(0))

  # read file1
  reader.SetFileName(str(test_path))
  reader.Update()
  extend1 = reader.GetOutput().GetExtent()
  spacing1 = reader.GetOutput().GetSpacing()
  origin1 = reader.GetOutput().GetOrigin()
  phi1 = VN.vtk_to_numpy(reader.GetOutput().GetCellData().GetAbstractArray(0))

  # check similarity of file geometry
  if spacing0 != spacing1:
    return sys.float_info.max

  if origin0 != origin1:
    return sys.float_info.max

  if extend0 != extend1:
    return sys.float_info.max

  difference = (phi0 - phi1).flatten()

  result = {
    "Deviations": len([x for x in difference if x != 0]) / len(phi0),
    "MSE": float(np.square(difference).mean()),
    "MAE": float(np.absolute(difference).mean()),
    "Max": float(max(difference))
  }

  with open(Path(sim_path).with_name(Path(sim_path).stem + "_vti_diff.json"), "w+") as f:
    json.dump(result, f)

  if(verbose):
    print("Total number of deviations: " + str(len([x for x in difference if x != 0])))
    print("MSE: " + str(np.square(difference).mean()))
    print("MAE: " + str(np.absolute(difference).mean()))
    print("Max: " + str(max(difference)))
