NAStJA system tests run NAStJA with each config file in the test folder.
Afterwards, the resulting data and previously generated test data are compared.
At first, the MD5 of the data is compared.
In case of deviation a nearly equal test is used to determine wether the test shall pass.

To create a new test, just add a new config file and a file with the MD5 hashes of the correctly executed simulation data.
The config file should have an additional field "#Testing" with the child "description".
If a nearly equal test should be supported, add test data to the data folder and add "error boundaries" to "#Testing" in the config file.
Under "error boundaries" specifiy wich error type for which data format should be used.
Currently, mean squared error("MSE"), mean absolute error("MAE") and maximal error("MAX") are the only supported error types.
Instead of "error boundaries" an preset can be used.
The presets are in the data/preset folder.

After the test is created use the NAStJA test shell to create the test data, i.e., execute the accept command with the name of your test. The data can be changed in case of code changes with the same command.
Afterwards, the NAStJA test shell can be helpful to narrow down the error boundaries. For example, after building NAStJa with different flags, the show command shows the error between the resulting data and the previously accepted test data.

# Prerequisits
- git-lfs
- python3
- python libraries:
  - vtk
  - numpy
  - pandas

# NAStJA test shell (ntest)
Installing the NAStJA test shell:
```
ln -s ~/workspace/nastja/test/scripts/ntest.py ~/bin/ntest
```
The test shell then can be started by executing `ntest` from the build folder.
Every test shell command can be exectued without starting the test shell by passing the command as argument.

## Test shell commands
| Command | Flags                | Arguments              | Description |
|---------|----------------------|------------------------|-------------|
| accept  |                      | Test name              | Replace the current test data with the data of the test execution and commit it to the test data submodule. |
| clean   |                      |                        | Removes all data generated with tests. |
| compare |                      | Test name, branch name | Compares the result data of the test with the test data of the branch. |
| list    |                      |                        | List all the test that are available. Only tests in the test folder can be executed by the shell. |
| run     |                      | Test name              | Execute the test and saves the result in a "tmp_" folder. The folder is created where the shell is executed. |
| runall  |                      |                        | Execute all tests. |
| show    | verbose, failed-only | Test name              | Print all diff files for the test. |
