file(GLOB files "${CMAKE_CURRENT_SOURCE_DIR}/*.json")

if(NOT DEFINED DISABLE_NEARLY_EQUAL_TEST)
    set(DISABLE_NEARLY_EQUAL_TEST "False")
endif()

# Remove option-dependent tests
if(NOT NASTJA_ENABLE_PFC)
  file(GLOB pfcfiles "${CMAKE_CURRENT_SOURCE_DIR}/*pfc*.json")
  list(REMOVE_ITEM files ${pfcfiles})
endif()

if(NOT NASTJA_ENABLE_CUDA)
  file(GLOB cudafiles "${CMAKE_CURRENT_SOURCE_DIR}/*cuda*.json")
  list(REMOVE_ITEM files ${cudafiles})
endif()

find_program(JQ_EXECUTABLE jq REQUIRED)

if(${CMAKE_VERSION} VERSION_LESS "3.12.0")
  message("Please check python dependencies")
else()
  find_package(Python REQUIRED)
  include(FindPythonModule)
  find_python_module(vtk REQUIRED)
  find_python_module(numpy REQUIRED)
  find_python_module(pandas REQUIRED)
endif()

list(SORT files)
foreach(file ${files})
  string(REGEX MATCH /[^/]*$ testname ${file})  # basename
  string(SUBSTRING ${testname} 1 -1 testname)
  string(REGEX MATCH ^[^_.]* label ${testname})  # part left of the first _

  add_test(NAME "simtest:${testname}" COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/llvm_annotate.sh python3 ${NAStJA_SOURCE_DIR}/test/scripts/run.py ${NAStJA_BINARY_DIR}/nastja ${file} ${NASTJA_ENABLE_NEARLY_EQUAL_TEST})
  set_tests_properties("simtest:${testname}" PROPERTIES
      LABELS "simulation;${label}"
      TIMEOUT "180")

  if (JQ_EXECUTABLE)
    # append number of parallel processors
    execute_process(COMMAND bash -c "${JQ_EXECUTABLE} -e '.\"#Testing\".mpi' ${file}"
        RESULT_VARIABLE ret
        OUTPUT_VARIABLE num_processors)
    string(STRIP ${num_processors} num_processors)
    if (NOT ${ret})
      set_tests_properties("simtest:${testname}" PROPERTIES
          PROCESSORS ${num_processors}
          LABELS "simulation;${label};parallel")
    endif()
  endif()
endforeach()
