/* Licensed as Public Domain */
#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static void dump_trace() {
  void *buffer[255];
  const int calls = backtrace(buffer, sizeof(buffer) / sizeof(void *));
  backtrace_symbols_fd(buffer, calls, 1);
  exit(EXIT_FAILURE);
}

void setup_signals() {
  signal(SIGSEGV, dump_trace);
}