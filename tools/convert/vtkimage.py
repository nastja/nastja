#!/usr/bin/env python
import os, sys
if not os.environ['LD_LIBRARY_PATH'] == '/usr/lib/openmpi/lib/':
  os.environ['LD_LIBRARY_PATH'] = '/usr/lib/openmpi/lib/'
  os.execv(sys.argv[0], sys.argv)

import vtk
import numpy as np
from vtk.util.numpy_support import vtk_to_numpy
from scipy.misc import imsave

import argparse

parser = argparse.ArgumentParser(description='Convert vtk to png.')
parser.add_argument(dest='file')
args = parser.parse_args()
filename = args.file
output = filename[filename.rfind('-')+1:-4] + '.png'

# Read the file
reader = vtk.vtkXMLImageDataReader()
reader.SetFileName(filename)
reader.Update()

# Convert the image to numpy
data = reader.GetOutput()
rows, cols, _ = data.GetDimensions()
sc = data.GetCellData().GetScalars()
a = vtk_to_numpy(sc)

# Z, Y, X
a = a.reshape(-1, cols-1, rows-1)

max = np.max(a)
min = np.min(a)

zlayer=0
x = (a[zlayer,:,:] - min) * 255/(max-min).astype(np.uint8)

#Output:
# .-->
# |   x
# v
#  y
imsave(output, x)
