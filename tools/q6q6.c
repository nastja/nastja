/* file flann_example.c */
// #define FLANN_USE_CUDA
#include "exception.h"
#include "q6q6_cmdline.h"
#include "string.h"
#include <assert.h>
#include <complex.h>
#include <flann/flann.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
  * @brief swap float value to big endian
  *
  * Paraview's vtk format want's all binary data in big-endian!
  */
static float endian_swap(float f) {
  union {
    float f;
    unsigned char b[4];
  } in, out;

  in.f     = f;
  out.b[0] = in.b[3];
  out.b[1] = in.b[2];
  out.b[2] = in.b[1];
  out.b[3] = in.b[0];
  return out.f;
}

static short endian_swap_short(short f) {
  union {
    short f;
    unsigned char b[2];
  } in, out;

  in.f     = f;
  out.b[0] = in.b[1];
  out.b[1] = in.b[0];

  return out.f;
}

static int readheader(FILE *file, int *numpoints) {
  char linebuf[1024];

  int major, minor;

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (sscanf(linebuf, "# vtk DataFile Version %d.%d", &major, &minor) != 2) return 0;
  // myverbose(VERBOSE_NORMAL, verbose, "vtk DataFile Version %d.%d", major, minor);

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  // myverbose(VERBOSE_NORMAL, verbose, "The commentline says:\n%s", linebuf);

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (!strcmp(linebuf, "BINARY")) {
    // mywarn("This is not a BINARY format.");
    return 0;
  }
  // myverbose(VERBOSE_NORMAL, verbose, "It is a BINARY format");

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (!strcmp(linebuf, "DATASET POLYDATA")) {
    // mywarn("This is not a DATASET POLYDATA format.");
    return 0;
  }
  // myverbose(VERBOSE_NORMAL, verbose, "It is a DATASET POLYDATA format.");

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (sscanf(linebuf, "POINTS %d short", numpoints) != 1) return 0;

  // myverbose(VERBOSE_NORMAL, verbose, "There are %ld POINTS.", *numpoints);

  return 1;
}

static int readadditionaldata_header(FILE *file, int numpoints, char *dataname) {
  char linebuf[1024];
  char name[1024];
  int tmpnumpoints;

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;

  if (sscanf(linebuf, "POINT_DATA %d", &tmpnumpoints) != 1) return 0;
  if (tmpnumpoints != numpoints) {
    // mywarn("POINTS and DATA mismatch.");
    return 0;
  }

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (sscanf(linebuf, "SCALARS %s float", name) != 1) return 0;
  if (strcmp(name, dataname) != 0) {
    // mywarn("This is not a %s float: '%s'", dataname, name);
    return 0;
  }

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (!strcmp(linebuf, "LOOKUP_TABLE default")) {
    // mywarn("This is not a LOOKUP_TABLE default.");
    return 0;
  }

  // myverbose(VERBOSE_NORMAL, verbose, "There are additionaldata: %s.", name);

  return 1;
}

static int *readfile(FILE *file, int *numpoints) {
  if (!readheader(file, numpoints)) return NULL;

  int *dataset = malloc(*numpoints * 3 * sizeof(int));
  for (int i = 0; i < 3 * *numpoints; i++) {
    unsigned short x;
    fread(&x, sizeof(unsigned short), 1, file);
    dataset[i] = endian_swap_short(x);
  }
  return dataset;
}

static float *readadditionaldata(FILE *file, int numpoints) {
  if (!readadditionaldata_header(file, numpoints, "size")) return NULL;
  float *data = malloc(numpoints * sizeof(float));
  for (int i = 0; i < numpoints; i++) {
    float value;
    fread(&value, sizeof(float), 1, file);
    data[i] = endian_swap(value);
  }
  return data;
}

#define BINARYOUT

static void writefile(int *dataset, float *data_size, double *data_q6q6, long numpoints, char *filename) {
  FILE *fout;
  if ((fout = fopen(filename, "w")) == NULL) {
    printf("bal %s\n", filename);
    return;
  }
  fprintf(fout,
          "# vtk DataFile Version 1.0\n"
          "vtk output\n"
#ifdef BINARYOUT
          "BINARY\n"
#else
          "ASCII\n"
#endif
          "DATASET POLYDATA\n"
          "POINTS %ld short\n",
          numpoints);
  for (int i = 0; i < numpoints; i++) {
// printf("%d %d %d %f %d\n", atom[i].x, atom[i].y, atom[i].z, atom[i].v, atom[i].pad);

#ifdef BINARYOUT
    unsigned short x = endian_swap_short((unsigned short)dataset[3 * i + 0]);
    unsigned short y = endian_swap_short((unsigned short)dataset[3 * i + 1]);
    unsigned short z = endian_swap_short((unsigned short)dataset[3 * i + 2]);
    fwrite(&x, sizeof(unsigned short), 1, fout);
    fwrite(&y, sizeof(unsigned short), 1, fout);
    fwrite(&z, sizeof(unsigned short), 1, fout);
#else
    fprintf(fout, "%d %d %d\n", (unsigned short)points[i].coord[X], (unsigned short)points[i].coord[Y], (unsigned short)points[i].coord[Z]);
#endif
  }
  fprintf(fout, "\nPOINT_DATA %ld\nSCALARS size float\nLOOKUP_TABLE default\n", numpoints);
  for (int i = 0; i < numpoints; i++) {
#ifdef BINARYOUT
    float value = endian_swap(data_size[i]);
    fwrite(&value, sizeof(float), 1, fout);
#else
    fprintf(fout, "%f\n", (float)data->size);
#endif
  }
  fprintf(fout, "\nSCALARS q6q6 float\nLOOKUP_TABLE default\n");
  for (int i = 0; i < numpoints; i++) {
#ifdef BINARYOUT
    float value = endian_swap((float)data_q6q6[i]);
    fwrite(&value, sizeof(float), 1, fout);
#else
    fprintf(fout, "%f\n", (float)data->q6q6);
#endif
  }

#ifdef BINARYOUT
  fprintf(fout, "\n");
#endif
  fclose(fout);
}

static double complex Y6m(double theta, double phi, int m) {
  // factor
  double N = 0.0, P = 0.0;
  double s, c = 0.0, s2, c2 = 0.0;
  int absm = abs(m);

  if (absm < 6) {
    c  = cos(theta);
    c2 = c * c;
  }
  if (absm > 0) {
    s  = sin(theta);
    s2 = s * s;
  }

  switch (absm) {
    case 0:
      N = sqrt(13.0 / M_PI) / 32.0;
      P = ((231.0 * c2 - 315.0) * c2 + 105.0) * c2 - 5.0;
      break;
    case 1:
      N = sqrt(0.5 * 273.0 / M_PI) / 16.0;
      P = s * ((33.0 * c2 - 30.0) * c2 + 5.0) * c;
      break;
    case 2:
      N = sqrt(1365.0 / M_PI) / 64.0;
      P = s2 * ((33.0 * c2 - 18.0) * c2 + 1.0);
      break;
    case 3:
      N = sqrt(1365.0 / M_PI) / 32.0;
      P = s2 * s * (11.0 * c2 - 3.0) * c;
      break;
    case 4:
      N = 3.0 * sqrt(0.5 * 91.0 / M_PI) / 32.0;
      P = s2 * s2 * (11.0 * c2 - 1.0);
      break;
    case 5:
      N = 3.0 * sqrt(1001.0 / M_PI) / 32.0;
      P = s * s2 * s2 * c;
      break;
    case 6:
      N = sqrt(3003.0 / M_PI) / 64.0;
      P = s2 * s2 * s2;
      break;
  }

  if (m > 0 && m % 2 == 1) N *= -1.0;  // for positive odds
  return N * P * cexp(I * m * phi);
}

static inline double vec_norm(double v[3]) {
  double dst = 0.0;
  for (int i = 0; i < 3; i++) dst += v[i] * v[i];
  return sqrt(dst);
}

/** Calculate azimuthal angle.
 *
 * @param[in] v     vector with orientation (x,y,z)
 *
 * @return angle in radiant.
 */
static inline double vec_angle_azimut(double v[3]) {
  return atan2(v[1], v[0]);
}
/** Calculate polar angle.
 *
 * @param[in] v     vector with orientation (x,y,z)
 *
 * @return angle in radiant.
 */
static inline double vec_angle_polar(double v[3]) {
  return acos(v[2] / vec_norm(v));
}

int main(int argc, char *argv[]) {
  char *filein;
  char *fileout;
  FILE *fin;

  float cutoff;

  setup_signals();

  struct gengetopt_args_info ai;
  if (cmdline_parser(argc, argv, &ai) != 0) {
    exit(1);
  }

  cutoff = ai.cutoff_arg;
  printf("Cutoff: %f\n", cutoff);
  cutoff *= cutoff;

  if (ai.inputs_num) {
    printf("FILE IN:  %s\n", ai.inputs[0]);
    filein = ai.inputs[0];
    if (ai.inputs_num > 1) {
      printf("FILE OUT: %s\n", ai.inputs[1]);
      fileout = ai.inputs[1];
    }
  } else {
    printf("I need an input file.\n");
  }

  if ((fin = fopen(filein, "r")) == NULL) {
    printf("Can't open file '%s'\n", filein);
    exit(1);
  }

  int rows, cols = 3;
  // int t_rows, t_cols;
  float speedup;

  /* read dataset points from file dataset.dat */
  int *dataset     = readfile(fin, &rows);
  float *data_size = readadditionaldata(fin, rows);

  /* points in dataset and testset should have the same dimensionality */
  // assert(cols==t_cols);

  /* number of nearest neighbors to search */
  int nn = 3;

  /* allocate memory for the nearest-neighbors indices */
  int *result = (int *)malloc(rows * nn * sizeof(int));
  /* allocate memory for the distances */
  float *dists = (float *)malloc(rows * nn * sizeof(float));

  /* index parameters are stored here */
  struct FLANNParameters p = DEFAULT_FLANN_PARAMETERS;
  // p.algorithm = FLANN_INDEX_AUTOTUNED; // or FLANN_INDEX_KDTREE, FLANN_INDEX_KMEANS, ...
  // p.algorithm = FLANN_INDEX_KDTREE; // or FLANN_INDEX_KDTREE, FLANN_INDEX_KMEANS, ...
  // p.algorithm = FLANN_INDEX_KMEANS;
  p.algorithm = FLANN_INDEX_KDTREE_SINGLE;
  // p.algorithm = FLANN_INDEX_KDTREE_CUDA;
  // p.target_precision = 1.0; // want 90% target precision
  p.checks = -1;
  // p.cores = 0;

  flann_index_t index = flann_build_index_int(dataset, rows, cols, &speedup, &p);
  printf("Build up index by factor %e.\n", speedup);

  int max_nn         = 100;
  int neighbors_size = 1024;
  int *neighbors     = malloc(neighbors_size * sizeof(int));

  float *distsn = malloc(max_nn * sizeof(float));

  int *neighbor_start = malloc((rows + 1) * sizeof(int));

  double complex *Q6msum = calloc(rows * 13, sizeof(double complex));
  double *q6q6           = calloc(rows, sizeof(double));

  int max           = 0;
  int sum           = 0;
  neighbor_start[0] = 0;
  double distVector[3];
  for (int i = 0; i < rows; i++) {
    // Buffer expand
    if (neighbor_start[i] + max_nn > neighbors_size) {
      neighbors_size *= 2;
      neighbors = realloc(neighbors, neighbors_size * sizeof(int));
      printf("Resize Buffer: %d\n", neighbors_size);
    }

    // Find Neighbors
    int neighbors_num     = flann_radius_search_int(index, &dataset[3 * i], &neighbors[neighbor_start[i]], distsn, max_nn, cutoff, &p);
    neighbor_start[i + 1] = neighbor_start[i] + neighbors_num;
    // printf("%d,%d,%d: %d\n", dataset[3*i + 0], dataset[3*i + 1], dataset[3*i + 2], neighbors_num);

    // Calculate Q6m
    int pointcnt = 0;
    for (int j = neighbor_start[i]; j < neighbor_start[i] + neighbors_num; j++) {
      for (int k = 0; k < 3; k++) {
        distVector[k] = dataset[3 * neighbors[j] + k] - dataset[3 * i + k];
      }

      if (vec_norm(distVector) == 0.0) continue;  // Same points

      for (int m = -6; m <= 6; m++) {
        Q6msum[i * 13 + m + 6] += Y6m(vec_angle_polar(distVector), vec_angle_azimut(distVector), m);
      }
      pointcnt++;
    }

    for (int m = -6; m <= 6; m++) {
      Q6msum[i * 13 + m + 6] /= (double)pointcnt;
    }

    max = (int)fmax(max, neighbors_num);
    sum += neighbors_num;
  }
  printf("Rows:%d\nNeighbors %d\n", rows, neighbor_start[rows]);

  //Calculate q6q6
  for (int i = 0; i < rows; i++) {
    double sumQ6m_i = 0.0;
    double normQ6m_i, normQ6m_j, cabsq6;
    // printf("A");
    double complex *Q6_i = &Q6msum[i * 13 + 6];
    // printf("B");
    normQ6m_i = 0.0;
    for (int m = -6; m <= 6; m++) {
      cabsq6 = cabs(Q6_i[m]);
      normQ6m_i += cabsq6 * cabsq6;
    }
    normQ6m_i = sqrt(normQ6m_i);

    int pointcnt = 0;
    // printf("C");
    // printf("j: %d .. %d, %d\n", neighbor_start[i], neighbor_start[i+1], rows);
    for (int j = neighbor_start[i]; j < neighbor_start[i + 1]; j++) {
      // printf("D");
      // printf("n: %d\n", neighbors[j]);
      // continue;
      // exit(0);
      double complex *Q6_j = &Q6msum[neighbors[j] * 13 + 6];
      // printf("E");
      normQ6m_j = 0.0;
      sumQ6m_i  = 0.0;
      for (int m = -6; m <= 6; m++) {
        sumQ6m_i += creal(Q6_i[m] * conj(Q6_j[m]));
        cabsq6 = cabs(Q6_j[m]);
        normQ6m_j += cabsq6 * cabsq6;
      }
      q6q6[i] += sumQ6m_i / (normQ6m_i * sqrt(normQ6m_j));
      pointcnt++;
    }
    // printf("X");
    q6q6[i] /= (double)pointcnt;
    // printf("%d: %lf\n", i, q6q6[i]);
    // printf("Z\n");
  }

  printf("Max: %d\n", max);
  printf("Sum: %d\n", sum);
  if (fileout) {
    printf("Ausgabe:...\n");
    writefile(dataset, data_size, q6q6, rows, fileout);
  }

  free(dataset);
  free(result);
  free(dists);
  return 0;
}