/* file flann_example.c */
// #define FLANN_USE_CUDA
#include "atoms_cmdline.h"
#include "string.h"
#include "../exception.h"
#include <assert.h>
#include <complex.h>
#include <flann/flann.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
  * @brief swap float value to big endian
  *
  * Paraview's vtk format want's all binary data in big-endian!
  */
static float endian_swap(float f) {
  union {
    float f;
    unsigned char b[4];
  } in, out;

  in.f     = f;
  out.b[0] = in.b[3];
  out.b[1] = in.b[2];
  out.b[2] = in.b[1];
  out.b[3] = in.b[0];
  return out.f;
}

static short endian_swap_short(short f) {
  union {
    short f;
    unsigned char b[2];
  } in, out;

  in.f     = f;
  out.b[0] = in.b[1];
  out.b[1] = in.b[0];

  return out.f;
}

static int readheader(FILE* file, int* numpoints) {
  char linebuf[1024];

  int xstart, xend, ystart, yend, zstart, zend;

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (sscanf(linebuf, "<ImageData WholeExtent=\"%d %d %d %d %d %d\"", &xstart, &xend, &ystart, &yend, &zstart, &zend) != 6) return 0;
  *numpoints = (zend - zstart) * (yend - ystart) * (xend - xstart);

  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;
  if (fgets(linebuf, sizeof(linebuf), file) == NULL) return 0;

  char startchar;
  long length;
  fread(&startchar, sizeof(char), 1, file);
  if (startchar != '_') return 0;
  fread(&length, sizeof(long), 1, file);
  if (length != *numpoints * sizeof(float)) return 0;

  return 1;
}

static float* readfile(FILE* file, int* numpoints) {
  if (!readheader(file, numpoints)) return NULL;

  float* dataset = malloc(*numpoints * sizeof(float));
  fread(dataset, sizeof(float), *numpoints, file);

  return dataset;
}

#define BINARYOUT

static void writefile(int* dataset, float* data_size, long numpoints, char* filename) {
  FILE* fout;
  if ((fout = fopen(filename, "w")) == NULL) {
    printf("bal %s\n", filename);
    return;
  }
  fprintf(fout,
          "# vtk DataFile Version 1.0\n"
          "vtk output\n"
#ifdef BINARYOUT
          "BINARY\n"
#else
          "ASCII\n"
#endif
          "DATASET POLYDATA\n"
          "POINTS %ld short\n",
          numpoints);
  for (int i = 0; i < numpoints; i++) {
// printf("%d %d %d %f %d\n", atom[i].x, atom[i].y, atom[i].z, atom[i].v, atom[i].pad);

#ifdef BINARYOUT
    unsigned short x = endian_swap_short((unsigned short)dataset[3 * i + 0]);
    unsigned short y = endian_swap_short((unsigned short)dataset[3 * i + 1]);
    unsigned short z = endian_swap_short((unsigned short)dataset[3 * i + 2]);
    fwrite(&x, sizeof(unsigned short), 1, fout);
    fwrite(&y, sizeof(unsigned short), 1, fout);
    fwrite(&z, sizeof(unsigned short), 1, fout);
#else
    fprintf(fout, "%d %d %d\n", (unsigned short)dataset[3 * i + 0], (unsigned short)dataset[3 * i + 1], (unsigned short)dataset[3 * i + 2]);
#endif
  }
  fprintf(fout, "\nPOINT_DATA %ld\nSCALARS size float\nLOOKUP_TABLE default\n", numpoints);
  for (int i = 0; i < numpoints; i++) {
#ifdef BINARYOUT
    float value = endian_swap(data_size[i]);
    fwrite(&value, sizeof(float), 1, fout);
#else
    fprintf(fout, "%f\n", (float)data_size[i]);
#endif
  }

#ifdef BINARYOUT
  fprintf(fout, "\n");
#endif
  fclose(fout);
}

float islocalPeak(float* dataset, int x, int y) {
  int index = y * 8192 + x;
  if (dataset[index] > dataset[index - 1] &&
      dataset[index] > dataset[index + 1] &&
      dataset[index] > dataset[index - 8192] &&
      dataset[index] > dataset[index + 8192])
    return dataset[index];
  return 0.0;
}

int main(int argc, char* argv[]) {
  char* filein;
  char* fileout;
  FILE* fin;

  float cutoff;

  setup_signals();

  struct gengetopt_args_info ai;
  if (cmdline_parser(argc, argv, &ai) != 0) {
    exit(1);
  }

  if (ai.inputs_num) {
    printf("FILE IN:  %s\n", ai.inputs[0]);
    filein = ai.inputs[0];
    if (ai.inputs_num > 1) {
      printf("FILE OUT: %s\n", ai.inputs[1]);
      fileout = ai.inputs[1];
    }
  } else {
    printf("I need an input file.\n");
  }

  if ((fin = fopen(filein, "r")) == NULL) {
    printf("Can't open file '%s'\n", filein);
    exit(1);
  }

  int rows, cols = 3;
  // int t_rows, t_cols;
  float speedup;

  /* read dataset points from file dataset.dat */
  float* dataset = readfile(fin, &rows);

  int* atoms   = malloc(rows * sizeof(int) * 3);
  float* sizes = malloc(rows * sizeof(float));

  int atomcnt = 0;
  for (int y = 1; y < 8191; y++) {
    for (int x = 1; x < 8191; x++) {
      float peak = islocalPeak(dataset, x, y);
      if (peak > 0.2) {
        atoms[3 * atomcnt + 0] = x;
        atoms[3 * atomcnt + 1] = y;
        atoms[3 * atomcnt + 2] = 0;
        sizes[atomcnt]         = peak;
        atomcnt++;
        printf("%d, %d, %d\n", x, y, 0);
      }
    }
  }

  if (fileout) {
    printf("Ausgabe:...\n");
    writefile(atoms, sizes, atomcnt, fileout);
  }

  return 0;
}