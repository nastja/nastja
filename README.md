The NAStJA Framework
==================
Neoteric Autonomous Stencil code for Jolly Algorithms

The NAStJA framework provides an easy way to enable massively parallel simulations for a wide range of multi-physics applications based on stencil algorithms.
It supports the development of parallel strategies for high-performance computing.
Modern C++ and the usage of template metaprogramming achieve excellent performance without losing ﬂexibility and usability.

# Prerequisits
- CMake 3.10+
- C++14 compliant compiler:
  - Clang 6, 7, 8, 9
  - GCC 7, 8
- Message Passing Interface (MPI) library

Optional, depending on the choosen module options:
- fftw
- OpenMP

Running tests and building the documentation has the following additional requirements:
- doxygen
- jq

# Quick Build
To build NAStJA, use the standard CMake procedure
```
mkdir build
cd build
cmake ..
make
```

## Compile Options
Use `NASTJA_ENABLE_AVX` to enable AVX vector support, `NASTJA_ENABLE_PFC` to build the phase-field crystal module,
`NASTJA_ENABLE_CUDA` to build with CUDA support. `NASTJA_ENABLE_FAST_MATH` enables the fast-math optimizations.
You can use `NASTJA_TYPE_CELLID` to change the cellid_t type, to support more than 4 billion cells.
`NASTJA_TYPE_FLOAT_PRECISION` changes the floating-point precision, default is double.

You can set `NASTJA_BUILD_TESTING` to enable tests even when you use NAStJA as a submodule.
Set `NASTJA_BUILD_SIMULATION_TESTS` to `OFF` to skip simulation tests.
You can disable the git submodule check with `NASTJA_GIT_SUBMODULE_CHECK` and disable the usage off ccache with `NASTJA_USE_CCACHE`.
For debugging purposes, you can output intermediate files with `NASTJA_SAVE_TEMPS`. Coverage instrumentation is controlled via `NASTJA_COVERAGE`.

For compiling on the fh2 add `source /opt/rh/devtoolset-4/enable` to `~/.bashrc` when using the intel compiler. Clang is working and gnu since v6.

## Enable clang-tidy linter and the language server protocol LSP
```
ln -s build/compile_commands.json .
```

# Please cite us
If you use NAStJA in a publication, please cite the following article:

> M. Berghoff, I. Kondov, and J. Hötzer. *Massively parallel stencil code solver with autonomous adaptive block distribution*. In: IEEE Transactions on Parallel and Distributed Systems, 2018. doi:[10.1109/TPDS.2018.2819672](https://doi.org/10.1109/TPDS.2018.2819672)

This repository and the software itself can be cite by:

> M. Berghoff, J. Rosenbauer, and N. Pfisterer. *The NAStJA Framework*. Version 1.0. 2020. doi:[10.5281/zenodo.3740079](https://dx.doi.org/10.5281/zenodo.3740079).

------------------
Mozilla Public License, v. 2.0 (MPL-2.0)

Copyright 2016 - 2020 Marco Berghoff and the NAStJA Core Developers

See [CONTRIBUTORS.md](CONTRIBUTORS.md) for a full list of contributors.
