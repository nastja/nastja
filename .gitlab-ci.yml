workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

cache:
   key: ccache
   paths:
   - ccache
   policy: pull-push

stages:
  - build
  - test
  - deployment

.template: &variables
  NASTJA_ENABLE_AVX: "ON"
  NASTJA_ENABLE_PFC: "ON"
  NASTJA_COVERAGE: "OFF"
  NASTJA_TYPE_FLOAT_PRECISION: "double"
  CMAKE_BUILD_TYPE: "Release"
  CXX: /usr/bin/clang++-9

.template: &variables_nvcc
  NASTJA_ENABLE_CUDA: "ON"
  NASTJA_ENABLE_PFC: "ON"
  NASTJA_COVERAGE: "OFF"
  NASTJA_TYPE_FLOAT_PRECISION: "double"
  CMAKE_BUILD_TYPE: "Release"
  CXX: /usr/bin/g++-8

.template: &script_build
  - mkdir -p ${CI_JOB_NAME}
  - cd ${CI_JOB_NAME}
  - cmake ..
      -DNASTJA_ENABLE_PFC=${NASTJA_ENABLE_PFC}
      -DNASTJA_COVERAGE=${NASTJA_COVERAGE}
      -DNASTJA_ENABLE_AVX=${NASTJA_ENABLE_AVX}
      -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
      -DNASTJA_TYPE_FLOAT_PRECISION=${NASTJA_TYPE_FLOAT_PRECISION}
      -DNASTJA_ENABLE_NEARLY_EQUAL_TEST="OFF"
      -G "Ninja"
  - ninja
  - ../scripts/ci/compress.sh

.template: &script_build_nvcc
  - mkdir -p ${CI_JOB_NAME}
  - cd ${CI_JOB_NAME}
  - cmake ..
      -DNASTJA_ENABLE_PFC=${NASTJA_ENABLE_PFC}
      -DNASTJA_COVERAGE=${NASTJA_COVERAGE}
      -DNASTJA_ENABLE_CUDA=${NASTJA_ENABLE_CUDA}
      -DCMAKE_CUDA_HOST_COMPILER=${CXX}
      -DCUDA_HOST_COMPILER=${CXX}
      -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
      -DNASTJA_ENABLE_NEARLY_EQUAL_TEST="OFF"
      -G "Ninja"
  - ninja
  - ../scripts/ci/compress.sh

.template: &script_test
  - cd ${CI_JOB_NAME/test/build}
  - tar xf artifacts.tar.gz
  - python3 ../test/scripts/ntest.py set_conf_and_data_path ${CI_JOB_NAME/test/build}/test ${CI_JOB_NAME/test/build}/test/data
  - ctest -T test --output-on-failure

.template: &script_after_test
  - cd ${CI_JOB_NAME/test/build}
  - xsltproc ../scripts/ci/ctest2junit.xsl Testing/$(head -n 1 < Testing/TAG)/Test.xml > junit_report_${CI_JOB_NAME}.xml
  - ../scripts/ci/junit_expand.sh junit_report_${CI_JOB_NAME}.xml

.template: &build
  stage: build
  image: registry.gitlab.com/nastja/nastja-build/build:clang-focal
  tags:
    - docker
  variables: *variables
  script: *script_build
  artifacts:
    paths:
    - ${CI_JOB_NAME}/artifacts.tar.gz
    expire_in: 1h
  before_script:
    - mkdir -p ccache
    - export CCACHE_BASEDIR=${PWD}
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --zero-stats || true
  after_script:
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --show-stats

.template: &build_gcc
  stage: build
  image: registry.gitlab.com/nastja/nastja-build/build:gcc-focal
  tags:
    - docker
  variables: *variables
  script: *script_build
  artifacts:
    paths:
    - ${CI_JOB_NAME}/artifacts.tar.gz
    expire_in: 1h
  before_script:
    - mkdir -p ccache
    - export CCACHE_BASEDIR=${PWD}
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --zero-stats || true
  after_script:
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --show-stats

.template: &build_nvcc
  stage: build
  tags:
    - cuda
  allow_failure: true
  image: registry.gitlab.com/nastja/nastja-build/build:nvcc
  variables: *variables_nvcc
  script: *script_build_nvcc
  artifacts:
    paths:
    - ${CI_JOB_NAME}/artifacts.tar.gz
    expire_in: 1h
  before_script:
    - mkdir -p ccache
    - export CCACHE_BASEDIR=${PWD}
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --zero-stats || true
  after_script:
    - export CCACHE_DIR=${PWD}/ccache
    - ccache --show-stats

.template: &test
  image: registry.gitlab.com/nastja/nastja-build/build:clang-focal
  stage: test
  tags:
    - docker
  script: *script_test
  after_script: *script_after_test
  artifacts:
    reports:
      junit: ${CI_JOB_NAME/test/build}/junit_report_${CI_JOB_NAME}.xml

.template: &test_gcc
  image: registry.gitlab.com/nastja/nastja-build/build:gcc-focal
  stage: test
  tags:
    - docker
  script: *script_test
  after_script: *script_after_test
  artifacts:
    reports:
      junit: ${CI_JOB_NAME/test/build}/junit_report.xml

.template: &test_nvcc
  image: registry.gitlab.com/nastja/nastja-build/build:nvcc
  stage: test
  tags:
    - cuda
  allow_failure: true
  script: *script_test
  after_script: *script_after_test
  artifacts:
    reports:
      junit: ${CI_JOB_NAME/test/build}/junit_report_${CI_JOB_NAME}.xml

build_clang:
  <<: *build

build_clang_debug:
  <<: *build
  variables:
    <<: *variables
    CMAKE_BUILD_TYPE: "Debug"
    NASTJA_COVERAGE: "ON"

.build_clang_6:
  <<: *build
  variables:
    <<: *variables
    CXX: /usr/bin/clang++-6.0
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_clang_7:
  <<: *build
  variables:
    <<: *variables
    CXX: /usr/bin/clang++-7
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_clang_8:
  <<: *build
  variables:
    <<: *variables
    CXX: /usr/bin/clang++-8
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_clang_8:
  <<: *build
  variables:
    <<: *variables
    CXX: /usr/bin/clang++-8
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_gcc_7:
  <<: *build_gcc
  variables:
    <<: *variables
    CXX: /usr/bin/g++-7
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_gcc_8:
  <<: *build_gcc
  variables:
    <<: *variables
    CXX: /usr/bin/g++-8

build_gcc_8_float:
  <<: *build_gcc
  variables:
    <<: *variables
    NASTJA_TYPE_FLOAT_PRECISION: "single"
    CXX: /usr/bin/g++-8

build_nvcc_gcc_8:
  <<: *build_nvcc
  variables:
    <<: *variables_nvcc
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

build_nvcc_clang_8:
  <<: *build_nvcc
  variables:
    <<: *variables_nvcc
    CXX: /usr/bin/clang++-8
    CC: /usr/bin/clang-8
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

test_clang:
  <<: *test
  needs: ["build_clang"]

test_clang_debug:
  <<: *test
  script:
    - *script_test
    - ../scripts/ci/llvm_coverage.sh
  needs: ["build_clang_debug"]
  artifacts:
    paths:
    - ${CI_JOB_NAME/test/build}/coverage.profdata
    - ${CI_JOB_NAME/test/build}/nastja
    - ${CI_JOB_NAME/test/build}/unittest/unittest
    expire_in: 1h
    reports:
      junit: ${CI_JOB_NAME/test/build}/junit_report_${CI_JOB_NAME}.xml

.test_clang_6:
  <<: *test
  needs: ["build_clang_6"]
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

test_clang_7:
  <<: *test
  needs: ["build_clang_7"]
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

test_clang_8:
  <<: *test
  needs: ["build_clang_8"]
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

.test_gcc_7:
  <<: *test_gcc
  needs: ["build_gcc_7"]
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never

.test_gcc_8:
  <<: *test_gcc
  needs: ["build_gcc_8"]

test_nvcc_gcc_8:
  <<: *test_nvcc
  needs: ["build_nvcc_gcc_8"]
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never


.test_nvcc_clang_8:
  <<: *test_nvcc
  needs: ["build_nvcc_clang_8"]

deploy_clang_debug:
  image: registry.gitlab.com/nastja/nastja-build/build:clang-focal
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never
  stage: deployment
  needs: ["test_clang_debug"]
  script:
    - cd ${CI_JOB_NAME/deploy/build}
    - cmake .. -G "Ninja"
    - ../scripts/ci/llvm_covreport.sh
    - if [ "$CI_COMMIT_REF_SLUG" == "master" ]; then
    -   ninja doc
    -   cd docs/html
    -   ../../../scripts/ci/prettifyconfigkeys.sh
    -   cd -
    - fi
  artifacts:
    paths:
    - ${CI_JOB_NAME/deploy/build}/coverage
    - ${CI_JOB_NAME/deploy/build}/docs/html

pages:
  image: registry.gitlab.com/nastja/nastja-build/build:clang-focal
  rules:
    - if: $NIGHTLY_BUILD == "true"
      when: always
    - when: never
  script:
    - if [ "$CI_COMMIT_REF_SLUG" == "master" ]; then
    -   mkdir public
    -   branch=$(echo $CI_COMMIT_REF_SLUG | cut -d"-" -f1)
    -   mv build_clang_debug/coverage public/coverage
    -   if [ -d build_clang_debug/docs/html ]; then mv build_clang_debug/docs/html public/docs; fi
    - fi
  stage: .post
  artifacts:
    paths:
    - public
  needs: ["deploy_clang_debug"]
